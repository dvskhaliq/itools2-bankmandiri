<?php

namespace App\ServiceHandler\PBMLayer\Bulk;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use App\WebServiceResponse;
use Auth;
use App\User;
use Crypt;

class KnowledgeImp implements KnowledgeService
{
    public $wsdl_kms;

    public function __construct()
    {
        $this->wsdl_kms = env('WSDL_KMS');
    }
    
    public function getWsdlKMS(){
        return $this->wsdl_kms;
    }   
    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    public function updateKnowledgeCategorization($payload): WebServiceResponse
    {
        $data = array(
            'KmsID' => $payload['KmsID'] == null ? null : $payload['KmsID'],
            'Title' => $payload['Title'] == null ? null : $payload['Title'],
            'ProductCatagorization1' => $payload['ProductCatagorization1'] == null ? null : $payload['ProductCatagorization1'],
            'ProductCatagorization2' => $payload['ProductCatagorization2'] == null ? null : $payload['ProductCatagorization2'],
            'ProductCatagorization3' => $payload['ProductCatagorization3'] == null ? null : $payload['ProductCatagorization3'],
            'OprCatagorization1' => $payload['OprCatagorization1'] == null ? null : $payload['OprCatagorization1'],
            'OprCatagorization2' => $payload['OprCatagorization2'] == null ? null : $payload['OprCatagorization2'],
            'OprCatagorization3' => $payload['OprCatagorization3'] == null ? null : $payload['OprCatagorization3'],
        );
        try {
            $client = new SoapClient($this->getWsdlKMS(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->UpdateKnowledgeCategorization($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse('Doc. ID:'.$payload['KmsID'].' has been updated !', 'success', $doc->getElementsByTagName('DocumentID')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

}
