import ENUM from "../../enum";

const state = {
    apiState: ENUM.INIT,
    alertMessage: "",
    incidentRelationData: {},
    incidentData: {},
    problemData: {},
};

const getters = {
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getIncidentRelationData: state => state.incidentRelationData,
    getIncidentData: state => state.incidentData,
    getProblemData: state => state.problemData,
};

const actions = {
    async createRelation({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/bulk/relation/create", payload
              );
              if(response.data.status == true){
                commit("setSuccess", "Relation has been created successfully !");
                // commit("setIncidentRelationData", response.data.result);
              }
              // else{
              //   commit("setException", response.data.message);
              // }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
                commit("setIncidentRelationData", response.data.result);
        }
      },
      async stopProcess({ commit }, payload) {
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/bulk/process/stop", payload);
                commit("setSuccess", "The process has been successfully terminated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
                commit("setIncidentRelationData", response.data.result);
        }
      },
      async createIncident({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/bulk/incident/create", payload
              );
              if(response.data.status == true){
                commit("setSuccess", "Incident has been created successfully !");
                commit("setIncidentData", response.data.result);
              }else{
                commit("setException", response.data.message);
              }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
                commit("setIncidentData", response.data.result);
        }
      },
      async updateIncident({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/bulk/incident/update", payload
              );
              if(response.data.status == true){
                commit("setSuccess", "Incident has been updated successfully !");
                commit("setIncidentData", response.data.result);
              }else{
                commit("setException", response.data.message);
              }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
                commit("setIncidentData", response.data.result);
        }
      },
      async updateProblem({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/bulk/problem/update", payload
              );
              if(response.data.status == true){
                commit("setSuccess", "Problem has been updated successfully !");
                commit("setProblemData", response.data.result);
              }else{
                commit("setException", response.data.message);
              }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
                commit("setProblemData", response.data.result);
        }
      },
};

const mutations = {
    setIncidentRelationData: (state, data)=>{
        state.incidentRelationData = data;
    },
    setIncidentData: (state, data)=>{
        state.incidentData = data;
    },
    setProblemData: (state, data)=>{
      state.problemData = data;
  },
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
