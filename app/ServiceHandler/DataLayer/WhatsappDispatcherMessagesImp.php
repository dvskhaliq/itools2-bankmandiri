<?php

namespace App\ServiceHandler\DataLayer;

use App\WhatsappDispatcherMessages;
use App\Http\Resources\WhatsappDispatcherMessages as WADispatcherMsgResource;
use App\SummaryWhatsappDispatcher;

class WhatsappDispatcherMessagesImp implements WhatsappDispatcherMessagesService
{
    public $status = false;
    public $message = 'this is default message';

    public function addDispatcherMessage($array_wa_dispatcher): WADispatcherMsgResource
    {
        try {
            $wa_dis = new WhatsappDispatcherMessages();
            $wa_dis->request_id = (array_key_exists("request_id", $array_wa_dispatcher) ? $array_wa_dispatcher["request_id"] : null);
            $wa_dis->sender_id = (array_key_exists("sender_id", $array_wa_dispatcher) ? $array_wa_dispatcher["sender_id"] : null);
            $wa_dis->content = (array_key_exists("content", $array_wa_dispatcher) ? $array_wa_dispatcher["content"] : null);
            $wa_dis->timestamp = date('Y-m-d H:i:s');
            $wa_dis->save();
            return new WADispatcherMsgResource($wa_dis);
        } catch (\Exception $th) {
            $this->status = false;
            $this->message = "message save failed: " . $th->__toString();
            return "ERROR";
        }
    }

    // ------------------------------------------------------------------ PERUBAHAN -->
    public function deleteDispatcherMessage($message_id)
    {

      $message = WhatsappDispatcherMessages::find($message_id);
      $message->delete();
    }
// ------------------------------------------------------------------ PERUBAHAN -->
    public function getDispatcherMessage($array_clause)
    {
        $generated_clause = array();
        foreach ($array_clause as $clause) {
            $arr_temp = [key($clause), $clause];
            array_push($generated_clause, $arr_temp);
        }
        $message = WhatsappDispatcherMessages::where(
            $generated_clause
        )->get();
        return $message;
    }

    public function getDispatcherMessageList($request_id)
    {
        return WADispatcherMsgResource::collection(WhatsappDispatcherMessages::where('request_id', $request_id)->get());
    }

    public function getDispathcerMessageHistory($request_id)
    {
        return SummaryWhatsappDispatcher::find($request_id);
    }
}
