<?php

namespace App\ServiceHandler\BDSLayer;

use App\ServiceHandler\BDSLayer\BDSServerService;

class BDSUserProfileImp implements BDSUserProfileService
{

    protected $setDatabaseConnection;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->setDatabaseConnection = $databaseConnection;
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function getList()
    {
        $result = $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->select('UserID', 'UserName', 'LevelIDTeller', 'supervisor', 'Status', 'LineStatus', 'IPAddress', 'MonetaryUser', 'CTT', 'LevelIDPlatfrm')->orderBy('UserID', 'ASC')->get();
        return $result;

    }

    public function findById($userId){
        $result = $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->select('UserID', 'UserName', 'LevelIDTeller', 'supervisor', 'Status', 'LineStatus', 'IPAddress', 'MonetaryUser', 'CTT', 'LevelIDPlatfrm')->where('UserId', $userId)->get();
        return $result;
    }

    public function updateUserProfile($userId, $dataUpdate){
        $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->where('userId', $userId)->update($dataUpdate);
    }

    public function findTransLimit($levelId){
        $result = $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->select('UserID', 'LevelID', 'CurrCode', 'LmtDepCash', 'LmtWthrCash', 'LmtDepNCash', 'LmtWthrNCash', 'LmtCashOnHandMin', 'LmtCashOnHandMax', 'OffLmtDepCash', 'OffLmtWthrCash', 'OffLmtDepNCash', 'OffLmtWthrNCash', 'LmtIBTDep', 'LmtIBTWthr', 'LmtPinDep', 'LmtPinWthr')->join('TranLimit', 'TranLimit.LevelID', '=', 'UserProfile.LevelIDTeller')->where('LevelID', $levelId)->get();
        return $result;

    }

    public function findByLevelId($levelId){
        $result = $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->select('UserID', 'UserName', 'LevelIDTeller', 'supervisor', 'Status', 'LineStatus', 'IPAddress', 'MonetaryUser', 'CTT', 'LevelIDPlatfrm')->where('LevelIDTeller', $levelId)->get();
        return $result;

    }

    public function findHeadTeller(){
        $result = $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->select('LevelIDTeller')->where('LevelIDPlatfrm', 'V')->get();
        return $result;

    }

    public function listHolder(){
        $result = $this->setDatabaseConnection->initBdsConnection()->table('UserProfile')->select('UserID')->get();
        return $result;

    }
}
