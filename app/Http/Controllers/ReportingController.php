<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\ServiceHandler\DataLayer\InboxService;
use View;
use JavaScript;
use App\ServiceHandler\ReportLayer\ReportService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;


class ReportingController extends BaseController
{
    protected $report_ds;
    protected $inbox_ds;
    public function __construct(
        ReportService $report_service,
        InboxService $inbox_service


        
    ) {
        $this->report_ds = $report_service;
        $this->inbox_ds = $inbox_service;
    }

    public function chartReport()
    {
        $data = $this->report_ds->generateSummaryTotal();

        JavaScript::put([
            "total_request_summary" => 1,
            "total_request_category" => $data[1],
            "total_request_channel" => $data[2],
            "total_request_type" => $data[3],
            "total_chart" => $data[4],
            "top_five_channel" => $data[5],
        ]);

        return View::make('report/public/reporting_chart');
    }

    public function generateSummaryTotalReport()
    {
        $data = $this->report_ds->generateSummaryTotal();
        JavaScript::put([
            "total_request_summary" => $data[0],
            "total_request_category" => $data[1],
            "total_request_channel" => $data[2],
            "total_request_type" => $data[3],
            "top_five_channel" => $data[5],
            "total_chart" => 1
        ]);

        return View::make('report/public/reporting_summary');
    }

    public function chartReportByDate($pdate)
    {
        $data = $this->report_ds->generateSummaryTotalByDate($pdate);

        JavaScript::put([
            "total_request_summary" => 1,
            "total_request_category" => $data[1],
            "total_request_channel" => $data[2],
            "total_request_type" => $data[3],
            "total_chart" => $data[4],
            "top_five_channel" => $data[5],
        ]);

        return View::make('report/public/reporting_chart_bydate');
    }

    public function generateSummaryTotalReportByDate($pdate)
    {
        $data = $this->report_ds->generateSummaryTotalByDate($pdate);
        JavaScript::put([
            "total_request_summary" => $data[0],
            "total_request_category" => $data[1],
            "total_request_channel" => $data[2],
            "total_request_type" => $data[3],
            "top_five_channel" => $data[5],
            "total_chart" => 1
        ]);

        return View::make('report/public/reporting_summary_bydate');
    }

    public function getPersonalTaskReport()
    {
        $assigned_task = $this->inbox_ds->getSingleDailyTaskStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'BookDateTime');
        $accomplished_task = $this->inbox_ds->getSingleDailyTaskStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'FinishDateTime');
        $label = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        $data['assigned'] = $assigned_task;
        $data['accomplished'] = $accomplished_task;
        $data['label'] = $label;
        return $data;
    }

    public function getTaskReportByDate()
    {
        $selecteddate = request()->get('selecteddate');
        $start = $selecteddate . ' 00:00:00';
        if ($selecteddate == "") {
            $start_date = date("Y-m-d H:i:s", strtotime("today"));
            $end_date = date("Y-m-d H:i:s", strtotime("tomorrow"));
            $start_date_weekly = date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today")));
            $end_date_weekly = date("Y-m-d H:i:s", strtotime("tomorrow"));
        } else {
            $start_date = $selecteddate . ' 00:00:00';
            $end_date = date("Y-m-d H:i:s", strtotime($start_date . "1 day", strtotime("tomorrow")));
            $start_date_weekly = date("Y-m-d H:i:s", strtotime($start . "-6 day", strtotime("today")));
            $end_date_weekly = date("Y-m-d H:i:s", strtotime($start . "1 day", strtotime("tomorrow")));
        }
        $assigned_task = $this->inbox_ds->getSingleDailyTaskStats(Auth::user()->employee_remedy_id, $start_date, $end_date, 'BookDateTime');
        $accomplished_task = $this->inbox_ds->getSingleDailyTaskStats(Auth::user()->employee_remedy_id, $start_date, $end_date, 'FinishDateTime');
        $label = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        $data['dailytask'] = ['assigned' => $assigned_task, 'accomplished' => $accomplished_task, 'label' => $label];

        $assigned_task_weekly = (array) $this->inbox_ds->getSingleWeeklyTaskStats(Auth::user()->employee_remedy_id, $start_date_weekly, $end_date_weekly, "BookDateTime");
        $accomplished_task_weekly = (array) $this->inbox_ds->getSingleWeeklyTaskStats(Auth::user()->employee_remedy_id, $start_date_weekly, $end_date_weekly, "FinishDateTime");
        $data['weeklytask'] = ['assigned' => array_values($assigned_task_weekly), 'accomplished' => array_values($accomplished_task_weekly), 'label' => array_values($accomplished_task_weekly)];

        $assigned_task_channel = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, $start_date, $end_date, 'INPROGRESS', 'BookDateTime');
        $accomplished_task_channel = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, $start_date, $end_date, 'CLOSED', 'FinishDateTime');
        $data['dailychannel'] = ['assigned' => array_values($assigned_task_channel), 'accomplished' => array_values($accomplished_task_channel), 'label' => array_keys($accomplished_task_channel)];

        $assigned_task_channel_weekly = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, $start_date_weekly, $end_date_weekly, 'INPROGRESS', 'BookDateTime');
        $accomplished_task_channel_weekly = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, $start_date_weekly, $end_date_weekly, 'CLOSED', 'FinishDateTime');
        $data['weeklychannel'] = ['assigned' => array_values($assigned_task_channel_weekly), 'accomplished' => array_values($accomplished_task_channel_weekly), 'label' => array_keys($accomplished_task_channel_weekly)];

        return $data;
    }

    public function getPersonalWeeklyTaskReport()
    {
        $assigned_task = (array) $this->inbox_ds->getSingleWeeklyTaskStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today"))), date("Y-m-d H:i:s", strtotime("tomorrow")), "BookDateTime");
        $accomplished_task = (array) $this->inbox_ds->getSingleWeeklyTaskStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today"))), date("Y-m-d H:i:s", strtotime("tomorrow")), "FinishDateTime");
        $data['assigned'] = array_values($assigned_task);
        $data['accomplished'] = array_values($accomplished_task);
        $data['label'] = array_keys($accomplished_task);
        return $data;
    }

    public function getPersonalDailyChannelReport()
    {
        $assigned_task = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'INPROGRESS', 'BookDateTime');
        $accomplished_task = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'CLOSED', 'FinishDateTime');
        $data['assigned'] = array_values($assigned_task);
        $data['accomplished'] = array_values($accomplished_task);
        $data['label'] = array_keys($accomplished_task);
        return $data;
    }

    public function getPersonalWeeklyChannelReport()
    {
        $assigned_task = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today"))), date("Y-m-d H:i:s", strtotime("tomorrow")), 'INPROGRESS', 'BookDateTime');
        $accomplished_task = (array) $this->inbox_ds->getChannelStats(Auth::user()->employee_remedy_id, date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today"))), date("Y-m-d H:i:s", strtotime("tomorrow")), 'CLOSED', 'FinishDateTime');
        $data['assigned'] = array_values($assigned_task);
        $data['accomplished'] = array_values($accomplished_task);
        $data['label'] = array_keys($accomplished_task);
        return $data;
    }

    //Manager dashboard
    public function getAllStaffTotalRequestByChannel($support_group_id)
    {
        $process_status = ['CLOSED', 'INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING', 'REJECT'];
        $accomplished_task = (array) $this->inbox_ds->getGroupGrandTotalTask($support_group_id, date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")),  $process_status);
        $data['data'] = array_values($accomplished_task);
        return $data;

        console.log($support_group_id);
    }

    public function getAllStaffTaskDailyReport($support_group_id)
    {
        $total_task = (array) $this->inbox_ds->getGroupDailyTotalTask($support_group_id,  date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today"))), date("Y-m-d H:i:s", strtotime("tomorrow")), "BookDateTime");
        $data['data'] = array_values($total_task);
        $data['label'] = array_keys($total_task[0]['totalDailyTask']);
        return $data;
    }

    public function getAllStaffTaskHourlyReport($support_group_id)
    {
        $process_status = ['CLOSED', 'INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'];
        $accomplished_task = (array) $this->inbox_ds->getGroupHourlyTotalTask($support_group_id, date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'BookDateTime', $process_status);
        $label = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        $data['data'] = array_values($accomplished_task);
        $data['label'] = $label;
        return $data;
    }

    //Akses via web
    public function searchAllStaffTotalRequestByChannel()
    {
        $selecteddate = request()->get('selecteddate');
        if ($selecteddate !== "") {
            $start = $selecteddate . ' 00:00:00';
            $end_date = date("Y-m-d H:i:s", strtotime($start . "1 day", strtotime("tomorrow")));
        } else {
            $end_date = date("Y-m-d H:i:s", strtotime("tomorrow"));
        }

        switch (request()->get('period')) {
            case 'MONTHLY':
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime("-30 day", strtotime("today")));
                } else {
                    $start_date = date("Y-m-d H:i:s", strtotime($start . "-30 day", strtotime("today")));
                }
                break;
            case 'WEEKLY':
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today")));
                } else {
                    $start_date = date("Y-m-d H:i:s", strtotime($start . "-6 day", strtotime("today")));
                }
                break;
            case 'BIWEEKLY':
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime("-13 day", strtotime("today")));
                } else {
                    $start_date = date("Y-m-d H:i:s", strtotime($start . "-13 day", strtotime("today")));
                }
                break;
            default:
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime('today'));
                } else {
                    $start_date = $start;
                }
                break;
        }
        $process_status = [];
        switch (request()->get('processstatus')) {
            case 'CLOSED':
                $process_status = ['CLOSED'];
                break;
            case 'INPROGRESS':
                $process_status = ['INPROGRESS'];
                break;
            case 'PENDING':
                $process_status = ['PENDING'];
                break;
            case 'WAITINGAPR':
                $process_status = ['WAITINGAPR'];
                break;
            case 'PLANNING':
                $process_status = ['PLANNING'];
                break;
            default:
                $process_status = ['CLOSED', 'INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING', 'REJECT'];
                break;
        }
        $total = (array) $this->inbox_ds->getGroupGrandTotalTask(Auth::user()->sgn_id_remedy, $start_date, $end_date, $process_status);
        $data['data'] = array_values($total);
        return $data;
    }

    public function searchAllStaffTaskHourlyReport()
    {
        $selecteddate = request()->get('selecteddate');
        if ($selecteddate !== "") {
            $start_date = $selecteddate . ' 00:00:00';
            $end_date = date("Y-m-d H:i:s", strtotime($start_date . "1 day", strtotime("tomorrow")));
        } else {
            $start_date = date("Y-m-d H:i:s", strtotime("today"));
            $end_date = date("Y-m-d H:i:s", strtotime("tomorrow"));
        }

        $process_status = [];
        switch (request()->get('processstatus')) {
            case 'CLOSED':
                $process_status = ['CLOSED'];
                break;
            case 'INPROGRESS':
                $process_status = ['INPROGRESS'];
                break;
            case 'PENDING':
                $process_status = ['PENDING'];
                break;
            case 'WAITINGAPR':
                $process_status = ['WAITINGAPR'];
                break;
            case 'PLANNING':
                $process_status = ['PLANNING'];
                break;
            default:
                $process_status = ['CLOSED', 'INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'];
                break;
        }
        $accomplished_task = (array) $this->inbox_ds->getGroupHourlyTotalTask(Auth::user()->sgn_id_remedy, $start_date, $end_date, 'UpdatedInDB', $process_status);
        $label = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        $data['data'] = array_values($accomplished_task);
        $data['label'] = $label;
        return $data;
    }

    public function searchAllStaffTaskDailyReport()
    {
        $selecteddate = request()->get('selecteddate');
        if ($selecteddate !== "") {
            $start = $selecteddate . ' 00:00:00';
            $end_date = date("Y-m-d H:i:s", strtotime($start . "1 day", strtotime("tomorrow")));
        } else {
            $end_date = date("Y-m-d H:i:s", strtotime("tomorrow"));
        }

        $start_date = date("Y-m-d H:i:s", strtotime('today'));
        switch (request()->get('period')) {
            case 'MONTHLY':
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime("-30 day", strtotime("today")));
                } else {
                    $start_date = date("Y-m-d H:i:s", strtotime($start . "-30 day", strtotime("today")));
                }
                break;
            case 'BIWEEKLY':
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime("-13 day", strtotime("today")));
                } else {
                    $start_date = date("Y-m-d H:i:s", strtotime($start . "-13 day", strtotime("today")));
                }
                break;
            default:
                if ($selecteddate == "") {
                    $start_date = date("Y-m-d H:i:s", strtotime("-6 day", strtotime("today")));
                } else {
                    $start_date = date("Y-m-d H:i:s", strtotime($start . "-6 day", strtotime("today")));
                }
                break;
        }
        $total_task = (array) $this->inbox_ds->getGroupDailyTotalTask(Auth::user()->sgn_id_remedy,  $start_date, $end_date, "BookDateTime");
        $data['data'] = array_values($total_task);
        $data['label'] = array_keys($total_task[0]['totalDailyTask']);
        return $data;
    }
}
