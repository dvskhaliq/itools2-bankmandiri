@extends('master')
@section('sidebar')
<div class="page-sidebar" id="sidebar">
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="#"><img src="{{asset('/img/logo_itools.png')}}" style="margin-top: -15px" width="138px" height="48px"></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-title">Navigation</li>
        @can('dashboard-agent')
        <li class="{{setActive('dashboard/agent/LAYANAN_IT', 'active') }}">
            <a href="{{action('MainController@sdAgentDashboard',['service_type'=> 'LAYANAN_IT'])}}"><span class="glyphicon glyphicon-home"></span> <span class="xn-text">Home - IT</span></a>
        </li>
        @endcan        

        @can('dashboard-agent-branch')
        <li class="{{setActive('dashboard/agent/LAYANAN_CABANG', 'active') }}">
            <a href="{{action('MainController@sdAgentDashboard',['service_type'=> 'LAYANAN_CABANG'])}}"><span class="glyphicon glyphicon-home"></span> <span class="xn-text">Home - Branch</span></a>
        </li>
        @endcan

        @can('dashboard-agent-cards')
        <li class="{{setActive('dashboard/agent/LAYANAN_CC', 'active') }}">
            <a href="{{action('MainController@sdAgentDashboard',['service_type'=> 'LAYANAN_CC'])}}"><span class="glyphicon glyphicon-home"></span> <span class="xn-text">Home - Cards</span></a>
        </li>
        @endcan

        @can('dashboard-agent-sds')
        <li class="{{setActive('dashboard/agentSDS/LAYANAN_SDS', 'active') }}">
            <a href="{{action('SDSpecialistController@sdAgentDashboard',['service_type'=> 'LAYANAN_SDS'])}}"><span class="glyphicon glyphicon-home"></span> <span class="xn-text">Home - SDS</span></a>
        </li>
        @endcan

        @can('dashboard-manager')
        <li class="{{setActive('dashboard/manager/'.Auth::user()->sgn_id_remedy, 'active') }}">
            <a href="{{action('ManagerDashboard@sdManagerDashboard',['support_group_id'=> Auth::user()->sgn_id_remedy])}}"><span class="glyphicon glyphicon-briefcase"></span> <span class="xn-text">Manager</span></a>
        </li>
        @endcan

        @can('pending-ticket-remedy')
        <li class="{{setActive('pending_ticket_remedy/index', 'active') }}">
            <a href="{{action('PendingTicketRemedyController@index')}}"><span class="glyphicon glyphicon-list-alt"></span> <span class="xn-text">Pending Ticket</span>
                <count-pending-ticket  :count="countpendingticket" v-on:on-count-refresh="refreshCount"></count-pending-ticket>
            </a>
        </li>
        @endcan

        @can('dashboard-manager')
        <li class="xn-openable {{setActive('dashboard/incident/report/index', 'active') }} {{setActive('bds_server_configuration/index', 'active') }} {{setActive('user/profile', 'active') }}">
            <a href="javascript:void(0);"><span class="glyphicon glyphicon-save"></span>
                <span class="xn-text">Report</span></a>
            <ul>
                @can('dashboard-manager')
                <li class="{{setActive('dashboard/incident/report/index', 'active') }}">
                    <a href="{{action('IncidentInterfaceController@index')}}">Incident</a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan

        @can('bds-connection')
        <li class="xn-openable {{setActive('bds_server/index', 'active') }}  {{setActive('user_profile/index', 'active') }} {{setActive('kartu_instan/index', 'active') }} {{ setActive('surat_berharga/index', 'active') }} {{setActive('access_desc/index', 'active') }} {{ setActive('ej2/index', 'active') }} {{ setActive('parameter/index', 'active') }} {{ setActive('atm_detail/index', 'active') }} {{ setActive('cdm_detail/index', 'active') }} {{ setActive('crm_detail/index', 'active') }} {{setActive('branch_conf/index', 'active')}} {{setActive('ftp_connect/storage', 'active')}}">
            <a href="javascript:void(0)"><span class="fa fa-desktop"></span> <span class="xn-text">BDS Connection</span></a>
            <ul>
                <li class="{{setActive('ftp_connect/storage', 'active') }} ">
                    <a href="{{action('FTPConnectController@index')}}">
                        <span class="xn-text">Storage File Infra</span></a>
                </li>
                @can('bds-server')
                <li class="{{setActive('bds_server/index', 'active') }} ">
                    <a href="{{action('BDSServerController@index')}}">
                        <span class="xn-text">BDS Server</span></a>
                </li>
                @endcan
                
                @if(Session::get('data'))
                @can('bds-user-profile')
                <li class="{{setActive('user_profile/index', 'active') }}">
                    <a href="{{action('BDSUserProfileController@index')}}">BDS User Profile</a>
                </li>
                @endcan

                @can('bds-document-inventory')
                <li class="xn-openable {{setActive('kartu_instan/index', 'active')}} {{ setActive('surat_berharga/index', 'active') }}">
                    <a href="javascript:void(0)">BDS Document Inventory</a>
                    <ul>
                        <li class="{{setActive('surat_berharga/index', 'active') }}">
                            <a href="{{action('BDSSuratBerhargaController@index')}}">Surat Berharga</a>
                        </li>
                        <li class="{{setActive('kartu_instan/index', 'active') }}">
                            <a href="{{action('BDSKartuInstanController@index')}}">Kartu Instan</a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('bds-access-desc')
                <li class="xn-openable {{setActive('access_desc/index', 'active') }}">
                    <a href="javascript:void(0)">BDS Access Desc</a>
                    <ul>
                        <li class="{{setActive('access_desc/index', 'active') }}">
                            <a href="{{action('BDSAccessDescController@index')}}">Kotran</a>
                        <li>
                    </ul>
                </li>
                @endcan

                @can('bds-ej2')
                <li class="{{ setActive('ej2/index', 'active') }}">
                    <a href="{{action('BDSEJ2Controller@index')}}">BDS EJ2</a>
                </li>
                @endcan

                @can('bds-parameter')
                <li class="{{ setActive('parameter/index', 'active') }}">
                    <a href="{{action('BDSParameterController@index')}}">BDS Parameter</a>
                </li>
                @endcan

                @can('bds-atm')
                <li class="xn-openable {{ setActive('atm_detail/index', 'active') }} {{ setActive('cdm_detail/index', 'active') }} {{ setActive('crm_detail/index', 'active') }}">
                    <a href="javascript:void(0)">BDS ATM</a>
                    <ul>
                        <li class="{{ setActive('atm_detail/index', 'active') }}">
                            <a href="{{ action('BDSAtmDetailController@index') }}">ATM Detail</a>
                        <li>
                        <li class="{{ setActive('cdm_detail/index', 'active') }}">
                            <a href="{{ action('BDSCdmDetailController@index') }}">CDM Detail</a>
                        <li>
                        <li class="{{ setActive('crm_detail/index', 'active') }}">
                            <a href="{{ action('BDSCrmDetailController@index') }}">CRM Detail</a>
                        <li>
                    </ul>
                </li>
                @endcan

                @can('bds-branch-conf')
                <li class="{{setActive('branch_conf/index', 'active')}}">
                    <a href="{{action('BDSBranchConfController@index')}}">BDS Branch Conf</a>
                </li>
                @endcan

                @endif
            </ul>
        </li>
        @endcan

        @can('los')
        <li class="xn-openable {{setActive('los_consumer/index', 'active') }} {{setActive('fingerprint_status/index', 'active') }}">
            <a href="javascript:void(0);"><span class="glyphicon glyphicon-tasks"></span> <span class="xn-text">LOS</span></a>
            <ul>
                <li class="{{setActive('fingerprint_status/index', 'active') }}">
                    <a href="{{action('LosFingerprintStatusController@index')}}">Fingerprint Status</a>
                </li>

                <li class="{{setActive('los_consumer/index', 'active') }}">
                    <a href="{{action('LosConsumerController@index')}}">Los Consumer</a>
                </li>
            </ul>
        </li>
        @endcan

        @can('administrator-console')
        <li class="xn-openable {{setActive('user_configuration/index', 'active') }} {{setActive('bds_server_configuration/index', 'active') }} {{setActive('user/profile', 'active') }}">
            <a href="javascript:void(0);"><span class="glyphicon glyphicon-cog"></span>
                <span class="xn-text">Administrator Console</span></a>
            <ul>
                @can('user-configuration')
                <li class="{{setActive('user_configuration/index', 'active') }}">
                    <a href="{{action('ConfigurationUsersController@index')}}">User Configuration</a>
                </li>
                @endcan

                @can('bds-server-configuration')
                <li class="{{setActive('bds_server_configuration/index', 'active') }}">
                    <a href="{{action('ConfigurationBDSServerController@index')}}">BDS Server Configuration</a>
                </li>
                @endcan

                @can('user-profile')
                <li class="{{setActive('user/profile', 'active') }}">
                    <a href="{{action('ConfigurationUsersController@loginuser')}}">User Profile</a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="mb-control">
                <span class="fa fa-sign-out"></span><span class="xn-text">Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        </li>
    </ul>
</div>
<div class="page-content" id="itools-content">
    <loading-vue></loading-vue>
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        {{--<li class="xn-icon-button pull-right">
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="fa fa-sign-out"></span></a>
        <form class="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        </li>--}}
        <li class="pull-right">
            <a href="#" style="background:#fff;">
                <h6>{{ Auth::user()->name }}</h6>
            </a>
        </li>
    </ul>
    <div class="page-content-wrap">
        @yield('content')
    </div>

</div>
<!-- <script type="text/javascript" src="../../js/sidebar.js"></script> -->
<!-- <script type="text/javascript" src="../../js/dashboard.js"></script> -->
@endsection