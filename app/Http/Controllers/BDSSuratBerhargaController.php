<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BDSSuratBerhargaController extends BaseController
{
    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return View::make('bds_connection/bds_document_inventory/surat_berharga');
    }

    public function getList()
    {
        $result = $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->select('DocType', 'Prefix', 'SerialNo', 'SerialNo2', 'Holder', 'Status', 'Spoilt')->whereNotIn('DocType', array('KI'))->where('Status', 'U')->where('Spoilt', 'N')->whereNotIn('Holder', array('VAULT'))->get();

        return response()->json($result);
    }

    function getFilter(){
        $result = $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->select('DocType', 'Prefix', 'SerialNo', 'SerialNo2', 'Holder', 'Status', 'Spoilt')
        ->whereNotIn('DocType' , array('KI'))
        ->whereIn('Status' , array('U','I','Y'))
        ->where('Spoilt' , request()->get('spoilt'))
        ->whereNotIn('holder', array('VAULT'))->get();
        return response()->json($result);
    }

    public function findList()
    {
        if (request()->get('search') == 'null' || request()->get('search') == '') {
            return $this->getList();
        } else {
            $result = $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->select('DocType', 'Prefix', 'SerialNo', 'SerialNo2', 'Holder', 'Status', 'Spoilt')->whereNotIn('DocType', array('KI'))->where('SerialNo', request()->get('search'))->get();
            return response()->json($result);
        }
    }

    function getBySerialNo($serialNo)
    {
        $serialNo = $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->select('DocType', 'Prefix', 'SerialNo', 'SerialNo2', 'Holder', 'Status', 'Spoilt')->where('SerialNo', $serialNo)->get();

        return $serialNo;
    }

    public function submitSuratBerharga()
    {
        $findData = $this->getBySerialNo(request()->get('serialNo'));
        if ( $findData[0]['Holder'] == request()->get('holder')) {
            return response()->json([
                'status' => false,
                'result' => 'Holder : ' . request()->get('holder') . ' already in SerialNo : ' . request()->get('serialNo'),
            ]);
        } else {
            $data = array(
                'Holder' => request()->get('holder'),
                'AllocatedToday' => request()->get('allocatedToday'),
                'LastHolder' => $findData[0]['Holder'],
            );
            $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->where('SerialNo', request()->get('serialNo'))->whereNotIn('DocType', array('KI'))->update($data);
            return response()->json([
                'status' => true,
                'result' => 'SerialNo : ' . request()->get('serialNo') . ' change to Holder : ' . request()->get('holder'),
            ]);
        }
    }

    public function submitSuratBerhargaSubmitAll()
    {
        $suratberharga = collect(request()->get('surat_berharga'));
        $suratberharga->map(function ($item) {
        $findData = $this->getBySerialNo($item['SerialNo']);
            $data = array(
                'Holder' => 'VAULT',
                'AllocatedToday' => 'N',
                'LastHolder' => $findData[0]['Holder'],
            );
            $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->where('SerialNo', $item['SerialNo'])->whereNotIn('DocType', array('KI'))->update($data);
        });
        return response()->json([
            'status' => true,
            'result' => 'Holder has been succesfully changed to VAULT !',
        ]);
    }

    public function findSerialNo()
    {

        $results = $this->bdsServerService->initBdsConnection()->table('DocumentInventory')->select('DocType', 'Prefix', 'SerialNo', 'SerialNo2', 'Holder', 'Status', 'Spoilt')->whereNotIn('DocType', array('KI'))->where('SerialNo', request()->get('serial_no'))->where('Status', 'U')->get();

        return response()->json($results);
    }
}
