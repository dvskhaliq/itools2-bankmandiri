<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluAtm extends Model
{
    protected $table = 'pm_kopra_ts';
    public $timestamps = false;
}
