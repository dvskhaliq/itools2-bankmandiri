<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Incident Management System</title>
    <!-- Scripts -->
    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="https://fonts.gstatic.com"> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <script type="text/javascript" src="{{asset('assets/js/plugins/stompjs.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/socksjs.js')}}"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>

<body>
    <div id="onduty_dashboard">
        <index :auth="auth"></index>
    </div>
    @include('footer')
    <script type="text/javascript">
        audiofile = "{{asset('assets/sounds/request_alert.mp3')}}";
        // var socket = new SockJS('http://localhost:8080/itools/websocket');
        // var socket = new SockJS('http://10.254.152.105:8080/itools/websocket'); production
        // var socket = new SockJS('http://10.204.1.16:8080/itools/websocket'); //dev    
       // var socket = new SockJS("{{env('MIX_SOCKET_DASHBOARD')}}");    
        // stompclient = Stomp.over(socket);
        // stompclient.heartbeat.outgoing = 100000;
        // stompclient.heartbeat.incoming = 100000;
        // stompclient.reconnect_delay = 0;
        // stompclient.debug = null;
    </script>
    <script src="../../../js/onduty.js"></script>
</body>