<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MainController;
use App\Http\Controllers\ReportingController;

use Auth;
use JavaScript;
use View;
use App\Incident;
use App\Workorder;

class ManagerDashboard extends MainController
{
    public function sdManagerDashboard($support_group_id)
    {
        $request_queue = $this->inbox_ds->getRequestListByStatusAndSupportGroupJson(['INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'], ['CALL', 'EMAIL', 'ITSRM', 'LIVECHAT', 'SMS', 'TELEGRAM', 'WA'], $support_group_id);
        // $working_task = $this->taskrequest_service->getTaskListByAssingeeAndStatusJson(Auth::user()->employeeremedy->id, 'IN', ['INPROGRESS','PENDING','WAITINGAPR','PLANNING']);
        $in_history = $this->inbox_ds->getRequestsHistory($support_group_id, 'IN', true, "false");
        $wo_history = $this->inbox_ds->getRequestsHistory($support_group_id, 'WO', true, "false");
        $daily_chart['dailytotal'] = app()->call('App\Http\Controllers\ReportingController@getAllStaffTaskDailyReport',['support_group_id' => $support_group_id]);
        $daily_chart['hourlytotal'] = app()->call('App\Http\Controllers\ReportingController@getAllStaffTaskHourlyReport',['support_group_id' => $support_group_id]);
        $daily_chart['grandtotal'] = app()->call('App\Http\Controllers\ReportingController@getAllStaffTotalRequestByChannel',['support_group_id' => $support_group_id]);
        // $audit = ($support_group_id=='SGP000000000414'?$this->cpsserver_service->auditTrailView():null);
        JavaScript::put([
            "serviceType" => $support_group_id,
            // "workingTask" => $working_task,
            "inHistory" => $in_history,
            "woHistory" => $wo_history,
            "requestQueue" => $request_queue,
            "chart" => $daily_chart,
            "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']),
            // "audit" => $audit
        ]);
        return View::make('landing/dashboard_manager');
    }

    public function delegateIncidentManager()
    {
        $incident = new Incident();
        // $request = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
        $request = $this->inbox_ds->find(request()->get('id'));
        $content = request()->get('assignee');
        $assignee = $this->suppusr_ds->findByCompanyOrgGroupName($content['assignee_company'], $content['assignee_organization'], $content['assignee_group'], $content['assignee_fullname']);
        if ($assignee) {
            $array_inbox = array(
                "AssigneeId" => Auth::user()->id,
                "EmployeeAssigneeId" => $assignee->employee_remedy_id,
                "EmployeeAssigneeGroupId" => $assignee->support_group_id,
                "ProcessStatus" => 'QUEUED',
            );
            $this->inbox_ds->update($request->id, $array_inbox);
            $incident->parseInbox($this->inbox_ds->refresh($request));
            $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident);
        } else {
            $array_inbox = array(
                "AssigneeId" => Auth::user()->id,
                "EmployeeAssigneeId" => null,
                "EmployeeAssigneeGroupId" => null,
                "ProcessStatus" => 'QUEUED',
            );
            $this->inbox_ds->update($request->id, $array_inbox);
            $incident->parseInbox($this->inbox_ds->refresh($request));
            $group = $this->suppgrp_ds->findByComOrgGrp($content['assignee_company'], $content['assignee_organization'], $content['assignee_group']);
            $incident->assignee_support_company = $group->company;
            $incident->assignee_support_organization = $group->organization;
            $incident->assignee_support_group_name = $group->support_group_name;
            $incident->assignee_support_group_id = $group->id;
            $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident);
        }
        if ($wsdl->getResponseStatus() == 'success') {
            $message = "Ticket with Ref No : " . $request->id . " - " . $wsdl->getContent() . " has been Assigned to group " . ($assignee ? $assignee->support_group_name . " -> " . $assignee->full_name : $group->support_group_name);
        } else {
            $array_inbox = array(
                "AssigneeId" => $request->AssigneeId,
                "EmployeeAssigneeId" => $request->EmployeeAssigneeId,
                "EmployeeAssigneeGroupId" => $request->EmployeeAssigneeGroupId,
                "ProcessStatus" => $request->ProcessStatus,
            );
            $this->inbox_ds->update($request->id, $array_inbox);
            $message = "Ticket with Ref No : " . $request->id . " failed to synce with remedy, please retry again for assigne to other group";
        }
        return response()->json(["status" => $wsdl->getResponseStatus() == 'success' ? true : false, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'incident' => $wsdl->getArrContent(), 'request' => $request, 'assignee' => $assignee]);
    }

    public function delegateWorkOrderManager()
    {
        $wo = new Workorder();
        // $request = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
        $request = $this->inbox_ds->find(request()->get('id'));
        $content = request()->get('assignee');
        $delegate_to = $this->suppusr_ds->findByCompanyOrgGroupName($content['assignee_company'], $content['assignee_organization'], $content['assignee_group'], $content['assignee_fullname']);
        $wo->parseInbox($this->inbox_ds->update($request->id, array("EmployeeAssigneeId" => $delegate_to->employee_remedy_id, "EmployeeAssigneeGroupId" => $delegate_to->support_group_id)));
        $wsdl = $this->woupdate_wsdl->updateWOGeneric($wo);
        if ($wsdl->getResponseStatus() == 'success') {
            $message = "Ref No :" . $request->id . " - " . $request->RemedyTicketId . " delegated to :" . $wo->assignee_name;
        } else {
            $this->inbox_ds->update($request->id, array("AssigneeId" => Auth::user()->id, "ActionTicketCreation" => 'IN_PENDING_DLG'));
            $message = "Ref No :" . $request->id . " - " . $request->RemedyTicketId . " failed to delegate, check Pending Ticket menu and resubmit immediately!";
        }
        return response()->json(["status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $wo, 'wsdl_request' => $wsdl->getArrContent()]);
    }

    public function searchAgentTask()
    {
        $status = ['INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'];
        switch (request()->get('status')) {
            case 'INPROGRESS':
                $status = ['INPROGRESS'];
                break;
            case 'PENDING':
                $status = ['PENDING'];
                break;
            case 'WAITINGAPR':
                $status = ['WAITINGAPR'];
                break;
            case 'PLANNING':
                $status = ['PLANNING'];
                break;
            default:
                $status = ['INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'];
                break;
        }

        $channel = ['CALL', 'EMAIL', 'ITSRM', 'LIVECHAT', 'SMS', 'TELEGRAM', 'WA'];
        switch (request()->get('channel')) {
            case 'CALL':
                $channel = ['CALL'];
                break;
            case 'EMAIL':
                $channel = ['EMAIL'];
                break;
            case 'ITSRM':
                $channel = ['ITSRM'];
                break;
            case 'LIVECHAT':
                $channel = ['LIVECHAT'];
                break;
            case 'SMS':
                $channel = ['SMS'];
                break;
            case 'TELEGRAM':
                $channel = ['TELEGRAM'];
                break;
            case 'WA':
                $channel = ['WA'];
                break;
            default:
                $channel = ['CALL', 'EMAIL', 'ITSRM', 'LIVECHAT', 'SMS', 'TELEGRAM', 'WA'];
                break;
        }

        return $this->inbox_ds->getRequestListByStatusAndSupportGroupJson($status, $channel, request()->get('supportgroupid'), request()->get('field'), request()->get('searchdate'), request()->get('tosearchdate'), request()->get('search'));
    }

    //************************* Below Code is Before Assessment ********************************************* 



    // public function closeIncidentManager() // Fungsi ini tidak terpakai, sekarang pakai yg updateIncident maincontroller
    // {
    //     // $request = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
    //     $task = $this->inbox_ds->find(request()->get('id'));
    //     $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($task->EmployeeAssigneeId, $task->EmployeeAssigneeGroupId);
    //     $wsdl_add_resolution = $this->incupdate_wsdl->addIncidentResolutionManager($task->RemedyTicketId, request()->get('notes'), $actual_assignee);
    //     if ($wsdl_add_resolution->getResponseStatus() == 'success') {
    //         $wsdl_update_status_rslt = $this->incupdate_wsdl->updateIncidentStatusManager($task, request()->get('status'), $actual_assignee);
    //         if ($wsdl_update_status_rslt->getResponseStatus() == 'success') {
    //             $text = $task->TextDecoded . request()->get('notes');
    //             // $inbox = $this->taskrequest_service->updateStatusIncidentById(request()->get('id'), 'CLOSED', 'IN_COMPLETED', $text);
    //             $array_inbox = array(
    //                 "TextDecoded" => $text,
    //                 "ProcessStatus" => 'CLOSED',
    //                 "ActionTicketCreation" => 'IN_COMPLETED',
    //                 "FinishDateTime" => date('Y-m-d H:i:s'),
    //                 "EmployeeAssigneeId" => $actual_assignee->supportgroupassigneeremedy->employee_remedy_id,
    //                 "EmployeeAssigneeGroupId" => $actual_assignee->supportgroupassigneeremedy->support_group_id
    //             );
    //             $inbox = $this->inbox_ds->update(request()->get('id'), $array_inbox);
    //             $this->notif_ds->performFeedBackNotifier($inbox, request()->get('status'), request()->get('notes'));
    //             return response()->json(["status" => true, "message" => "Note added and ticket " . $task->RemedyTicketId . " has been closed", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //         } else {
    //             $this->taskrequest_service->updateStatusById(request()->get('id'), request()->get('assigneeId'), $actual_assignee->id, 'CLOSED', 'IN_PENDING_CLS');
    //             return response()->json(["status" => true, "message" => "Note added, but failed to close the ticket! please retry process from Pending Ticket Remedy menu", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     } else {
    //         return response()->json(["status" => false, "message" => "Unable to connect to Remedy, your request not being processed!", 'wsdl' => $wsdl_add_resolution->getResponseMessage(), 'wsdlStatus' => false]);
    //     }
    // }

    // public function closeWorkOrderManager() // Fungsi ini tidak terpakai, sekarang pakai yg updateWO maincontroller
    // {
    //     // $request = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
    //     $task = $this->inbox_ds->find(request()->get('id'));
    //     $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($task->EmployeeAssigneeId, $task->EmployeeAssigneeGroupId);
    //     $wsdl_add_note_rslt = $this->wocreate_wsdl->addNote($task, request()->get('notes'));
    //     if ($wsdl_add_note_rslt->getResponseStatus() == 'success') {
    //         $wsdl_update_status_rslt = $this->woupdate_wsdl->updateStatus($task, request()->get('status'), $actual_assignee->id);
    //         if ($wsdl_update_status_rslt->getResponseStatus() == 'success') {
    //             // $inbox = $this->taskrequest_service->updateStatusById(request()->get('id'), request()->get('assigneeId'), $actual_assignee->id, 'CLOSED', 'WO_COMPLETED');
    //             $array_inbox = array(
    //                 "AssigneeId" => request()->get('assigneeId'),
    //                 "ProcessStatus" => 'CLOSED',
    //                 "ActionTicketCreation" => 'WO_COMPLETED',
    //                 "FinishDateTime" => date('Y-m-d H:i:s'),
    //                 "EmployeeAssigneeId" => $actual_assignee->supportgroupassigneeremedy->employee_remedy_id,
    //                 "EmployeeAssigneeGroupId" => $actual_assignee->supportgroupassigneeremedy->support_group_id
    //             );
    //             $inbox = $this->inbox_ds->update(request()->get('id'), $array_inbox);
    //             $this->notif_ds->performFeedBackNotifier($inbox, request()->get('status'), request()->get('notes'));
    //             return response()->json(["status" => true, "message" => "Note added and ticket " . $task->RemedyTicketId . " has been closed", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //         } else {
    //             $this->taskrequest_service->updateStatusById(request()->get('id'), $actual_assignee->id, request()->get('assigneeId'), 'CLOSED', 'WO_PENDING_CLS');
    //             return response()->json(["status" => true, "message" => "Note added, but failed to close the ticket! please retry process from Pending Ticket Remedy menu", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     } else {
    //         return response()->json(["status" => false, "message" => "Unable to connect to Remedy, your request not being processed!"]);
    //     }
    // }

    // private function parseWorkorder(Inbox $request)
    // {
    //     if ($request->ProcessStatus == 'BOOKED') {
    //         if ($request->RecipientID == 'ITSRM') {
    //             $wsdl = $this->wocreate_wsdl->bookTicket($request, request()->get('assigneeId'));
    //             $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task" : "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task, but not reflected in remedy, please retry process from Pending Ticket Remedy!";
    //         } else {
    //             $wsdl = $this->wocreate_wsdl->createWorkOrder($request, request()->get('assigneeId'));
    //             $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $wsdl->getContent() . " created and assigned to your task" : "Ref ID : " . $request->id . " assigned to your task, but failed to create remedy ticket, please retry process from Pending Ticket Remedy!";
    //         }
    //         $data = $wsdl->getResponseStatus() == 'success' ? $this->initRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id')) : $this->pendingRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id'), "INPROGRESS", "WO_PENDING_PRS", request()->get('assigneeId'), request()->get('assigneeId'));
    //         $inbox = $this->inbox_ds->refresh($request);
    //         $this->notif_ds->performFeedBackNotifierWO($inbox, $note = '');
    //         return response()->json(["status" => true, "message" => $message, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false]);
    //     } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING' || $request->ProcessStatus == 'CLOSED') && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
    //         $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //         return response()->json(["status" => true, "message" => "Related workorder in progress", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //     } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING' || $request->ProcessStatus == 'CLOSED') && request()->get('manager') == true) {
    //         $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //         return response()->json(["status" => true, "message" => "Related workorder is in progress", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //     }else if ($request->ProcessStatus == 'INPROGRESS') {
    //         $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //         return response()->json(["status" => false, "message" => "Workorder with Ref ID : " . $data->id . " - " . $data->RemedyTicketId . " has been BOOKED by: " . $data->supportgroupassigneeremedy->full_name, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //     } else {
    //         return response()->json(["status" => false, "message" => "No ticket available with Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " has been booked or closed by other agent", "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //     }
    // }

    // private function parseIncident(Inbox $request)
    // {
    //     if($this->tlgrmusr_ds->hasActiveChat($request->EmployeeSenderId) && ($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING')){
    //         return redirect()->action("LiveChatController@getChat", ["request_id" => $request->id]);
    //     } else {
    //         if ($request->ProcessStatus == 'BOOKED') {
    //             if ($request->RecipientID == 'ITSRM') {
    //                 $wsdl = $this->incupdate_wsdl->bookIncident($request, request()->get('assigneeId'));
    //                 $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task" : "Ref ID : " . $request->id . "-" . $request->RemedyTicketId . " assigned to your task, but not reflected in remedy, please retry process from Pending Ticket Remedy!";
    //             } else if ($request->RecipientID == 'LIVECHAT') {
    //                 return redirect()->action("LiveChatController@initChat", ["request_id" => $request->id]);
    //             } else if ($request->RecipientID == 'WA') {
    //                 return redirect()->action("WhatsappDispatcherController@initIncidentWhatsappDispatcher", ["request_id" => $request->id]);
    //             } else {
    //                 if ($request->RemedyTicketId !== "N/A") {
    //                     $wsdl = $this->incupdate_wsdl->bookIncident($request, request()->get('assigneeId'));
    //                     $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task" : "Ref ID : " . $request->id . "-" . $request->RemedyTicketId . " assigned to your task, but not reflected in remedy, please retry process from Pending Ticket Remedy!";
    //                 } else {
    //                     $assignee = $this->suppusr_ds->findByPersonIdGroupId(Auth::user()->employee_remedy_id, Auth::user()->sgn_id_remedy);
    //                     $wsdl = $this->inccreate_wsdl->createIncidentBook($request, $assignee);
    //                     $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $wsdl->getContent() . " created and assigned to your task" : "Ref ID : " . $request->id . " assigned to your task, but failed to create remedy ticket, please retry process from Pending Ticket Remedy!";
    //                 }
    //             }
    //             $data = $wsdl->getResponseStatus() == 'success' ? $this->initRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id')) : $this->pendingRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id'), "INPROGRESS", "IN_PENDING_PRS", request()->get('assigneeId'), request()->get('assigneeId'));
    //             $inbox = $this->inbox_ds->refresh($request);
    //             $this->notif_ds->performFeedBackNotifier($inbox);
    //             return response()->json(["status" => true, "message" => $message, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false]);
    //         } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING') &&    $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
    //             if ($request->RecipientID == 'WA') {
    //                 return redirect()->action("WhatsappDispatcherController@getIncidentWhatsappDispatcher", ["request_id" => $request->id]);
    //             } else {
    //                 $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //                 return response()->json(["status" => true, "message" => "Related incident is in progress 1", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);                    
    //             }
    //         } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING') && request()->get('manager') == true) {
    //             $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //             return response()->json(["status" => true, "message" => "Related incident is in progress 4", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //         } else if ($request->ProcessStatus == 'INPROGRESS') {
    //             $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //             return response()->json(["status" => false, "message" => "Incident with Ref ID : " . $data->id . " - " . $data->RemedyTicketId . " has been BOOKED by: " . $data->supportgroupassigneeremedy->full_name, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //         } else if ($request->ProcessStatus == 'CLOSED') {
    //             $data = $this->getRequestDetail(request()->get('serviceCatalogId'), request()->get('id'));
    //             return response()->json(["status" => true, "message" => "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " is closed", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //         } else {
    //             return response()->json(["status" => false, "message" => "No ticket available with Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " has been booked or closed by other agent", "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    //         }
    //     }
    // }

    // public function selectRequest()
    // {
    //     $request = $this->inbox_ds->book(request()->get('id'));
    //     if ($request->servicecatalog->service_catalog_type == 'WO') {
    //         return $this->parseWorkorder($request);
    //     } else if ($request->servicecatalog->service_catalog_type == 'IN') {
    //         return $this->parseIncident($request);
    //     }
    // }
}
