<?php

namespace App\ServiceHandler\DataLayer;

use App\PBMProblemInvestigation as Problem;
use Illuminate\Database\Eloquent\Collection;
use phpDocumentor\Reflection\Types\Boolean;

interface ProblemService
{
    public function getSummary($year, $category): ?Collection;    
    public function tranformToProductionIssues(array $year): ?Collection;
}
