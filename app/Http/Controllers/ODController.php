<?php

namespace App\Http\Controllers;

use Auth;
use JavaScript;

use Illuminate\Http\Request;
use App\ServiceHandler\DataLayer\InboxService;
use App\ServiceHandler\DataLayer\ODIncidentService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderUpdateService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use App\ServiceHandler\DataLayer\InboxWorkInfoLogService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\NotificationLayer\NotificationService;

use SebastianBergmann\Environment\Console;
use View;
use App\Incident;
use App\EmployeeRemedy;



class ODController extends BaseController
{

    use AuthenticatesUsers;
    protected $inbox_ds;
    protected $od_incident;
    protected $notif_ds;
    protected $inccreate_wsdl;
    protected $incupdate_wsdl;
    protected $wocreate_wsdl;
    protected $woupdate_wsdl;
    protected $workinfolog_ds;
    protected $suppusr_ds;


    protected $redirectTo = '/onduty/dashboard';
    protected $guard = 'onduty';
    
    /*
    public function __construct(
        InboxService $inbox_service,
        ODIncidentService $od_incident_service
    ) {
        $this->inbox_ds = $inbox_service;
        $this->od_incident = $od_incident_service;
        $this->middleware('auth');
    }*/

    public function __construct(InboxService $inbox_service,
    ODIncidentService $od_incident_service,
    WOIWorkOrderService $workorder_create_service,
    WOIWorkOrderUpdateService $workorder_update_service,
    WOIIncidentService $incident_create_service,
    WOIIncidentUpdateService $incident_update_services,
    InboxWorkInfoLogService $workinfolog_service,
    NotificationService $notif_service,
    SupportUserService $support_user_service
    )
    {
        $this->middleware('guest:onduty')->except('logout');
        $this->inbox_ds = $inbox_service;
        $this->od_incident = $od_incident_service;
        $this->wocreate_wsdl = $workorder_create_service;
        $this->woupdate_wsdl = $workorder_update_service;
        $this->inccreate_wsdl = $incident_create_service;
        $this->incupdate_wsdl = $incident_update_services;
        $this->workinfolog_ds = $workinfolog_service;
        $this->suppusr_ds = $support_user_service;
        $this->notif_ds = $notif_service;

    }

    public function index(){
        try{
            if(Auth::user()){
                JavaScript::put([
                    "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']),
                    "roles" => array("dashboard-od-comcen"=>Auth::user()->can('dashboard-od-comcen'),"dashboard-od-first-layer"=>Auth::user()->can('dashboard-od-first-layer'),"dashboard-od-second-layer"=>Auth::user()->can('dashboard-od-second-layer'))
                ]);
                return View::make('onduty/od_dashboard');
            }else{
                return redirect('/onduty/login');
                //return View::make('onduty/login');
            }
        }catch(Throwable $e){
            return View::make('onduty/login');
        }
    }

    protected function logout(Request $request)
    {
        $this->guard()->logout();
        // $request->session()->invalidate();
        return redirect('/onduty/login');
    }

    public function updateRequestDueException(Int $request_id, String $process_status, String $integration_status, Int $user_id, Int $assignee_id = null)
    {
        $assignee = $this->suppusr_ds->findByUserId($assignee_id);
        if ($assignee) {
            $array_inbox = array(
                "ProcessStatus" => $process_status,
                "ActionTicketCreation" => $integration_status,
                "AssigneeId" => $user_id,
                "EmployeeAssigneeId" => $assignee->employee_remedy_id,
                "EmployeeAssigneeGroupId" => $assignee->support_group_id,
            );
        } else {
            $array_inbox = array(
                "ProcessStatus" => $process_status,
                "ActionTicketCreation" => $integration_status,
                "AssigneeId" => $user_id,
            );
        }
        $this->inbox_ds->update($request_id, $array_inbox);
    }

    public function oddashboarddev(){
        $service_type = 'LAYANAN_CABANG';
        // $request_queue = $this->taskrequest_service->getRequestListByStat3usAndCategoryJson('QUEUED', $service_type);
        $request_queue = $this->inbox_ds->getByStatusAndCategory('QUEUED', 'LAYANAN_CABANG');
        // $working_task = $this->taskrequest_service->getTaskListByAssingeeAndStatusJson(Auth::user()->employeeremedy->id, 'INPROGRESS');
        $working_task = $this->inbox_ds->getByAssigneeAndStatus(Auth::user()->employeeremedy->id, 'INPROGRESS');
        $in_history = $this->od_incident->getRequestsHistory(Auth::user()->employee_remedy_id, 'IN', false);
        //$wo_history = $this->inbox_ds->getRequestsHistory(Auth::user()->employee_remedy_id, 'WO', false);
      //  $in_history=null;        
        $wo_history = null;
        $daily_chart['dailytask'] = app()->call('App\Http\Controllers\ReportingController@getPersonalTaskReport');
        $daily_chart['weeklytask'] = app()->call('App\Http\Controllers\ReportingController@getPersonalWeeklyTaskReport');
        $daily_chart['dailychannel'] = app()->call('App\Http\Controllers\ReportingController@getPersonalDailyChannelReport');
        $daily_chart['weeklychannel'] = app()->call('App\Http\Controllers\ReportingController@getPersonalWeeklyChannelReport');
        JavaScript::put([
            "serviceType" => $service_type,
            "workingTask" => $working_task,
            "inHistory" => $in_history,
            "woHistory" => $wo_history,
            "requestQueue" => $request_queue,
            "chart" => $daily_chart,
            "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']),
        ]);
        return View::make('onduty/od_content');
    }

     /* Add by 1991725227 - putut.mukti dashboard OD 19082020*/
    public function createIncomingIncidentRemedy()
    {
        try {
            $inc = new Incident();
            $content = request()->get('content');
            
            $inc->parseContentIncidentOd($content);
            $inbox = $this->inbox_ds->addIncByFirstLayer($inc, request()->get('requestId') ? request()->get('requestId') : null);
            $inc->parseInboxOd($inbox);
            $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($inc->service);
            $wsdl = $this->inccreate_wsdl->createIncidentGenericOd($inc, $wsdl_reconID->getContent());
            $request = $this->inbox_ds->refresh($inbox);
            $inc->parseInboxOd($request);
            if ($wsdl->getResponseStatus() == 'success') {
                    $findWorkinfoId = $this->workinfolog_ds->findWorkinfobyId($request->id);
                    if($findWorkinfoId !== null){
                        $getFile = Storage::disk('public')->get($request->id . "/" . $request->id . "_" . $findWorkinfoId->id.".".$findWorkinfoId->file_ext);
                        if($this->uploadFiletoRemedy($getFile, $findWorkinfoId) == true){
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        }else{
                            $this->inbox_ds->update($findWorkinfoId->id, array("ActionTicketCreation" => "IN_PENDING_UPL"));
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created, but failed to upload file, Please try again in Pending Ticket !";
                        }
                    }else{

                        $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        $this->inbox_ds->update($request->id, array("RemedyTicketId" => $wsdl->getContent()));
                    }
                $request['incident'] = $inc->parseInbox($request);
                return response()->json(['status' => true, 'incident_number'=>$wsdl->getContent(), 'responsedata' => $request,'inboxId'=>$inbox->id,'message'=> $wsdl->getResponseMessage(),'inc'=>$inc,'inbox'=>$inbox]);
            } else {
                $message = "Opps.. Error captured, unable to create Incident Number, please retry from Pending Ticket Menu immediately! " . $wsdl->getResponseMessage();
                $this->updateRequestDueException($inbox->id, $inbox->ProcessStatus, "IN_PENDING_PRS", Auth::user()->id, $inbox->AssigneeId);
                return response()->json(['status' => false, 'responsedata' => $request,'inboxId'=>$inbox->id,'message'=> $wsdl->getResponseMessage(),'inc'=>$inc,'inbox'=>$inbox]);

            }
        // $odIncident = $this->od_incident->requestListIncident('IN',$inbox->id);
            
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed ! Error : " . $ex,'inc'=>$inc,'inbox'=>$inbox]);
        }
    }


    public function createIncomingIncidentOd()
    {
        try {
        
            $inc = new Incident();
            $content = request()->get('content');
            $inc->parseContentIncidentOd($content);
            $inbox = $this->inbox_ds->addIncByFirstLayer($inc, request()->get('requestId') ? request()->get('requestId') : null);
            $inc->parseInboxOd($inbox);
            //$wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($inc->service);
            //$wsdl = $this->inccreate_wsdl->createIncidentGenericOd($inc, $wsdl_reconID->getContent());
            $request = $this->inbox_ds->refresh($inbox);
            $inc->parseInboxOd($request);
            /*
            if ($wsdl->getResponseStatus() == 'success') {
                    $findWorkinfoId = $this->workinfolog_ds->findWorkinfobyId($request->id);
                    if($findWorkinfoId !== null){
                        $getFile = Storage::disk('public')->get($request->id . "/" . $request->id . "_" . $findWorkinfoId->id.".".$findWorkinfoId->file_ext);
                        if($this->uploadFiletoRemedy($getFile, $findWorkinfoId) == true){
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        }else{
                            $this->inbox_ds->update($findWorkinfoId->id, array("ActionTicketCreation" => "IN_PENDING_UPL"));
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created, but failed to upload file, Please try again in Pending Ticket !";
                        }
                    }else{

                        $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        $this->inbox_ds->update($request->id, array("RemedyTicketId" => $wsdl->getContent()));
                    }
                $request['incident'] = $inc->parseInbox($request);
            } else {
                $message = "Opps.. Error captured, unable to create Incident Number, please retry from Pending Ticket Menu immediately! " . $wsdl->getResponseMessage();
                $this->updateRequestDueException($inbox->id, $inbox->ProcessStatus, "IN_PENDING_PRS", Auth::user()->id, $inbox->AssigneeId);
            }*/
        // $odIncident = $this->od_incident->requestListIncident('IN',$inbox->id);
            return response()->json(['status' => true, 'responsedata' => $request,'inboxId'=>$inbox->id]);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed ! Error : " . $ex]);
        }
    }

    /* Add by 1991725227 - putut.mukti dashboard OD 07092020*/
    public function updateIncidentOd(){

        $inc = new Incident();
        $content = request()->get('status');
        // $inc->updateResolution(request()->get('resolution'), $inc->solusi .' - '.$inc->updateEkskalasi);
        $inc->parseContentIncidentOd($content);
      //  $inc->resolution = $inc->resolution.' - '.$inc->updateEkskalasi;
        $request = $this->inbox_ds->updateIncOd(request()->get('id'), $inc);
        $inc->parseInboxOd($request);
        $inc->impact = (array_key_exists("impactValue", $content) ? $content["impactValue"] : null);
        $inc->summary_description = (array_key_exists("incident", $content) ? $content["incident"] : null);
        $inc->assignee_name = (array_key_exists("requesterByRemedyUser", $content) ? $content["requesterByRemedyUser"]["full_name"] : null);
        $inc->assignee_login_id = (array_key_exists("requesterByRemedyUser", $content) ? $content["requesterByRemedyUser"]["login_id"] : null);
        
       
        $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($inc);
      //  var_dump($wsdl);
      //  exit();
        if ($wsdl->getResponseStatus() == 'success') {
            $request = $this->inbox_ds->update(request()->get('id'), array("TextDecoded" => $inc->text_decoded));
            $message = "Ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . " has been marked as " . $request->ProcessStatus;
            $this->notif_ds->performFeedBackNotifierINC($request);
            return response()->json(["notes" => $inc->notes_description, "status" => true, "incident" => $inc, "request"=>$request, "wsdl_data" =>$wsdl->getArrContent(),"message"=>$message]);
        } else {
            $request = $this->inbox_ds->update(request()->get('id'), array("TextDecoded" => $inc->text_decoded, "ActionTicketCreation" => 'IN_PENDING_CLS'));
            $message = "Failed to change status ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . ", please retry process from Pending Ticket menu";
            return response()->json(["notes" => $inc->notes_description, "status" => false, "incident" => $inc, "request"=>$request, "wsdl_data" =>$wsdl->getArrContent(),"message"=>$message]);
        }
        // return response()->json(["notes" => $inc->notes_description, "status" => true,"inc"=>$inc, "request"=>$request]);
    
    }

    public function oddashboard(String $inboxId,String $oddashboard){
        $odIncident = $this->od_incident->requestListIncident('IN',(int) $inboxId,$oddashboard);
        return $odIncident;
    }

    public function searchIncidentByOd(){
        $paramSearchIncident = request()->get('paramSearch');
        $searchIncidentByOd = $this->od_incident->searchIncidentByOd($paramSearchIncident['field'], $paramSearchIncident['isDateFrom'], $paramSearchIncident['isDateTo'], $paramSearchIncident['textSearch']);
        return $searchIncidentByOd;
    }

    public function getCustomerProfileSecondLayer()
    {
        $result = EmployeeRemedy::where('company', request()->get('company'))
        ->where(function($q) {
            $q->where('full_name', 'ILIKE', '%' . request()->get('inputname') . '%')
              ->orWhere('nip', 'ILIKE', '%' . request()->get('inputname') . '%')
              ->orWhere('id', 'ILIKE', '%' . request()->get('inputname') . '%');
        })->where('profile_status','1')->get();
        return response()->json($result);
    }

    public function sendMessageIncidentByOd(){	
            
            $messageIncidentOd = request()->get('messageIncidentOd');
			$data_dev01="data_dashboard_od";			//${$data_dev01}
			$json_dev01="json_dashboard_od";			//${$json_dev01}
			$url_dev01="url_dashboard_od";			//${$url_dev01}
			$options_dev01="options_dashboard_od";	//${$options_dev01}
			$options_dev01_kosong="optionskosong_dashboard_od";			//${$options_dev01_kosong}
			$cek_api="cekapi_dashboard_od";			//${$cek_api}
			$result_dev01="result_dashboard_od";		//${$result_dev01}
			echo "message: ".$messageIncidentOd;
			///
			${$data_dev01} = [
				'body'  => $messageIncidentOd, 
				'phone' => '6282341470442', // 
			];

			${$json_dev01} = json_encode(${$data_dev01}); // Encode data to JSON
			//${$url_dev01}='https://eu25.chat-api.com/instance12056/message?token=jhs1rmabm6uyhtrc';
			${$url_dev01}='https://eu25.chat-api.com/instance12056/sendMessage?token=jhs1rmabm6uyhtrc';
			
			${$options_dev01} = stream_context_create(['http' => [
					'method'  => 'POST',
					'header'  => 'Content-type: application/json',
					'content' => ${$json_dev01}
				],
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			]);
			
			${$options_dev01_kosong} = stream_context_create(['http' => [
					'method'  => 'POST',
					'header'  => 'Content-type: application/json'
				],
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			]);
			
			${$cek_api}=file_get_contents(${$url_dev01}, false, ${$options_dev01_kosong});

				if(${$cek_api})	
				{	${$result_dev01} = file_get_contents(${$url_dev01}, false, ${$options_dev01}); echo "Berhasil nih";		}
				else
				{	//sleep(10);
			
					${$cek_api}=file_get_contents(${$url_dev01}, false, ${$options_dev01_kosong});
			
					if(${$cek_api})	
					{	${$result_dev01} = file_get_contents(${$url_dev01}, false, ${$options_dev01});
						echo "Berhasil Kirim ";	}
					else
					{	echo "Gagal Kirim ";	}
				
					echo "Gagal nih";
				}
			//echo ${$url_dev01};
    }

    public function showLoginForm(){

        return View::make('onduty/od_login');
    }

    public function processLogin(){
        echo "test login";
    }


}