<?php

namespace App\ServiceHandler\BDSLayer;

use App\Helpers\DatabaseConnection;
use Illuminate\Support\Facades\Session;
use App\BDSServer;
use Illuminate\Support\Facades\DB;

class BDSServerImp implements BDSServerService
{

    public function listAll()
    {
        return BDSServer::all();
    }

    public function listAllByPagination()
    {
        $results = BDSServer::select('id', 'ip_server', 'kode_cabang', 'nama_cabang')->orderBy('kode_cabang', 'asc')->paginate(20);
        return response()->json([
            'statusConnection' => Session::get('data.status'),
            'result' => $results
        ]);
    }

    public function findByKodeCabang($search)
    {

        $results = BDSServer::select('id', 'kode_cabang', 'ip_server', 'nama_cabang', DB::raw("kode_cabang || ' - ' || ip_server || ' - ' || nama_cabang as combine"))->where('kode_cabang', 'ilike', '%' . $search . '%')
            ->orWhere('nama_cabang', 'ilike', '%' . $search . '%')
            ->orWhere('ip_server', 'ilike', '%' . $search . '%')->get();
        // $results = BDSServer::where('kode_cabang', 'ilike', '%' . $search . '%')->get();
        return $results;
    }

    public function findByIpServer($ipServer)
    {
        $results = BDSServer::where('ip_server', $ipServer)->first();
        return $results;
    }

    public function dataSelected($id)
    {
        $results = BDSServer::where('id', $id)->first();
        if ($results != null) {
            return response()->json([
                'status' => true,
                'result' => $results
            ]);
        } else {
            return response()->json([
                'status' => false,
                'result' => "Data source field with name cannot be found"
            ]);
        }
    }

    public function findData($search)
    {
        $BDSServer = BDSServer::where('kode_cabang', 'ilike', '%' . $search . '%')
            ->orWhere('nama_cabang', 'ilike', '%' . $search . '%')
            ->orWhere('ip_server', 'ilike', '%' . $search . '%')->orderBy('kode_cabang', 'asc')->paginate(20);

        return response()->json(
            [
                'statusConnection' => Session::get('data.status'),
                'result' => $BDSServer,
                'serverConf' => Session::get('data')
            ]
        );
    }

    public function setConnection($ipServer, $kodeCabang, $namaCabang, $username = '', $password = '')
    {
        $message = "Connect to IP Server : " . $ipServer . " - " . $kodeCabang . " success, please wait page to reload.";
        try {
            $connection = DatabaseConnection::Connection($ipServer, $kodeCabang);
            if ($connection) {
                $data = collect(
                    array(
                        'ipServer' => $ipServer,
                        'database' => $kodeCabang,
                        'namaCabang' => $namaCabang,
                        'status' => true,
                    )
                );
                session()->put('data', $data);
                session()->save();
                return response()->json(['status' => true, 'result' => $message]);
            }
        } catch (\Exception $ex) {
            $reconnect = DatabaseConnection::Reconnect($ipServer, $kodeCabang);
            if ($reconnect) {
                $data = collect(
                    array(
                        'ipServer' => $ipServer,
                        'database' => $kodeCabang,
                        'namaCabang' => $namaCabang,
                        'status' => true,
                    )
                );
                session()->put('data', $data);
                session()->save();
                return response()->json(['status' => true, 'result' => $message]);
            } else {
                return response()->json(['status' => false, 'message' => "Fatal Error : " . $ex]);
            }
        }
    }

    public function setDisonnect($ipServer, $database)
    {
        $connection = DatabaseConnection::Disconnection();
        session()->forget('data');
        session()->save();

        if (!$connection) {
            return response()->json([
                'status' => true,
                'result' => "Disconnect to IP Server : " . $ipServer . " - " . $database . " success, please wait page to reload",
            ]);
        } else {
            return response()->json([
                'status' => false,
                'result' => "Disconnect failed",
            ]);
        }
    }

    public function initBdsConnection()
    {
        try {
            return DatabaseConnection::Connection(Session::get('data.ipServer'), Session::get('data.database'));
        } catch (\Exception $ex) {
            return DatabaseConnection::Reconnect(Session::get('data.ipServer'), Session::get('data.database'));
        }
    }

    public function submitUpdated($id, $dataUpdate)
    {
        BDSServer::where('id', $id)->update($dataUpdate);
    }

    public function submitDeleted($id)
    {
        BDSServer::where('id', $id)->delete();
    }

    public function submitInsert($dataUpdate)
    {
        BDSServer::insert($dataUpdate);
    }
}