<?php

namespace App\Http\Controllers;

use App\AppConfig;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\IntegrationLayer\WOICTMRemedyService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderUpdateService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentService;
use App\ServiceHandler\IntegrationLayer\CHGInfraChangesService;
use App\ServiceHandler\DataLayer\InboxService;
use App\EmployeeRemedy;
use App\HostMasterServer;
use App\Incident;
use App\MaJobs;
use App\ServiceHandler\BDSLayer\BDSServerService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use App\ServiceHandler\PBMLayer\Bulk\IncidentProblemService;
use App\ServiceHandler\PBMLayer\ResponseTime\TbluApdyService;
use App\Workorder;
use App\SupportGroupAssigneeRemedy;
use GuzzleHttp\Client;

class ItoolsAPIController extends BaseController
{
    protected $remedy_user_service;
    protected $wocreate_wsdl;
    protected $woupdate_wsdl;
    protected $in_wsdl_service;
    protected $infrachanges_int;
    protected $inbox_ds;
    protected $bds_service;
    protected $incupdate_wsdl;
    protected $tblu_apdy_repo;
    protected $incprb_wsdl;

    public function __construct(
        InboxService $inbox_service,
        CHGInfraChangesService $infra_changes_service,
        WOICTMRemedyService $remedy_user_service,
        WOIWorkOrderService $workorder_create_service,
        WOIWorkOrderUpdateService $workorder_update_service,
        WOIIncidentService $in_service,
        WOIIncidentUpdateService $incident_update_services,
        BDSServerService $service_bds,
        TbluApdyService $tblu_apdy_service,
        IncidentProblemService $incprb_service
    ) {
        $this->inbox_ds = $inbox_service;
        $this->infrachanges_int = $infra_changes_service;
        $this->remedy_user_service = $remedy_user_service;
        $this->wocreate_wsdl = $workorder_create_service;
        $this->woupdate_wsdl = $workorder_update_service;
        $this->in_wsdl_service = $in_service;
        $this->incupdate_wsdl = $incident_update_services;
        $this->bds_service = $service_bds;
        $this->incupdate_wsdl = $incident_update_services;
        $this->tblu_apdy_repo = $tblu_apdy_service;
        $this->incprb_wsdl = $incprb_service;
    }

    public function fetchRemedyUser()
    {
        $remedy_username =  request()->get('username');
        $remedy_password = request()->get('password');
        $remedy_person_id = request()->get('personId');
        $wsdl_result = $this->remedy_user_service->getPeopleEntityByPersonId($remedy_username, $remedy_password, $remedy_person_id);
        if ($wsdl_result->getResponseStatus() == 'success') {
            $employee = EmployeeRemedy::find($wsdl_result->getArrContent()['PersonId']);
            if ($employee) {
                $employee->id = $wsdl_result->getArrContent()['PersonId'];
                // $employee->nip = $wsdl_result->getArrContent()['NIP'];
                $employee->full_name = $wsdl_result->getArrContent()['FullName'];
                $employee->first_name = $wsdl_result->getArrContent()['FirstName'];
                $employee->last_name = $wsdl_result->getArrContent()['LastName'];
                $employee->profile_status = $wsdl_result->getArrContent()['ProfileStatus'];
                $employee->company = $wsdl_result->getArrContent()['Company'];
                $employee->organization = $wsdl_result->getArrContent()['Organization'];
                $employee->site_country = $wsdl_result->getArrContent()['SiteCountry'];
                $employee->site_state = $wsdl_result->getArrContent()['SiteState'];
                $employee->site_city = $wsdl_result->getArrContent()['SiteCity'];
                // $employee->email=$wsdl_result->getArrContent()['Email'];
                $employee->region_name = $wsdl_result->getArrContent()['Region'];
                $employee->group_name = $wsdl_result->getArrContent()['Group'];
                $employee->department_name = $wsdl_result->getArrContent()['Department'];
                // $employee->login_id = $remedy_username;
                // $employee->password = encrypt($remedy_password);
                $employee->itsrm_flag = true;
                $employee->save();
                return response()->json(["status" => true, 'message' => 'user updated']);
            } else {
                $remedy_user = new EmployeeRemedy();
                $remedy_user->id = $wsdl_result->getArrContent()['PersonId'];
                // $remedy_user->nip = $wsdl_result->getArrContent()['NIP'];
                $remedy_user->full_name = $wsdl_result->getArrContent()['FullName'];
                $remedy_user->first_name = $wsdl_result->getArrContent()['FirstName'];
                $remedy_user->last_name = $wsdl_result->getArrContent()['LastName'];
                $remedy_user->profile_status = $wsdl_result->getArrContent()['ProfileStatus'];
                $remedy_user->company = $wsdl_result->getArrContent()['Company'];
                $remedy_user->organization = $wsdl_result->getArrContent()['Organization'];
                $remedy_user->site_country = $wsdl_result->getArrContent()['SiteCountry'];
                $remedy_user->site_state = $wsdl_result->getArrContent()['SiteState'];
                $remedy_user->site_city = $wsdl_result->getArrContent()['SiteCity'];
                // $remedy_user->email=$wsdl_result->getArrContent()['Email'];
                $remedy_user->region_name = $wsdl_result->getArrContent()['Region'];
                $remedy_user->group_name = $wsdl_result->getArrContent()['Group'];
                $remedy_user->department_name = $wsdl_result->getArrContent()['Department'];
                // $remedy_user->login_id = $remedy_username;
                // $remedy_user->password = encrypt($remedy_password);
                $remedy_user->itsrm_flag = true;
                $remedy_user->save();
                return response()->json(["status" => true, 'message' => 'user created']);
            }
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage()]);
        }
    }

    public function fetchSGP()
    {
        $remedy_username =  request()->get('username');
        $remedy_password = request()->get('password');
        $person_id =  request()->get('PersonId');
        $support_group_id = request()->get('SupportGroupId');
        $wsdl_result = $this->remedy_user_service->getSupportGroupPeopleAssociationByPersonId($remedy_username, $remedy_password, $person_id, $support_group_id);
        if ($wsdl_result->getResponseStatus() == 'success') {
            $support_group = SupportGroupAssigneeRemedy::where([['employee_remedy_id', $wsdl_result->getArrContent()['EmployeeRemedyId']], ['support_group_id', $wsdl_result->getArrContent()['SupportGroupId']]])->first();
            if ($support_group) {
                $support_group->request_id = $wsdl_result->getArrContent()['RequestId'];
                $support_group->remedy_login_id = $wsdl_result->getArrContent()['RemedyLoginId'];
                $support_group->first_name = $wsdl_result->getArrContent()['FirstName'];
                $support_group->last_name = $wsdl_result->getArrContent()['LastName'];
                $support_group->full_name = $wsdl_result->getArrContent()['FullName'];
                $support_group->company = $wsdl_result->getArrContent()['Company'];
                $support_group->organization = $wsdl_result->getArrContent()['Organization'];
                $support_group->support_group_name = $wsdl_result->getArrContent()['SupportGroupName'];
                $support_group->support_group_id = $wsdl_result->getArrContent()['SupportGroupId'];
                $support_group->employee_remedy_id = $wsdl_result->getArrContent()['EmployeeRemedyId'];
                $support_group->save();
                return response()->json(["status" => true, 'message' => 'user updated']);
            } else {
                $support_group_new = new SupportGroupAssigneeRemedy();
                $support_group_new->request_id = $wsdl_result->getArrContent()['RequestId'];
                $support_group_new->remedy_login_id = $wsdl_result->getArrContent()['RemedyLoginId'];
                $support_group_new->first_name = $wsdl_result->getArrContent()['FirstName'];
                $support_group_new->last_name = $wsdl_result->getArrContent()['LastName'];
                $support_group_new->full_name = $wsdl_result->getArrContent()['FullName'];
                $support_group_new->company = $wsdl_result->getArrContent()['Company'];
                $support_group_new->organization = $wsdl_result->getArrContent()['Organization'];
                $support_group_new->support_group_name = $wsdl_result->getArrContent()['SupportGroupName'];
                $support_group_new->support_group_id = $wsdl_result->getArrContent()['SupportGroupId'];
                $support_group_new->employee_remedy_id = $wsdl_result->getArrContent()['EmployeeRemedyId'];
                $support_group_new->save();
                return response()->json(["status" => true, 'message' => 'user created']);
            }
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage()]);
        }
    }

    public function fetchSGPbyLoginId()
    {
        $remedy_username =  request()->get('username');
        $remedy_password = request()->get('password');
        $login_id =  request()->get('LoginId');
        $support_group_id = request()->get('SupportGroupId');
        // return response()->json([$remedy_username,$remedy_password,$login_id,$support_group_id]);
        $wsdl_result = $this->remedy_user_service->getSupportGroupPeopleAssociationByLoginId($remedy_username, $remedy_password, $login_id, $support_group_id);
        if ($wsdl_result->getResponseStatus() == 'success') {
            $support_group = SupportGroupAssigneeRemedy::where([['employee_remedy_id', $wsdl_result->getArrContent()['EmployeeRemedyId']], ['support_group_id', $wsdl_result->getArrContent()['SupportGroupId']]])->first();
            if ($support_group) {
                $support_group->request_id = $wsdl_result->getArrContent()['RequestId'];
                $support_group->remedy_login_id = $wsdl_result->getArrContent()['RemedyLoginId'];
                $support_group->first_name = $wsdl_result->getArrContent()['FirstName'];
                $support_group->last_name = $wsdl_result->getArrContent()['LastName'];
                $support_group->full_name = $wsdl_result->getArrContent()['FullName'];
                $support_group->company = $wsdl_result->getArrContent()['Company'];
                $support_group->organization = $wsdl_result->getArrContent()['Organization'];
                $support_group->support_group_name = $wsdl_result->getArrContent()['SupportGroupName'];
                $support_group->support_group_id = $wsdl_result->getArrContent()['SupportGroupId'];
                $support_group->employee_remedy_id = $wsdl_result->getArrContent()['EmployeeRemedyId'];
                $support_group->save();
                return response()->json(["status" => true, 'message' => 'user updated']);
            } else {
                $support_group_new = new SupportGroupAssigneeRemedy();
                $support_group_new->request_id = $wsdl_result->getArrContent()['RequestId'];
                $support_group_new->remedy_login_id = $wsdl_result->getArrContent()['RemedyLoginId'];
                $support_group_new->first_name = $wsdl_result->getArrContent()['FirstName'];
                $support_group_new->last_name = $wsdl_result->getArrContent()['LastName'];
                $support_group_new->full_name = $wsdl_result->getArrContent()['FullName'];
                $support_group_new->company = $wsdl_result->getArrContent()['Company'];
                $support_group_new->organization = $wsdl_result->getArrContent()['Organization'];
                $support_group_new->support_group_name = $wsdl_result->getArrContent()['SupportGroupName'];
                $support_group_new->support_group_id = $wsdl_result->getArrContent()['SupportGroupId'];
                $support_group_new->employee_remedy_id = $wsdl_result->getArrContent()['EmployeeRemedyId'];
                $support_group_new->save();
                return response()->json(["status" => true, 'message' => 'user created']);
            }
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage()]);
        }
    }

    public function createRemedyUser()
    {
        $username = request()->get('username');
        $password = request()->get('password');
        $remedy_login_id_or_nip = request()->get('usernameNip');
        $wsdl_result = $this->remedy_user_service->getPeopleEntityByLoginId($username, $password, $remedy_login_id_or_nip);
        if ($wsdl_result->getResponseStatus() == 'success') {
            $employee = EmployeeRemedy::find($wsdl_result->getArrContent()['PersonId']);
            if ($employee) {
                $employee->id = $wsdl_result->getArrContent()['PersonId'];
                // $employee->nip = $wsdl_result->getArrContent()['NIP'];
                $employee->full_name = $wsdl_result->getArrContent()['FullName'];
                $employee->first_name = $wsdl_result->getArrContent()['FirstName'];
                $employee->last_name = $wsdl_result->getArrContent()['LastName'];
                $employee->profile_status = $wsdl_result->getArrContent()['ProfileStatus'];
                $employee->company = $wsdl_result->getArrContent()['Company'];
                $employee->organization = $wsdl_result->getArrContent()['Organization'];
                $employee->site_country = $wsdl_result->getArrContent()['SiteCountry'];
                $employee->site_state = $wsdl_result->getArrContent()['SiteState'];
                $employee->site_city = $wsdl_result->getArrContent()['SiteCity'];
                // $employee->email=$wsdl_result->getArrContent()['Email'];
                $employee->region_name = $wsdl_result->getArrContent()['Region'];
                $employee->group_name = $wsdl_result->getArrContent()['Group'];
                $employee->department_name = $wsdl_result->getArrContent()['Department'];
                $employee->login_id = $remedy_login_id_or_nip;
                $employee->telegram_flag = true;
                $employee->save();
                return response()->json(["status" => true, 'message' => 'user updated']);
            } else {
                $remedy_user = new EmployeeRemedy();
                $remedy_user->id = $wsdl_result->getArrContent()['PersonId'];
                // $remedy_user->nip = $wsdl_result->getArrContent()['NIP'];
                $remedy_user->full_name = $wsdl_result->getArrContent()['FullName'];
                $remedy_user->first_name = $wsdl_result->getArrContent()['FirstName'];
                $remedy_user->last_name = $wsdl_result->getArrContent()['LastName'];
                $remedy_user->profile_status = $wsdl_result->getArrContent()['ProfileStatus'];
                $remedy_user->company = $wsdl_result->getArrContent()['Company'];
                $remedy_user->organization = $wsdl_result->getArrContent()['Organization'];
                $remedy_user->site_country = $wsdl_result->getArrContent()['SiteCountry'];
                $remedy_user->site_state = $wsdl_result->getArrContent()['SiteState'];
                $remedy_user->site_city = $wsdl_result->getArrContent()['SiteCity'];
                // $remedy_user->email=$wsdl_result->getArrContent()['Email'];
                $remedy_user->region_name = $wsdl_result->getArrContent()['Region'];
                $remedy_user->group_name = $wsdl_result->getArrContent()['Group'];
                $remedy_user->department_name = $wsdl_result->getArrContent()['Department'];
                $remedy_user->login_id = $remedy_login_id_or_nip;
                $remedy_user->telegram_flag = true;
                $remedy_user->save();
                return response()->json(["status" => true, 'message' => 'user created']);
            }
        } else {
            $wsdl_result = $this->remedy_user_service->getPeopleEntityByCorporateID($username, $password, $remedy_login_id_or_nip);
            if ($wsdl_result->getResponseStatus() == 'success') {
                $employee = EmployeeRemedy::find($wsdl_result->getArrContent()['PersonId']);
                if ($employee) {
                    $employee->id = $wsdl_result->getArrContent()['PersonId'];
                    // $employee->nip = $wsdl_result->getArrContent()['NIP'];
                    $employee->full_name = $wsdl_result->getArrContent()['FullName'];
                    $employee->first_name = $wsdl_result->getArrContent()['FirstName'];
                    $employee->last_name = $wsdl_result->getArrContent()['LastName'];
                    $employee->profile_status = $wsdl_result->getArrContent()['ProfileStatus'];
                    $employee->company = $wsdl_result->getArrContent()['Company'];
                    $employee->organization = $wsdl_result->getArrContent()['Organization'];
                    $employee->site_country = $wsdl_result->getArrContent()['SiteCountry'];
                    $employee->site_state = $wsdl_result->getArrContent()['SiteState'];
                    $employee->site_city = $wsdl_result->getArrContent()['SiteCity'];
                    // $employee->email=$wsdl_result->getArrContent()['Email'];
                    $employee->region_name = $wsdl_result->getArrContent()['Region'];
                    $employee->group_name = $wsdl_result->getArrContent()['Group'];
                    $employee->department_name = $wsdl_result->getArrContent()['Department'];
                    $employee->login_id = $remedy_login_id_or_nip;
                    $employee->telegram_flag = true;
                    $employee->save();
                    return response()->json(["status" => true, 'message' => 'user updated']);
                } else {
                    $remedy_user = new EmployeeRemedy();
                    $remedy_user->id = $wsdl_result->getArrContent()['PersonId'];
                    // $remedy_user->nip = $wsdl_result->getArrContent()['NIP'];
                    $remedy_user->full_name = $wsdl_result->getArrContent()['FullName'];
                    $remedy_user->first_name = $wsdl_result->getArrContent()['FirstName'];
                    $remedy_user->last_name = $wsdl_result->getArrContent()['LastName'];
                    $remedy_user->profile_status = $wsdl_result->getArrContent()['ProfileStatus'];
                    $remedy_user->company = $wsdl_result->getArrContent()['Company'];
                    $remedy_user->organization = $wsdl_result->getArrContent()['Organization'];
                    $remedy_user->site_country = $wsdl_result->getArrContent()['SiteCountry'];
                    $remedy_user->site_state = $wsdl_result->getArrContent()['SiteState'];
                    $remedy_user->site_city = $wsdl_result->getArrContent()['SiteCity'];
                    // $remedy_user->email=$wsdl_result->getArrContent()['Email'];
                    $remedy_user->region_name = $wsdl_result->getArrContent()['Region'];
                    $remedy_user->group_name = $wsdl_result->getArrContent()['Group'];
                    $remedy_user->department_name = $wsdl_result->getArrContent()['Department'];
                    $remedy_user->login_id = $remedy_login_id_or_nip;
                    $remedy_user->telegram_flag = true;
                    $remedy_user->save();
                    return response()->json(["status" => true, 'message' => 'user created']);
                }
            } else {
                return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage()]);
            }
        }
    }

    public function createRemedyWO()
    {
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $request = request()->get('content');

        $wsdl_result = $this->wocreate_wsdl->createWorkOrderByEntity($remedy_login_id, $remedy_password, $request);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["status" => $wsdl_result->getResponseStatus(), "message" => $wsdl_result->getResponseMessage()]);
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage()]);
        }
    }

    public function updateRemedyWO()
    {
        $wo = new Workorder();
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $request = $this->inbox_ds->find(request()->get('id'));
        $wo->parseInbox($request);
        $wsdl_result = $this->woupdate_wsdl->updateWOGeneric($wo, $remedy_login_id, $remedy_password);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["status" => $wsdl_result->getResponseStatus(), "message" => $wsdl_result->getResponseMessage()]);
        } else {
            return response()->json(["status" => 'failed', 'message' => $wsdl_result->getResponseMessage()]);
        }
    }

    public function createRemedyIN()
    {
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $request = request()->get('content');
        $wsdl_result = $this->in_wsdl_service->createIncidentByEntity($remedy_login_id, $remedy_password, $request);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["status" => $wsdl_result->getResponseStatus(), "message" => $wsdl_result->getResponseMessage(), "incident_number" => $wsdl_result->getContent(), "wsdl_request" => $wsdl_result->getArrContent()]);
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage(), "wsdl_request" => $wsdl_result->getArrContent()]);
        }
    }

    public function uploadFile()
    {
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $request = request()->get('content');
        $wsdl_result = $this->incupdate_wsdl->addWorkInfoLogInc($request, $remedy_login_id, $remedy_password);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["status" => $wsdl_result->getResponseStatus(), "message" => $wsdl_result->getResponseMessage(), "work_info_id" => $wsdl_result->getContent(), "wsdl_request" => $wsdl_result->getArrContent()]);
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage(), "wsdl_request" => $wsdl_result->getArrContent()]);
        }
    }

    public function listChangesInfra()
    {
        $username = request()->get('username');
        $password = request()->get('password');
        $star_date = request()->get('startDate');
        $end_date = request()->get('endDate');
        $wsdl = $this->infrachanges_int->getInfraChangesList($username, $password, $star_date, $end_date);
        return response()->json($wsdl);
    }

    public function updateRemedyIN()
    {
        $inc = new Incident();
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $request_id = $this->inbox_ds->findByInc(request()->get('remedy_ticket_id'));
        $request = $this->inbox_ds->update($request_id->id, array("ProcessStatus" => $inc->generateStatus(request()->get('status'))));
        $inc->parseInbox($request);
        $inc->updateResolution(request()->get('resolution'), $inc->notes_description);
        $wsdl_result = $this->incupdate_wsdl->updateIncidentApi($inc, $remedy_login_id, $remedy_password);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["status" => $wsdl_result->getResponseStatus(), "message" => $wsdl_result->getResponseMessage(), "remedy_ticket_id" => $wsdl_result->getContent(), "wsdl_request" => $wsdl_result->getArrContent()]);
        } else {
            return response()->json(["status" => 'failed', 'message' => $wsdl_result->getResponseMessage(), "wsdl_request" => $wsdl_result->getArrContent()]);
        }
    }

    public function fetchRemedyIN()
    {
        $inc = new Incident();
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $incident_number = request()->get('incident_number');
        $wsdl_result = $this->incupdate_wsdl->fetchIncident($incident_number, $remedy_login_id, $remedy_password);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["response" => $wsdl_result->getContent(), "request" => $wsdl_result->getArrContent()]);
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage(), "wsdl_request" => $wsdl_result->getArrContent()]);
        }
    }

    public function createTbluApdy()
    {
        try {
            $payload = [
                'layanan' => request()->get('layanan'),
                'node' => request()->get('node'),
                'restime' => request()->get('restime'),
                'tanggal' => request()->get('tanggal'),
            ];
            $data = $this->tblu_apdy_repo->create($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function updateProblem()
    {
        $remedy_login_id = request()->get('username');
        $remedy_password = request()->get('password');
        $request = request()->get('content');
        $wsdl_result = $this->incprb_wsdl->updateProblemGeneric($request, $remedy_login_id, $remedy_password);
        if ($wsdl_result->getResponseStatus() == 'success') {
            return response()->json(["status" => $wsdl_result->getResponseStatus(), "message" => $wsdl_result->getResponseMessage(), "problem_number" => $wsdl_result->getContent(), "wsdl_request" => $wsdl_result->getArrContent()]);
        } else {
            return response()->json(["status" => false, 'message' => $wsdl_result->getResponseMessage(), "wsdl_request" => $wsdl_result->getArrContent()]);
        }
    }

    //Custom query
    public function insertToHostMasterServers()
    {
        try {
            $region1 = ['277','278','279','280','281','282','284','285','876','483','979','936','244','260','F50','199','F53','M70','P18','P19','452','295','J05','J06','J07','M75','M76','U33','U80','W95','X14','W09','W12','I78','314','315','316','317','318','319','320','321','322','324','325','841','904','937','938','939','H23','033','638','326','327','T25','P78','I71','I72','I73','K95','J79','J80','S38','M77','U34','X82','Y93','S99','I74','I75','I76','573','574','576','H32','T26','M51','V12','S96','U97','583','D73','D72','D71','D70','D69','D68','Q82','561','W56','K68','K70','K71','L46','L47','R11','S28','W81','W87','R83','504','F77','505','506','F79','H19','507','508','513','514','515','844','906','572','267','F87','B54','516','517','G30','034','C31','M36','T27','S98','A88','J40','K72','K73','L64','R91','S65','R79','R80','P97','U70','U71','U72','W91','W92','W93','U60','Y49','Y50','W02','Y12','747','Q56','648','F51','649','023','651','652','653','654','655','656','657','658','744','842','678','C95','172','269','035','M53','U74','L23','L24','672','A53','N92','Y34','R60','W29','P56','L78','D49','D50','D52','D55','V03','V05','X30','V09','D66','V01','D45','D54','D56','D60','D65','D58','D64','D61','D43','D48','D57','D51','D74','E01','E03','E08','G39','R93','X77','M86','E09','E10','B42','U63','M93','P58','Z46','E05','G38','R34','V02','X48','V10','V11','U44','U45','D53','D59','D63','D67','V04','V06','V08','X29','V07','P57','D46','D47'];
            for($i=0; $i < count($region1); $i++){
                $query = new HostMasterServer();
                $query->region_name = 'region1';
                $query->server_code = $region1[$i];
                $query->save();
            }

            $region2 = ['467', '468', '470', '684', 'B62', 'F88', 'M47', '472', '074', '735', 'T39', 'W61', 'W63', 'W64', 'W71', 'W82', 'J97', 'J13', 'J98', 'J99', 'K01', 'J96', 'L67', 'L73', 'L74', 'S66', 'S67', 'S68', 'S69', 'S70', 'R67', 'R96', 'R68', 'M90', 'W01', 'W25', 'W30', 'X24', 'X34', '539', '541', '542', '544', '545', '736', '824', '288', 'Z42', '667', '546', '547', 'J95', 'J14', 'J15', 'J34', 'J35', 'S63', 'S64', 'S71', 'R69', 'S72', 'R97', 'M78', 'M79', 'U32', 'M92', 'W31', 'V98', 'Y01', 'U66', 'U67', 'U68', 'U91', 'U93', '429', '430', '431', '433', '434', '435', '907', 'F54', 'F89', 'F90', 'F82', 'R04', 'S12', 'S13', 'S14', 'S15', 'S16', 'M49', 'L91', 'H68', '471', 'J91', 'J92', 'J93', 'J16', 'L68', 'L69', 'R81', 'R30', 'R48', 'M71', 'S24', 'U73', 'W32', 'W70', 'Y02', 'Y03', 'A81', 'A82', 'G09', 'G40', 'M06', '353', '357', '360', '361', '362', '853', 'B46', '209', 'B90', 'S17', 'S18', '364', 'S19', 'M48', 'L90', 'A51', 'AE6', 'L22', 'A80', 'A79', 'A42', 'J87', 'J90', 'L70', 'P91', 'P92', 'U69', 'X01', 'X02', 'X03', '616', '617', '619', '620', '621', '910', '623', '580', 'F21', 'L92', '356', '622', 'J17', 'J18', 'J19', 'K02', 'K03', 'L71', 'L72', 'L76', 'S51', 'S52', 'S53', 'S54', 'S55', 'S56', 'S57', 'U10', 'U14', 'U15', 'U16', 'U95', 'U96', 'S03', 'S04', 'S05', 'S08', 'S10', 'W94', 'AE8', 'S02', 'S07', 'S09', 'A67', 'A76', 'S92', 'S11', 'A71', 'A73', 'A77', 'A78', 'A62', 'A65', 'A70', 'A63', 'A72', 'A75', 'A64', 'A69', 'A74', 'A61', 'G59', '466', '469', '538', 'E74', '540', '940', 'J94', '428', '908', 'L75', '352', 'R49', '612', '613', '614', '615', 'J72', 'S01', 'A68', '465'];
            for($i=0; $i < count($region2); $i++){
                $query2 = new HostMasterServer();
                $query2->region_name = 'region2';
                $query2->server_code = $region2[$i];
                $query2->save();
            }

            $region3 = ['080', '088', '884', '687', 'B57', 'B12', 'E32', 'F29', 'G14', 'G77', 'G78', 'G79', 'G80', '096', 'G81', '013', '025', 'J21', '191', '689', 'B18', 'B19', 'B21', 'B82', '038', '198', 'E35', 'N63', 'N61', 'N62', 'N74', '159', 'F34', '400', 'P79', 'R02', '241', '243', '251', 'B56', 'B85', 'B87', 'B88', 'B89', 'H36', '253', 'F30', 'F31', 'F37', '254', 'G18', 'G19', 'N72', 'N73', 'G82', 'H42', 'G72', 'H69', 'K04', 'J27', 'U46', '109', '110', '116', '119', '121', '976', '696', '697', '698', 'B29', '122', 'C55', 'C56', 'C57', 'E37', 'F85', 'G83', 'G84', 'G85', 'G86', 'G87', 'G88', '530', 'N93', '765', '056', '066', '699', '700', 'B35', 'B37', 'E97', 'N17', 'G91', 'G92', 'F27', 'M62', 'J28', 'P71', 'C06', 'C07', 'C08', 'C14', 'C15', 'C18', '018', '118', 'N64', 'N65', 'N66', 'N67', 'N69', 'N70', 'N71', 'G93', 'G94', 'H09', 'W68', 'X10', 'Z41', 'Z89', 'R71', 'AE1', 'J01', 'J24', 'K06', 'L84', 'R14', 'S94', 'U51', 'Y65', 'Y67', 'T49', '920', 'N02', '031', 'M40', 'X07', 'U37', 'U54', 'W36', 'G05', 'P20', 'P24', 'P25', 'P26', 'P29', 'P37', 'P36', 'P43', '221', 'L15', 'S95', 'N44', 'N45', 'N46', 'N48', 'N50', 'N52', 'N53', 'N57', 'N59', '093', 'J20', 'Q78', 'N18', 'N19', 'N20', 'N07', 'N08', 'F28', 'M60', 'P28', 'P31', 'P38', 'N37', 'L14', 'L17', 'Y71', 'N54', 'N55', 'N23', 'N24', 'N25', 'N29', 'N31', 'N32', 'U87', 'G04', 'P21', 'P23', 'N40', 'N43', 'N49', 'N30', 'Z24', 'Z25', 'Z28', 'AC8', 'AE2', 'Z30', 'Z32', 'Z33', 'Z35', 'Z37', 'Z39', 'N05', 'N06', 'P30', 'P32', 'P44', 'L16', 'Y73', 'F68', 'N51', 'N56', 'N34', 'Z27', 'N03', 'N04', 'Y68', 'P35', 'F35', 'F36', 'W75', 'Y85', 'N41', 'N42', 'N47', 'N58', 'N21', 'N28', '987', 'Z19', 'Z22', 'Z26', 'Z49', 'Y76', 'Y78', 'A38', 'R16', 'P27', 'P17', '495', 'N22', 'N27', 'Z29', 'Z36', 'Y75', 'N26', '703', 'Z31', 'Z34', 'Y74', 'Y77', 'Z40', '529', 'Q80', '189', '242', '108', '114', '053', '057', '060', '857', '423', 'C05', 'X08', 'P22', 'AG5', 'P40', 'Z20', 'Z23', 'C01', 'N01'];
            for($i=0; $i < count($region3); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region3';
                $query3->server_code = $region3[$i];
                $query3->save();
            }

            $region4 = ['204','210','852','890','124','H43','H44','H45','095','169','P39','215','A93','Y30','127','128','129','C62','C63','C64','C65','C66','C67','C68','C69','G13','G95','G96','G97','R07','160','162','164','166','167','B26','B28','B95','B96','B97','F38','G21','G22','G23','175','G24','H47','H48','H49','H50','138','139','140','141','142','143','144','C58','C59','C60','C61','E42','G25','G89','G90','145','G99','H02','101','L04','R08','R19','P41','AD1','217','218','222','223','224','226','C70','C71','229','H13','H03','H04','H08','H37','H38','H39','H40','A83','AF7','AG9','039','040','042','855','892','C72','C73','C74','C75','C76','C77','C78','C79','C80','E43','H15','H11','H41','M33','X05','J32','W65','255','259','261','264','265','266','783','706','H53','H54','L77','Y31','T38','T36','C28','C32','C35','C37','C38','B60','111','134','C40','E46','E49','E50','G31','H46','H55','H56','M74','Z50','446','Q70','Q71','P65','J31','K58','L19','L20','P66','P67','T95','T96','T97','U64','Y35','Y36','Y37','P42','355','M42','N75','N76','N77','N78','N79','N80','N81','N82','N83','N84','N85','N86','N87','N88','N89','N91','AD9','L96','L10','U56','Y97','P02','P03','P04','P05','P06','P07','P09','P10','P13','P14','P16','N38','A94','L12','L13','P68','R17','K59','K60','200','201','351','126','G98','137','B31','F33','B32','N60','219','F76','047','292','H52','C27','N90','P11','P12','P15','125','R84','163','L45','C21','C39','P01','P08'];
            for($i=0; $i < count($region4); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region4';
                $query3->server_code = $region4[$i];
                $query3->save();
            }

            $region5 = ['146','148','150','151','153','154','155','H58','H59','H60','020','V73','027','102','103','104','107','840','B50','C86','P45','P46','P47','P48','024','V16','252','180','182','185','897','401','B51','G71','H61','H62','H63','188','H64','H65','H66','H67','062','M46','V17','A55','177','002','003','004','005','006','007','009','010','011','C81','893','B33','E64','F83','G74','H77','H78','H79','H80','H81','H82','H83','H84','237','531','P80','V18','Q74','U08','026','230','239','240','235','231','234','232','C85','F23','H70','H71','H72','733','V19','L88','072','073','075','076','077','079','766','B53','B61','C89','E66','H18','H73','H74','H75','H76','B84','238','S25','X06','R25','271','272','847','848','915','B64','E56','E57','E58','G28','G76','H87','H88','H89','H90','246','256','V20','V74','L63','K77','J37','K78','J38','L81','P61','R26','T93','U58','Y32','Y33','858','860','865','A43','M15','Q48','AA1','C44','C45','C46','C47','C48','C50','C52','135','E59','E60','H85','H86','L83','R27','V99','Y38','A48','N94','M18','156','157','944','228','K93','233','896','071','078','P60','C54','097','777','178','001','078','T20','270','C49','C41'];
            for($i=0; $i < count($region5); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region5';
                $query3->server_code = $region5[$i];
                $query3->save();
            }

            $region6 = ['367','368','369','370','371','373','374','911','376','912','941','942','943','B65','248','249','B93','106','M44','S50','K07','K08','K09','K10','S78','R72','R54','S45','S36','T83','W90','819','442','444','449','913','707','450','B66','B67','037','451','390','455','S32','U94','U23','K12','K13','L89','R55','R56','S80','392','395','396','397','845','399','708','946','136','262','817','402','Q52','T46','X12','M64','K85','K86','L27','L28','R74','S46','R58','S73','S83','404','405','406','407','408','767','849','916','947','245','247','M45','M99','S44','AH7','J02','J03','K81','K83','K84','L29','R12','S33','S81','M65','X35','X79','X80','X90','X91','T29','U86','U59','S41','V45','V46','V47','V48','V49','V50','V51','V52','F57','F58','445','Y19','V53','V54','V55','V57','V58','V59','V60','V61','V62','V63','V64','V65','V66','V67','V68','V69','V70','V71','V72','M30','X13','X28','X40','Y23','T47','R51','R92','Z51','Z52','Z53','Z54','Z55','Z56','Z57','Z58','Z59','Z61','Z62','Z63','Z64','Z65','Z66','Z67','Z68','Z69','Z70','Z71','Z72','Z73','Z74','Z75','Z76','Z77','Z78','Z79','Z80','Z81','Z82','Z83','Z84','Z85','Z86','Z87','Z88','M94','R52','Y84','836','D20','D21','D22','D23','D24','D25','D26','D27','D28','D29','D30','D31','D32','D33','D34','D35','D36','D38','D39','D40','D41','366','372','375','X11','441','968','389','393','945','263','U90','403','K82','V40','V43'];
            for($i=0; $i < count($region6); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region6';
                $query3->server_code = $region6[$i];
                $query3->save();
            }

            $region7 = ['379','380','388','917','F56','640','B76','K15','I66','I67','I68','308','A91','Z92','Y96','L54','L55','S48','S91','W72','U92','412','413','416','832','734','790','F55','B75','641','M21','M55','J78','A32','X47','I62','I63','I65','I64','K87','J74','J75','J43','J44','P72','P55','R76','M66','M68','Z94','Z96','B94','475','476','480','482','984','792','H27','F96','B24','063','732','484','M22','Z90','Y98','485','J76','J45','J77','K62','L56','R36','R37','R38','R39','R40','S84','W26','Z95','Q51','Q50','AA3','AA4','AA5','AE9','457','461','985','866','887','462','464','E91','F97','016','463','273','274','M69','048','788','I69','J04','K53','K54','K55','K56','L98','L99','M01','M16','P53','P54','P73','R24','R22','S49','S85','T85','M87','S88','S89','X33','Y07','Y08','Y09','AD4','487','494','986','E34','T28','M50','M56','M95','X18','X19','K64','K65','K66','K67','M02','M04','L31','M17','S31','R75','S35','S58','W03','X20','Q03','Q04','Q05','Q06','Q07','Q08','Q09','Q10','Q11','Q12','Q13','Q14','Q15','Q16','Q17','Q18','Q19','Q20','Q21','Q22','Q23','Q24','Q25','Q26','Q27','Q28','L57','I04','I05','I06','I07','I08','I09','I10','I11','I12','I13','I14','I15','I16','I17','I18','I19','I20','I21','I22','I23','I24','I25','I26','I27','I28','I29','I30','I31','I32','I33','I34','I35','I36','I37','I41','I42','I43','I44','I45','I46','I47','I48','I49','I50','I51','I52','I53','I54','I55','I56','I57','I58','I59','I60','I61','377','378','828','381','S39','409','411','473','474','477','478','479','481','290','791','AH5','486','456','458','459','460','B68','740','C53','488','491','492','919','Q01','I01','I03','I39','I38','793'];
            for($i=0; $i < count($region7); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region7';
                $query3->server_code = $region7[$i];
                $query3->save();
            }

            $region8 = ['298','299','300','303','304','310','301','313','E44','F32','V24','V25','V26','V83','V84','V85','V86','V88','V91','443','R61','A31','J70','W83','W84','W85','W86','J42','341','342','347','348','989','861','711','712','716','749','G32','N36','F84','M29','V27','V28','V29','V30','V31','V32','V33','V34','V35','V36','V37','V38','V39','V75','V76','R45','S37','M88','M89','U88','W05','X22','329','331','332','333','335','336','337','717','988','784','G64','008','M24','339','V79','V81','V82','V92','V93','V94','V96','S86','R43','Q72','K20','K21','L58','M20','S77','U62','W06','M72','Q60','Q61','587','588','589','590','591','991','992','E14','593','594','G60','P87','P88','P89','P90','M25','F67','028','AD2','929','L79','Y57','Y58','Y56','Y27','Y28','Y29','W73','U40','U05','U06','U07','S82','R23','W39','J53','J54','K25','J55','J56','K26','L07','L08','M07','M08','L32','L33','L34','L35','S59','R31','S60','S61','S62','S42','S43','R62','P62','P63','549','550','551','552','863','722','B69','B70','E15','E17','H26','556','G69','H10','065','275','769','A97','X27','R35','AA2','558','922','921','L80','E21','Y42','Y43','559','Y20','W40','W41','W42','U48','T90','L36','L37','M09','R44','P82','P83','P93','P94','T88','T89','T51','T52','T53','T54','T55','T56','T57','T58','T60','T62','T63','U41','A98','T82','Q49','Y13','G06','J47','J46','J39','E48','E47','Y40','Y21','T64','T65','T66','T68','T69','T70','T71','T73','T74','T75','T76','T77','T78','T79','T80','U27','U29','W43','W44','Y10','Y11','Y22','A01','A02','A03','A05','A06','A07','A08','A09','A10','A11','A12','A13','A14','A15','A16','A17','A18','A19','A20','A21','A22','A23','A24','A25','A26','A27','A28','A29','A30','Q46','X26','S76','Q58','297','306','P86','V87','V90','340','346','923','715','328','E90','330','334','V77','V77','V78','V80','V95','V97','585','586','592','M85','548','721','T50','T61','W76','T67','T72','U28','A04','F95','U04','T59'];
            for($i=0; $i < count($region8); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region8';
                $query3->server_code = $region8[$i];
                $query3->save();
            }

            $region9 = ['418','419','420','926','959','P33','424','F59','F86','425','426','427','993','W66','Y99','G67','J58','J59','J60','J61','K88','J62','M10','L60','R29','U30','U31','Y79','Y80','Z97','A95','597','598','599','925','995','032','G35','F64','F19','H06','A57','F16','AF4','601','602','L86','AH6','K89','K27','K28','K90','K29','K30','L61','L62','U77','U78','W45','W46','X87','X86','Y60','Y61','Y62','R06','T44','J33','564','566','567','851','927','571','996','G29','197','M38','T31','S79','D87','K31','K33','K34','K35','U35','U49','W47','W48','Y52','P96','Q81','499','500','E19','960','F61','U21','V14','501','502','503','X09','A89','D75','D78','D79','D80','D81','D82','D83','K36','K37','L38','R87','R88','U36','Y55','AD6','D84','D85','D86','E27','E28','E29','E30','F25','F63','069','149','A90','K91','J63','J64','L39','P76','M27','P77','U42','Y44','Y45','Y53','Y54','P50','P51','P52','A96','823','838','190','595','634','596','600','S23','562','563','569','M37','M11','497','498','D76','E26','J08'];
            for($i=0; $i < count($region9); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region9';
                $query3->server_code = $region9[$i];
                $query3->save();
            }

            $region10 = ['604','605','608','609','610','750','822','835','961','E20','E88','U26','V15','V21','K16','A54','L01','L02','L03','M12','P84','R33','M67','U17','W49','W27','Y24','Y48','Y81','L85','Y82','Y83','T91','M03','627','628','630','629','928','M26','302','W69','J65','R13','W20','W21','W22','W23','Y88','Y89','Y90','Y91','Y92','X04','AD8','523','532','956','957','536','966','752','161','H21','U01','U02','U03','T32','527','K19','W51','W07','W08','K39','J69','M14','X42','S97','F12','F13','F14','F15','G43','196','B80','K80','K99','L41','R59','U09','U43','U89','J68','Y59','R15','489','490','T01','T02','T03','T04','T05','T06','T07','T08','T10','U24','E13','T11','T12','T13','T14','T15','T16','T18','T19','U19','W14','W15','W52','X44','X45','X78','N09','X55','X56','X57','X58','X59','X60','X61','X62','T33','X64','X65','X66','X67','X68','X69','X70','X71','X72','M13','J89','I79','I80','I81','I82','I83','I84','I85','I86','I87','I88','I89','I91','Q87','Q88','Q89','Q90','Q91','Q92','Q93','Q94','Q95','Q96','603','606','607','624','625','626','W17','Z44','520','521','522','535','F11','T09','X50','X53','I90'];
            for($i=0; $i < count($region10); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region10';
                $query3->server_code = $region10[$i];
                $query3->save();
            }

            $region11 = ['661','662','669','670','675','676','677','680','681','725','994','G34','679','152','M57','U25','J22','Z47','958','S90','W55','W57','W58','W59','W62','G07','Q57','G03','F69','F71','G41','G42','H14','U20','X21','T86','T87','674','673','R50','R46','J88','T40','T42','W98','Y17','Y16','Y15','X98','K41','K42','K43','K44','K45','K46','K47','K48','K49','K50','P98','P99','M84','U52','U53','X92','X93','X94','X95','X96','X97','Z01','Z02','Z03','Z04','Z05','Z06','Z07','Z08','Z09','Z10','Z11','Z12','Z13','Z48','J41','Q59','Z15','Z16','Z17','Z18','R53','X23','Q30','Q33','Q34','Q35','Q36','Q37','Q38','Q67','Q39','Q40','Q42','Q43','Q44','Q45','864','720','659','660','G01','G02','F70','U18','R18','Z14','Q29','Q32','Q31'];
            for($i=0; $i < count($region11); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region11';
                $query3->server_code = $region11[$i];
                $query3->save();
            }

            $region12 = ['632','633','635','636','637','639','642','644','645','646','879','F81','H22','184','M61','X99','P95','K51','K76','L43','R05','R64','R20','U50','U82','W10','W11','X37','Y47','Z93','A44','M39','Y46','Q47','719','965','AA6','F03','F04','F72','F73','F74','365','K52','R65','R21','U83','U84','W13','X38','X39','Z99','A37','X46','631','643','647','L42','X43','F01','F02','194'];
            for($i=0; $i < count($region12); $i++){
                $query3 = new HostMasterServer();
                $query3->region_name = 'region12';
                $query3->server_code = $region12[$i];
                $query3->save();
            }
            
            return response()->json(["status" => true]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }
}
