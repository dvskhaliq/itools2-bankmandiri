<?php

namespace App\ServiceHandler\IntegrationLayer;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use Illuminate\Support\Facades\DB;
use App\WebServiceResponse;
use App\User;
use App\EmployeeRemedy;
use App\SupportGroupAssigneeRemedy;
use App\Workorder;
use Crypt;

class WOIWorkOrderImp implements WOIWorkOrderService
{
    public $wsdl_create_wo;
    public $wsdl_wo_att;

    public function __construct()
    {
        $this->wsdl_create_wo = env('WSDL_CREATEUPDATE_WO');
        $this->wsdl_wo_att = env('WSDL_WO_ATT');
    }
    
    public function getWsdlCreateWo(){
        return $this->wsdl_create_wo;
    }

    public function getWsdlWoAtt(){
        return $this->wsdl_wo_att;
    }
    
    // private $urlWsdl_Testing = "http://10.204.86.236/arsys/WSDL/public/10.204.86.235/MANDIRI:CSI:WOI:Interface2:RemedyItoolsDevelopment_Testing9"; // url testing
    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeaderAPI($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    ///////////////////////////////////////////////////////////////////////HEAD//////////////////////////////////////////////////////////////
    public function createWO(Workorder $workorder): WebServiceResponse
    {
        try {
            $data = array(
                'CustomerFirstName' => $workorder->requester_first_name == "null" || $workorder->requester_first_name == null ? null : $workorder->requester_first_name,
                'CustomerLastName' => $workorder->requester_last_name == "null" || $workorder->requester_last_name == null ? null : $workorder->requester_last_name,
                'CustomerCompany' => $workorder->requester_company == "null" || $workorder->requester_company == null ? null : $workorder->requester_company,
                'Notes' => $workorder->notes_description == "null" || $workorder->notes_description == null ? null : $workorder->notes_description,
                'Summary' => $workorder->summary_description == "null" || $workorder->summary_description == null ? null : $workorder->summary_description,
                'TemplateName' => '',
                'WorkOrderType' => $workorder->service_type == "null" || $workorder->service_type == null ? null : $workorder->service_type,
                'ManagerSupportGroupName' => 'Servicedesk',
                'AssigneeSupportGroupName' => $workorder->assignee_support_group_name == "null" || $workorder->assignee_support_group_name == null ? null : $workorder->assignee_support_group_name,
                'AssigneeName' => $workorder->assignee_name == "null" || $workorder->assignee_name == null ? null : $workorder->assignee_name,
                'ProCatTier1' => $workorder->prod_cat_tier1 == "null" || $workorder->prod_cat_tier1 == null ? null : $workorder->prod_cat_tier1,
                'ProCatTier2' => $workorder->prod_cat_tier2 == "null" || $workorder->prod_cat_tier2 == null ? null : $workorder->prod_cat_tier2,
                'ProCatTier3' => $workorder->prod_cat_tier3 == "null" || $workorder->prod_cat_tier3 == null ? null : $workorder->prod_cat_tier3,
                'OprCatTier1' => $workorder->opr_cat_tier1 == "null" || $workorder->opr_cat_tier1 == null ? null : $workorder->opr_cat_tier1,
                'OprCatTier2' => $workorder->opr_cat_tier2 == "null" || $workorder->opr_cat_tier2 == null ? null : $workorder->opr_cat_tier2,
                'OprCatTier3' => $workorder->opr_cat_tier3 == "null" || $workorder->opr_cat_tier3 == null ? null : $workorder->opr_cat_tier3,
                'ReserveField01' => $workorder->resrv_field_1 == "null" || $workorder->resrv_field_1 == null ? null : $workorder->resrv_field_1,
                'ReserveField02' => $workorder->resrv_field_2 == "null" || $workorder->resrv_field_2 == null ? null : $workorder->resrv_field_2,
                'ReserveField03' => $workorder->resrv_field_3 == "null" || $workorder->resrv_field_3 == null ? null : $workorder->resrv_field_3,
                'ReserveField04' => $workorder->resrv_field_4 == "null" || $workorder->resrv_field_4 == null ? null : $workorder->resrv_field_4,
                'ReserveField05' => $workorder->resrv_field_5 == "null" || $workorder->resrv_field_5 == null ? null : $workorder->resrv_field_5,
                'ReserveDate06' => $workorder->resrv_field_6 == "null" || $workorder->resrv_field_6 == null ? null : $workorder->resrv_field_6,
                'ReserveDate07' => $workorder->resrv_field_7 == "null" || $workorder->resrv_field_7 == null ? null : $workorder->resrv_field_7,
                'ReserveFlag08' => $workorder->resrv_field_8 == "null" || $workorder->resrv_field_8 == null ? null : $workorder->resrv_field_8,
                'ReserveFlag09' => $workorder->resrv_field_9 == "null" || $workorder->resrv_field_9 == null ? null : $workorder->resrv_field_9,
                'ReserveField10' => $workorder->resrv_field_10 == "null" || $workorder->resrv_field_10 == null ? null : $workorder->resrv_field_10,
                'ReserveField11' => $workorder->resrv_field_11 == "null" || $workorder->resrv_field_11 == null ? null : $workorder->resrv_field_11,
                'ReserveField12' => $workorder->resrv_field_12 == "null" || $workorder->resrv_field_12 == null ? null : $workorder->resrv_field_12,
                'ReserveField13' => $workorder->resrv_field_13 == "null" || $workorder->resrv_field_13 == null ? null : $workorder->resrv_field_13,
                'ReserveField14' => $workorder->resrv_field_14 == "null" || $workorder->resrv_field_14 == null ? null : $workorder->resrv_field_14,
                'ReserveField15' => $workorder->resrv_field_15 == "null" || $workorder->resrv_field_15 == null ? null : $workorder->resrv_field_15,
                'Company' => $workorder->requester_company == "null" || $workorder->requester_company == null ? null : $workorder->requester_company,
                'Priority' => $workorder->priority == "null" || $workorder->priority == null ? null : $workorder->priority,
                'AssigneeSupportCompany' => $workorder->assignee_support_company == "null" || $workorder->assignee_support_company == null ? null : $workorder->assignee_support_company,
                'AssigneeSupportOrganization' => $workorder->assignee_support_organization == "null" || $workorder->assignee_support_organization == null ? null : $workorder->assignee_support_organization,
                'Status' => $workorder->status == "null" || $workorder->status == null ? null : $workorder->status,
                'ItoolsRequestId' => $workorder->itools_request_id == "null" || $workorder->itools_request_id == null ? null : $workorder->itools_request_id,
                'ReportedSources' => $workorder->reported_source == "null" || $workorder->reported_source == null ? null : $workorder->reported_source
            );
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->CreateWorkorder($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, $doc->getElementsByTagName('WorkOrderId')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '', $data);
        }
    }

    public function bookWO(Workorder $workorder): WebServiceResponse
    {
        try {
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'WorkorderId' => $workorder->workorder_number == "null" || $workorder->workorder_number == null ? null : $workorder->workorder_number,
                'SupportGroupName' => $workorder->assignee_support_group_name == "null" || $workorder->assignee_support_group_name == null ? null : $workorder->assignee_support_group_name,
                'AssigneeName' => $workorder->assignee_name == "null" || $workorder->assignee_name == null ? null : $workorder->assignee_name,
                'AssigneeLoginId' => $workorder->assignee_user_id == "null" || $workorder->assignee_user_id == null ? null : $workorder->assignee_user_id,
                'Status' => $workorder->status == "null" || $workorder->status == null ? null : $workorder->status,
                'ItoolsRequestId' => $workorder->itools_request_id == "null" || $workorder->itools_request_id == null ? null : $workorder->itools_request_id
            );
            $client->BookTicket($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '', $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '', $data);
        }
    }

    public function checkingWO(Workorder $workorder): WebServiceResponse  // untuk pengecekan Remedy tiket yang terbentuk apakah tersimpan di Remedy atau tidak
    {
        try {
            $data = array('WorkoderId' => $workorder->workorder_number == "null" || $workorder->workorder_number == null ? null : $workorder->workorder_number);
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);            
            $client->isWorkorderExist($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, 'success', null, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    
    public function addNoteWO(Workorder $workorder, $note = ''): WebServiceResponse  // untuk add Note sebelum melakukan update status WO
    {
        try {
            $data = array(
                'WorkorderId' => $workorder->workorder_number,
                'WorkorderEntryId' => $workorder->workorder_number,
                'WorkInfoNote' => $note,
                'WorkInfoType' => 'General Information',
                'WorkInfoLocked' => 'No',
                'WorkInfoViewAccess' => 'Public'
            );
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);            
            $client->AddNote($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    ///////////////////////////////////////////////////////Below Code is Before Asssessment /////////////////////////////////////////////////

    public function addWorkInfoLog(array $array_data): WebServiceResponse
    {
        try {
            $data = array(
                'WorkOrderId'=> $array_data["ticket_id"],
                'WorkorderEntryId'=> $array_data["ticket_id"],
                'WorkInfoType' => $array_data["type"],
                'WorkInfoNotes' => $array_data["notes"],
                'AttachmentData' => base64_decode($array_data["attachment_data"]),
                'AttachmentName' => $array_data["attachment_name"],
                'AttachmentSize' => $array_data["attachment_size"]
                );
            $client = new SoapClient($this->getWsdlWoAtt(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->AddWorkInfoLog($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('WorkInfoId')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function bookTicket($request, $assignee_id)//Still used by parseWorkorder() on MainController
    {
        try {
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $actual_assingee = User::with('supportgroupassigneeremedy')->find($assignee_id);
            $data = array(
                'WorkorderId' => $request->RemedyTicketId,
                'SupportGroupName' => $actual_assingee->supportgroupassigneeremedy->support_group_name,
                'AssigneeName' => $actual_assingee->supportgroupassigneeremedy->full_name,
                'AssigneeLoginId' => $actual_assingee->un_remedy,
                'Status' => '',
                'ItoolsRequestId' => $request->id
            );
            $client->BookTicket($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '', $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function createWorkOrder($request, $assignee_id)//Still used by parseWorkorder() on MainController
    {
        try {
            $textTextDecoded = $request->TextDecoded;
            $splitTextDecoded = explode('#', trim($textTextDecoded));
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $customer = EmployeeRemedy::find($request->EmployeeSenderId);
            $actual_assingee = User::with('supportgroupassigneeremedy')->find($assignee_id);
            $data = array(
                'CustomerFirstName' => $customer->first_name,
                'CustomerLastName' => $customer->last_name,
                'CustomerCompany' => $customer->company,
                'Notes' => ($splitTextDecoded[3] != "null") ? $splitTextDecoded[3] : '',
                'Summary' => ($splitTextDecoded[0] != "null") ? $splitTextDecoded[0] : '',
                'TemplateName' => '',
                'WorkOrderType' => ($splitTextDecoded[2] != "null") ? $splitTextDecoded[2] : '',
                'ManagerSupportGroupName' => '',
                'AssigneeSupportGroupName' => $actual_assingee->supportgroupassigneeremedy->support_group_name,
                'AssigneeName' => $actual_assingee->supportgroupassigneeremedy->full_name,
                'ProCatTier1' => ($splitTextDecoded[8] != "null") ? $splitTextDecoded[8] : '',
                'ProCatTier2' => ($splitTextDecoded[9] != "null") ? $splitTextDecoded[9] : '',
                'ProCatTier3' => ($splitTextDecoded[10] != "null") ? $splitTextDecoded[10] : '',
                'OprCatTier1' => ($splitTextDecoded[11] != "null") ? $splitTextDecoded[11] : '',
                'OprCatTier2' => ($splitTextDecoded[12] != "null") ? $splitTextDecoded[12] : '',
                'OprCatTier3' => ($splitTextDecoded[13] != "null") ? $splitTextDecoded[13] : '',
                'ReserveField01' => ($splitTextDecoded[14] != "null") ? $splitTextDecoded[14] : '',
                'ReserveField02' => ($splitTextDecoded[15] != "null") ? $splitTextDecoded[15] : '',
                'ReserveField03' => ($splitTextDecoded[16] != "null") ? $splitTextDecoded[16] : '',
                'ReserveField04' => ($splitTextDecoded[17] != "null") ? $splitTextDecoded[17] : '',
                'ReserveField05' => ($splitTextDecoded[18] != "null") ? $splitTextDecoded[18] : '',
                'ReserveDate06' => ($splitTextDecoded[19] != "null") ? $splitTextDecoded[19] : '',
                'ReserveDate07' => ($splitTextDecoded[20] != "null") ? $splitTextDecoded[20] : '',
                'ReserveFlag08' => ($splitTextDecoded[21] != "null") ? $splitTextDecoded[21] : '',
                'ReserveFlag09' => ($splitTextDecoded[22] != "null") ? $splitTextDecoded[22] : '',
                'ReserveField10' => ($splitTextDecoded[23] != "null") ? $splitTextDecoded[23] : '',
                'ReserveField11' => ($splitTextDecoded[24] != "null") ? $splitTextDecoded[24] : '',
                'ReserveField12' => ($splitTextDecoded[25] != "null") ? $splitTextDecoded[25] : '',
                'ReserveField13' => ($splitTextDecoded[26] != "null") ? $splitTextDecoded[26] : '',
                'ReserveField14' => ($splitTextDecoded[27] != "null") ? $splitTextDecoded[27] : '',
                'ReserveField15' => ($splitTextDecoded[28] != "null") ? $splitTextDecoded[28] : '',
                'Company' => $customer->company,
                'Priority' => ($splitTextDecoded[7] != "null") ? $splitTextDecoded[7] : '',
                'AssigneeSupportCompany' => $actual_assingee->supportgroupassigneeremedy->company,
                'AssigneeSupportOrganization' => $actual_assingee->supportgroupassigneeremedy->organization,
                'Status' => 'In Progress',
                'ItoolsRequestId' => $request->id,
                'ReportedSources' => $request->servicecatalog->servicechannel->name,
		'url' => $this->getWsdlCreateWo()
            );
            $client->CreateWorkorder($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, $doc->getElementsByTagName('WorkOrderId')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function createWorkOrderByEntity($username, $password, $request)//Still used by createRemedyWO() on itoolsAPIController
    {
        try {
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeaderAPI($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'CustomerFirstName' => $request['CustomerFirstName'],
                'CustomerLastName' => $request['CustomerLastName'],
                'CustomerCompany' => $request['CustomerCompany'],
                'Notes' => $request['Notes'],
                'Summary' => $request['Summary'],
                'TemplateName' => $request['TemplateName'],
                'WorkOrderType' => $request['WorkOrderType'],
                'ManagerSupportGroupName' => $request['ManagerSupportGroupName'],
                'AssigneeSupportGroupName' => $request['AssigneeSupportGroupName'],
                'AssigneeName' => $request['AssigneeName'],
                'ProCatTier1' => $request['ProCatTier1'],
                'ProCatTier2' => $request['ProCatTier2'],
                'ProCatTier3' => $request['ProCatTier3'],
                'OprCatTier1' => $request['OprCatTier1'],
                'OprCatTier2' => $request['OprCatTier2'],
                'OprCatTier3' => $request['OprCatTier3'],
                'ReserveField01' => $request['ReserveField01'],
                'ReserveField02' => $request['ReserveField02'],
                'ReserveField03' => $request['ReserveField03'],
                'ReserveField04' => $request['ReserveField04'],
                'ReserveField05' => $request['ReserveField05'],
                'ReserveDate06' => $request['ReserveDate06'],
                'ReserveDate07' => $request['ReserveDate07'],
                'ReserveFlag08' => $request['ReserveFlag08'],
                'ReserveFlag09' => $request['ReserveFlag09'],
                'ReserveField10' => $request['ReserveField10'],
                'ReserveField11' => $request['ReserveField11'],
                'ReserveField12' => $request['ReserveField12'],
                'ReserveField13' => $request['ReserveField13'],
                'ReserveField14' => $request['ReserveField14'],
                'ReserveField15' => $request['ReserveField15'],
                'Company' => $request['Company'],
                'Priority' => $request['Priority'],
                'AssigneeSupportCompany' => $request['AssigneeSupportCompany'],
                'AssigneeSupportOrganization' => $request['AssigneeSupportOrganization'],
                'Status' => $request['Status'],
                'ItoolsRequestId' => $request['id'],
                'ReportedSources' => $request['ReportedSources']
            );
            $client->CreateWorkorder($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, $doc->getElementsByTagName('WorkOrderId')->item(0)->nodeValue,$data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    //Kayanya ga kepake
    public function isWorkorderExist($request)
    {
        try {
            $client = new SoapClient($this->getWsdlCreateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array('WorkorderId' => $request->RemedyTicketId);
            $client->isWorkorderExist($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }  
}
