import Vue from 'vue';
import ENUM from "../enum";

const defaultItem = () => {
    return {
        message: {
            message: "",
            type: ""
        }
    };
};

const state = {
    apiState: ENUM.INIT,
    message: defaultItem().message,
    items: [],
};

const getters = {
    apiState: state => state.apiState,
    message: state => state.message,
    items: state => state.items,
};

const actions = {
    async fetch({ commit }) {
        commit("setApiState", ENUM.LOADING);
        try {
            let response =  await axios.get("/sds/query/fetch");
            if(response.data.status){
                console.log('Connect to backend')
                commit("setItems", response.data.ticket);
                commit("setApiState", ENUM.LOADED);
            }else{
                console.log('Error')
                commit("setApiState", ENUM.LOADED);
            }
        } catch (error) {
            if (error.response !== undefined) {
                commit("setMessage", {
                    message: error.response.data.error.message,
                    type: "warning"
                })
            } else {
                commit("setMessage", {
                    message: "Failure system ! Data cannot be load",
                    type: "error"
                });
            }
        }
    },
    rese({ state }) {
        state.items=[];
    },
};

const mutations = {
    setItems: (state, items) => {
        state.apiState = ENUM.LOADED;
        state.items = items;
    },
    setApiState: (state, enumState) => (state.apiState = enumState),
    setMessage: (state, message) => {
        switch (message.type) {
            case "success":
                state.apiState = ENUM.SUCCESS;
                break;
            case "error":
                state.apiState = ENUM.ERROR;
                break;
            case "warning":
                state.apiState = ENUM.ERROR;
                break;
        }
        state.message.message = message.message
        state.message.type = message.type;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
