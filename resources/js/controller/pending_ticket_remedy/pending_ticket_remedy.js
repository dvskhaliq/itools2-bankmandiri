/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("../../bootstrap");

window.Vue = require("vue");
window.VueSelect = require("vue-select");

window.events = new Vue();
window.flash = function(message) {
    window.events.$emit("flash", message);
};
window.dismissflash = function() {
    window.events.$emit("dismissflash");
};

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component("pending-task", () =>
    import("../../components/pending_ticket_remedy/PendingTicketRemedy.vue")
);

Vue.component("modal-pending-workorder-closure", () =>
    import("../../components/pending_ticket_remedy/PendingTicketWOClosure.vue")
);

Vue.component("modal-pending-incident-closure", () =>
    import("../../components/pending_ticket_remedy/PendingTicketINClosure.vue")
);

Vue.component("modal-pending-incident-delegate", () =>
    import("../../components/pending_ticket_remedy/PendingTicketINDelegate.vue")
);

Vue.component("modal-pending-workorder-delegate", () =>
    import("../../components/pending_ticket_remedy/PendingTicketWODelegate.vue")
);

Vue.component("modal-wo-view", () =>
    import("../../components/pending_ticket_remedy/PendingTicketViewWO.vue")
);

Vue.component("modal-incident-view", () =>
    import("../../components/pending_ticket_remedy/PendingTicketViewDraft.vue")
);

Vue.component("spinner-data-loading", () =>
    import("../../components/dashboard/SpinnerDataLoading.vue")
);

Vue.component("flash", () => import("../../components/FlashMessage.vue"));
Vue.component("v-select", VueSelect.VueSelect);
Vue.component("loading-vue", () => import("../../components/LoadingVue.vue"));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import "es6-promise/auto";
import Vuex from "vuex";
import VeeValidate from "vee-validate";
import VueClipboard from "vue-clipboard2";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import { ValidationProvider } from "vee-validate";

import moment from "moment";
Vue.filter("mydateformat", function(value) {
    if (value) {
        return moment(String(value)).format("DD/MM/YYYY HH:mm:ss");
    }
});
Vue.use(VeeValidate);
Vue.use(VueClipboard);
Vue.use(Vuex);
Vue.use(Loading);

window.pending = new Vue({
    el: "#pending_ticket_remedy",
    data: {
        pendingdata: result,
        requestobject: {},
        isclosuring: false,
        ismodal: false,
        isdelegating: false,
        requestdata: [],
        isdataloaded: false,
        ismodal: false,
        modalname: "",
        auth: auth,
        customer: null,
    },

    methods: {
        modalClosed(req) {
            this.pendingdata = req.pendingdata;
            this.isclosuring = false;
            this.isdelegating = false;
            this.modalname = "";
            this.requestdata = ""
          
        },
        retryPendingCls(req) {
            this.requestobject = req.request;
            this.isclosuring = req.isclosuring;
        },
        retryPendingDlg(req) {
            this.requestobject = req.request;
            this.isdelegating = req.isdelegating;
        },

        viewTask(req) {
            this.ismodal = true;
            axios
                .post("/pending_ticket_remedy/viewtask", req)
                .then(response => {
                    this.requestdata = response.data;
                    (this.isdataloaded = true),
                        (this.modalname = req.modalname);

                    console.log(this.requestdata, "TEST");
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                });
        },

        cancelView() {
            this.isdataloaded = false;
            this.ismodal = false;
            this.modalname = "";
        }
    },
    created() {}
});
