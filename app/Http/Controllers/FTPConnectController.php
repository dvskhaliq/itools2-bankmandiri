<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Storage;
use View;
use App\FTPFileInventory;
use App\FTPCategory;
use JavaScript;

class FTPConnectController extends BaseController
{
    public function index(){
        $data = $this->refreshFileList();
        $category = FTPCategory::get();
        JavaScript::put([
            "storage" => $data,
            "category" => $category
        ]);
        return View::make('storage_ftp/storage_ftp');
    }

    public function uploadFile(){
        $category = request()->get('category');
        if ($category == "1"){
            $target_dir = "\\\\10.254.103.54\\Source_IT\\driver\\";
        } elseif ($category == "2"){
            $target_dir = "\\\\10.254.103.54\\Source_IT\\aplikasi_lainnya\\";
        } else {
            $target_dir = "\\\\10.254.103.54\\Source_IT\\bds\\";
        }
        $size = $_FILES["file"]["size"];
        $path = $target_dir . basename($_FILES["file"]["name"]);
        if(move_uploaded_file($_FILES["file"]["tmp_name"], $path)){
            $FTPFileInventory = new FTPFileInventory;
            $FTPFileInventory->file_name = basename($_FILES["file"]["name"]) ;
            $FTPFileInventory->path_file = $path;
            $FTPFileInventory->file_size = $size;
            $FTPFileInventory->ftp_category_id = $category;
            $FTPFileInventory->save();

            $status = true;
            $message = "successfully upload file to storage";
        } else{
            $status = false;
            $message = "sorry unable upload file to storage";
        }
        $data = $this->refreshFileList();
        return response()->json([ 
            'status' => $status,
            'message' => $message,
            'result' => $data 
        ]);
    }

    public function deletefile(){
        $hapus = FTPFileInventory::where('id', request()->get('id'))->pluck('path_file');
        unlink($hapus[0]);
        $hapus->delete();
    }

    public function refreshFileList(){
        return FTPFileInventory::with('ftp_category')->get();
        // return response()->json([ 
        //     'status' => true,
        //     'result' => $data,
        // ]);
    }

    public function storeToConfig($username, $password, $ipserver){
        config(['filesystems.disks.ftp' => [
            'driver' => 'ftp',
            'host' => $ipserver,
            'username' => $username,
            'password' => $password,
            'root'=> '.\\',
            'port' => 21
        ]]);
    }

    public function ftpUpload2(Request $request){
        try {
            $this->storeToConfig(session()->get('ftp.username'), session()->get('ftp.password'), session()->get('ftp.ipserver'));
            $file = $request->file('file');
            // $param = json_decode(request()->get('file'));
            $destination_file = basename($_FILES["file"]["name"]);
            Storage::disk('ftp')->put($destination_file, fopen($file, 'r+'));
            return response()->json(["status" => true, 'message' => "file have been upload to FTP Server"]);
        } catch (\Throwable $th) {
            return response()->json(["status" => false, 'message' => 'failed upload the file' . $th->__toString()]);
        }
        
    }

    public function ftpconnect2(){
        $data = collect(
            array(
                'ipserver' => request()->get('ipserver'),
                'username' => request()->get('username'),
                'password' => request()->get('password'),
            )
        );
        session()->put('ftp', $data);
        session()->save();

        $list = $this->ftpconnect();
        if($list[1]){
            return response()->json([                
                'result' => $list[0],
                'status' => true,
                'message' => "Connect to FTP Server ". session()->get('ftp.ipserver'),
                'ipserver' => session()->get('ftp.ipserver')
            ]);
        } else{
            return response()->json([ 
                'status' => false,
                'message' => $list[0],
            ]);
        }
        
    }

    public function ftpDisconnect(){
        $ftp_conn = $this->ftpLogin();
        ftp_close($ftp_conn[0]);
        session()->forget('ftp');
        session()->save();

        return response()->json([
            'status' => true,
            'message' => "Disconnect from FTP Server"
        ]);
    }

    public function ftpFileList(){
        $this->storeToConfig(session()->get('ftp.username'), session()->get('ftp.password'), session()->get('ftp.ipserver'));
        // $list = Storage::disk('ftp')->allFiles('.\\');

        $list = array();
        $files = Storage::disk('ftp')->files(null, false);
        foreach ($files as $key => $file) {
            $list[$key]['name'] = $file;
            $list[$key]['size'] = Storage::disk('ftp')->size($file);
            $list[$key]['last_modified'] = date("Y-m-d H:i:s", Storage::disk('ftp')->lastModified($file));
        }

        return $list;
    }

    public function refreshList(){
        $list = $this->ftpconnect();
        return response()->json([                
            'result' => $list[0],
            'status' => true,
        ]);
    }

    public function ftpLogin(){
        $ftp_server = session()->get('ftp.ipserver');
        $usernameftp = session()->get('ftp.username');
        $passwordftp = session()->get('ftp.password');
        
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to server ". $ftp_server);
        // $login = ftp_login($ftp_conn, $usernameftp, $passwordftp);
        if (@ftp_login($ftp_conn, $usernameftp, $passwordftp)){
            $status = true;
            return [$ftp_conn, $status];
        } else {
            $status = false;
            $message = "Couldn't establish a connection.";
            return [$message, $status];
        }
    }

    public function ftpconnect(){
        $ftp_conn = $this->ftpLogin();
        ftp_pasv($ftp_conn[0], true);
        if ($ftp_conn[1]) {
            $file_list = ftp_rawlist($ftp_conn[0], ".");
            $file_nlist = ftp_nlist($ftp_conn[0], ".");
            $array_length = count($file_list);
            
            // for($i=0;$i<$array_length;$i++){
            //     $post_list[$i] = preg_split("/[\s]+/", $file_list[$i],9);
            // }
            foreach ($file_nlist as $key => $file) {
                $post_list[$key]['name'] = $file;
                $post_list[$key]['size'] = ftp_size($ftp_conn[0], $file);
                $post_list[$key]['last_modified'] = date("Y-m-d H:i:s", ftp_mdtm($ftp_conn[0], $file));
            }
            $list = [$post_list,$ftp_conn[1]];
        } else {
            $list = $ftp_conn;
        }

        return $list;
    }

    public function ftpUpload(){
        ob_end_flush();
        $ftp_conn = $this->ftpLogin();
        $file = $_FILES["file"]["tmp_name"];
        $destination_file = basename($_FILES["file"]["name"]);

        $fp = fopen($file, 'r');
        $ret = ftp_nb_fput($ftp_conn[0], $destination_file, $fp, FTP_BINARY);
        while ($ret == FTP_MOREDATA) {
            // print ftell ($fp)."\n";
            
            $ret = ftp_nb_continue($ftp_conn[0]);
                 
        }
         
        if ($ret != FTP_FINISHED) {
            return response()->json(["status" => false, 'message' => 'failed upload the file']);
            exit(1);
        }
        else {
            return response()->json(["status" => true, 'message' => "file have been upload to FTP Server"]);
        }
        fclose($fp);
    }
}
