<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Inbox;
use Auth;

class BaseController extends Controller
{

    public function __construct()
    {
        // $data = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
        //     ->where('AssigneeId', Auth::user()->id)
        //     ->where('ProcessStatus', 'like', '%PENDING%')
        //     ->orderBy('ReceivingDateTime', 'desc')
        //     ->orderBy('id', 'desc')
        //     ->get();

        // JavaScript::put([
        //     "pendingTicket" => $data,
        //     "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type']),
        // ]);
    }

    public function countPendingTicket()
    {
        $count = Inbox::where('AssigneeId', Auth::user()->id)
            ->where('ActionTicketCreation', 'ILIKE', '%PENDING%')
            ->orderBy('ReceivingDateTime', 'desc')
            ->orderBy('id', 'desc')
            ->count();

        return response()->json(["status" => true, "message" => $count]);
    }
}
