<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/custom/host_master_servers/add','ItoolsAPIController@insertToHostMasterServers');
Route::post('/remedy/user/add','ItoolsAPIController@createRemedyUser');
Route::post('/remedy/user/fetch','ItoolsAPIController@fetchRemedyUser');
Route::post('/remedy/workorder/add','ItoolsAPIController@createRemedyWO');
Route::post('/remedy/workorder/update','ItoolsAPIController@updateRemedyWO');
Route::post('/remedy/incident/add','ItoolsAPIController@createRemedyIN');
Route::post('/remedy/incident/upload_file','ItoolsAPIController@uploadFile');
Route::post('/remedy/incident/update','ItoolsAPIController@updateRemedyIN');
Route::post('/remedy/incident/fetch','ItoolsAPIController@fetchRemedyIN');
Route::post('/tblu_apdy/add','ItoolsAPIController@createTbluApdy');
Route::post('/remedy/problem/update','ItoolsAPIController@updateProblem');

//Dashboard Reporting
Route::get('/view/public/reporting','ReportingController@generateSummaryTotalReport');
Route::get('/view/public/reporting/{pdate}','ReportingController@generateSummaryTotalReportByDate');
Route::get('/view/public/chart','ReportingController@chartReport');
Route::get('/view/public/chart/{pdate}','ReportingController@chartReportByDate');

Route::post('/remedy/sgar/fetch1','ItoolsAPIController@fetchSGP');
Route::post('/remedy/sgar/fetch2','ItoolsAPIController@fetchSGPbyLoginId');
// Route::post('/remedy/create/incident','ItoolsAPIController@createIncident');
// Route::post('/remedy/find/incident','ItoolsAPIController@findIncident');
Route::post('/remedy/search/changes','ItoolsAPIController@listChangesInfra');
Route::get('/testing/imam/inc','ItoolsAPIController@testingImam');
Route::get('/appdynamics/get','AppdynamicsController@index');
Route::get('/appdynamics/get/ranged','AppdynamicsController@getBetweenRange');
Route::get('/appdynamics/get/longranged','AppdynamicsController@getBetweenLongRange');

// Source from Mysql Yogi
Route::get('/edcUssd', 'DataTableauController@edcUssd');

// Source from Sql Server Tabel Vian ASP
Route::get('/success_rate', 'DataTableauController@success');
Route::get('/asp_restime', 'DataTableauController@aspResptime');

// Source from Kibana
Route::get('/success_soa_biru', 'DataTableauController@successSoaBiru');
Route::get('/emoney_kibana', 'DataTableauController@eMoneyRestimeKibana');
Route::get('/success_emoney', 'DataTableauController@successEmoney');
Route::post('/check_migration', 'BulkController@checkMigration');

//Source Host ICS
Route::get('/ics', 'Host400Controller@icsData');
Route::get('/ve', 'Host400Controller@veData');

//Kopra Transaction Snapshot
Route::get('/appdynamics/koprats','AppdynamicsController@koprats');
