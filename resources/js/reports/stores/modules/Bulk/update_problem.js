import ENUM from "../../enum";
const defaultItem = () => {
    return {
        item: {
            ProblemNumber: null,
            SupportCompanyPbmMgr: null,
            SupportOrganizationPbmMgr: null,
            SupportGroupPbmMgr: null,
            SupportGroupIDPbmMgr: null,
            AssigneePbmMgr: null,
            AssigneePbmMgrID: null,
            AssigneePbmMgrPersonId: null,
            ServiceCI: null,
            ServiceCIReconID: null,
            Summary: null,
            Notes: null,
            InvestigationDriver: null,
            InvestigationJustification: null,
            TargetDate: null,
            Impact: null,
            Urgency: null,
            AssignedSupportCompany: null,
            AssignedSupportOrganization: null,
            AssignedSupportGroup: null,
            AssignedSupportGroupID: null,
            Assignee: null,
            Status: null,
            StatusReason: null,
            Resolution: null,
            ProdCatTier1: null,
            ProdCatTier2: null,
            ProdCatTier3: null,
            OprCatTier1: null,
            OprCatTier2: null,
            OprCatTier3: null,
            ApplicationRootCause: null,
            JamKejadian: null,
            PenyebabMasalah: null,
            Workaround: null,
            WorkaroundTime: null,
            RootCause: null,
            ClosedTime: null,
            IR: null,
            RootCauseCategory: null,
            WorkaroundTerm: null,
            PermanentTerm: null,
            ImpactedApplication: null,
            Category: null,
            Downtime: null,
            MatrixRootCause: null,
            StatusWorkaround: null,
            ImpactedAppCategory: null,
            DescTableau: null,
            ResponseMessage: null,
            ResponseStatus:null,
        },
        message: {
            message: "",
            type: ""
        }
    };
};

const state = {
    apiState: ENUM.INIT,
    editedItem: defaultItem().item,
    message: defaultItem().message,
};

const getters = {
    apiState: state => state.apiState,
    message: state => state.message,
    editItem: state => state.editedItem,
};

const actions = {
    initBulk({ commit }, payload) {
        let data = {
            ProblemNumber: payload.ProblemNumber,
            SupportCompanyPbmMgr: payload.SupportCompanyPbmMgr,
            SupportOrganizationPbmMgr: payload.SupportOrganizationPbmMgr,
            SupportGroupPbmMgr: payload.SupportGroupPbmMgr,
            SupportGroupIDPbmMgr: payload.SupportGroupIDPbmMgr,
            AssigneePbmMgr: payload.AssigneePbmMgr,
            AssigneePbmMgrID: payload.AssigneePbmMgrID,
            AssigneePbmMgrPersonId: payload.AssigneePbmMgrPersonId,
            ServiceCI: payload.ServiceCI,
            ServiceCIReconID: payload.ServiceCIReconID,
            Summary: payload.Summary,
            Notes: payload.Notes,
            InvestigationDriver: payload.InvestigationDriver,
            InvestigationJustification: payload.InvestigationJustification,
            TargetDate: payload.TargetDate,
            Impact: payload.Impact,
            Urgency: payload.Urgency,
            AssignedSupportCompany: payload.AssignedSupportCompany,
            AssignedSupportOrganization: payload.AssignedSupportOrganization,
            AssignedSupportGroup: payload.AssignedSupportGroup,
            AssignedSupportGroupID: payload.AssignedSupportGroupID,
            Assignee: payload.Assignee,
            Status: payload.Status,
            StatusReason: payload.StatusReason,
            Resolution: payload.Resolution,
            ProdCatTier1: payload.ProdCatTier1,
            ProdCatTier2: payload.ProdCatTier2,
            ProdCatTier3: payload.ProdCatTier3,
            OprCatTier1: payload.OprCatTier1,
            OprCatTier2: payload.OprCatTier2,
            OprCatTier3: payload.OprCatTier3,
            ApplicationRootCause: payload.ApplicationRootCause,
            JamKejadian: payload.JamKejadian,
            PenyebabMasalah: payload.PenyebabMasalah,
            Workaround: payload.Workaround,
            WorkaroundTime: payload.WorkaroundTime,
            RootCause: payload.RootCause,
            ClosedTime: payload.ClosedTime,
            IR: payload.IR,
            RootCauseCategory: payload.RootCauseCategory,
            WorkaroundTerm: payload.WorkaroundTerm,
            PermanentTerm: payload.PermanentTerm,
            ImpactedApplication: payload.ImpactedApplication,
            Category: payload.Category,
            Downtime: payload.Downtime,
            MatrixRootCause: payload.MatrixRootCause,
            StatusWorkaround: payload.StatusWorkaround,
            ImpactedAppCategory: payload.ImpactedAppCategory,
            DescTableau: payload.DescTableau,
            ResponseStatus: "in progress",
            ResponseMessage: null
        };
        commit("setEditItem", data);
    },
    async doUpdateProblem({ commit }, payload) {
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/problem/update",
                payload
            );
            console.log({ responseUpdate: response });
            commit("setSubmitted", {
                status: true,
                response: response.data,
                payload: payload
            });
        } catch (error) {
            console.log({ errorUpdate: error });
            if (error.response.status == 403) {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.error,
                    payload: payload
                });
            } else {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.message,
                    payload: payload
                });
            }
        }
    },
    async stopProcess({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/process/stop", payload);
                commit("setMessage", {
                    message: "The process has been successfully terminated !",
                    type: "success"
                })
        } catch (error) {
            error.response.status == 403
                ? commit("setMessage", {
                    message: error.response.data.error,
                    type: "error"
                })
                : commit("setMessage", {
                    message: "Something wrong !",
                    type: "error"
                });
        }
    },
};

const mutations = {
    setEditItem: (state, payload) => (state.editedItem = payload),
    setSubmitted: (state, data) => {
        let result = {}
        if (data.status) {
            result = {
                ProblemNumber: data.response.data.ProblemNumber,
                SupportCompanyPbmMgr: data.response.data.SupportCompanyPbmMgr,
                SupportOrganizationPbmMgr: data.response.data.SupportOrganizationPbmMgr,
                SupportGroupPbmMgr: data.response.data.SupportGroupPbmMgr,
                SupportGroupIDPbmMgr: data.response.data.SupportGroupIDPbmMgr,
                AssigneePbmMgr: data.response.data.AssigneePbmMgr,
                AssigneePbmMgrID: data.response.data.AssigneePbmMgrID,
                AssigneePbmMgrPersonId: data.response.data.AssigneePbmMgrPersonId,
                ServiceCI: data.response.data.ServiceCI,
                ServiceCIReconID: data.response.data.ServiceCIReconID,
                Summary: data.response.data.Summary,
                Notes: data.response.data.Notes,
                InvestigationDriver: data.response.data.InvestigationDriver,
                InvestigationJustification: data.response.data.InvestigationJustification,
                TargetDate: data.response.data.TargetDate,
                Impact: data.response.data.Impact,
                Urgency: data.response.data.Urgency,
                AssignedSupportCompany: data.response.data.AssignedSupportCompany,
                AssignedSupportOrganization: data.response.data.AssignedSupportOrganization,
                AssignedSupportGroup: data.response.data.AssignedSupportGroup,
                AssignedSupportGroupID: data.response.data.AssignedSupportGroupID,
                Assignee: data.response.data.Assignee,
                Status: data.response.data.Status,
                StatusReason: data.response.data.StatusReason,
                Resolution: data.response.data.Resolution,
                ProdCatTier1: data.response.data.ProdCatTier1,
                ProdCatTier2: data.response.data.ProdCatTier2,
                ProdCatTier3: data.response.data.ProdCatTier3,
                OprCatTier1: data.response.data.OprCatTier1,
                OprCatTier2: data.response.data.OprCatTier2,
                OprCatTier3: data.response.data.OprCatTier3,
                ApplicationRootCause: data.response.data.ApplicationRootCause,
                JamKejadian: data.response.data.JamKejadian,
                PenyebabMasalah: data.response.data.PenyebabMasalah,
                Workaround: data.response.data.Workaround,
                WorkaroundTime: data.response.data.WorkaroundTime,
                RootCause: data.response.data.RootCause,
                ClosedTime: data.response.data.ClosedTime,
                IR: data.response.data.IR,
                RootCauseCategory: data.response.data.RootCauseCategory,
                WorkaroundTerm: data.response.data.WorkaroundTerm,
                PermanentTerm: data.response.data.PermanentTerm,
                ImpactedApplication: data.response.data.ImpactedApplication,
                Category: data.response.data.Category,
                Downtime: data.response.data.Downtime,
                MatrixRootCause: data.response.data.MatrixRootCause,
                StatusWorkaround: data.response.data.StatusWorkaround,
                ImpactedAppCategory: data.response.data.ImpactedAppCategory,
                DescTableau: data.response.data.DescTableau,
                ResponseMessage: data.response.message,
                ResponseStatus: data.response.processStatus
            };
        } else {
            result = {
                ProblemNumber: data.payload.ProblemNumber,
                SupportCompanyPbmMgr: data.payload.SupportCompanyPbmMgr,
                SupportOrganizationPbmMgr: data.payload.SupportOrganizationPbmMgr,
                SupportGroupPbmMgr: data.payload.SupportGroupPbmMgr,
                SupportGroupIDPbmMgr: data.payload.SupportGroupIDPbmMgr,
                AssigneePbmMgr: data.payload.AssigneePbmMgr,
                AssigneePbmMgrID: data.payload.AssigneePbmMgrID,
                AssigneePbmMgrPersonId: data.payload.AssigneePbmMgrPersonId,
                ServiceCI: data.payload.ServiceCI,
                ServiceCIReconID: data.payload.ServiceCIReconID,
                Summary: data.payload.Summary,
                Notes: data.payload.Notes,
                InvestigationDriver: data.payload.InvestigationDriver,
                InvestigationJustification: data.payload.InvestigationJustification,
                TargetDate: data.payload.TargetDate,
                Impact: data.payload.Impact,
                Urgency: data.payload.Urgency,
                AssignedSupportCompany: data.payload.AssignedSupportCompany,
                AssignedSupportOrganization: data.payload.AssignedSupportOrganization,
                AssignedSupportGroup: data.payload.AssignedSupportGroup,
                AssignedSupportGroupID: data.payload.AssignedSupportGroupID,
                Assignee: data.payload.Assignee,
                Status: data.payload.Status,
                StatusReason: data.payload.StatusReason,
                Resolution: data.payload.Resolution,
                ProdCatTier1: data.payload.ProdCatTier1,
                ProdCatTier2: data.payload.ProdCatTier2,
                ProdCatTier3: data.payload.ProdCatTier3,
                OprCatTier1: data.payload.OprCatTier1,
                OprCatTier2: data.payload.OprCatTier2,
                OprCatTier3: data.payload.OprCatTier3,
                ApplicationRootCause: data.payload.ApplicationRootCause,
                JamKejadian: data.payload.JamKejadian,
                PenyebabMasalah: data.payload.PenyebabMasalah,
                Workaround: data.payload.Workaround,
                WorkaroundTime: data.payload.WorkaroundTime,
                RootCause: data.payload.RootCause,
                ClosedTime: data.payload.ClosedTime,
                IR: data.payload.IR,
                RootCauseCategory: data.payload.RootCauseCategory,
                WorkaroundTerm: data.payload.WorkaroundTerm,
                PermanentTerm: data.payload.PermanentTerm,
                ImpactedApplication: data.payload.ImpactedApplication,
                Category: data.payload.Category,
                Downtime: data.payload.Downtime,
                MatrixRootCause: data.payload.MatrixRootCause,
                StatusWorkaround: data.payload.StatusWorkaround,
                ImpactedAppCategory: data.payload.ImpactedAppCategory,
                DescTableau: data.payload.DescTableau,
                ResponseMessage: data.response.length>30 ? data.response.substring(0,50)+'...':data.response,
                ResponseStatus: 'rejected'
            }
        }
        state.editedItem = result;
    },
    setApiState: (state, enumApiState) => {
        state.apiState = enumApiState;
    },
    setMessage: (state, message) => {
        switch (message.type) {
            case "success":
                state.apiState = ENUM.SUCCESS;
                break;
            case "error":
                state.apiState = ENUM.ERROR;
                break;
        }
        state.message.message = message.message
        state.message.type = message.type;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
