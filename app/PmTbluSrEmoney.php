<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmTbluSrEmoney extends Model
{
    protected $table = 'pm_tblu_sr_emoney';
    public $timestamps = false;
}
