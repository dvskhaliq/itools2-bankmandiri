import '../bootstrap';
import 'chart.js'
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'; // Ensure you are using css-loader
import Alert from './components/alert/Alert';
import SimpleLoading from './components/SimpleLoading'; // Global for simple loading
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import Vue from 'vue';
import store from './stores';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router'; // User original roter from vue
import router from './routers/router'; // Global variable used for routing purpose
import ProgressLinear from "./components/ProgressLinear";
import ResponseMessage from "./components/alert/ResponseMessage";

axios.defaults.baseURL = '/'; // Set base URL for axios
window.Vue = Vue;
Vue.router = router; // Set Vuew for use router variable
Vue.use(VueRouter); // Tell vue for using vue router lib
Vue.use(Vuetify);
Vue.component('simple-loading',SimpleLoading);
Vue.component('alert-message',Alert);
Vue.component('loading',Loading);
Vue.component('progress-linear',ProgressLinear);
Vue.component('response-message',ResponseMessage);

const vuetify = new Vuetify();
const app = new Vue({
    el: '#report_problem',
    vuetify,
    store,
    router
});
store.$routes = app.$router;
export default app;