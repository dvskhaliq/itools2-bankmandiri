<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\Bulk\IncidentProblemService;
use Illuminate\Http\Request;
use App\BulkStatus;
use App\Jobs\CreateRelation;
use App\ServiceHandler\DataLayer\EmployeeRemedyService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\IntegrationLayer\DLDMigrateLoginService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use App\ServiceHandler\PBMLayer\Bulk\BulkStatusService;
use App\ServiceHandler\PBMLayer\Bulk\KnowledgeService;
use Auth;

class BulkController extends BaseController
{
    protected $inc_prb_repo;
    protected $support_group_repo;
    protected $employee_remedy_repo;
    protected $incupdate_repo;
    protected $bulkstatus_repo;
    protected $migration_repo;
    protected $kms_repo;

    public function __construct(
        IncidentProblemService $inc_prb_service,
        SupportUserService $support_group_service,
        EmployeeRemedyService $employee_remedy_service,
        WOIIncidentUpdateService $incupdate_service,
        BulkStatusService $bulkstatus_service,
        DLDMigrateLoginService $migrateL_login_service,
        KnowledgeService $kms_service
    ) {
        $this->inc_prb_repo = $inc_prb_service;
        $this->support_group_repo = $support_group_service;
        $this->employee_remedy_repo = $employee_remedy_service;
        $this->incupdate_repo = $incupdate_service;
        $this->bulkstatus_repo = $bulkstatus_service;
        $this->migration_repo = $migrateL_login_service;
        $this->kms_repo = $kms_service;
    }

    public function checkMigration(Request $request)
    {
        $data_wizard_id = $request->input('wizardId');
        $checking_count = $request->input('checkCount');
        try {
            $rslt = $this->migration_repo->checkMigrationStatus($data_wizard_id);
            if ($rslt->getResponseStatus() == 'success') {
                return response()->json([
                    'nextCheckCount' => 0,
                    'wizardId' => $data_wizard_id,
                    'processStatus' => 'completed',
                    'message' => $data_wizard_id . ' process has been completed',
                    'arr' => $rslt->getArrContent(),
                    'status' => true
                ], 200);
            } else {
                return response()->json([
                    'nextCheckCount' => $checking_count + 1,
                    'wizardId' => $data_wizard_id,
                    'processStatus' => 'processed',
                    'message' => $data_wizard_id . ' in progress, checked for ' . $checking_count . ' times',
                    'arr' => $rslt->getResponseMessage(),
                    'status' => true
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'wizardId' => $data_wizard_id,
                'processStatus' => 'rejected',
                'message' => $th->__toString(),
                'status' => false
            ], 400);
        }
    }

    public function migrateLoginId(Request $request)
    {
        BulkStatus::where('name', 'migration_loginid')->update(['is_processing' => true]);
        $source = $request->input('source');
        $target = $request->input('target');
        try {
            $rslt = $this->migration_repo->initMigration($source, $target, strtotime(now()));
            if ($rslt->getResponseStatus() == 'success') {
                return response()->json([
                    'source' => $source,
                    'target' => $target,
                    'wizardId' => $rslt->getArrContent()['DataWizardID'],
                    'processStatus' => 'submited',
                    'message' => $rslt->getResponseMessage(),
                    'arr' => $rslt->getArrContent(),
                    'status' => true
                ], 200);
            } else {
                return response()->json([
                    'source' => $source,
                    'target' => $target,
                    'processStatus' => 'rejected',
                    'message' => $rslt->getResponseMessage(),
                    'status' => false
                ], 400);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'source' => $source,
                'target' => $target,
                'processStatus' => 'rejected',
                'message' => $th->__toString(),
                'status' => false
            ], 400);
        }
    }

    public function stopProcess(Request $request)
    {
        BulkStatus::where('name', $request->input('name'))->update(['is_processing' => false]);
        return response()->json([
            'status' => true,
            'is_processing' => false
        ], 200);
    }

    // public function createRelation(Request $request)
    // {
    //     // CreateRelation::dispatch($this->inc_prb_repo, $request->input('data'), $this->bulkstatus);
    //     CreateRelation::dispatch($this->inc_prb_repo, $request->input('data'), Auth::user())->onQueue('createrelation');
    //     return response()->json([
    //         'status' => true,
    //         'message' => 'Create relation job dispathced',
    //         'auth' => Auth::user()
    //     ], 200);
    // }

    public function createRelation(Request $request)
    {
        BulkStatus::where('name', $request->input('FieldName'))->update(['is_processing' => true]);
        $check_incident = $this->inc_prb_repo->chekingIncident($request->input("IncidentNumber"));
        $check_problem = $this->inc_prb_repo->chekingProblem($request->input("ProblemNumber"));
        $detail = [
            'IncidentNumber' => $request->input("IncidentNumber"),
            'ProblemNumber' => $request->input("ProblemNumber"),
            'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage()
        ];
        $result = [];
        $temp = null;

        try {
            if ($check_incident->getResponseStatus() == 'success' && $check_problem->getResponseStatus() == 'success') {
                $temp = true;
                $wsdl_incident = $this->inc_prb_repo->createIncidentRelation($detail);
                if ($wsdl_incident->getResponseStatus() == 'success'){
                    $wsdl_problem = $this->inc_prb_repo->createProblemRelation($detail);
                    if($wsdl_problem->getResponseStatus() == 'success'){
                        $result = [
                            'IncidentNumber' => $request->input("IncidentNumber"),
                            'ProblemNumber' => $request->input("ProblemNumber"),
                            'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage(),
                            'IncidentRelation' => $wsdl_incident->getContent(),
                            'ProblemRelation' =>  $wsdl_problem->getContent(),
                        ];
                    }else{
                        $result = [
                            'IncidentNumber' => $request->input("IncidentNumber"),
                            'ProblemNumber' => $request->input("ProblemNumber"),
                            'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage(),
                            'IncidentRelation' => $wsdl_incident->getContent(),
                            'ProblemRelation' =>  $wsdl_problem->getResponseMessage(),
                        ];
                    }
                }else{
                    $wsdl_problem = $this->inc_prb_repo->createProblemRelation($detail);
                    if($wsdl_problem->getResponseStatus() == 'success'){
                        $result = [
                            'IncidentNumber' => $request->input("IncidentNumber"),
                            'ProblemNumber' => $request->input("ProblemNumber"),
                            'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage(),
                            'IncidentRelation' => $wsdl_incident->getResponseMessage(),
                            'ProblemRelation' =>  $wsdl_problem->getContent(),
                        ];
                    }else{
                        $result = [
                            'IncidentNumber' => $request->input("IncidentNumber"),
                            'ProblemNumber' => $request->input("ProblemNumber"),
                            'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage(),
                            'IncidentRelation' => $wsdl_incident->getContent(),
                            'ProblemRelation' =>  $wsdl_problem->getContent(),
                        ];
                    }
                }
            }else{
                $temp = false;
                $result = [
                    'IncidentNumber' => $request->input("IncidentNumber"),
                    'ProblemNumber' => $request->input("ProblemNumber"),
                    'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage(),
                    'IncidentRelation' => $check_incident->getResponseStatus() == 'success' ? 'Failed create incident relation' : $check_incident->getResponseMessage(),
                    'ProblemRelation' =>  $check_problem->getResponseStatus() == 'success' ? 'Failed create problem relation' : $check_problem->getResponseMessage(),
                ];
            }
            if($temp){
                if ($wsdl_incident->getResponseStatus() == 'success' && $wsdl_problem->getResponseStatus() == 'success') {
                    return response()->json([
                        'processStatus' => 'completed',
                        'status' => true,
                        'data' => $result
                    ], 200);
                } else {
                    return response()->json([
                        'processStatus' => 'rejected',
                        'status' => false,
                        'data' => $result
                    ], 200);
                }
            }else{
                return response()->json([
                    'processStatus' => 'rejected',
                    'status' => false,
                    'data' => $result
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'processStatus' => 'rejected',
                'status' => false,
                'data' => $detail,
                'message' => $th->__toString(),
            ], 200);
        }
    }

    public function fetchIncident(Request $request)
    {
        BulkStatus::where('name', $request->input('FieldName'))->update(['is_processing' => true]);

        $wsdlFetch = $this->incupdate_repo->fetchIncident($request->input('IncidentNumber'));
        if ($wsdlFetch->getResponseStatus() == 'success') {
            return response()->json([
                'status' => true,
                'data' => $wsdlFetch->getContent(),
            ], 200);
        } else {
            BulkStatus::where('name', $request->input('FieldName'))->update(['is_processing' => true]);
            return response()->json([
                'status' => false,
                'message' => $wsdlFetch->getResponseMessage(),
            ], 200);
        }
    }

    public function createIncident(Request $request)
    {
        BulkStatus::where('name', $request->input('FieldName'))->update(['is_processing' => true]);

        $payload = [
            'AssignedSupportCompany' => $request->input('AssignedSupportCompany'),
            'AssignedSupportOrganization' => $request->input('AssignedSupportOrganization'),
            'AssignedSupportGroup' => $request->input('AssignedSupportGroup'),
        ];
        $sgar = $this->support_group_repo->findByCompanyOrgGroupName($payload['AssignedSupportCompany'], $payload['AssignedSupportOrganization'], $payload['AssignedSupportGroup']);

        $payload_em = [
            'CustomerFirstName' => $request->input('CustomerFirstName'),
            'CustomerLastName' => $request->input('CustomerLastName'),
        ];
        $employee = $this->employee_remedy_repo->getEmployeeRemedyByName($payload_em['CustomerFirstName'], $payload_em['CustomerLastName']);
        if (strlen($request->input('ServiceCI')) > 0) {
            $wsdl_reconID = $this->incupdate_repo->getServiceReconID($request->input('ServiceCI'));
            $serviceCI = $request->input('ServiceCI');
        } else {
            $serviceCI = null;
        }
        $detail = [
            'AssignedSupportCompany' => $payload['AssignedSupportCompany'],
            'AssignedSupportOrganization' => $payload['AssignedSupportOrganization'],
            'AssignedSupportGroup' => $payload['AssignedSupportGroup'],
            'AssignedSupportGroupID' => $sgar == null ? null : $sgar->support_group_id,
            'Assignee' => $request->input('Assignee'),
            'CustomerFirstName' => $payload_em['CustomerFirstName'],
            'CustomerLastName' => $payload_em['CustomerLastName'],
            'CustomerID' => $employee == null ? null : $employee->id,
            'Summary' => $request->input('Summary'),
            'Notes' => $request->input('Notes'),
            'Impact' => $request->input('Impact'),
            'Urgency' => $request->input('Urgency'),
            'ProdCatTier1' => $request->input('ProdCatTier1'),
            'ProdCatTier2' => $request->input('ProdCatTier2'),
            'ProdCatTier3' => $request->input('ProdCatTier3'),
            'OprCatTier1' => $request->input('OprCatTier1'),
            'OprCatTier2' => $request->input('OprCatTier2'),
            'OprCatTier3' => $request->input('OprCatTier3'),
            'ReportedSource' => $request->input('ReportedSource'),
            'Resolution' => $request->input('Resolution'),
            'Status' => $request->input('Status'),
            'TanggalEmailMasuk' => $request->input('TanggalEmailMasuk'),
            'Survey' => $request->input('Survey'),
            'ServiceType' => $request->input('ServiceType'),
            'StatusReason' => $request->input('StatusReason'),
            'ServiceCI' => $request->input('ServiceCI'),
            'AssigneeLoginId' => $request->input('AssigneeLoginId'),
            'ServiceCIReconID' => !is_null($serviceCI) ? $wsdl_reconID->getContent() : null,
        ];

        try {
            $rslt = $this->inc_prb_repo->createIncidentGeneric($detail);
            if ($rslt->getResponseStatus() == 'success') {
                return response()->json([
                    'processStatus' => 'completed',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => true,
                    'data' => $detail
                ], 200);
            } else {
                return response()->json([
                    'processStatus' => 'rejected',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => false,
                    'data' => $detail
                ], 400);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'processStatus' => 'rejected',
                'message' => $th->__toString(),
                'status' => false,
                'data' => $detail
            ], 400);
        }
    }

    public function updateIncident(Request $request)
    {
        BulkStatus::where('name', 'update_incident')->update(['is_processing' => true]);

        $sgar = $this->support_group_repo->findByCompanyOrgGroupName($request->input('AssignedSupportCompany'), $request->input('AssignedSupportOrganization'), $request->input('AssignedSupportGroup'), $request->input("Assignee"));
        if (strlen($request->input('ServiceCI')) > 0) {
            $wsdl_reconID = $this->incupdate_repo->getServiceReconID($request->input('ServiceCI'));
            $serviceCI = $request->input('ServiceCI');
        } else {
            $serviceCI = null;
        }
        $detail = [
            'IncidentNumber' => $request->input('IncidentNumber'),
            'AssignedSupportCompany' => $request->input('AssignedSupportCompany'),
            'AssignedSupportOrganization' => $request->input('AssignedSupportOrganization'),
            'AssignedSupportGroup' => $request->input('AssignedSupportGroup'),
            'AssignedSupportGroupID' => $sgar == null ? null : $sgar->support_group_id,
            'Assignee' => $request->input('Assignee'),
            'AssigneeLoginID' => $sgar == null ? null : $sgar->remedy_login_id,
            'Summary' => $request->input("Summary"),
            'Notes' => $request->input("Notes"),
            'Impact' => $request->input("Impact"),
            'Urgency' => $request->input("Urgency"),
            'ProdCatTier1' => $request->input("ProdCatTier1"),
            'ProdCatTier2' => $request->input("ProdCatTier2"),
            'ProdCatTier3' => $request->input("ProdCatTier3"),
            'OprCatTier1' => $request->input("OprCatTier1"),
            'OprCatTier2' => $request->input("OprCatTier2"),
            'OprCatTier3' => $request->input("OprCatTier3"),
            'ReportedSource' => $request->input("ReportedSource"),
            'Resolution' => $request->input("Resolution"),
            'Status' => $request->input("Status"),
            'TanggalEmailMasuk' => $request->input("TanggalEmailMasuk"),
            'Survey' => $request->input("Survey"),
            'ServiceType' => $request->input("ServiceType"),
            'StatusReason' => $request->input("StatusReason"),
            'ServiceCI' => $request->input("ServiceCI"),
            'ServiceCIReconID' => !is_null($serviceCI) ? $wsdl_reconID->getContent() : null,
        ];

        try {
            $rslt = $this->inc_prb_repo->updateIncidentGeneric($detail);
            if ($rslt->getResponseStatus() == 'success') {
                return response()->json([
                    'processStatus' => 'completed',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => true,
                    'data' => $detail
                ], 200);
            } else {
                return response()->json([
                    'processStatus' => 'rejected',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => false,
                    'data' => $detail
                ], 400);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'processStatus' => 'rejected',
                'message' => $th->__toString(),
                'status' => false,
                'data' => $detail
            ], 400);
        }
    }

    public function updateProblem(Request $request)
    {
        BulkStatus::where('name', $request->input('FieldName'))->update(['is_processing' => true]);

        $payload_mgr = [
            'SupportCompanyPbmMgr' => $request->input("SupportCompanyPbmMgr"),
            'SupportOrganizationPbmMgr' => $request->input("SupportOrganizationPbmMgr"),
            'SupportGroupPbmMgr' => $request->input("SupportGroupPbmMgr"),
            'AssigneePbmMgr' => $request->input("AssigneePbmMgr"),
        ];
        $payload = [
            'AssignedSupportCompany' => $request->input("AssignedSupportCompany"),
            'AssignedSupportOrganization' => $request->input("AssignedSupportOrganization"),
            'AssignedSupportGroup' => $request->input("AssignedSupportGroup"),
        ];
        $sgar_mgr = $this->support_group_repo->findByCompanyOrgGroupName($payload_mgr['SupportCompanyPbmMgr'], $payload_mgr['SupportOrganizationPbmMgr'], $payload_mgr['SupportGroupPbmMgr'], $payload_mgr["AssigneePbmMgr"]);
        $sgar = $this->support_group_repo->findByCompanyOrgGroupName($payload['AssignedSupportCompany'], $payload['AssignedSupportOrganization'], $payload['AssignedSupportGroup']);
        if (strlen($request->input('ServiceCI')) > 0) {
            $wsdl_reconID = $this->incupdate_repo->getServiceReconID($request->input('ServiceCI'));
            $serviceCI = $request->input('ServiceCI');
        } else {
            $serviceCI = null;
        }
        $detail = [
            'ProblemNumber' => $request->input("ProblemNumber"),
            'SupportCompanyPbmMgr' => $request->input("SupportCompanyPbmMgr"),
            'SupportOrganizationPbmMgr' => $request->input("SupportOrganizationPbmMgr"),
            'SupportGroupPbmMgr' => $request->input("SupportGroupPbmMgr"),
            'SupportGroupIDPbmMgr' => $sgar_mgr == null ? null : $sgar_mgr->support_group_id,
            'AssigneePbmMgr' => $request->input("AssigneePbmMgr"),
            'AssigneePbmMgrID' => $sgar_mgr == null ? null : $sgar_mgr->remedy_login_id,
            'AssigneePbmMgrPersonId' => $sgar_mgr == null ? null : $sgar_mgr->employee_remedy_id,
            'ServiceCI' => $request->input("ServiceCI"),
            'ServiceCIReconID' => !is_null($serviceCI) ? $wsdl_reconID->getContent() : null,
            'Summary' => $request->input("Summary"),
            'Notes' => $request->input("Notes"),
            'InvestigationDriver' => $request->input("InvestigationDriver"),
            'InvestigationJustification' => $request->input("InvestigationJustification"),
            'TargetDate' => $request->input("TargetDate"),
            'Impact' => $request->input("Impact"),
            'Urgency' => $request->input("Urgency"),
            'AssigneeSupportCompany' => $request->input("AssigneeSupportCompany"),
            'AssigneeSupportOrganization' => $request->input("AssigneeSupportOrganization"),
            'AssigneeSupportGroup' => $request->input("AssigneeSupportGroup"),
            'AssigneeSupportGroupID' => $sgar == null ? null : $sgar->support_group_id,
            'Assignee' => $request->input("Assignee"),
            'Status' => $request->input("Status"),
            'StatusReason' => $request->input("StatusReason"),
            'Resolution' => $request->input("Resolution"),
            'ProdCatTier1' => $request->input("ProdCatTier1"),
            'ProdCatTier2' => $request->input("ProdCatTier2"),
            'ProdCatTier3' => $request->input("ProdCatTier3"),
            'OprCatTier1' => $request->input("OprCatTier1"),
            'OprCatTier2' => $request->input("OprCatTier2"),
            'OprCatTier3' => $request->input("OprCatTier3"),
            'ApplicationRootCause' => $request->input("ApplicationRootCause"),
            'JamKejadian' => $request->input("JamKejadian"),
            'PenyebabMasalah' => $request->input("PenyebabMasalah"),
            'Workaround' => $request->input("Workaround"),
            'WorkaroundTime' => $request->input("WorkaroundTime"),
            'RootCause' => $request->input("RootCause"),
            'ClosedTime' => $request->input("ClosedTime"),
            'IR' => $request->input("IR"),
            'RootCauseCategory' => $request->input("RootCauseCategory"),
            'WorkaroundTerm' => $request->input("WorkaroundTerm"),
            'PermanentTerm' => $request->input("PermanentTerm"),
            'ImpactedApplication' => $request->input("ImpactedApplication"),
            'ImpactedAppCategory' => $request->input("ImpactedAppCategory"),
            'Category' => $request->input("Category"),
            'Downtime' => $request->input("Downtime"),
            'MatrixRootCause' => $request->input("MatrixRootCause"),
            'StatusWorkaround' => $request->input("StatusWorkaround"),
            'DescTableau' => $request->input("DescTableau")
        ];

        try {
            $rslt = $this->inc_prb_repo->updateProblemGeneric($detail);
            if ($rslt->getResponseStatus() == 'success') {
                return response()->json([
                    'processStatus' => 'completed',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => true,
                    'data' => $detail
                ], 200);
            } else {
                return response()->json([
                    'processStatus' => 'rejected',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => false,
                    'data' => $detail
                ], 400);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'processStatus' => 'rejected',
                'message' => $th->__toString(),
                'status' => false,
                'data' => $detail
            ], 400);
        }
    }

    public function updateKnowledgeCategorization(Request $request)
    {
        BulkStatus::where('name', 'update_knowledge')->update(['is_processing' => true]);
        $payload = [
            'KmsID' => $request->input('Doc_ID'),
            'Title' => $request->input('Title'),
            'ProductCatagorization1' => $request->input('Product_Categorization_Tier_1'),
            'ProductCatagorization2' => $request->input('Product_Categorization_Tier_2'),
            'ProductCatagorization3' => $request->input('Product_Categorization_Tier_3'),
            'OprCatagorization1' => $request->input('Operational_Tier_1'),
            'OprCatagorization2' => $request->input('Operational_Tier_2'),
            'OprCatagorization3' => $request->input('Operational_Tier_3')
        ];
        try {
            $rslt = $this->kms_repo->updateKnowledgeCategorization($payload);
            if ($rslt->getResponseStatus() == 'success') {
                return response()->json([
                    'processStatus' => 'completed',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => true,
                    'data' => $payload
                ], 200);
            } else {
                return response()->json([
                    'processStatus' => 'rejected',
                    'arr' => $rslt->getArrContent(),
                    'message' => $rslt->getResponseMessage(),
                    'status' => false,
                    'data' => $payload
                ], 400);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'processStatus' => 'rejected',
                'message' => $th->__toString(),
                'status' => false,
                'data' => $payload
            ], 400);
        }
    }
}
