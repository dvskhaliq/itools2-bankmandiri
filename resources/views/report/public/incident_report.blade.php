@extends('sidebar')
@section('content')
<div id="report_incident">
<div class="row report-incident">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div class="panel-heading" style="background: #fff;">
                <div class="panel-title">
                    <h4>Incident Report</h4>
                </div>
            </div>
            <!-- START DEFAULT DATATABLE -->
            <div class="panel-body">
                <incident-report></incident-report>
            </div>
        </div>
    </div>
</div>
</div>
@include('footer')
<!-- <script src="../../../../js/incident_report.js"></script> -->
<script src="../../../js/incident_report.js"></script>
@endsection