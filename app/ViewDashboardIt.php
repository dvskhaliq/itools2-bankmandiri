<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewDashboardIt extends Model
{
    protected $table = 'view_dashboard_it';
    public $timestamps = false;
}
