@extends('sidebar')
@section('content')
<div id="user_configuration">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>User Configuration</h4>
                </div>
                <div class="panel-body">
                    <flash v-if="alert" info=""></flash>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>User Information</span>
                            </div>
                            <div class="panel-body">
                                <component-table :result="result" :alert="alert" :auth="auth" v-on:update-password="updatePassword" v-on:user-selected="userSelected" v-on:search-data="searchUser" v-on:user-roles="userSelectedRoles" v-on:create-user="createUser"></component-table>
                                <vue-pagination :pagination="pages" @paginate="getData()" :offset="4">
                                </vue-pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <componen-confirm-delete :selected_prop="selected_prop" v-on:submit-delete="submitDelete" v-on:close-modal="closeWindow" :isloaded="isloaded" :modalname="modalname" :ismodal="ismodal"></componen-confirm-delete>
    <componen-insert v-on:close-modal="closeWindow" v-on:submit-insert="submitInsert" :modalname="modalname" :ismodal="ismodal"> </componen-insert>
    <componen-edit :selected_prop="selected_prop" v-on:close-modal="closeWindow" v-on:submit-update="submitUpdate" :isloaded="isloaded" :modalname="modalname" :ismodal="ismodal"></componen-edit>
    <componen-detail :selected_prop="selected_prop" v-on:close-modal="closeWindow" :isloaded="isloaded" :modalname="modalname" :ismodal="ismodal"></componen-detail>
    <componen-roles :name_user="name_user" v-on:flash="flashAddRole" :user_id="user_id" :selected_role="selected_role" :selected_user="selected_user" v-on:close-modal="closeWindow" v-on:add-role="createUser" :isloaded="isloaded" :modalname="modalname" :ismodal="ismodal"></componen-roles>
    <!-- Not Used -->
    <!-- <componen-add-role :user_id="user_id" v-on:close-modal="closeWindow" v-on:flash="flashAddRole" :modalname="modalname" :ismodal="ismodal"></componen-add-role> -->

</div>
@include('footer')
<script src="../../js/user_configuration.js"></script>
@endsection
