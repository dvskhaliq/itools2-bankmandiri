<?php

namespace App\ServiceHandler\DataLayer;

use App\PBMProblemInvestigation as Problem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ProblemImp implements ProblemService
{
    protected $pi_repo;

    public function __construct(ProductionIssuesService $pi_service)
    {
        $this->pi_repo = $pi_service;
    }

    public function getSummary($year, $category): ?Collection
    {
        $index = array_search(null, $category, true);
        if ($index !== false) {
            unset($category[$index]);
            return Problem::select(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')) as year_created"), DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')) as month_created"), DB::raw('COUNT(1) as total'))
                ->where(function ($q) use ($year) {
                    $q->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $year)
                        ->whereNull('Category__c');
                })
                ->orWhere(function ($q) use ($year, $category) {
                    $q->whereIn('Category__c', $category)
                        ->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $year);
                })
                ->groupBy(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"))
                ->orderBy('month_created')
                ->get();
        } else {
            return Problem::select(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')) as year_created"), DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')) as month_created"), DB::raw('COUNT(1) as total'))
                ->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $year)
                ->whereIn('Category__c', $category)
                ->groupBy(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"))
                ->orderBy('month_created')
                ->get();
        }
    }

    public function tranformToProductionIssues(array $year): ?Collection
    {
        $this->pi_repo->truncateProductionIssues();
        $data = Problem::select('Problem_Investigation_ID', 'Jam_Kejadian', 'Priority', 'Status_Workaround__c', 'Matrix_Root_Cause__c', 'Penyebab_Masalah', 'Aplikasi_Root_Cause', 'Impacted_Application__c', 'Impacted_App_Category', 'Workaround_Time', 'Closed_Time', 'Desc_Tableau', 'Investigation_Status')
            ->where('Category__c', 0)
            ->whereIn(DB::raw("YEAR(DATEADD(ss,Jam_Kejadian,'19700101 07:00:00:000'))"), $year)
            ->whereNotIn('Investigation_Status', [7, 9])
            ->get();
	$data->transform(function ($item) {
            $priority = '';
            switch ($item->Priority) {
                case 0:
                    $priority = 'Very High';
                    break;
                case 1:
                    $priority = 'High';
                    break;
                case 2:
                    $priority = 'Medium';
                    break;
                case 3:
                    $priority = 'Low';
                    break;
            }
            $workaroud = '';
            switch ($item->Status_Workaround__c) {
                case 0:
                    $workaroud = 'Met Resolution Time';
                    break;
                case 1:
                    $workaroud = 'Breach Resolution Time';
                    break;
            }
            $root_cause  = '';
            switch ($item->Matrix_Root_Cause__c) {
                case 1:
                    $root_cause = 'Application';
                    break;
                case 2:
                    $root_cause = 'Jaringan';
                    break;
                case 3:
                    $root_cause = 'Server';
                    break;
                case 4:
                    $root_cause = 'Database';
                    break;
                case 5:
                    $root_cause = '3rd Party';
                    break;
                case 6:
                    $root_cause = 'Security Device';
                    break;
                case 7:
                    $root_cause = 'Security Attack';
                    break;
                case 8:
                    $root_cause = 'Arsitektur';
                    break;
                case 9:
                    $root_cause = 'Prosedur';
                    break;
            }
            $source_cause = '';
            switch ($item->Penyebab_Masalah) {
                case 0:
                    $source_cause = 'External';
                    break;
                case 1:
                    $source_cause = 'Internal';
                    break;
            }
            $impact_app_cat = '';
            switch ($item->Impacted_App_Category) {
                case 0:
                    $impact_app_cat = 'ATM';
                    break;
                case 1:
                    $impact_app_cat = 'Switcher ATM';
                    break;
                case 2:
                    $impact_app_cat = 'Biller';
                    break;
                case 3:
                    $impact_app_cat = 'Switcher Biller';
                    break;
                case 4:
                    $impact_app_cat = 'System Mandiri';
                    break;
                case 5:
                    $impact_app_cat = 'Telco';
                    break;
                case 6:
                    $impact_app_cat = 'Other';
                    break;
                case 7:
                    $impact_app_cat = 'ATM Mandiri';
                    break;
           }
            $status = '';
            switch ($item->Investigation_Status) {
                case 0:
                    $status = 'Draft';
                    break;
                case 1:
                    $status = 'Under Review';
                    break;
                case 2:
                    $status = 'Request For Authorization';
                    break;
                case 3:
                    $status = 'Assigned';
                    break;
                case 4:
                    $status = 'Under Investigation';
                    break;
                case 5:
                    $status = 'Pending';
                    break;
                case 6:
                    $status = 'Completed';
                    break;
                case 8:
                    $status = 'Closed';
                    break;
           }
            return [
                'problem_id' => $item->Problem_Investigation_ID,
                'recorded_at' => $item->Jam_Kejadian,
                'priority' => $priority,
                'workaround' => $workaroud,
                'root_cause' => $root_cause,
                'source_cause' => $source_cause,
                'app_root_cause' => $item->Aplikasi_Root_Cause,
                'impacted_app' => $item->Impacted_Application__c,
                'impacted_app_category' => $impact_app_cat,
                'desc' => $item->Desc_Tableau !== null ? substr(utf8_encode($item->Desc_Tableau),0,254): null,
                'jam_kejadian' => $item->Jam_Kejadian,
                'workaround_time' => $item->Workaround_Time !== null ? date('Y-m-d H:i:s', $item->Workaround_Time): null,
                'closed_time' => $item->Closed_Time !== null ? date('Y-m-d H:i:s', $item->Closed_Time): null,
                'status' => $status,
		'resolution' => $item->Implemented_Solution,
    	   	'downtime_minute' => $item->downtime_minute !== null ?  $item->downtime_minute  : null,
	        'workaround_desc' => $item->Temporary_Workaround,
	        'root_cause_desc' => $item->Root_Cause,
	        'notes' => $item->Detailed_Decription,
            ];
        });
	  
        $this->pi_repo->bulkInsert($data);
        return $this->pi_repo->getProductionIssues();
    }
}
