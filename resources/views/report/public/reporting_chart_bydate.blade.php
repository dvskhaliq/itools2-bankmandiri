@extends('master')
@section('sidebar')
<div class="row" id="report_summary">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div>
                <div class="panel-body" style="padding:2px">
                    <div class="col-md-12">
                        <div class="col-xs-12">
                            <div class="panel panel-default" style="margin-bottom:0px;">
                                <div class="panel-heading">
                                    <h4>Incoming Request in Chart</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <chart-line-1 :chartreport="chartreport"></chart-line-1>
                                    </div>
                                    <div class="col-md-4" style="margin-top: 50px;">
                                        <chart-bar-2 :categorydata="categorydata"></chart-bar-2>
                                    </div>
                                    <div class="col-md-4" style="margin-top: 50px;">
                                        <chart-web-1 :channeldata="channeldata"></chart-web-1>
                                    </div>
                                    <div class="col-md-4" style="margin-top: 50px;">
                                        <chart-bar-1 :typedata="typedata"></chart-bar-1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>Chart </h4>
                                </div>
                                <div class="panel-body">
                                    <br /><br /><br /><br />
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="../../../../js/summary_report.js"></script>
@endsection