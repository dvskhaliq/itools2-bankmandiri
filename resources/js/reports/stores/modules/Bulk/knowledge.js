import ENUM from "../../enum";

const state = {
    apiState: ENUM.INIT,
    alertMessage: "",
    knowledgeData: {
        Doc_ID: null,
        Title: null,
        Product_Categorization_Tier_1: null,
        Product_Categorization_Tier_2: null,
        Product_Categorization_Tier_3: null,
        Operational_Tier_1: null,
        Operational_Tier_2: null,
        Operational_Tier_3: null,
        Process_Status: null,
        Response: null,
    }
};

const getters = {
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getKnowledgeData: state => state.knowledgeData
};

const actions = {
    initBulk({ state }, payload) {
        let data = {
            Doc_ID: payload.Doc_ID,
            Title: payload.Title,
            Product_Categorization_Tier_1: payload.Product_Categorization_Tier_1,
            Product_Categorization_Tier_2: payload.Product_Categorization_Tier_2,
            Product_Categorization_Tier_3: payload.Product_Categorization_Tier_3,
            Operational_Tier_1: payload.Operational_Tier_1,
            Operational_Tier_2: payload.Operational_Tier_2,
            Operational_Tier_3: payload.Operational_Tier_3,
            Process_Status: "in progress",
            Response: null
        };
        state.knowledgeData = data;
    },
    async doUpdateKnowledge({ commit }, payload) {
        let response;
        try {
            response = await axios.post(
                "/data_management/problems/bulk/update_knowledge",
                payload
            );
            commit("setSubmitted", response.data);
        } catch (error) {
            error.response.status == 403
                ? commit(
                      "setException",
                      "You are not authorized to do this!"
                  )
                : commit("setKnowledgeError", {
                      payload: payload,
                      data: error.response.data
                  });
        }
    },
    async stopProcess({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/bulk/process/stop", payload);
                commit("setSuccess", "The process has been successfully terminated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                    "setException",
                    "Something wrong !"
                )
        }
      },
};

const mutations = {
    setSubmitted: (state, data) => {
        let result = {
            Doc_ID: data.data.KmsID,
            Title: data.data.Title,
            Product_Categorization_Tier_1: data.data.ProductCatagorization1,
            Product_Categorization_Tier_2: data.data.ProductCatagorization2,
            Product_Categorization_Tier_3: data.data.ProductCatagorization3,
            Operational_Tier_1: data.data.OprCatagorization1,
            Operational_Tier_2: data.data.OprCatagorization2,
            Operational_Tier_3: data.data.OprCatagorization3,
            Process_Status: data.processStatus,
            Response: data.message
        };
        state.knowledgeData = result;
    },
    setKnowledgeError: (state, rsp) => {
        console.log(rsp);
        let result = {
            Doc_ID: rsp.payload.Doc_ID,
            Title: rsp.payload.Title,
            Product_Categorization_Tier_1: rsp.payload.Product_Categorization_Tier_1,
            Product_Categorization_Tier_2: rsp.payload.Product_Categorization_Tier_2,
            Product_Categorization_Tier_3: rsp.payload.Product_Categorization_Tier_3,
            Operational_Tier_1: rsp.payload.Operational_Tier_1,
            Operational_Tier_2: rsp.payload.Operational_Tier_2,
            Operational_Tier_3: rsp.payload.Operational_Tier_3,
            Process_Status: 'rejected',
            Response: rsp.data.message,
        };
        state.knowledgeData = result;
    },
    setApiState: (state, enumApiState) => {
        state.apiState = enumApiState;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
