<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsappDispatcherMessages extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['request_id','sender_id', 'content', 'timestamp'];
    public $timestamps = false;

    public function inbox()
    {
        return $this->belongsTo(Inbox::class, 'request_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'sender_id','id');
    }
}
