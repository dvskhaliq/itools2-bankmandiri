/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 **/

require("../../bootstrap");

window.Vue = require("vue");
window.events = new Vue();
window.VueSelect = require("vue-select");

window.flash = function(message) {
    window.events.$emit("flash", message);
};
window.dismissflash = function() {
    window.events.$emit("dismissflash");
};

// Vue.component(
//     "count-pending-ticket",
//     require("../../components/sidebar/CountPendingTicket.vue")
// );

Vue.component("user-profile", () =>
    import("../../components/sidebar/UserProfile.vue")
);
Vue.component("flash", () => import("../../components/FlashMessage2.vue"));
Vue.component("v-select", VueSelect.VueSelect);

import "es6-promise/auto";
import Vuex from "vuex";

import Vue from "vue";
import VeeValidate from "vee-validate";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: "errors", // change if property conflicts
    events: "input|blur",
    fieldsBagName: "fields",
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: "validations", // the nested key under which the validation messages will be located
    inject: true,
    locale: "en",
    validity: false,
    useConstraintAttrs: true
};

Vue.use(VeeValidate, config);
Vue.use(Loading);

// Vue.use(Vuex);

// const sidebar = new Vue({
//     el: "#sidebar",
//     data: {
//         countpendingticket: 0,
//     },
//     methods: {
//         initPendingTicketCount() {
//             axios.get('/side/notify/pendingticket').then(res => {
//                 this.countpendingticket = res.data.message;
//             });
//         },
//         refreshCount(req) {
//             switch (req.type) {
//                 case "pending-ticket":
//                     this.initPendingTicketCount();
//                     break;
//             }
//         }
//     },
//     created() {
//     }
// });
const userprofile = new Vue({
    el: "#userprofile",
    data: {
        selected_prop: {},
        isloaded: false,
        ismodal: false,
        alert: false,
        modalname: "",
        auth: auth
    },

    // mounted() {
    //     this.loginUser();
    // },

    methods: {
        submitUpdate(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.alert = true;
            this.ismodal = false;
            axios
                .post("/user_configuration/submit_update", reqResponse.data)
                .then(response => {
                    if (response.data.status == true) {
                        flash({
                            message: response.data.result,
                            flashtype: "info"
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                });
        }
    },
    created() {}
});
