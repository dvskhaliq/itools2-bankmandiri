<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluBds extends Model
{
    protected $table = 'pm_master_ics';
    public $timestamps = false;
}
