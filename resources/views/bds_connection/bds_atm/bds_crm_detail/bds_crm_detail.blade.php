@extends('sidebar')
@section('content')
<div id="bds_crm_detail">
    <div class="row bds-crm-detail">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS CRM Detail</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>CRM Detail</span>
                            </div>
                            <div class="panel-body">
                                <table-bds-crm-detail :data_crm_detail="data_crm_detail" v-on:crm-detail-selected="crmDetailSelected" :isloaded="isloaded"></table-bds-crm-detail>
                                <!-- <vue-pagination :pagination="pages" @paginate="getData()" :offset="4">
                                </vue-pagination> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Add CRM Detail</span>
                            </div>
                            <componen-bds-crm-add :data_crm_detail="data_crm_detail" v-on:submit-add-crm="submitAdd"></componen-bds-crm-add>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Confirm Delete</h4>
                </div>
                <div v-if="isloaded">
                    <componen-confirm-delete v-on:submit-delete="submitDelete" v-on:closewindows="closeWindow" :crm_detail_prop="crm_detail_prop" :isloaded="isloaded" :ismodal="ismodal"></componen-confirm-delete>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">CRM Detail</h4>
                </div>
                <div v-if="isloaded">
                    <componen-modal-edit v-on:closewindows="closeWindow" v-on:submit-update="submitUpdate" :crm_detail_prop="crm_detail_prop" :isloaded="isloaded" :ismodal="ismodal"></componen-modal-edit>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/bds_crm_detail.js"></script>
@endsection
