<?php

namespace App\ServiceHandler\PBMLayer\Bulk;

use App\BulkStatus;

class BulkStatusImp implements BulkStatusService
{
    public function create($payload)
    { 
        $data = new BulkStatus();
        $data->incident_number = (array_key_exists("incident_number", $payload) ? $payload["incident_number"] : null);
        $data->problem_number = (array_key_exists("problem_number", $payload) ? $payload["problem_number"] : null);
        return $data;
    }
}
