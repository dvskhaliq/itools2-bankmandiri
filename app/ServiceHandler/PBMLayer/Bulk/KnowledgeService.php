<?php

namespace App\ServiceHandler\PBMLayer\Bulk;

use App\WebServiceResponse;

interface KnowledgeService
{
    public function updateKnowledgeCategorization($payload): WebServiceResponse;
}
