<?php

namespace App\ServiceHandler\IntegrationLayer;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use App\WebServiceResponse;

class WOICTMRemedyImp implements WOICTMRemedyService
{
    private $urlWsdl =  "http://mandiriservicedesk/arsys/WSDL/public/appremedy/MANDIRI:CSI:CTM:User:Interface1:RemedyItoolsIntegration";
    private $urlWsdl_SGP = "https://mandiriservicedesk/arsys/WSDL/public/appremedy/MANDIRI:CSI:CTM:Interface1:SupportGroupPeopleAssociation";
    // private $urlWsdl = "";
    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeader($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    public function getPeopleEntityByCredential($username, $password)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeader($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'LoginId' => $username,
            );
            $client->GetPeopleEntityByLoginId($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'PersonId' => $doc->getElementsByTagName('PersonId')->item(0)->nodeValue,
                'NIP' => $doc->getElementsByTagName('NIP')->item(0) != null ? $doc->getElementsByTagName('NIP')->item(0)->nodeValue : "Not Defined",
                'FullName' => $doc->getElementsByTagName('FullName')->item(0) != null ? $doc->getElementsByTagName('FullName')->item(0)->nodeValue : "Not Defined",
                'FirstName' => $doc->getElementsByTagName('FirstName')->item(0) != null ? $doc->getElementsByTagName('FirstName')->item(0)->nodeValue : "Not Defined",
                'LastName' => $doc->getElementsByTagName('LastName')->item(0) != null ? $doc->getElementsByTagName('LastName')->item(0)->nodeValue : "Not Defined",
                'ProfileStatus' => $doc->getElementsByTagName('ProfileStatus')->item(0) != null ? $doc->getElementsByTagName('ProfileStatus')->item(0)->nodeValue : "Not Defined",
                'Company' => $doc->getElementsByTagName('Company')->item(0) != null ? $doc->getElementsByTagName('Company')->item(0)->nodeValue : "Not Defined",
                'Organization' => $doc->getElementsByTagName('Organization')->item(0) != null ? $doc->getElementsByTagName('Organization')->item(0)->nodeValue : "Not Defined",
                'SiteCountry' => $doc->getElementsByTagName('SiteCountry')->item(0) != null ? $doc->getElementsByTagName('SiteCountry')->item(0)->nodeValue : "Not Defined",
                'SiteState' => $doc->getElementsByTagName('SiteState')->item(0) != null ? $doc->getElementsByTagName('SiteState')->item(0)->nodeValue : "Not Defined",
                'SiteCity' => $doc->getElementsByTagName('SiteCity')->item(0) != null ? $doc->getElementsByTagName('SiteCity')->item(0)->nodeValue : "Not Defined",
                // 'Email' => $doc->getElementsByTagName('Email')->item(0) != null ? $doc->getElementsByTagName('')->item(0)->nodeValue : "Not Defined",
                'Region' => $doc->getElementsByTagName('Region')->item(0) != null ? $doc->getElementsByTagName('Region')->item(0)->nodeValue : "Not Defined",
                'Group' => $doc->getElementsByTagName('Group')->item(0) != null ? $doc->getElementsByTagName('Group')->item(0)->nodeValue : "Not Defined",
                'Department' => $doc->getElementsByTagName('Department')->item(0) != null ? $doc->getElementsByTagName('Department')->item(0)->nodeValue : "Not Defined",
            );
            return new WebServiceResponse('Data Fetched', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }



    public function getPeopleEntityByPersonId($username, $password, $person_id)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeader($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'PersonId' => $person_id,
            );
            $client->GetPeopleEntityByPersonId($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'PersonId' => $doc->getElementsByTagName('PersonId')->item(0)->nodeValue,
                'NIP' => $doc->getElementsByTagName('NIP')->item(0) != null ? $doc->getElementsByTagName('NIP')->item(0)->nodeValue : "Not Defined",
                'FullName' => $doc->getElementsByTagName('FullName')->item(0) != null ? $doc->getElementsByTagName('FullName')->item(0)->nodeValue : "Not Defined",
                'FirstName' => $doc->getElementsByTagName('FirstName')->item(0) != null ? $doc->getElementsByTagName('FirstName')->item(0)->nodeValue : "Not Defined",
                'LastName' => $doc->getElementsByTagName('LastName')->item(0) != null ? $doc->getElementsByTagName('LastName')->item(0)->nodeValue : "Not Defined",
                'ProfileStatus' => $doc->getElementsByTagName('ProfileStatus')->item(0) != null ? $doc->getElementsByTagName('ProfileStatus')->item(0)->nodeValue : "Not Defined",
                'Company' => $doc->getElementsByTagName('Company')->item(0) != null ? $doc->getElementsByTagName('Company')->item(0)->nodeValue : "Not Defined",
                'Organization' => $doc->getElementsByTagName('Organization')->item(0) != null ? $doc->getElementsByTagName('Organization')->item(0)->nodeValue : "Not Defined",
                'SiteCountry' => $doc->getElementsByTagName('SiteCountry')->item(0) != null ? $doc->getElementsByTagName('SiteCountry')->item(0)->nodeValue : "Not Defined",
                'SiteState' => $doc->getElementsByTagName('SiteState')->item(0) != null ? $doc->getElementsByTagName('SiteState')->item(0)->nodeValue : "Not Defined",
                'SiteCity' => $doc->getElementsByTagName('SiteCity')->item(0) != null ? $doc->getElementsByTagName('SiteCity')->item(0)->nodeValue : "Not Defined",
                // 'Email' => $doc->getElementsByTagName('Email')->item(0) != null ? $doc->getElementsByTagName('')->item(0)->nodeValue : "Not Defined",
                'Region' => $doc->getElementsByTagName('Region')->item(0) != null ? $doc->getElementsByTagName('Region')->item(0)->nodeValue : "Not Defined",
                'Group' => $doc->getElementsByTagName('Group')->item(0) != null ? $doc->getElementsByTagName('Group')->item(0)->nodeValue : "Not Defined",
                'Department' => $doc->getElementsByTagName('Department')->item(0) != null ? $doc->getElementsByTagName('Department')->item(0)->nodeValue : "Not Defined",
            );
            return new WebServiceResponse('Data Fetched', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function  getPeopleEntityByLoginId($username, $password, $remedy_login_id)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeader($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'LoginId' => $remedy_login_id,
            );
            $client->GetPeopleEntityByLoginId($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'PersonId' => $doc->getElementsByTagName('PersonId')->item(0)->nodeValue,
                'NIP' => $doc->getElementsByTagName('NIP')->item(0) != null ? $doc->getElementsByTagName('NIP')->item(0)->nodeValue : "Not Defined",
                'FullName' => $doc->getElementsByTagName('FullName')->item(0) != null ? $doc->getElementsByTagName('FullName')->item(0)->nodeValue : "Not Defined",
                'FirstName' => $doc->getElementsByTagName('FirstName')->item(0) != null ? $doc->getElementsByTagName('FirstName')->item(0)->nodeValue : "Not Defined",
                'LastName' => $doc->getElementsByTagName('LastName')->item(0) != null ? $doc->getElementsByTagName('LastName')->item(0)->nodeValue : "Not Defined",
                'ProfileStatus' => $doc->getElementsByTagName('ProfileStatus')->item(0) != null ? $doc->getElementsByTagName('ProfileStatus')->item(0)->nodeValue : "Not Defined",
                'Company' => $doc->getElementsByTagName('Company')->item(0) != null ? $doc->getElementsByTagName('Company')->item(0)->nodeValue : "Not Defined",
                'Organization' => $doc->getElementsByTagName('Organization')->item(0) != null ? $doc->getElementsByTagName('Organization')->item(0)->nodeValue : "Not Defined",
                'SiteCountry' => $doc->getElementsByTagName('SiteCountry')->item(0) != null ? $doc->getElementsByTagName('SiteCountry')->item(0)->nodeValue : "Not Defined",
                'SiteState' => $doc->getElementsByTagName('SiteState')->item(0) != null ? $doc->getElementsByTagName('SiteState')->item(0)->nodeValue : "Not Defined",
                'SiteCity' => $doc->getElementsByTagName('SiteCity')->item(0) != null ? $doc->getElementsByTagName('SiteCity')->item(0)->nodeValue : "Not Defined",
                // 'Email' => $doc->getElementsByTagName('Email')->item(0) != null ? $doc->getElementsByTagName('')->item(0)->nodeValue : "Not Defined",
                'Region' => $doc->getElementsByTagName('Region')->item(0) != null ? $doc->getElementsByTagName('Region')->item(0)->nodeValue : "Not Defined",
                'Group' => $doc->getElementsByTagName('Group')->item(0) != null ? $doc->getElementsByTagName('Group')->item(0)->nodeValue : "Not Defined",
                'Department' => $doc->getElementsByTagName('Department')->item(0) != null ? $doc->getElementsByTagName('Department')->item(0)->nodeValue : "Not Defined",
            );
            return new WebServiceResponse('Data Fetched', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function  getPeopleEntityByCorporateID($username, $password, $corporate_id)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeader($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'CorporateID' => $corporate_id,
            );
            $client->GetPeopleEntityByCorporateID($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'PersonId' => $doc->getElementsByTagName('PersonId')->item(0)->nodeValue,
                'NIP' => $doc->getElementsByTagName('NIP')->item(0) != null ? $doc->getElementsByTagName('NIP')->item(0)->nodeValue : "Not Defined",
                'FullName' => $doc->getElementsByTagName('FullName')->item(0) != null ? $doc->getElementsByTagName('FullName')->item(0)->nodeValue : "Not Defined",
                'FirstName' => $doc->getElementsByTagName('FirstName')->item(0) != null ? $doc->getElementsByTagName('FirstName')->item(0)->nodeValue : "Not Defined",
                'LastName' => $doc->getElementsByTagName('LastName')->item(0) != null ? $doc->getElementsByTagName('LastName')->item(0)->nodeValue : "Not Defined",
                'ProfileStatus' => $doc->getElementsByTagName('ProfileStatus')->item(0) != null ? $doc->getElementsByTagName('ProfileStatus')->item(0)->nodeValue : "Not Defined",
                'Company' => $doc->getElementsByTagName('Company')->item(0) != null ? $doc->getElementsByTagName('Company')->item(0)->nodeValue : "Not Defined",
                'Organization' => $doc->getElementsByTagName('Organization')->item(0) != null ? $doc->getElementsByTagName('Organization')->item(0)->nodeValue : "Not Defined",
                'SiteCountry' => $doc->getElementsByTagName('SiteCountry')->item(0) != null ? $doc->getElementsByTagName('SiteCountry')->item(0)->nodeValue : "Not Defined",
                'SiteState' => $doc->getElementsByTagName('SiteState')->item(0) != null ? $doc->getElementsByTagName('SiteState')->item(0)->nodeValue : "Not Defined",
                'SiteCity' => $doc->getElementsByTagName('SiteCity')->item(0) != null ? $doc->getElementsByTagName('SiteCity')->item(0)->nodeValue : "Not Defined",
                // 'Email' => $doc->getElementsByTagName('Email')->item(0) != null ? $doc->getElementsByTagName('')->item(0)->nodeValue : "Not Defined",
                'Region' => $doc->getElementsByTagName('Region')->item(0) != null ? $doc->getElementsByTagName('Region')->item(0)->nodeValue : "Not Defined",
                'Group' => $doc->getElementsByTagName('Group')->item(0) != null ? $doc->getElementsByTagName('Group')->item(0)->nodeValue : "Not Defined",
                'Department' => $doc->getElementsByTagName('Department')->item(0) != null ? $doc->getElementsByTagName('Department')->item(0)->nodeValue : "Not Defined",
            );
            return new WebServiceResponse('Data Fetched', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function getSupportGroupPeopleAssociationByPersonId($username, $password, $person_id, $support_group_id)
    {
        try {
            $client = new SoapClient($this->urlWsdl_SGP, $this->options());
            $headerbody = $this->generateHeader($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'PersonId' => $person_id,
                'SupportGroupId' => $support_group_id,
            );
            $client->GetSupportGroupPeopleAssociationByPersonId($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'RequestId' => $doc->getElementsByTagName('RequestId')->item(0)->nodeValue,
                'RemedyLoginId' => $doc->getElementsByTagName('RemedyLoginId')->item(0) != null ? $doc->getElementsByTagName('RemedyLoginId')->item(0)->nodeValue : "Not Defined",
                'FirstName' => $doc->getElementsByTagName('FirstName')->item(0) != null ? $doc->getElementsByTagName('FirstName')->item(0)->nodeValue : "Not Defined",
                'LastName' => $doc->getElementsByTagName('LastName')->item(0) != null ? $doc->getElementsByTagName('LastName')->item(0)->nodeValue : "Not Defined",
                'FullName' => $doc->getElementsByTagName('FullName')->item(0) != null ? $doc->getElementsByTagName('FullName')->item(0)->nodeValue : "Not Defined",
                'Company' => $doc->getElementsByTagName('Company')->item(0) != null ? $doc->getElementsByTagName('Company')->item(0)->nodeValue : "Not Defined",
                'Organization' => $doc->getElementsByTagName('Organization')->item(0) != null ? $doc->getElementsByTagName('Organization')->item(0)->nodeValue : "Not Defined",
                'SupportGroupName' => $doc->getElementsByTagName('SupportGroupName')->item(0) != null ? $doc->getElementsByTagName('SupportGroupName')->item(0)->nodeValue : "Not Defined",
                'SupportGroupId' => $doc->getElementsByTagName('SupportGroupId')->item(0) != null ? $doc->getElementsByTagName('SupportGroupId')->item(0)->nodeValue : "Not Defined",
                'EmployeeRemedyId' => $doc->getElementsByTagName('EmployeeRemedyId')->item(0) != null ? $doc->getElementsByTagName('EmployeeRemedyId')->item(0)->nodeValue : "Not Defined",
            );
            return new WebServiceResponse('Data Fetched', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function getSupportGroupPeopleAssociationByLoginId($username, $password, $login_id, $support_group_id)
    {
        try {
            $client = new SoapClient($this->urlWsdl_SGP, $this->options());
            $headerbody = $this->generateHeader($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'LoginId' => $login_id,
                'SupportGroupId' => $support_group_id,
            );
            $client->GetSupportGroupPeopleAssociationByLoginId($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'RequestId' => $doc->getElementsByTagName('RequestId')->item(0)->nodeValue,
                'RemedyLoginId' => $doc->getElementsByTagName('RemedyLoginId')->item(0) != null ? $doc->getElementsByTagName('RemedyLoginId')->item(0)->nodeValue : "Not Defined",
                'FirstName' => $doc->getElementsByTagName('FirstName')->item(0) != null ? $doc->getElementsByTagName('FirstName')->item(0)->nodeValue : "Not Defined",
                'LastName' => $doc->getElementsByTagName('LastName')->item(0) != null ? $doc->getElementsByTagName('LastName')->item(0)->nodeValue : "Not Defined",
                'FullName' => $doc->getElementsByTagName('FullName')->item(0) != null ? $doc->getElementsByTagName('FullName')->item(0)->nodeValue : "Not Defined",
                'Company' => $doc->getElementsByTagName('Company')->item(0) != null ? $doc->getElementsByTagName('Company')->item(0)->nodeValue : "Not Defined",
                'Organization' => $doc->getElementsByTagName('Organization')->item(0) != null ? $doc->getElementsByTagName('Organization')->item(0)->nodeValue : "Not Defined",
                'SupportGroupName' => $doc->getElementsByTagName('SupportGroupName')->item(0) != null ? $doc->getElementsByTagName('SupportGroupName')->item(0)->nodeValue : "Not Defined",
                'SupportGroupId' => $doc->getElementsByTagName('SupportGroupId')->item(0) != null ? $doc->getElementsByTagName('SupportGroupId')->item(0)->nodeValue : "Not Defined",
                'EmployeeRemedyId' => $doc->getElementsByTagName('EmployeeRemedyId')->item(0) != null ? $doc->getElementsByTagName('EmployeeRemedyId')->item(0)->nodeValue : "Not Defined",
            );
            return new WebServiceResponse('Data Fetched', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }
}
