<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FTPFileInventory extends Model
{
    protected $fillable = ['ftp_file_inventory'];
    protected $table = 'ftp_file_inventory';
    public $timestamps = false;

    public function ftp_category()
    {
        return $this->belongsTo(FTPCategory::class, 'ftp_category_id', 'id');
    }
}
