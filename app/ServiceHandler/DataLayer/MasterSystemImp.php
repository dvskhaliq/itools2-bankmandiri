<?php

namespace App\ServiceHandler\DataLayer;

use App\PMMasterSystemAvailability as MasterSystem;
use Illuminate\Database\Eloquent\Collection;

class MasterSystemImp implements MasterSystemService
{
    protected $system_repo;

    public function __construct(SystemAvailabilityService $system_service)
    {
        $this->system_repo = $system_service;
    }
    public function getMasterSystem(): ?Collection
    {
        $result = MasterSystem::orderBy('system_name','asc')->get();
        return $result;
    }

    public function createMasterSystem($payload)
    { 
        $mastersystem = new MasterSystem();
        $mastersystem->system_name = (array_key_exists("system_name", $payload) ? $payload["system_name"] : null);
        $mastersystem->save();
        return $mastersystem;
    }

    public function updateMasterSystem($payload)
    { 
        $mastersystem = MasterSystem::find($payload['id']);
        $mastersystem->system_name = (array_key_exists("system_name", $payload) ? $payload["system_name"] : null);
        $mastersystem->save();
        return $mastersystem;
    }

    public function deleteMasterSystem(int $id){
        $mastersystem = MasterSystem::find($id);
        $this->system_repo->deleteSystemAvail($id);
        $mastersystem->delete();
        return $mastersystem;
    }
}
