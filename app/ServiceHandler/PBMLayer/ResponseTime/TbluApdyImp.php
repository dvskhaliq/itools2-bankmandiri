<?php

namespace App\ServiceHandler\PBMLayer\ResponseTime;

use App\PMTbluApdy as TbluApdy;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\PMTbluAtm as TbluApdyKoprats;

class TbluApdyImp implements TbluApdyService
{
    public function create($payload)
    { 
        $tbluapdy = new TbluApdy();
        $tbluapdy->layanan = (array_key_exists("layanan", $payload) ? $payload["layanan"] : null);
        $tbluapdy->node = (array_key_exists("node", $payload) ? $payload["node"] : null);
        $tbluapdy->restime = (array_key_exists("restime", $payload) ? $payload["restime"] : null);
        $tbluapdy->tanggal = (array_key_exists("tanggal", $payload) ? $payload["tanggal"] : null);
        $tbluapdy->save();
        return $tbluapdy;
    }

    public function insert($array){
        return TbluApdy::insert($array);
    }

    public function getResponseTime(): ?Collection
    {
        return TbluApdy::where('received_date', '=', date('Y-m-d', strtotime('yesterday')))->orderBy('received_date','desc')
        ->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByDate($date): ?Collection
    {
        return TbluApdy::where('received_date', '=', $date)->orderBy('received_date','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByMonth($month): ?Collection
    {
        return TbluApdy::whereMonth('received_date', '=', date('m', strtotime($month)))->whereYear('received_date', '=', date('Y', strtotime($month)))
        ->orderBy('received_date','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByMonthSystem($layanan, $month): ?Collection
    {
        $result = TbluApdy::where('layanan',$layanan)
        ->whereMonth('received_date', '=', date('m', strtotime($month)))->whereYear('received_date', '=', date('Y', strtotime($month)))
        ->orderBy('received_date','asc')->get();
        return $result;
    }

    public function getResponseTimeByMonthAvg($month): ?Collection
    {
        $result = TbluApdy::select(DB::raw("layanan, ROUND(AVG(count),2) as count, ROUND(AVG(sum),2) as sum, to_char(received_date, 'YYYY-MM') as bulan"))
        ->whereMonth('received_date', '=', date('m', strtotime($month)))->whereYear('received_date', '=', date('Y', strtotime($month)))
        ->groupBy('layanan')->groupBy('bulan')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getResponseTimeByYear(array $years): ?Collection
    {
        return TbluApdy::where(function($q) use($years) {
            foreach ($years as $year) {
                $q->whereYear('received_date', '=', $year, 'or');
            }
        })->orderBy('received_date','desc')->orderBy('layanan','asc')->get();
    }

    public function getYears(){
        return DB::select("select distinct EXTRACT(YEAR from received_date) as year from pm_tblu_apdy order by EXTRACT(YEAR from received_date) desc");
    }

    public function updateResponseTime($payload)
    { 
        $responsetime = TbluApdy::find($payload['id']);
        $responsetime->count = (array_key_exists("count", $payload) ? $payload["count"] : null);
        $responsetime->sum = (array_key_exists("sum", $payload) ? $payload["sum"] : null);
        $responsetime->avg_value = (array_key_exists("avg_value", $payload) ? $payload["avg_value"] : null);
        $responsetime->save();
        $responsetime->refresh();
        return $responsetime;
    }

    public function insertKoprats($array){
        return TbluApdyKoprats::insert($array);
    }
}
