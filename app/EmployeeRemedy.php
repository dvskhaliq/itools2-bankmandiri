<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeRemedy extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['employee_remedy'];
    protected $table = 'employee_remedy';
    public $timestamps = false;
    public $keyType = 'string';

    public function user()
    {
        return $this->hasMany(User::class, 'employee_remedy_id', 'id');
    }

    public function chatsubscribeduser()
    {
        return $this->hasMany(ChatSubscribedUser::class, 'employee_remedy_id', 'id');
    }

    public function inbox()
    {
        // return $this->hasMany(Inbox::class,'EmployeeSenderId','id');
        return $this->hasMany(Inbox::class,'EmployeeSenderId','id');

    }

    public function incidentinterface(){
        return $this->hasMany(HPDIncidentInterface::class,'CustomerPersonId','id');
    }
}
