<?php

namespace App\ServiceHandler\IntegrationLayer;

use GuzzleHttp\Client;
use stdClass;

class AppdynamicsImp implements AppdynamicsService
{

    protected $client;

    public function __construct(Client $clnt)
    {
        $this->client = $clnt;
    }

    public function fetchData($rest_url, $appdy)
    {
        return $this->requestClient($rest_url, $appdy);
    }

    private function requestClient($target_url, $appdy)
    {
        try {
            //$response = $this->client->request('GET', $target_url, ['auth' => ['admin@customer1', 'mandirimandiri1']]);
	    if($appdy == "appdy_1"){
                        $client = new Client(['base_uri' => env('APPDY_BASE_URL_1')]);
                        $response = $client->request('GET', $target_url, ['auth' => ['admin@customer1', 'mandirimandiri1']]);//appdy_1
                } else{
                        $client = new Client(['base_uri' => env('APPDY_BASE_URL_2'), 'verify' => false]);
                        $response = $client->request('GET', $target_url, ['auth' => ['ifscsmdmonsvc@customer1', 'b!T8EJ6qK$5C']]);//appdy_2
                }
        } catch (\Exception $e) {
            $results = new stdClass();
            $results->startTimeInMillis = time();
            $results->occurrences = 0;
            $results->current = 0;
            $results->min = 0;
            $results->max = 0;
            $results->useRange = 0;
            $results->count = 0;
            $results->sum = 0;
            $results->value = 0;
            $results->standardDeviation = 0;
            $results->status = 'failed';
            $results->messages = 'Invalid address, unable to reach the address';
            $results->metric_name = 'undefined';
            $results->metric_path = 'undefined';
            return $results;
        }
        return $this->responseHandler($response->getBody()->getContents());
    }

    private function responseHandler($htt_response)
    {
        if ($htt_response) {
            $response = json_decode($htt_response);
            if (isset($response[0]->{'metricValues'})) {
                $values = !empty($response[0]->metricValues) ? $response[0]->metricValues[0] : [];
                if (!empty($values)) {
                    $results = new stdClass();
                    $results->startTimeInMillis = $values->startTimeInMillis;
                    $results->occurrences = $values->occurrences;
                    $results->current = $values->current;
                    $results->min = $values->min;
                    $results->max = $values->max;
                    $results->useRange = $values->useRange;
                    $results->count = $values->count;
                    $results->sum = $values->sum;
                    $results->value = $values->value;
                    $results->standardDeviation = $values->standardDeviation;
                    $results->status = 'success';
                    $results->messages = 'Origin value from apdy fetched';
                    $results->metric_name = isset($response[0]->{'metricName'}) ? $response[0]->metricName : null;
                    $results->metric_path = isset($response[0]->{'metricPath'}) ? $response[0]->metricPath : null;
                    return $results;
                } else {
                    $results = new stdClass();
                    $results->startTimeInMillis = time();
                    $results->occurrences = 0;
                    $results->current = 0;
                    $results->min = 0;
                    $results->max = 0;
                    $results->useRange = 0;
                    $results->count = 0;
                    $results->sum = 0;
                    $results->value = 0;
                    $results->standardDeviation = 0;
                    $results->status = 'failed';
                    $results->messages = 'Origin value from apdy overrided';
                    $results->metric_name = isset($response[0]->{'metricName'}) ? $response[0]->metricName : null;;
                    $results->metric_path = isset($response[0]->{'metricPath'}) ? $response[0]->metricPath : null;;
                    return $results;
                }
            } else {
                $results = new stdClass();
                $results->startTimeInMillis = time();
                $results->occurrences = 0;
                $results->current = 0;
                $results->min = 0;
                $results->max = 0;
                $results->useRange = 0;
                $results->count = 0;
                $results->sum = 0;
                $results->value = 0;
                $results->standardDeviation = 0;
                $results->status = 'failed';
                $results->messages = 'invalid structure data';
                $results->metric_name = isset($response[0]->{'metricName'}) ? $response[0]->metricName : null;
                $results->metric_path = isset($response[0]->{'metricPath'}) ? $response[0]->metricPath : null;
                return $results;
            }
        }
        $results = new stdClass();
        $results->startTimeInMillis = time();
        $results->occurrences = 0;
        $results->current = 0;
        $results->min = 0;
        $results->max = 0;
        $results->useRange = 0;
        $results->count = 0;
        $results->sum = 0;
        $results->value = 0;
        $results->standardDeviation = 0;
        $results->status = 'failed';
        $results->messages = 'Empty response from Appdynamics';
        $results->metric_name = 'undefined';
        $results->metric_path = 'undefined';
        return $results;
    }

    public function fetchDataKoprats($target_url)
    {
        try {
            $client = new Client(['base_uri' => env('APPDY_BASE_URL_2'), 'verify' => false]);
            $response =  $client->request('GET', $target_url, ['auth' => ['csmd.ifs@customer1', 'mandiriuser123']]);            
            $htt_response = $response->getBody()->getContents();
            if ($htt_response) {
                $response_decode = json_decode($htt_response);
                return $response_decode;
            }
        } catch (\Exception $e) {
            $results = new stdClass();
            $results->startTimeInMillis = time();
            $results->localStartTime = 0;
            $results->errorSummary = 'undefined';
            $results->userExperience = 'undefined';            
            return $e->__toString();
        }        
    }
}
