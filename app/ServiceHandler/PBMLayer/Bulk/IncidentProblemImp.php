<?php

namespace App\ServiceHandler\PBMLayer\Bulk;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use App\User;
use App\EmployeeRemedy;
use App\Inbox;
use App\WebServiceResponse;
use App\SupportGroupAssigneeRemedy;
use App\Incident;
use DateTime;
use Illuminate\Support\Facades\Crypt;

use function GuzzleHttp\json_encode;

class IncidentProblemImp implements IncidentProblemService
{
    public $wsdl_create_inc;
    public $wsdl_problem_relation;
    public $wsdl_update_inc;
    public $wsdl_update_problem;

    public function __construct()
    {
        $this->wsdl_create_inc = env('WSDL_CREATE_INC');
        $this->wsdl_problem_relation = env('WSDL_PROBLEM_RELATION');
        $this->wsdl_update_inc = env('WSDL_UPDATE_INC');
        $this->wsdl_update_problem = env('WSDL_UPDATE_PROBLEM');
    }
    
    public function getWsdlCreateInc(){
        return $this->wsdl_create_inc;
    }

    public function getWsdlProblemRelation(){
        return $this->wsdl_problem_relation;
    }

    public function getWsdlUpdateInc(){
        return $this->wsdl_update_inc;
    }

    public function getWsdlUpdateProblem(){
        return $this->wsdl_update_problem;
    }

    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateHeaderAPI($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    ///////////////////////////////////////////////////////////////////////HEAD//////////////////////////////////////////////////////////////

    public function createIncidentRelation($payload)
    {
        $data = array(
            'IncidentNumber' => $payload['IncidentNumber'] == null ? null : $payload['IncidentNumber'],
            'ProblemReconID' => $payload['ProblemNumber'] == null ? null : $payload['ProblemNumber'],
            'SummaryInc' => ".",
            'Description' => $payload['Description'] == null ? null : $payload['Description'],
            'RequestType' => 20000,
            'AssociationType' => 31000,
            'StatusRLM' => 8000,
            'ConsolidatedStatus' => 32000,
            'FormName01' => "PBM:Problem Investigation",
            'FormName02' => "HPD:Help Desk"
           
        );
        try {
            $client = new SoapClient($this->getWsdlCreateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
          
            $client->__setSoapHeaders($header); 
            $client->CreateIncidentRelation($data);
            $xml = $client->__getLastResponse();
           
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('AssociationsID')->item(0)->nodeValue, 'success', $doc->getElementsByTagName('AssociationsID')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function createProblemRelation($payload)
    {
        $data = array(
            'ProblemNumber' => $payload['ProblemNumber'] == null ? null : $payload['ProblemNumber'],
            'IncidentNumber' => $payload['IncidentNumber'] == null ? null : $payload['IncidentNumber'],
            'RequestType' => 9000,
            'AssociationType' => 31000,
            'FormName01' => "HPD:Help Desk",
            'FormName02' => "PBM:Problem Investigation",
            'Description' => $payload['Description'] == null ? null : $payload['Description']
           
        );
        try {
            $client = new SoapClient($this->getWsdlProblemRelation(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
          
            $client->__setSoapHeaders($header); 
            $client->CreateProblemRelation($data);
            $xml = $client->__getLastResponse();
           
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('AssociationsID')->item(0)->nodeValue, 'success', $doc->getElementsByTagName('AssociationsID')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function chekingIncident($incident_number): WebServiceResponse
    {
        $data = array('IncidentNumber' => $incident_number);
        try {
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->FetchIncidentGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('Description')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function chekingProblem($problemNumber)
    {
        $data = array('ProblemNumber' => $problemNumber);
        try {
            $client = new SoapClient($this->getWsdlUpdateProblem(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->isProblemExist($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, 'success', null, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function createIncidentGeneric($payload): WebServiceResponse
    {
        $data = array(
            'PersonId' => $payload['CustomerID'] == null ? null : $payload['CustomerID'],
            'First_Name' => $payload['CustomerFirstName'] == null ? null : $payload['CustomerFirstName'],
            'Last_Name' => $payload['CustomerLastName'] == null ? null : $payload['CustomerLastName'],
            'Service_Type' => $payload['ServiceType'] == null ? null : $payload['ServiceType'],
            'Status' => $payload['Status'] == null ? null : $payload['Status'],
            'Impact' => $payload['Impact'] == null ? null : $payload['Impact'],
            'Urgency' => $payload['Urgency'] == null ? null : $payload['Urgency'],
            'Description' => $payload['Summary'] == null ? null : $payload['Summary'],
            'Reported_Source' => $payload['ReportedSource'] == null ? null : $payload['ReportedSource'],
            'Product_Categorization_Tier_1' => $payload['ProdCatTier1'] == null ? null : $payload['ProdCatTier1'],
            'Product_Categorization_Tier_2' => $payload['ProdCatTier2'] == null ? null : $payload['ProdCatTier2'],
            'Product_Categorization_Tier_3' => $payload['ProdCatTier3'] == null ? null : $payload['ProdCatTier3'],
            'Operational_Categorization_Tier_1' => $payload['OprCatTier1'] == null ? null : $payload['OprCatTier1'],
            'Operational_Categorization_Tier_2' => $payload['OprCatTier2'] == null ? null : $payload['OprCatTier2'],
            'Operational_Categorization_Tier_3' => $payload['OprCatTier3'] == null ? null : $payload['OprCatTier3'],
            'z1D_Action' => 'CREATE',
            'Assigned_Support_Company' => $payload['AssignedSupportCompany'] == null ? null : $payload['AssignedSupportCompany'],
            'Assigned_Support_Organization' => $payload['AssignedSupportOrganization'] == null ? null : $payload['AssignedSupportOrganization'],
            'Assigned_Group' => $payload['AssignedSupportGroup'] == null ? null : $payload['AssignedSupportGroup'],
            'Assignee' => $payload['Assignee'] == null ? null : $payload['Assignee'],
            'Assigned_Group_Id' => $payload['AssignedSupportGroupID'] == null ? null : $payload['AssignedSupportGroupID'],
            'Resolution' => $payload['Resolution'] == null ? null : $payload['Resolution'],
            'Detailed_Description' => $payload['Notes'] == null ? null : $payload['Notes'],
            'Status_Reason' => $payload['StatusReason'] == null ? null : $payload['StatusReason'],
            'Itools_Request_Id' => null,
            'ServiceCI' => $payload['ServiceCI'] == null ? null : $payload['ServiceCI'],
            'ServiceCI_ReconID' => $payload['ServiceCIReconID'] == null ? null : $payload['ServiceCIReconID'],
            'Survey' => $payload['Survey'] == null ? null : $payload['Survey'],
            'TanggalEmailMasuk' => $payload['TanggalEmailMasuk'] == null ? null : $payload['TanggalEmailMasuk'],
            'Assignee_Login_Id' => $payload['AssigneeLoginId'] == "null" || $payload['AssigneeLoginId'] == null ? null : $payload['AssigneeLoginId'],
            'Assigned_To' => $payload['AssigneeLoginId'] == "null" || $payload['AssigneeLoginId'] == null ? null : $payload['AssigneeLoginId']
        );
        try {
            $client = new SoapClient($this->getWsdlCreateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
          
            $client->__setSoapHeaders($header); 
            $client->CreateIncident($data);
            $xml = $client->__getLastResponse();
           
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('Incident_Number')->item(0)->nodeValue, 'success', $doc->getElementsByTagName('Incident_Number')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function updateIncidentGeneric($payload): WebServiceResponse
    {
        if($payload['TanggalEmailMasuk'] !== null){
            $epoch = $payload['TanggalEmailMasuk'];
            $dt = new DateTime("@$epoch");
            $tanggalEmailMasuk = $dt->format('c');
        }else{
            $tanggalEmailMasuk = null;
        }
        $data = array(
            'IncidentNumber' => $payload['IncidentNumber'] == null ? null : $payload['IncidentNumber'],
            'ServiceType' => $payload['ServiceType'] == null ? null : $payload['ServiceType'],
            'Status' => $payload['Status'] == null ? null : $payload['Status'],
            'Impact' => $payload['Impact'] == null ? null : $payload['Impact'],
            'Urgency' => $payload['Urgency'] == null ? null : $payload['Urgency'],
            'Description' => $payload['Summary'] == null ? null : $payload['Summary'],
            'ProTier1' => $payload['ProdCatTier1'] == null ? null : $payload['ProdCatTier1'],
            'ProTier2' => $payload['ProdCatTier2'] == null ? null : $payload['ProdCatTier2'],
            'ProTier3' => $payload['ProdCatTier3'] == null ? null : $payload['ProdCatTier3'],
            'OprTier1' => $payload['OprCatTier1'] == null ? null : $payload['OprCatTier1'],
            'OprTier2' => $payload['OprCatTier2'] == null ? null : $payload['OprCatTier2'],
            'OprTier3' => $payload['OprCatTier3'] == null ? null : $payload['OprCatTier3'],
            'AssignedSupportCompany' => $payload['AssignedSupportCompany'] == null ? null : $payload['AssignedSupportCompany'],
            'AssignedSupportOrganization' => $payload['AssignedSupportOrganization'] == null ? null : $payload['AssignedSupportOrganization'],
            'AssignedGroup' => $payload['AssignedSupportGroup'] == null ? null : $payload['AssignedSupportGroup'],
            'Assignee' => $payload['Assignee'] == null ? null : $payload['Assignee'],
            'AssignedGroupId' => $payload['AssignedSupportGroupID'] == null ? null : $payload['AssignedSupportGroupID'],
            'DetailedDescription' => $payload['Notes'] == null ? null : $payload['Notes'],
            'ItoolsRequestId' => null,
            'Resolution' => $payload['Resolution'] == null ? null : $payload['Resolution'],
            'StatusReason' => $payload['StatusReason'] == null ? null : $payload['StatusReason'],
            'AssigneeLoginId' => $payload['AssigneeLoginID'] == null ? null : $payload['AssigneeLoginID'],
            'ServiceCI' => $payload['ServiceCI'] == null ? null : $payload['ServiceCI'],
            'ServiceCI_ReconID' => $payload['ServiceCIReconID'] == null ? null : $payload['ServiceCIReconID'],
            'Survey' => $payload['Survey'] == null ? null : $payload['Survey'],
            'TanggalEmailMasuk' => $tanggalEmailMasuk,
            'ReportedSource' => $payload['ReportedSource'] == null ? null : $payload['ReportedSource'],
        );
        try {
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->UpdateIncidentGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($payload['IncidentNumber'].' has been updated !', 'success', $doc->getElementsByTagName('IncidentNumber')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function updateProblemGeneric($payload, $username = null, $password = null)
    {
        $data = array(
            'PBINumber' => $payload['ProblemNumber'] == null ? null : $payload['ProblemNumber'],
            'SupportCompanyPbmMgr' => $payload['SupportCompanyPbmMgr'] == null ? null : $payload['SupportCompanyPbmMgr'], 
            'SupportOrganizationPbmMgr' => $payload['SupportOrganizationPbmMgr'] == null ? null : $payload['SupportOrganizationPbmMgr'], 
            'SupportGroupPbmMgr' => $payload['SupportGroupPbmMgr'] == null ? null : $payload['SupportGroupPbmMgr'], 
            'SupportGroupIDPbmMgr' => $payload['SupportGroupIDPbmMgr'] == null ? null : $payload['SupportGroupIDPbmMgr'], 
            'AssigneePbmMgr' => $payload['AssigneePbmMgr'] == null ? null : $payload['AssigneePbmMgr'], 
            'AssigneePbmMgrID' => $payload['AssigneePbmMgrID'] == null ? null : $payload['AssigneePbmMgrID'], 
            'AssigneePbmMgrPersonId' => $payload['AssigneePbmMgrPersonId'] == null ? null : $payload['AssigneePbmMgrPersonId'], 
            'ServiceCI' => $payload['ServiceCI'] == null ? null : $payload['ServiceCI'], 
            'ServiceCIReconID' => $payload['ServiceCIReconID'] == null ? null : $payload['ServiceCIReconID'], 
            'Summary' => $payload['Summary'] == null ? null : $payload['Summary'], 
            'Notes' => $payload['Notes'] == null ? null : $payload['Notes'], 
            'InvestigationDriver' => $payload['InvestigationDriver'] == null ? null : $payload['InvestigationDriver'], 
            'InvestigationJustification' => $payload['InvestigationJustification'] == null ? null : $payload['InvestigationJustification'], 
            'TargetDate' => $payload['TargetDate'] == null ? null : $payload['TargetDate'], 
            'Impact' => $payload['Impact'] == null ? null : $payload['Impact'], 
            'Urgency' => $payload['Urgency'] == null ? null : $payload['Urgency'], 
            'AssigneeSupportCompany' => $payload['AssigneeSupportCompany'] == null ? null : $payload['AssigneeSupportCompany'], 
            'AssigneeSupportOrganization' => $payload['AssigneeSupportOrganization'] == null ? null : $payload['AssigneeSupportOrganization'], 
            'AssigneeSupportGroup' => $payload['AssigneeSupportGroup'] == null ? null : $payload['AssigneeSupportGroup'], 
            'AssigneeSupportGroupID' => $payload['AssigneeSupportGroupID'] == null ? null : $payload['AssigneeSupportGroupID'], 
            'Assignee' => $payload['Assignee'] == null ? null : $payload['Assignee'], 
            'Status' => $payload['Status'] == null ? null : $payload['Status'], 
            'StatusReason' => $payload['StatusReason'] == null ? null : $payload['StatusReason'], 
            'Resolution' => $payload['Resolution'] == null ? null : $payload['Resolution'], 
            'ProdCatTier1' => $payload['ProdCatTier1'] == null ? null : $payload['ProdCatTier1'], 
            'ProdCatTier2' => $payload['ProdCatTier2'] == null ? null : $payload['ProdCatTier2'], 
            'ProdCatTier3' => $payload['ProdCatTier3'] == null ? null : $payload['ProdCatTier3'], 
            'OprCatTier1' => $payload['OprCatTier1'] == null ? null : $payload['OprCatTier1'], 
            'OprCatTier2' => $payload['OprCatTier2'] == null ? null : $payload['OprCatTier2'], 
            'OprCatTier3' => $payload['OprCatTier3'] == null ? null : $payload['OprCatTier3'], 
            'ApplicationRootCause' => $payload['ApplicationRootCause'] == null ? null : $payload['ApplicationRootCause'], 
            'JamKejadian' => $payload['JamKejadian'] == null ? null : $payload['JamKejadian'], 
            'PenyebabMasalah' => $payload['PenyebabMasalah'] == null ? null : $payload['PenyebabMasalah'], 
            'Workaround' => $payload['Workaround'] == null ? null : $payload['Workaround'], 
            'WorkaroundTime' => $payload['WorkaroundTime'] == null ? null : $payload['WorkaroundTime'], 
            'RootCause' => $payload['RootCause'] == null ? null : $payload['RootCause'], 
            'ClosedTime' => $payload['ClosedTime'] == null ? null : $payload['ClosedTime'], 
            'IR' => $payload['IR'] == null ? null : $payload['IR'], 
            'RootCauseCategory' => $payload['RootCauseCategory'] == null ? null : $payload['RootCauseCategory'], 
            'WorkaroundTerm' => $payload['WorkaroundTerm'] == null ? null : $payload['WorkaroundTerm'], 
            'PermanentTerm' => $payload['PermanentTerm'] == null ? null : $payload['PermanentTerm'], 
            'ImpactedApplication' => $payload['ImpactedApplication'] == null ? null : $payload['ImpactedApplication'], 
            'ImpactedAppCategory' => $payload['ImpactedAppCategory'] == null ? null : $payload['ImpactedAppCategory'], 
            'Category' => $payload['Category'] == null ? null : $payload['Category'], 
            'Downtime' => $payload['Downtime'] == null ? null : $payload['Downtime'], 
            'MatrixRootCause' => $payload['MatrixRootCause'] == null ? null : $payload['MatrixRootCause'], 
            'StatusWorkaround' => $payload['StatusWorkaround'] == null ? null : $payload['StatusWorkaround'], 
            'DescTableau' => $payload['DescTableau'] == null ? null : $payload['DescTableau'], 
        );
        try {
            $client = new SoapClient($this->getWsdlUpdateProblem(), $this->options());
            if($username == null || $password == null){
                $headerbody = $this->generateHeader();
            }else{
                $headerbody = $this->generateHeaderAPI($username, $password);
            }
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->UpdateProblemGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($payload['ProblemNumber'].' has been updated !', 'success', $doc->getElementsByTagName('PBINumber')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }
}
