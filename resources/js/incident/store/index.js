import Vuex from "vuex";
import Vue from "vue";
import incident from "./modules/incident";

Vue.use(Vuex); // Load Vuex to Vue

export default new Vuex.Store({
    modules: {
        incident
    }
});
