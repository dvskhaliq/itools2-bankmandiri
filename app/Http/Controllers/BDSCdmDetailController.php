<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BDSCdmDetailController extends BaseController
{
    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {

        return View::make('bds_connection/bds_atm/bds_cdm_detail/bds_cdm_detail');
    }

    public function getList()
    {
        $result = $this->bdsServerService->initBdsConnection()->table('CDMDetail')->select()->get();

        return response()->json($result);
    }

    public function findCdmDetailById($id)
    {
        $data = $this->bdsServerService->initBdsConnection()->table('CDMDetail')->select()->where('CDMGLRefNo', $id)->get();
        return $data;
    }

    public function detailSelected()
    {
        $data = $this->findCdmDetailById(request()->get('id'));
        return response()->json([
            'status' => true,
            'result' => $data[0],
        ]);
    }

    public function submitDeleted()
    {

        $this->bdsServerService->initBdsConnection()->table('CDMDetail')->where([['CDMGLRefNo',  request()->get('CDMGLRefNo')]])->delete();

        return response()->json([
            'status' => true,
            'result' => 'Delete CDMGLRefNo : ' . request()->get('CDMGLRefNo') . ' - ' . request()->get('Description') . ' success',
        ]);
    }

    public function submitUpdated()
    {
        $findCdm = $this->findCdmDetailById(request()->get('CDMGLRefNo'))->toArray();
        if (!empty($findCdm)) {
            if($findCdm[0]['Description'] == request()->get('Description')){
                return response()->json([
                    'status' => false,
                    'result' => 'CDMGLRefNo : ' . request()->get('CDMGLRefNo') . ' - ' . request()->get('Description') . ' not change',
                ]);
            }else{
                $this->bdsServerService->initBdsConnection()->table('CDMDetail')->where('CDMGLRefNo', request()->get('CDMGLRefNo'))->update(['Description' => request()->get('Description')]);

                return response()->json([
                    'status' => true,
                    'result' => 'Update CDMGLRefNo : ' . request()->get('CDMGLRefNo') . ' - ' . request()->get('Description') . ' success',
                ]);
            }
        }
    }

    public function submitAdd()
    {
        $data = array(
            'CDMGLRefNo' => request()->get('CDMGLRefNo'),
            'Description' => request()->get('Description')
        );

        $findCdm = $this->findCdmDetailById(request()->get('CDMGLRefNo'))->toArray();;
        if (empty($findCdm)) {
            $this->bdsServerService->initBdsConnection()->table('CDMDetail')->insert($data);

            return response()->json([
                'status' => true,
                'result' => 'Add CDMGLRefNo : ' . request()->get('CDMGLRefNo') . ' - ' . request()->get('Description') . ' success',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'result' => 'Add failed, CDMGLRefNo : ' . request()->get('CDMGLRefNo') . ' already exists',
            ]);
        }
    }
}
