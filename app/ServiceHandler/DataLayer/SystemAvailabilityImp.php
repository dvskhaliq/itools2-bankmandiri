<?php

namespace App\ServiceHandler\DataLayer;

use App\PMSystemAvailability as SystemAvailability;
use App\PMMasterSystemAvailability as MasterSystem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class SystemAvailabilityImp implements SystemAvailabilityService
{
    public function getSystemAvail(): ?Collection
    {
        // $result = SystemAvailability::with('header')->orderBy('recorded_at','desc')->orderBy('header.system_name','asc')->get();
        // $result->load('header');
        $result = SystemAvailability::where('recorded_at', '=', date('Y-m-d', strtotime('yesterday')))->with('header')->orderBy('recorded_at','desc')
        ->orderBy('id_system','asc')->get();
        return $result;
    }

    public function getSystemAvailByYear(array $years): ?Collection
    {
        $result = SystemAvailability::with('header')
        ->where(function($q) use($years) {
            foreach ($years as $year) {
                $q->whereYear('recorded_at', '=', $year, 'or');
            }
        })->orderBy('recorded_at','desc')->orderBy('id_system','asc')->get();
        return $result;
    }

    public function getSystemAvailByMonth($month): ?Collection
    {
        $result = SystemAvailability::with('header')
        ->whereMonth('recorded_at', '=', date('m', strtotime($month)))->whereYear('recorded_at', '=', date('Y', strtotime($month)))
        ->orderBy('recorded_at','desc')->orderBy('id_system','asc')->get();
        return $result;
    }

    public function getSystemAvailByMonthSystem($id_system, $month): ?Collection
    {
        $result = SystemAvailability::with('header')->where('id_system',$id_system)
        ->whereMonth('recorded_at', '=', date('m', strtotime($month)))->whereYear('recorded_at', '=', date('Y', strtotime($month)))
        ->orderBy('recorded_at','asc')->get();
        return $result;
    }

    public function getSystemAvailByMonthAvg($month): ?Collection
    {
        $result = SystemAvailability::select(DB::raw("id_system, SUM(system_downtime) as system_downtime, AVG(percent_system_avail) as percent_system_avail, to_char(recorded_at, 'YYYY-MM') as bulan"))
        ->whereMonth('recorded_at', '=', date('m', strtotime($month)))->whereYear('recorded_at', '=', date('Y', strtotime($month)))
        ->groupBy('id_system')->groupBy('bulan')->orderBy('id_system','asc')->get()->load('header');
        return $result;
    }

    public function getSystemAvailByDate($date): ?Collection
    {
        $result = SystemAvailability::with('header')->where('recorded_at', '=', $date)->orderBy('recorded_at','desc')->orderBy('id_system','asc')->get();
        return $result;
    }

    public function getSystemName(){
        return MasterSystem::orderBy('system_name')->get();
    }

    public function createSystemAvail($payload)
    { 
        $systemavail = new SystemAvailability();
        $systemavail->id_system = (array_key_exists("id_system", $payload) ? $payload["id_system"] : null);
        $systemavail->system_downtime = (array_key_exists("system_downtime", $payload) ? $payload["system_downtime"] : null);
        $systemavail->percent_system_avail = (array_key_exists("percent_system_avail", $payload) ? $payload["percent_system_avail"] : null);
        $systemavail->description = (array_key_exists("description", $payload) ? $payload["description"] : null);
        $systemavail->recorded_at = (array_key_exists("recorded_at", $payload) ? $payload["recorded_at"] : null);
        $systemavail->save();
        $systemavail->load('header')->refresh();
        return $systemavail;
    }

    public function updateSystemAvail($payload)
    { 
        $systemavail = SystemAvailability::find($payload['id']);
        $systemavail->id_system = (array_key_exists("id_system", $payload) ? $payload["id_system"] : null);
        $systemavail->system_downtime = (array_key_exists("system_downtime", $payload) ? $payload["system_downtime"] : null);
        $systemavail->percent_system_avail = (array_key_exists("percent_system_avail", $payload) ? $payload["percent_system_avail"] : null);
        $systemavail->description = (array_key_exists("description", $payload) ? $payload["description"] : null);
        $systemavail->recorded_at = (array_key_exists("recorded_at", $payload) ? $payload["recorded_at"] : null);
        $systemavail->save();
        $systemavail->load('header')->refresh();
        return $systemavail;
    }

    public function updateDescSystemAvail($payload)
    { 
        $systemavail = SystemAvailability::find($payload['id']);
        $systemavail->description = (array_key_exists("description", $payload) ? $payload["description"] : null);
        $systemavail->save();
        $systemavail->load('header')->refresh();
        return $systemavail;
    }

    public function deleteSystemAvail(int $id){
        $data = SystemAvailability::where('id_system', $id)->first();
        $data->delete();
        return $data;
    }

    public function bulkInsert($id_system){
        $year_start = date('Y-m-d', strtotime('first day of january this year'));
        $year_end = date('Y-m-d', strtotime('last day of december this year'));

        return DB::insert("insert into pm_system_availability(recorded_at,id_system,percent_system_avail,system_downtime) select i :: DATE AS recorded_at, $id_system AS id_system, 1 AS percent_system_avail, 0 AS system_downtime FROM generate_series ( '$year_start', '$year_end', '1 day' :: INTERVAL ) i");
        
        // return DB::insert("insert into pm_system_availability(recorded_at,id_system,percent_system_avail,system_downtime) select i :: DATE AS recorded_at, :system_id AS id_system, 1 AS percent_system_avail, 0 AS system_downtime FROM generate_series ( '2019-01-01', :currentdate, '1 day' :: INTERVAL ) i",['system_id'=>$id_system, 'currentdate'=>date('2020-12-31')]);
    }

    public function getYears(){
        return DB::select("select distinct EXTRACT(YEAR from recorded_at) as year from pm_system_availability order by EXTRACT(YEAR from recorded_at) desc");
    }
}
