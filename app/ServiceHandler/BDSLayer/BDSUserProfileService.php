<?php

namespace App\ServiceHandler\BDSLayer;

interface BDSUserProfileService
{
    public function getList();

    public function listHolder();

    public function findById($userId);

    public function updateUserProfile($userId, $dataUpdate);

    public function findTransLimit($levelId);

    public function findByLevelId($levelId);

    public function findHeadTeller();

}
