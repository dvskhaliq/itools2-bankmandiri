import ENUM from "../enum";

const state = {
    years: [],
    successrate: [],
    successrateavg: [],
    simpleHeader: [
        { text: "Layanan", value: "layanan"},
        { text: "Success", value: "sukses"},
        { text: "Total", value: "total"},
        { text: "Success Rate", value: "success_rate"},
        { text: "Tanggal", value: "tanggal"},
    ],
    simpleHeaderAvg: [
        { text: "Layanan", value: "layanan"},
        { text: "SUM Success", value: "sukses"},
        { text: "SUM Total", value: "total"},
        { text: "AVG Success Rate", value: "success_rate"},
    ],
    apiState: ENUM.INIT,
    alertMessage: "",
};

const actions = {
    async initYears({ commit }) {
      let response;
      try {
        response = await axios.get("/data_management/problems/success_rate/fetchYears");
        commit("setYears", response.data.data);        
      } catch (error) {
          error.response.status == 403
              ? commit("setWarning", error.response.data.error)
              : commit("setException", "Something went wrong on fetch!");
      }
    },
    async initSuccessRate({ commit }) {
      commit("setApiState", ENUM.LOADING);
      let response;
      try {
        response = await axios.get("/data_management/problems/success_rate/fetch");
        commit("setSuccessRate", response.data.data);        
      } catch (error) {
          error.response.status == 403
              ? commit("setWarning", error.response.data.error)
              : commit("setException", "Something went wrong on fetch!");
      }
    },
    async populateSuccessRateByDate({ commit }, payload) {
      commit("setApiState", ENUM.LOADING);
      let response;
      try {
            response = await axios.post(
              "/data_management/problems/success_rate/getByDate", payload
            );
          commit("setSuccessRate", response.data.data);  
      } catch (error) {
          error.response.status == 403
              ? commit("setWarning", error.response.data.error)
              : commit("setException", "Something went wrong on fetch!");
      }
    },
    async populateSuccessRateByMonth({ commit }, payload) {
      commit("setApiState", ENUM.LOADING);
      let response;
      try {
            response = await axios.post(
              "/data_management/problems/success_rate/getByMonth", payload
            );
          commit("setSuccessRate", response.data.data);  
      } catch (error) {
          error.response.status == 403
              ? commit("setWarning", error.response.data.error)
              : commit("setException", "Something went wrong on fetch!");
      }
    },
    async populateSuccessRateByMonthAvg({ commit }, payload) {
      commit("setApiState", ENUM.LOADING);
      let response;
      try {
            response = await axios.post(
              "/data_management/problems/success_rate/getByMonthAvg", payload
            );
          commit("setSuccessRateAvg", response.data.data);  
      } catch (error) {
          error.response.status == 403
              ? commit("setWarning", error.response.data.error)
              : commit("setException", "Something went wrong on fetch!");
      }
    },
    async populateSuccessRateByYear({ commit }, payload) {
      commit("setApiState", ENUM.LOADING);
      let response;
      try {
              response = await axios.post(
              "/data_management/problems/success_rate/getByYear", payload
            );
          commit("setSuccessRate", response.data.data);  
      } catch (error) {
          error.response.status == 403
              ? commit("setWarning", error.response.data.error)
              : commit("setException", "Something went wrong on fetch!");
      }
    },
    resetSuccessRate({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setSuccessRate", []);
    },
    resetSuccessRateAvg({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setSuccessRateAvg", []);
    },
    resetYears({ commit }) {
        commit("setYears", []);
    }
};

const mutations = {
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setYears: (state, years) => {
        state.years = years;
    },
    setSuccessRate: (state, successrate) => {
        state.apiState = ENUM.LOADED;
        state.successrate = successrate;
    },
    setSuccessRateAvg: (state, successrateavg) => {
        state.apiState = ENUM.LOADED;
        state.successrateavg = successrateavg;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

const getters = {
    getSuccessRateAvg: state => state.successrateavg,
    getSuccessRate: state => state.successrate,
    getSimpleHeader: state => state.simpleHeader,
    getSimpleHeaderAvg: state => state.simpleHeaderAvg,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getYears: state => state.years,
};

export default{
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}