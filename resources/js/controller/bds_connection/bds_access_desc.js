import "../../bootstrap";
import "vuetify/dist/vuetify.min.css";
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from "vuex";
import VeeValidate from "vee-validate";
import Vue from "vue";
import Loading from "vue-loading-overlay";
import Vuetify from "vuetify";

window.Vue = Vue;
window.VueSelect = require("vue-select");
window.events = new Vue();

window.flash = function(message) {
    window.events.$emit("flash", message);
};

Vue.component("table-bds-access-desc", () =>
    import("../../components/bds_connection/access_desc/BDSAccessDesc.vue")
);
Vue.component("componen-form", () =>
    import("../../components/bds_connection/access_desc/SetBDSAccessDesc.vue")
);
Vue.component("componen-list-kotran", () =>
    import("../../components/bds_connection/access_desc/ListKotran.vue")
);
Vue.component("componen-confirm-delete", () =>
    import("../../components/bds_connection/access_desc/ConfirmDelete.vue")
);
Vue.component("flash", () => import("../../components/FlashMessage2.vue"));
Vue.component("v-select", VueSelect.VueSelect);
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Loading);

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: "errors", // change if property conflicts
    events: "input|blur",
    fieldsBagName: "fields",
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: "validations", // the nested key under which the validation messages will be located
    inject: true,
    locale: "en",
    validity: false,
    useConstraintAttrs: true
};
Vue.use(VeeValidate, config);

const vuetify = new Vuetify();

const bds_access_desc = new Vue({
    el: "#bds_access_desc",
    vuetify,
    data: {
        head_teller: null,
        data_access_desc: null,
        list_access_prop: null,
        level_id_selected: null,
        ismodal: false,
        isloaded: false,
        data_list_kotran: null
    },

    mounted() {
        this.getData();
        this.getHeadTeller();
        this.getDataListKotran();
    },

    methods: {
        getData() {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.ismodal = true;
            axios
                .get(`/access_desc/list`)
                .then(response => {
                    this.data_access_desc = response.data;
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.setFlashDanger(error.response.data.message);
                    }
                    loader.hide();
                });
        },

        getHeadTeller() {
            this.ismodal = true;
            axios
                .get(`/user_profile/get_head_teller`)
                .then(response => {
                    this.head_teller = response.data.results.LevelIDTeller;
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.setFlashDanger(error.response.data.message);
                    }
                });
        },

        getDataListKotran() {
            this.ismodal = true;
            axios
                .get(`/access_desc/list_kotran`)
                .then(response => {
                    this.data_list_kotran = response.data;
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.setFlashDanger(error.response.data.message);
                    }
                });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
        },

        setFlashDanger(reqResponse) {
            flash({
                id: "alert-danger",
                message: reqResponse.message,
                flashtype: "danger"
            });
        },

        setFlashInfo(reqResponse) {
            flash({
                id: "alert-info",
                message: reqResponse.message,
                flashtype: "info"
            });
        },

        deleteSelected(reqResponse) {
            this.ismodal = true;
            this.isloaded = true;
            this.list_access_prop = reqResponse.data;
        },

        levelIdSelected(reqResponse) {
            this.level_id_selected = reqResponse.levelId;
        },

        submitDelete(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.ismodal = true;
            axios
                .post("/access_desc/delete_access_desc", reqResponse.data)
                .then(response => {
                    this.getData();
                    this.getDataListKotran();
                    if (response.data.status == true) {
                        this.setFlashInfo({
                            message: response.data.result
                        });
                    } else {
                        this.setFlashDanger({
                            message: response.data.result
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.setFlashDanger(error.response.data.message);
                    }
                    loader.hide();
                });
        },

        submitForm(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.ismodal = true;
            axios
                .post("/access_desc/insert_access_desc", reqResponse)
                .then(response => {
                    this.getData();
                    this.getDataListKotran();
                    if (response.data.status == true) {
                        this.setFlashInfo({
                            message: response.data.result
                        });
                    } else {
                        this.setFlashDanger({
                            message: response.data.result
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.setFlashDanger(error.response.data.message);
                    }
                    loader.hide();
                });
        }
    }
});
