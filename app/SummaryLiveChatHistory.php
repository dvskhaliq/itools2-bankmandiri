<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SummaryLiveChatHistory extends Model
{
    public $primaryKey = 'id';	
    protected $table = 'summary_live_chat_history';
    public $timestamps = false;    
    
}