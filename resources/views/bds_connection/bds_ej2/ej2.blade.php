@extends('sidebar')
@section('content')
<div id="bds_ej2">
    <div class="row bds-ej2">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS EJ2</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <div class="panel-body">
                    <flash v-if="ismodal" info=""></flash>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>EJ2</span>
                            </div>
                            <div class="panel-body">
                                <table-bds-ej2></table-bds-ej2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="../../js/bds_ej2.js"></script>
@endsection
