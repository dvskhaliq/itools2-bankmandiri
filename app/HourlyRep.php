<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HourlyRep extends Model
{
    protected $connection = 'mysql_edc_ussd';
    protected $table = 'hourly_rep';
    protected $primaryKey = 'id';
}
