<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCatalog extends Model
{

    protected $fillable = ['service_catalog'];
    protected $table = 'service_catalog';
    public $timestamps = false;

    public function inbox(){
        return $this->hasMany(Inbox::class,'ServiceCatalogId','id');
    }

    public function servicechannel(){
        return $this->belongsTo(ServiceChannel::class,'service_channel_name','encoding_name');
    }
    
}