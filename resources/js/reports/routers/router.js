import VueRouter from "vue-router";
import Console from "../Index";

const appUrl = "/data_management/console";
const routes = [
    {
        path: appUrl + "/success_rate",
        name: "success_rate",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/users",
        name: "users",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/dashboard",
        name: "dashboard",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/dashboard_it",
        name: "dashboard_it",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/production_issues",
        name: "production_issues",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/system_availability",
        name: "system_availability",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/response_time",
        name: "response_time",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/roles",
        name: "roles",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/create_relation",
        name: "create_relation",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/create_incident",
        name: "create_incident",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/update_incident",
        name: "update_incident",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/update_problem",
        name: "update_problem",
        component: Console,
        meta: {
            auth: true
        }
    },
    {
        path: appUrl + "/migrate_loginid",
        name: "migrate_loginid",
        component: Console,
        meta : {
            auth: true
        }
    },
    {
        path: appUrl + "/update_knowledge",
        name: "update_knowledge",
        component: Console,
        meta : {
            auth: true
        }
    }
    
];
const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});
export default router;
