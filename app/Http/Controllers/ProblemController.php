<?php

namespace App\Http\Controllers;

use App\ServiceHandler\DataLayer\ProblemService;
use App\ServiceHandler\DataLayer\ProductionIssuesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ProblemController extends BaseController
{
    protected $problem_repo;
    protected $pi_repo;

    public function __construct(ProblemService $problem_srv, ProductionIssuesService $pi_service)
    {
        $this->problem_repo = $problem_srv;
        $this->pi_repo = $pi_service;
        // $this->middleware('auth:problem');
    }

    public function index()
    {
        // return View::make('report/public/reporting_problem');
        return redirect('/data_management/console/dashboard');
    }

    public function populateProblemProductionIssues(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        $data = $this->problem_repo->tranformToProductionIssues($selected_year);
        $total = $this->pi_repo->getTotalByYear();
        $biller = $this->pi_repo->getProductionIssuesBiller();
        return response()->json([
            'status' => true,
            'data' => $data,
            'total' => $total,
            'biller' => $biller
        ], 200);
    }

    public function fetchProblemProductionIssues()
    {
        $data = $this->pi_repo->getProductionIssues();
        $total = $this->pi_repo->getTotalByYear();
        $biller = $this->pi_repo->getProductionIssuesBiller();
        return response()->json([
            'status' => true,
            'data' => $data,
            'total' => $total,
            'biller' => $biller
        ], 200);
    }

    public function test()
    {
        $data = $this->problem_repo->tranformToProductionIssues([2018]);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function getSummary($report_type, Request $request)
    {
        $category = collect($request->input('category'))->map(function ($item) {
            return $item['id'];
        })->toArray();
        switch ($report_type) {
            case 'Timeseries':

                $data = $this->getByYear($request->input('years'), $category);
                return response()->json([
                    'labels' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    'datasets' => [$data],
                ], 200);
                break;
            case 'YoY':
                $data = array();
                foreach ($request->input('years') as $value) {
                    $temp = $this->getByYear($value, $category);
                    array_push($data, $temp);
                }
                return response()->json([
                    'labels' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    'datasets' => $data,
                ], 200);
                break;
        }
    }

    private function getByYear($year, $category)
    {
        $data = $this->problem_repo->getSummary($year, $category);
        if ($data->count() != 12) {
            $temp = $data->mapWithKeys(function ($item) {
                return [$item->month_created => $item->total];
            })->toArray();
            $prop['data'] = array();
            for ($i = 1; $i <= 12; $i++) {
                if (array_key_exists($i, $temp)) {
                    array_push($prop['data'], $temp[$i]);
                } else {
                    array_push($prop['data'], 0);
                }
            }
        } else {
            $prop['data'] = $data->map(function ($item) {
                return $item->total;
            });
        }
        $prop['label'] = $year;
        $prop['fill'] = false;
        $prop['backgroundColor'] = $this->shuffleColor();
        $prop['borderColor'] = $this->shuffleColor();
        return $prop;
    }

    private function shuffleColor()
    {
        $colors = collect(['red', 'blue', 'green', 'yellow', 'black', 'pink', 'magenta', 'cyan']);
        $randomColor = $colors->shuffle();
        return $randomColor[0];
    }
}
