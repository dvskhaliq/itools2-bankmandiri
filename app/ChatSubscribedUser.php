<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatSubscribedUser extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['chat_subscribed_user'];
    public $timestamps = false;

    // public function chatliverequests(){
    //     return $this->hasMany(ChatLiveRequests::class,'user_id','id');
    // }

    // public function inbox(){
    //     return $this->belongsTo(Inbox::class,'live_chat_request_id','id');
    // }

    public function inbox()
    {
        return $this->hasMany(Inbox::class, 'TelegramSenderId', 'id');
    }

    public function employeeremedy()
    {
        return $this->belongsTo(EmployeeRemedy::class, 'employee_remedy_id', 'id');
    }
}
