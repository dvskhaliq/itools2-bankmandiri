import '../../bootstrap';
import 'vuetify/dist/vuetify.min.css';
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from 'vuex';
import VeeValidate from 'vee-validate';
import Vue from 'vue';
import Loading from "vue-loading-overlay";
import Vuetify from 'vuetify';

window.Vue = Vue;
window.VueSelect = require('vue-select');
window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('table-bds-user-profile', ()=> import('../../components/bds_connection/user_profile/BDSUserProfile.vue'));
Vue.component('component-set-user-profile', ()=> import('../../components/bds_connection/user_profile/SetBDSUserProfile.vue'));
Vue.component('component-tran-limit', ()=> import('../../components/bds_connection/user_profile/TranLimit.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));
Vue.component("v-select", VueSelect.VueSelect);
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Loading);

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: 'errors', // change if property conflicts
    events: 'input|blur',
    fieldsBagName: 'fields',
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: 'validations', // the nested key under which the validation messages will be located
    inject: true,
    locale: 'en',
    validity: false,
    useConstraintAttrs: true
  };

  Vue.use(VeeValidate, config);

  const vuetify = new Vuetify();

const bds_user_profile = new Vue({
    el: '#bds_user_profile',
    vuetify,
    data: {
        data_user_profile: null,
        ismodal: false,
        alert: false,
        isloaded: false,
        user_profile_prop: {},
        tran_limit_props: {},
    },

    mounted() {
        this.getData();
    },

    methods: {
        getData() {
          let loader = this.$loading.show({
            loader: "dots"
          });
            this.isloaded = false;
            this.alert = true;
            axios.get(`/user_profile/list`)
                .then((response) => {
                    this.data_user_profile = response.data;
                    loader.hide();
                }).catch(error => {
                    if (error.response.status === 419 || error.response.status === 401) {
                      window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                      location.reload(false);
                    } else {
                      this.setFlashDanger({
                        message: error.response.data.message
                      });
                    }
                    loader.hide();
                  });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
        },

        setFlashDanger(reqResponse) {
            this.alert= true;
            flash({
                id: "alert-danger",
                message: reqResponse.message,
                flashtype: "danger"
            });
        },

        setFlashInfo(reqResponse) {
            this.alert= true;
            flash({
                id: "alert-info",
                message: reqResponse.message,
                flashtype: "info"
            });
        },

        tranLimitSelected(reqResponse) {
            this.ismodal = true;
            axios.post('/user_profile/tran_limit_selected', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.tran_limit_props = response.data.result;
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        userProfileSelected(reqResponse) {
            this.ismodal = true;
            axios.post('/user_profile/selected', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.user_profile_prop = response.data.result[0];
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        setSignOff(reqResponse) {
            axios.post('/user_profile/set_sign_off', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.getData();
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        setMonetaryUser(reqResponse) {
            axios.post('/user_profile/set_monetary_user', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.getData();
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        setCTT(reqResponse) {
            axios.post('/user_profile/set_ctt', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.getData();
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        setLineStatus(reqResponse) {
            axios.post('/user_profile/set_line_status', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.getData();
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        searchData(reqResponse) {
            axios.get(`/user_profile/search?search=` + reqResponse.search)
                .then((response) => {
                    if (response.data == '') {                        
                        flash({
                            id: "warning",
                            message: "No Data Available in Table (click here to remove this message)",
                            flashtype: "danger"
                        });
                    } else {
                        this.data_user_profile = response.data;
                    }
                    
                }).catch(error => {
                    if (error.response.status === 419 || error.response.status === 401) {
                      window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                      location.reload(false);
                    } else {
                      this.setFlashDanger({
                        message: error.response.data.message
                      });
                    }
                  });
        }

    },

});
