<?php

namespace App\ServiceHandler\PBMLayer\ResponseTime;
use Illuminate\Database\Eloquent\Collection;

interface TbluEdcService
{
    public function getResponseTime(): ?Collection;
    public function getResponseTimeByDate($date): ?Collection;
    public function getResponseTimeByMonth($month): ?Collection;
    public function getResponseTimeByMonthSystem($layanan, $month): ?Collection;
    public function getResponseTimeByMonthAvg($month): ?Collection;
    public function getResponseTimeByYear(array $years): ?Collection;
    public function getYears();
}
