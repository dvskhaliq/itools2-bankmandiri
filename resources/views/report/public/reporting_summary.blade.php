@extends('master')
@section('sidebar')
<div class="row" id="report_summary">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div>
                <div class="panel-body" style="padding:2px">
                    <div class="col-md-12">
                        <div class="col-xs-12">
                            <div class="panel panel-default" style="margin-bottom:0px;">
                                <div class="panel-heading">
                                    <h4>Incoming Request</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <chart-table-2 :data="channeldata"></chart-table-2>
                                    </div>
                                    <div class="col-md-6">
                                        <chart-table-1 :title="'Layanan IT'" :data="categorydata.LAYANAN_IT"></chart-table-1>
                                        <chart-table-1 :title="'Layanan Cabang'" :data="categorydata.LAYANAN_CABANG"></chart-table-1>
                                        <chart-table-1 :title="'Layanan Kartu Kredit'" :data="categorydata.LAYANAN_CC"></chart-table-1>
                                        <chart-table-3 :data="typedata" :channel="topfivechannel"></chart-table-3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>Chart </h4>
                                </div>
                                <div class="panel-body">
                                    <br /><br /><br /><br />
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="../../../js/summary_report.js"></script>
@endsection