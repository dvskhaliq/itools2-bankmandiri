<?php

namespace App\ServiceHandler\IntegrationLayer;

use App\Workorder;
use App\WebServiceResponse;

interface WOIWorkOrderUpdateService
{
    public function updateWOGeneric(Workorder $workorder, $remedy_login_id = '', $remedy_password = ''): WebServiceResponse;

    public function updateStatusWO(Workorder $workorder): WebServiceResponse; //Kayanya ga kepake

    public function updateAssigneeWO(Workorder $workorder): WebServiceResponse; //Kayanya ga kepake

    ///////////////////////////////////////////// Below Code is Before Assessment ////////////////////////////////////////////////////

    public function updateStatus($request, $status,$assignee_id);

    public function updateAssignee($request, $assignee_id);

    public function updateAssigneeOtherGroup($request, $assignee);
}
