<?php

namespace App\ServiceHandler\DataLayer;

use App\WhatsappDispatcherMessages;
use App\Http\Resources\WhatsappDispatcherMessages as WADispatcherMsgResource;
use App\SummaryWhatsappDispatcher;

interface WhatsappDispatcherMessagesService
{
    public function addDispatcherMessage($array_wa_dispatcher): WADispatcherMsgResource;

    public function deleteDispatcherMessage($message_id);

    public function getDispatcherMessage($array_clause);

    public function getDispatcherMessageList($request_id);

    public function getDispathcerMessageHistory($request_id);
}
