<?php

namespace App\ServiceHandler;

use Illuminate\Support\Facades\DB;
use Auth;
use App\SupportGroupAssigneeRemedy;
use App\ChatSubscribedUser;
use App\EmployeeRemedy;
use App\User;

class UserSupportImp implements UserSupportService
{

    //Class not used
    // public function getSupportGroupUserByUserId($user_id) //will be replace later due move to SupportUserService
    // {
    //     return SupportGroupAssigneeRemedy::with('user')->whereHas('user', function ($q) use ($user_id) {
    //         $q->where('id', $user_id);
    //     })->first();
    // }

    // public function getSupportGroupUser($employee_remedy_id, $support_group_id) //will be replace later due move to SupportUserService
    // {
    //     return SupportGroupAssigneeRemedy::with('user')->where('employee_remedy_id', $employee_remedy_id)->where('support_group_id', $support_group_id)->first();
    // }

    // public function getUserSupportGroup($remedy_login_id) //will be replace due moved to UserService
    // {
    //     return User::with('supportgroupassigneeremedy')->where('un_remedy', $remedy_login_id)->first();
    // }

    //////////////////////////////////////////////////////// Below Code is before Assessment ////////////////////////////////////////////
    // public function getUser($user_id)//Not Used
    // {
    //     return User::find($user_id);
    // }

    // public function getEmployeeRemedy($employee_id)//Not Used
    // {
    //     return EmployeeRemedy::find($employee_id);
    // }

    // public function getUserEmployee($user_id)//Not Used
    // {
    //     return User::with('employeeremedy')->find($user_id);
    // }

    // public function getUserEmployeeRemedy($user_id)//Not Used
    // {
    //     return User::with('employeeremedy')->find($user_id);
    // }

    // public function getSubscribedUserEmployeeRemedy($subsricbed_user_id)//Not Used
    // {
    //     return ChatSubscribedUser::with('employeeremedy')->find($subsricbed_user_id);
    // }

    // public function getSubscribedUser($subsricbed_user_id)//Not Used
    // {
    //     return ChatSubscribedUser::find($subsricbed_user_id);
    // }

    // public function getUserEmployeeRemedySupportGroup($user_id)//Not Used
    // {
    //     return User::with('employeeremedy', 'supportgroupassigneeremedy')->find($user_id);
    // }

    // public function getUserByEmployeeAndGroup($employee_id, $support_group_id)//Not Used
    // {
    //     return User::with('supportgroupassigneeremedy', 'employeeremedy')->where('employee_remedy_id', $employee_id)->where('sgn_id_remedy', $support_group_id)->first();
    // }

    // public function getUserByFullNameAndGroupName($assignee)//Pindah ke SupportUserImp
    // {
    //     if ($assignee['assignee_fullname'] == null) {
    //         return SupportGroupAssigneeRemedy::where('company', $assignee['assignee_company'])
    //             ->where('organization', $assignee['assignee_organization'])
    //             ->where('support_group_name', $assignee['assignee_group'])
    //             ->first();
    //     } else {
    //         return SupportGroupAssigneeRemedy::where('company', $assignee['assignee_company'])
    //             ->where('organization', $assignee['assignee_organization'])
    //             ->where('support_group_name', $assignee['assignee_group'])
    //             ->where('full_name', $assignee['assignee_fullname'])
    //             ->first();
    //     }
    // }
}
