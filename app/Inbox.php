<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{

    use \Awobaz\Compoships\Compoships;

    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['inbox'];
    protected $table = 'inbox';
    public $timestamps = false;
    public $keyType = 'string';


    public function whatsappdispacthermessages()
    {
        return $this->hasMany(WhatsappDispatcherMessages::class, 'request_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'AssigneeId', 'id');
    }

    public function chatsubscribeduser()
    {
        return $this->belongsTo(ChatSubscribedUser::class, 'TelegramSenderId', 'id');
    }

    public function servicecatalog()
    {
        return $this->belongsTo(ServiceCatalog::class, 'ServiceCatalogId', 'id');
    }

    public function employeeremedy()
    {
        return $this->belongsTo(EmployeeRemedy::class, 'EmployeeSenderId', 'id');
    }

    public function chatlivemessages()
    {
        return $this->hasMany(ChatLiveMessages::class, 'request_id', 'id');
    }

    public function inboxworkloginfo()
    {
        return $this->hasMany(InboxWorkinfolog::class, 'request_id', 'id');
    }

    public function inbox()
    {
        return $this->hasMany(ChatSubscribedUser::class, 'live_chat_request_id', 'id');
    }

    public function supportgroupassigneeremedy()
    {
        return $this->belongsTo(SupportGroupAssigneeRemedy::class, ['EmployeeAssigneeId', 'EmployeeAssigneeGroupId'], ['employee_remedy_id', 'support_group_id']);
    }
}
