<?php

namespace App\ServiceHandler\DataLayer;

use App\ChatSubscribedUser as TelegramUser;
use App\Inbox;

interface TelegramUserService
{
    public function find(Int $telegram_id): TelegramUser;    

    public function hasActiveChat(String $person_id): bool;

    public function updateToChat(Inbox $request): TelegramUser;

    public function updateToMenu(Inbox $request): TelegramUser;
}
