<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMRolesFeatures extends Model
{
    protected $table = 'pm_roles_features';

    public function pmroles(){
        return $this->belongsTo(PMRoles::class,'role_id');
    }

    public function pmfeatures(){
        return $this->belongsTo(PMFeatures::class,'feature_id');
    }
}
