<?php

namespace App\ServiceHandler\ReportLayer;

use App\ServiceHandler\DataLayer\InboxService;
use App\SummaryTotalRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ReportImp implements ReportService
{
    protected $inbox_ds;

    public function __construct(InboxService $inbox_service)
    {
        $this->inbox_ds = $inbox_service;
    }

    // Dashboard Reporting
    public function generateSummaryTotal()
    {
        $data_summary = SummaryTotalRequest::select(
            'process_status',
            DB::raw('sum(total_request) total_request'),
            DB::raw('sum(layanan_it) layanan_it'),
            DB::raw('sum(layanan_cabang) layanan_cabang'),
            DB::raw('sum(layanan_cc) layanan_cc'),
            DB::raw('sum(channel_call) channel_call'),
            DB::raw('sum(channel_email) channel_email'),
            DB::raw('sum(channel_livechat) channel_livechat'),
            DB::raw('sum(channel_sms) channel_sms'),
            DB::raw('sum(channel_itsrm) channel_itsrm'),
            DB::raw('sum(channel_telegram) channel_telegram'),
            DB::raw('sum(channel_whatsapp) channel_whatsapp')
        )
            ->where('received_date', date("Y-m-d", strtotime("today")))
            ->groupBy('process_status')
            ->get();

        $data_summary_type = SummaryTotalRequest::select(
            'catalog_type',
            DB::raw('sum(total_request) total')
        )
            ->where('received_date', date("Y-m-d", strtotime("today")))
            ->groupBy('catalog_type')
            ->get();

        $data_grand_total = ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0];
        $data_category = [
            'LAYANAN_IT' => ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'LAYANAN_CABANG' => ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'LAYANAN_CC' =>  ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0]
        ];
        $data_channel = [
            'CALL' => ['NAME' => 'CALL', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'EMAIL' => ['NAME' => 'EMAIL', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'LIVECHAT' => ['NAME' => 'LIVECHAT', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'SMS' => ['NAME' => 'SMS', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'ITSRM' => ['NAME' => 'ITSRM', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'TELEGRAM' => ['NAME' => 'TELEGRAM', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'WHATSAPP' => ['NAME' => 'WHATSAPP', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0]
        ];

        foreach ($data_summary as $data) {
            if (empty($data_grand_total[$data->process_status])) {
                $data_grand_total[$data->process_status] = (int) $data->layanan_it + $data->layanan_cabang + $data->layanan_cc;
            }
            if (empty($data_category['LAYANAN_IT'][$data->process_status] && $data->layanan_it != null)) {
                $data_category['LAYANAN_IT'][$data->process_status] = (int) $data->layanan_it;
            }
            if (empty($data_category['LAYANAN_CABANG'][$data->process_status]) && $data->layanan_cabang != null) {
                $data_category['LAYANAN_CABANG'][$data->process_status] = (int) $data->layanan_cabang;
            }
            if (empty($data_category['LAYANAN_CC'][$data->process_status]) && $data->layanan_cc != null) {
                $data_category['LAYANAN_CC'][$data->process_status] = (int) $data->layanan_cc;
            }
            if (empty($data_channel['CALL'][$data->process_status]) && $data->channel_call != null) {
                $data_channel['CALL'][$data->process_status] = (int) $data->channel_call;
            }
            if (empty($data_channel['EMAIL'][$data->process_status]) && $data->channel_email != null) {
                $data_channel['EMAIL'][$data->process_status] = (int) $data->channel_email;
            }
            if (empty($data_channel['LIVECHAT'][$data->process_status]) && $data->channel_livechat != null) {
                $data_channel['LIVECHAT'][$data->process_status] = (int) $data->channel_livechat;
            }
            if (empty($data_channel['SMS'][$data->process_status]) && $data->channel_sms != null) {
                $data_channel['SMS'][$data->process_status] = (int) $data->channel_sms;
            }
            if (empty($data_channel['ITSRM'][$data->process_status]) && $data->channel_itsrm != null) {
                $data_channel['ITSRM'][$data->process_status] = (int) $data->channel_itsrm;
            }
            if (empty($data_channel['TELEGRAM'][$data->process_status]) && $data->channel_telegram != null) {
                $data_channel['TELEGRAM'][$data->process_status] = (int) $data->channel_telegram;
            }
            if (empty($data_channel['WHATSAPP'][$data->process_status]) && $data->channel_whatsapp != null) {
                $data_channel['WHATSAPP'][$data->process_status] = (int) $data->channel_whatsapp;
            }
        }
        $data_grand_total['TOTAL'] = (int) array_sum($data_grand_total);
        $data_category['LAYANAN_IT']['TOTAL'] = (int) array_sum($data_category['LAYANAN_IT']);
        $data_category['LAYANAN_CABANG']['TOTAL'] = (int) array_sum($data_category['LAYANAN_CABANG']);
        $data_category['LAYANAN_CC']['TOTAL'] = (int) array_sum($data_category['LAYANAN_CC']);
        $data_channel['CALL']['TOTAL'] = (int) array_sum($data_channel['CALL']);
        $data_channel['EMAIL']['TOTAL'] = (int) array_sum($data_channel['EMAIL']);
        $data_channel['SMS']['TOTAL'] = (int) array_sum($data_channel['SMS']);
        $data_channel['LIVECHAT']['TOTAL'] = (int) array_sum($data_channel['LIVECHAT']);
        $data_channel['ITSRM']['TOTAL'] = (int) array_sum($data_channel['ITSRM']);
        $data_channel['TELEGRAM']['TOTAL'] = (int) array_sum($data_channel['TELEGRAM']);
        $data_channel['WHATSAPP']['TOTAL'] = (int) array_sum($data_channel['WHATSAPP']);

        $sorted = collect([$data_channel['EMAIL'], $data_channel['CALL'], $data_channel['SMS'], $data_channel['LIVECHAT'], $data_channel['ITSRM'], $data_channel['TELEGRAM'], $data_channel['WHATSAPP']]);
        $sorted = array_values(Arr::sort($sorted, function ($value) {
            return $value['TOTAL'];
        }));
        $topfive = array_slice(array_reverse($sorted), 0, 5, true);

        $layanan_it = $this->inbox_ds->getSingleDailyTaskStatsFilter('SGP000000000151', date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'ReceivingDateTime', 'LAYANAN_IT', 'LAYANAN_CABANG');
        $layanan_cabang = $this->inbox_ds->getSingleDailyTaskStatsFilter('SGP000000000152', date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'ReceivingDateTime', 'LAYANAN_CABANG', 'LAYANAN_CABANG');
        $layanan_cc = $this->inbox_ds->getSingleDailyTaskStatsFilter('SGP000000000154', date("Y-m-d H:i:s", strtotime("today")), date("Y-m-d H:i:s", strtotime("tomorrow")), 'ReceivingDateTime', 'LAYANAN_CC', 'LAYANAN_CABANG');
        $label = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        $data_chart['layanan_it'] = $layanan_it;
        $data_chart['layanan_cabang'] = $layanan_cabang;
        $data_chart['layanan_cc'] = $layanan_cc;
        $data_chart['label'] = $label;

        $data_total = [$data_grand_total, $data_category, $data_channel, $data_summary_type, $data_chart, $topfive];
        return $data_total;
    }

    public function generateSummaryTotalByDate($pdate)
    {
        $data_summary = SummaryTotalRequest::select(
            'process_status',
            DB::raw('sum(total_request) total_request'),
            DB::raw('sum(layanan_it) layanan_it'),
            DB::raw('sum(layanan_cabang) layanan_cabang'),
            DB::raw('sum(layanan_cc) layanan_cc'),
            DB::raw('sum(channel_call) channel_call'),
            DB::raw('sum(channel_email) channel_email'),
            DB::raw('sum(channel_livechat) channel_livechat'),
            DB::raw('sum(channel_sms) channel_sms'),
            DB::raw('sum(channel_itsrm) channel_itsrm'),
            DB::raw('sum(channel_telegram) channel_telegram'),
            DB::raw('sum(channel_whatsapp) channel_whatsapp')
        )
            ->where('received_date', $pdate)
            ->groupBy('process_status')
            ->get();

        $data_summary_type = SummaryTotalRequest::select(
            'catalog_type',
            DB::raw('sum(total_request) total')
        )
            ->where('received_date', $pdate)
            ->groupBy('catalog_type')
            ->get();

        $data_grand_total = ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0];
        $data_category = [
            'LAYANAN_IT' => ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'LAYANAN_CABANG' => ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'LAYANAN_CC' =>  ['QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0]
        ];
        $data_channel = [
            'CALL' => ['NAME' => 'CALL', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'EMAIL' => ['NAME' => 'EMAIL', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'LIVECHAT' => ['NAME' => 'LIVECHAT', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'SMS' => ['NAME' => 'SMS', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'ITSRM' => ['NAME' => 'ITSRM', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'TELEGRAM' => ['NAME' => 'TELEGRAM', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0],
            'WHATSAPP' => ['NAME' => 'WHATSAPP', 'QUEUED' => 0, 'BOOKED' => 0, 'INPROGRESS' => 0, 'CLOSED' => 0, 'REJECT' => 0, 'PENDING' => 0, 'WAITINGAPR' => 0, 'PLANNING' => 0]
        ];

        foreach ($data_summary as $data) {
            if (empty($data_grand_total[$data->process_status])) {
                $data_grand_total[$data->process_status] = (int) $data->layanan_it + $data->layanan_cabang + $data->layanan_cc;
            }
            if (empty($data_category['LAYANAN_IT'][$data->process_status] && $data->layanan_it != null)) {
                $data_category['LAYANAN_IT'][$data->process_status] = (int) $data->layanan_it;
            }
            if (empty($data_category['LAYANAN_CABANG'][$data->process_status]) && $data->layanan_cabang != null) {
                $data_category['LAYANAN_CABANG'][$data->process_status] = (int) $data->layanan_cabang;
            }
            if (empty($data_category['LAYANAN_CC'][$data->process_status]) && $data->layanan_cc != null) {
                $data_category['LAYANAN_CC'][$data->process_status] = (int) $data->layanan_cc;
            }
            if (empty($data_channel['CALL'][$data->process_status]) && $data->channel_call != null) {
                $data_channel['CALL'][$data->process_status] = (int) $data->channel_call;
            }
            if (empty($data_channel['EMAIL'][$data->process_status]) && $data->channel_email != null) {
                $data_channel['EMAIL'][$data->process_status] = (int) $data->channel_email;
            }
            if (empty($data_channel['LIVECHAT'][$data->process_status]) && $data->channel_livechat != null) {
                $data_channel['LIVECHAT'][$data->process_status] = (int) $data->channel_livechat;
            }
            if (empty($data_channel['SMS'][$data->process_status]) && $data->channel_sms != null) {
                $data_channel['SMS'][$data->process_status] = (int) $data->channel_sms;
            }
            if (empty($data_channel['ITSRM'][$data->process_status]) && $data->channel_itsrm != null) {
                $data_channel['ITSRM'][$data->process_status] = (int) $data->channel_itsrm;
            }
            if (empty($data_channel['TELEGRAM'][$data->process_status]) && $data->channel_telegram != null) {
                $data_channel['TELEGRAM'][$data->process_status] = (int) $data->channel_telegram;
            }
            if (empty($data_channel['WHATSAPP'][$data->process_status]) && $data->channel_whatsapp != null) {
                $data_channel['WHATSAPP'][$data->process_status] = (int) $data->channel_whatsapp;
            }
        }
        $data_grand_total['TOTAL'] = (int) array_sum($data_grand_total);
        $data_category['LAYANAN_IT']['TOTAL'] = (int) array_sum($data_category['LAYANAN_IT']);
        $data_category['LAYANAN_CABANG']['TOTAL'] = (int) array_sum($data_category['LAYANAN_CABANG']);
        $data_category['LAYANAN_CC']['TOTAL'] = (int) array_sum($data_category['LAYANAN_CC']);
        $data_channel['CALL']['TOTAL'] = (int) array_sum($data_channel['CALL']);
        $data_channel['EMAIL']['TOTAL'] = (int) array_sum($data_channel['EMAIL']);
        $data_channel['SMS']['TOTAL'] = (int) array_sum($data_channel['SMS']);
        $data_channel['LIVECHAT']['TOTAL'] = (int) array_sum($data_channel['LIVECHAT']);
        $data_channel['ITSRM']['TOTAL'] = (int) array_sum($data_channel['ITSRM']);
        $data_channel['TELEGRAM']['TOTAL'] = (int) array_sum($data_channel['TELEGRAM']);
        $data_channel['WHATSAPP']['TOTAL'] = (int) array_sum($data_channel['WHATSAPP']);


        $sorted = collect([$data_channel['EMAIL'], $data_channel['CALL'], $data_channel['SMS'], $data_channel['LIVECHAT'], $data_channel['ITSRM'], $data_channel['TELEGRAM'], $data_channel['WHATSAPP']]);
        $sorted = array_values(Arr::sort($sorted, function ($value) {
            return $value['TOTAL'];
        }));
        $topfive = array_slice(array_reverse($sorted), 0, 5, true);

        $datetime = $pdate . ' 00:00:00';
        $datetimetomorrow = date("Y-m-d H:i:s", strtotime($datetime . "1 day", strtotime("tomorrow")));

        $layanan_it = $this->inbox_ds->getSingleDailyTaskStatsFilter('SGP000000000151', (string) $datetime, (string) $datetimetomorrow, 'ReceivingDateTime', 'LAYANAN_IT', 'LAYANAN_CABANG');
        $layanan_cabang = $this->inbox_ds->getSingleDailyTaskStatsFilter('SGP000000000152', (string) $datetime, (string) $datetimetomorrow, 'ReceivingDateTime', 'LAYANAN_CABANG', 'LAYANAN_CABANG');
        $layanan_cc = $this->inbox_ds->getSingleDailyTaskStatsFilter('SGP000000000154', (string) $datetime, (string) $datetimetomorrow, 'ReceivingDateTime', 'LAYANAN_CC', 'LAYANAN_CABANG');
        $label = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
        $data_chart['layanan_it'] = $layanan_it;
        $data_chart['layanan_cabang'] = $layanan_cabang;
        $data_chart['layanan_cc'] = $layanan_cc;
        $data_chart['label'] = $label;

        $data_total = [$data_grand_total, $data_category, $data_channel, $data_summary_type, $data_chart, $topfive];
        return $data_total;
    }
}
