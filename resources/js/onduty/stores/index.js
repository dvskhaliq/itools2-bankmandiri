import Vuex from 'vuex'
import ENUM from './enum'
import Vue from 'vue'
import incidentod from "./modules/incident_od";
import incidentformod from "./modules/incident_form_od";

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        apiState: ENUM.INIT,
        exceptionMessage: ""
    },
    actions: {
    },
    mutations: {
        setApiState: (state, enumState) => (state.apiState = enumState),
        setException: (state, message) => {
            state.exceptionMessage = message;
            state.apiState = ENUM.ERROR;
        },
    },
    getters: {
        getApiState: state => state.apiState
    },
    modules: {
        incidentod,
        incidentformod
    }
});
