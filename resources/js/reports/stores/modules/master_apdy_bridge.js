import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            pmmastersystemavailability: {},
            site:'',
            machine: '',
            type:'',
            business_txn_metrics:'',
            rest_url:'',
            node:'',
        }
    };
};

const state = {
    masterapdys: [],
    simpleHeader: [
        { text: "Service Name", value: "pmmastersystemavailability.system_name" },
        { text: "Site", value: "site" },
        { text: "Machine", value: "machine" },
        { text: "Type", value: "type" },
        { text: "Business Txn Metrics", value: "business_txn_metrics" },
        { text: "Rest URL", value: "rest_url" },
        { text: "Node", value: "node" },
        { text: "Action", value: "action", width: "80px", sortable: false }
    ],
    editedIndex: defaultItem().index,
    editedItem: defaultItem().item,
    apiState: ENUM.INIT,
    alertMessage: "",
    systemname: [],
};

const getters = {
    getMasterApdys: state => state.masterapdys,
    getSimpleHeader: state => state.simpleHeader,
    getEditItem: state => state.editedItem,
    getEditIndex: state => state.editedIndex,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getSystemName: state => state.systemname,
};

const actions = {
    async fetchSystemName({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/master_apdy_bridge/fetchSystemName");
            commit("setSystemName", response.data.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Something went wrong on fetch!"
                  );
        }
    },
    async initPMMasterApdyBridge({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/data_management/problems/master_apdy_bridge/fetch");
          commit("setMasterApdy", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async addPMMasterApdyBridge({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/master_apdy_bridge/create", payload);
            commit("newMasterApdy", response.data.data);
            commit("setSuccess", "Data has been successfully saved !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Adding data has failed"
                  );
        }
    },
    async updateMasterApdyBridge({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.put("/data_management/problems/master_apdy_bridge/update", payload);
            commit("updateMasterApdy", response.data.data);
            commit("setSuccess", "Data has been successfully updated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Updating data has failed"
                  );
        }
    },
    async deletePMMasterApdyBridge({ commit }, id) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.delete("/data_management/problems/master_apdy_bridge/delete/" + id);
            commit("deleteMasterApdy", response.data.data);
            commit("setSuccess", "Data has been successfully deleted !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Deleting data has failed"
                  );
        }
    },
    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
    resetMasterApdys({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setMasterApdy", []);
    },
    resetSystemNames({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setSystemName", []);
    },
};

const mutations = {
    newMasterApdy: (state, masterapdy) => {
        state.apiState = ENUM.LOADED;
        state.masterapdys.unshift(masterapdy);
    },
    deleteMasterApdy: (state, deletedMasterApdy) => {
        state.apiState = ENUM.LOADED;
        const index = state.masterapdys.findIndex(masterapdy => masterapdy.id == deletedMasterApdy.id);
        state.masterapdys.splice(index, 1);
    },
    updateMasterApdy: (state, updatedMasterApdy) => {
        state.apiState = ENUM.LOADED;
        const index = state.masterapdys.findIndex(masterapdy => masterapdy.id == updatedMasterApdy.id);
        state.masterapdys.splice(index, 1, updatedMasterApdy);
    },
    setSystemName: (state, systemname) => {
        state.apiState = ENUM.LOADED;
        state.systemname = systemname;
    },
    setEditItem: (state, payload) => {
        state.editedItem = payload;
    },
    setEditIndex: (state, index) => {
        state.editedIndex = index;
    },
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setMasterApdy: (state, masterapdy) => {
        state.apiState = ENUM.LOADED;
        state.masterapdys = masterapdy;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
