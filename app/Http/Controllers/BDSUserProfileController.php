<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSUserProfileService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BDSUserProfileController extends BaseController
{

    protected $userProfileService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSUserProfileService $userProfile)
    {
        $this->userProfileService = $userProfile;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return View::make('bds_connection/bds_user_profile/user_profile');
    }

    public function getList()
    {
        $result = $this->userProfileService->getList();

        return response()->json($result);
    }

    public function findList()
    {
        if (request()->get('search') == 'null' || request()->get('search') == '') {
            $result = $this->userProfileService->getList();
        } else {
            $result = $this->userProfileService->findById(request()->get('search'));
        }

        return response()->json($result);
    }

    public function userProfileSelected()
    {
        $result = $this->userProfileService->findById(request()->get('userId'));
        return response()->json([
            'status' => true,
            'result' => $result,
        ]);
    }

    public function setSignOff()
    {
        $findData =  $this->userProfileService->findById(request()->get('userId'));
            $dataUpdate = array(
                'Status' => 'O',
                'IPAddress' => null
            );

            $this->userProfileService->updateUserProfile($findData[0]['UserID'], $dataUpdate);
            return response()->json([
                'status' => true,
                'result' => "Change sign off UserID : " . $findData[0]['UserID'] . " success",
            ]);
    }

    public function setMonetaryUser()
    {
        $findData =  $this->userProfileService->findById(request()->get('userId'));
        if ($findData[0]['MonetaryUser'] == null) {
            $MonetaryUser = "NULL";
        } else {
            $MonetaryUser = $findData[0]['MonetaryUser'];
        }

        if ($MonetaryUser == request()->get('monetaryUser')) {
            return response()->json([
                'status' => false,
                'result' => "Change monetary status user to " . request()->get('monetaryUser') . " failed, because Monetary status user is "  . $MonetaryUser,
            ]);
        } else {
            if (request()->get('monetaryUser') == 'NULL') {
                $dataUpdate = array(
                    'MonetaryUser' => null
                );
            } else {
                $dataUpdate = array(
                    'MonetaryUser' => request()->get('monetaryUser')
                );
            }
            $this->userProfileService->updateUserProfile(request()->get('userId'), $dataUpdate);
            return response()->json([
                'status' => true,
                'result' => "Change monetary status user to " . request()->get('monetaryUser') . " success",
            ]);
        }
    }

    public function setCTT()
    {
        $findData =  $this->userProfileService->findById(request()->get('userId'));
        if ($findData[0]['CTT'] == request()->get('ctt')) {
            return response()->json([
                'status' => false,
                'result' => "Change CTT status user to " . request()->get('ctt') . " failed, because CTT status user is "  . $findData[0]['CTT'],
            ]);
        } else {
            $dataUpdate = array(
                'CTT' => request()->get('ctt')
            );
            $this->userProfileService->updateUserProfile(request()->get('userId'), $dataUpdate);
            return response()->json([
                'status' => true,
                'result' => "Change CTT status user to " . request()->get('ctt') . " success",
            ]);
        }
    }

    public function setLineStatus()
    {
        $findData =  $this->userProfileService->findById(request()->get('userId'));
        if ($findData[0]['LineStatus'] == request()->get('lineStatus')) {
            return response()->json([
                'status' => false,
                'result' => "Change LineStatus user to " . request()->get('lineStatus') . " failed, because LineStatus User is "  . $findData[0]['LineStatus'] ,
            ]);
        } else {
            $dataUpdate = array(
                'LineStatus' => request()->get('lineStatus')
            );

            $this->userProfileService->updateUserProfile(request()->get('userId'), $dataUpdate);
            return response()->json([
                'status' => true,
                'result' => "Change LineStatus user to " . request()->get('lineStatus') . " success",
            ]);
        }
    }

    public function tranLimitSelected()
    {
        $results = $this->userProfileService->findTransLimit(request()->get('levelId'));

        return response()->json([
            'status' => true,
            'result' => $results,
        ]);
    }

    public function findUserId()
    {
        $results = $this->userProfileService->findById(request()->get('user_id'));
        return response()->json($results);
    }

    public function listHolder()
    {
        $results = $this->userProfileService->listHolder();
        return response()->json($results);
    }

    public function findLevelId()
    {
        $results = $this->userProfileService->findByLevelId(request()->get('level_id'));
        return response()->json($results);
    }

    public function getHeadTeller()
    {
        $results = $this->userProfileService->findHeadTeller();
        return response()->json(['results' => $results[0]]);
    }
}
