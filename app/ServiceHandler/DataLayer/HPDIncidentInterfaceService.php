<?php

namespace App\ServiceHandler\DataLayer;
use Illuminate\Database\Eloquent\Collection;

interface HPDIncidentInterfaceService
{
    public function truncateHPDIncidentInterface();
    // public function bulkInsert($collection): ?bool;
    public function getHPDIncidentInterface(): ?Collection;
    public function insertIncident($payload);
    // public function getTotalByYear();
}
