@extends('sidebar')
@section('content')
<div id="bds_kartu_instan">
    <div class="row bds-kartu-instan">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS Document Inventory</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <div class="panel-body">
                    <flash v-if="ismodal" info></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Kartu Instan</span>
                            </div>
                            <div class="panel-body">
                                <componen-kartu-instan :data_kartu_instan="data_kartu_instan" :isloaded="isloaded" v-on:submit-all="submitAll" v-on:list-selected="listSelected" v-on:onchange="onChangeFilter"></componen-kartu-instan>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Set Allocation</span>
                            </div>
                            <componen-form v-on:submit-form="submitForm" :list_selected="list_selected" :data_holder="data_holder"></componen-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="../../js/bds_kartu_instan.js"></script>
@endsection
