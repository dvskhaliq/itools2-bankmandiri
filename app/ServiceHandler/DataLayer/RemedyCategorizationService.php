<?php

namespace App\ServiceHandler\DataLayer;

interface RemedyCategorizationService
{
    public function getProductCategoryTier1();

    public function getProductCategoryTier2(String $product_category_tier_1);

    public function getProductCategoryTier3(String $product_category_tier_1, String $product_category_tier_2);

    public function getOprationalCategoryTier1(String $product_category_tier_1, String $product_category_tier_2, String $product_category_tier_3);

    public function getOprationalCategoryTier2(String $product_category_tier_1, String $product_category_tier_2, String $product_category_tier_3, String $operational_category_tier_1);

    public function getOprationalCategoryTier3(String $product_category_tier_1, String $product_category_tier_2, String $product_category_tier_3, String $operational_category_tier_1, String $operational_category_tier_2);

    public function searchServiceCatalog($name);
}
