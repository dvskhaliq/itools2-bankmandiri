require('../../bootstrap');

window.Vue = require('vue');

window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('component-table', ()=> import('../../components/los/los_fingerprint_status/LosFingerprintStatus.vue'));
Vue.component('component-upload', ()=> import('../../components/los/los_fingerprint_status/LosFingerprintStatusUpload.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));

import 'es6-promise/auto'
import Vuex from 'vuex';

Vue.use(Vuex);
const los_fingerprint_status = new Vue({
    el: '#los_fingerprint_status',
    data: {
        ismodal: false,
        alert: false,
        isloaded: false,
    },

    methods: {

        setFlashDanger(reqResponse) {
            this.alert = true;
            flash({
                id: "alert-danger",
                message: reqResponse.message,
                flashtype: "danger"
            });
        },

        setFlashInfo(reqResponse) {
            this.alert = true;
            flash({
                id: "alert-info",
                message: reqResponse.message,
                flashtype: "info"
            });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
        },
    },

});
