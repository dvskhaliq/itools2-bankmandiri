<?php

namespace App\Http\Controllers;

use JavaScript;
use Auth;
use View;
use App\Incident;
use Illuminate\Support\Facades\Storage;

class SDSpecialistController extends MainController
{
    public function sdAgentDashboard($service_type)
    {
        // $request_queue = $this->taskrequest_service->getRequestListByStatusAndCategoryJson('QUEUED', $service_type);
        $request_queue = $this->inbox_ds->getByStatusAndCategory('QUEUED', $service_type);
        // $working_task = $this->taskrequest_service->getTaskListByAssingeeAndStatusJson(Auth::user()->employeeremedy->id, 'INPROGRESS');
        $working_task = $this->inbox_ds->getByAssigneeAndStatus(Auth::user()->employeeremedy->id, 'INPROGRESS');
        $in_history = $this->inbox_ds->getRequestsHistory(Auth::user()->employee_remedy_id, 'IN', false, "false");
        $wo_history = $this->inbox_ds->getRequestsHistory(Auth::user()->employee_remedy_id, 'WO', false, "false");
        $daily_chart['dailytask'] = app()->call('App\Http\Controllers\ReportingController@getPersonalTaskReport');
        $daily_chart['weeklytask'] = app()->call('App\Http\Controllers\ReportingController@getPersonalWeeklyTaskReport');
        $daily_chart['dailychannel'] = app()->call('App\Http\Controllers\ReportingController@getPersonalDailyChannelReport');
        $daily_chart['weeklychannel'] = app()->call('App\Http\Controllers\ReportingController@getPersonalWeeklyChannelReport');
        JavaScript::put([
            "serviceType" => $service_type,
            "workingTask" => $working_task,
            "inHistory" => $in_history,
            "woHistory" => $wo_history,
            "requestQueue" => $request_queue,
            "chart" => $daily_chart,
            "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']),
        ]);
        return View::make('landing/dashboard_sds');
    }

    public function resultTT()
    {
        $data = request()->get('data');
        $servicecatalog = request()->get('servicecatalog');
        switch ($servicecatalog) {
            case 62: {
                    return $this->cpsserver_service->queryTT1($data);
                    break;
                }
            case 63: {
                    return $this->cpsserver_service->queryTT2($data);
                    break;
                }
            case 64: {
                    return $this->cpsserver_service->queryTT3($data);
                    break;
                }
            case 65: {
                    return $this->cpsserver_service->queryTT4($data);
                    break;
                }
            case 66: {
                    return $this->cpsserver_service->queryRTGS1($data);
                    break;
                }
            case 95: {
                    return $this->cpsserver_service->queryRTGS2($data);
                    break;
                }
            case 67: {
                    return $this->cpsserver_service->queryMCS1($data);
                    break;
                }
            case 68: {
                    return $this->cpsserver_service->queryMandol1($data, request()->get('field'));
                    break;
                }
        }
    }

    public function updateDataTT()
    {
        $trackid = request()->get('trackid');
        $servicecatalog = request()->get('servicecatalog');
        $data = request()->get('data');
        $old_data = request()->get('olddata');
        $remedy_ticket = request()->get('remedyticket');
        $summary_ticket = request()->get('summary');
        switch ($servicecatalog) {
            case 62: {
                    return $this->cpsserver_service->updateDataTT1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
            case 63: {
                    return $this->cpsserver_service->updateDataTT2($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
            case 64: {
                    return $this->cpsserver_service->updateDataTT3($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
            case 65: {
                    return $this->cpsserver_service->updateDataTT4($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
            case 66: {
                    return $this->cpsserver_service->updateDataRTGS1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
            case 95: {
                    return $this->cpsserver_service->updateDataRTGS2($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
            case 67: {
                    return $this->cpsserver_service->updateDataMCS1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);
                    break;
                }
        }
    }

    public function searchAuditTrail(){
        $data = $this->cpsserver_service->auditTrailView(request()->get('field'), request()->get('search'));
        return response()->json(['result' => $data,]);
    }

    private function parseUrgency($urgency_string)
    {
        $urgency = 'low';
        if ($urgency_string == '4-Low') {
            $urgency = 'Low';
        } else if ($urgency_string == '3-Medium') {
            $urgency = 'High';
        } else if ($urgency_string == '2-High') {
            $urgency = 'Critical';
        } else if ($urgency_string == '1-Critical') {
            $urgency = 'Critical';
        }
        return $urgency;
    }

    public function createIncidentSDS()
    {
        $inc = new Incident();
        $content = request()->get('content');
        $inc->parseContent($content);
        $inbox = $this->inbox_ds->addByInc($inc, request()->get('requestId'));
        $inc->parseInbox($inbox);
        $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($inc->service);
        $wsdl = $this->inccreate_wsdl->createIncidentGeneric($inc, $wsdl_reconID->getContent());

        $request = $this->inbox_ds->refresh($inbox);
        $inc->parseInbox($request);
            if ($wsdl->getResponseStatus() == 'success') {
                $findWorkinfoId = $this->workinfolog_ds->findWorkinfobyId($request->id);
                if($findWorkinfoId !== null){
                    $getFile = Storage::disk('public')->get($request->id . "/" . $request->id . "_" . $findWorkinfoId->id.".".$findWorkinfoId->file_ext);
                    if($this->uploadFiletoRemedy($getFile, $findWorkinfoId) == true){
                        $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                    }else{
                        $this->inbox_ds->update($findWorkinfoId->id, array("ActionTicketCreation" => "IN_PENDING_UPL"));
                        $message = "Incident Number: " . $wsdl->getContent() . " has been created, but failed to upload file, Please try again in Pending Ticket !";
                    }
                    $request['inboxworkloginfo_url'] = url("/storage/".$findWorkinfoId->request_id. "/" .$findWorkinfoId->request_id. "_" .$findWorkinfoId->id. "." .$findWorkinfoId->file_ext);
                    $request['file_name'] = $findWorkinfoId->first_attachment_file_name;
                }else{
                    $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                    $request['inboxworkloginfo_url'] = null;
                }
                $request['incident'] = $inc->parseInbox($request);
            } else {
            $message = "Opps.. Error captured, unable to create Incident Number, please retry from Pending Ticket Menu immediately! " . $wsdl->getResponseMessage();
            $this->updateRequestDueException($request->id, $request->ProcessStatus, "IN_PENDING_PRS", Auth::user()->id, $request->AssigneeId);
        }
        return response()->json(['status' => true, 'message' => $message, 'responsedata' => $request, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'wsdl_request' => $wsdl->getArrContent()]);
    }

    //**********************************Below code is before assesment *************************/

    // public function createStagingSDSIncidentToRemedyClosed()// not used
    // {
    //     // $request = $this->taskrequest_service->getQueueObjectById(request()->get('requestId'));
    //     $request = $this->inbox_ds->find(request()->get('requestId'));
    //     $content = request()->get('content');
    //     $cstmr = $this->user_service->getEmployeeRemedy(request()->get('customer')['id']);
    //     $asgn = $this->user_service->getUserByFullNameAndGroupName(request()->get('assignee'));
    //     $urgency = $this->parseUrgency($content['urgency']);
    //     $text_decoded = "INCIDENT#" . $cstmr->company . "#User Service Restoration#" . $content['notes'] . "#" . $content['summary'] . "#" . $asgn->support_group_name . "#" . $asgn->full_name . "#" . $content['impact'] . "#" . $urgency . "##" . $content['reportedSource'] . "#" . $asgn->company . "#" . $content['productCategoryTier1'] . "#" . $content['productCategoryTier2'] . "#" . $content['productCategoryTier3'] . "###" . $content['operationalCategoryTier1'] . "#" . $content['operationalCategoryTier2'] . "#" . $content['operationalCategoryTier3'] . "#" . $content['resolution'];

    //     //Menyimpan Data ke DB Remedy
    //     $wsdl_rslt = $this->inccreate_wsdl->createIncidentGeneric2($request->id, $text_decoded, $cstmr, $asgn, $content['status']);

    //     $status = 'CLOSED';
    //     switch ($content['status']) {
    //         case 'Assigned':
    //             $status = 'INPROGRESS';
    //             $action_ticket_creation = 'IN_WAIT';
    //             break;
    //         case 'In Progress':
    //             $status = 'INPROGRESS';
    //             $action_ticket_creation = 'IN_PROCESS';
    //             break;
    //         case 'Pending':
    //             $status = 'PENDING';
    //             $action_ticket_creation = 'IN_PROCESS';
    //             break;
    //         case 'Resolved':
    //             $status = 'CLOSED';
    //             $action_ticket_creation = 'IN_COMPLETED';
    //             break;
    //         case 'Cancelled':
    //             $status = 'REJECT';
    //             $action_ticket_creation = 'IN_COMPLETED';
    //             break;
    //         default:
    //             $status = 'CLOSED';
    //             $action_ticket_creation = 'IN_COMPLETED';
    //             break;
    //     }
    //     $tamp_service_catalog_id = $this->taskrequest_service->findServiceCatalogByCategorization($content['productCategoryTier1'], $content['productCategoryTier2'], $content['productCategoryTier3'], $content['operationalCategoryTier1'], $content['operationalCategoryTier2'], $content['operationalCategoryTier3']);
    //     try {
    //         $array_inbox = array(
    //             "Text" => $content['summary'],
    //             "SenderNumber" => $cstmr->full_name,
    //             "SMSCNumber" => $cstmr->id,
    //             "TextDecoded" => $text_decoded,
    //             "BookDateTime" => date('Y-m-d H:i:s'),
    //             "TicketCreationDateTime" => date('Y-m-d H:i:s'),
    //             "ActionTicketCreation" => "IN_WAIT",
    //             "AssigneeId" => Auth::user()->id,
    //             "ServiceCatalogId" => $tamp_service_catalog_id !== null ? $tamp_service_catalog_id->id : 69,
    //         );
    //         $this->taskrequest_service->update(request()->get("requestId"), $array_inbox);
    //         if ($wsdl_rslt->getResponseStatus() == 'success') {
    //             if ($this->taskrequest_service->isRemedyTicketExist($wsdl_rslt->getContent())) {
    //                 $duplicated = $this->taskrequest_service->findRemedyTicketDuplicated($wsdl_rslt->getContent(), $request->id);
    //                 $inbox = $this->taskrequest_service->mergeRequest($request, $duplicated);

    //                 if ($status == 'CLOSED') {
    //                     //Merubah ProcessStatus jadi sesuai parameter & ActionTicketCreation menjadi IN_COMLETED 
    //                     $array_inbox = array(
    //                         "TextDecoded" => $text_decoded,
    //                         "ProcessStatus" => "CLOSED",
    //                         "ActionTicketCreation" => "IN_COMPLETED",
    //                     );
    //                     $inbox = $this->taskrequest_service->update(request()->get("requestId"), $array_inbox);
    //                 }
    //             } else {
    //                 $array_inbox = array(
    //                     "UDH" => $wsdl_rslt->getArrContent()->udh,
    //                     "ActionTicketCreation" => "IN_PROCESS",
    //                     "ProcessStatus" => $status,
    //                     "RemedyTicketId" => $wsdl_rslt->getContent(),
    //                     "EmployeeAssigneeId" => $asgn->employee_remedy_id,
    //                     "EmployeeSenderId" => $cstmr->id,
    //                     "EmployeeAssigneeGroupId" => $asgn->support_group_id,
    //                 );
    //                 $inbox = $this->taskrequest_service->update(request()->get("requestId"), $array_inbox);
    //             }
    //             return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Incident Number : " . $wsdl_rslt->getContent() . " has been merged", 'wsdlStatus' => true, 'wsdl' => $wsdl_rslt->getResponseMessage()]);
    //         } else {
    //             $array_inbox = array(
    //                 "UDH" => $wsdl_rslt->getArrContent()->udh,
    //                 "ActionTicketCreation" => "IN_PENDING_PRS",
    //                 "RemedyTicketId" => "N/A",
    //                 "EmployeeAssigneeId" => $asgn->employee_remedy_id,
    //                 "EmployeeSenderId" => $cstmr->id,
    //                 "EmployeeAssigneeGroupId" => $asgn->support_group_id,
    //             );
    //             $inbox = $this->taskrequest_service->update(request()->get("requestId"), $array_inbox);
    //             return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Unable to submit incident, with current response :" . $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false, 'wsdl' => $wsdl_rslt->getResponseMessage()]);
    //         }
    //     } catch (Exception $e) {
    //         return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed!"]);
    //     }
    // }
}
