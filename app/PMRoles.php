<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMRoles extends Model
{
    protected $table = 'pm_roles';

    public function pmusers()
    {
        return $this->hasMany(User::class, 'problem_role_id');
    }

    public function pmrolesfeatures(){
        return $this->hasMany(PMRolesFeatures::class,'role_id','id');
    }
}
