<?php

namespace App\ServiceHandler\PBMLayer;
use Illuminate\Database\Eloquent\Collection;

interface DashboardItService
{
    public function getDashboardIt(): ?Collection;
    public function getDashboardItByYear(array $years): ?Collection;
    public function getDashboardItByMonth($month): ?Collection;
    public function getDashboardItByMonthLayanan($layanan, $month): ?Collection;
    public function getDashboardItByMonthAvg($month): ?Collection;
    public function getDashboardItByDate($date): ?Collection;
    public function getYears();
}
