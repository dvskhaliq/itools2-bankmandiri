const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.styl$/,
                loader: ["style-loader", "css-loader", "stylus-loader"]
            }
        ]
    },
    output: {
        filename: "[name].js",
        chunkFilename: "js/chunks/[name].js"
    }
});

mix.js("resources/js/sds/sds.js", "public/js")
    .js("resources/js/controller/dashboard/dashboard.js", "public/js")
    .js("resources/js/onduty/onduty.js", "public/js")
    .js("resources/js/reports/reports.js", "public/js")
    .js("resources/js/incident/incident_report.js", "public/js")
    .js("resources/js/controller/dashboard/dashboard_manager.js", "public/js")
    /*add by putut od dashboard */
    .js("resources/js/controller/dashboard/dashboard_od.js", "public/js")
    /*end by putut od dashboard */
    .js("resources/js/controller/bds_connection/bds_server.js", "public/js")
    .js(
        "resources/js/controller/bds_connection/bds_user_profile.js",
        "public/js"
    )
    .js(
        "resources/js/controller/bds_connection/bds_kartu_instan.js",
        "public/js"
    )
    .js(
        "resources/js/controller/bds_connection/bds_surat_berharga.js",
        "public/js"
    )
    .js(
        "resources/js/controller/bds_connection/bds_access_desc.js",
        "public/js"
    )
    .js("resources/js/controller/bds_connection/bds_ej2.js", "public/js")
    .js("resources/js/controller/bds_connection/bds_parameter.js", "public/js")
    .js("resources/js/controller/bds_connection/bds_atm_detail.js", "public/js")
    .js("resources/js/controller/bds_connection/bds_cdm_detail.js", "public/js")
    .js("resources/js/controller/bds_connection/bds_crm_detail.js", "public/js")
    .js(
        "resources/js/controller/bds_connection/bds_branch_conf.js",
        "public/js"
    )
    .js(
        "resources/js/controller/administrator_console/user_configuration.js",
        "public/js"
    )
    .js(
        "resources/js/controller/administrator_console/bds_server_configuration.js",
        "public/js"
    )
    .js("resources/js/controller/los/los_consumer.js", "public/js")
    .js("resources/js/controller/los/los_fingerprint_status.js", "public/js")
    .js(
        "resources/js/controller/pending_ticket_remedy/pending_ticket_remedy.js",
        "public/js"
    )
    .js("resources/js/controller/report/summary_report.js", "public/js")
    // .js('resources/js/controller/report/incident_report.js', 'public/js')
    .js("resources/js/controller/sidebar/sidebar.js", "public/js")
    .js("resources/js/controller/storage_ftp/storage_ftp.js", "public/js")
    .sass("resources/sass/app.scss", "public/css");
