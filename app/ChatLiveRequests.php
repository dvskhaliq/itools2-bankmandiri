<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatLiveRequests extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['chat_live_requests'];
    public $timestamps = false;

    public function chatsubscribeduser(){
        return $this->belongsTo(ChatSubscribedUser::class,'user_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'assigned_agent_id','id');
    }
}
