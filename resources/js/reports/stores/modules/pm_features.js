import ENUM from "../enum";
const state = {
    features: [],
    apiState: ENUM.INIT,
    alertMessage: "",
    simpleHeader: [
        { text: "Nama Fitur", value: "display_name" },
        { text: "Baca", value: "baca", align: "center" },
        { text: "Tulis", value: "tulis", align: "center" },
        { text: "Edit", value: "edit", align: "center" },
        { text: "Hapus", value: "hapus", align: "center" }
    ],
};

const getters = {
    getFeatures: state => state.features,
    getApiState: state => state.apiState,
    getSimpleHeader: state => state.simpleHeader,
    getAlertMessage: state => state.alertMessage,
};

const actions = {
    async fetchFeatures({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/features");
            commit("setFeatures", response.data.features);
        } catch (error) {
            error.response.status == 403
                ? commit("setException", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! tidak dapat memuat data."
                  );
        }
    },
    async fetchInactiveFeatures({ commit }, roleId) {
        commit("setApiState", ENUM.LOADING);
        let responseOwned;
        try {
            responseOwned = await axios.get("/data_management/problems/features/owned/" + roleId);
            if (responseOwned.data.status) {
            commit("setFeatures", responseOwned.data.features);
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setException", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! tidak dapat memuat data"
                  );
        }
    },
    resetFeatures({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setFeatures",[])
    }
};

const mutations = {
    setFeatures: (state, features) => {
        state.apiState = ENUM.LOADED;
        state.features = features;
    },
    setApiState: (state, enumState) => (state.apiState = enumState),
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
