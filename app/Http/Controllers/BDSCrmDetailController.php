<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BDSCrmDetailController extends BaseController
{
    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {

        return View::make('bds_connection/bds_atm/bds_crm_detail/bds_crm_detail');
    }

    public function getList()
    {
        $result = $this->bdsServerService->initBdsConnection()->table('CRMDetail')->select()->get();

        return response()->json($result);
    }

    public function findCrmDetailById($id)
    {
        $data = $this->bdsServerService->initBdsConnection()->table('CRMDetail')->select()->where('CRMGLRefNo', $id)->get();
        return $data;
    }

    public function detailSelected()
    {
        $data = $this->findCrmDetailById(request()->get('id'));
        return response()->json([
            'status' => true,
            'result' => $data[0],
        ]);
    }

    public function submitDeleted()
    {

        $this->bdsServerService->initBdsConnection()->table('CRMDetail')->where([['CRMGLRefNo',  request()->get('CRMGLRefNo')]])->delete();

        return response()->json([
            'status' => true,
            'result' => 'Delete CRMGLRefNo : ' . request()->get('CRMGLRefNo') . ' - ' . request()->get('Description') . ' success',
        ]);
    }

    public function submitUpdated()
    {
        $findCrm = $this->findCrmDetailById(request()->get('CRMGLRefNo'))->toArray();
        if (!empty($findCrm)) {
            if ($findCrm[0]['Description'] == request()->get('Description')) {
                return response()->json([
                    'status' => false,
                    'result' => 'CRMGLRefNo : ' . request()->get('CRMGLRefNo') . ' - ' . request()->get('Description') . ' not change',
                ]);
            } else {
                $this->bdsServerService->initBdsConnection()->table('CRMDetail')->where('CRMGLRefNo', request()->get('CRMGLRefNo'))->update(['Description' => request()->get('Description')]);

                return response()->json([
                    'status' => true,
                    'result' => 'Update CRMGLRefNo : ' . request()->get('CRMGLRefNo') . ' - ' . request()->get('Description') . ' success',
                ]);
            }
        }
    }

    public function submitAdd()
    {
        $data = array(
            'CRMGLRefNo' => request()->get('CRMGLRefNo'),
            'Description' => request()->get('Description')
        );

        $findCrm = $this->findCrmDetailById(request()->get('CRMGLRefNo'))->toArray();
        if (empty($findCrm)) {
            $this->bdsServerService->initBdsConnection()->table('CRMDetail')->insert($data);

            return response()->json([
                'status' => true,
                'result' => 'Add CRMGLRefNo : ' . request()->get('CRMGLRefNo') . ' - ' . request()->get('Description') . ' success',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'result' => 'Add failed, CRMGLRefNo : ' . request()->get('CRMGLRefNo') . ' already exists',
            ]);
        }
    }
}
