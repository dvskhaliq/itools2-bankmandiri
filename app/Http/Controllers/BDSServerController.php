<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;
use View;

class BDSServerController extends BaseController
{

    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {

        return View::make('bds_connection/bds_server/bds_server');
    }

    public function page()
    {
        return $this->bdsServerService->listAllByPagination();
    }

    public function findByKodeCabang()
    {
        return $this->bdsServerService->findByKodeCabang(request()->get('search'));
    }

    public function searchData()
    {
        return $this->bdsServerService->findData(request()->get('search'));
    }

    function setConnectToDatabase()
    {
        return $this->bdsServerService->setConnection(request()->get('ip_server'), request()->get('kode_cabang'), request()->get('nama_cabang'));
    }

    function setDisconnectDatabase()
    {
        return $this->bdsServerService->setDisonnect(Session::get('data.ipServer'), Session::get('data.database'));
    }

    public function ftpLogin($ftpserver, $username, $password)
    {
        $ftp_server = $ftpserver;
        $usernameftp = $username;
        $passwordftp = $password;

        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        // $login = ftp_login($ftp_conn, $usernameftp, $passwordftp);
        if (@ftp_login($ftp_conn, $usernameftp, $passwordftp)) {
            $status = true;
            return [$ftp_conn, $status];
        } else {
            $status = false;
            $message = "Couldn't establish a connection.";
            return [$message, $status];
        }

        // return View::make('ftp_server/ftp_connect');
    }

    public function ftpconnect()
    {
        $ftp_conn = $this->ftpLogin(request()->get('ipserver'), request()->get('username'), request()->get('password'));
        if ($ftp_conn[1]) {
            $file_list = ftp_rawlist($ftp_conn[0], ".");
            $file_nlist = ftp_nlist($ftp_conn[0], ".");
            // var_dump($file_list);
            $array_length = count($file_list);

            for ($i = 0; $i < $array_length; $i++) {
                $list[$i] = preg_split("/[\s]+/", $file_list[$i], 9);
                // $list[$i] = $file_list[$i];
            }
        } else {
            $list = $ftp_conn[0];
        }
        // return $ftp_conn;
        return response()->json([
            'result' => $list,
            'status' => $ftp_conn[1]
        ]);
    }

    public function ftpUpload()
    { }
}