<?php

namespace App\ServiceHandler\DataLayer;

use App\ServiceCatalog as Catalog;

interface ServiceCatalogService
{
    public function find(Int $catalog_id): ?Catalog;

    public function findBySummaryAndTier(String $summary, String $pro_cat_tier1, String $pro_cat_tier2, String $pro_cat_tier3, String $opr_cat_tier1, String $opr_cat_tier2, String $opr_cat_tier3): ?Catalog;

    public function findByTier(String $pro_cat_tier1, String $pro_cat_tier2, String $pro_cat_tier3, String $opr_cat_tier1, String $opr_cat_tier2, String $opr_cat_tier3): ?Catalog;

    public function searchServiceCatalog(String $name, String $channel, String $service_type);
}
