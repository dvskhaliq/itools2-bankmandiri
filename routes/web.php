<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Inbox;
use App\Http\Resources\Inbox as InboxResource;
use App\Http\Resources\WhatsappDispatcherMessages as WADispatcherMsgResource;
use App\WhatsappDispatcherMessages;
use Illuminate\Support\Facades\Route;

Auth::routes();
/* MAIN CONTROLLER */
Route::get('/', 'MainController@index');
Route::get('/test/resource', function () {
    return new WADispatcherMsgResource(WhatsappDispatcherMessages::find(136));
});

Route::get('/test/chart/{support_group_id}', 'MainController@getAllStaffTaskDailyReport');
// Route::post('create_incident_incoming/whatapp/upload_image','MainController@uploadImage');
Route::get('/side/notify/pendingticket', 'BaseController@countPendingTicket');
Route::get('/product_categorization_tier1', 'RemedyCategorizationController@getProductCategoryTier1');
Route::post('/product_categorization_tier2', 'RemedyCategorizationController@getProductCategoryTier2');
Route::post('/product_categorization_tier3', 'RemedyCategorizationController@getProductCategoryTier3');
Route::post('/operational_category_tier_1', 'RemedyCategorizationController@getOprationalCategoryTier1');
Route::post('/operational_category_tier_2', 'RemedyCategorizationController@getOprationalCategoryTier2');
Route::post('/operational_category_tier_3', 'RemedyCategorizationController@getOprationalCategoryTier3');

// Service
Route::post('/service', 'RemedyCategorizationController@getServices');

Route::get('/employee_remedy/support_company', 'SupportGroupAssigneeRemedyController@getAssignedSupportCompany');
Route::post('/employee_remedy/support_organization', 'SupportGroupAssigneeRemedyController@getAssignedSupportOrganization');
Route::post('/employee_remedy/support_group', 'SupportGroupAssigneeRemedyController@getAssignedSupportGroup');
Route::post('/employee_remedy/assignee', 'SupportGroupAssigneeRemedyController@getAssigned');
Route::post('/employee_remedy/customer_profile', 'EmployeeRemedyController@getCustomerProfile');
// Route::post('/employee_remedy/agent_name', 'EmployeeRemedyController@getAssignee');
Route::post('/employee_remedy/assignee_login_id', 'SupportGroupAssigneeRemedyController@getAssignedByLoginId');
Route::get('/employee_remedy/all_support_group', 'SupportGroupAssigneeRemedyController@getAllSupportGroup');



/* TELEGRAM LIVECHAT */
Route::group(['prefix' => 'telegram'], function () {
    // Route::get('/telegram/livechat', 'LiveChatController@index');
    Route::group(['prefix' => 'livechat'], function () {
        Route::get('/init/{request_id}', 'LiveChatController@initChat');
        Route::get('/get/{request_id}', 'LiveChatController@getChat');
        // Route::get('/redirect','LiveChatController@redirect');
        // Route::post('/booklivemessage', 'LiveChatController@bookLiveMessage');
        Route::post('/redirecttolivechat', 'LiveChatController@redirectToLivechat');
        Route::post('/dismissfromlivechat', 'LiveChatController@dismissFromLivechat');
        Route::post('/rejectchatrequest', 'LiveChatController@rejectTaskLiveChat');
        Route::post('/closure', 'LiveChatController@ticketClosureLiveChat');
        Route::post('/updateincident', 'LiveChatController@updateIncidentLiveChat');
        Route::group(['prefix' => 'message'], function () {
            // Route::post('/load', 'MainController@streamChatRequests');
            Route::post('/from/customer', 'LiveChatController@postMessageFromCustomer');
            Route::post('/from/agent', 'LiveChatController@postMessageFromAgent');
            // Route::get('/load/new', 'LiveChatController@streamNewChatRequests');
            Route::get('/load/chat/history/{request_id}', 'LiveChatController@getSummaryChatHistory');
        });
    });
});

/* DASHBOARD */
Route::group(['prefix' => 'dashboard'], function () {

    Route::post('/workinfo/upload_file', 'MainController@uploadFile');
    Route::delete('/workinfo/delete_file', 'MainController@deleteFile');

    Route::get('/agent/{service_type}', 'MainController@sdAgentDashboard');
    Route::get('/agentSDS/{service_type}', 'SDSpecialistController@sdAgentDashboard');

    Route::get('/workingtask/fetch', 'MainController@searchWorkingTask');
    Route::get('/workingtask/get_note', 'MainController@getNote');

    Route::group(['prefix' => 'requestqueue'], function () {
        Route::post('/pick', 'MainController@selectRequest');
        Route::post('/view', 'MainController@viewRequest');
        Route::post('/book', 'MainController@bookRequest');
        Route::get('/request/fetch', 'MainController@searchRequestQueue');
    });

    Route::group(['prefix' => 'request'], function () {
        Route::get('/updated/{category}', 'MainController@streamNewRequest');
        // Route::post('/delegate', 'MainController@delegateTask');
        // Route::post('/close', 'MainController@closeTask');
        Route::post('/search/servicecatalog', 'RemedyCategorizationController@searchServiceCatalog');
        Route::post('/delegateWO', 'MainController@delegateWO');
        Route::post('/closeWO', 'MainController@updateWO');
    });

    Route::group(['prefix' => 'stats'], function () {
        Route::post('/search/taskbydate', 'ReportingController@getTaskReportByDate');
    });

    Route::group(['prefix' => 'history'], function () {
        Route::get('/request/fetch', 'HistoryController@searchRequestHistory');
        // Route::post('/searchtask', 'HistoryController@searchAgentTask')
        //     ->middleware('can:dashboard-manager');
    });

    Route::group(['prefix' => 'incident'], function () {
        Route::post('/close', 'MainController@closeIncident');
        Route::post('/update', 'MainController@updateIncident');
        Route::post('/delegate', 'MainController@delegateIncident');
        Route::post('/create/staging', 'MainController@createStagingIncident');
        Route::post('/create/livechat', 'MainController@createLivechatIncident');
        Route::post('/create/{service_type}', 'MainController@createIncomingIncident');
        // Route::post('/create/staging/upload_image', 'MainController@uploadDispatcherImage');  /* Pindah ke WhatsApp Dispatcher Controller */
        // Route::delete('/create/staging/delete_image', 'MainController@deleteDispatcherImage'); /* Pindah ke WhatsApp Dispatcher Controller */
        // Route::post('/create/staging/add_feedback', 'MainController@addDispatcherFeedback'); /* Pindah ke WhatsApp Dispatcher Controller */
        // Route::post('/create/staging/update', 'MainController@updateStagingIncident'); /* Pindah ke WhatsApp Dispatcher Controller */
        // Route::post('/create/staging/remedyticket', 'MainController@createStagingIncidentToRemedy'); /* Pindah ke WhatsApp Dispatcher Controller */
        // Route::post('/create/staging/close', 'MainController@closeDispatcherTicket'); /* Pindah ke WhatsApp Dispatcher Controller */
        // Route::post('/create/staging/mark/{mark}', 'MainController@markForDispatcherTicket'); /* Pindah ke WhatsApp Dispatcher Controller */
        Route::post('/changeadmin', 'MainController@changeAdminDispatcher');
        Route::post('/update/tier', 'MainController@updateTier');
        Route::get('/report/index', 'IncidentInterfaceController@index')
            ->middleware('can:dashboard-manager');
        Route::get('/report/index/fetch', 'IncidentInterfaceController@fetchHPDIncidentInterface');
        Route::post('/report/index/populatebyyear', 'IncidentInterfaceController@populateHPDIncidentInterfaceByYear');
        Route::post('/report/index/populatebymonth', 'IncidentInterfaceController@populateHPDIncidentInterfaceByMonth');
        Route::post('/report/index/populatebydate', 'IncidentInterfaceController@populateHPDIncidentInterfaceByDate');
        Route::post('/report/index/populatebyyear/source', 'IncidentInterfaceController@populateHPDIncidentInterfaceByYearSource');
        Route::post('/report/index/populatebymonth/source', 'IncidentInterfaceController@populateHPDIncidentInterfaceByMonthSource');
        Route::post('/report/index/populatebydate/source', 'IncidentInterfaceController@populateHPDIncidentInterfaceByDateSource');
        Route::post('/report/index/populatebyyear/survey', 'IncidentInterfaceController@populateHPDIncidentInterfaceByYearSurvey');
        Route::post('/report/index/populatebymonth/survey', 'IncidentInterfaceController@populateHPDIncidentInterfaceByMonthSurvey');
        Route::post('/report/index/populatebydate/survey', 'IncidentInterfaceController@populateHPDIncidentInterfaceByDateSurvey');
        Route::post('/report/index/populatebyyear/sourcesurvey', 'IncidentInterfaceController@populateHPDIncidentInterfaceByYearSourceSurvey');
        Route::post('/report/index/populatebymonth/sourcesurvey', 'IncidentInterfaceController@populateHPDIncidentInterfaceByMonthSourceSurvey');
        Route::post('/report/index/populatebydate/sourcesurvey', 'IncidentInterfaceController@populateHPDIncidentInterfaceByDateSourceSurvey');
    });

    Route::group(['prefix' => 'manager'], function () {
        Route::post('/searchtask', 'ManagerDashboard@searchAgentTask')
            ->middleware('can:dashboard-manager');
        // Route::get('/history/request/fetch', 'ManagerDashboard@searchRequestHistoryManager')
        //     ->middleware('can:dashboard-manager');
        Route::group(['prefix' => 'requestqueue'], function () {
            Route::post('/pick', 'ManagerDashboard@selectRequest'); //Overriding maincontroller
        });

        Route::post('/request/delegate', 'ManagerDashboard@delegateWorkOrderManager');
        Route::post('/incident/delegate', 'ManagerDashboard@delegateIncidentManager');
        Route::post('/incident/close', 'ManagerDashboard@updateIncident') //Overriding maincontroller
            ->middleware('can:dashboard-manager');
        Route::post('/request/close', 'ManagerDashboard@updateWO') //Overriding maincontroller
            ->middleware('can:dashboard-manager');
        Route::get('/{support_group_id}', 'ManagerDashboard@sdManagerDashboard')
            ->middleware('can:dashboard-manager');
        Route::post('/search/totalrequest', 'ReportingController@searchAllStaffTotalRequestByChannel')
            ->middleware('can:dashboard-manager');
        Route::post('/search/hourlyrequest', 'ReportingController@searchAllStaffTaskHourlyReport')
            ->middleware('can:dashboard-manager');
        Route::post('/search/dailyrequest', 'ReportingController@searchAllStaffTaskDailyReport')
            ->middleware('can:dashboard-manager');
    });
});

/* BDS SERVER */
Route::group(['prefix' => 'bds_server'], function () {
    Route::get('/index', 'BDSServerController@index')
        ->middleware('can:bds-server');

    Route::get('/page', 'BDSServerController@page')
        ->middleware('can:bds-server');

    Route::get('/search', 'BDSServerController@searchData')
        ->middleware('can:bds-server');

    Route::post('/disconnect', 'BDSServerController@setDisconnectDatabase')
        ->middleware('can:bds-server');

    Route::post('/connect', 'BDSServerController@setConnectToDatabase')
        ->middleware('can:bds-server');
});

Route::post('/bds_server/search/', 'BDSServerController@findByKodeCabang');
/* END BDS SERVER */

/* BDS USER PROFILE */
Route::group(['prefix' => 'user_profile'], function () {
    Route::get('/index', 'BDSUserProfileController@index')
        ->middleware('can:bds-user-profile');

    Route::get('/list', 'BDSUserProfileController@getList')
        ->middleware('can:bds-document-inventory');

    Route::get('/list_holder', 'BDSUserProfileController@listHolder')
        ->middleware('can:bds-document-inventory');

    Route::post('/selected', 'BDSUserProfileController@userProfileSelected')
        ->middleware('can:bds-user-profile');

    Route::post('/tran_limit_selected', 'BDSUserProfileController@tranLimitSelected')
        ->middleware('can:bds-user-profile');

    Route::get('/search', 'BDSUserProfileController@findList')
        ->middleware('can:bds-user-profile');

    Route::post('/set_sign_off', 'BDSUserProfileController@setSignOff')
        ->middleware('can:bds-user-profile');

    Route::post('/set_monetary_user', 'BDSUserProfileController@setMonetaryUser')
        ->middleware('can:bds-user-profile');

    Route::post('/set_ctt', 'BDSUserProfileController@setCTT')
        ->middleware('can:bds-user-profile');

    Route::post('/set_line_status', 'BDSUserProfileController@setLineStatus')
        ->middleware('can:bds-user-profile');

    Route::get('/get_head_teller', 'BDSUserProfileController@getHeadTeller')
        ->middleware('can:bds-user-profile');
});

Route::post('/user_profile/search_user_id', 'BDSUserProfileController@findUserId');
Route::post('/user_profile/search_level_id', 'BDSUserProfileController@findLevelId');
/* END BDS USER PROFILE */

/* BDS KARTU INSTAN */
Route::group(['prefix' => 'kartu_instan'], function () {
    Route::get('/index', 'BDSKartuInstanController@index')
        ->middleware('can:bds-document-inventory');

    Route::get('/page', 'BDSKartuInstanController@getList')
        ->middleware('can:bds-document-inventory');

    Route::post('/filter', 'BDSKartuInstanController@getFilter')
        ->middleware('can:bds-document-inventory');

    Route::get('/search', 'BDSKartuInstanController@findList')
        ->middleware('can:bds-document-inventory');

    Route::post('/submit', 'BDSKartuInstanController@submitKartuInstan')
        ->middleware('can:bds-document-inventory');

    Route::post('/submit_all', 'BDSKartuInstanController@submitKartuInstanSubmitAll')
        ->middleware('can:bds-document-inventory');

    Route::post('/search_serial_no', 'BDSKartuInstanController@findSerialNo')
        ->middleware('can:bds-document-inventory');
});
/* END BDS KARTU INSTAN */

/* BDS SURAT BERHARGA */
Route::group(['prefix' => 'surat_berharga'], function () {
    Route::get('/index', 'BDSSuratBerhargaController@index')
        ->middleware('can:bds-document-inventory');

    Route::get('/list', 'BDSSuratBerhargaController@getList')
        ->middleware('can:bds-document-inventory');

    Route::post('/filter', 'BDSSuratBerhargaController@getFilter')
        ->middleware('can:bds-document-inventory');

    Route::get('/search', 'BDSSuratBerhargaController@findList')
        ->middleware('can:bds-document-inventory');

    Route::post('/submit', 'BDSSuratBerhargaController@submitSuratBerharga')
        ->middleware('can:bds-document-inventory');

    Route::post('/submit_all', 'BDSSuratBerhargaController@submitSuratBerhargaSubmitAll')
        ->middleware('can:bds-document-inventory');

    Route::post('/search_serial_no', 'BDSSuratBerhargaController@findSerialNo')
        ->middleware('can:bds-document-inventory');
});
/* END BDS SURAT BERHARGA */

/* BDS ACCESS DESC */
Route::group(['prefix' => 'access_desc'], function () {
    Route::get('/index', 'BDSAccessDescController@index')
        ->middleware('can:bds-access-desc');

    Route::get('/list', 'BDSAccessDescController@getList')
        ->middleware('can:bds-access-desc');

    Route::get('/search', 'BDSAccessDescController@findList')
        ->middleware('can:bds-access-desc');

    Route::get('/list_kotran', 'BDSAccessDescController@getListKotran')
        ->middleware('can:bds-access-desc');

    Route::post('/insert_access_desc', 'BDSAccessDescController@addAccessDesc')
        ->middleware('can:bds-access-desc');

    Route::post('/delete_access_desc', 'BDSAccessDescController@deleteAccessDesc')
        ->middleware('can:bds-access-desc');
});
/* END BDS ACCESS DESC */

/* BDS EJ2 */
Route::group(['prefix' => 'ej2'], function () {
    Route::get('/index', 'BDSEJ2Controller@index')
        ->middleware('can:bds-ej2');

    Route::get('/search', 'BDSEJ2Controller@findList')
        ->middleware('can:bds-ej2');

    Route::get('/list', 'BDSEJ2Controller@getList')
        ->middleware('can:bds-ej2');

    Route::post('/submit', 'BDSEJ2Controller@submitEJ2')
        ->middleware('can:bds-ej2');
});
/* END BDS EJ2 */

/* BDS PARAMETER */
Route::group(['prefix' => 'parameter'], function () {
    Route::get('/index', 'BDSParameterController@index')
        ->middleware('can:bds-parameter');

    Route::get('/get_param_data', 'BDSParameterController@getParameterDB')
        ->middleware('can:bds-parameter');

    Route::get('/get_param_file', 'BDSParameterController@getParameterFile')
        ->middleware('can:bds-parameter');
});

/* END BDS PARAMETER */

/* BDS ATM */
Route::group(['prefix' => 'atm_detail'], function () {
    Route::get('/index', 'BDSAtmDetailController@index')
        ->middleware('can:bds-atm');

    Route::get('/list', 'BDSAtmDetailController@getList')
        ->middleware('can:bds-atm');

    Route::post('/selected', 'BDSAtmDetailController@listSelected')
        ->middleware('can:bds-atm');

    Route::post('/delete', 'BDSAtmDetailController@submitDeleted')
        ->middleware('can:bds-atm');

    Route::post('/update', 'BDSAtmDetailController@submitUpdated')
        ->middleware('can:bds-atm');

    Route::post('/add', 'BDSAtmDetailController@submitAdd')
        ->middleware('can:bds-atm');
});

Route::group(['prefix' => 'cdm_detail'], function () {
    Route::get('/index', 'BDSCdmDetailController@index')
        ->middleware('can:bds-atm');

    Route::get('/list', 'BDSCdmDetailController@getList')
        ->middleware('can:bds-atm');

    Route::post('/selected', 'BDSCdmDetailController@detailSelected')
        ->middleware('can:bds-atm');

    Route::post('/delete', 'BDSCdmDetailController@submitDeleted')
        ->middleware('can:bds-atm');

    Route::post('/update', 'BDSCdmDetailController@submitUpdated')
        ->middleware('can:bds-atm');

    Route::post('/add', 'BDSCdmDetailController@submitAdd')
        ->middleware('can:bds-atm');
});

Route::group(['prefix' => 'crm_detail'], function () {
    Route::get('/index', 'BDSCrmDetailController@index')
        ->middleware('can:bds-atm');

    Route::get('/list', 'BDSCrmDetailController@getList')
        ->middleware('can:bds-atm');

    Route::post('/selected', 'BDSCrmDetailController@detailSelected')
        ->middleware('can:bds-atm');

    Route::post('/delete', 'BDSCrmDetailController@submitDeleted')
        ->middleware('can:bds-atm');

    Route::post('/update', 'BDSCrmDetailController@submitUpdated')
        ->middleware('can:bds-atm');

    Route::post('/add', 'BDSCrmDetailController@submitAdd')
        ->middleware('can:bds-atm');
});
/* END BDS ATM */

/* BDS BRANCH CONF */
Route::group(['prefix' => 'branch_conf'], function () {
    Route::get('/index', 'BDSBranchConfController@index')
        ->middleware('can:bds-branch-conf');

    Route::get('/get_data', 'BDSBranchConfController@getData')
        ->middleware('can:bds-branch-conf');
});
/* END BDS BRANCH CONF */

/* LOS */
Route::group(['prefix' => 'los_consumer'], function () {
    Route::get('/index', 'LosConsumerController@index')
        ->middleware('can:los');

    Route::post('/search_data', 'LosConsumerController@findDataByApRegNo')
        ->middleware('can:los');

    Route::post('/update_ap_currtrcode', 'LosConsumerController@submitUpdateApCurrtrcode')
        ->middleware('can:los');

    Route::post('/update_ap_nexttrby', 'LosConsumerController@submitUpdateApNexttrby')
        ->middleware('can:los');
});

Route::group(['prefix' => 'fingerprint_status'], function () {
    Route::get('/index', 'LosFingerprintStatusController@index')
        ->middleware('can:los');
});
/* END LOS */

/* BDS SERVER CONFIGURATION */
Route::group(['prefix' => 'bds_server_configuration'], function () {
    Route::get('/index', 'ConfigurationBDSServerController@index')
        ->middleware('can:bds-server-configuration');

    Route::get('/page', 'ConfigurationBDSServerController@page')
        ->middleware('can:bds-server-configuration');

    Route::get('/search', 'ConfigurationBDSServerController@searchData')
        ->name('bds_server')
        ->middleware('can:bds-server');

    Route::post('/selected', 'ConfigurationBDSServerController@dataSelected')
        ->middleware('can:bds-server-configuration');

    Route::post('/submit_update', 'ConfigurationBDSServerController@submitUpdated')
        ->middleware('can:bds-server-configuration');

    Route::post('/submit_delete', 'ConfigurationBDSServerController@submitDeleted')
        ->middleware('can:bds-server-configuration');

    Route::post('/submit_insert', 'ConfigurationBDSServerController@submitInsert')
        ->middleware('can:bds-server-configuration');

    Route::get('/export_to_excel', 'ConfigurationBDSServerController@submitInsert')
        ->middleware('can:bds-server-configuration');
});
/* END BDS SERVER CONFIGURATION */

/* USER CONFIGURATION */
Route::group(['prefix' => 'user_configuration'], function () {
    Route::get('/index', 'ConfigurationUsersController@index')
        ->middleware('can:user-configuration');

    Route::get('/page', 'ConfigurationUsersController@page')
        ->middleware('can:user-configuration');

    Route::post('/select_user', 'ConfigurationUsersController@detailSelected');

    Route::post('/submit_update', 'ConfigurationUsersController@submitUpdate');
    Route::post('/update_password', 'ConfigurationUsersController@updatePassword');

    Route::post('/search', 'ConfigurationUsersController@searchUser');

    Route::post('/get_assignee', 'ConfigurationUsersController@getAssignee');

    Route::post('/search/bygroup', 'ConfigurationUsersController@searchUserByGroup');

    Route::post('/submit_insert', 'ConfigurationUsersController@submitInsert')
        ->middleware('can:user-configuration');

    Route::post('/user_roles', 'ConfigurationUsersController@detailSelectedRoles')
        ->middleware('can:user-configuration');

    Route::get('/roles_list', 'ConfigurationUsersController@rolesList')
        // ->name('user_configuration')
        ->middleware('can:user-configuration');

    Route::post('/add_roles', 'ConfigurationUsersController@addRoles')
        // ->name('user_configuration')
        ->middleware('can:user-configuration');

    // Route::post('/delete_roles', 'ConfigurationUsersController@deleteRole') Not Used
    //     ->name('user_configuration')
    //     ->middleware('can:user-configuration');

    Route::post('/submit_delete', 'ConfigurationUsersController@submitDelete')
        // ->name('user_configuration')
        ->middleware('can:user-configuration');

    Route::get('/user/profile', 'ConfigurationUsersController@loginuser')
        ->middleware('can:user-profile');
});
/* END USER CONFIGURATION */

/* END PENDING TICEKT REMEDY */
Route::group(['prefix' => 'pending_ticket_remedy'], function () {
    Route::get('/index', 'PendingTicketRemedyController@index')
        ->middleware('can:pending-ticket-remedy');

    Route::post('/retryprocess', 'PendingTicketRemedyController@submitRetryProcess')
        ->middleware('can:pending-ticket-remedy');

    Route::post('/retryclose', 'PendingTicketRemedyController@submitRetryClose')
        ->middleware('can:pending-ticket-remedy');

    Route::post('/retrydelegate', 'PendingTicketRemedyController@submitRetryDelegate')
        ->middleware('can:pending-ticket-remedy');

    Route::post('/viewtask', 'PendingTicketRemedyController@viewTask')
        ->middleware('can:pending-ticket-remedy');

    Route::post('/savedraft', 'PendingTicketRemedyController@saveDraft')
        ->middleware('can:pending-ticket-remedy');
});

/* END PENDING TICEKT REMEDY */
// Route::get('/pending_ticket_remedy/index', 'PendingTicketRemedyController@index');

Route::get('/services_request/book_work_order', 'SoapController@index');

/* FTP Connect */
Route::group(['prefix' => 'ftp_connect'], function () {
    Route::get('/storage', 'FTPConnectController@index');
    Route::post('/connect', 'FTPConnectController@ftpconnect2');
    Route::post('/upload', 'FTPConnectController@ftpUpload');
    Route::post('/uploadfile', 'FTPConnectController@uploadFile');
    Route::get('/disconnect', 'FTPConnectController@ftpDisconnect');
    Route::get('/refreshlist', 'FTPConnectController@refreshList');
    Route::get('/refreshfilelist', 'FTPConnectController@refreshFileList');
});
/* END FTP Connect */

Route::group(['prefix' => 'sds'], function () {
    Route::post('/incident/create', 'SDSpecialistController@createIncidentSDS');
    /* CPS Server */
    Route::post('/patching/selectTT', 'SDSpecialistController@resultTT');
    Route::post('/patching/updatedataTT', 'SDSpecialistController@updateDataTT');
    Route::post('/search/auditTrail', 'SDSpecialistController@searchAuditTrail');
    /* END CPS Server */
});


/* WhatsApp Dispatcher */
Route::group(['prefix' => 'whatsapp'], function () {
    Route::group(['prefix' => 'dispatcher'], function () {
	// ------------------------------------------------------------------ PERUBAHAN --> 
        Route::post('/create/staging/delete_feedback','WhatsappDispatcherController@deleteDispatcherFeedback');
    // ------------------------------------------------------------------ PERUBAHAN -->
        Route::post('/create/staging/add_feedback', 'WhatsappDispatcherController@addDispatcherFeedback');
        Route::post('/create/staging/upload_image', 'WhatsappDispatcherController@uploadDispatcherImage');
        Route::delete('/create/staging/delete_image', 'WhatsappDispatcherController@deleteDispatcherImage');
        Route::post('/create/staging/update', 'WhatsappDispatcherController@updateStagingIncident');
        Route::post('/create/staging/remedyticket', 'WhatsappDispatcherController@createStagingIncidentToRemedy');
        Route::post('/update/tier', 'WhatsappDispatcherController@updateTier');
        Route::post('/delegate/othergroup', 'WhatsappDispatcherController@delegateOtherGroup');
        Route::get('/get/{request_id}', 'WhatsappDispatcherController@getIncidentWhatsappDispatcher');
        Route::get('/init/{request_id}', 'WhatsappDispatcherController@initIncidentWhatsappDispatcher');
        Route::post('/create/staging/mark/{mark}', 'WhatsappDispatcherController@markForDispatcherTicket');
        Route::post('/create/staging/close', 'WhatsappDispatcherController@closeDispatcherTicket');
    });
});
/* END WhatsApp Dispatcher */

/* PROBLEM MANAGEMENT */
Route::get('/data_management/login', 'ProblemManagementController@showLoginForm')->middleware('guest:problem');
Route::post('/data_management/login', 'ProblemManagementController@login');
Route::group(['prefix' => 'data_management', 'middleware' => 'auth:problem'], function () {
    Route::get('/', 'ProblemController@index');
    Route::get('/logout', 'ProblemManagementController@logout');
    Route::get('/console/{any?}', function () {
    return view('report/public/reporting_problem');
    })->where('any', '^(?!web\/)[\/\w\.-]*');
    Route::group(['prefix' => 'problems'], function () {
        Route::post('/{report_type}', 'ProblemController@getSummary');
        Route::get('/production_issues/fetch', 'ProblemController@fetchProblemProductionIssues')->middleware('is_permitted:production_issues,read');
        Route::post('/production_issues/populate', 'ProblemController@populateProblemProductionIssues')->middleware('is_permitted:production_issues,read');
        // System Availability
        Route::get('/system_availability/fetchYears', 'SystemAvailabilityController@fetchYears')->middleware('is_permitted:system_availability,read');
        Route::get('/system_availability/fetch', 'SystemAvailabilityController@fetchSystemAvailabilities')->middleware('is_permitted:system_availability,read');
        Route::get('/system_availability/fetchSystemName', 'SystemAvailabilityController@fetchSystemName')->middleware('is_permitted:system_availability,read');
        Route::post('/system_availability/getByYear', 'SystemAvailabilityController@fetchSystemAvailByYear')->middleware('is_permitted:system_availability,read');
        Route::post('/system_availability/getByMonth', 'SystemAvailabilityController@fetchSystemAvailByMonth')->middleware('is_permitted:system_availability,read');
        Route::post('/system_availability/getByMonthAvg', 'SystemAvailabilityController@fetchSystemAvailByMonthAvg')->middleware('is_permitted:system_availability,read');
        Route::post('/system_availability/getByDate', 'SystemAvailabilityController@fetchSystemAvailByDate')->middleware('is_permitted:system_availability,read');
        Route::post('/system_availability/create', 'SystemAvailabilityController@createSystemAvail')->middleware('is_permitted:system_availability,create');
        Route::put('/system_availability/update', 'SystemAvailabilityController@updateSystemAvail')->middleware('is_permitted:system_availability,update');
        Route::put('/system_availability/updatedesc', 'SystemAvailabilityController@updateDescSystemAvail')->middleware('is_permitted:system_availability,update');
        Route::delete('/system_availability/delete/{id}', 'SystemAvailabilityController@deleteSystemAvail')->middleware('is_permitted:system_availability,delete');
        // Master System Availability
        Route::get('/master_system/fetch', 'MasterSystemController@fetchMasterSystems')->middleware('is_permitted:system_availability,read');
        Route::post('/master_system/create', 'MasterSystemController@createMasterSystem')->middleware('is_permitted:system_availability,create');
        Route::put('/master_system/update', 'MasterSystemController@updateMasterSystem')->middleware('is_permitted:system_availability,update');
        Route::delete('/master_system/delete/{id}', 'MasterSystemController@deleteMasterSystem')->middleware('is_permitted:system_availability,delete');
        // Response Time
        Route::get('/response_time/fetch', 'ResponseTimeController@fetchResponseTime')->middleware('is_permitted:response_time,read');
        Route::get('/response_time/fetchYears', 'ResponseTimeController@fetchYears')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/getByDate', 'ResponseTimeController@fetchResponseTimeByDate')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/getByMonth', 'ResponseTimeController@fetchResponseTimeByMonth')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/getByMonthAvg', 'ResponseTimeController@fetchResponseTimeByMonthAvg')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/getByYear', 'ResponseTimeController@fetchResponseTimeByYear')->middleware('is_permitted:response_time,read');
        // Response Time Apdy
        Route::get('/response_time/apdy/fetch', 'AppdynamicsController@fetchResponseTime')->middleware('is_permitted:response_time,read');
        Route::get('/response_time/apdy/fetchYears', 'AppdynamicsController@fetchYears')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/apdy/getByDate', 'AppdynamicsController@fetchResponseTimeByDate')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/apdy/getByMonth', 'AppdynamicsController@fetchResponseTimeByMonth')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/apdy/getByMonthAvg', 'AppdynamicsController@fetchResponseTimeByMonthAvg')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/apdy/getByYear', 'AppdynamicsController@fetchResponseTimeByYear')->middleware('is_permitted:response_time,read');
        Route::put('/response_time/apdy/update', 'AppdynamicsController@updateResponseTime')->middleware('is_permitted:response_time,update');
        // Response Time Common
        Route::post('/response_time/common/fetch', 'ResponseTimeCommonController@fetchResponseTime')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/common/fetchYears', 'ResponseTimeCommonController@fetchYears')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/common/getByDate', 'ResponseTimeCommonController@fetchResponseTimeByDate')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/common/getByMonth', 'ResponseTimeCommonController@fetchResponseTimeByMonth')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/common/getByMonthAvg', 'ResponseTimeCommonController@fetchResponseTimeByMonthAvg')->middleware('is_permitted:response_time,read');
        Route::post('/response_time/common/getByYear', 'ResponseTimeCommonController@fetchResponseTimeByYear')->middleware('is_permitted:response_time,read');
        // Dashboard IT
        Route::get('/dashboard_it/fetchYears', 'DashboardItController@fetchYears')->middleware('is_permitted:dashboard_it,read');
        Route::get('/dashboard_it/fetch', 'DashboardItController@fetchDashboardIt')->middleware('is_permitted:dashboard_it,read');
        Route::post('/dashboard_it/getByYear', 'DashboardItController@fetchDashboardItByYear')->middleware('is_permitted:dashboard_it,read');
        Route::post('/dashboard_it/getByMonth', 'DashboardItController@fetchDashboardItByMonth')->middleware('is_permitted:dashboard_it,read');
        Route::post('/dashboard_it/getByMonthAvg', 'DashboardItController@fetchDashboardItByMonthAvg')->middleware('is_permitted:dashboard_it,read');
        Route::post('/dashboard_it/getByDate', 'DashboardItController@fetchDashboardItByDate')->middleware('is_permitted:dashboard_it,read');
        // Success Rate
        Route::get('/success_rate/fetchYears', 'SuccessRateController@fetchYears')->middleware('is_permitted:success_rate,read');
        Route::get('/success_rate/fetch', 'SuccessRateController@fetchSuccessRate')->middleware('is_permitted:success_rate,read');
        Route::post('/success_rate/getByYear', 'SuccessRateController@fetchSuccessRateByYear')->middleware('is_permitted:success_rate,read');
        Route::post('/success_rate/getByMonth', 'SuccessRateController@fetchSuccessRateByMonth')->middleware('is_permitted:success_rate,read');
        Route::post('/success_rate/getByMonthAvg', 'SuccessRateController@fetchSuccessRateByMonthAvg')->middleware('is_permitted:success_rate,read');
        Route::post('/success_rate/getByDate', 'SuccessRateController@fetchSuccessRateByDate')->middleware('is_permitted:success_rate,read');
        // User Config
        Route::get('/user/current_user', 'ConfigurationUsersController@fetchCurrentUser')->middleware('is_permitted:users,read');
        Route::get('/user/list_user', 'ConfigurationUsersController@fetchListUser')->middleware('is_permitted:users,read');
        Route::get('/user/list_user_itools', 'ConfigurationUsersController@fetchListUserItools')->middleware('is_permitted:users,read');
        Route::post('/user/check_permission', 'ConfigurationUsersController@checkPermission')->middleware('is_permitted:users,read');
        Route::post('/user/activated_users', 'ConfigurationUsersController@activatedUsers')->middleware('is_permitted:users,update');
        Route::post('/user/deactivated_users', 'ConfigurationUsersController@deactivatedUsers')->middleware('is_permitted:users,update');
        Route::post('/user/submit_update', 'ConfigurationUsersController@submitUpdate')->middleware('is_permitted:users,read');
        // Navigation
        Route::get('/menu', 'ConfigurationUsersController@showMenu');
        // PM Roles
        Route::get('/roles', 'PMRolesController@getRoles')->middleware('is_permitted:roles,read');
        Route::post('/roles/create', 'PMRolesController@createRole')->middleware('is_permitted:roles,create');
        Route::put('/roles/update', 'PMRolesController@updateRole')->middleware('is_permitted:roles,update');
        Route::delete('/roles/delete/{roleId}', 'PMRolesController@deleteRole')->middleware('is_permitted:roles,delete');
        // PM Features
        Route::get('/features', 'PMFeaturesController@getFeatures');
        Route::get('/features/{role_id}', 'PMFeaturesController@getInactiveFeatures');
        Route::get('/features/owned/{role_id}', 'PMFeaturesController@getOwnedFeatures');
        Route::delete('/features/delete/{rolefeatures_id}', 'PMFeaturesController@deleteOwnedFeature');
        // PM Master Apdy Bridge
        Route::get('/master_apdy_bridge/fetch', 'PMMasterApdyBridgeController@fetchPMMasterApdyBridge')->middleware('is_permitted:response_time,read');
        Route::post('/master_apdy_bridge/create', 'PMMasterApdyBridgeController@createPMMasterApdyBridge')->middleware('is_permitted:response_time,create');
        Route::put('/master_apdy_bridge/update', 'PMMasterApdyBridgeController@updatePMMasterApdyBridge')->middleware('is_permitted:response_time,update');
        Route::delete('/master_apdy_bridge/delete/{id}', 'PMMasterApdyBridgeController@deletePMMasterApdyBridge')->middleware('is_permitted:response_time,delete');
        Route::get('/master_apdy_bridge/fetchSystemName', 'PMMasterApdyBridgeController@fetchSystemName')->middleware('is_permitted:response_time,read');
        Route::post('/master_apdy_bridge/create/longranged', 'AppdynamicsController@getBetweenLongRange')->middleware('is_permitted:response_time,create');
        // Bulk create relation between incident and problem
        Route::post('/bulk/relation/create', 'BulkController@createRelation')->middleware('is_permitted:create_relation,create');
        Route::post('/bulk/relation/stop', 'BulkController@stopCreateRelation')->middleware('is_permitted:create_relation,create');
        Route::post('/bulk/incident/create', 'BulkController@createIncident')->middleware('is_permitted:create_incident,create');
        Route::post('/bulk/incident/update', 'BulkController@updateIncident')->middleware('is_permitted:update_incident,create');
        Route::post('/bulk/incident/fetch', 'BulkController@fetchIncident')->middleware('is_permitted:update_incident,read');
        Route::post('/bulk/problem/update', 'BulkController@updateProblem')->middleware('is_permitted:update_problem,create');
        Route::post('/bulk/migrate_login', 'BulkController@migrateLoginId')->middleware('is_permitted:update_problem,create');
        Route::post('/bulk/check_migration', 'BulkController@checkMigration')->middleware('is_permitted:update_problem,create');
        Route::post('/bulk/process/stop', 'BulkController@stopProcess')->middleware('is_permitted:update_problem,create');
        Route::post('/bulk/update_knowledge', 'BulkController@updateKnowledgeCategorization')->middleware('is_permitted:update_problem,create');
    });
});


/* OD COMMAND CENTER */
Route::get('/onduty/login', 'ODController@showLoginForm')->middleware('guest:onduty');
Route::post('/onduty/login', 'ODController@login');
Route::group(['prefix'=>'onduty'],function(){
    Route::get('/dashboardod','ODController@oddashboarddev');
    Route::get('/dashboard','ODController@index');
    Route::post('/incident/createincidentod','ODController@createIncomingIncidentOd');
    Route::post('/incident/createaproveincidentremedy','ODController@createIncomingIncidentRemedy');
    Route::post('/incident/updateincidentod','ODController@updateIncidentOd');
    Route::get('/incident/listincidentod/{inboxId}/{oddashboard}','ODController@oddashboard');
    Route::post('/incident/searchIncidentByOd','ODController@searchIncidentByOd');
    Route::post('/incident/sendMessageIncidentByOd','ODController@sendMessageIncidentByOd');
    Route::post('/incident/customer_profile', 'ODController@getCustomerProfileSecondLayer');
    Route::get('/logout', 'ODController@logout');
});

/* SDS DASHBOARD */
Route::group(['prefix' => 'sds'], function () {
    Route::get('/', 'Sds\SdsController@index');
    Route::get('/dashboard/{any?}', function () {
        return view('sds/public/index');
    })->where('any', '^(?!web\/)[\/\w\.-]*');
    Route::group(['prefix' => 'query'], function () {
        Route::get('/fetch', 'Sds\SdsController@fetch');
    });
});
