<?php

namespace App\ServiceHandler\DataLayer;

use App\Survey;

class SurveyImp implements SurveyService
{
    public $status = false;
    public $message = 'this is default message';

    public function saveSurvey($array_attr)
    {
        try {
            $survey = new Survey();
            $survey->request_id = (array_key_exists("request_id", $array_attr) ? $array_attr["request_id"] : null);
            $survey->survey = (array_key_exists("survey", $array_attr) ? $array_attr["survey"] : null);
            $survey->submitter_id = (array_key_exists("submitter_id", $array_attr) ? $array_attr["submitter_id"] : null);
            $survey->submit_date = date('Y-m-d H:i:s');
            $survey->save();
            return $survey;
        } catch (\Exception $th) {
            $this->status = false;
            $this->message = "message save failed: " . $th->__toString();
            return $th;
        }
    }

    public function deleteSurvey($request_id)
    {
        $data = Survey::find($request_id);
        $data->delete();
    }

    public function findSurvey($request_id)
    {
        return Survey::find($request_id);
    }

    // public function findWorkinfobyId($id)
    // {
    //     return InboxWorkinfolog::where('request_id',$id)->first();
    // }
}