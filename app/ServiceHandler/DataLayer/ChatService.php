<?php

namespace App\ServiceHandler\DataLayer;

use App\ChatLiveMessages as LiveChat;
use App\Inbox;
use App\Http\Resources\Chat;
use App\Http\Resources\ChatCollection;

interface ChatService
{
    public function addChat(Inbox $request,String $sender_type,String $message):Chat;    

    public function find(Int $message_id): LiveChat;

    public function findDTO(Int $message_id): Chat;
    
    public function getListByRequestId(Int $request_id): LiveChat;

    public function getListDTOByRequestId(Int $request_id): ChatCollection;

    public function postTelegramFeedback(Int $request_id, $content);

    public function postSMSFeedback($number, $content);
}
