<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceChannel extends Model
{

    protected $fillable = ['service_channel'];
    protected $table = 'service_channel';
    public $timestamps = false;

    public function servicecatalog(){
        return $this->hasMany(ServiceCatalog::class,'service_channel_name','encoding_name');
    }

    public function servicetype(){
        return $this->belongsTo(ServiceType::class,'service_type_name','encoding_name');
    }
    
}