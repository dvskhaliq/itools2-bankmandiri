<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatLiveMessages extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['sender', 'content', 'timestamp'];
    public $timestamps = false;

    public function inbox()
    {
        return $this->belongsTo(Inbox::class, 'request_id', 'id');
    }
}
