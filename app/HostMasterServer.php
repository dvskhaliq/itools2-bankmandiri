<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostMasterServer extends Model
{
    protected $table = 'host_master_servers';
    public $incrementing = false;
    public $timestamps = false;
}