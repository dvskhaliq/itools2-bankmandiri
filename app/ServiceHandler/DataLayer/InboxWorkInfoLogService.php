<?php

namespace App\ServiceHandler\DataLayer;

use App\Http\Resources\InboxWorkInfoLog as WorkInfoResource;

interface InboxWorkInfoLogService
{
    public function addAttachFile($array_attr);

    public function deleteAttachFile($workinfo_id);

    public function findWorkinfo($workinfo_id);

    public function findWorkinfobyId($id);
}
