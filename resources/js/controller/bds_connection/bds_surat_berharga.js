import "../../bootstrap";
import "vuetify/dist/vuetify.min.css";
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from "vuex";
import VeeValidate from "vee-validate";
import Vue from "vue";
import Loading from "vue-loading-overlay";
import Vuetify from "vuetify";

window.Vue = Vue;
window.VueSelect = require("vue-select");
window.events = new Vue();

window.flash = function(message) {
    window.events.$emit("flash", message);
};

Vue.component("componen-table", () =>
    import(
        "../../components/bds_connection/document_inventory/surat_berharga/BDSSuratBerharga.vue"
    )
);
Vue.component("componen-set-allocation", () =>
    import(
        "../../components/bds_connection/document_inventory/surat_berharga/SetBDSSuratBerharga.vue"
    )
);
Vue.component("v-select", VueSelect.VueSelect);
Vue.component("flash", () => import("../../components/FlashMessage2.vue"));
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Loading);

import onErrorResponse from "../../response/index.";

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: "errors", // change if property conflicts
    events: "input|blur",
    fieldsBagName: "fields",
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: "validations", // the nested key under which the validation messages will be located
    inject: true,
    locale: "en",
    validity: false,
    useConstraintAttrs: true
};
Vue.use(VeeValidate, config);

const vuetify = new Vuetify();

const bds_surat_berharga = new Vue({
    el: "#bds_surat_berharga",
    vuetify,
    data: {
        data_surat_berharga: null,
        ismodal: false,
        list_selected: null,
        data_holder: [],
        isloaded: false,
        selectedoc: "List Doc. Belum Kembali"
    },

    mounted() {
        this.getData();
        this.listHolder();
    },

    methods: {
        getData(spoilt = null) {
            console.log(spoilt);
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.isloaded = true;
            this.ismodal = true;

            console.log({ isiSpoitl: spoilt });

            if (spoilt === null) {
                axios
                    .get("/surat_berharga/list")
                    .then(response => {
                        this.data_surat_berharga = response.data;
                        loader.hide();
                    })
                    .catch(error => {
                        // onErrorResponse(error);
                        onErrorResponse(error);
                    });
            } else {
                console.log({ spoiltSelected: spoilt });
                if (spoilt.selected == "List Doc. Belum Kembali") {
                    var data = { spoilt: "N" };
                } else {
                    var data = { spoilt: "Y" };
                }
                axios
                    .post("/surat_berharga/filter", data)
                    .then(response => {
                        this.data_surat_berharga = response.data;
                        loader.hide();
                    })
                    .catch(error => {
                        // onErrorResponse(error);
                        onErrorResponse(error);
                    });
            }
        },

        onChangeFilter(reqResponse) {
            console.log(reqResponse);
            this.selectedoc = reqResponse.selected;
            this.getData(reqResponse);
        },

        listHolder() {
            axios
                .get("/user_profile/list_holder")
                .then(response => {
                    this.data_holder = response.data;
                    this.data_holder.unshift("VAULT");
                })
                .catch(error => {
                    // if (
                    //     error.response.status === 419 ||
                    //     error.response.status === 401
                    // ) {
                    //     window.alert(
                    //         "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                    //     );
                    //     location.reload(false);
                    // } else {
                    //     flash({
                    //         id: "error code status",
                    //         message: error.response.data.message,
                    //         flashtype: "danger"
                    //     });
                    // }
                    onErrorResponse(error);
                });
        },

        submitAll(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.isloaded = true;
            this.ismodal = true;
            var data = { surat_berharga: reqResponse.data };
            axios
                .post("/surat_berharga/submit_all", data)
                .then(response => {
                    if (response.data.status == true) {
                        this.getData();
                        flash({
                            id: "success",
                            message: response.data.result,
                            flashtype: "info"
                        });
                    } else {
                        flash({
                            id: "warning",
                            message: response.data.result,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    onErrorResponse(error);
                    // if (
                    //     error.response.status === 419 ||
                    //     error.response.status === 401
                    // ) {
                    //     window.alert(
                    //         "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                    //     );
                    //     location.reload(false);
                    // } else {
                    //     flash({
                    //         id: "error code status",
                    //         message: error.response.data.message,
                    //         flashtype: "danger"
                    //     });
                    // }
                    // loader.hide();
                });
        },

        submitForm(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.isloaded = true;
            this.ismodal = true;
            if (reqResponse.holder == "VAULT") {
                var data = {
                    serialNo: reqResponse.serialNo,
                    holder: reqResponse.holder,
                    allocatedToday: "N"
                };
            } else {
                var data = {
                    serialNo: reqResponse.serialNo,
                    holder: reqResponse.holder.UserID,
                    allocatedToday: "Y"
                };
            }

            console.log(data, "data dari submit form");

            axios
                .post("/surat_berharga/submit", data)
                .then(response => {
                    if (response.data.status == true) {
                        if (this.selectedoc == "List Doc. Belum Kembali") {
                            this.getData();
                        } else {
                            this.getData({ selected: this.selectedoc });
                        }

                        flash({
                            id: "success",
                            message: response.data.result,
                            flashtype: "info"
                        });
                    } else {
                        flash({
                            id: "warning",
                            message: response.data.result,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    onErrorResponse(error);
                    // if (
                    //     error.response.status === 419 ||
                    //     error.response.status === 401
                    // ) {
                    //     window.alert(
                    //         "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                    //     );
                    //     location.reload(false);
                    // } else {
                    //     flash({
                    //         id: "error code status",
                    //         message: error.response.data.message,
                    //         flashtype: "danger"
                    //     });
                    // }
                    // loader.hide();
                });
        },

        listSelected(reqResponse) {
            console.log({ listSelected: reqResponse });
            this.list_selected = reqResponse.serial_no;
        }
    }
});
