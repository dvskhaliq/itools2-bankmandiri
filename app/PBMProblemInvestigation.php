<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PBMProblemRelationshipInterfa;

class PBMProblemInvestigation extends Model
{
    protected $casts = [
        'Submit_Date' => 'datetime:Y-m-d H:i:s',
        'Jam_Kejadian' => 'datetime:Y-m-d'
    ];
    protected $connection = 'sqlsrv_remedy';
    protected $table = 'PBM_Problem_Investigation';
    protected $primaryKey = 'Problem_Invetigation_ID';

    public function pbmrelationshipinterface()
    {
      return $this->belongsTo(PBMProblemRelationshipInterfa::class,'Problem_Investigation_ID','problem_id');
    }

}
