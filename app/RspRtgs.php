<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RspRtgs extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'RSP_RTGS';
}
