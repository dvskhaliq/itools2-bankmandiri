<?php

namespace App\ServiceHandler\DataLayer;

use App\Inbox;
use App\Incident;
use App\Workorder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

interface InboxService
{
    public function addByInc(Incident $incident, Int $request_id = null): ?Inbox;

    //add by PMW 03102020 dashboard OD
    public function addIncByFirstLayer(Incident $incident, Int $request_id = null): ?Inbox;

    public function addByWo(Workorder $workorder): ?Inbox;

    public function book($id): Inbox;

    public function find($id): Inbox;

    public function update(Int $request_id, array $array_inbox): ?Inbox;

    //add by PMW 
    public function updateIncOd(Int $request_id, Incident $incident): ?Inbox;

    public function refresh(Inbox $inbox): Inbox;

    public function findPendingTicket(Int $user_id): ?AnonymousResourceCollection;

    public function getByStatusAndCategory(String $process_status, String $service_type, String $service_channel_name = null, String $date = null): ?AnonymousResourceCollection;

    public function getByAssigneeAndStatus(String $employee_assignee_id, String $process_status = null): ?AnonymousResourceCollection;

    public function add(array $array_inbox): ?Inbox;

    public function findByInc($remedy_ticket_id): Inbox;

    //Reporting
    public function getGroupGrandTotalTask(String $support_group_id, String $start_datetime, String $end_datetime, array $status);

    public function getGroupHourlyTotalTask(String $support_group_id, String $start_datetime, String $end_datetime, String $field_name, array $process_status);

    public function getGroupDailyTotalTask(String $support_group_id, String $start_date, String $end_date, String $field_name);

    public function getSingleDailyTaskStats(String $employee_assginee_id, String $start_datetime, String $end_datetime, String $field_name);

    public function getSingleWeeklyTaskStats(String $employee_assginee_id, String $start_date, String $end_date, String $field_name);

    public function getChannelStats(String $employee_assginee_id, String $start_date, String $end_datet, String $status, String $field_name);

    public function getSingleDailyTaskStatsFilter(String $user_id, String $start_datetime, String $end_datetime, String $field_name, String $layanan, String $layananc);
    //End of reporting

    //History
    public function getRequestsHistory(String $support_group_id, String $service_catalog_type, $manager, $checksgp, String $field = "", $searchdate = "", $tosearchdate = "", $search = "");

    public function getRequestListByStatusAndSupportGroupJson($process_status, $channel_name, $employee_assignee_group_id, $field = "", $searchdate = "", $tosearchdate = "", $name = "");

    /* dari Renov*/
    // public function getRequestListByStatusAndSupportGroupJson(array $process_status, array $channel_name, String $employee_assignee_group_id, String $searchdate = null, String $tosearchdate = null, String $search = null);
    /*===========*/

    //End of history
}
