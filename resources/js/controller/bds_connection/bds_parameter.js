require('../../bootstrap');
import 'es6-promise/auto'
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
window.Vue = require('vue');
window.flash = function (message) {
    window.events.$emit('flash', message);
}

window.events = new Vue();

Vue.component('table-bds-parameter-db', ()=> import('../../components/bds_connection/parameter/BDSParameterDB.vue'));
Vue.component('table-bds-parameter-file', ()=> import('../../components/bds_connection/parameter/BDSParameterFile.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));


Vue.use(Vuetify);
Vue.use(Vuex);


const vuetify = new Vuetify();

const bds_parameter = new Vue({
    el: '#bds_parameter',
    vuetify,
    data: {
        data_parameter: null,
        file_parameter: null,
        ismodal: false,
        isloaded: false,
    },

    mounted() {
        this.getData();
        this.getListFile();
    },

    methods: {

        getData() {
            this.isloaded = true;
            this.ismodal = true;
            axios.get(`/parameter/get_param_data`)
            .then((response) => {
                this.data_parameter = response.data.result;
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                flash({
                  id: "error code status",
                  message: error.response.data.message,
                  flashtype: "danger"
                });
              }
            });
        },

        getListFile() {
            this.isloaded = true;
            this.ismodal = true;
            axios.get(`/parameter/get_param_file`)
            .then((response) => {
                this.file_parameter = response.data.result;
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                flash({
                  id: "error code status",
                  message: error.response.data.message,
                  flashtype: "danger"
                });
              }
            });
        },
    },

});
