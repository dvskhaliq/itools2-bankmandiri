<?php

namespace App\ServiceHandler\DataLayer;
use Illuminate\Database\Eloquent\Collection;

interface MasterSystemService
{
    public function getMasterSystem(): ?Collection;
    public function createMasterSystem($payload);
    public function updateMasterSystem($payload);
    public function deleteMasterSystem(int $id);
}
