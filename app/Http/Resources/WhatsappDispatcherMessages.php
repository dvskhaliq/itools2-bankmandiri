<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WhatsappDispatcherMessages extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "requestId" => $this->request_id,
            "content" => $this->content,
            "senderName" => $this->user->name,
            "senderId" => $this->sender_id,
            "timestamp" => date('Y-m-d H:i:s',strtotime($this->timestamp)),            
        ];
    }
}
