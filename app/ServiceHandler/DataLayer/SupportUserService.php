<?php

namespace App\ServiceHandler\DataLayer;

use App\SupportGroupAssigneeRemedy as SupportUser;

interface SupportUserService
{
    public function findByUserId(?Int $user_id):? SupportUser;

    public function findByPersonIdGroupId(String $person_id, String $group_id): SupportUser;

    public function findByCompanyOrgGroupName($company_name, $organization_name, $group_name, $assignee_name = null): ?SupportUser;    

    // public function findByFullNameGroupName(array $assignee):? SupportUser;//Not Used
}