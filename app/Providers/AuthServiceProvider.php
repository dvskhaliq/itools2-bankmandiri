<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPostPolicies();
    }

    public function registerPostPolicies()
    {
        /* DASHBOARD AGENT IT*/
        Gate::define('dashboard-agent', function ($user) {
            return $user->hasAccess(['dashboard-agent']);
        });

        /* DASHBOARD AGENT CABANG*/
        Gate::define('dashboard-agent-branch', function ($user) {
            return $user->hasAccess(['dashboard-agent-branch']);
        });

        /* DASHBOARD AGENT CARDS*/
        Gate::define('dashboard-agent-cards', function ($user) {
            return $user->hasAccess(['dashboard-agent-cards']);
        });

        /* DASHBOARD AGENT SDS*/
        Gate::define('dashboard-agent-sds', function ($user) {
            return $user->hasAccess(['dashboard-agent-sds']);
        });

        /* DASHBOARD MANAGER*/
        Gate::define('dashboard-manager', function ($user) {
            return $user->hasAccess(['dashboard-manager']);
        });
        /* SERVICE REQUEST */
        Gate::define('services-request', function ($user) {
            return $user->hasAccess(['services-request']);
        });

        /* BDS CONNECTION */
        Gate::define('bds-connection', function ($user) {
            return $user->hasAccess(['bds-connection']);
        });

        /* BDS CONNECTION */
        Gate::define('bds-server', function ($user) {
            return $user->hasAccess(['bds-server']);
        });

        /* BDS CONNECTION USER PROFILE */
        Gate::define('bds-user-profile', function ($user) {
            return $user->hasAccess(['bds-user-profile']);
        });

        /* BDS CONNECTION DOCUMENT INVENTORY */
        Gate::define('bds-document-inventory', function ($user) {
            return $user->hasAccess(['bds-document-inventory']);
        });

        /* BDS CONNECTION ACCESS DESC */
        Gate::define('bds-access-desc', function ($user) {
            return $user->hasAccess(['bds-access-desc']);
        });

        /* BDS CONNECTION EJ2 */
        Gate::define('bds-ej2', function ($user) {
            return $user->hasAccess(['bds-ej2']);
        });

        /* BDS CONNECTION PARAMETER */
        Gate::define('bds-parameter', function ($user) {
            return $user->hasAccess(['bds-parameter']);
        });

        /* BDS CONNECTION ATM */
        Gate::define('bds-atm', function ($user) {
            return $user->hasAccess(['bds-atm']);
        });

        /* BDS CONNECTION BRANCH CONF */
        Gate::define('bds-branch-conf', function ($user) {
            return $user->hasAccess(['bds-branch-conf']);
        });

        /* ADMINISTRATOR CONSOLE */
        Gate::define('administrator-console', function ($user) {
            return $user->hasAccess(['administrator-console']);
        });

        /* USER CONFIGURATION*/
        Gate::define('user-configuration', function ($user) {
            return $user->hasAccess(['user-configuration']);
        });

        /* BDS SERVER CONFIGURATION*/
        Gate::define('bds-server-configuration', function ($user) {
            return $user->hasAccess(['bds-server-configuration']);
        });

        /* LOS */
        Gate::define('los', function ($user) {
            return $user->hasAccess(['los']);
        });

        /* PENDING TICEKT REMEDY */
        Gate::define('pending-ticket-remedy', function ($user) {
            return $user->hasAccess(['pending-ticket-remedy']);
        });

        /* USER PROFILE*/
        Gate::define('user-profile', function ($user) {
            return $user->hasAccess(['user-profile']);
        });

        /* add by putut.mukti 27092020 penambahan role dashboard command center */
        /* USER OD COMCEN*/
        Gate::define('dashboard-od-comcen', function ($user) {
            return $user->hasAccess(['dashboard-od-comcen']);
        });

        /* USER 1ST LAYER COMCEN*/
        Gate::define('dashboard-od-first-layer', function ($user) {
            return $user->hasAccess(['dashboard-od-first-layer']);
        });

        /* USER 2ND LAYER COMCEN*/
        Gate::define('dashboard-od-second-layer', function ($user) {
            return $user->hasAccess(['dashboard-od-second-layer']);
        });
        /* end add */
    }
}
