<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SummaryTotalRequest extends Model
{
    public $primaryKey = 'id';
	protected $casts = [ 'id' => 'string' ];
    protected $table = 'summary_total_request';
    public $timestamps = false;
    
}

