import VueRouter from "vue-router";
import Console from "../page/Dashboard";

const appUrl = "/sds";
const routes = [
    {
        path: appUrl + "/dashboard",
        name: "dashboard",
        component: Console,
        meta: {
            auth: true
        }
    },
    
];
const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});
export default router;
