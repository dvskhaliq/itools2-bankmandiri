@extends('sidebar')
@section('content')
<div id="bds_parameter">
    <div class="row bds-parameter">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS Parameter</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="ismodal" info></flash>
                    <div class="panel panel-default tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Parameter DB</a></li>
                            <li><a href="#tab-second" role="tab" data-toggle="tab">Parameter File</a></li>
                        </ul>
                    </div>

                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-first">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <table-bds-parameter-db :data_parameter="data_parameter" :isloaded="isloaded" :ismodal="ismodal"></table-bds-parameter-db>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-second">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                    <table-bds-parameter-file :file_parameter="file_parameter" :isloaded="isloaded" :ismodal="ismodal"></table-bds-parameter-file>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/bds_parameter.js"></script>
@endsection
