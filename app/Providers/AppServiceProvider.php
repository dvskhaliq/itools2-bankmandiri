<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Resource::withoutWrapping();
        // ChatResources::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // OD Comcen Layer
        $this->app->bind('App\ServiceHandler\DataLayer\ODIncidentService','App\ServiceHandler\DataLayer\ODIncidentImp');
        // Http Client
        $apdy_url = env('APPDY_BASE_URL_1');
        $this->app->singleton('GuzzleHttp\Client', function($api) use ($apdy_url) {
            return new Client([
                'base_uri' => $apdy_url,
                //'verify' => false
            ]);
        });

        // Data Layer
        $this->app->bind('App\ServiceHandler\DataLayer\HistoryService','App\ServiceHandler\DataLayer\HistoryImp');
        $this->app->bind('App\ServiceHandler\DataLayer\InboxService','App\ServiceHandler\DataLayer\InboxImp');
        $this->app->bind('App\ServiceHandler\DataLayer\UserService','App\ServiceHandler\DataLayer\UserImp');
        $this->app->bind('App\ServiceHandler\DataLayer\ChatService','App\ServiceHandler\DataLayer\ChatImp');
        $this->app->bind('App\ServiceHandler\DataLayer\TelegramUserService','App\ServiceHandler\DataLayer\TelegramUserImp');
        $this->app->bind('App\ServiceHandler\DataLayer\SupportUserService','App\ServiceHandler\DataLayer\SupportUserImp');
        $this->app->bind('App\ServiceHandler\DataLayer\ChatHistoryService','App\ServiceHandler\DataLayer\ChatHistoryImp');
        $this->app->bind('App\ServiceHandler\DataLayer\WhatsappDispatcherMessagesService','App\ServiceHandler\DataLayer\WhatsappDispatcherMessagesImp');
        $this->app->bind('App\ServiceHandler\DataLayer\EmployeeRemedyService','App\ServiceHandler\DataLayer\EmployeeRemedyImp');
        $this->app->bind('App\ServiceHandler\DataLayer\SupportGroupService','App\ServiceHandler\DataLayer\SupportGroupImp');
        $this->app->bind('App\ServiceHandler\DataLayer\ServiceCatalogService','App\ServiceHandler\DataLayer\ServiceCatalogImp');
        $this->app->bind('App\ServiceHandler\DataLayer\RemedyCategorizationService','App\ServiceHandler\DataLayer\RemedyCategorizationImp');
        $this->app->bind('App\ServiceHandler\DataLayer\CPSServerService','App\ServiceHandler\DataLayer\CPSServerImp'); 
        $this->app->bind('App\ServiceHandler\DataLayer\ServiceCIService','App\ServiceHandler\DataLayer\ServiceCIImp'); 
        $this->app->bind('App\ServiceHandler\DataLayer\InboxWorkInfoLogService','App\ServiceHandler\DataLayer\InboxWorkInfoLogImp'); 
        $this->app->bind('App\ServiceHandler\DataLayer\SurveyService','App\ServiceHandler\DataLayer\SurveyImp'); 
        // Integration Layer 
        $this->app->bind('App\ServiceHandler\IntegrationLayer\CHGInfraChangesService','App\ServiceHandler\IntegrationLayer\CHGInfraChangesImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\WOIWorkOrderService','App\ServiceHandler\IntegrationLayer\WOIWorkOrderImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\WOIWorkOrderUpdateService','App\ServiceHandler\IntegrationLayer\WOIWorkOrderUpdateImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\WOICTMRemedyService','App\ServiceHandler\IntegrationLayer\WOICTMRemedyImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\WOIIncidentService','App\ServiceHandler\IntegrationLayer\WOIIncidentImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService','App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\AppdynamicsService','App\ServiceHandler\IntegrationLayer\AppdynamicsImp');
        // Notification Layer
        $this->app->bind('App\ServiceHandler\NotificationLayer\NotificationService','App\ServiceHandler\NotificationLayer\NotificationImp');
        // Report Layer
        $this->app->bind('App\ServiceHandler\ReportLayer\ReportService','App\ServiceHandler\ReportLayer\ReportImp');
        // BDS Layer
        $this->app->bind('App\ServiceHandler\BDSLayer\BDSServerService','App\ServiceHandler\BDSLayer\BDSServerImp');
        $this->app->bind('App\ServiceHandler\BDSLayer\BDSUserProfileService','App\ServiceHandler\BDSLayer\BDSUserProfileImp');
        ////////////////////////////////////// Below Code is Before Assessment ///////////////////////////////////////////////////////////
        $this->app->bind('App\ServiceHandler\LiveChatService','App\ServiceHandler\LiveChatImp');//Not Used
        $this->app->bind('App\ServiceHandler\TaskRequestService','App\ServiceHandler\TaskRequestImp');//Not Used
        $this->app->bind('App\ServiceHandler\UserSupportService','App\ServiceHandler\UserSupportImp');//Not Used
        $this->app->bind('App\ServiceHandler\DataLayer\ProblemService','App\ServiceHandler\DataLayer\ProblemImp');
        $this->app->bind('App\ServiceHandler\DataLayer\ProductionIssuesService','App\ServiceHandler\DataLayer\ProductionIssuesImp');
        // System Availability
        $this->app->bind('App\ServiceHandler\DataLayer\SystemAvailabilityService','App\ServiceHandler\DataLayer\SystemAvailabilityImp');
        // Master System Availability
        $this->app->bind('App\ServiceHandler\DataLayer\MasterSystemService','App\ServiceHandler\DataLayer\MasterSystemImp');
        // Featch Data For Tableau
        $this->app->bind('App\ServiceHandler\DataLayer\TbluEdcUssdService','App\ServiceHandler\DataLayer\TbluEdcUssdImp');
        $this->app->bind('App\ServiceHandler\DataLayer\TbluSourceKibanaService','App\ServiceHandler\DataLayer\TbluSourceKibanaImp');
        $this->app->bind('App\ServiceHandler\DataLayer\TbluSourceAspService','App\ServiceHandler\DataLayer\TbluSourceAspImp');
        // Incident Report
        $this->app->bind('App\ServiceHandler\DataLayer\RemedyHPDIncidentInterfaceService','App\ServiceHandler\DataLayer\RemedyHPDIncidentInterfaceImp');
        $this->app->bind('App\ServiceHandler\DataLayer\HPDIncidentInterfaceService','App\ServiceHandler\DataLayer\HPDIncidentInterfaceImp');
        // PBM Layer
        $this->app->bind('App\ServiceHandler\PBMLayer\DashboardItService','App\ServiceHandler\PBMLayer\DashboardItImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\SuccessRateService','App\ServiceHandler\PBMLayer\SuccessRateImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\PMRolesService','App\ServiceHandler\PBMLayer\PMRolesImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\PMRolesFeaturesService','App\ServiceHandler\PBMLayer\PMRolesFeaturesImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\PMMasterApdyBridgeService','App\ServiceHandler\PBMLayer\PMMasterApdyBridgeImp');
        // Response Time
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\ResponseTimeService','App\ServiceHandler\PBMLayer\ResponseTime\ResponseTimeImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluApdyService','App\ServiceHandler\PBMLayer\ResponseTime\TbluApdyImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluEdcService','App\ServiceHandler\PBMLayer\ResponseTime\TbluEdcImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluMandolService','App\ServiceHandler\PBMLayer\ResponseTime\TbluMandolImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluKibanaService','App\ServiceHandler\PBMLayer\ResponseTime\TbluKibanaImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluAtmService','App\ServiceHandler\PBMLayer\ResponseTime\TbluAtmImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluEmasService','App\ServiceHandler\PBMLayer\ResponseTime\TbluEmasImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluRtgsService','App\ServiceHandler\PBMLayer\ResponseTime\TbluRtgsImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluBdsService','App\ServiceHandler\PBMLayer\ResponseTime\TbluBdsImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluCpsService','App\ServiceHandler\PBMLayer\ResponseTime\TbluCpsImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\ResponseTime\TbluAspRespService','App\ServiceHandler\PBMLayer\ResponseTime\TbluAspRespImp');
        //Bulk upload
        $this->app->bind('App\ServiceHandler\PBMLayer\Bulk\IncidentProblemService','App\ServiceHandler\PBMLayer\Bulk\IncidentProblemImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\Bulk\BulkStatusService','App\ServiceHandler\PBMLayer\Bulk\BulkStatusImp');
        $this->app->bind('App\ServiceHandler\IntegrationLayer\DLDMigrateLoginService','App\ServiceHandler\IntegrationLayer\DLDMigrateLoginImp');
        $this->app->bind('App\ServiceHandler\PBMLayer\Bulk\KnowledgeService','App\ServiceHandler\PBMLayer\Bulk\KnowledgeImp');
    }
}
