import '../bootstrap';
import 'chart.js'
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'; // Ensure you are using css-loader
import Alert from './components/alert/Alert';
import SimpleLoading from './components/SimpleLoading'; // Global for simple loading
import Vue from 'vue';
import store from './stores';
import Vuetify from 'vuetify';
import Index from './Index';



axios.defaults.baseURL = '/'; // Set base URL for axios
window.Vue = Vue;
window.events = new Vue();
window.flash = function (message) {
    window.events.$emit("flash", message);
};
window.dismissflash = function () {
    window.events.$emit("dismissflash");
};
Vue.use(Vuetify);
Vue.component('index',Index);
Vue.component('simple-loading',SimpleLoading);
Vue.component('alert-message',Alert);
Vue.component(
    "spinner-loader",
    require("./components/alert/SpinnerCenter.vue")
);
Vue.component(
    "flash",
    require("./components/alert/FlashMessage.vue")
);

const vuetify = new Vuetify();
const app = new Vue({
    el: '#onduty_dashboard',
    vuetify,
    store,
    data: {
        auth: auth,
        roles: roles
        //stomps: stompclient,
    },methods: {

    },
    created() {
       /* this.stomps.connect({},
            function (e) {
                var audio = new Audio(audiofile);
                app.isresponsereceived = true;
                flash({
                    id: "websocket-success",
                    message: "Connection to backend established, click this alert to ignore!!",
                    flashtype: "success"
                });

                stompclient.subscribe("/request/drop/", function (stream) {
                    var data = JSON.parse(stream.body);
                    var rqObj = app.requests.find(item => item.id === data.id);
                    var wtObj = app.tasks.find(item => item.id === data.id);
                    var rqIdx = null;
                    var wtIdx = null;
                    if (typeof rqObj !== "undefined") {
                        console.log("request was deleted");
                        rqIdx = app.requests.indexOf(rqObj);
                        app.$delete(app.requests, rqIdx);
                    }
                    if (typeof wtObj !== "undefined") {
                        console.log("request was deleted");
                        wtIdx = app.tasks.indexOf(wtObj);
                        app.$delete(app.tasks, wtIdx);
                    }
                });

                //ON GOING
                stompclient.subscribe("/request/feed/", function (stream) {
                    var rqObj = {};
                    var wtObj = {};
                    var data = JSON.parse(stream.body);
                    console.log(data);

                    if (app.usedchannel !== "ALL") {
                        rqObj = app.requests.find(item => item.id === data.id && item.serviceChannelName === app.usedchannel);
                        console.log("RQ filter " + data.serviceChannelName);
                        console.log(rqObj);
                    } else {
                        rqObj = app.requests.find(item => item.id === data.id);
                        console.log("RQ if ALL " + data.serviceChannelName);
                        console.log(rqObj);
                    }

                    if (app.usedprocessstatus !== "ALL") {
                        wtObj = app.tasks.find(item => item.id === data.id && item.processStatus === app.usedprocessstatus);
                        console.log("WT filter " + data.processStatus);
                        console.log(wtObj);
                    } else {
                        wtObj = app.tasks.find(item => item.id === data.id);
                        console.log("WT if ALL " + data.processStatus);
                        console.log(wtObj);
                    }

                    if(data.serviceCatalogId == 88){
                        console.log('Ini serviceCatalogId 88');
                    }

                    var rqIdx = null;
                    var wtIdx = null;

                    if (typeof rqObj !== "undefined") {
                        rqIdx = app.requests.indexOf(rqObj);
                        switch (data.processStatus) {
                            case "INPROGRESS": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus !="ALL") {
                                    console.log("moved to my task #");
                                } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "PENDING": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus !="ALL") {
                                    console.log("moved to my task #");
                                } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "WAITINGAPR": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus !="ALL") {
                                    console.log("moved to my task #");
                                } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "PLANNING": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus !="ALL") {
                                    console.log("moved to my task");
                                } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "CLOSED": {
                                console.log("remove from queue to history");
                                app.$delete(app.requests, rqIdx);
                                break;
                            }
                            case "REJECT": {
                                console.log("remove from my task due rejected")
                                app.$delete(app.requests, rqIdx);
                                break;
                            }
                            case "QUEUED": {
                                console.log("MASUK QUEUE");
                                if (data.serviceCatalogId == 88) {
                                    console.log("CLEAR SERVICEID 88");
                                    app.$delete(app.requests, rqIdx);
                                }
                                break;
                            }
                        }
                    } else {
                        if (typeof wtObj !== "undefined") {
                            wtIdx = app.tasks.indexOf(wtObj);
                            switch (data.processStatus) {
                                case "QUEUED": {
                                    if (app.auth.employee_remedy_id !== data.employeeAssigneeId) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                        console.log("remove from my task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (data.serviceTypeEncodingname == serviceType) {
                                        console.log("added to request queue");
                                        app.requests.push(data);
                                        audio.play();
                                    }
                                    break;
                                }
                                case "INPROGRESS": {
                                    if (app.auth.employee_remedy_id !== data.employeeAssigneeId) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus != "ALL") {
                                        console.log("update my task #");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "PENDING": {
                                    if (app.auth.employee_remedy_id !== data.employeeAssigneeId) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus != "ALL") {
                                        console.log("update my task #");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "PLANNING": {
                                    if (app.auth.employee_remedy_id !== data.employeeAssigneeId) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus !="ALL") {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "WAITINGAPR": {
                                    if (app.auth.employee_remedy_id !== data.employeeAssigneeId) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (app.auth.employee_remedy_id === data.employeeAssigneeId && app.usedprocessstatus !== data.processStatus && app.usedprocessstatus !="ALL") {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "CLOSED": {
                                    console.log("remove from my task to history");
                                    app.$delete(app.tasks, wtIdx);
                                    break;
                                }
                                case "REJECT": {
                                    console.log("remove from my task due rejected")
                                    app.$delete(app.tasks, wtIdx);
                                    break;
                                }
                            }
                        } else {
                            if (app.usedchannel !== "ALL") {
                                if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.serviceChannelName == app.usedchannel) {
                                    console.log("newly added to request queue [filter]");
                                    data.processStatus = "QUEUED";
                                    app.requests.push(data);
                                    audio.play();
                                }
                            } else if (app.usedprocessstatus != "ALL") {
                                if ((data.processStatus === "INPROGRESS" || data.processStatus === "PENDING" || data.processStatus === "WAITINGAPR" || data.processStatus === "PLANNING") && app.auth.employee_remedy_id === data.employeeAssigneeId && data.processStatus == app.usedprocessstatus) {
                                    console.log("added to my task from other [filter]");
                                    data.processStatus = data.processStatus;
                                    app.tasks.push(data);
                                    audio.play();
                                    flash({
                                        id: data.id,
                                        message: "Request ID : " +
                                            data.id +
                                            " added to your task, assigned by : " +
                                            data.assigneeName,
                                        flashtype: "info"
                                    });
                                } else {
                                    if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.remedyTicketId !== "N/A") {
                                        console.log("newly added to request queue ##");
                                        data.processStatus = "QUEUED";
                                        app.requests.push(data);
                                        audio.play();
                                    }
                                    if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.remedyTicketId == "N/A" && (data.recipientID == "LIVECHAT" || data.recipientID == "TELEGRAM")) {
                                        console.log("newly added to request queue #");
                                        data.processStatus = "QUEUED";
                                        app.requests.push(data);
                                        audio.play();
                                    }
                                }
                            } else {
                                if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.remedyTicketId !== "N/A") {
                                    console.log("newly added to request queue");
                                    data.processStatus = "QUEUED";
                                    app.requests.push(data);
                                    audio.play();
                                }
                                if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.remedyTicketId == "N/A" && (data.recipientID == "LIVECHAT" || data.recipientID == "TELEGRAM")) {
                                    console.log("newly added to request queue #1");
                                    data.processStatus = "QUEUED";
                                    app.requests.push(data);
                                    audio.play();
                                }
                                if ((data.processStatus === "INPROGRESS" || data.processStatus === "PENDING" || data.processStatus === "WAITINGAPR" || data.processStatus === "PLANNING") && app.auth.employee_remedy_id === data.employeeAssigneeId) {
                                    console.log("added to my task from other");
                                    data.processStatus = data.processStatus;
                                    app.tasks.push(data);
                                    audio.play();
                                    flash({
                                        id: data.id,
                                        message: "Request ID : " +
                                            data.id +
                                            " added to your task, assigned by : " +
                                            data.assigneeName,
                                        flashtype: "info"
                                    });
                                }
                            }
                        }
                    }
                });

                stompclient.subscribe("/request/livechat", function (stream) {
                    var data = JSON.parse(stream.body);
                    if (app.isdataloaded && app.requestid == data.requestId) {
                        app.requestdata.chatdata.push(data);
                        app.scrollToEnd();
                    }
                    if (!app.ismodal && !app.isdataloaded && (data.agentId === app.auth.id)) {
                        flash({
                            id: data.id,
                            message: "New message from " + data.senderName + " on Request ID :" + data.requestId + ", Message : " + data.content,
                            flashtype: "info"
                        });
                    }
                    if (app.ismodal && app.isdataloaded && app.requestid != data.requestId && (data.agentId === app.auth.id)) {
                        flash({
                            id: data.id,
                            message: "New message from " + data.senderName + " on Request ID :" + data.requestId + ", Message : " + data.content,
                            flashtype: "info"
                        });
                    }
                });
            },
            function (message) {
                app.isresponsereceived = true;
                stompclient.disconnect();
                flash({
                    id: "websocket-failure",
                    message: "Connection to backend was interrupted, please try to refresh the page, if alert still appear change your browser or report to Administrator!!",
                    flashtype: "danger"
                });
            }
        );*/
    }
});
export default app;