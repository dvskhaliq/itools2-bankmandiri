<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewResponseTime extends Model
{
    protected $table = 'view_response_time';
    public $timestamps = false;
    // public $incrementing = false;
}
