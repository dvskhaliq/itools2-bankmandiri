@extends('sidebar')
@section('content')
<div id="bds_cdm_detail">
    <div class="row bds-cdm-detail">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS CDM Detail</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>CDM Detail</span>
                            </div>
                            <div class="panel-body">
                                <table-bds-cdm-detail :data_cdm_detail="data_cdm_detail" v-on:cdm-detail-selected="cdmDetailSelected"  :isloaded="isloaded"></table-bds-cdm-detail>
                                <!-- <vue-pagination :pagination="pages" @paginate="getData()" :offset="4">
                                </vue-pagination> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Add CDM Detail</span>
                            </div>
                            <componen-bds-cdm-add :data_cdm_detail="data_cdm_detail" v-on:submit-add-cdm="submitAdd"></componen-bds-cdm-add>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Confirm Delete</h4>
                </div>
                <div v-if="isloaded">
                    <componen-confirm-delete v-on:submit-delete="submitDelete" v-on:closewindows="closeWindow" :cdm_detail_prop="cdm_detail_prop" :isloaded="isloaded" :ismodal="ismodal"></componen-confirm-delete>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">CDM Detail</h4>
                </div>
                <div v-if="isloaded">
                    <componen-modal-edit v-on:closewindows="closeWindow" v-on:submit-update="submitUpdate" :cdm_detail_prop="cdm_detail_prop" :isloaded="isloaded" :ismodal="ismodal"></componen-modal-edit>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/bds_cdm_detail.js"></script>
@endsection
