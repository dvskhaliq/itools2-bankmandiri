import ENUM from "../enum";

const state = {
    simpleHeader: [
      { text: "Incident Number", value: "IncidentNumber" },
      { text: "Assigned Group", value: "AssignedGroup"},
      { text: "Assignee", value: "Assignee"},
    //   { text: "Customer", value: "employeeremedy.full_name"},
    { text: "Customer", value: "CustomerPersonId"},
      { text: "Summary", value: "Description"},
      { text: "Status", value: "Status"},
      { text: "Reported Source", value: "ReportedSource"},
      { text: "Priority", value: "Priority"},
      { text: "Survey", value: "Survey", width:"110px", align:"center"},
      { text: "Submit Date", value: "SubmitDate", width:"100px", align:"center"},
    ],
    apiState: ENUM.INIT,
    alertMessage: "",
    incidentInterface: [],
};

const getters = {
    getIncident: state => state.incidentInterface,
    getSimpleHeader: state => state.simpleHeader,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
};

const actions = {
    async initHPDIncidentInterface({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/dashboard/incident/report/index/fetch");
          if(response.data.status){
            commit("setIncident", response.data.data);       
            commit("setSuccess", "Data loaded succesfully !");
          }else{
            commit("setWarning", "Unable to load");
          }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByYear({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebyyear", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByMonth({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebymonth", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByDate({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebydate", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByYearSource({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebyyear/source", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByMonthSource({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebymonth/source", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByDateSource({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebydate/source", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByYearSurvey({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebyyear/survey", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByMonthSurvey({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebymonth/survey", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByDateSurvey({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebydate/survey", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByYearSourceSurvey({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebyyear/sourcesurvey", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByMonthSourceSurvey({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebymonth/sourcesurvey", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async populateByDateSourceSurvey({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/dashboard/incident/report/index/populatebydate/sourcesurvey", payload
              );
            commit("setIncident", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    resetIncident({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setIncident", []);
    },
};

const mutations = {
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setIncident: (state, incident) => {
        state.apiState = ENUM.LOADED;
        state.incidentInterface = incident;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
