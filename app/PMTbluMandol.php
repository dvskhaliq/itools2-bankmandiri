<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluMandol extends Model
{
    protected $table = 'pm_tblu_mandol';
    public $timestamps = false;
}
