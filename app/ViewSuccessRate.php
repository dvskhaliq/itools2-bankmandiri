<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewSuccessRate extends Model
{
    protected $table = 'view_success_rate';
    public $timestamps = false;
}
