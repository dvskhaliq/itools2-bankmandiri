<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\BaseController;
use App\EmployeeRemedy;
use App\User;


class EmployeeRemedyController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getCustomerProfile()
    {
        $result = EmployeeRemedy::where('company', request()->get('company'))
        ->where(function($q) {
            $q->where('full_name', 'ILIKE', '%' . request()->get('inputname') . '%')
              ->orWhere('nip', 'ILIKE', '%' . request()->get('inputname') . '%');
        })->where('profile_status','1')->get();
        return response()->json($result);
    }

    // public function getAssignee()
    // {
    //     $result = User::orderBy('name', 'asc')->pluck('name');
    //     // $result =  DB::table('users')->join('employee_remedy', 'employee_remedy.id', '=', 'users.employee_remedy_id')->select('users.name', 'employee_remedy.department_name')->get();
    //     return response()->json($result);
    // }
}
