require('../../bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.events = new Vue();
window.VueSelect = require('vue-select');

window.flash = function (message) {
    window.events.$emit('flash', message);
}
window.dismissflash = function () {
    window.events.$emit("dismissflash");
};

Vue.component('component-table', ()=> import('../../components/administrator_console/user_configuration/UserConfiguration.vue'));
Vue.component('componen-detail', ()=> import('../../components/administrator_console/user_configuration/UserConfigurationDetail.vue'));
Vue.component('componen-edit', ()=> import('../../components/administrator_console/user_configuration/UserConfigurationUpdate.vue'));
Vue.component('componen-insert', ()=> import('../../components/administrator_console/user_configuration/UserConfigurationInsert.vue'));
Vue.component('componen-confirm-delete', ()=> import('../../components/administrator_console/user_configuration/UserConfigurationDelete.vue'));
Vue.component('componen-roles', ()=> import('../../components/administrator_console/user_configuration/UserConfigurationRoles.vue'));

// Vue.component('componen-add-role', ()=> import('../../components/administrator_console/user_configuration/UserConfigurationAddRole.vue')); Not Used

Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));
Vue.component("v-select", VueSelect.VueSelect);

import VuePagination from '../../components/Pagination.vue';
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)
import 'es6-promise/auto'
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from 'vuex';

import Vue from 'vue';
import VeeValidate from 'vee-validate';

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: 'errors', // change if property conflicts
    events: 'input|blur',
    fieldsBagName: 'fields',
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: 'validations', // the nested key under which the validation messages will be located
    inject: true,
    locale: 'en',
    validity: false,
    useConstraintAttrs: true
};


Vue.use(VeeValidate, config);
Vue.use(Loading);
Vue.use(Vuex);
const user_configuration = new Vue({
    el: '#user_configuration',
    data: {
        auth: auth,
        result: "",
        selected_prop: {},
        isloaded: false,
        ismodal: false,
        alert: false,
        pages: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        search: "",
        user_id: "",
        selected_user: {},
        selected_role: {},
        name_user:"",
        modalname: ""
    },

    mounted() {
        this.getData();
    },

    components: {
        VuePagination,
    },

    methods: {
        searchUser(req) {
            this.search = req.search;
            this.alert = true;
            axios.get(`/user_configuration/page?page=1&inputname=` + req.search)
                .then((response) => {
                    this.pages = response.data.result;
                    this.result = response.data.result.data;
                }).catch(error => {
                  if (error.response.status === 419 || error.response.status === 401) {
                    window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                    location.reload(false);
                  } else {
                    this.setFlashDanger(error.response.data.message)
                  }
                });
        },

        getData() {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.alert = true;
            axios.get(`/user_configuration/page?page=${this.pages.current_page}&inputname=` + this.search)
                .then((response) => {
                    this.pages = response.data.result;
                    this.result = response.data.result.data;
                    loader.hide();
                }).catch(error => {
                  if (error.response.status === 419 || error.response.status === 401) {
                    window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                    location.reload(false);
                  } else {
                    this.setFlashDanger(error.response.data.message)
                  }
                  loader.hide();
                });
        },

        setFlashInfo(reqResponse) {
            flash({
                id: "alert-info",
                message: reqResponse,
                flashtype: "info"
            });
        },

        setFlashDanger(reqResponse) {
            flash({
                id: "alert-danger",
                message: reqResponse,
                flashtype: "danger"
            });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
            this.modalname = "";
        },

        userSelected(reqResponse) {
            this.modalname = reqResponse.modalname
            this.ismodal = true;
            axios.post('/user_configuration/select_user', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.selected_prop = response.data.result;
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        },

        userSelectedRoles(reqResponse) {
            this.modalname = reqResponse.modalname
            this.ismodal = true;
            this.user_id = reqResponse.id;
            axios.post('/user_configuration/user_roles', reqResponse).then(response => {                
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.selected_user = response.data.result;
                    this.selected_role = response.data.arr_role_id;
                    this.user_id = reqResponse.id;
                    this.name_user = reqResponse.userName;
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        },

        submitUpdate(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.alert = true;
            this.ismodal = false;
            axios.post('/user_configuration/submit_update', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
                loader.hide();
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
              loader.hide();
            });
        },

        updatePassword(reqResponse) {
          let loader = this.$loading.show({
              loader: "dots"
          });
          axios.post('/user_configuration/update_password', reqResponse).then(response => {
              if (response.data.status == true) {
                  this.setFlashInfo(response.data.message)
              } else {
                  this.setFlashDanger(response.data.message)
              }
              loader.hide();
          }).catch(error => {
            if (error.response.status === 419 || error.response.status === 401) {
              window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
              location.reload(false);
            } else {
              this.setFlashDanger(error.response.data.message)
            }
            loader.hide();
          });
      },

        submitInsert(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.alert = true;
            this.ismodal = false;
            axios.post('/user_configuration/submit_insert', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
                loader.hide();
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
              loader.hide();
            });
        },

        submitDelete(reqResponse) {
            this.alert = true;
            axios.post('/user_configuration/submit_delete', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        },

        createUser(reqResponse){
            this.ismodal = true;
            this.modalname = reqResponse.modalname
        },

        flashAddRole(reqResponse){
            if(reqResponse.status){
                this.setFlashInfo(reqResponse.message)
            } else{
                this.setFlashDanger(reqResponse.message)
            }
        }
    },

});
