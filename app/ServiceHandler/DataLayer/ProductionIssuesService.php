<?php

namespace App\ServiceHandler\DataLayer;
use Illuminate\Database\Eloquent\Collection;

interface ProductionIssuesService
{
    public function truncateProductionIssues();
   // public function bulkInsert($collection): ?bool;
    public function bulkInsert($collection);
    public function getProductionIssues(): ?Collection;
    public function getTotalByYear();
    public function getProductionIssuesBiller(): ?Collection;
}
