<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluEdc extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pm_tblu_edc';
    public $timestamps = false;
}
