<?php

namespace App\ServiceHandler\PBMLayer\ResponseTime;

use App\PMTbluCps as TbluCps;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TbluCpsImp implements TbluCpsService
{
    public function getResponseTime(): ?Collection
    {
        return TbluCps::select(DB::raw("layanan, node, AVG(restime) as restime, tanggal"))
        ->where('tanggal', '=', date('Y-m-d', strtotime('yesterday')))
        ->groupBy('layanan')->groupBy('node')->groupBy('tanggal')
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByDate($date): ?Collection
    {
        return TbluCps::select(DB::raw("layanan, node, AVG(restime) as restime, tanggal"))
        ->where('tanggal', '=', $date)
        ->groupBy('layanan')->groupBy('node')->groupBy('tanggal')
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByMonth($month): ?Collection
    {
        return TbluCps::select(DB::raw("layanan, node, AVG(restime) as restime, tanggal"))
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))
        ->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->groupBy('layanan')->groupBy('node')->groupBy('tanggal')
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByMonthSystem($layanan, $month): ?Collection
    {
        $result = TbluCps::where('layanan',$layanan)
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','asc')->get();
        return $result;
    }

    public function getResponseTimeByMonthAvg($month): ?Collection
    {
        $result = TbluCps::select(DB::raw("layanan, AVG(restime) as restime, to_char(tanggal, 'YYYY-MM') as bulan"))
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->groupBy('layanan')->groupBy('bulan')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getResponseTimeByYear(array $years): ?Collection
    {
        return TbluCps::select(DB::raw("layanan, node, AVG(restime) as restime, tanggal"))
        ->where(function($q) use($years) {
            foreach ($years as $year) {
                $q->whereYear('tanggal', '=', $year, 'or');
            }
        })->groupBy('layanan')->groupBy('node')->groupBy('tanggal')
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getYears(){
        return DB::select("select distinct EXTRACT(YEAR from tanggal) as year from pm_tblu_cps order by EXTRACT(YEAR from tanggal) desc");
    }
}
