<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\ResponseTime\ResponseTimeService;
use Illuminate\Http\Request;

class ResponseTimeController extends BaseController
{
    protected $response_time_repo;

    public function __construct(ResponseTimeService $response_time_service)
    {
        $this->response_time_repo = $response_time_service;
    }

    public function fetchResponseTime()
    {
        $query = $this->response_time_repo->getResponseTime();
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByDate(Request $request)
    {
        $selected_date = $request->input('selectedDate');
        $query = $this->response_time_repo->getResponseTimeByDate($selected_date);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByMonth(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $query = $this->response_time_repo->getResponseTimeByMonth($selected_month);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByMonthAvg(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $avg_query = $this->response_time_repo->getResponseTimeByMonthAvg($selected_month);
        $data = $this->loopForeachGroup($avg_query, $selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByYear(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        $query = $this->response_time_repo->getResponseTimeByYear($selected_year);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function loopForeach($data){
        $hasil=array();
        foreach ($data as $key => $value) {
            $prop['layanan'] = $value->layanan;
            $prop['node'] = $value->node;
            $prop['restime'] = ROUND($value->restime*1,2);
            $prop['tanggal'] = $value->tanggal;

            array_push($hasil, [
                'layanan' => $prop['layanan'],
                'node' => $prop['node'],
                'restime' => $prop['restime'],
                'tanggal' => $prop['tanggal'],
                ]);
        }
        return $hasil;
    }

    public function loopForeachGroup($avg, $month){
        $hasil=array();
        foreach ($avg as $key => $value) {
            $prop['layanan'] = $value->layanan;
            $prop['restime'] = ROUND($value->restime*1,2);
            $prop['tanggal'] = $value->bulan;
            $query = $this->response_time_repo->getResponseTimeByMonthSystem($prop['layanan'], $month);
            $decodedetail=array();
            foreach ($query as $keydetail => $value) {
                    $prop['layanan'] = $value->layanan;
                    $prop['node'] = $value->node;
                    $prop['restime'] = ROUND($value->restime*1,2);
                    $prop['tanggal'] = $value->tanggal;
                array_push($decodedetail, [
                    'layanan' => $prop['layanan'],
                    'node' => $prop['node'],
                    'restime' => $prop['restime'],
                    'tanggal' => $prop['tanggal'],
                    ]);
            }
            array_push($hasil, [
                'id' => $key+1,
                'layanan' => $prop['layanan'],
                'restime' => $prop['restime'],
                'tanggal' => $prop['tanggal'],
                'detail' => $decodedetail
                ]);
        }
        return $hasil;
    }

    public function fetchYears()
    {
        $data = $this->response_time_repo->getYears();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }
}
