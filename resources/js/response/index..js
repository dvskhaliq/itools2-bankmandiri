const onErrorResponse = function(error) {
    if (error.response.status === 419 || error.response.status === 401) {
        window.alert(
            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
        );
        location.reload(false);
    } else {
        flash({
            id: "error code status",
            message: error.response.data.message,
            flashtype: "danger"
        });
    }
    loader.hide();
};

export default {
    onErrorResponse
};
