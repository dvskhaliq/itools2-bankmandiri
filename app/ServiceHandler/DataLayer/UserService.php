<?php

namespace App\ServiceHandler\DataLayer;

use App\User;

interface UserService
{
    public function findByLoginId(String $login_id): User;

    public function findByUserId(?Int $user_id): ?User;

    public function getRestrictedMe(): array;

    public function getMe(): array;

    public function getUserByPMRole(int $role_id);
}
