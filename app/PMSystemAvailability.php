<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMSystemAvailability extends Model
{
    protected $table = 'pm_system_availability';
    public $timestamps = false;

    public function header(){
        return $this->belongsTo(PMMasterSystemAvailability::class,'id_system');
    }
}
