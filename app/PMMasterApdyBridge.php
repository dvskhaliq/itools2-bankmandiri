<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMMasterApdyBridge extends Model
{
    public $table = 'pm_master_apdy_bridge';
    public $timestamps = false;

    public function pmmastersystemavailability(){
        return $this->belongsTo(PMMasterSystemAvailability::class,'system_id');
    }
}
