<?php 

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\BaseController;
use App\Inbox;
use App\InboxWorkinfolog;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\ServiceHandler\LiveChatService;
use App\ServiceHandler\TaskRequestService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderUpdateService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use App\ServiceHandler\UserSupportService;
use JavaScript;
use Auth;
use View;
use App\Incident;
use App\Workorder;
use App\ServiceHandler\DataLayer\CPSServerService;
use App\ServiceHandler\DataLayer\ChatService;
use App\ServiceHandler\DataLayer\HistoryService;
use App\ServiceHandler\DataLayer\InboxService;
use App\ServiceHandler\DataLayer\ServiceCatalogService;
use App\ServiceHandler\DataLayer\SupportGroupService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\DataLayer\TelegramUserService;
use App\ServiceHandler\DataLayer\UserService;
use App\ServiceHandler\DataLayer\WhatsappDispatcherMessagesService;
use App\ServiceHandler\DataLayer\ChatHistoryService;
//add by pmw for odincident
use App\ServiceHandler\DataLayer\ODIncidentService;
use App\ServiceHandler\NotificationLayer\NotificationService;
use Symfony\Component\HttpFoundation\Request;
use App\ServiceHandler\DataLayer\InboxWorkInfoLogService;
use Illuminate\Support\Facades\Storage;

class MainController extends BaseController
{
    protected $livechat_service; //should deprecated
    protected $taskrequest_service; //should deprecated
    protected $wo_wsdl_service; //has been rename to be wocrate_wsdl
    //
    protected $srvctlg_ds;
    protected $notif_ds;
    protected $suppgrp_ds;
    protected $wocreate_wsdl;
    protected $woupdate_wsdl;
    protected $history_service;
    protected $inccreate_wsdl;
    protected $incupdate_wsdl;
    protected $chat_ds;
    protected $user_service;
    protected $cpsserver_service;
    protected $inbox_ds;
    protected $od_incident;
    protected $suppusr_ds;
    protected $tlgrmusr_ds;
    protected $wadispatcher_ds;
    protected $workinfolog_ds;
    protected $chathis_ds;
    /**
     * Create a new controller instance.
     */
    public function __construct(
        LiveChatService $lc_service,
        HistoryService $h_service,
        TaskRequestService $tr_service,
        WOIWorkOrderService $workorder_create_service,
        WOIWorkOrderUpdateService $workorder_update_service,
        WOIIncidentService $incident_create_service,
        WOIIncidentUpdateService $incident_update_services,
        UserService $user_service,
        CPSServerService $cps_service,
        InboxService $inbox_service,
        SupportUserService $support_user_service,
        ChatService $chat_service,
        TelegramUserService $telegram_user_service,
        WhatsappDispatcherMessagesService $wa_dispatcher_service,
        SupportGroupService $support_group_service,
        NotificationService $notif_service,
        ServiceCatalogService $service_catalog_service,
        InboxWorkInfoLogService $workinfolog_service,
        ChatHistoryService $chat_history_service,
        ODIncidentService $od_incident_service
    ) {
        $this->workinfolog_ds = $workinfolog_service;
        $this->srvctlg_ds = $service_catalog_service;
        $this->notif_ds = $notif_service;
        $this->taskrequest_service = $tr_service;
        $this->history_service = $h_service;
        $this->suppgrp_ds = $support_group_service;
        $this->livechat_service = $lc_service;
        $this->wocreate_wsdl = $workorder_create_service;
        $this->woupdate_wsdl = $workorder_update_service;
        $this->inccreate_wsdl = $incident_create_service;
        $this->incupdate_wsdl = $incident_update_services;
        $this->user_service = $user_service;
        $this->cpsserver_service = $cps_service;
        $this->inbox_ds = $inbox_service;
        $this->suppusr_ds = $support_user_service;
        $this->chat_ds = $chat_service;
        $this->tlgrmusr_ds = $telegram_user_service;
        $this->wadispatcher_ds = $wa_dispatcher_service;
        $this->chathis_ds = $chat_history_service;
        $this->od_incident = $od_incident_service;
        $this->middleware('auth');
    }

    /*
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function updateRequestDueException(Int $request_id, String $process_status, String $integration_status, Int $user_id, Int $assignee_id = null)
    {
        $assignee = $this->suppusr_ds->findByUserId($assignee_id);
        if ($assignee) {
            $array_inbox = array(
                "ProcessStatus" => $process_status,
                "ActionTicketCreation" => $integration_status,
                "AssigneeId" => $user_id,
                "EmployeeAssigneeId" => $assignee->employee_remedy_id,
                "EmployeeAssigneeGroupId" => $assignee->support_group_id,
            );
        } else {
            $array_inbox = array(
                "ProcessStatus" => $process_status,
                "ActionTicketCreation" => $integration_status,
                "AssigneeId" => $user_id,
            );
        }
        $this->inbox_ds->update($request_id, $array_inbox);
    }

    private function pendingRequestToMyTask($service_catalog_id, $request_id, $pending_process_status, $integration_status, $user_id, $actual_assignee_id)
    {
        $this->updateRequestDueException($request_id, $pending_process_status, $integration_status, $user_id, $actual_assignee_id);
        return $this->initRequestToMyTask($service_catalog_id, $request_id);
    }

    private function parseIncident(Inbox $request)
    {
        // if ($this->livechat_service->isIncidentInLivechatMode($request->id) && ($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING')) {
        if(($this->tlgrmusr_ds->hasActiveChat($request->EmployeeSenderId) && ($request->ServiceCatalogId == 2 || $request->ServiceCatalogId == 3 || $request->ServiceCatalogId == 4)) && ($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING')){
            return redirect()->action("LiveChatController@getChat", ["request_id" => $request->id]);
        } else {
            if ($request->ProcessStatus == 'BOOKED') {
                if ($request->RecipientID == 'ITSRM') {
                    $wsdl = $this->incupdate_wsdl->bookIncident($request, request()->get('assigneeId'));
                    $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task" : "Ref ID : " . $request->id . "-" . $request->RemedyTicketId . " assigned to your task, but not reflected in remedy, please retry process from Pending Ticket Remedy!";
                } else if ($request->RecipientID == 'LIVECHAT') {
                    return redirect()->action("LiveChatController@initChat", ["request_id" => $request->id]);
                } else if ($request->RecipientID == 'WA') {
                    return redirect()->action("WhatsappDispatcherController@initIncidentWhatsappDispatcher", ["request_id" => $request->id]);
                } else {
                    if ($request->RemedyTicketId !== "N/A") {
                        $wsdl = $this->incupdate_wsdl->bookIncident($request, request()->get('assigneeId'));
                        $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task" : "Ref ID : " . $request->id . "-" . $request->RemedyTicketId . " assigned to your task, but not reflected in remedy, please retry process from Pending Ticket Remedy!";
                    } else {
                        $assignee = $this->suppusr_ds->findByPersonIdGroupId(Auth::user()->employee_remedy_id, Auth::user()->sgn_id_remedy);
                        $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($request->service);
                        $wsdl = $this->inccreate_wsdl->createIncidentBook($request, $assignee, $wsdl_reconID->getContent());
                        $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $wsdl->getContent() . " created and assigned to your task" : "Ref ID : " . $request->id . " assigned to your task, but failed to create remedy ticket, please retry process from Pending Ticket Remedy!";
                    }
                }
                $data = $wsdl->getResponseStatus() == 'success' ? $this->initRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id')) : $this->pendingRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id'), "INPROGRESS", "IN_PENDING_PRS", request()->get('assigneeId'), request()->get('assigneeId'));
                $inbox = $this->inbox_ds->refresh($request);
                $this->notif_ds->performFeedBackNotifier($inbox);
                return response()->json(["status" => true, "message" => $message, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false]);
            } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING') && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
                if ($request->RecipientID == 'WA') {
                    return redirect()->action("WhatsappDispatcherController@getIncidentWhatsappDispatcher", ["request_id" => $request->id]);
                } else {
                    $data = $this->getRequestDetail(request()->get('id'));
                    return response()->json(["status" => true, "message" => "Related incident is in progress", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);                    
                }
            } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING') && request()->get('manager') == true) {
                $data = $this->getRequestDetail(request()->get('id'));
                return response()->json(["status" => true, "message" => "Related incident is in progress", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
            } else if ($request->ProcessStatus == 'INPROGRESS') {
                $data = $this->getRequestDetail(request()->get('id'));
                return response()->json(["status" => false, "message" => "Incident with Ref ID : " . $data->id . " - " . $data->RemedyTicketId . " has been BOOKED by: " . $data->supportgroupassigneeremedy->full_name, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
            } else if ($request->ProcessStatus == 'CLOSED') {
                $data = $this->getRequestDetail(request()->get('id'));
                return response()->json(["status" => true, "message" => "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " is closed", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
            } else {
                return response()->json(["status" => false, "message" => "No ticket available with Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " has been booked or closed by other agent", "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
            }
        }
    }

    private function parseWorkorder(Inbox $request)
    {
        if ($request->ProcessStatus == 'BOOKED') {
            if ($request->RecipientID == 'ITSRM') {
                $wsdl = $this->wocreate_wsdl->bookTicket($request, request()->get('assigneeId'));
                $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task" : "Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " assigned to your task, but not reflected in remedy, please retry process from Pending Ticket Remedy!";
            } else {
                $wsdl = $this->wocreate_wsdl->createWorkOrder($request, request()->get('assigneeId'));
                $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $wsdl->getContent() . " created and assigned to your task" : "Ref ID : " . $request->id . " assigned to your task, but failed to create remedy ticket, please retry process from Pending Ticket Remedy!";
            }
            $data = $wsdl->getResponseStatus() == 'success' ? $this->initRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id')) : $this->pendingRequestToMyTask(request()->get('serviceCatalogId'), request()->get('id'), "INPROGRESS", "WO_PENDING_PRS", request()->get('assigneeId'), request()->get('assigneeId'));
            $inbox = $this->inbox_ds->refresh($request);
            $this->notif_ds->performFeedBackNotifierWO($inbox, $note = '');
            return response()->json(["status" => true, "message" => $message, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'wsdl_request' => $wsdl->getArrContent()]);
        } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING' || $request->ProcessStatus == 'CLOSED') && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            $data = $this->getRequestDetail(request()->get('id'));
            return response()->json(["status" => true, "message" => "Related workorder in progress", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
        } else if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING' || $request->ProcessStatus == 'CLOSED') && request()->get('manager') == true) {
            $data = $this->getRequestDetail(request()->get('id'));
            return response()->json(["status" => true, "message" => "Related workorder is in progress", "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
        } else if ($request->ProcessStatus == 'INPROGRESS') {
            $data = $this->getRequestDetail(request()->get('id'));
            return response()->json(["status" => false, "message" => "Workorder with Ref ID : " . $data->id . " - " . $data->RemedyTicketId . " has been BOOKED by: " . $data->supportgroupassigneeremedy->full_name, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
        } else {
            return response()->json(["status" => false, "message" => "No ticket available with Ref ID : " . $request->id . " - " . $request->RemedyTicketId . " has been booked or closed by other agent", "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
        }
    }

    public function selectRequest()
    {
        $request = $this->inbox_ds->book(request()->get('id'));
        if ($request->servicecatalog->service_catalog_type == 'WO') {
            return $this->parseWorkorder($request);
        } else if ($request->servicecatalog->service_catalog_type == 'IN') {
            return $this->parseIncident($request);
        }
    }

    public function updateTier()
    {
        $inc = new Incident();
        $inc->parseInbox($this->inbox_ds->find(request()->get('id')));
        $inc->generateTier(request()->get('content'));
        $request = $this->inbox_ds->update(request()->get('id'), array("ProcessStatus" => request()->get('status'), "TextDecoded" => $inc->text_decoded));
        $data = $this->getRequestDetail($request->id);
        if ($request!==null) {
            $message = "Tier has been updated";
        } else {
            $message = "Opps.. Failed to update tier ";
        }
        return response()->json(['status' => true, 'message' => $message, 'responsedata' => $data]);
    }

    public function updateIncident()
    {
        $inc = new Incident();
        $request = $this->inbox_ds->update(request()->get('id'), array("ProcessStatus" => $inc->generateStatus(request()->get('status'))));
        $inc->parseInbox($request);
        if($request->Text == 'Livechat IT' || $request->Text == 'Livechat Cabang' || $request->Text == 'Livechat Kartu Kredit'){
            $inc->notes_description = $this->chathis_ds->getHistory(request()->get('id'))['content_summary'];
            $inc->notes_description = $inc->notes_description != null ? preg_replace('/[^\x00-\x7F]/', '', $inc->notes_description): $inc->notes_description;
            $inc->notes_description = $inc->notes_description != null ? preg_replace('/[#]/', '', $inc->notes_description): $inc->notes_description;
            $inc->reported_source = "Livechat";
        }        
        $inc->updateResolution(request()->get('resolution'), $inc->notes_description);
        $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($inc);
        if ($wsdl->getResponseStatus() == 'success') {
            $request = $this->inbox_ds->update(request()->get('id'), array("TextDecoded" => $inc->text_decoded));
            $message = "Ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . " has been marked as " . $request->ProcessStatus;
        } else {
            $request = $this->inbox_ds->update(request()->get('id'), array("TextDecoded" => $inc->text_decoded, "ActionTicketCreation" => 'IN_PENDING_CLS'));
            $message = "Failed to change status ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . ", please retry process from Pending Ticket menu";
        }
        $this->notif_ds->performFeedBackNotifierINC($request);
        return response()->json(["notes" => $inc->notes_description, "status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $inc, 'wsdl_request' => $wsdl->getArrContent()]);
    }

    public function delegateIncident()
    {
        $inc = new Incident();
        $request = $this->inbox_ds->find(request()->get('id'));
        $assignee = $this->suppusr_ds->findByUserId(request()->get('delegateTo'));
        if ($assignee) {
            $inc->parseInbox($this->inbox_ds->update($request->id, array("EmployeeAssigneeId" => $assignee->employee_remedy_id, "EmployeeAssigneeGroupId" => $assignee->support_group_id)));
            $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($inc);
            if ($wsdl->getResponseStatus() == 'success') {
                $message = "Ref No :" . $request->id . " - " . $request->RemedyTicketId . " delegated to : " . $inc->assignee_name;
                $inbox = $this->inbox_ds->refresh($request);
                $this->notif_ds->performFeedBackNotifierINC($inbox);
            } else {
                $this->inbox_ds->update($request->id, array("AssigneeId" => Auth::user()->id, "ActionTicketCreation" => 'IN_PENDING_DLG'));
                $message = "Ref No :" . $request->id . " - " . $request->RemedyTicketId . " failed to delegate, check Pending Ticket menu and resubmit immediately!";
            }
            return response()->json(["status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $inc, 'wsdl_request' => $wsdl->getArrContent()]);
        } else {
            return response()->json(["status" => false, "message" => "User is not found! contact admin immediately! if you wish to delegate to support group, contact your manager and delegate from manager menu"]);
        }
    }

    public function updateWO()
    {
        $wo = new Workorder();
        $request = $this->inbox_ds->update(request()->get('id'), array("ProcessStatus" => $wo->generateStatus(request()->get('status'))));
        $wo->parseInbox($request);
        $wsdl_add_note_rslt = $this->wocreate_wsdl->addNoteWO($wo, request()->get('notes'));
        if ($wsdl_add_note_rslt->getResponseStatus() == 'success') {
            $wsdl = $this->woupdate_wsdl->updateWOGeneric($wo);
            if ($wsdl->getResponseStatus() == 'success') {
                $this->notif_ds->performFeedBackNotifierWO($request, request()->get('notes'));
                $message = "Ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . " has been marked as " . $request->ProcessStatus;
            } else {
                $request = $this->inbox_ds->update(request()->get('id'), array("ActionTicketCreation" => 'WO_PENDING_CLS'));
                $message = "Failed to change status ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . ", please retry process from Pending Ticket menu";
            }
        } else {
            $this->inbox_ds->update(request()->get('id'), array("ProcessStatus" => 'INPROGRESS'));
            return response()->json(["status" => false, "message" => "Unable to connect to Remedy, your request not being processed!", 'wsdl' => $wsdl_add_note_rslt->getResponseMessage(), 'wsdlStatus' => $wsdl_add_note_rslt->getResponseStatus() == 'success' ? true : false, "workorder" => $wo, 'wsdl_request' => $wsdl_add_note_rslt->getArrContent()]);
        }        
        return response()->json(["status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "workorder" => $wo, 'wsdl_request' => $wsdl->getArrContent()]);
    }

    public function delegateWO()
    {
        $wo = new Workorder();
        $request = $this->inbox_ds->find(request()->get('id'));
        $assignee = $this->suppusr_ds->findByUserId(request()->get('delegateTo'));
        if ($assignee) {
            $wo->parseInbox($this->inbox_ds->update($request->id, array("EmployeeAssigneeId" => $assignee->employee_remedy_id, "EmployeeAssigneeGroupId" => $assignee->support_group_id)));
            $wsdl = $this->woupdate_wsdl->updateWOGeneric($wo);
            if($wsdl->getResponseStatus()=='success'){
                $message = "Ref No :" . $request->id . " - ".$request->RemedyTicketId." delegated to :". $wo->assignee_name;
                $inbox = $this->inbox_ds->refresh($request);
                $this->notif_ds->performFeedBackNotifierWO($inbox);
            }else{
                $this->inbox_ds->update($request->id,array("AssigneeId" => Auth::user()->id,"ActionTicketCreation" => 'IN_PENDING_DLG'));
                $message = "Ref No :" . $request->id . " - ".$request->RemedyTicketId." failed to delegate, check Pending Ticket menu and resubmit immediately!";
            }
            return response()->json(["status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $wo, 'wsdl_request' => $wsdl->getArrContent()]);
        } else {
            return response()->json(["status" => false, "message" => "User is not found! contact admin immediately! if you wish to delegate to support group, contact your manager and delegate from manager menu"]);
        }
    }

    public function index()
    {
       
        if (Auth::user()->can('dashboard-manager')) {
            return redirect('/dashboard/manager/' . Auth::user()->sgn_id_remedy);
        } else        
        if (Auth::user()->can('dashboard-agent')) {
            return redirect('/dashboard/agent/LAYANAN_IT');
        } else
        if (Auth::user()->can('dashboard-agent-branch')) {
            return redirect('/dashboard/agent/LAYANAN_CABANG');
        } else
        if (Auth::user()->can('dashboard-agent-cards')) {
            return redirect('/dashboard/agent/LAYANAN_CC');
        } else
        if (Auth::user()->can('dashboard-agent-sds')) {
            return redirect('/dashboard/agentSDS/LAYANAN_SDS');
        }
    }

    public function createIncomingIncident($service_type)
    {
        try {
            $inc = new Incident();
            $content = request()->get('content');
            $inc->parseContent($content);
            $inbox = $this->inbox_ds->addByInc($inc, request()->get('requestId') ? request()->get('requestId') : null);
            $inc->parseInbox($inbox);
            
            $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($inc->service);
            $wsdl = $this->inccreate_wsdl->createIncidentGeneric($inc, $wsdl_reconID->getContent());

            $request = $this->inbox_ds->refresh($inbox);
            $inc->parseInbox($request);
            if ($wsdl->getResponseStatus() == 'success') {
                    $findWorkinfoId = $this->workinfolog_ds->findWorkinfobyId($request->id);
                    if($findWorkinfoId !== null){
                        $getFile = Storage::disk('public')->get($request->id . "/" . $request->id . "_" . $findWorkinfoId->id.".".$findWorkinfoId->file_ext);
                        if($this->uploadFiletoRemedy($getFile, $findWorkinfoId) == true){
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        }else{
                            $this->inbox_ds->update($findWorkinfoId->id, array("ActionTicketCreation" => "IN_PENDING_UPL"));
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created, but failed to upload file, Please try again in Pending Ticket !";
                        }
                    }else{
                        $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                    }
                $request['incident'] = $inc->parseInbox($request);
            } else {
                $message = "Opps.. Error captured, unable to create Incident Number, please retry from Pending Ticket Menu immediately! " . $wsdl->getResponseMessage();
                $this->updateRequestDueException($inbox->id, $inbox->ProcessStatus, "IN_PENDING_PRS", Auth::user()->id, $inbox->AssigneeId);
            }
            return response()->json(['status' => true, 'message' => $message, 'responsedata' => $request, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'wsdl_request' => $wsdl->getArrContent(), "processStatus" => $inbox->ProcessStatus]);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed ! Error : " . $ex]);
        }
    }

    public function createAndAproveIncident(){
        try {
          
            $inc = new Incident();
            $content = request()->get('content');
            $inc->parseContentIncidentOd($content);
            $inbox = $this->inbox_ds->addByInc($inc, request()->get('requestId') ? request()->get('requestId') : null);
            $inc->parseInboxOd($inbox);
            $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($inc->service);
            $wsdl = $this->inccreate_wsdl->createIncidentGenericOd($inc, $wsdl_reconID->getContent());
            $request = $this->inbox_ds->refresh($inbox);
            $inc->parseInboxOd($request);
            if ($wsdl->getResponseStatus() == 'success') {
                    $findWorkinfoId = $this->workinfolog_ds->findWorkinfobyId($request->id);
                    if($findWorkinfoId !== null){
                        $getFile = Storage::disk('public')->get($request->id . "/" . $request->id . "_" . $findWorkinfoId->id.".".$findWorkinfoId->file_ext);
                        if($this->uploadFiletoRemedy($getFile, $findWorkinfoId) == true){
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        }else{
                            $this->inbox_ds->update($findWorkinfoId->id, array("ActionTicketCreation" => "IN_PENDING_UPL"));
                            $message = "Incident Number: " . $wsdl->getContent() . " has been created, but failed to upload file, Please try again in Pending Ticket !";
                        }
                    }else{

                        $message = "Incident Number: " . $wsdl->getContent() . " has been created";
                        $this->inbox_ds->update($request->id, array("RemedyTicketId" => $wsdl->getContent()));
                    }
                $request['incident'] = $inc->parseInbox($request);
            } else {
                $message = "Opps.. Error captured, unable to create Incident Number, please retry from Pending Ticket Menu immediately! " . $wsdl->getResponseMessage();
                $this->updateRequestDueException($inbox->id, $inbox->ProcessStatus, "IN_PENDING_PRS", Auth::user()->id, $inbox->AssigneeId);
            }
           // $odIncident = $this->od_incident->requestListIncident('IN',$inbox->id);
            return response()->json(['status' => true, 'message' => $message, 'responsedata' => $request,'inboxId'=>$inbox->id, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'wsdl_request' => $wsdl->getArrContent(), "processStatus" => $inbox->ProcessStatus]);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed ! Error : " . $ex]);
        }

    }
    /* End */

    public function uploadFile(Request $request)
    {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            
            //     "path"=> $file->getRealPath();
            $param = json_decode(request()->get('file'));
            $array_attr = array(
                "request_id" => $param->requestid,
                "submitter_id" => Auth::user()->id,
                "workinfo_type" => "General Information",
                "workinfo_content" => $param->content,
                "first_attachment_file_name" => $name,
                "file_ext" => $file->getClientOriginalExtension(),
                "file_size_in_bytes" => $file->getSize()
            );
            $tamp_file = $this->workinfolog_ds->addAttachFile($array_attr);
            
            Storage::disk('public')->putFileAs($tamp_file->request_id, $file, $tamp_file->request_id . "_" . $tamp_file->id.".".$file->getClientOriginalExtension());
            if ($tamp_file->id !== null | $tamp_file->id !== "") {
                $message = "File berhasil disimpan";
            } else {
                $message = "Opps.. File gagal disimpan";
            }
            return response()->json(["status" => true, 'message' => $message, 'responsedata' => $tamp_file, 'param' => $param]);
        // } catch (\Throwable $th) {
        //     return response()->json(["status" => true, 'message' => 'failed upload the file' . $th->__toString()]);
        // }
    }

    public function deleteFile(Request $request)
    {
        $arr_str_split = explode("_", $request->getContent());
        $detail_workinfo = $this->workinfolog_ds->findWorkinfo($arr_str_split[1]);
        Storage::disk("public")->delete($arr_str_split[0] . "/" . $request->getContent() . "." . $detail_workinfo['file_ext']);
        $this->workinfolog_ds->deleteAttachFile($arr_str_split[1]);
        // return response()->json(["status" => true, 'message' => 'successfully remove the file', 'content' => $request->getContent()]);
        return response($request->getContent(), 200)
            ->header('Content-Type', 'text/plain');
    }

    public function uploadFiletoRemedy($getFile, $findWorkinfoId){
        $encryptedContent = base64_encode($getFile);
        $findIncident = $this->inbox_ds->find($findWorkinfoId->request_id);
        $array_data = array(
            "ticket_id" => $findIncident->RemedyTicketId,
            "attachment_data" => $encryptedContent,
            "attachment_name" => $findWorkinfoId->first_attachment_file_name,
            "attachment_size" => $findWorkinfoId->file_size_in_bytes,
            "type"=> "General Information",
            "notes"=> $findWorkinfoId->workinfo_content
        );
        $wsdl = $this->incupdate_wsdl->addWorkInfoLogInc($array_data);
        if ($wsdl->getResponseStatus() == 'success') {
            return true;
        } else {
            return false;
        }
    }

    public function changeAdminDispatcher()
    {
        $actual_assignee = $this->user_service->findByUserId(request()->get('delegateTo'));
        $array_inbox = array(
            "EmployeeAssigneeId" => $actual_assignee->employee_remedy_id,
            "EmployeeAssigneeGroupId" => $actual_assignee->sgn_id_remedy,
            "AssigneeId" => $actual_assignee->id,
            "ActionTicketCreation" => 'IN_REVIEW'
        );
        $inbox = $this->inbox_ds->update(request()->get("id"),$array_inbox);
        return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Your ticket with id: " . $inbox->id . " change Admin dispatcher to :" . $actual_assignee->name]);
    }

    protected function initIncidentWhatsappDispatcher($request_id)
    {
        $array_inbox = array(
            "EmployeeAssigneeId" => Auth::user()->employee_remedy_id,
            "EmployeeAssigneeGroupId" => Auth::user()->sgn_id_remedy,
            "ProcessStatus" => "INPROGRESS"
        );
        // $request = $this->taskrequest_service->update($request_id, $array_inbox);
        $request = $this->inbox_ds->update($request_id, $array_inbox);
        $request['messages'] = $this->wadispatcher_ds->getDispatcherMessageList($request_id);
        return $request;
    }

    protected function getIncidentWhatsappDispatcher($request_id)
    {
        $request = $this->inbox_ds->find($request_id);
        $request['messages'] = $this->wadispatcher_ds->getDispatcherMessageList($request_id);
        return $request;
    }

    public function searchRequestQueue()
    {
        // return $this->taskrequest_service->getRequestListByStatusAndCategoryJson('QUEUED', request()->get('servicetype'), request()->get('search'));
        return $this->inbox_ds->getByStatusAndCategory('QUEUED', request()->get('servicetype'), request()->get('search'), request()->get('date'));
    }

    public function searchWorkingTask()
    {
        // return $this->taskrequest_service->getTaskListByAssingeeAndStatusJson(Auth::user()->employeeremedy->id, request()->get('search'));
        return $this->inbox_ds->getByAssigneeAndStatus(Auth::user()->employeeremedy->id, request()->get('search'));
    }

    public function getNote()
    {
        $inc = new Incident();
        $request = $this->inbox_ds->find(request()->get('requestId'));
        $data['incident'] = $inc->parseInbox($request);
        if($data['incident'] == null){
            return response()->json(['status' => false]);
        }else{
            return response()->json(['status' => true,'data' => $data['incident']]);
        }
    }

    public function searchServiceCatalog()
    {
        return $this->srvctlg_ds->searchServiceCatalog(request()->get('name'), request()->get('nameOfService'), request()->get('serviceType'));
    }

    public function getRequestDetail($request_id)
    {
        // $data = null;
        // switch ($service_catalog_id) {
        //     case 64:
        //         //SDS Remitten 
        //         // $data = $this->getIncidentStatusP2ERR($request_id);
        //         $data = $this->inbox_ds->find($request_id);
        //         break;
        //     default:
        //         $data = $this->inbox_ds->find($request_id);
        //         // $data = $this->getGenericRequest($request_id);
        //         break;
        // }
        // return $data;

        $inc = new Incident();
        $request = $this->inbox_ds->find(request()->get("id"));
        $data = $this->inbox_ds->refresh($request);
        
        $findWorkinfoId = $this->workinfolog_ds->findWorkinfobyId($request_id);
        if($findWorkinfoId !== null){
            $data['inboxworkloginfo_url'] = url("/storage/".$findWorkinfoId->request_id. "/" .$findWorkinfoId->request_id. "_" .$findWorkinfoId->id. "." .$findWorkinfoId->file_ext);
            $data['file_name'] = $findWorkinfoId->first_attachment_file_name;
        }else{
            $data['inboxworkloginfo_url'] = null;
        }
        $data['incident'] = $inc->parseInbox($data);
        return $data;
    }

    public function initRequestToMyTask($service_catalog_id, $request_id)
    {
        switch ($service_catalog_id) {
            case 37:
                //Dispatcher Whatsapp
                return $this->initIncidentWhatsappDispatcher($request_id);
                break;
            default:
                $inc = new Incident();
                $request = $this->inbox_ds->find($request_id);
                $data = $this->inbox_ds->refresh($request);
                $data['incident'] = $inc->parseInbox($data);
                return $data;
                break;
        }
    }

    ///////////////////////////////////////// Below Code is Before Assessment ////////////////////////////    

    public function createStagingIncident() // Masih pake function lama (digunakan oleh Email Layanan SDS & WA)
    {
        $array_inbox = array(
            "RecipientID" => request()->get('recipientid'),
            "ProcessStatus" => request()->get("status"),
            "ActionTicketCreation" => request()->get('actticketcreation'),
            "AssigneeId" => Auth::user()->id,
            "ServiceCatalogId" => request()->get('servicecatalogid')
        );
        // $request = $this->taskrequest_service->add($array_inbox);
        // $content = request()->get('content');
        // $inc->parseContent($array_inbox);
        $request = $this->inbox_ds->add($array_inbox);
        // $inc->parseInbox($inbox);
        // $wsdl = $this->inccreate_wsdl->createIncidentGeneric($inc);
        return response()->json(["status" => true, "message" => "Creating Incident draft with Request ID : " . $request->id, "responsedata" => $request, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
    }

    public function sdAgentDashboard($service_type)
    {
        // $request_queue = $this->taskrequest_service->getRequestListByStatusAndCategoryJson('QUEUED', $service_type);
        $request_queue = $this->inbox_ds->getByStatusAndCategory('QUEUED', $service_type);
        
        // $working_task = $this->taskrequest_service->getTaskListByAssingeeAndStatusJson(Auth::user()->employeeremedy->id, 'INPROGRESS');
        $working_task = $this->inbox_ds->getByAssigneeAndStatus(Auth::user()->employeeremedy->id, 'INPROGRESS');
        $in_history = $this->inbox_ds->getRequestsHistory(Auth::user()->employee_remedy_id, 'IN', false, "false");
        $wo_history = $this->inbox_ds->getRequestsHistory(Auth::user()->employee_remedy_id, 'WO', false, "false");
        $daily_chart['dailytask'] = app()->call('App\Http\Controllers\ReportingController@getPersonalTaskReport');
        $daily_chart['weeklytask'] = app()->call('App\Http\Controllers\ReportingController@getPersonalWeeklyTaskReport');
        $daily_chart['dailychannel'] = app()->call('App\Http\Controllers\ReportingController@getPersonalDailyChannelReport');
        $daily_chart['weeklychannel'] = app()->call('App\Http\Controllers\ReportingController@getPersonalWeeklyChannelReport');
        JavaScript::put([
            "serviceType" => $service_type,
            "workingTask" => $working_task,
            "inHistory" => $in_history,
            "woHistory" => $wo_history,
            "requestQueue" => $request_queue,
            "chart" => $daily_chart,
            "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']),
        ]);
        return View::make('landing/dashboard');
    }


    // public function fetchTest($request_id)
    // {
    //     $request = $this->taskrequest_service->getQueueObjectById($request_id);
    //     $workinfos = $this->wocreate_wsdl->fetchWorkInfoList($request->RemedyTicketId)->getArrContent();
    //     foreach ($workinfos as $workinfo) {
    //         $wi = new InboxWorkinfolog();
    //         $wi->id = $workinfo['workinfo_id'];
    //         $wi->request_id = $request_id;
    //         $wi->workinfo_type = $workinfo['workinfo_type'];
    //         $wi->workinfo_content = $workinfo['workinfo_content'];
    //         $wi->submit_date = $workinfo['submit_date'];
    //         $wi->submitter_id = $workinfo['submitter_id'];
    //         $wi->save();
    //     }
    //     return response()->json(["data" => $workinfos]);
    // }

    // public function closeTask()
    // {
    //     $status = 'CLOSED';
    //     switch (request()->get('status')) {
    //         case 'In Progress':
    //             $status = 'INPROGRESS';
    //             break;
    //         case 'Pending':
    //             $status = 'PENDING';
    //             break;
    //         case 'Waiting Approval':
    //             $status = 'WAITINGAPR';
    //             break;
    //         case 'Planning':
    //             $status = 'PLANNING';
    //             break;
    //         case 'Cancelled':
    //             $status = 'REJECT';
    //             break;
    //         case 'Rejected':
    //             $status = 'REJECT';
    //             break;
    //         default:
    //             $status = 'CLOSED';
    //             break;
    //     }
    //     $task = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
    //     $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($task->EmployeeAssigneeId, $task->EmployeeAssigneeGroupId);
    //     $wsdl_add_note_rslt = $this->wocreate_wsdl->addNote($task, request()->get('notes'));
    //     if ($wsdl_add_note_rslt->getResponseStatus() == 'success') {
    //         $wsdl_update_status_rslt = $this->woupdate_wsdl->updateStatus($task, request()->get('status'), $actual_assignee->id);
    //         if ($wsdl_update_status_rslt->getResponseStatus() == 'success') {
    //             $inbox = $this->taskrequest_service->updateStatusById(request()->get('id'), request()->get('assigneeId'), $actual_assignee->id, $status, 'WO_COMPLETED');
    //             $this->notif_ds->performFeedBackNotifier($inbox, request()->get('status'), request()->get('notes'));
    //             return response()->json(["status" => true, "message" => "Ticket " . $task->RemedyTicketId . " has been mark as " . $status, 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //         } else {
    //             $this->taskrequest_service->updateStatusById(request()->get('id'), $actual_assignee->id, request()->get('assigneeId'), $status, 'WO_PENDING_CLS');
    //             return response()->json(["status" => true, "message" => "Failed to mark as " . $status . " for ticket " . $task->RemedyTicketId . ", please retry process from Pending Ticket Remedy menu", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     } else {
    //         return response()->json(["status" => false, "message" => "Unable to connect to Remedy, your request not being processed!"]);
    //     }
    // }     

    // public function delegateTask()
    // {
    //     try {
    //         $request = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
    //         if ($request->RecipientID == 'LIVECHAT') {
    //             $delegate_to = $this->user_service->getUser(request()->get('delegateTo'));
    //             $array_inbox = array(
    //                 "AssigneeId" => $delegate_to->id,
    //                 "EmployeeAssigneeId" => $delegate_to->employee_remedy_id,
    //                 "EmployeeAssigneeGroupId" => $delegate_to->sgn_id_remedy
    //             );
    //             $inbox = $this->taskrequest_service->update(request()->get('id'), $array_inbox);
    //             // $inbox = $this->taskrequest_service->updateQueueStatusById(request()->get('id'), 'INPROGRESS', $actual_assignee->id, request()->get('delegateTo'));
    //             $this->notif_ds->performFeedBackNotifier($inbox);
    //             return response()->json(["status" => true, "message" => "Ticket " . $request->id . " delegated and has been moved from your task!"]);
    //         } else {
    //             $wsdl_rslt = $this->woupdate_wsdl->updateAssignee($request, request()->get('delegateTo'));
    //             if ($wsdl_rslt->getResponseStatus() == 'success') {
    //                 $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($request->EmployeeAssigneeId, $request->EmployeeAssigneeGroupId);
    //                 $this->taskrequest_service->updateQueueStatusById(request()->get('id'), 'INPROGRESS', $actual_assignee->id, request()->get('delegateTo'));
    //                 return response()->json(["status" => true, "message" => "Ticket " . $request->id . " delegated and has been moved from your task!", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //             } else {
    //                 $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($request->EmployeeAssigneeId, $request->EmployeeAssigneeGroupId);
    //                 $this->taskrequest_service->updateStatusById(request()->get('id'), Auth::user()->id, $actual_assignee->id, 'INPROGRESS', 'WO_PENDING_DLG');
    //                 return response()->json(["status" => true, "message" => "Delegate failed! you still the owner of the ticket", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //             }
    //         }
    //     } catch (Exception $e) {
    //         report($e);
    //         return response()->json(["status" => false, "message" => "Fatal Error : Unable to connect to Remedy, your request not being processed!"]);
    //     }
    // }    

    // protected function getIncidentStatusP2ERR($request_id)
    // {
    //     $request = $this->taskrequest_service->getQueueObjectById($request_id);
    //     return $request;
    // }

    // protected function parseUrgency($urgency_string) \\moved to SDSSpecialist Controller
    // {
    //     $urgency = 'low';
    //     if ($urgency_string == '4-Low') {
    //         $urgency = 'Low';
    //     } else if ($urgency_string == '3-Medium') {
    //         $urgency = 'High';
    //     } else if ($urgency_string == '2-High') {
    //         $urgency = 'Critical';
    //     } else if ($urgency_string == '1-Critical') {
    //         $urgency = 'Critical';
    //     }
    //     return $urgency;
    // }

    // protected function getAssignee()
    // {
    //     return $this->user_service->getSupportGroupUserByUserId(request()->get('assignee_id'));
    // }

    // protected function getCustomer()
    // {
    //     return $this->user_service->getEmployeeRemedy(request()->get('customer_id'));
    // }    

    // protected function getGenericRequest($request_id)
    // {        
    //     $request = $this->taskrequest_service->getQueueObjectById($request_id);
    //     return $request;
    // }    
}
