import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            name: ""
        }
    };
};

const state = {
    roles: [],
    simpleHeader: [
        { text: "Nama", value: "name" },
        { text: "Dibuat Pada", value: "created_at" },
        { text: "Actions", value: "action", sortable: false }
    ],
    editedItem: defaultItem().item,
    editedIndex: defaultItem().index,
    apiState: ENUM.INIT,
    alertMessage: "",
};

const getters = {
    getRoles: state => state.roles,
    getSimpleHeaders: state => state.simpleHeader,
    getEditItem: state => state.editedItem,
    getEditIndex: state => state.editedIndex,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
};

const actions = {
    async fetchRoles({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/roles");
            commit("setRoles", response.data.roles);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async addRole({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/roles/create", payload);
            commit("newRole", response.data.role);
            commit("setSuccess", "Data berhasil disimpan !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on add!");
        }
    },
    async updateRole({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.put("/data_management/problems/roles/update", payload);
            commit("updateRole", response.data.role);
            commit("setSuccess", "Data berhasil diperbarui !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on update");
        }
    },
    async deleteRole({ commit }, roleId) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.delete("/data_management/problems/roles/delete/" + roleId);
            if(response.data.status === true){
                commit("deleteRole", response.data.role);
                commit("setSuccess", "Data berhasil dihapus !");
            }else{
                commit("setWarning", response.data.message)
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Penghapusan role gagal dilakukan, hubungi admin !"
                  );
        }
    },
    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
    resetRoles({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setRoles", []);
    }
};

const mutations = {
    newRole: (state, role) => {
        state.apiState = ENUM.LOADED;
        state.roles.unshift(role);
    },
    deleteRole: (state, deletedRole) => {
        state.apiState = ENUM.LOADED;
        const index = state.roles.findIndex(role => role.id == deletedRole.id);
        state.roles.splice(index, 1);
    },
    updateRole: (state, updatedRole) => {
        state.apiState = ENUM.LOADED;
        const index = state.roles.findIndex(role => role.id === updatedRole.id);
        state.roles.splice(index, 1, updatedRole);
    },
    setRoles: (state, roles) => {
        state.apiState = ENUM.LOADED;
        state.roles = roles;
    },
    setEditItem: (state, payload) => (state.editedItem = payload),
    setEditIndex: (state, index) => (state.editedIndex = index),
    setApiState: (state, enumState) => (state.apiState = enumState),
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
