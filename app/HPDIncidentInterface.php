<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HPDIncidentInterface extends Model
{
    protected $primaryKey = 'IncidentNumber';
    protected $table = 'hpd_incident_interface';
    public $incrementing = false;
    public $timestamps = false;

    public function employeeremedy()
    {
        return $this->belongsTo(EmployeeRemedy::class, 'CustomerPersonId', 'id');
    }
}