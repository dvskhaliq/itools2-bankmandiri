<?php

namespace App\ServiceHandler\DataLayer;

use App\Inbox;
use App\Http\Resources\InboxCollection;
use App\Http\Resources\Inbox as InboxResource;

//Not Used
class HistoryImp implements HistoryService
{

    public function getRequestsHistory($employee_remedy_id, $service_catalog_type, $manager, $field = "", $searchdate = "", $tosearchdate = "", $search = "")//Pindah ke InboxImp
    {
        if ($manager == 'true') {
            $group_id = 'EmployeeAssigneeGroupId';
        } else {
            $group_id = 'EmployeeAssigneeId';
        }
        if ($field && $searchdate && $tosearchdate && $search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->where($field, 'ilike', '%' . $search . '%')
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } elseif ($searchdate && $tosearchdate && !$search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } elseif ($field && $searchdate && !$tosearchdate && $search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                ->where($field, 'ilike', '%' . $search . '%')
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } elseif ($field && !$searchdate && $tosearchdate && $search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->whereBetween('FinishDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->where($field, 'ilike', '%' . $search . '%')
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } elseif ($searchdate && !$tosearchdate && !$search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } elseif ($field && !$searchdate && !$tosearchdate && $search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->where($field, 'ilike', '%' . $search . '%')
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } elseif (!$searchdate && $tosearchdate && !$search) {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->whereBetween('FinishDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        } else {
            $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where($group_id, $employee_remedy_id)
                ->where('ProcessStatus', 'CLOSED')
                ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                    $q->where('service_catalog_type', $service_catalog_type);
                })
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'desc');
        }
        $data = new InboxCollection($results->paginate(15));
        $collect_data = [
            'data' => $data,
            'count' => $results->count()
        ];
        return $collect_data;
    }

    public function getRequestListByStatusAndSupportGroupJson($process_status, $channel_name, $employee_assignee_group_id, $searchdate = "", $tosearchdate = "", $name = "")//Pindah ke InboxImp
    {
        if ($searchdate && $tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereHas('supportgroupassigneeremedy', function ($q) use ($name) {
                    $q->where('full_name', 'ILIKE', '%' . $name . '%');
                })
                ->whereBetween('ReceivingDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif ($searchdate && $tosearchdate && !$name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereBetween('ReceivingDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif ($searchdate && !$tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereHas('supportgroupassigneeremedy', function ($q) use ($name) {
                    $q->where('full_name', 'ILIKE', '%' . $name . '%');
                })
                ->whereBetween('ReceivingDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif (!$searchdate && $tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereHas('supportgroupassigneeremedy', function ($q) use ($name) {
                    $q->where('full_name', 'ILIKE', '%' . $name . '%');
                })
                ->whereBetween('ReceivingDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif ($searchdate && !$tosearchdate && !$name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereBetween('ReceivingDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif (!$searchdate && $tosearchdate && !$name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereBetween('ReceivingDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif (!$searchdate && !$tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereHas('supportgroupassigneeremedy', function ($q) use ($name) {
                    $q->where('full_name', 'ILIKE', '%' . $name . '%');
                })
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } else {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        }
        $collect_data = [
            'data' => $data,
            'count' => $data->count()
        ];
        return $collect_data;
    }
}
