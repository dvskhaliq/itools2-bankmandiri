@extends('sidebar')
@section('content')
<div id="bds_server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <div v-if="status_connection">
                        <h4>BDS Server - Connected To : @{{targetBranch.ipServer + ' - ' + targetBranch.database + ' - ' + targetBranch.namaCabang}}</h4>
                    </div>
                    <div v-else>
                        <h4>BDS Server - No Server Connected</h4>
                    </div>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="ismodal" info=""></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <component-table :result="result" v-on:search-data="searchData" :ismodal="ismodal" v-on:ip-server="ftpConnect" v-on:edit-data="dataSelected"></component-table>
                                <vue-pagination :pagination="pages" @paginate="getData()" :offset="4">
                                </vue-pagination>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <component-seleceted :status_connection="status_connection" v-on:branch-selected="submitConnect" v-on:submit-disconnect="submitDisconnect"></component-seleceted>
                                @can('bds-server-configuration')
                                <component-insert v-on:submit-insert="submitInsert" v-on:submit-update="submitUpdate" v-on:delete-data="dataSelectedDel" :selected_prop="selected_prop" :editable="hasData"></component-seleceted>
                                @endcan
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <modal-ftp :ismodal="ismodal" :modalname="modalname" :ipserver="ipserver" v-on:close-modal="closeModal"></modal-ftp>

    <div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Confirm Delete</h4>
                </div>
                <div v-if="isloaded">
                    <confirm-delete :selected_prop="selected_prop" v-on:submit-delete="submitDelete"></confirm-delete>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')

<script src="../../js/bds_server.js"></script>
@endsection