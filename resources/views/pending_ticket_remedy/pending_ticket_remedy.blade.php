@extends('sidebar')
@section('content')
<div class="row" id="pending_ticket_remedy">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div class="panel-heading" style="background: #fff;">
                <h4>Pending Ticket Remedy</h4>
            </div>
            <!-- START DEFAULT DATATABLE -->
            <div class="panel-body">
                <flash v-if="!ismodal" info=></flash>
                <div class="panel panel-default">
                    <div class="panel-heading">Pending Ticket Remedy</div>
                    <div class="panel-body">
                        <pending-task v-on:modalclosed="modalClosed" v-on:retryclose="retryPendingCls" v-on:retrydelegate="retryPendingDlg" v-on:view-task="viewTask" :pendingdata="pendingdata" :isclosuring="isclosuring" :ismodal="ismodal" :isdelegating="isdelegating" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Pending WO Closure-->
    <modal-pending-workorder-closure :requestobject="requestobject" :isclosuring="isclosuring" v-on:modalclosed="modalClosed"></modal-pending-workorder-closure>
    <!-- MODAL Pending IN Closure-->
    <modal-pending-incident-closure :requestobject="requestobject" :isclosuring="isclosuring" v-on:modalclosed="modalClosed"></modal-pending-incident-closure>
    <!-- MODAL Pending WO Delegate-->
    <modal-pending-workorder-delegate :requestobject="requestobject" :isdelegating="isdelegating" v-on:modalclosed="modalClosed"></modal-pending-workorder-delegate>
    <!-- MODAL Pending IN Delegate-->
    <modal-pending-incident-delegate :requestobject="requestobject" :isdelegating="isdelegating" v-on:modalclosed="modalClosed"></modal-pending-incident-delegate>
    <!-- MODAL Pending WO View-->
    <modal-wo-view ref="wopendingview"  :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :modalname="modalname" :auth="auth" v-on:modal-closed="cancelView" v-on:reload="modalClosed"></modal-wo-view>
    <!-- MODAL Pending IN View-->
    <modal-incident-view ref="inpendingview" v-on:reload="modalClosed" :requestdata="requestdata" :draftcustomer="customer" :isdataloaded="isdataloaded" :ismodal="ismodal" :modalname="modalname" :auth="auth" v-on:modal-closed="cancelView" v-on:modalclosed="modalClosed"></modal-incident-view>
</div>
@include('footer')
<script src="../../js/pending_ticket_remedy.js"></script>
@endsection
