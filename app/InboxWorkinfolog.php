<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InboxWorkinfolog extends Model
{
    
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['inbox_workinfolog'];
    protected $table = 'inbox_workinfolog';
    public $timestamps = false;
    // public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function inbox()
    {
        return $this->belongsTo(Inbox::class, 'request_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'submitter_id','id');
    }

}

