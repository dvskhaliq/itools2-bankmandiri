<?php

namespace App\ServiceHandler\DataLayer;

interface TbluSourceAspService
{    
    public function fetchAspSourceData();
    public function fetchSuccessRate();
}