<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use View;
use App\User;
use App\RoleUser;
use App\Roles;
use App\SupportGroupAssigneeRemedy;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use JavaScript;
use Crypt;

class ConfigurationUsersController extends BaseController
{
    /**
     * Create a new controller instance.
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        JavaScript::put([
            "auth" => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']),
        ]);
        return View::make('administrator_console/user_configuration/user_configuration');
    }

    public function page()
    {
        $results = User::where('name', 'ILIKE', '%' . request()->get('inputname') . '%')->orWhere('email', 'iLIKE', '%' . request()->get('inputname') . '%')->orderBy('name', 'ASC')->paginate(10);
        return response()->json([
            'result' => $results
        ]);
    }

    public function searchUser()
    {
        $rslts = User::where('name', 'ILIKE', '%' . request()->get('inputname') . '%')
        ->orWhere('email', 'ILIKE', '%' . request()->get('inputname') . '%')
        ->get();
        return response()->json($rslts);
    }

    public function getAssignee()
    {
        $rslts = User::orderBy('name','asc')->get(['id','name','sgn_remedy', 'employee_remedy_id']);
        return response()->json($rslts);
    }

    public function searchUserByGroup()
    {
        $rslts = User::where('name', 'ILIKE', '%' . request()->get('inputname') . '%')
        //->where('sgn_id_remedy',Auth::user()->sgn_id_remedy)
	->whereIn('sgn_id_remedy',array('SGP000000000151','SGP000000000152'))
        ->get();
        return response()->json($rslts);
    }

    public function detailSelected()
    {
        $results = User::where('id', request()->get('id'))->first();
        return response()->json([
            'status' => true,
            'result' => $results
        ]);
    }

    public function detailSelectedRoles()
    {
        // $results = RoleUser::with('roles')->where('user_id', request()->get('id'))->get();
        $results = Roles::orderBy('id')->get();
        $arr_role_id = DB::table('role_users')->where('user_id', request()->get('id'))->pluck('role_id')->toArray();
        return response()->json([
            'status' => true,
            'result' => $results,
            'arr_role_id' => $arr_role_id
        ]);
    }

    public function rolesList()
    {
        $results = Roles::get();
        return response()->json([
            'status' => true,
            'result' => $results
        ]);
    }

    public function addRoles()
    {
        // $findData = RoleUser::where('user_id', request()->get('user_id'))->where('role_id', request()->get('role_id'))->first();
        // if($findData !=null){
        //     if(($findData->user_id == request()->get('user_id')) && ($findData->role_id == request()->get('role_id')) ){            
        //         return response()->json([
        //             'status' => false,
        //             'result' => "Role already added",
        //         ]);
                
        //     }
        // } 
            RoleUser::where('user_id', request()->get('user_id'))->delete();
            $role_id=request()->get('role_id');
            $data = array();
            foreach($role_id as $role)
            {
                if(!empty($role))
                {
                  $data[] =[
                           'user_id' => request()->get('user_id'),
                           'role_id' => $role,
                           'created_at' => date('Y-m-d H:i:s'),
                           'updated_at' => date('Y-m-d H:i:s')
                           ];                 
                        
                }
            }
            RoleUser::insert($data);
            
            return response()->json([
                'status' => true,
                'result' => 'Data has been successfully saved !',
            ]);
                
    }

    public function submitUpdate()
    {
        if(request()->get('status') == 'Enable'){
            $status = true;
        } else {
            $status = false;
        }
        if(request()->get('module_problem')!==null){
            if(request()->get('password')==''){
                $dataUpdate = array(
                    'name' => request()->get('name'),
                    'email' => request()->get('email'),
                    'module_problem' => request()->get('module_problem')=="Enable"?true:false,
                    'problem_role_id' => request()->get('module_problem')=="Enable" ? request()->get('problem_role_id') : null
                );
            }else{
                $dataUpdate = array(
                    'name' => request()->get('name'),
                    'email' => request()->get('email'),
                    'module_problem' => request()->get('module_problem')=="Enable"?true:false,
                    'password' => Hash::make(request()->get('password')),
                    'problem_role_id' => request()->get('module_problem')=="Enable" ? request()->get('problem_role_id') : null
                );
            }
            User::where('id', request()->get('id'))->update($dataUpdate);
            $data = User::where('id', request()->get('id'))->first()->load('pmroles');
            if(Auth::user()->id == request()->get('id')){
                $auth = true;
            }else{
                $auth = false;
            }
            return response()->json([
                'status' => true,
                'message' => "Data has been successfullly updated !",
                'data' => $data,
                'auth' => $auth
            ]);
        }else{
            $dataUpdate = array(
                'name' => request()->get('name'),
                'email' => request()->get('email'),
                'status' => $status,
                'password' => Hash::make(request()->get('passwordiTools')),
                // 'nip' => request()->get('nip'),
                'un_remedy' => request()->get('usernameRemedy'),
                'pass_remedy' => Crypt::encrypt(request()->get('passwordRemedy')),
                'fn_remedy' => request()->get('firstNameRemedy'),
                'ln_remedy' => request()->get('lastNameRemedy'),
                // 'assignee_support_company' => request()->get('assigneeSupportCompany'),
                // 'assignee_support_organization' => request()->get('assigneeSupportOrganization'),
                'sgn_remedy' => request()->get('assigneeSupportGroupName'),
            );
            User::where('id', request()->get('id'))->update($dataUpdate);
            return response()->json([
                'status' => true,
                'result' => "Data has been successfully updated !",
                'data' => $dataUpdate
            ]);
        }
    }

    public function updatePassword()
    {
        try{
            $data = collect(request()->get('data'));
            $data->map(function ($item) {
                if(strlen($item['pass_remedy'])<100){
                    return User::where('id', $item['id'])->update(array('pass_remedy' => Crypt::encrypt($item['pass_remedy'])));
                }
            });        
            return response()->json([
                'status' => true,
                'result' => $data,
                'message' => 'Password has been successfully updated !'
            ], 200);
        }catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function submitInsert()
    {
        $data = SupportGroupAssigneeRemedy::where('remedy_login_id', request()->get('usernameRemedy'))->first();
        if($data == null || $data == ''){
            return response()->json([
                'status' => false,
                'result' => "User is not registered in Remedy",
            ]);
        } else{
            $find_user = User::where('un_remedy', request()->get('usernameRemedy'))->orWhere('email', request()->get('email'))->first();
            if($find_user !=null){
                if($find_user->un_remedy == request()->get('usernameRemedy')){
                    return response()->json([
                        'status' => false,
                        'result' => "Username Remedy has been registered",
                    ]);
                } else if ($find_user->email == request()->get('email')){
                    return response()->json([
                        'status' => false,
                        'result' => "Email address has been registered",
                    ]);
                } 
            } else{
                if(request()->get('status') == 'Enable'){
                    $status = true;
                } else {
                    $status = false;
                }
                $dataInsert = array(
                    'name' => request()->get('name'),
                    'email' => request()->get('email'),
                    'status' => $status,
                    'password' => Hash::make(request()->get('password')),
                    // 'nip' => request()->get('nip'),
                    'un_remedy' => request()->get('usernameRemedy'),
                    'pass_remedy' => Crypt::encrypt(request()->get('passwordRemedy')),
                    'fn_remedy' => request()->get('firstNameRemedy'),
                    'ln_remedy' => request()->get('lastNameRemedy'),
                    // 'assignee_support_company' => request()->get('assigneeSupportCompany'),
                    // 'assignee_support_organization' => request()->get('assigneeSupportOrganization'),
                    'sgn_remedy' => $data->support_group_name,
                    'user_type' => request()->get('userType'),
                    'sgn_id_remedy' => $data->support_group_id,
                    'employee_remedy_id' => $data->employee_remedy_id
                );
        
                User::create($dataInsert);
        
                return response()->json([
                    'status' => true,
                    'result' => "Data has been successfully created !",
                ]);
            }
            
        }
        
    }

    public function deleteRole(){

        RoleUser::where('user_id', request()->get('user_id'))->where('role_id', request()->get('role_id'))->delete();

        return response()->json([
            'status' => true,
            'result' => "Role has been successfully deleted !",
        ]);
    }

    public function submitDelete(){

        User::where('id', request()->get('id'))->delete();
       
        
        RoleUser::where('user_id', request()->get('id'))->delete();
        return response()->json([
            'status' => true,
            'result' => "Data has been successfully deleted !",
        ]);
    }

    public function loginuser(){
        $results = User::where('id', Auth::user()->id)->first();
        JavaScript::put([
            "auth" => $results,
        ]);
        return View::make('administrator_console/user_configuration/userprofile');
    }

    //Problem management
    public function fetchCurrentUser(){
        return response()->json([
            'status' => true,
            'data' => Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled','module_problem','problem_role_id']),
        ]);
    }

    public function fetchListUser()
    {
        $data = User::whereNotNull('module_problem')->orderBy('name', 'asc')->get()->load('pmroles');
        return response()->json([
            'data' => $data
        ]);
    }

    public function fetchListUserItools()
    {
        $data = User::select('id','name','email','employee_remedy_id','sgn_remedy')->whereNull('module_problem')->orderBy('name', 'asc')->get()->load('pmroles');
        return response()->json([
            'data' => $data
        ]);
    }

    public function showMenu()
    {
        $granted = Auth::user()->load('pmroles.pmrolesfeatures.pmfeatures')->pmroles->pmrolesfeatures;
        $menu = array();
        foreach ($granted as $value) {
            if ($value->pmfeatures->is_child) {
                if (in_array($value->pmfeatures->display_group, array_column($menu, 'text'), true)) {
                    array_push($menu[array_search($value->pmfeatures->display_group, array_column($menu, 'text'))]['children'], ['icon' => $value->pmfeatures->icon_name, 'text' => $value->pmfeatures->display_name, 'id' => $value->pmfeatures->name, 'group' => $value->pmfeatures->display_group]);
                } else {
                    $parent = ['icon' => 'mdi-chevron-up', 'text' => $value->pmfeatures->display_group, 'iconalt' => 'mdi-chevron-down', 'model' => false, 'id' => $value->pmfeatures->group,'group' => $value->pmfeatures->display_group];
                    $parent['children'][0] = ['icon' => $value->pmfeatures->icon_name, 'text' => $value->pmfeatures->display_name, 'id' => $value->pmfeatures->name,'group' => $value->pmfeatures->display_group];
                    array_push($menu, $parent);
                }
            } else {
                array_push($menu, ['icon' => $value->pmfeatures->icon_name, 'text' => $value->pmfeatures->display_group, 'id' => $value->pmfeatures->name,'group' => $value->pmfeatures->display_group]);
            }
        }
        return response()->json([
            'status' => true,
            'menu' => $menu
        ], 200);
    }

    public function checkPermission(){
        $given_permit = Auth::user()->load('pmroles.pmrolesfeatures.pmfeatures')->pmroles->pmrolesfeatures;
        $permit = $given_permit->where('pmfeatures.name', request()->get('module'));
        if ($permit->isNotEmpty()) {
            $actions = collect(request()->get('action'));
            $status_permission = $actions->map(function ($value) use ($permit){
                switch ($value) {
                    case 'create':
                        if ($permit->contains('action_create', 1)) {
                            return true;
                        }
                        break;
                    case 'read':
                        if ($permit->contains('action_read', 1)) {
                            return true;
                        }
                        break;
                    case 'update':
                        if ($permit->contains('action_update', 1)) {
                            return true;
                        }
                        break;
                    case 'delete':
                        if ($permit->contains('action_delete', 1)) {
                            return true;
                        }
                        break;
                    default:
                        return false;
                        break;
                }  
            });
            if ($status_permission->contains(false)) {
                return response()->json([
                    'status' => false,
                    'error' => 'Tidak memiliki wewenang, hubungi admin!'
                ], 200);
            } else {
                return response()->json([
                    'status' => true
                ], 200);
            }     
        }
        return response()->json([
            'status' => false,
            'error' => 'Tidak memiliki wewenang, hubungi admin!'], 403);
    }

    public function activatedUsers()
    {
        try{
            $details = collect(request()->get('details'));
            $rslt = $details->map(function ($item) {
                $user = User::find($item['id']);
                $user->module_problem = true;
                $user->save();
                $user->load('pmroles')->refresh();
                return $user;
            });
        return response()->json([
            'status' => true,
            'message' => "Users has been activated !",
            'data' => $rslt
        ]);
        }catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function deactivatedUsers()
    {
        try{
            $details = collect(request()->get('details'));
            $rslt = $details->map(function ($item) {
                $user = User::find($item['id']);
                $user->module_problem = null;
                $user->problem_role_id = null;
                $user->save();
                return $user;
            });
            return response()->json([
                'status' => true,
                'message' => "Users has been deactivated !",
                'data' => $rslt
            ]);
        }catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }
}
