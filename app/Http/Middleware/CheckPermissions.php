<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module, $action)
    {
        $given_permit = Auth::user()->load('pmroles.pmrolesfeatures.pmfeatures')->pmroles->pmrolesfeatures;
        $permit = $given_permit->where('pmfeatures.name', $module);
        if ($permit->isNotEmpty()) {
            switch ($action) {
                case 'create':
                    if ($permit->contains('action_create', 1)) {
                        return $next($request);
                    }
                    break;
                case 'read':
                    if ($permit->contains('action_read', 1)) {
                        return $next($request);
                    }
                    break;
                case 'update':
                    if ($permit->contains('action_update', 1)) {
                        return $next($request);
                    }
                    break;
                case 'delete':
                    if ($permit->contains('action_delete', 1)) {
                        return $next($request);
                    }
                    break;
                default:
                    return response()->json(['error' => 'Is not authorized'], 200);
                    break;
            }            
        }
        return response()->json([
            'status' => false,
            'error' => 'Tidak memiliki wewenang, hubungi admin!'], 403);
    }
}
