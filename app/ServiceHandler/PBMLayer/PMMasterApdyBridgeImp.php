<?php

namespace App\ServiceHandler\PBMLayer;

use App\PMMasterApdyBridge as MasterApdy;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class PMMasterApdyBridgeImp implements PMMasterApdyBridgeService
{
    public function getPMMasterApdyBridge(): ?Collection
    {
        $result = MasterApdy::with('pmmastersystemavailability')->orderBy('system_id','asc')->get();
        return $result;
    }

    public function createPMMasterApdyBridge($payload)
    { 
        $pmmasterapdybridge = new MasterApdy();
        $pmmasterapdybridge->system_id = (array_key_exists("system_id", $payload) ? $payload["system_id"] : null);
        $pmmasterapdybridge->site = (array_key_exists("site", $payload) ? $payload["site"] : null);
        $pmmasterapdybridge->machine = (array_key_exists("machine", $payload) ? $payload["machine"] : null);
        $pmmasterapdybridge->type = (array_key_exists("type", $payload) ? $payload["type"] : null);
        $pmmasterapdybridge->business_txn_metrics = (array_key_exists("business_txn_metrics", $payload) ? $payload["business_txn_metrics"] : null);
        $pmmasterapdybridge->rest_url = (array_key_exists("rest_url", $payload) ? $payload["rest_url"] : null);
        $pmmasterapdybridge->node = (array_key_exists("node", $payload) ? $payload["node"] : null);
        $pmmasterapdybridge->save();
        $pmmasterapdybridge->load('pmmastersystemavailability')->refresh();
        return $pmmasterapdybridge;
    }

    public function updatePMMasterApdyBridge($payload)
    { 
        $pmmasterapdybridge = MasterApdy::find($payload['id']);
        $pmmasterapdybridge->system_id = (array_key_exists("system_id", $payload) ? $payload["system_id"] : null);
        $pmmasterapdybridge->site = (array_key_exists("site", $payload) ? $payload["site"] : null);
        $pmmasterapdybridge->machine = (array_key_exists("machine", $payload) ? $payload["machine"] : null);
        $pmmasterapdybridge->type = (array_key_exists("type", $payload) ? $payload["type"] : null);
        $pmmasterapdybridge->business_txn_metrics = (array_key_exists("business_txn_metrics", $payload) ? $payload["business_txn_metrics"] : null);
        $pmmasterapdybridge->rest_url = (array_key_exists("rest_url", $payload) ? $payload["rest_url"] : null);
        $pmmasterapdybridge->node = (array_key_exists("node", $payload) ? $payload["node"] : null);
        $pmmasterapdybridge->save();
        $pmmasterapdybridge->load('pmmastersystemavailability')->refresh();
        return $pmmasterapdybridge;
    }

    public function deletePMMasterApdyBridge(int $id){
        $pmmasterapdybridge = MasterApdy::find($id);
        $pmmasterapdybridge->delete();
        return $pmmasterapdybridge;
    }

    public function getSystemName(){
        // return MasterApdy::get()->load('pmmastersystemavailability');
        return DB::select("select distinct(b.system_name) from pm_master_apdy_bridge a, pm_master_system_availability b where b.id=a.system_id order by b.system_name");
    }
}
