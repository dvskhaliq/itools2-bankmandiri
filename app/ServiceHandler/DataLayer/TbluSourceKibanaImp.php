<?php 

namespace App\ServiceHandler\DataLayer;

use App\PMTbluKibana as Kibana;
use App\PmTbluSrEmoney as SReMoney;
use App\PMTbluSrSoaBiru as SRsoaBiru;
use Elasticsearch\ClientBuilder;

class TbluSourceKibanaImp implements TbluSourceKibanaService
{
    public function eMoneyRestime($k){
        $date1 = date('Y-m-d', strtotime('-'.($k+1).' day', strtotime(date('Y-m-d'))));
        $date2 = date('Y-m-d', strtotime('-'.$k.' day', strtotime(date('Y-m-d'))));
        $json = '{"query": {"bool": {"must": [{"match": {"apiName.keyword": "Payment"}},{"match": {"status.keyword": "SUCCESS"}},{"match": {"reqPayload.keyword": "topupRequest"}},{"range": {"creationDate": {"gte": "'.$date1.'T17:00:00","lt": "'.$date2.'T17:00:00"}}}],"must_not": [{"match": {"applicationName.keyword": "Unknown"}},{"match": {"status.keyword": "FAILURE"}}],"should": [ ]}},"from": 0,"size": 1,"sort": [ ],"aggs": {"group_by_app": {"terms": { "field": "applicationName.keyword" },"aggs": {"avg_price": {"avg": {"field": "totalTime"}}, "total_price": {"sum": {"field": "totalTime"}}}}}}';
        $parameters = [
            "index" => "gateway_default_analytics",
            "type" => "transactionalEvents",
            "_source" => ["creationDate"],
            "body" => $json
        ];
        $hosts=[env('KIBANA_URL')];
        $client = ClientBuilder::create()->setHosts($hosts)->build();
        $return = $client->search($parameters);
        // Kibana::Truncate();
        foreach ($return['aggregations']['group_by_app'] as $result){
            $data[] = $result;
        }
        for ($i=0;$i<count($data[2]);$i++){
            $coba[] = ([
                        'layanan' => 'eMoney',
                        'restime' => round($data[2][$i]['avg_price']['value'])/1000,
                        'tanggal' => date('Y-m-d', round($return['hits']['hits'][0]['_source']['creationDate']/1000)),
                        'node' => "TopUp from ".$data[2][$i]['key'],
                        'count' => $data[2][$i]['doc_count'],
                        'sum' => $data[2][$i]['total_price']['value']
                    ]);
        }        
        $rslt = Kibana::insert($coba);
        
        return ['data' => $coba, 'result' => $data[2]];
    }

    public function emoneySuccess($k){
        $date1 = date('Y-m-d', strtotime('-'.($k+1).' day', strtotime(date('Y-m-d'))));
        $date2 = date('Y-m-d', strtotime('-'.$k.' day', strtotime(date('Y-m-d'))));
        $json = ['{"query":{"bool":{"must":[{"match":{"apiName":"Payment"}},{"match":{"resPayload":"\"status\":\"0\""}},{"range":{"creationDate":{"gte":"'.$date1.'T17:00:00","lt":"'.$date2.'T17:00:00"}}}],"must_not":[{"match":{"resPayload":"51013"}},{"match":{"resPayload":"51005"}},{"match":{"resPayload":"51002"}}],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{"group_by_app":{"terms":{"field":"applicationName"}}}}', '{"query": {"bool": {"must": [{"match": {"apiName": "Payment"}},{"match": {"resPayload": "error"}},{"range": {"creationDate": {"gte": "'.$date1.'T17:00:00","lt": "'.$date2.'T17:00:00"}}}],"must_not": [{"match": {"resPayload": "51002"}}],"should": [ ]}},"from": 0,"size": 10,"sort": [ ],"aggs":{"group_by_app":{"terms":{"field":"applicationName"}}}}', '{"query": {"bool": {"must": [{"match": {"apiName": "eMoneyCard"}},{"range": {"creationDate": {"gte": "'.$date1.'T17:00:00","lt": "'.$date2.'T17:00:00"}}}],"must_not": [{"match": {"resPayload": "P7"}},{"match": {"resPayload": "51000"}},{"match": {"resPayload": "error"}}],"should": [ ]}},"from": 0,"size": 1,"sort": [ ],"aggs":{"group_by_app":{"terms":{"field":"applicationName"}}}}', '{"query": {"bool": {"must": [{"match": {"apiName": "eMoneyCard"}},{"range": {"creationDate": {"gte": "'.$date1.'T17:00:00","lt": "'.$date2.'T17:00:00"}}}],"must_not": [{"match": {"resPayload": "P7"}},{"match": {"resPayload": "\"0\""}},{"match": {"resPayload": "51010"}}],"should": [ ]}},"from": 0,"size": 1,"sort": [ ],"aggs":{"group_by_app":{"terms":{"field":"applicationName"}}}}'];        
        $parameters = [
            "index" => "gateway_default_analytics",
            "type" => "transactionalEvents",
            "_source" => "creationDate",
            "body" => $json
        ];
        $hosts=env('KIBANA_URL');
        $data = collect($json)->map(function ($item) use ($parameters, $hosts) {
            $response = $this->fetchKibanaData($item, $parameters, $hosts);
            foreach ($response['aggregations']['group_by_app'] as $result){
                $data[] = $result;
            }
            return ['value' => $data[2], 'date' => date('Y-m-d', round($response['hits']['hits'][0]['_source']['creationDate']/1000))];
        });        
        // SReMoney::Truncate();        
        for ($i=0;$i<count($data[0]['value']);$i++){
            switch ($data[0]['value'][$i]['key']) {
                case 'Indomaret':
                    $layanan = 'Indomaret';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Indomaret':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Alfamart':
                    $layanan = 'Alfamart';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Alfamart':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Tokopedia':
                    $layanan = 'Tokopedia';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Tokopedia':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Shopee':
                    $layanan = 'Shopee';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Shopee':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Buka Lapak':
                    $layanan = 'Buka Lapak';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Buka Lapak':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Bliibli':
                    $layanan = 'Bliibli';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Bliibli':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Linkaja':
                    $layanan = 'Linkaja';
                    $success = $data[0]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[1]['value']);$j++){
                        switch ($data[1]['value'][$j]['key']) {
                            case 'Linkaja':
                                $error = $data[1]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                default:
                    $layanan = '';
                    $success = '';
                    $error = '';
                    break;
            }
            if($layanan != ''){
                $coba[] = ([
                    'layanan' => 'eMoney',
                    'tanggal' => $data[1]['date'],
                    'node' => "TopUp from ".$layanan,
                    'success' => intval($success),
                    'error' => intval($error),
                    'total' => (intval($success)+intval($error)),
                    'success_rate' => round((intval($success)/(intval($success)+intval($error)))*100,2)
                ]);
            }
        }
        
        for ($i=0;$i<count($data[2]['value']);$i++){
            switch ($data[2]['value'][$i]['key']) {
                case 'Indomaret':
                    $layanan = 'Indomaret';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Indomaret':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Alfamart':
                    $layanan = 'Alfamart';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Alfamart':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Tokopedia':
                    $layanan = 'Tokopedia';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Tokopedia':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Shopee':
                    $layanan = 'Shopee';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Shopee':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Buka Lapak':
                    $layanan = 'Buka Lapak';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Buka Lapak':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Bliibli':
                    $layanan = 'Bliibli';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Bliibli':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Linkaja':
                    $layanan = 'Linkaja';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Linkaja':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                case 'Mandol':
                    $layanan = 'Mandol';
                    $success = $data[2]['value'][$i]['doc_count'];
                    for ($j=0;$j<count($data[3]['value']);$j++){
                        switch ($data[3]['value'][$j]['key']) {
                            case 'Mandol':
                                $error = $data[3]['value'][$j]['doc_count'];
                                break;
                        }
                    }
                    break;
                default:
                    $layanan = '';
                    $success = '';
                    $error = '';
                    break;
            }
            if($layanan != ''){
                $coba2[] = ([
                    'layanan' => 'eMoney',
                    'tanggal' => $data[3]['date'],
                    'node' => "Inquiry from ".$layanan,
                    'success' => intval($success),
                    'error' => intval($error),
                    'total' => (intval($success)+intval($error)),
                    'success_rate' => round((intval($success)/(intval($success)+intval($error)))*100,2)
                ]);
            }            
        }
        $rslt = SReMoney::insert($coba);
        $rslt = SReMoney::insert($coba2);

        return ['TopUp' => $coba, 'Inquiry' => $coba2];
    }

    public function soaBiruSuccess($k){
        $date2 = date('Y.m.d', strtotime('-'.$k.' day', strtotime(date('Y-m-d'))));
        $json = ['{"query":{"bool":{"must":[{"match":{"status.keyword":"ERROR"}}],"must_not":[],"should":[]}},"from":0,"size":1,"sort":[],"aggs":{"group_by_app":{"terms":{"field":"_type"}}}}', '{"query":{"bool":{"must":[{"match":{"error_code.keyword":"-"}}],"must_not":[],"should":[]}},"from":0,"size":1,"sort":[],"aggs":{"group_by_app":{"terms":{"field":"_type"}}}}'];
        $parameters = [
            "index" => "svcsaudit-prod-common-".$date2,
            "type" => "doc",
            "_source" => "audit_timestamp"
        ];
        $hosts=env('KIBANA_2_URL');        
        $data = collect($json)->map(function ($item) use ($parameters, $hosts) {
            $response = $this->fetchKibanaData($item, $parameters, $hosts);
            foreach ($response['aggregations']['group_by_app'] as $result){
                $data[] = $result;
            }
            return ['value' => $data[2], 'date' => date('Y-m-d', strtotime($response['hits']['hits'][0]['_source']['audit_timestamp']))];
        });
        // SRsoaBiru::Truncate();
                
        $coba[] = ([
            'layanan' => 'SOA Biru',
            'tanggal' => $data[0]['date'],
            'node' => "Default",
            'success' => intval($data[1]['value'][0]['doc_count']),
            'error' => intval($data[0]['value'][0]['doc_count']),
            'total' => (intval($data[1]['value'][0]['doc_count'])+intval($data[0]['value'][0]['doc_count'])),
            'success_rate' => round((intval($data[1]['value'][0]['doc_count'])/(intval($data[1]['value'][0]['doc_count'])+intval($data[0]['value'][0]['doc_count'])))*100,2)
        ]);
        $rslt = SRsoaBiru::insert($coba);
        
        return ['value' => $coba];
    }

    private function fetchKibanaData($json, $param, $host){
        $parameters = [
            "index" => $param['index'],
            "type" => $param['type'],
            "_source" => [$param['_source']],
            "body" => $json
        ];
        $hosts=[$host];
        $client = ClientBuilder::create()->setHosts($hosts)->build();
        $return = $client->search($parameters);
        return $return;
    }
}
