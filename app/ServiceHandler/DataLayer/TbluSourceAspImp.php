<?php

namespace App\ServiceHandler\DataLayer;

use App\PMViewRsptime as AspSource;
use App\PMTbluAspResp as AspResp;
use App\SuccessRate as Success;
use App\PmTbluSr as SuccessRate;

class TbluSourceAspImp implements TbluSourceAspService
{
    public function fetchAspSourceData(){
        $tgl = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $asp_source = AspSource::select('layanan', 'node', 'restime', 'tanggal', 'type')            
            ->get();
        $asp_source->transform(function ($item) {
            $node = '';
            switch ($item->layanan) {                
                case 'Base24':
                    $node = $item->node;
                    break;
                default:
                    $node = 'Default';
                    break;
            }            

            return [
                'layanan' => $item->layanan,
                'restime' => round($item->restime, 3),
                'tanggal' => $item->tanggal,
                'node' => $node,
                'type' => $item->type
            ];
        });
        AspResp::Truncate();
        $chunk_array = array_chunk($asp_source->toArray(),1000,true);
        foreach ($chunk_array as $new_record_chunk)
        {            
            $rslt = AspResp::insert($new_record_chunk);
        }        

        return $rslt;
    }

    public function fetchSuccessRate(){
        $tgl = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $data = Success::select('CHANNEL', 'TGL', 'SUKSES', 'TOTAL', 'AddField1', 'AddField2', 'AddField3')
        ->where('TGL', '<=', $tgl)
        ->get();
        $data->transform(function ($item) {
            $layanan = '';
            switch ($item->CHANNEL) {
                case 'Mandiri Online':
                    $layanan = 'LIVIN 1.0';
                    break;
                default:
                    $layanan = $item->CHANNEL;
                    break;
            }

            return [
                'layanan' => $layanan,
                'tanggal' => $item->TGL,
                'sukses' => $item->SUKSES != null ? round($item->SUKSES, 0) : null,
                'total' => $item->TOTAL != null ? round($item->TOTAL, 0) : null,
                'addfield1' => ($item->AddField1/100),
                'addfield2' => $item->AddField2,
                'addfield3' => $item->AddField3,
            ];
        });
        SuccessRate::Truncate();
        $chunk_array = array_chunk($data->toArray(),1000,true);
        foreach ($chunk_array as $new_record_chunk)
        {
            $rslt = SuccessRate::insert($new_record_chunk);
        }
        return $rslt;
    }
}
