<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluProductionIssues extends Model
{
    protected $table = 'pm_tblu_production_issues';
    public $timestamps = false;
}
