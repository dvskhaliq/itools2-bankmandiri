<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = ['roles'];
    protected $table = 'roles';
    public $timestamps = false;    
    
    public function roles(){
        return $this->hasMany(RoleUser::class,'role_id','id');
    }
}
