<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\ServiceHandler\DataLayer\InboxService;
use Auth;

class HistoryController extends BaseController
{
    protected $inbox_ds;
    public function __construct(
        InboxService $inbox_service
        ) {
        $this->inbox_ds = $inbox_service;
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function searchRequestHistory()
    {
        return $this->inbox_ds->getRequestsHistory(request()->get('manager') == 'true' ? Auth::user()->sgn_id_remedy : Auth::user()->employee_remedy_id, request()->get('servicetype'), request()->get('manager'), request()->get('checksgp'), request()->get('field'), request()->get('searchdate'), request()->get('tosearchdate'), request()->get('search'));
    }
}
