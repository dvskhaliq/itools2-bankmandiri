<?php

namespace App\Http\Controllers;

use App\PMFeatures;
use App\PMRolesFeatures;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class PMFeaturesController extends BaseController
{
    public function getFeatures()
    {
        try {
            $features = PMFeatures::orderBy('id','asc')->get();
            $data = $features->map(function ($item) {
                $f = $item->toArray();
                $r = ['id'=>null,'feature_id'=>$item->id,'role_id'=>null,'action_read'=> false,'action_create'=> false,'action_update'=> false,'action_delete'=> false];
                return array_merge($f, ['pmrolesfeatures' => $r]);
            });
            return response()->json([
                'status' => true,
                'features' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function getOwnedFeatures($role_id)
    {
        try {
            $rf = PMRolesFeatures::with('pmfeatures')->where("role_id", $role_id)->get();
            $features = $rf->map(function ($item) {
                $f = $item->pmfeatures->toArray();
                $r = $item->toArray();
                unset($r['pmfeatures']);
                return array_merge($f, ['pmrolesfeatures' => $r]);
            });

            $inactive_features = PMFeatures::whereNotIn('id', function ($q) use ($role_id) {
                $q->select('feature_id')->from('pm_roles_features')->where('role_id', $role_id);
            })->get();
            $data = $inactive_features->map(function ($item) {
                $f = $item->toArray();
                $r = ['id'=>null,'feature_id'=>$item->id,'role_id'=>null,'action_read'=> false,'action_create'=> false,'action_update'=> false,'action_delete'=> false];
                return array_merge($f, ['pmrolesfeatures' => $r]);
            });
            $merge_own_avail = array_merge($features->toArray(),$data->toArray());
            return response()->json([
                'status' => true,
                "features" => $merge_own_avail
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }
}
