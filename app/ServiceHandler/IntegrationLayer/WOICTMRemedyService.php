<?php

namespace App\ServiceHandler\IntegrationLayer;

interface WOICTMRemedyService
{
    public function getPeopleEntityByCredential($username,$password);

    public function getPeopleEntityByLoginId($username,$password,$remedy_login_id);

    public function getPeopleEntityByCorporateID($username,$password,$corporate_id);

    public function getPeopleEntityByPersonId($username,$password,$person_id);

    public function getSupportGroupPeopleAssociationByPersonId($username,$password,$person_id,$support_group_id);
    
    public function getSupportGroupPeopleAssociationByLoginId($username,$password,$login_id,$support_group_id);
}
