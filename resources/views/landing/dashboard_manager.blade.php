@extends('sidebar')
@section('content')
<div class="row" id="app-manager">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div>
                <div class="panel-heading" style="background: #fff;">
                    <div class="panel-title">
                        <h4>Dashboard</h4>
                    </div>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="!ismodal" info=></flash>
                    <div class="panel panel-default tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tab-manage-task" role="tab" data-toggle="tab">Manage Task</a></li>
                            <li><a href="#tab-stats" role="tab" data-toggle="tab">Stats</a></li>
                            <li><a href="#tab-history" role="tab" data-toggle="tab">History</a></li>
                            <li v-if="servicetype == 'SGP000000000414'"><a href="#tab-audit-trail" role="tab" data-toggle="tab">SDS Audit Trail</a></li>
                        </ul>
                    </div>

                    <div class="panel-body tab-content">

                        <div class="tab-pane active" id="tab-manage-task">
                            <manage-task ref="requestqueuecnt" :requests="requests" v-on:requestselected="loadSelectedRequest" :auth="auth" :supportgroupid="servicetype" />
                        </div>

                        <div class="tab-pane" id="tab-stats">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Total Request by Agent
                                    </div>
                                    <div class="panel-body">
                                        <div>
                                            <div class="col-md-1 pull-right" style="margin-top:-10px">
                                                <button style="margin-top:20px" type="button" class="btn btn-info btn-md" @click="searchTotalDailyRequestByChannel(selectedstatus,selectedperiod,selecteddate)">Search</button>
                                            </div>
                                            <div class="col-md-2 pull-right" style="margin-top:-10px">
                                            <label>Process Status</label>
                                                <v-select :options="statusoptions" name="TotalType" v-model="selectedstatus" style="height:40px">
                                            </div>
                                            <div class="col-md-2 pull-right" style="margin-top:-10px">
                                            <label>Periode</label>
                                                <v-select :options="periodoptions" name="TotalType" v-model="selectedperiod" style="height:40px">
                                            </div>
                                            <div class="col-md-2 pull-right" style="margin-top:-10px">
                                            <label>Start Date</label>
                                                <input
                                                  v-model="selecteddate"
                                                  type="date"
                                                  autocomplete="off"
                                                  class="form-control"
                                                />
                                            </div>
                                        </div>
                                        <stats-group-grand-total ref="rptTotalChannel" :personalstats="grandtotalstats" :title="'Total task / agent'">
                                        </stats-group-grand-total>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Daily Task By Agent</div>
                                    <div class="panel-body">
                                        <div class="col-md-1 pull-right" style="margin-top:-10px">
                                            <button style="margin-top:20px" type="button" class="btn btn-info btn-md" @click="searchTaskDailyRequest(selected2period, selected2date)">Search</button>
                                        </div>
                                        <div class="col-md-2 pull-right" style="margin-top:-10px">
                                        <label>Periode</label>
                                            <v-select :options="period2options" name="TotalType" v-model="selected2period" style="height:40px">
                                        </div>
                                        <div class="col-md-2 pull-right" style="margin-top:-10px">
                                        <label>Start Date</label>
                                            <input
                                              v-model="selected2date"
                                              type="date"
                                              autocomplete="off"
                                              class="form-control"
                                            />
                                        </div>
                                        <div class="col-md-12">
                                            <stats-group-daily-total :personalstats="dailytotalstats" :title="'Task / Day'">
                                            </stats-group-daily-total>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Hourly Task By Agent</div>
                                    <div class="panel-body">
                                        <div class="col-md-1 pull-right" style="margin-top:-10px">
                                            <button style="margin-top:20px" type="button" class="btn btn-info btn-md" @click="searchTaskDailyRequestByHour(selectedstatus, selected3date)">Search</button>
                                        </div>
                                        <div class="col-md-2 pull-right" style="margin-top:-10px">
                                        <label>Process Status</label>
                                            <v-select :options="statusoptions" name="TotalType" v-model="selectedstatus" style="height:40px">
                                        </div>
                                        <div class="col-md-2 pull-right" style="margin-top:-10px">
                                        <label>Start Date</label>
                                            <input
                                              v-model="selected3date"
                                              type="date"
                                              autocomplete="off"
                                              class="form-control"
                                            />
                                        </div>
                                        <div class="col-md-12">
                                            <stats-group-hourly-total :personalstats="hourlytotalstats" :title="'Task / Hour'">
                                            </stats-group-hourly-total>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-history">
                            <div class="panel panel-default">
                                <a href="#">
                                    <div onclick="ecServiceRequest()" class="panel-heading">Service Request<span style="color:gray" id="icon" class="pull-right glyphicon glyphicon-chevron-up" aria-hidden="true"></span></div>
                                </a>
                                <job-history-wo class="cServiceRequest" v-on:taskselected="loadSelectedRequest" :wohistory="wohistory" :auth="auth" :manager="true"></job-history-wo>
                            </div>
                            <div class="panel panel-default">
                                <a href="#">
                                    <div onclick="ecIncident()" class="panel-heading">Incident<span style="color:gray" id="iconRQ" class="pull-right glyphicon glyphicon-chevron-up" aria-hidden="true"></span></div>
                                </a>
                                <job-history-inc class="cIncident" v-on:taskselected="loadSelectedRequest" :inhistory="inhistory" :auth="auth" :manager="true"></job-history-inc>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-audit-trail">
                        <audit-trail-sds :supportgroup="servicetype"></audit-trail-sds>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- MODAL Closure-->
        <modal-workorder-closure :requestobject="requestobject" :isclosuring="isclosuring" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-workorder-closure>
        <!-- MODAL Livechat-->
        <modal-livechat ref="chatmodal" :manager="true" :isredirecttolivechat="isredirecttolivechat" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" v-on:complete-task="taskCompleteLiveChat" v-on:push-flash-notification="pushPopFlashMessage" v-on:task-delegated="delegateTask">
        </modal-livechat>
        <!-- MODAL View deprecated for agent-->
        <!-- <modal-view :requestdata="requestdata" :auth="auth" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modalclosed="modalClosed" v-on:requestbooked="bookSelectedRequest">            
        </modal-view> -->
        <!-- MODAL Delegate-->
        <modal-delegate :requestobject="requestobject" :isdelegating="isdelegating" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-delegate>
        <!-- MODAL BDS Error Telegram-->
        <modal-bds-error-tlg v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed">
        </modal-bds-error-tlg>
        <!-- MODAL Generic-->
        <modal-workorder-manager v-on:taskclosure="closuringTask" v-on:taskdelegated="delegateTask" :isclosuring="isclosuring" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" :isdelegateenabled="isdelegateenabled">
        </modal-workorder-manager>
        <!-- MODAL Critical Report-->
        <modal-change-assignee :requestobject="requestobject" :isdelegating="isdelegating" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-change-assignee>
        <!-- MODAL Admin Manual-->
        <modal-admin-manual v-on:taskclosure="closuringTask" :isdelegateenabled="isdelegateenabled" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-admin-manual>
        <!-- MODAL Admin Manual Extended-->
        <modal-admin-manual-extended v-on:taskclosure="closuringTask" :isdelegateenabled="isdelegateenabled" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-admin-manual-extended>
        <!-- MODAL Incoming Incident-->
        <modal-incoming-incident :source="source" :isincidentcreation="isincidentcreation" v-on:push-flash-notification="pushPopFlashMessage" :auth="auth" v-on:modal-closed="modalClosed" :servicetype="servicetype" :ismodal="ismodal">
        </modal-incoming-incident>
        <!-- MODAL Create Ticket Live Chat-->
        <modal-create-ticket-live-chat v-on:modal-closed="modalClosed" :requestobject="requestobject" :isincidentlivechat="isincidentlivechat" v-on:push-flash-notification="pushPopFlashMessage" :auth="auth">
        </modal-create-ticket-live-chat>
        <!-- MODAL Incident Generic-->
        <modal-incident-manager v-on:taskclosure="closuringTask" v-on:redirect-livechat="redirectToLivechat" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" :isdelegateenabled="isdelegateenabled">
        </modal-incident-manager>
        <!-- MODAL Incident Closure-->
        <modal-closure-manager :requestobject="requestobject" :isclosuring="isclosuring" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-closure-manager>
        <!-- MODAL Incident Delegate-->
        <modal-delegate-manager :requestobject="requestobject" :isdelegating="isdelegating" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-delegate-manager>
        <!-- MODAL Incident History-->
        <modal-incident-history :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" v-on:modal-closed="modalClosed">
        </modal-incident-history>
        <!-- MODAL WO History-->
        <modal-wo-history :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" v-on:modal-closed="modalClosed">
        </modal-wo-history>
        <!-- MODAL WO Konfirmasi Transaksi-->
        <modal-konfirmasi-transaksi :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-konfirmasi-transaksi>
        <!-- MODAL AKTIVASI REKENING-->
        <modal-aktivasi-rekening :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-aktivasi-rekening>
        <!-- MODAL LAYANAN KOTRAN 0036/0037-->
        <modal-kotran :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-kotran>
        <!-- MODAL LAYANAN TRENDMICRO-->
        <modal-trendmicro :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-trendmicro>
        <!-- MODAL LAYANAN INFORMASI TOKEN-->
        <modal-informasi-token :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-informasi-token>
        <!-- GALERY -->
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a> <a class="next">›</a> <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
    </div>
</div>
@include('footer')
<script type="text/javascript">
    audiofile = "{{asset('assets/sounds/request_alert.mp3')}}";
    // var socket = new SockJS('http://localhost:8080/itools/websocket');
</script>
<script type="text/javascript" src="../../js/dashboard_manager.js"></script>
<script>
    function ecServiceRequest() {
        if ($(".cServiceRequest").css('display') == 'none') {
            $("#icon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cServiceRequest").show("fast");
        } else {
            $("#icon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cServiceRequest").hide("fast");
        }
    }

    function ecIncident() {
        if ($(".cIncident").css('display') == 'none') {
            $("#iconRQ").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cIncident").show("fast");
        } else {
            $("#iconRQ").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cIncident").hide("fast");
        }
    }
</script>
<!-- <script type="text/javascript" src="../../js/sidebar.js"></script> -->
@endsection