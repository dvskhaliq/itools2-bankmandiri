<div class="modal-dialog modal-size-small">
    <div class="modal-content ">
        <div class="modal-header">
            <h4><b>Warning</b></h4>
        </div>
        <div class="modal-body">
            <center>
                <h5>Sorry, contact your administrator!</h5>
            </center>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>