<?php

namespace App\ServiceHandler\NotificationLayer;

use App\Inbox;
use App\Incident;

interface NotificationService
{    
    public function performFeedBackNotifier(Inbox $request, String $ticket_status = null, String $resolution_note = null);

    public function performFeedBackNotifierINC(Inbox $inbox);

    public function performFeedBackNotifierWO(Inbox $inbox, String $note = null);

    public function afterClosureFeedback(Inbox $request, Incident $incident);

    public function chatFromAgent(Inbox $request, String $message);

    public function dismissLiveChat(Inbox $request);
}
