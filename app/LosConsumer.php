<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LosConsumer extends Model
{
    protected $connection = 'sqlsrv_los_consumer';
    public $timestamps = false;
    protected $table = 'appflag';
}
