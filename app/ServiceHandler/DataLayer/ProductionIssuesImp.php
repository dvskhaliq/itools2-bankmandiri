<?php

namespace App\ServiceHandler\DataLayer;

use App\PMProductionIssues as ProductionIssues;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\PMTbluProductionIssues as ProdBill;

class ProductionIssuesImp implements ProductionIssuesService
{
    public function truncateProductionIssues()
    {
        $rslt = ProductionIssues::truncate();
        return $rslt;
    }

   // public function bulkInsert($collection): ?bool
    public function bulkInsert($collection)
    {
	$test = collect($collection);
        $rslt = ProductionIssues::insert($test->toArray());
        return $rslt;
    }

    public function getProductionIssues(): ?Collection
    {
        $rslt = ProductionIssues::orderBy('recorded_at','asc')->get();
        return $rslt;
    }

    public function getTotalByYear()
    {
        $rslt = DB::table('pm_tblu_production_issues_tab')->select(array(DB::raw('EXTRACT(year from recorded_at) as year'), (DB::raw('count(*) as total')) ))->groupBy('year')->orderBy('year','asc')->get();
        return $rslt;                
    }

    public function getProductionIssuesBiller(): ?Collection
    {
        $rslt = ProdBill::orderBy('recorded_at','asc')->get();
        return $rslt;
    }
}
