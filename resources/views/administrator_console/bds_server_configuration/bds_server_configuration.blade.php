@extends('sidebar')
@section('content')
<div id="bds_server_configuration">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS Server Configuration</h4>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info=""></flash>
                    <div class="col-md-12">
                        <component-table :result="result" v-on:search-data="searchData" v-on:edit-data="dataSelected" v-on:delete-data="dataSelected" :alert="alert" :json_fields="json_fields"></component-table>
                        <vue-pagination :pagination="pages" :search="search" @paginate="getData()" :offset="4">
                        </vue-pagination>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalInsert" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog  modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">BDS Server</h4>
                </div>
                <div>
                    <componen-insert v-on:close-modal="closeWindow" v-on:submit-insert="submitInsert" :ismodal="ismodal"> </componen-insert>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog  modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">BDS Server</h4>
                </div>
                <div v-if="isloaded">
                    <componen-edit-seleceted :selected_prop="selected_prop" v-on:close-modal="closeWindow" v-on:submit-update="submitUpdate" :ismodal="ismodal">
                        </component-edit-seleceted>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Confirm Delete</h4>
                </div>
                <div v-if="isloaded">
                    <componen-confirm-delete :selected_prop="selected_prop" v-on:submit-delete="submitDelete"></componen-confirm-delete>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')

<script src="../../js/bds_server_configuration.js"></script>
@endsection