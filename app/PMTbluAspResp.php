<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluAspResp extends Model
{
    protected $table = 'pm_tblu_asp_resp';
    public $timestamps = false;
}
