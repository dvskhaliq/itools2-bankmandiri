<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluSrSoaBiru extends Model
{
    protected $table = 'pm_tblu_sr_soabiru';
    public $timestamps = false;
}
