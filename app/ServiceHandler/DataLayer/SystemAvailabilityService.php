<?php

namespace App\ServiceHandler\DataLayer;
use Illuminate\Database\Eloquent\Collection;

interface SystemAvailabilityService
{
    public function getSystemAvail(): ?Collection;
    public function getSystemAvailByYear(array $years): ?Collection;
    public function getSystemAvailByMonth($month): ?Collection;
    public function getSystemAvailByMonthSystem($id_system, $month): ?Collection;
    public function getSystemAvailByMonthAvg($month): ?Collection;
    public function getSystemAvailByDate($date): ?Collection;
    public function getSystemName();
    public function createSystemAvail($payload);
    public function updateSystemAvail($payload);
    public function updateDescSystemAvail($payload);
    public function deleteSystemAvail(int $id);
    public function bulkInsert($id_system);
    public function getYears();
}
