<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PMFeatures extends Model
{
    protected $table = 'pm_features';

    public function pmrolesfeatures(){
        return $this->hasMany(PMRolesFeatures::class,'feature_id','id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('group', 'asc')->orderBy('name', 'asc')->get();
    }
}
