<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuccessRate extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'SUCCESS_RATE';
}
