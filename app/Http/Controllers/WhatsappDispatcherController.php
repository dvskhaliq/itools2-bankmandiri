<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Auth;
use App\ServiceHandler\DataLayer\WhatsappDispatcherMessagesService;
use App\ServiceHandler\DataLayer\EmployeeRemedyService;
use App\ServiceHandler\DataLayer\UserService;
use App\ServiceHandler\DataLayer\InboxService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\DataLayer\SupportGroupService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\json_decode;
use App\Incident;

class WhatsappDispatcherController extends BaseController
{
  protected $wadispatch_ds;
  protected $emploremedy_ds;
  protected $user_ds;
  protected $inbox_ds;
  protected $suppusr_ds;
  protected $inccreate_wsdl;
  protected $incupdate_wsdl;
  protected $suppgrp_ds;

  public function __construct(
    WhatsappDispatcherMessagesService $wadispatch_service,
    EmployeeRemedyService $emploremedy_service,
    UserService $user_service,
    InboxService $inbox_service,
    WOIIncidentService $incident_create_service,
    SupportUserService $support_user_service,
    WOIIncidentUpdateService $incident_update_service,
    SupportGroupService $support_group_service
  ) {
    $this->wadispatch_ds = $wadispatch_service;
    $this->emploremedy_ds = $emploremedy_service;
    $this->user_ds = $user_service;
    $this->inbox_ds = $inbox_service;
    $this->suppusr_ds = $support_user_service;
    $this->suppgrp_ds = $support_group_service;
    $this->inccreate_wsdl = $incident_create_service;
    $this->incupdate_wsdl = $incident_update_service;
  }

  public function addDispatcherFeedback()
  {
    $array_wa_dsp = array(
      "request_id" => request()->get('requestid'),
      "sender_id" => Auth::user()->id,
      "content" => request()->get('feedback')
    );
    return $this->wadispatch_ds->addDispatcherMessage($array_wa_dsp);
  }

  public function uploadDispatcherImage(Request $request)
  {
    try {
      $file = $request->file('image');
      $param = json_decode(request()->get('image'));
      $array_wa_dsp = array(
        "request_id" => $param->requestid,
        "sender_id" => Auth::user()->id,
        "content" => "@!image"
      );
      $wa_dsp_msg = $this->wadispatch_ds->addDispatcherMessage($array_wa_dsp);
      Storage::disk('public')->putFileAs($wa_dsp_msg->request_id, $file, $wa_dsp_msg->request_id . "_" . $wa_dsp_msg->id . ".jpeg");
      return response()->json(["status" => true, 'message' => 'successfully upload the file', 'responsedata' => $wa_dsp_msg]);
    } catch (\Throwable $th) {
      return response()->json(["status" => true, 'message' => 'failed upload the file' . $th->__toString()]);
    }
  }

  public function deleteDispatcherImage(Request $request)
  {
    $arr_str_split = explode("_", $request->getContent());
    Storage::disk("public")->delete($arr_str_split[0] . "/" . $request->getContent() . ".jpeg");
    $this->wadispatch_ds->deleteDispatcherMessage($arr_str_split[1]);
    // return response()->json(["status" => true, 'message' => 'successfully remove the file', 'content' => $request->getContent()]);
    return response($request->getContent(), 200)
      ->header('Content-Type', 'text/plain');
  }

  // ------------------------------------------------------------------ PERUBAHAN
  public function deleteDispatcherFeedback(Request $request)
  {
    try {
      $messageId = $request->get('payload');
      $public = "public";
      $local = "local";
      Storage::disk('public')->delete($messageId["requestId"] . "/" . $messageId["requestId"] . "_" . $messageId["id"] . ".jpeg");
      $this->wadispatch_ds->deleteDispatcherMessage($messageId["id"]);
      return response()->json([
        'status' => true,
        'data' => $messageId,
        'message' => $messageId["content"] == "@!image" ? $messageId["requestId"] . "_" . $messageId["id"] . ".jpeg" . " Has been deleted." : $messageId["id"] . " Has been deleted."
      ], 200)->header('Content-Type', 'text/plain');
    } catch (Exception $ex) {
      return response()->json([
        'status' => false,
        'message' => $ex->__toString()
      ], 500);
    }
  }

  // ------------------------------------------------------------------ PERUBAHAN
  public function updateStagingIncident()
  {
    $request = $this->inbox_ds->find(request()->get("requestId"));
    $customer = $this->emploremedy_ds->getEmployeeRemedy(request()->get('customer')["id"]);
    $assignee =  request()->get('assignee') == '' || request()->get('assignee') == null ? null : $this->user_ds->findByUserId(request()->get('assignee')["id"]);
    $recipient_id = (request()->get('reportedSource') == 'Whatsapp' ? "WA" : null);
    $array_inbox = array(
      "Text" => request()->get('summary'),
      "SenderNumber" => $customer->full_name,
      "SMSCNumber" => request()->get("phoneNumber"),
      "UDH" => $customer->full_name . "#" . $customer->first_name . "#" . "NULL#" . $customer->company . "#" . $customer->region_name . "#" . $customer->group_name . "#" . $customer->department_name,
      "RecipeintID" => $recipient_id,
      "Status" => 112,
      "AssigneeId" => Auth::user()->id,
      "ProcessStatus"  => request()->get("processStatus"),
      "ActionTicketCreation" => request()->get("actionTicketCreation"),
      "EmployeeSenderId" => $customer->id,
      "EmployeeAssigneeId" => ($assignee != null ? $assignee->employee_remedy_id : null),
      "EmployeeAssigneeGroupId" => ($assignee != null ? $assignee->sgn_id_remedy : null),
      "TextDecoded" => "INCIDENT#" . request()->get('company') . "#" . "User Service Request#" . request()->get("notes") . "#" . request()->get('summary') . "#" . ($assignee != null ? $assignee->supportgroupassigneeremedy->support_group_name : "") . "#" . ($assignee != null ? $assignee->supportgroupassigneeremedy->full_name : "") . "#" . request()->get("impact") . "#" . request()->get("urgency") . "##" . "Whatsapp#" . request()->get('company') . "###########",
    );
    $this->inbox_ds->update(request()->get("requestId"), $array_inbox);
    $inbox = $this->inbox_ds->refresh($request);
    if ($assignee != null) {
      return response()->json(["status" => true, "message" => "Your ticket has been assigned to: " . $inbox->supportgroupassigneeremedy->full_name . " with request id: " . $inbox->id, "responsedata" => $inbox]);
    } else {
      return response()->json(["status" => true, "message" => "Your ticket has been added to request queue", "responsedata" => $inbox]);
    }
  }

  protected function updateTextDecoded($text, $content)
  {
    $explode = explode("#", $text);
    $explode[12] = $content['prod_cat_tier1'];
    $explode[13] = $content['prod_cat_tier2'];
    $explode[14] = $content['prod_cat_tier3'];
    $explode[17] = $content['opr_cat_tier1'];
    $explode[18] = $content['opr_cat_tier2'];
    $explode[19] = $content['opr_cat_tier3'];
    $explode[20] = $content['resolution'];
    $explode[7] = $content['impact'];
    $explode[8] = $content['urgency'];
    $explode[21] = $content['service'];
    $explode[22] = $content['survey'];
    $text = implode("#", $explode);
    return $text;
  }

  public function createStagingIncidentToRemedy()
  {
    $incident = new Incident();
    $request = $this->inbox_ds->find(request()->get("id"));
    $asgn = $this->suppusr_ds->findByPersonIdGroupId($request->EmployeeAssigneeId, $request->EmployeeAssigneeGroupId);
    $content = request()->get("content");
    $text = $this->updateTextDecoded($request->TextDecoded, $content);

    $this->inbox_ds->update($request->id, array(
      "TextDecoded" => $text
    ));
    $request = $this->inbox_ds->refresh($request);
    $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($content['service']);
    $wsdl_rslt = $this->inccreate_wsdl->createIncidentBook($request, $asgn, $wsdl_reconID->getContent());
    try {
      if ($wsdl_rslt->getResponseStatus() == 'success') {
        // $this->inbox_ds->update($request->id, $array_inbox = array(
        //     "TicketCreationDateTime" => date('Y-m-d H:i:s')
        // ));
        $inbox = $this->inbox_ds->refresh($request);
        $inbox['incident'] = $incident->parseInbox($inbox);
        $inbox['messages'] = $this->wadispatch_ds->getDispatcherMessageList(request()->get("id"));
        return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Incident Number : " . $wsdl_rslt->getContent() . " has been created", 'wsdlStatus' => true, 'wsdl' => $wsdl_rslt->getResponseMessage()]);
      } else {
        return response()->json(['status' => false, 'message' => "Fatal Error 1: Unable to connect to Remedy, your request not being processed!", 'wsdl' => $wsdl_rslt->getResponseMessage()]);
      }
    } catch (Exception $e) {
      return response()->json(['status' => false, 'message' => "Fatal Error 2: Unable to connect to Remedy, your request not being processed!", 'wsdl' => $wsdl_rslt->getResponseMessage()]);
    }
  }

  public function updateTier()
  {
    $incident = new Incident();
    $request = $this->inbox_ds->find(request()->get("id"));
    $content = request()->get("content");

    // $array_inbox = array(
    //     "TextDecoded" =>  $request->TextDecoded . "#",
    // );
    // $this->inbox_ds->update(request()->get("id"), $array_inbox);
    // $request = $this->inbox_ds->refresh($request);

    $text = $this->updateTextDecoded($request->TextDecoded, $content);
    $this->inbox_ds->update($request->id, array(
      "TextDecoded" => $text
    ));
    $incident->parseInbox($this->inbox_ds->refresh($request));

    $explode = explode("#", $request->TextDecoded);
    $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($incident->service);
    // $reconID = $wsdl_reconID->getContent();
    // return response()->json(["service" => $explode[21], "getContent" => $wsdl_reconID->getContent(), "arrContent" => $wsdl_reconID->getArrContent()]);
    try {
      $wsdl_rslt = $this->incupdate_wsdl->updateIncidentGeneric($incident, $wsdl_reconID->getContent());
      if ($wsdl_rslt->getResponseStatus() == 'success') {
        $inbox = $this->inbox_ds->refresh($request);
        $inbox['incident'] = $incident->parseInbox($inbox);
        $inbox['messages'] = $this->wadispatch_ds->getDispatcherMessageList(request()->get("id"));
        return response()->json(["status" => true, "responsedata" => $inbox, "message" => "Data has been updated " . $inbox->RemedyTicketId, 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => true, 'service' => $explode[21], 'reconID' => $wsdl_reconID->getContent()]);
      } else {
        // if(strpos($wsdl_rslt->getResponseMessage(),"WSDL: ERROR (309)")==true){
        //     $status_loop = true;
        //     while($status_loop) {
        //         $wsdl_rslt_loop = $this->incupdate_wsdl->updateIncidentGeneric($incident, $wsdl_reconID->getContent());
        //         if($wsdl_rslt_loop->getResponseStatus() == 'success'){
        //             $status_loop = false;
        //             $inbox_loop = $this->inbox_ds->refresh($request);
        //             $inbox_loop['incident'] = $incident->parseInbox($inbox_loop);
        //             $inbox_loop['messages'] = $this->wadispatch_ds->getDispatcherMessageList(request()->get("id"));
        //             return response()->json(["status" => true, "responsedata" => $inbox_loop, "message" => "Data has been updated " . $inbox_loop->RemedyTicketId, 'wsdl' => $wsdl_rslt_loop->getResponseMessage(), 'wsdlStatus' => true, 'service' => $explode[21], 'reconID' => $wsdl_reconID->getContent()]);
        //         }else if(strpos($wsdl_rslt->getResponseMessage(),"WSDL: ERROR (309)")){
        //             $status_loop = true;
        //         }else{
        //             $status_loop = false;
        //             return response()->json(["status" => false, "responsedata" => "Failed", "message" => "Failed update tier.", 'wsdl' => $wsdl_rslt_loop->getResponseMessage(), 'wsdlStatus' => false]);
        //         }
        //     }
        // }else{
        //     return response()->json(["status" => false, "responsedata" => "Failed", "message" => "Failed update tier.", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
        // }
        return response()->json(["status" => false, "responsedata" => "Failed", "message" => "Failed update tier.", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
      }
    } catch (Exception $e) {
      return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed!"]);
    }
  }

  public function delegateOtherGroup()
  {
    try {
      $incident = new Incident();
      $request = $this->inbox_ds->find(request()->get("id"));
      $old_data = array(
        "AssigneeId" => $request->AssigneeId,
        "EmployeeAssigneeId" => $request->EmployeeAssigneeId,
        "EmployeeAssigneeGroupId" => $request->EmployeeAssigneeGroupId,
        "ProcessStatus" => $request->ProcessStatus,
      );
      $content = request()->get('assignee');
      $assignee = $this->suppusr_ds->findByCompanyOrgGroupName($content['assignee_support_company'], $content['assignee_support_organization'], $content['assignee_support_group_name'], $content['assignee_name']);
      if ($assignee) {
        $array_inbox = array(
          "AssigneeId" => Auth::user()->id,
          "EmployeeAssigneeId" => $assignee->employee_remedy_id,
          "EmployeeAssigneeGroupId" => $assignee->support_group_id,
          "ProcessStatus" => 'QUEUED',
        );
        $this->inbox_ds->update($request->id, $array_inbox);
        $incident->parseInbox($this->inbox_ds->refresh($request));
        $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident);
      } else {
        $array_inbox = array(
          "AssigneeId" => Auth::user()->id,
          "EmployeeAssigneeId" => null,
          "EmployeeAssigneeGroupId" => null,
          "ProcessStatus" => 'QUEUED',
        );
        $this->inbox_ds->update($request->id, $array_inbox);
        $incident->parseInbox($this->inbox_ds->refresh($request));
        $group = $this->suppgrp_ds->findByComOrgGrp($content['assignee_support_company'], $content['assignee_support_organization'], $content['assignee_support_group_name']);
        $incident->assignee_support_company = $group->company;
        $incident->assignee_support_organization = $group->organization;
        $incident->assignee_support_group_name = $group->support_group_name;
        $incident->assignee_support_group_id = $group->id;
        $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident);
      }
      if ($wsdl->getResponseStatus() == 'success') {
        $message = "Ticket with Ref No : " . $request->id . " - " . $wsdl->getContent() . " has been Assigned to group " . ($assignee ? $assignee->support_group_name . " -> " . $assignee->full_name : $group->support_group_name);
      } else {
        $this->inbox_ds->update($request->id, array("ActionTicketCreation" => 'IN_PENDING_DLG'));
        $message = "Failed Delegate ticket to other user with Ref No : " . $request->id . " - " . $request->RemedyTicketId . ", please retry process from Pending Ticket menu";
      }
      return response()->json(["status" => $wsdl->getResponseStatus() == 'success' ? true : false, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'incident' => $wsdl->getArrContent(), 'request' => $request, 'assignee' => $assignee]);
    } catch (Exception $e) {
      report($e);
      return response()->json(["status" => false, "message" => "Unable to connect to Remedy, your request not being processed!"]);
    }
  }

  public function initIncidentWhatsappDispatcher($request_id)
  {
    $request = $this->inbox_ds->find($request_id);
    $assignee = $this->suppusr_ds->findByPersonIdGroupId(Auth::user()->employee_remedy_id, Auth::user()->sgn_id_remedy);
    $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($request->service);
    $wsdl = $this->inccreate_wsdl->createIncidentBook($request, $assignee, $wsdl_reconID->getContent());
    $message = $wsdl->getResponseStatus() == 'success' ? "Ref ID : " . $request->id . " - " . $wsdl->getContent() . " created and assigned to your task" : "Ref ID : " . $request->id . " assigned to your task, but failed to create remedy ticket, please retry process from Pending Ticket Remedy!";

    $incident = new Incident();
    $array_inbox = array(
      "EmployeeAssigneeId" => Auth::user()->employee_remedy_id,
      "EmployeeAssigneeGroupId" => Auth::user()->sgn_id_remedy,
      "ProcessStatus" => "INPROGRESS"
    );
    $this->inbox_ds->update($request->id, $array_inbox);
    $data = $this->inbox_ds->refresh($request);
    $data['incident'] = $incident->parseInbox($data);
    $data['messages'] = $this->wadispatch_ds->getDispatcherMessageList($request->id);

    return response()->json(["status" => true, "message" => $message, "responsedata" => $data, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false]);
  }

  public function getIncidentWhatsappDispatcher($request_id)
  {
    $incident = new Incident();
    $request = $this->inbox_ds->find($request_id);
    $request['incident'] = $incident->parseInbox($request);
    $request['messages'] = $this->wadispatch_ds->getDispatcherMessageList($request_id);

    return response()->json(["status" => true, "message" => "Related incident from WA is in progress", "responsedata" => $request, "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId]);
  }

  public function markForDispatcherTicket($mark)
  {
    $origin = $this->suppusr_ds->findByPersonIdGroupId(request()->get('targetEmployeeId'), request()->get('targetEmployeeGroupId'));
    $target = $this->user_ds->findByUserId(request()->get('originUserId'));
    $array_inbox = array(
      "EmployeeAssigneeId" => $target->employee_remedy_id,
      "EmployeeAssigneeGroupId" => $target->sgn_id_remedy,
      "AssigneeId" => $origin->user->id,
      "ActionTicketCreation" => $mark
    );
    $this->inbox_ds->update(request()->get("requestId"), $array_inbox);
    $inbox = $this->inbox_ds->find(request()->get("requestId"));
    switch ($mark) {
      case 'IN_REVIEW':
        return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Your ticket with id: " . $inbox->id . " on review by :" . $target->name]);
        break;
      case 'IN_MARK_CLS':
        return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Your ticket with id: " . $inbox->id . " marked for closure by :" . $target->name]);
        break;
      case 'IN_PROCESS':
        return response()->json(['status' => true, 'responsedata' => $inbox, 'message' => "Your ticket with id: " . $inbox->id . " marked for feedback by :" . $target->name]);
        break;
    }
  }

  public function closeDispatcherTicket()
  {
    try {
      $incident = new Incident();
      $request = $this->inbox_ds->find(request()->get('requestId'));
      $old_data = array(
        "TextDecoded" => $request->TextDecoded,
        "ProcessStatus" => $request->ProcessStatus,
        "ActionTicketCreation" => $request->ActionTicketCreation,
      );
      $resolution = $this->wadispatch_ds->getDispathcerMessageHistory(request()->get('requestId'));

      $explode = explode("#", $request->TextDecoded);
      $explode[20] = $resolution->content_summary;
      $request->TextDecoded = implode("#", $explode);

      $this->inbox_ds->update($request->id, $array_inbox = array(
        "TextDecoded" => $request->TextDecoded,
        "ProcessStatus" => "CLOSED",
        "ActionTicketCreation" => "IN_COMPLETED",
        "FinishDateTime" => date('Y-m-d H:i:s'),
      ));
      $request = $this->inbox_ds->refresh($request);
      $incident->parseInbox($this->inbox_ds->refresh($request));
      $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident);
      if ($wsdl->getResponseStatus() == 'success') {
        $message = "Ticket with Ref No : " . $request->id . " - " . $wsdl->getContent() . " has been Resolved";
      } else {
        $this->inbox_ds->update($request->id, array("AssigneeId" => Auth::user()->id, "ActionTicketCreation" => 'IN_PENDING_CLS'));
        $message = "Failed to Resolved ticket with Ref No : " . $request->id . " - " . $request->RemedyTicketId . ", please retry process from Pending Ticket menu";
      }
      return response()->json(["status" => $wsdl->getResponseStatus() == 'success' ? true : false, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'incident' => $wsdl->getArrContent(), 'responsedata' => $request]);
    } catch (Exception $e) {
      report($e);
      return response()->json(["status" => false, "message" => "Unable to connect to Remedy, your request not being processed!"]);
    }
  }
}
