<?php

namespace App\ServiceHandler\DataLayer;

use Illuminate\Support\Facades\Session;
use App\SDSAuditril;
use DB;
use Auth;
use App\Helpers\DatabaseConnection;
use App\AppConfig;

class CPSServerImp implements CPSServerService
{
    public function queryTT1($osn)
    {
        $table1 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_MT1XX')->select('TrackID', 'Osn', 'T20SndrRef', 'ProcStatus', 'MatchStatus')->whereIn('Osn', $osn)->orderBy('TrackID', 'ASC')->get();
        $array_length = count($table1);
        for($i=0;$i<$array_length;$i++){
            $data[$i] = $table1[$i]['TrackID'];
        }
        $table2 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('msg_MsgTracks')->select('TrackID', 'PostStatus', 'TRN', 'Osn', 'ValueDate')->whereIn('TrackID', $data)->orderBy('TrackID', 'ASC')->get();
        return response()->json([
            'table1' => $table1,
            'table2' => $table2,
            'table3' => null,
            'table4' => null,
            'status' => true
        ]);
    }

    public function updateDataTT1($trackid, $osn, $old_data, $remedy_ticket, $summary_ticket)
    {
        $dataUpdate1 = array('ProcStatus' => 'RTN', 'MatchStatus' => 'RTN');
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_MT1XX')->where('TrackID', $trackid)->update($dataUpdate1);
        $dataUpdate2 = array('PostStatus' => 'RTNS');
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('msg_MsgTracks')->where('TrackID', $trackid)->update($dataUpdate2);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'pym_MT1XX',
            'dataold' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $old_data['ProcStatus'] . '; MatchStatus = ' . $old_data['MatchStatus'] .';',
            'datanew' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $dataUpdate1['ProcStatus'] . '; MatchStatus = ' . $dataUpdate1['MatchStatus'] .';',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'msg_MsgTracks',
            'dataold' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $old_data['PostStatus'] . ';',
            'datanew' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $dataUpdate2['PostStatus'] . ';',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryTT1($osn);
    }

    public function queryTT2($data)
    {
        $table1 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP2')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'DtCreated', 'RIERR# AS rierr')->whereIn('RITRRF', $data)->orderBy('RISEQN', 'DESC')->get();
        $table2 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP1')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'RIBENA', 'RIPMOD', 'RIIAGT')->whereIn('RITRRF', $data)->orderBy('RISEQN', 'DESC')->get();
        $table3 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('msg_MsgTracks')->select('TrackID', 'PostStatus', 'PostErr')->whereIn('TrackID', $data)->orderBy('TrackID', 'ASC')->get();
        return response()->json([
            'table1' => $table1,
            'table2' => $table2,
            'table3' => $table3,
            'table4' => null,
            'status' => true
        ]);
    }

    public function updateDataTT2($trackid, $ritrrf, $old_data, $remedy_ticket, $summary_ticket)
    {
        $dataUpdate1 = array('PostStatus' => 'PROC', 'PostErr' => null);
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('msg_MsgTracks')->where('TrackID', $trackid)->update($dataUpdate1);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'msg_MsgTracks',
            'dataold' => 'TrackID = ' . $trackid .'; PostStatus = ' . $old_data['PostStatus'] . '; PostErr = ' . $old_data['PostErr'] .';',
            'datanew' => 'TrackID = ' . $trackid .'; PostStatus = ' . $dataUpdate1['PostStatus'] . '; PostErr = null;',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryTT2($ritrrf);
    }

    public function queryTT3($data)
    {
        $table1 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP2')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'DtCreated', 'RIERR# AS rierr')->whereIn('RITRRF', $data)->orderBy('InterStatus', 'DESC')->get();
        $table2 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP1')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'RIBENA', 'RIPMOD', 'RIIAGT')->whereIn('RITRRF', $data)->orderBy('RISEQN', 'DESC')->get();
        $table3 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('msg_MsgTracks')->select('TrackID', 'PostStatus', 'PostErr')->whereIn('TrackID', $data)->orderBy('TrackID', 'ASC')->get();
        $table4 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_MT1XX')->select('TrackID', 'PymMode', 'PymBranch', 'ProcStatus')->whereIn('TrackID', $data)->orderBy('TrackID', 'ASC')->get();
        return response()->json([
            'table1' => $table1,
            'table2' => $table2,
            'table3' => $table3,
            'table4' => $table4,
            'status' => true
        ]);
    }

    public function updateDataTT3($trackid, $ritrrf, $old_data, $remedy_ticket, $summary_ticket)
    {
        $dataUpdate1 = array('PymMode' => null, 'PymBranch' => null, 'ProcStatus' => 'RDY');
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_MT1XX')->where('TrackID', $trackid)->update($dataUpdate1);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'pym_MT1XX',
            'dataold' => 'TrackID = ' . $trackid .'; PymMode = ' . $old_data['PymMode'] . '; PymBranch = ' . $old_data['PymBranch'] .'; ProcStatus = ' . $old_data['ProcStatus'] .';',
            'datanew' => 'TrackID = ' . $trackid .'; PymMode = null; PymBranch = null; ProcStatus = ' . $dataUpdate1['ProcStatus'] .';',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryTT3($ritrrf);
    }

    public function queryTT4($data)
    {
        $table1 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_MT101')->select('TrackID', 'MsgTrackID', 'T21TrxRef', 'MatchStatus', 'ProcStatus', 'T32ValueDate', 'T32SettleCurrency', 'T32SettleAmount')->whereIn('TrackID', $data)->orderBy('TrackID', 'ASC')->get();
        return response()->json([
            'table1' => $table1,
            'table2' => null,
            'table3' => null,
            'table4' => null,
            'status' => true
        ]);
    }

    public function updateDataTT4($trackid, $data, $old_data, $remedy_ticket, $summary_ticket)
    {
        $dataUpdate1 = array('ProcStatus' => 'VOID', 'MatchStatus' => 'VOID');
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_MT101')->where('TrackID', $trackid)->update($dataUpdate1);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'pym_MT101',
            'dataold' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $old_data['ProcStatus'] . '; MatchStatus = ' . ($old_data['MatchStatus'] == null?'null':$old_data['MatchStatus']) .';',
            'datanew' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $dataUpdate1['ProcStatus'] . '; MatchStatus = ' . $dataUpdate1['MatchStatus'] .';',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryTT4($data);
    }

    public function queryRTGS1($data)
    {
        $table1 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_IftsInSingleTrx')->select('TrackID', 'TRN', 'RelatedTRN', 'ProcStatus', 'ProcFlag', 'DtCreated', 'DtLastMtn')->whereIn('RelatedTRN', $data)->orderBy('TrackID', 'DESC')->get()->take(5);
        return response()->json([
            'table1' => $table1,
            'table2' => null,
            'table3' => null,
            'table4' => null,
            'status' => true
        ]);
    }

    public function updateDataRTGS1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket)
    {
        $dataUpdate1 = array('ProcStatus' => 'INVS', 'ProcFlag' => 15);
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_IftsInSingleTrx')->where('TrackID', $trackid)->update($dataUpdate1);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'pym_IftsInSingleTrx',
            'dataold' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $old_data['ProcStatus'] . '; ProcFlag = ' . $old_data['ProcFlag'] .';',
            'datanew' => 'TrackID = ' . $trackid .'; ProcStatus = ' . $dataUpdate1['ProcStatus'] . '; ProcFlag = ' . $dataUpdate1['ProcFlag'] .';',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryRTGS1($data);
    }

    public function queryRTGS2($data)
    {
        $table1 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('pym_IftsInSingleTrx')->select('TrackID', 'RelatedTRN', 'ProcStatus', 'PymMode', 'PymBranch', 'ProcStatus', 'TRN', 'ValueDate', 'AmountVal', 'DtCreated', 'DtLastMtn')->whereIn('RelatedTRN', $data)->orderBy('TrackID', 'DESC')->get()->take(5);
        $array_length = count($table1);
        for($i=0;$i<$array_length;$i++){
            $data[$i] = $table1[$i]['TrackID'];
        }
        $table2 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP2')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'DtCreated')->whereIn('RITRRF', $data)->orderBy('RITRRF', 'DESC')->get();
        $table3 = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP1')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'RIBENA', 'RIPMOD', 'RISAMT')->whereIn('RITRRF', $data)->orderBy('RITRRF', 'DESC')->get();
        return response()->json([
            'table1' => $table1,
            'table2' => $table2,
            'table3' => $table3,
            'table4' => null,
            'status' => true
        ]);
    }

    public function updateDataRTGS2($trackid, $data, $old_data, $remedy_ticket, $summary_ticket)
    {
        // $query = $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP1')->select('RISEQN')->where('RITRRF', $trackid)->get();
        $dataUpdate1 = array('RIPMOD' => 'ACCT', 'RISEQN' => $old_data['RISEQN']+1, 'InterStatus' => 'RDY');
        $this->dbConnection('cps_server', 'cps_pass', 'cps_user')->table('INT_RMINP1')->where('RITRRF', $trackid)->update($dataUpdate1);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'INT_RMINP1',
            'dataold' => 'TrackID = ' . $trackid .'; RIPMOD = ' . $old_data['RIPMOD'] . '; RISEQN = ' . $old_data['RISEQN'] .'; InterStatus = ' . $old_data['InterStatus'] .';',
            'datanew' => 'TrackID = ' . $trackid .'; RIPMOD = ' . $dataUpdate1['RIPMOD'] . '; RISEQN = ' . $dataUpdate1['RISEQN'] .'; InterStatus = ' . $dataUpdate1['InterStatus'] .';',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryRTGS2($data);
    }

    public function queryMCS1($data)
    {
        $table1 = $this->dbConnection('mcs_server', 'mcs_pass', 'mcs_user')->table('INT_RMINP2')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'DtCreated', 'RIERR# AS RIERR')->whereIn('RITRRF', $data)->orderBy('DtCreated', 'ASC')->get();
        $table2 = $this->dbConnection('mcs_server', 'mcs_pass', 'mcs_user')->table('INT_RMINP1')->select('RITRRF', 'InterStatus', 'InterErrMsg', 'RISEQN', 'RIBENA', 'RIPMOD', 'RIIAGT')->whereIn('RITRRF', $data)->orderBy('DtCreated', 'ASC')->get();
        $table3 = $this->dbConnection('mcs_server', 'mcs_pass', 'mcs_user')->table('msg_MsgTracks')->select('TrackID', 'PostStatus', 'PostErr')->whereIn('TrackID', $data)->orderBy('TrackID', 'DESC')->get();
        return response()->json([
            'table1' => $table1,
            'table2' => $table2,
            'table3' => $table3,
            'table4' => null,
            'status' => true
        ]);
    }

    public function updateDataMCS1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket)
    {
        $dataUpdate1 = array('PostStatus' => 'PROC', 'PostErr' => null);
        $this->dbConnection('mcs_server', 'mcs_pass', 'mcs_user')->table('msg_MsgTracks')->where('TrackID', $trackid)->update($dataUpdate1);
        $audit = array(
            'remedyticketid' => $remedy_ticket,
            'tablename' => 'msg_MsgTracks',
            'dataold' => 'TrackID = ' . $trackid .'; PostStatus = ' . $old_data['PostStatus'] . '; PostErr = '. $old_data['PostErr'] .';',
            'datanew' => 'TrackID = ' . $trackid .'; PostStatus = ' . $dataUpdate1['PostStatus'] . '; PostErr = null;',
            'summary' => $summary_ticket
        );
        $this->auditrail($audit);
        return $this->queryMCS1($data);
    }

    public function queryMandol1($data, $field)
    {
        switch ($field) {
            case 'CIF': {
                    $field_table = 'HOST_CIF_ID';
                    break;
                }
            case 'USER ID': {
                    $field_table = 'USER_ID';
                    break;
                }
            case 'NO HP': {
                    $field_table = 'MOBILE_PH_NO_1';
                    break;
                }
        }
        $table1 = $this->dbConnection('mandol_server', 'mandol_pass', 'mandol_user')->table('pcr_cust')->select('*')->whereIn($field_table, $data)->get();
        $array_length = count($table1);
        for($i=0;$i<$array_length;$i++){
            $data[$i] = $table1[$i]->id;
        }
        $table2 = $this->dbConnection('mandol_server', 'mandol_pass', 'mandol_user')->table('PCR_CUST_CC')->select('*')->whereIn('CUST_ID', $data)->get();
        return response()->json([
            'table1' => $table1,
            'table2' => $table2,
            'table3' => null,
            'table4' => null,
            'status' => true
        ]);
    }

    /* ========= tidak dipake dulu kode nya ================ */
    // public function connectToCPS($username, $password, $ipserver){
    //     config(['filesystems.disks.cps' => [
    //         'driver' => 'odbc',
    //         'dsn' => 'Driver={SQL Server};Server='.$ipserver.';Database=DBCPS',
    //         'database' => 'DBCPS',
    //         'host' => $ipserver,
    //         'username' => $username,
    //         'password' => $password,
    //         'charset' => 'utf8',
    //         'prefix' => '',
    //         'prefix_indexes' => true,
    //     ]]);
    // }

    // public function connectToMCS($username, $password, $ipserver){
    //     config(['filesystems.disks.mcs' => [
    //         'driver' => 'odbc',
    //         'dsn' => 'Driver={SQL Server};Server='.$ipserver.';Database=DBMCS',
    //         'database' => 'DBMCS',
    //         'host' => $ipserver,
    //         'username' => $username,
    //         'password' => $password,
    //         'charset' => 'utf8',
    //         'prefix' => '',
    //         'prefix_indexes' => true,
    //     ]]);
    // }
    /* ========================================= */

    protected function auditrail($data){
        $audit = new SDSAuditril();
        $audit->Username = Auth::user()->name;
        $audit->RemedyTicketId = $data['remedyticketid'];
        $audit->Table_Name = $data['tablename'];
        $audit->Old_Data = $data['dataold'];
        $audit->New_Data = $data['datanew'];
        $audit->Update_date_time = date('Y-m-d H:i:s');
        $audit->Login_user = Auth::user()->un_remedy;
        $audit->Summary = $data['summary'];
        $audit->save();
    }

    public function auditTrailView($field, $search){
        return SDSAuditril::where($field, 'ILIKE', '%' . $search . '%')->orderBy('Update_date_time', 'ASC')->get();
    }

    private function dbConnection($server, $pass, $user){
        $ipserver = AppConfig::where('id', $server)->pluck('value')->first();
        $username = AppConfig::where('id', $user)->pluck('value')->first();
        $password = AppConfig::where('id', $pass)->pluck('value')->first();
        if($server == 'cps_server'){
            return DatabaseConnection::ConnectionCPS($ipserver, $password, $username);
        }
        if($server == 'mcs_server'){
            return DatabaseConnection::ConnectionMCS($ipserver, $password, $username);
        }
        if($server == 'mandol_server'){
            return DatabaseConnection::ConnectionMandol($ipserver, $password, $username);
        }
    }
}
