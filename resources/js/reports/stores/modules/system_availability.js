import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            header: {},
            system_downtime: "",
            percent_system_avail:"",
            description:null,
            recorded_at:null,
        }
    };
};

const state = {
    years: [],
    systemavails: [],
    systemavailsavg: [],
    systemname: [],
    simpleHeader: [
        { text: "System Name", value: "header.system_name" },
        { text: "System Downtime", value: "system_downtime", align: 'center' },
        { text: "% System Avail", value: "percent_system_avail", align: 'center' },
        { text: "Notes", value: "description"},
        { text: "Recorded at", value:"recorded_at"},
        { text: "Action", value: "action", sortable: false }
    ],
    simpleHeaderAvg: [
        { text: "Recorded at", value: "recorded_at" },
        { text: "System Name", value: "header.system_name" },
        { text: "Summary System Downtime", value: "system_downtime" },
        { text: "% Average System Avail", value: "percent_system_avail" },
    ],
    editedIndex: defaultItem().index,
    editedItem: defaultItem().item,
    apiState: ENUM.INIT,
    alertMessage: "",
};

const getters = {
    getSystemAvailsAvg: state => state.systemavailsavg,
    getSystemAvails: state => state.systemavails,
    getSystemName: state => state.systemname,
    getSimpleHeader: state => state.simpleHeader,
    getSimpleHeaderAvg: state => state.simpleHeaderAvg,
    getEditItem: state => state.editedItem,
    getEditIndex: state => state.editedIndex,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getYears: state => state.years,
};

const actions = {
    async initYears({ commit }) {
        let response;
        try {
          response = await axios.get("/data_management/problems/system_availability/fetchYears");
          commit("setYears", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
    async initSystemAvailability({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/data_management/problems/system_availability/fetch");
          commit("setSystemAvails", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateSystemAvailByDate({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/system_availability/getByDate", payload
              );
            commit("setSystemAvails", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateSystemAvailByMonth({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/system_availability/getByMonth", payload
              );
            commit("setSystemAvails", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateSystemAvailByMonthAvg({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/system_availability/getByMonthAvg", payload
              );
            commit("setSystemAvailsAvg", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateSystemAvailByYear({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
                response = await axios.post(
                "/data_management/problems/system_availability/getByYear", payload
              );
            commit("setSystemAvails", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },

      async fetchSystemName({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/system_availability/fetchSystemName");
            commit("setSystemName", response.data.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Something went wrong on fetch!"
                  );
        }
    },

      async addSystemAvail({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/system_availability/create", payload);
            commit("newSystemAvail", response.data.data);
            commit("setSuccess", "Data has been successfully saved !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Adding data has failed"
                  );
        }
    },
    async updateSystemAvail({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.put("/data_management/problems/system_availability/update", payload);
            commit("updateSystemAvail", response.data.data);
            commit("setSuccess", "Data has been successfully updated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Updating data has failed"
                  );
        }
    },
    async updateDescSystemAvail({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.put("/data_management/problems/system_availability/updatedesc", payload);
            commit("updateSystemAvail", response.data.data);
            commit("setSuccess", "Data has been successfully updated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Updating data has failed"
                  );
        }
    },
    async deleteSystemAvail({ commit }, id) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.delete("/data_management/problems/system_availability/delete/" + id);
            commit("deleteSystemAvail", response.data.data);
            commit("setSuccess", "Data has been successfully deleted !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Deleting data has failed"
                  );
        }
    },

    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
    resetSystemNames({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setSystemName", []);
    },
    resetSystemAvails({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setSystemAvails", []);
    },
    resetSystemAvailsAvg({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setSystemAvailsAvg", []);
    },
    resetYears({ commit }) {
        commit("setYears", []);
    }
};

const mutations = {
    newSystemAvail: (state, systemavail) => {
        state.apiState = ENUM.LOADED;
        state.systemavails.unshift(systemavail);
    },
    deleteSystemAvail: (state, deletedSystemAvail) => {
        state.apiState = ENUM.LOADED;
        const index = state.systemavails.findIndex(systemavail => systemavail.id == deletedSystemAvail.id);
        state.systemavails.splice(index, 1);
    },
    updateSystemAvail: (state, updatedSystemAvail) => {
        state.apiState = ENUM.LOADED;
        const index = state.systemavails.findIndex(systemavail => systemavail.id == updatedSystemAvail.id);
        const header ={
            id:updatedSystemAvail.header.id,
            system_name:updatedSystemAvail.header.system_name
        };
        var num = updatedSystemAvail.percent_system_avail*100;
        const fixObject = {
            id: updatedSystemAvail.id,
            id_system: updatedSystemAvail.id_system,
            system_downtime: updatedSystemAvail.system_downtime,
            percent_system_avail:num.toFixed(2),
            description: updatedSystemAvail.description,
            recorded_at:updatedSystemAvail.recorded_at,
            header:header
        };
        state.systemavails.splice(index, 1, fixObject);
    },
    setEditItem: (state, payload) => {
        state.editedItem = payload;
    },
    setEditIndex: (state, index) => {
        state.editedIndex = index;
    },
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setYears: (state, years) => {
        state.years = years;
    },
    setSystemAvails: (state, systemavail) => {
        state.apiState = ENUM.LOADED;
        state.systemavails = systemavail;
    },
    setSystemAvailsAvg: (state, systemavailavg) => {
        state.apiState = ENUM.LOADED;
        state.systemavailsavg = systemavailavg;
    },
    setSystemName: (state, systemname) => {
        state.apiState = ENUM.LOADED;
        state.systemname = systemname;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
