<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemedyHPDIncidentInterface extends Model
{
    protected $casts = [
        'Submit_Date' => 'datetime:Y-m-d H:i:s'
    ];
    protected $connection = 'sqlsrv_remedy';
    protected $table = 'HPD_IncidentInterface';
    // protected $primaryKey = 'Incident_Number';
}
