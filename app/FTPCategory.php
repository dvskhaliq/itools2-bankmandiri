<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FTPCategory extends Model
{
    protected $fillable = ['ftp_category'];
    protected $table = 'ftp_category';
    public $timestamps = false;

    public function ftp_file_inventory()
    {
        return $this->hasMany(FTPFileInventory::class, 'ftp_category_id', 'id');
    }
}
