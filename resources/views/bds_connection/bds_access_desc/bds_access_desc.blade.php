@extends('sidebar')
@section('content')
<div id="bds_access_desc">
    <div class="row bds-access-desc">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS Access Desc</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="ismodal" info></flash>
                    <div class="panel panel-default tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Penerbitan Kotran</a></li>
                            <li><a href="#tab-second" role="tab" data-toggle="tab">List Kotran</a></li>
                        </ul>
                    </div>

                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-first">
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>User Profile</span>
                                    </div>
                                    <div class="panel-body">
                                        <table-bds-access-desc :data_access_desc="data_access_desc" :ismodal="ismodal" v-on:level-id-selected="levelIdSelected"></table-bds-access-desc>
                                    </div>
                                    <!-- <vue-pagination :pagination="pages" @paginate="getData()" :offset="4"></vue-pagination> -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>Set Kotran</span>
                                    </div>
                                    <componen-form :head_teller="head_teller" :level_id_selected="level_id_selected" v-on:submit-form="submitForm"></componen-form>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-second">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>list Kotran</span>
                                    </div>
                                    <div class="panel-body">
                                        <componen-list-kotran :data_list_kotran="data_list_kotran" v-on:delete-selected="deleteSelected"></componen-list-kotran>
                                        <!-- <vue-pagination :pagination="pages_list_kotran" @paginate="getDataListKotran()" :offset="4"></vue-pagination> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Confirm Delete</h4>
                </div>
                <div v-if="isloaded">
                    <componen-confirm-delete v-on:closewindows="closeWindow" v-on:submit-delete="submitDelete" :list_access_prop="list_access_prop"></componen-confirm-delete>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/bds_access_desc.js"></script>
@endsection