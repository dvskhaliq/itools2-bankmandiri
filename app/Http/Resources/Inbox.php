<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Inbox extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $text_encoded = explode("#", $this->TextDecoded);
        $od_incident_decode = json_decode($this->IncidentOdDecode);
        if($od_incident_decode != null){
            $saverity = explode("-",$od_incident_decode->saverity);
            $typeIncident = explode("-",$od_incident_decode->type_incident);

            return [
                "id" => $this->id,
                "senderNumber" => $this->SenderNumber,
                "serviceTypeName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->servicechannel->servicetype->name : null),
                "serviceChannelName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->servicechannel->name : null),
                "serviceCatalogType" => ($this->ServiceCatalogId != null ? $this->servicecatalog->service_catalog_type : null),
                "serviceCatalogName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->name : null),
                "modalName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->modal_name : null),
                "text" => $this->Text,
                "remedyTicketId" => $this->RemedyTicketId,
                "receivingDateTime" => date('d F Y H:i',strtotime($this->ReceivingDateTime)),
                "bookDateTime" => date('Y-m-d H:i:s',strtotime($this->BookDateTime)),
                "finishDateTime" => date('Y-m-d H:i:s',strtotime($this->FinishDateTime)),
                "processStatus" => $this->ProcessStatus,
                "serviceCatalogId" => $this->ServiceCatalogId,
                "recipientID" => $this->RecipientID,
                "telegramSenderId" => $this->TelegramSenderId,
                "liveChatRequestId" => ($this->TelegramSenderId != null ? $this->chatsubscribeduser->live_chat_request_id : null),
                "assigneeId" => $this->AssigneeId,
                "submitterName" => ($this->AssigneeId != null ? $this->user->name : 'Remedy User'),
                "assigneeName" => ($this->EmployeeAssigneeId !=null ? $this->supportgroupassigneeremedy->full_name : null),
                "employeeAssigneeId" => $this->EmployeeAssigneeId,            
                "employeeAssigneeGroupId" => $this->EmployeeAssigneeGroupId,
                "incidentOdDecode" => $od_incident_decode,
                "odcommandcentername" => (isset($od_incident_decode->odcommandcentername) ? $od_incident_decode->odcommandcentername : ''),
                "saverityOdCommandCenter" => $saverity[1]==='Critical' ? 'Very High' :  $saverity[1],
                "serviceOdCommandCenter" => $od_incident_decode->service,
                "impactOdCommandCenter" => $od_incident_decode->impact,
                "suspectOdCommandCenter" => $od_incident_decode->suspect, 
                "estimasiOdCommandCenter" => $od_incident_decode->estimasi,
                "picIncidentOdCommandCenter" => $od_incident_decode->assignSelected,
                "typeIncidentOdCommandCenter" => $typeIncident[1],
                "ekskalasiStartDate" => date('d F Y H:i',strtotime($this->IncidentStartEkslasiDate)),
                "ekskalasiEndDate" => ($this->IncidentEndEkslasiDate != null ? date('d F Y H:i',strtotime($this->IncidentEndEkslasiDate)) : null),
                "ekskalasiStatus" => $this->ProcessStatusEkskalasiIncident,
                "employeeAssigneeId" => $this->EmployeeAssigneeId,            
                "employeeAssigneeGroupId" => $this->EmployeeAssigneeGroupId,
                "requestbyremedyuser" => (isset($od_incident_decode->requesterByRemedyUser->full_name) ? $od_incident_decode->requesterByRemedyUser->full_name : ''),
                "picSupportInfra" => (isset($od_incident_decode->picSupportInfra) ? $od_incident_decode->picSupportInfra : ''),
                "picSupportTeam" => (isset($od_incident_decode->picSupportTeam) ? $od_incident_decode->picSupportTeam : '')
            ];
        }else{
            return [
                "id" => $this->id,
                "senderNumber" => $this->SenderNumber,
                "serviceTypeName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->servicechannel->servicetype->name : null),
                "serviceChannelName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->servicechannel->name : null),
                "serviceCatalogType" => ($this->ServiceCatalogId != null ? $this->servicecatalog->service_catalog_type : null),
                "serviceCatalogName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->name : null),
                "modalName" => ($this->ServiceCatalogId != null ? $this->servicecatalog->modal_name : null),
                "text" => $this->Text,
                "remedyTicketId" => $this->RemedyTicketId,
                "receivingDateTime" => date('Y-m-d H:i:s',strtotime($this->ReceivingDateTime)),
                "bookDateTime" => date('Y-m-d H:i:s',strtotime($this->BookDateTime)),
                "finishDateTime" => date('Y-m-d H:i:s',strtotime($this->FinishDateTime)),
                "processStatus" => $this->ProcessStatus,
                "serviceCatalogId" => $this->ServiceCatalogId,
                "recipientID" => $this->RecipientID,
                "telegramSenderId" => $this->TelegramSenderId,
                "liveChatRequestId" => ($this->TelegramSenderId != null ? $this->chatsubscribeduser->live_chat_request_id : null),
                "assigneeId" => $this->AssigneeId,
                "submitterName" => ($this->AssigneeId != null ? $this->user->name : 'Remedy User'),
                "assigneeName" => ($this->EmployeeAssigneeId !=null ? $this->supportgroupassigneeremedy->full_name : null),
                "employeeAssigneeId" => $this->EmployeeAssigneeId,            
                "employeeAssigneeGroupId" => $this->EmployeeAssigneeGroupId,
                "srid" => array_key_exists(23,$text_encoded) ? $text_encoded[23] : null,
                "actionTicketCreation" => $this->ActionTicketCreation
            ];
        }
    }
}
