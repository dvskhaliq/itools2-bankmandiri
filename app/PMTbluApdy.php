<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluApdy extends Model
{
    protected $table = 'pm_tblu_apdy';
    public $timestamps = false;    
}
