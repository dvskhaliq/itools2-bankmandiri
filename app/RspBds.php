<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RspBds extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'RSP_BDS';
}
