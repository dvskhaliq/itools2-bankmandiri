require('../../bootstrap');

window.Vue = require('vue');

window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('component-table-before-update', ()=> import('../../components/los/los_consumer/LosConsumerBeforeUpdate.vue'));
Vue.component('component-table-after-update', ()=> import('../../components/los/los_consumer/LosConsumerAfterUpdate.vue'));
Vue.component('component-search', ()=> import('../../components/los/los_consumer/LosConsumerSearch.vue'));
Vue.component('componen-ap_currtrcode', ()=> import('../../components/los/los_consumer/LosConsumerAP_CURRTRCODE.vue'));
Vue.component('componen-ap_nexttrby', ()=> import('../../components/los/los_consumer/LosConsumerAP_NEXTTRBY.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));

import 'es6-promise/auto'
import Vuex from 'vuex';

Vue.use(Vuex);
const los_consumer = new Vue({
    el: '#los_consumer',
    data: {
        ismodal: false,
        isloaded_data: false,
        alert: false,
        isloaded_after: false,
        isloaded: false,
        result_before_update: {},
        result_after_update: {},
        ap_nexttrby: {},
        ap_currtrcode: {},


    },

    methods: {

        setFlashDanger(reqResponse) {
            this.alert = true;
            flash({
                id: "alert-danger",
                message: reqResponse.message,
                flashtype: "danger"
            });
        },

        setFlashInfo(reqResponse) {
            this.alert = true;
            flash({
                id: "alert-info",
                message: reqResponse.message,
                flashtype: "info"
            });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
        },

        searchData(reqResponse) {
            this.ismodal = true;
            axios.post('/los_consumer/search_data', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded_data = true;
                    this.result_before_update = response.data.result;
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        AP_CURRTRCODEselected(reqResponse) {
            this.ismodal = true;

            this.ap_nexttrby = {};
            axios.post('/los_consumer/search_data', reqResponse.request).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.ap_currtrcode = response.data.result;
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        updateAP_CURRTRCODE(reqResponse) {
            axios.post('/los_consumer/update_ap_currtrcode', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.result_before_update = response.data.result.before;
                    this.result_after_update = response.data.result.after;
                    this.isloaded_data = true;
                    this.isloaded_after = true;
                    this.closeWindow();
                    this.setFlashInfo({
                        message: response.data.result.message,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        AP_NEXTTRBYselected(reqResponse) {
            this.ismodal = true;
            this.ap_currtrcode= {};
            axios.post('/los_consumer/search_data', reqResponse.request).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.ap_nexttrby = response.data.result;
                } else {
                    this.setFlashDanger({
                        message: response.data.result,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

        updateAP_NEXTTRBY(reqResponse) {
            axios.post('/los_consumer/update_ap_nexttrby', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.result_before_update = response.data.result.before;
                    this.result_after_update = response.data.result.after;
                    this.isloaded_data = true;
                    this.isloaded_after = true;
                    this.closeWindow();
                    this.setFlashInfo({
                        message: response.data.result.message,
                    });
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger({
                    message: error.response.data.message
                  });
                }
              });
        },

    },

});
