<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceHandler\DataLayer\TbluEdcUssdService;
use App\ServiceHandler\DataLayer\TbluSourceKibanaService;
use App\ServiceHandler\DataLayer\TbluSourceAspService;

class DataTableauController extends BaseController
{
    protected $tblu_edc;
    protected $tblu_kibana;
    protected $tblu_asp;

    public function __construct(TbluEdcUssdService $edc_srv, TbluSourceKibanaService $kibana_srv, TbluSourceAspService $asp_srv)
    {
        $this->tblu_edc = $edc_srv;
        $this->tblu_kibana = $kibana_srv;
        $this->tblu_asp = $asp_srv;
    }

    // Source from Mysql Yogi
    public function edcUssd(Request $request)
    {
	if($request->get('day') == null || $request->get('day') == ''){
            $day='1';
        } else{
            $day=$request->get('day');
        }
        $data = $this->tblu_edc->fetchEdcUssdData($day);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    // public function mandol()
    // {
    //     $data = $this->tblu_edc->fetchMandolData();
    //     return response()->json([
    //         'status' => true,
    //         'data' => $data
    //     ], 200);
    // }
    
    // Source from Sql Server Tabel Vian ASP
    public function aspResptime()
    {
        $data = $this->tblu_asp->fetchAspSourceData();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function success()
    {
        $data = $this->tblu_asp->fetchSuccessRate();
        return response()->json($data, 200);
    }

    // Source from Kibana

    public function eMoneyRestimeKibana(Request $request)
    {        
        $today=date_create(date('Y-m-d'));        
        if(($request->get('start_time') == null || $request->get('start_time') == '') && ($request->get('end_time') == null || $request->get('end_time') == '')){
            $start_time=date_create(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
            $end_time=date_create(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
        } else{
            $start_time=date_create($request->get('start_time'));
            $end_time=date_create($request->get('end_time'));
        }
        if(($start_time>=$today) || ($end_time>=$today) ){
            return response()->json([
                'status' => false,
                'messages' => 'start or end date cannot be same or bigger then today'
            ], 200);
        } else{
            try {
                $diff2=date_diff($today,$end_time);
                $range2=$diff2->format("%a");
                $diff=date_diff($today,$start_time);
                $range=$diff->format("%a");
                for($k=intval($range2);$k<(intval($range2)+intval($range));$k++){
                    $data[$k] = $this->tblu_kibana->eMoneyRestime($k);
                }
                return response()->json([
                    'status' => true,
                    'data' => $data
                ], 200);
            } catch (\Exception $th) {
                return ["messages" => $th->__toString()];
            }
        }
    }

    public function successEmoney(Request $request)
    {        
        $today=date_create(date('Y-m-d'));        
        if(($request->get('start_time') == null || $request->get('start_time') == '') && ($request->get('end_time') == null || $request->get('end_time') == '')){
            $start_time=date_create(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
            $end_time=date_create(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
        } else{
            $start_time=date_create($request->get('start_time'));
            $end_time=date_create($request->get('end_time'));
        }
        if(($start_time>=$today) || ($end_time>=$today) ){
            return response()->json([
                'status' => false,
                'messages' => 'start or end date cannot be same or bigger then today'
            ], 200);
        } else{
            try {
                $diff2=date_diff($today,$end_time);
                $range2=$diff2->format("%a");
                $diff=date_diff($today,$start_time);
                $range=$diff->format("%a");
                for($k=intval($range2);$k<(intval($range2)+intval($range));$k++){
                    $data[$k] = $this->tblu_kibana->emoneySuccess($k);
                }
                return response()->json([
                    'status' => true,
                    'data' => $data
                ], 200);
            } catch (\Exception $th) {
                return ["messages" => $th->__toString()];
            }
        }
    }

    public function successSoaBiru(Request $request)
    {
        $today=date_create(date('Y-m-d'));        
        if(($request->get('start_time') == null || $request->get('start_time') == '') && ($request->get('end_time') == null || $request->get('end_time') == '')){
            $start_time=date_create(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
            $end_time=date_create(date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
        } else{
            $start_time=date_create($request->get('start_time'));
            $end_time=date_create($request->get('end_time'));
        }
        if(($start_time>=$today) || ($end_time>=$today) ){
            return response()->json([
                'status' => false,
                'messages' => 'start or end date cannot be same or bigger then today'
            ], 200);
        } else{
            try {
                $diff2=date_diff($today,$end_time);
                $range2=$diff2->format("%a");
                $diff=date_diff($today,$start_time);
                $range=$diff->format("%a");
                for($k=intval($range2);$k<(intval($range2)+intval($range));$k++){
                    $data[$k] = $this->tblu_kibana->soaBiruSuccess($k);
                }
                return response()->json([
                    'status' => true,
                    'data' => $data
                ], 200);
            } catch (\Exception $th) {
                return ["messages" => $th->__toString()];
            }
        }
    }
}
