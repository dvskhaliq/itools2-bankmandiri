<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RspEmasKln extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'RSP_EMAS_KLN';
}
