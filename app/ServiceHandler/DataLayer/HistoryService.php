<?php

namespace App\ServiceHandler\DataLayer;

//Not Used
interface HistoryService
{
    public function getRequestsHistory($employee_remedy_id, $service_catalog_type, $manager, $field = null, $searchdate = null, $tosearchdate = null, $search = null);//pindah ke InboxService

    public function getRequestListByStatusAndSupportGroupJson($process_status, $channel_name, $employee_assignee_group_id);//pindah ke InboxService
}
