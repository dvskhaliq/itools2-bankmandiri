<?php

namespace App\ServiceHandler\PBMLayer;

use App\ViewSuccessRate;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class SuccessRateImp implements SuccessRateService
{
    public function getSuccessRate(): ?Collection
    {
        $result = ViewSuccessRate::where('tanggal', '=', date('Y-m-d', strtotime('yesterday')))->orderBy('tanggal','desc')
        ->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getSuccessRateByYear(array $years): ?Collection
    {
        $result = ViewSuccessRate::where(function($q) use($years) {
            foreach ($years as $year) {
                $q->whereYear('tanggal', '=', $year, 'or');
            }
        })->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getSuccessRateByMonth($month): ?Collection
    {
        $result = ViewSuccessRate::whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getSuccessRateByMonthLayanan($layanan, $month): ?Collection
    {
        $result = ViewSuccessRate::where('layanan',$layanan)
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','asc')->get();
        return $result;
    }

    //correction
    public function getSuccessRateByMonthAvg($month): ?Collection
    {
        $result = ViewSuccessRate::select(DB::raw("to_char(tanggal, 'YYYY-MM') as bulan, layanan, sum(sukses) as sukses, sum(total) as total, avg(success_rate) as success_rate"))
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->groupBy('bulan')->groupBy('layanan')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getSuccessRateByDate($date): ?Collection
    {
        $result = ViewSuccessRate::where('tanggal', '=', $date)->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getYears(){
        return DB::select("select distinct EXTRACT(YEAR from tanggal) as year from view_success_rate order by EXTRACT(YEAR from tanggal) desc");
    }
}