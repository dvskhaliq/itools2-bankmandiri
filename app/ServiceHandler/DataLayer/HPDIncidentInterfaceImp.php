<?php

namespace App\ServiceHandler\DataLayer;

use App\HPDIncidentInterface as IncidentInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class HPDIncidentInterfaceImp implements HPDIncidentInterfaceService
{
    public function truncateHPDIncidentInterface()
    {
        return IncidentInterface::truncate();
    }

    // public function bulkInsert($collection): ?bool
    // {
    //     return IncidentInterface::insert($collection->toArray());
    // }

    public function insertIncident($payload)
    {
        $rf = new IncidentInterface();
        $rf->IncidentNumber = $payload['IncidentNumber'];
        $rf->AssignedGroupId = $payload['AssignedGroupId'];
        $rf->AssigneeLoginId = $payload['AssigneeLoginId'];
        $rf->CustomerPersonId = $payload['CustomerPersonId'];
        $rf->ItoolsRequestId = $payload['ItoolsRequestId'];
        $rf->ProductCategorizationTier1 = $payload['ProductCategorizationTier1'];
        $rf->ProductCategorizationTier2 = $payload['ProductCategorizationTier2'];
        $rf->ProductCategorizationTier3 = $payload['ProductCategorizationTier3'];
        $rf->OperationalCategorizationTier1 = $payload['OperationalCategorizationTier1'];
        $rf->OperationalCategorizationTier2 = $payload['OperationalCategorizationTier2'];
        $rf->OperationalCategorizationTier3 = $payload['OperationalCategorizationTier3'];
        $rf->ReportedSource = $payload['ReportedSource'];
        $rf->Description = $payload['Description'];
        $rf->Notes = $payload['Notes'];
        $rf->Resolution = $payload['Resolution'];
        $rf->ServiceCI = $payload['ServiceCI'];
        $rf->ServiceType = $payload['ServiceType'];
        $rf->Status = $payload['Status'];
        $rf->StatusReason = $payload['StatusReason'];
        $rf->Impact = $payload['Impact'];
        $rf->Urgency = $payload['Urgency'];
        $rf->Priority = $payload['Priority'];
        $rf->Survey = $payload['Survey'];
        $rf->SubmitDate = $payload['SubmitDate'];
        $rf->AssignedSupportCompany = $payload['AssignedSupportCompany'];
        $rf->AssignedSupportOrganization = $payload['AssignedSupportOrganization'];
        $rf->AssignedGroup = $payload['AssignedGroup'];
        $rf->Assignee = $payload['Assignee'];
        $rf->save();
    }

    public function getHPDIncidentInterface(): ?Collection
    {
        return IncidentInterface::orderBy('SubmitDate','desc')->orderBy('Survey','asc')->get()->load('employeeremedy');
    }

    // public function getTotalByYear()
    // {
    //     return DB::table('hpd_incident_interface')->select(array(DB::raw('EXTRACT(year from SubmitDate) as year'), (DB::raw('count(*) as total')) ))->groupBy('year')->orderBy('year','asc')->get();
    // }
}
