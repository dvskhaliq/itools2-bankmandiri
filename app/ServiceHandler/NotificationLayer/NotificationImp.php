<?php

namespace App\ServiceHandler\NotificationLayer;;

use App\Inbox;
use Auth;
use App\Incident;
use App\ServiceHandler\NotificationLayer\NotificationService;
use App\Workorder;
use App\User;
use App\ServiceHandler\DataLayer\ChatService;
use App\ServiceHandler\DataLayer\TelegramUserService;

class NotificationImp implements NotificationService
{
    protected $chat_ds;
    protected $tlguser_ds;

    public function __construct(
        ChatService $chat_service,
        TelegramUserService $telegram_user_service
        )
    {
        $this->chat_ds = $chat_service;
        $this->tlguser_ds = $telegram_user_service;
    }

    public function performFeedBackNotifier(Inbox $request, String $ticket_status = null, String $resolution_note = null)
    {
        if ($request->RecipientID == 'SMS' && $request->ProcessStatus == 'CLOSED' && ($ticket_status == 'Completed' || $ticket_status == 'Resolved')) {
            $content = 'Request ID : ' . $request->id . ' telah selesai ditangani dengan nomor penanganan : ' . $request->RemedyTicketId . ' . Mandiri Servicedesk';
            $this->chat_ds->postSMSFeedback($request->SenderNumber, $content);
        }
        if ($request->RecipientID == 'SMS' && $request->ProcessStatus == 'REJECT' && ($ticket_status == 'Cancelled' || $ticket_status == 'Rejected')) {
            $content = 'Request ID : ' . $request->id . ' diabaikan, mohon periksa kembali input data anda sesuai format yang diinfokan. Mandiri Servicedesk';
            $this->chat_ds->postSMSFeedback($request->SenderNumber, $content);
        }

        //NOTIFIER CHANNEL TELEGRAM
        if ($request->RecipientID == 'TELEGRAM' && $request->ProcessStatus == 'INPROGRESS') {
            $content = $request->RemedyTicketId == 'N/A' ? 'Request ID: ' . $request->id . ' dalam penanganan Agent Mandiri Servicedesk' : 'Request ID: ' . $request->id . ' dengan nomor penanganan: ' . $request->RemedyTicketId . ' dalam penanganan ' . Auth::user()->fn_remedy ;
            $this->chat_ds->postTelegramFeedback($request->id, $content);
        }
        if ($request->RecipientID == 'TELEGRAM' && $request->ProcessStatus == 'CLOSED' && (($ticket_status == 'Completed' || $ticket_status == 'Resolved') || $ticket_status == '')) {
            $content1 = 'Request ID: ' . $request->id . ' telah selesai ditangani dengan nomor penanganan : ' . $request->RemedyTicketId . ' . Senang dapat membantumu';
            $this->chat_ds->postTelegramFeedback($request->id, $content1);
            $content2 = 'Resolution note: ' . $resolution_note;
            $this->chat_ds->postTelegramFeedback($request->id, $content2);
        }
        if ($request->RecipientID == 'TELEGRAM' && $request->ProcessStatus == 'REJECT' && (($ticket_status == 'Cancelled' || $ticket_status == 'Rejected') || $ticket_status == '')) {
            $content1 = 'Request ID : ' . $request->id . ' dengan nomor penanganan : ' . $request->RemedyTicketId . ' diabaikan/ditolak ';
            $this->chat_ds->postTelegramFeedback($request->id, $content1);
            $content2 = 'Cancelled/rejected reason: ' . $resolution_note;
            $this->chat_ds->postTelegramFeedback($request->id, $content2);
        }

        //NOTIFIER CHANNEL LIVECHAT
        if ($request->RecipientID == 'LIVECHAT' && $request->ProcessStatus == 'REJECT' && ($ticket_status == 'Cancelled' || $ticket_status == 'Rejected')) {
            $content1 = 'Request ID : ' . $request->id . ' dengan nomor penanganan : ' . $request->RemedyTicketId . ' diabaikan/ditolak ';
            $this->chat_ds->postTelegramFeedback($request->id, $content1);
            $content2 = 'Cancelled/rejected reason: ' . $resolution_note;
            $this->chat_ds->postTelegramFeedback($request->id, $content2);
        }
        if ($request->RecipientID == 'LIVECHAT' && $request->ProcessStatus == 'INPROGRESS') {
            $delegated_user = User::where('employee_remedy_id', $request->EmployeeAssigneeId)->where('sgn_id_remedy', $request->EmployeeAssigneeGroupId)->first();
            $content1 = 'Permintaan kamu sedang ditangani, kamu terhubung dengan ' . $delegated_user->fn_remedy . ', Agent Mandiri Servicedesk.';
            $this->chat_ds->postTelegramFeedback($request->id, $content1);
        }
        if ($request->RecipientID == 'LIVECHAT' && $request->ProcessStatus == 'CLOSED' && ($ticket_status == 'Resolved' || $ticket_status == 'Completed')) {
            $content1 = 'Request ID: ' . $request->id . ' telah selesai ditangani dengan nomor penanganan : ' . $request->RemedyTicketId . ' . Senang dapat membantumu';
            $this->chat_ds->postTelegramFeedback($request->id, $content1);
            $content2 = 'Resolution note: ' . $resolution_note;
            $this->chat_ds->postTelegramFeedback($request->id, $content2);
        }
    }

    public function performFeedBackNotifierINC(Inbox $inbox)
    {
        $inc = new Incident();
        $inc->parseInbox($inbox);
        if ($inbox->RecipientID == 'SMS' && ($inc->status == 'Completed' || $inc->status == 'Resolved')) {
            $message = 'Request ID : ' . $inbox->id . ' telah selesai ditangani dengan nomor penanganan : ' . $inbox->RemedyTicketId . ' . Mandiri Servicedesk';
            $this->chat_ds->postSMSFeedback($inbox->SenderNumber, $message);
        }else if ($inbox->RecipientID == 'SMS' && ($inc->status == 'Cancelled' || $inc->status == 'Rejected')) {
            $message = 'Request ID : ' . $inbox->id . ' diabaikan, mohon periksa kembali input data atau request via App Telegram ke @bankmandiri_sd_officialbot. Mandiri Servicedesk';
            $this->chat_ds->postSMSFeedback($inbox->SenderNumber, $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'QUEUED' && $inbox->AssigneeId != Auth::user()->id) {
            $message = "Nomor tiket : " . $inc->incident_number . " telah kami teruskan ke tim " .$inc->assignee_support_group_name. " untuk penanganan lebih lanjut";
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'PENDING' && $inbox->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            $message = "Nomor tiket : " . $inc->incident_number . " dipending untuk dilakukan analisa lebih lanjut, mohon maaf atas ketidaknyamanannya, mohon menunggu";
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'INPROGRESS' && $inbox->AssigneeId == Auth::user()->id) {
            $message = "Nomor tiket : " . $inc->incident_number . " saat ini sedang ditindaklanjuti oleh " . $inc->assignee_first_name . " agent " . $inc->assignee_support_group_name;
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'INPROGRESS' && $inbox->AssigneeId != Auth::user()->id) {
            $message = "Nomor tiket : " . $inc->incident_number . " saat ini sedang ditangani oleh  " . $inc->assignee_first_name;
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && ($inc->status == 'Completed' || $inc->status == 'Resolved')) {
            $message1 = 'Request ID: ' . $inbox->id . ' telah selesai dilakukan dengan nomor penanganan : ' . $inbox->RemedyTicketId . ' . Senang dapat membantumu';
            $this->chat_ds->addChat($inbox, 'Agent', $message1);
            $message2 = 'Resolution note: ' . $inc->resolution;
            $this->chat_ds->addChat($inbox, 'Agent', $message2);
        }else if ($inbox->RecipientID == 'TELEGRAM' && ($inc->status == 'Cancelled' || $inc->status == 'Rejected')) {
            $message1 = 'Request ID : ' . $inbox->id . ' dengan nomor penanganan : ' . $inbox->RemedyTicketId . ' diabaikan/ditolak ';
            $this->chat_ds->addChat($inbox, 'Agent', $message1);
            $message2 = 'Cancelled/rejected reason: ' . $inc->resolution;
            $this->chat_ds->addChat($inbox, 'Agent', $message2);
        }else if ($inbox->RecipientID == 'LIVECHAT' && $inbox->ProcessStatus == 'INPROGRESS') {
            $message = "Nomor tiket : " . $inc->incident_number . " saat ini sedang ditindaklanjuti oleh " . $inc->assignee_first_name . " agent " . $inc->assignee_support_group_name;
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }
    }

    public function performFeedBackNotifierWO(Inbox $inbox, String $note = null)
    {
        $wo = new Workorder();
        $wo->parseInbox($inbox);
        if ($inbox->RecipientID == 'SMS' && ($wo->status == 'Completed' || $wo->status == 'Resolved')) {
            $message = 'Request ID : ' . $inbox->id . ' telah selesai dilakukan dengan nomor penanganan : ' . $inbox->RemedyTicketId . ' . Mandiri Servicedesk';
            $this->chat_ds->postSMSFeedback($inbox->SenderNumber, $message);
        }else if ($inbox->RecipientID == 'SMS' && ($wo->status == 'Cancelled' || $wo->status == 'Rejected')) {
            $message = 'Request ID : ' . $inbox->id . ' diabaikan, mohon periksa kembali input data atau request via App Telegram ke @bankmandiri_sd_officialbot. Mandiri Servicedesk';
            $this->chat_ds->postSMSFeedback($inbox->SenderNumber, $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'QUEUED' && $inbox->EmployeeAssigneeId != Auth::user()->employee_remedy_id) {
            $message = "Nomor tiket : " . $wo->workorder_number . " telah kami teruskan ke tim " . $wo->assignee_support_group_name . ", untuk penanganan lebih lanjut";
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'PENDING' && $inbox->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            $message = "Nomor tiket : " . $wo->workorder_number . " dipending untuk dilakukan analisa lebih lanjut, mohon maaf atas ketidaknyamanannya, mohon menunggu";
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'INPROGRESS' && $inbox->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            $message = "Nomor tiket : " . $wo->workorder_number . " saat ini sedang ditindaklanjuti oleh " . $wo->assignee_first_name . " agent " . $wo->assignee_support_group_name;
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && $inbox->ProcessStatus == 'INPROGRESS' && $inbox->EmployeeAssigneeId != Auth::user()->employee_remedy_id) {
            $message = "Nomor tiket : " . $wo->workorder_number . " saat ini sedang ditangani oleh  " . $wo->assignee_first_name;
            $this->chat_ds->addChat($inbox, 'Agent', $message);
        }else if ($inbox->RecipientID == 'TELEGRAM' && ($wo->status == 'Completed')) {
            $message1 = 'Request ID: ' . $inbox->id . ' telah selesai dilakukan dengan nomor penanganan : ' . $inbox->RemedyTicketId . ' . Senang dapat membantumu';
            $this->chat_ds->addChat($inbox, 'Agent', $message1);
            $message2 = 'WorkOrder note: ' . $note;
            $this->chat_ds->addChat($inbox, 'Agent', $message2);
        }else if ($inbox->RecipientID == 'TELEGRAM' && ($wo->status == 'Cancelled' || $wo->status == 'Rejected')) {
            $message1 = 'Request ID : ' . $inbox->id . ' dengan nomor penanganan : ' . $inbox->RemedyTicketId . ' diabaikan/ditolak ';
            $this->chat_ds->addChat($inbox, 'Agent', $message1);
            $message2 = 'Cancelled/rejected reason: ' . $note;
            $this->chat_ds->addChat($inbox, 'Agent', $message2);
        }
    }

    public function afterClosureFeedback(Inbox $request, Incident $incident)
    {
        if ($request->ProcessStatus == 'QUEUED' && $request->EmployeeAssigneeId != Auth::user()->employee_remedy_id) {
            $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $incident->incident_number . " telah kami teruskan ke tim " . $incident->assignee_support_group_name . ", untuk penanganan lebih lanjut");
        }
        if ($request->ProcessStatus == 'PENDING' && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $incident->incident_number . " dipending untuk dilakukan analisa lebih lanjut, mohon maaf atas ketidaknyamanannya, mohon menunggu");
        }
        if ($request->ProcessStatus == 'INPROGRESS' && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $incident->incident_number . " masih dalam proses penanganan oleh team Mandiri Servicedesk");
        }
        if ($request->ProcessStatus == 'INPROGRESS' && $request->EmployeeAssigneeId != Auth::user()->employee_remedy_id) {
            $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $incident->incident_number . " telah kami teruskan ke tim " . $incident->assignee_support_group_name . ", untuk penanganan lebih lanjut");
        }
        if ($request->ProcessStatus == 'CLOSED') {
            $this->chatFromAgent($request, 'Request ID: ' . $request->id . ' dengan nomor penanganan : ' . $request->RemedyTicketId . ' telah selesai');
            $this->chatFromAgent($request, 'Resolution note : ' . $incident->resolution);
        }
        if ($request->ProcessStatus == 'REJECT') {
            $this->chatFromAgent($request, 'Request ID: ' . $request->id . ' dengan nomor penanganan : ' . $request->RemedyTicketId . ' telah dibatalkan');
            $this->chatFromAgent($request, 'Cancellation note : ' . $incident->resolution);
        }
    }

    public function chatFromAgent(Inbox $request, String $message)
    {
        return $this->chat_ds->addChat($request, 'Agent', $message);
    }

    public function dismissLiveChat(Inbox $request)
    {
        $this->chat_ds->addChat($request, 'Agent', 'Saat ini ' . Auth::user()->fn_remedy . ' sudah meninggalkan chat, kamu kembali ke menu layanan Mandiri Servicedesk, senang dapat membantumu ;)');
        $this->tlguser_ds->updateToMenu($request);
    }
}