<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkStatus extends Model
{
    protected $table = 'pm_bulk_status';
    public $timestamps = false;
}
