@extends('sidebar')
@section('content')
<div class="row" id="app-oddashboard">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div>
                <div class="panel-heading" style="background: #fff;">
                    <div class="panel-title">
                        <h4>Dashboard Incident Management</h4>
                    </div>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="!ismodal" info=></flash>
                    <spinner-loader :isresponsereceived="isresponsereceived" :stomps="stomps"></spinner-loader>
                    <div class="panel panel-default tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tab-incident" role="tab" data-toggle="tab">Incident</a></li>
                             <incident-incoming-od v-on:create-incoming-incident-od="createIncomingIncidentOD" v-on:create-staging-incident-od="createStagingIncidentOD" :servicetype="servicetype"> </task-incoming> 
                            <!-- <task-incoming v-on:create-incoming-incident="createIncomingIncident" v-on:create-staging-incident="createStagingIncident" :servicetype="servicetype"> </task-incoming> -->
                        </ul>
                    </div>

                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-incident">
                            <incident-od v-on:taskselected="loadSelectedRequest" :inhistory="inhistory" :auth="auth" :manager="false"></job-history-inc>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <patching-data :isdataloaded="isdataloaded" :modalname="modalname" :ismodal="ismodal" :requestdata="requestdata" v-on:taskclosure="closuringTask"></patching-data>
        
        <!-- MODAL Incoming Incident OD-->
        <!-- <modal-incoming-incident-od :auth="auth" :source="source" :isdataloaded="isdataloaded" :requestdata="requestdata" :ismodal="ismodal" :processstatus="processstatus" v-on:push-flash-notification="pushPopFlashMessage" v-on:modal-closed="modalClosed" :servicetype="servicetype" :isincidentcreation="isincidentcreation">
        </modal-incoming-incident-od> -->
        <modal-incoming-incident-od v-on:taskclosure="closuringTask" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" :isdelegateenabled="isdelegateenabled">
        </modal-incoming-incident-od>

        <!-- MODAL Create Incident OD-->
        <modal-create-incident-od :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" v-on:modal-closed="modalClosed">
        </modal-create-incident-od>
        
        <!-- MODAL Incident OD-->
        <modal-incident-od :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" v-on:modal-closed="modalClosed">
        </modal-incident-od>

        <!-- MODAL Incident SDS-->
        <modal-create-incident-sds :auth="auth" :source="source" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :modalname="modalname" :processstatus="processstatus" v-on:push-flash-notification="pushPopFlashMessage" v-on:modal-closed="modalClosed" v-on:taskdelegated="delegateTask" v-on:changeadmin="changeAdminDispatcher" :servicetype="servicetype" :isincidentcreation="isincidentcreation" v-on:patching-data="patchingData" v-on:modal-closed="modalClosed">
        </modal-create-incident-sds>
        

        <!-- GALERY -->
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a> <a class="next">›</a> <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
    </div>
</div>
@include('footer')
<script type="text/javascript">
    audiofile = "{{asset('assets/sounds/request_alert.mp3')}}";
    // var socket = new SockJS('http://localhost:8080/itools/websocket');
    // var socket = new SockJS('http://10.254.152.105:8080/itools/websocket'); production
    var socket = new SockJS('http://10.204.1.16:8080/itools/websocket'); //dev
    // var socket = new SockJS('http://10.204.100.129:8080/itools/websocket'); local imam
    stompclient = Stomp.over(socket);
    stompclient.heartbeat.outgoing = 100000;
    stompclient.heartbeat.incoming = 100000;
    stompclient.reconnect_delay = 0;
    stompclient.debug = null;
</script>
<script type="text/javascript" src="../../js/dashboard_od.js"></script>
<script>
    function ecWorkingTask() {
        if ($(".cWorkingTask").css('display') == 'none') {
            $("#icon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cWorkingTask").show("fast");
        } else {
            $("#icon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cWorkingTask").hide("fast");
        }
    }

    function ecRequestQueue() {
        if ($(".cRequestQueue").css('display') == 'none') {
            $("#iconRQ").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cRequestQueue").show("fast");
        } else {
            $("#iconRQ").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cRequestQueue").hide("fast");
        }
    }
</script>
<!-- <script type="text/javascript" src="../../js/sidebar.js"></script> -->
<style>
    #overflowTest {
        height: 100px;
        overflow-y: scroll;
        overflow-x: hidden;
    }
</style>
@endsection