<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'pgsql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [
        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_MYSQL', '127.0.0.1'),
            'port' => env('DB_PORT_MYSQL', '3306'),
            'database' => env('DB_DATABASE_MYSQL', 'forge'),
            'username' => env('DB_USERNAME_MYSQL', 'forge'),
            'password' => env('DB_PASSWORD_MYSQL', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_SQLSRV'),
            'port' => env('DB_PORT_SQLSRV'),
            'database' => env('DB_DATABASE_SQLSRV'),
            'username' => env('DB_USERNAME_SQLSRV', 'axisbds'),
            'password' => env('DB_PASSWORD_SQLSRV', 'ax1sbds'),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        // 'sqlsrv_remedy' => [
        //     'driver' => 'sqlsrv',
        //     'host' => env('DB_HOST_REMEDY'),
        //     'port' => env('DB_PORT_REMEDY'),
        //     'database' => env('DB_DATABASE_REMEDY'),
        //     'username' => env('DB_USERNAME_REMEDY'),
        //     'password' => env('DB_PASSWORD_REMEDY'),
        //     'charset' => 'utf8',
        //     'prefix' => '',
        //     'prefix_indexes' => true,
        // ],

        'as400_query' => [
            'driver' => 'odbc',
            'dsn' => 'AS400',
            'host' => '10.254.4.24',
            'port' => '446',
            'database' => 'QGPL',
            'username' => 'BMSMDCSI',
            'password' => 'qwerty90',
        ],

        'sqlsrv_remedy' => [
            'driver' => 'odbc',
            'dsn' => 'Driver={'.env('ODBC_SQLServer').'};Server=10.254.153.136;Database=ARSystem',
            // 'dsn' => 'Driver=SQL Server;Server=10.204.86.230;Database=ARSystem',
            'host' => '10.254.153.136',
            'port' => '1433',
            'database' => 'ARSystem',
            'username' => 'aradmin',
            'password' => 'AR#Admin#',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        'sqlsrv_los_consumer' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_SQLSRV_LOS_CONSUMER', ''),
            'port' => env('DB_PORT_SQLSRV_LOS_CONSUMER', '1433'),
            'database' => env('DB_DATABASE_SQLSRV_LOS_CONSUMER', ''),
            'username' => env('DB_USERNAME_SQLSRV_LOS_CONSUMER', ''),
            'password' => env('DB_PASSWORD_SQLSRV_LOS_CONSUMER', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        'mysql_edc_ussd' => [
            'driver' => 'mysql',
            'host' => '10.204.38.225',
            'port' => '3306',
            'database' => 'proyek_telegram',
            'username' => 'user.edm',
            'password' => 'edmv01',
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
        ],

        'sqlsrv_appd_mandol' => [
            'driver' => 'odbc',
            'dsn' => 'Driver={' . env('ODBC_SQLServer') . '};Server=10.204.87.13;Database=pmdb',
            'host' => '10.204.87.13',
            'database' => 'pmdb',
            'username' => 'EDM_Replication',
            'password' => 'P@ssw0rd',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        'sqlsrv_atm' => [
            'driver' => 'odbc',
            'dsn' => 'Driver={' . env('ODBC_SQLServer') . '};Server=10.254.153.25;Database=TRX_MONITORING',
            'host' => '10.254.153.25',
            'database' => 'TRX_MONITORING',
            'username' => 'edm',
            'password' => 'Mand1r12020',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        // 'cps' => [
        //     'driver' => 'odbc',
        //     'dsn' => 'Driver={SQL Server};Server=10.243.128.177;Database=DBCPS',
        //     'database' => 'DBCPS',
        //     'host' => '10.243.128.177',
        //     'username' => 'sa',
        //     'password' => 'P@ssw0rd',
        //     'charset' => 'utf8',
        //     'prefix' => '',
        //     'prefix_indexes' => true,
        // ],

        // 'mcs' => [
        //     'driver' => 'odbc',
        //     'dsn' => 'Driver={SQL Server};Server=10.243.128.115;Database=DBMCS',
        //     'database' => 'DBMCS',
        //     'host' => '10.243.128.115',
        //     'username' => 'sa',
        //     'password' => 'P@ssw0rd',
        //     'charset' => 'utf8',
        //     'prefix' => '',
        //     'prefix_indexes' => true,
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [
        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],
    ],
];