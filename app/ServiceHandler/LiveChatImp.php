<?php

namespace App\ServiceHandler;

use App\ChatLiveMessages;
use App\Inbox;
use App\Http\Resources\ChatLiveMessage as ChatLiveMessageResource;
use App\ChatSubscribedUser;
use App\SummaryLiveChatHistory;
use Auth;
use App\WhatsappDispatcherMessages;
use App\Http\Resources\WhatsappDispatcherMessages as WADispatcherMsgResource;
use App\SummaryWhatsappDispatcher;

class LiveChatImp implements LiveChatService
{
    /**
     * @Override
     */

    // public function dismissLivechatRequest($request_id)//Not Used
    // {
    //     $request = Inbox::find($request_id);
    //     if (($request->ProcessStatus == 'INPROGRESS' || $request->ProcessStatus == 'PENDING' || $request->ProcessStatus == 'WAITINGAPR' || $request->ProcessStatus == 'PLANNING')  && $request->AssigneeId == Auth::user()->id) {
    //         $clm = new ChatLiveMessages();
    //         $clm->request_id = $request->id;
    //         $clm->sender = 'Agent';
    //         $clm->content = 'Saat ini ' . Auth::user()->name . ' sudah meninggalkan chat, kamu kembali terhubung dengan Sinta, senang bisa membantumu ;)';
    //         $clm->timestamp = date('Y-m-d H:i:s');
    //         $clm->save();

    //         ChatSubscribedUser::where('id', $request->TelegramSenderId)->update(['live_chat_request_id' => null, 'current_level' => 'level0', 'current_level_id' => 1, 'current_active_mode' => 'MENU', 'total_unrecognized_message' => 0]);
    //         Inbox::where('id', $request->id)->update(['ProcessStatus' => 'CLOSED']);

    //         return response()->json(["status" => true]);
    //     } else {
    //         return response()->json(["status" => false]);
    //     }
    // }

    // public function dismissRedirectLivechat($request_id, $content, $telegram_sender_id)//Not Used
    // {
    //     $clm = new ChatLiveMessages();
    //     $clm->request_id = $request_id;
    //     $clm->sender = 'Agent';
    //     $clm->content = $content;
    //     $clm->save();

    //     $csu = ChatSubscribedUser::find($telegram_sender_id);
    //     $csu->live_chat_request_id = null;
    //     $csu->current_level = 'level0';
    //     $csu->current_level_id = 1;
    //     $csu->current_active_mode = 'MENU';
    //     $csu->total_unrecognized_message = 0;
    //     $csu->save();
    // }

    // public function initLiveChat($request_id)//Not Used
    // {
    //     $clm = new ChatLiveMessages();
    //     $clm->request_id = $request_id;
    //     $clm->sender = 'Agent';
    //     $clm->content = 'Saat ini kamu sudah terhubung dengan ' . Auth::user()->name . ', Agent Mandiri Servicedesk.';
    //     $clm->timestamp = date('Y-m-d H:i:s');
    //     $clm->save();

    //     $chatlog = ChatLiveMessageResource::collection(ChatLiveMessages::where('request_id', $request_id)->orderBy('id')->get());

    //     $inbox = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype')->find($request_id);

    //     return ['chatdata' => $chatlog, 'inbox' => $inbox];
    // }

    // public function getLiveChat($request_id)//Not Used
    // {
    //     $chatlog = ChatLiveMessageResource::collection(ChatLiveMessages::where('request_id', $request_id)->orderBy('id')->get());
    //     $inbox = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype')->find($request_id);
    //     return ['chatdata' => $chatlog, 'inbox' => $inbox];
    // }

    ///////////////////////////////////////////////////////// Below Code is Befor Assessment /////////////////////////////////////////////////////

    // public function getLiveChatHistory($request_id)//Not Used
    // {
    //     return SummaryLiveChatHistory::find($request_id);
    // }

    /**
     * 
     * Whatsapp Dispatcher
     * 
     */
    // public function addDispatcherMessage($array_wa_dispatcher)//Not Used
    // {
    //     $wa_dis = new WhatsappDispatcherMessages();
    //     $wa_dis->request_id = (array_key_exists("request_id", $array_wa_dispatcher) ? $array_wa_dispatcher["request_id"] : null);
    //     $wa_dis->sender_id = (array_key_exists("sender_id", $array_wa_dispatcher) ? $array_wa_dispatcher["sender_id"] : null);
    //     $wa_dis->content = (array_key_exists("content", $array_wa_dispatcher) ? $array_wa_dispatcher["content"] : null);
    //     $wa_dis->timestamp = date('Y-m-d H:i:s');
    //     $wa_dis->save();
    //     $results = new WADispatcherMsgResource(WhatsappDispatcherMessages::find($wa_dis->id));
    //     return $results;
    // }

    // public function deleteDispatcherMessage($message_id)//Not Used
    // {
    //     $message = WhatsappDispatcherMessages::find($message_id);
    //     $message->delete();
    // }

    // public function getDispatcherMessage($array_clause)//Not Used
    // {
    //     $generated_clause = array();
    //     foreach ($array_clause as $clause) {
    //         $arr_temp = [key($clause), $clause];
    //         array_push($generated_clause, $arr_temp);
    //     }
    //     $message = WhatsappDispatcherMessages::where(
    //         $generated_clause
    //     )->get();
    //     return $message;
    // }

    // public function getDispatcherMessageList($request_id)//Not Used
    // {
    //     return WADispatcherMsgResource::collection(WhatsappDispatcherMessages::where('request_id', $request_id)->get());
    // }

    // public function getDispathcerMessageHistory($request_id)//Not Used
    // {
    //     return SummaryWhatsappDispatcher::find($request_id);
    // }

    /**
     * 
     * Livechat
     * 
     */
    // public function redirectToLiveChat($request_id, $assigned_name)//Not Used
    // {
    //     $inbox = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype')->find($request_id);

    //     $csu = ChatSubscribedUser::find($inbox->TelegramSenderId);
    //     $csu->live_chat_request_id = $request_id;
    //     $csu->current_active_mode = 'LIVECHAT';
    //     $csu->total_unrecognized_message = 0;
    //     $csu->save();

    //     $clm = new ChatLiveMessages();
    //     $clm->request_id = $request_id;
    //     $clm->sender = 'Agent';
    //     $clm->content = $assigned_name . ' terhubung melalui livechat untuk melakukan verifikasi atas permintaan anda dengan nomor : ' . $request_id;
    //     $clm->timestamp = date('Y-m-d H:i:s');
    //     $clm->save();

    //     $chatlog = ChatLiveMessageResource::collection(ChatLiveMessages::where('request_id', $request_id)->orderBy('id')->get());

    //     return ['chatdata' => $chatlog, 'inbox' => $inbox];
    // }

    // public function isCustomerInLivechatMode($employee_remedy_id)//Not Used
    // {
    //     return ChatSubscribedUser::where('employee_remedy_id', $employee_remedy_id)
    //         ->where('current_active_mode', 'LIVECHAT')
    //         ->whereNotNull('live_chat_request_id')
    //         ->exists();
    // }

    // public function isIncidentInLivechatMode($request_id)//Not Used
    // {
    //     return ChatSubscribedUser::where('live_chat_request_id', $request_id)->exists();
    // }

    // public function postMessageFromAgent($request_id, $content)//Not Used
    // {
    //     $message = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')->find($request_id)->chatlivemessages()->create([
    //         'content' => $content,
    //         'timestamp' => date('Y-m-d H:i:s'),
    //         'sender' => 'Agent'
    //     ]);
    //     $message->load('inbox.user', 'inbox.chatsubscribeduser', 'inbox.servicecatalog.servicechannel.servicetype');
    //     return $message;
    // }

    // public function postMessageFromCustomer($message_id)//Not Used
    // {
    //     $message = ChatLiveMessages::with('inbox.user', 'inbox.chatsubscribeduser', 'inbox.servicecatalog.servicechannel.servicetype')->find($message_id);
    //     return $message;
    // }
}
