<?php

namespace App\ServiceHandler\IntegrationLayer;

use App\Workorder;
use App\WebServiceResponse;

interface WOIWorkOrderService
{
    public function createWO(Workorder $workorder): WebServiceResponse;

    public function checkingWO(Workorder $workorder): WebServiceResponse;

    public function bookWO(Workorder $workorder): WebServiceResponse;//Kayanya ga kepake

    public function addNoteWO(Workorder $workorder, $note = ''): WebServiceResponse;//Kayanya ga kepake
    
    ///////////////////////////////////////////// Below Code is Before Assessment ////////////////////////////////////////////////////
    public function addWorkInfoLog(array $array_data): WebServiceResponse;

    public function bookTicket($request,$assignee_id);//Still used by parseWorkorder() on MainController

    public function createWorkOrder($request,$assignee_id);//Still used by parseWorkorder() on MainController

    public function createWorkOrderByEntity($remedy_login_id, $remedy_password,$request);//Still used by createRemedyWO() on itoolsAPIController
}
