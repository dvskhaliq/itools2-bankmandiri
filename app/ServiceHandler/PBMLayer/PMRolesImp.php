<?php

namespace App\ServiceHandler\PBMLayer;

use Illuminate\Database\Eloquent\Collection;
use App\PMRoles;

class PMRolesImp implements PMRolesService
{
    public function getItems(): ?Collection
    { 
        return PMRoles::where('status', true)->get();
    }
    public function create($payload): ?PMRoles
    {
        $role = new PMRoles();
        $role->name = $payload['name'];
        $role->status = true;
        $role->created_at = date('Y-m-d H:i:s');
        $role->save();
        return $role;
    }
    public function update($payload): ?PMRoles
    {
        $role = PMRoles::find($payload['id']);
        $role->name = $payload['name'];
        $role->updated_at = date('Y-m-d H:i:s');
        $role->save();
        return $role;
    }
    public function delete(int $id): ?PMRoles
    { 
        $role = PMRoles::find($id);
        $role->delete();
        return $role;
    }
}
