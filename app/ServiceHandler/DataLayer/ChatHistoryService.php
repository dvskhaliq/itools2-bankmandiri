<?php

namespace App\ServiceHandler\DataLayer;

use App\SummaryLiveChatHistory;

interface ChatHistoryService
{
    public function getHistory(Int $request_id): SummaryLiveChatHistory;
}
