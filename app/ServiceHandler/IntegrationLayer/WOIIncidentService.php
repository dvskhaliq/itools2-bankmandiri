<?php

namespace App\ServiceHandler\IntegrationLayer;

use App\EmployeeRemedy;
use App\Inbox;
use App\Incident;
use App\SupportGroupAssigneeRemedy;
use App\WebServiceResponse;

interface WOIIncidentService
{
    public function createIncidentGeneric(Incident $incident, $reconID = null): WebServiceResponse;

    public function createIncidentBook(Inbox $request, SupportGroupAssigneeRemedy $assignee, $reconID = null): WebServiceResponse;

    public function createIncidentGenericOd(Incident $incident, $reconID = null): WebServiceResponse;
    /////////////////////////////////////////// Below Code Is Before Assessment /////////////////////////////////////////////////
    public function createIncidentByEntity($remedy_login_id, $remedy_password, $request);//Still used by createRemedyIN() itoolsAPIController
}
