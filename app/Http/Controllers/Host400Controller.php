<?php 

namespace App\Http\Controllers;

use App\HostBranch;
use App\HostClosedBranch;
use App\HostOpenBranch;
use Illuminate\Support\Facades\DB;
use App\PMTbluSwift as TbluIcs;
use App\PMTbluEmasKln as MasterIcs;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class Host400Controller extends BaseController
{
    public function openBranchData()
    {
        HostOpenBranch::truncate();
        $data = DB::connection('as400_query')->select("select substr(digits(b.tlbrn#),1,3) as area, a.tlbcud, b.tlbrn#, a.tlbtdt as tgl, max(a.tlbtmi) as jam  from bmdatprbk.tllog a left join bmparp.tlctlu b on a.tlbcud=b.tlctl where a.tlbtcd in (998,917,2111) and a.tltxok = 'Y' and b.tlbrn# < 20000 group by substr(digits(b.tlbrn#),1,3), a.tlbcud, b.tlbrn#, a.tlbtdt order by b.tlbrn# asc");
        return HostOpenBranch::insert($data);
    }

    public function closedBranchData()
    { 
        HostClosedBranch::truncate();
        $data = DB::connection('as400_query')->select("select  substr(digits(b.tlbrn#),1,3) as area, a.tlbcud, b.tlbrn#, a.tlbtdt as tgl, max(a.tlbtmi) as jam  from bmdatprbk.tllog a left join bmparp.tlctlu b on a.tlbcud=b.tlctl where a.tlbtcd = 999 and a.tltxok = 'Y' and b.tlbrn# < 20000 and b.TLBRST <>  'Y' group by a.tlbcud, b.tlbrn#, substr(digits(b.tlbrn#),1,3), a.tlbtdt order by b.tlbrn# asc");
        return HostClosedBranch::insert($data);
    }

    public function branchData()
    { 
        HostBranch::truncate();
        $data = DB::connection('as400_query')->select("select substr(a.jdbrid,1,3) as area, b.tlbrn#, b.tlctl,b.tlctld, a.jdname from  bmparp.jhdata a left join bmparp.tlctlu b on b.tlbrn#=a.jdbr where b.tlbrn# < 20000 and b.tlbrn# <> 0");
        return HostBranch::insert($data);
    }

      private function getData($filter = null)
    {
      $origin = $filter == null ? MasterIcs::get()->toArray() : MasterIcs::where('channel', $filter)->get()->toArray();
        $data = collect($origin)->map(function ($item) {
            if($item['channel'] == 'LIVIN_2'){
                DB::connection('as400_query')->statement("create alias slwcs.tpqaXY for bmdatptpi.tpqtlgpf(Q0".date("Ymd", strtotime($this->start_time)).")");
                $livin2 = DB::connection('as400_query')->select($item['query_1'].$this->start_time.$item['query_2'].$this->start_time."' and '".$this->end_time."'");
                DB::connection('as400_query')->statement("drop alias slwcs.tpqaXY");
                return $livin2;
            } else{
                return DB::connection('as400_query')->select($item['query_1'].$this->start_time.$item['query_2'].$this->start_time."' and '".$this->end_time."'");
            }
        });
        return $data;
    }

    public function icsData(Request $request)
    {        
        try{
            $start_time = $request->has('start_time') ? new DateTime($request->get('start_time')) : new DateTime(date('Y/m/d',strtotime("yesterday") ));
            $end_time = $request->has('end_time') ? new DateTime($request->get('end_time')) : new DateTime(date('Y/m/d',strtotime("today") ));

            $interval =  DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start_time, $interval, $end_time);
            $temp = [];

            foreach ($period as $day) {
                $this->start_time = $day->format('Y-m-d H:i:s.u');
                $this->end_time = $day->modify('+1 day -1 sec')->format('Y-m-d H:i:s.u');

                $data = $request->has('application_name') ? $this->getData($request->get('application_name')) : $this->getData();

                array_push($temp, $data);
            }
            foreach ($data as $item) {
                if (!TbluIcs::insert($item)) {
                    return false;
                }
            }
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $th) {
            return response()->json([
                'status' => false,
                'message' => $th->__toString()
            ], 200);
        }
    }

public function veData(Request $request)
    {
        if($request->get('day') == null || $request->get('day') == ''){
            $day='1';
        } else{
            $day=$request->get('day');
        }
        $tgl = date('j', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $bln = date('n', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $thn = date('y', strtotime('-'.$day.' day', strtotime(date('y-m-d'))));
        $date = date('Y-m-d H:i:s.u', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $tgl_2 = date('d', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $bln_2 = date('m', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $data = DB::connection('as400_query')->select("select ttdy, ttmo, ttyr, ttim, ttc , tmtyp from bmdatpatm.x1ptlg2 where snode <>'00' and ttdy = ".$tgl." and ttmo = ".$bln." and ttyr = ".$thn);
        $data4 = DB::connection('as400_query')->select("select 'VE' as channel, count(tmtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'success' as code from bmdatpatm.x1ptlg2 where snode <>'00' and tmtyp LIKE '/%' and ttdy = ".$tgl." and ttmo = ".$bln." and ttyr = ".$thn);
        $data5 = DB::connection('as400_query')->select("select 'VE' as channel, count(tmtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'error' as code from bmdatpatm.x1ptlg2 where snode <>'00' and tmtyp NOT LIKE '/%' and ttdy = ".$tgl." and ttmo = ".$bln." and ttyr = ".$thn);
        if(count($data) == 0){             
                $data4 = DB::connection('as400_query')->select("select 'VE' as channel, count(tmtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'success' as code from bmdatpatm.x1ptlg where snode <>'00' and tmtyp LIKE '/%' and ttdy = ".$tgl." and ttmo = ".$bln." and ttyr = ".$thn);
                $data5 = DB::connection('as400_query')->select("select 'VE' as channel, count(tmtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'error' as code from bmdatpatm.x1ptlg where snode <>'00' and tmtyp NOT LIKE '/%' and ttdy = ".$tgl." and ttmo = ".$bln." and ttyr = ".$thn);
             
                 TbluIcs::insert($data4);
                 TbluIcs::insert($data5);
             
        } else{
             
                 TbluIcs::insert($data4);
                 TbluIcs::insert($data5);
             
        }
        $data1 = DB::connection('as400_query')->select("select ixtdt6 , ixttim, ixtc, ixtyp from bmdatpatm.atmlog2pf where ixtdt6 = ".$thn.$bln_2.$tgl_2." and ixtyp NOT LIKE '/%'");
        $data2 = DB::connection('as400_query')->select("select 'VE_2' as channel, count(ixtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'success' as code from bmdatpatm.atmlog2pf where ixtdt6 = ".$thn.$bln_2.$tgl_2." and ixtyp LIKE '/%'");
        $data3 = DB::connection('as400_query')->select("select 'VE_2' as channel, count(ixtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'error' as code from bmdatpatm.atmlog2pf where ixtdt6 = ".$thn.$bln_2.$tgl_2." and ixtyp NOT LIKE '/%'");
        if(count($data1) == 0){
            $data2 = DB::connection('as400_query')->select("select 'VE_2' as channel, count(ixtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'success' as code from bmdatpatm.atmlogpf where ixtdt6 = ".$thn.$bln_2.$tgl_2." and ixtyp LIKE '/%'");
            $data3 = DB::connection('as400_query')->select("select 'VE_2' as channel, count(ixtyp) as count, TIMESTAMP(CURRENT TIMESTAMP) AS FETCHED_AT, date('".$date."') as FETCHED_DATE, 'error' as code from bmdatpatm.atmlogpf where ixtdt6 = ".$thn.$bln_2.$tgl_2." and ixtyp NOT LIKE '/%'");
            TbluIcs::insert($data2);
            TbluIcs::insert($data3);
        } else{
            TbluIcs::insert($data2);
            TbluIcs::insert($data3);
        }

        return [$data4,$data5,$data2,$data3];
    }
}
