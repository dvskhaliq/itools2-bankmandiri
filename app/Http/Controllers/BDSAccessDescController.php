<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use App\ServiceHandler\BDSLayer\BDSUserProfileService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BDSAccessDescController extends BaseController
{

    protected $bdsServerService;
    protected $userProfileService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection, BDSUserProfileService $userProfile)
    {
        $this->bdsServerService = $databaseConnection;
        $this->userProfileService = $userProfile;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {

        return View::make('bds_connection/bds_access_desc/bds_access_desc');
    }

    public function getList()
    {
        $result = $this->userProfileService->getList();

        return response()->json($result);
    }

    public function getListKotran()
    {
        $result = $this->bdsServerService->initBdsConnection()->table('AccessDesc')->whereIn('FuncID', array('0037', '0067', 'AM37', 'CM67'))->orderBy('FuncID', 'desc')->orderBy('LevelID', 'desc')->get();

        return response()->json($result);
    }

    public function findList()
    {
        if (request()->get('search') == 'null' || request()->get('search') == '') {
            $result = $this->userProfileService->getList();
        } else {
            $result = $this->userProfileService->findById(request()->get('search'));
        }

        return response()->json($result);
    }

    public function findAccessDesc($funcId, $levelIDTeller)
    {
        $result = $this->bdsServerService->initBdsConnection()->table('AccessDesc')->where(['FuncID' => $funcId, 'LevelID' => $levelIDTeller])->get();
        return $result;
    }

    public function addAccessDesc()
    {
        if (request()->get('levelIDTeller') == request()->get('headTeller') ){
            return response()->json([
                'status' => false,
                'result' => 'The level of the ID added must be different from the Head Teller ID level',
            ]);
        } else{
            $findAccessDesc = $this->findAccessDesc(request()->get('funcId'), request()->get('levelIDTeller'))->toArray();
            if (empty($findAccessDesc)) {
                $findAccessDescHeadTeller = $this->findAccessDesc(request()->get('funcId'), request()->get('headTeller'))->toArray();
                if (empty($findAccessDescHeadTeller)) {
                    $dataHeadTeller = array(
                        'LevelID' => request()->get('headTeller'),
                        'FuncID' => request()->get('funcId'),
                        'FuncType' => request()->get('funcId') == 'CM67' ? 'OT' : 'TX',
                        'Override' => 'N',
                    );
                    $data =  array(
                        'LevelID' => request()->get('levelIDTeller'),
                        'FuncID' => request()->get('funcId'),
                        'FuncType' => request()->get('funcId') == 'CM67' ? 'OT' : 'TX',
                        'Override' => 'Y',
                    );
                    $this->bdsServerService->initBdsConnection()->table('AccessDesc')->insert($dataHeadTeller);
                    $this->bdsServerService->initBdsConnection()->table('AccessDesc')->insert($data);

                    return response()->json([
                        'status' => true,
                        'result' => 'Add FuncID : ' . request()->get('funcId') . ' LevelIDTeller : ' . request()->get('levelIDTeller') . '  success',
                    ]);
                }else{
                    $data =  array(
                        'LevelID' => request()->get('levelIDTeller'),
                        'FuncID' => request()->get('funcId'),
                        'FuncType' => request()->get('funcId') == 'CM67' ? 'OT' : 'TX',
                        'Override' => 'Y',
                    );
                    $this->bdsServerService->initBdsConnection()->table('AccessDesc')->insert($data);
                    return response()->json([
                        'status' => true,
                        'result' => 'Add FuncID : ' . request()->get('funcId') . ' LevelIDTeller : ' . request()->get('levelIDTeller') . '  success',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'result' => 'LevelIDTeller : ' . $findAccessDesc[0]['LevelID'] . ' already exists in FuncID : ' . $findAccessDesc[0]['FuncID'],
                ]);
            }
        }
    }

    public function deleteAccessDesc()
    {
        $this->bdsServerService->initBdsConnection()->table('AccessDesc')->where([['LevelID',  request()->get('LevelID')], ['FuncID',  request()->get('FuncID')]])->delete();

        return response()->json([
            'status' => true,
            'result' => 'Delete FuncID : ' . request()->get('FuncID') . ' LevelIDTeller : ' . request()->get('LevelID') . '  success',
        ]);
    }
}
