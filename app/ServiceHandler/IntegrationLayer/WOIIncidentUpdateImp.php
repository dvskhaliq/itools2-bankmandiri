<?php

namespace App\ServiceHandler\IntegrationLayer;

use App\Inbox;
use App\Incident;
use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use App\User;
use App\WebServiceResponse;
use Crypt;

class WOIIncidentUpdateImp implements WOIIncidentUpdateService
{
    public $wsdl_update_inc;
    public $wsdl_recon_id;

    public function __construct()
    {
       // echo "wsdl recon".env('WSDL_RECON_ID');
       // exit();
        $this->wsdl_update_inc = env('WSDL_UPDATE_INC');
        $this->wsdl_recon_id = env('WSDL_RECON_ID');
    }
    
    public function getWsdlUpdateInc(){
        return $this->wsdl_update_inc;
    }

    public function getWsdlReconId(){
        return $this->wsdl_recon_id;
    }

    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeaderAPI($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    ///////////////////////////////////////////////////////////////////////// HEAD /////////////////////////////////////////////////////////////////////

    public function fetchIncident($incident_number, $username = null, $password = null): WebServiceResponse
    {
        try {
            $data = array('IncidentNumber' => $incident_number);
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            if($username == null || $password == null){
                $headerbody = $this->generateHeader();
            }else{
                $headerbody = $this->generateHeaderAPI($username, $password);
            }
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->FetchIncidentGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $payload = [
                'PersonId' => !empty($doc->getElementsByTagName('PersonId')->item(0)->nodeValue) ? $doc->getElementsByTagName('PersonId')->item(0)->nodeValue : null,
                'First_Name' => !empty($doc->getElementsByTagName('First_Name')->item(0)->nodeValue) ? $doc->getElementsByTagName('First_Name')->item(0)->nodeValue : null,
                'Last_Name' => !empty($doc->getElementsByTagName('Last_Name')->item(0)->nodeValue) ? $doc->getElementsByTagName('Last_Name')->item(0)->nodeValue : null,
                'Service_Type' => !empty($doc->getElementsByTagName('Service_Type')->item(0)->nodeValue) ? $doc->getElementsByTagName('Service_Type')->item(0)->nodeValue : null,
                'Status' => !empty($doc->getElementsByTagName('Status')->item(0)->nodeValue) ? $doc->getElementsByTagName('Status')->item(0)->nodeValue : null,
                'Impact' => !empty($doc->getElementsByTagName('Impact')->item(0)->nodeValue) ? $doc->getElementsByTagName('Impact')->item(0)->nodeValue : null,
                'Urgency' => !empty($doc->getElementsByTagName('Urgency')->item(0)->nodeValue) ? $doc->getElementsByTagName('Urgency')->item(0)->nodeValue : null,
                'Description' => !empty($doc->getElementsByTagName('Description')->item(0)->nodeValue) ? $doc->getElementsByTagName('Description')->item(0)->nodeValue : null,
                'Product_Categorization_Tier_1' => !empty($doc->getElementsByTagName('Product_Categorization_Tier_1')->item(0)->nodeValue) ? $doc->getElementsByTagName('Product_Categorization_Tier_1')->item(0)->nodeValue : null,
                'Product_Categorization_Tier_2' => !empty($doc->getElementsByTagName('Product_Categorization_Tier_2')->item(0)->nodeValue) ? $doc->getElementsByTagName('Product_Categorization_Tier_2')->item(0)->nodeValue : null,
                'Product_Categorization_Tier_3' => !empty($doc->getElementsByTagName('Product_Categorization_Tier_3')->item(0)->nodeValue) ? $doc->getElementsByTagName('Product_Categorization_Tier_3')->item(0)->nodeValue : null,
                'Operational_Categorization_Tier_1' => !empty($doc->getElementsByTagName('Operational_Categorization_Tier_1')->item(0)->nodeValue) ? $doc->getElementsByTagName('Operational_Categorization_Tier_1')->item(0)->nodeValue : null,
                'Operational_Categorization_Tier_2' => !empty($doc->getElementsByTagName('Operational_Categorization_Tier_2')->item(0)->nodeValue) ? $doc->getElementsByTagName('Operational_Categorization_Tier_2')->item(0)->nodeValue : null,
                'Operational_Categorization_Tier_3' => !empty($doc->getElementsByTagName('Operational_Categorization_Tier_3')->item(0)->nodeValue) ? $doc->getElementsByTagName('Operational_Categorization_Tier_3')->item(0)->nodeValue : null,
                'Assigned_Support_Company' => !empty($doc->getElementsByTagName('Assigned_Support_Company')->item(0)->nodeValue) ? $doc->getElementsByTagName('Assigned_Support_Company')->item(0)->nodeValue : null,
                'Assigned_Support_Organization' => !empty($doc->getElementsByTagName('Assigned_Support_Organization')->item(0)->nodeValue) ? $doc->getElementsByTagName('Assigned_Support_Organization')->item(0)->nodeValue : null,
                'Assigned_Group_Id' => !empty($doc->getElementsByTagName('Assigned_Group_Id')->item(0)->nodeValue) ? $doc->getElementsByTagName('Assigned_Group_Id')->item(0)->nodeValue : null,
                'Assigned_Group' => !empty($doc->getElementsByTagName('Assigned_Group')->item(0)->nodeValue) ? $doc->getElementsByTagName('Assigned_Group')->item(0)->nodeValue : null,
                'Assignee' => !empty($doc->getElementsByTagName('Assignee')->item(0)->nodeValue) ? $doc->getElementsByTagName('Assignee')->item(0)->nodeValue : null,
                'Resolution' => !empty($doc->getElementsByTagName('Resolution')->item(0)->nodeValue) ? $doc->getElementsByTagName('Resolution')->item(0)->nodeValue : null,
                'Detailed_Description' => !empty($doc->getElementsByTagName('Detailed_Description')->item(0)->nodeValue) ? $doc->getElementsByTagName('Detailed_Description')->item(0)->nodeValue : null,
                'ReportedSource' => !empty($doc->getElementsByTagName('ReportedSource')->item(0)->nodeValue) ? $doc->getElementsByTagName('ReportedSource')->item(0)->nodeValue : null,
                'TanggalEmailMasuk' => !empty($doc->getElementsByTagName('TanggalEmailMasuk')->item(0)->nodeValue) ? $doc->getElementsByTagName('TanggalEmailMasuk')->item(0)->nodeValue : null,
                'IncidentNumber' => !empty($doc->getElementsByTagName('IncidentNumber')->item(0)->nodeValue) ? $doc->getElementsByTagName('IncidentNumber')->item(0)->nodeValue : null,
                'AssigneeLoginId' => !empty($doc->getElementsByTagName('AssigneeLoginId')->item(0)->nodeValue) ? $doc->getElementsByTagName('AssigneeLoginId')->item(0)->nodeValue : null,
            ];
            return new WebServiceResponse(null, 'success', $payload, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function updateIncidentGeneric(Incident $incident, $reconID = null): WebServiceResponse //perlu ditambahkan field login id remedy pada wsdl contoh: 'AssigneeLoginId' => $assignee->remedy_login_id,
    {
        try {
            $data = array(
                'IncidentNumber' => $incident->incident_number == "N/A" || $incident->incident_number == null ? null : $incident->incident_number,
                'ServiceType' => $incident->service_type == "null" || $incident->service_type == null ? null : $incident->service_type,
                'Status' => $incident->status == "null" || $incident->status == null ?  : $incident->status,
                'Impact' => $incident->impact == "null" || $incident->impact == null ? null : $incident->impact,
                'Urgency' => $incident->urgency == "null" || $incident->urgency == null ? null : $incident->urgency,
                'Description' => $incident->summary_description == "null" || $incident->summary_description == null ? null : $incident->summary_description,
                'ProTier1' => $incident->prod_cat_tier1 == "null" || $incident->prod_cat_tier1 == null ? null : $incident->prod_cat_tier1,
                'ProTier2' => $incident->prod_cat_tier2 == "null" || $incident->prod_cat_tier2 == null ? null : $incident->prod_cat_tier2,
                'ProTier3' => $incident->prod_cat_tier3 == "null" || $incident->prod_cat_tier3 == null ? null : $incident->prod_cat_tier3,
                'OprTier1' => $incident->opr_cat_tier1 == "null" || $incident->opr_cat_tier1 == null ? null : $incident->opr_cat_tier1,
                'OprTier2' => $incident->opr_cat_tier2 == "null" || $incident->opr_cat_tier2 == null ? null : $incident->opr_cat_tier2,
                'OprTier3' => $incident->opr_cat_tier3 == "null" || $incident->opr_cat_tier3 == null ? null : $incident->opr_cat_tier3,
                'AssignedSupportCompany' => $incident->assignee_support_company == "null" || $incident->assignee_support_company == null ? null : $incident->assignee_support_company,
                'AssignedSupportOrganization' => $incident->assignee_support_organization == "null" || $incident->assignee_support_organization == null ? null : $incident->assignee_support_organization,
                'AssignedGroup' => $incident->assignee_support_group_name == "null" || $incident->assignee_support_group_name == null ? null : $incident->assignee_support_group_name,
                'Assignee' => $incident->assignee_name == "null" || $incident->assignee_name == null ? null : $incident->assignee_name,
                'AssignedGroupId' => $incident->assignee_support_group_id == "null" || $incident->assignee_support_group_id == null ? null : $incident->assignee_support_group_id,
                'DetailedDescription' => $incident->notes_description == "null" || $incident->notes_description == null ? null : $incident->notes_description,
                'ItoolsRequestId' => $incident->itools_request_id,
                'Resolution' => $incident->resolution == "null" || $incident->resolution == null ? null : $incident->resolution,
                'StatusReason' => $incident->status_reason == "null" || $incident->status_reason == null ? null : $incident->status_reason,
                'AssigneeLoginId' => $incident->assignee_login_id == "null" || $incident->assignee_login_id == null ? null : $incident->assignee_login_id,
                'ServiceCI' => $incident->service == "null" || $incident->service == null ? null : $incident->service,
                'ServiceCI_ReconID' => $reconID == null ? null : $reconID,
                'Survey' => $incident->survey == "null" || $incident->survey == null ? null : $incident->generateSurvey($incident->survey)
            );
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->UpdateIncidentGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('IncidentNumber')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function chekingIncident(Incident $incident): WebServiceResponse  // unutk pengecekan Remedy tiket yang terbentuk apakah tersimpan di Remedy atau tidak
    {
        try {
            $data = array('IncidentNumber' => $incident->incident_number);
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->isIncidentExist($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, 'success', null, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function getServiceReconID(String $serviceCI = null): WebServiceResponse  // unutk pengecekan Remedy tiket yang terbentuk apakah tersimpan di Remedy atau tidak
    {
        try {
            $data = array(
                'ServiceCI'=> $serviceCI == null ? null : $serviceCI,
                'Data_Set_Id'=> "BMC.ASSET"
            );
            $client = new SoapClient($this->getWsdlReconId(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->GetServiceReconID($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('ReconID')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function updateIncidentApi(Incident $incident, $username = '', $password = '', $reconID = null): WebServiceResponse //Update Incident API, ini bisa jadi satu sama updateIncidentGeneric tapi takut nyenggol jadi dipisah dulu
    {
        try {
            $data = array(
                'IncidentNumber' => $incident->incident_number == "N/A" || $incident->incident_number == null ? null : $incident->incident_number,
                'ServiceType' => $incident->service_type == "null" || $incident->service_type == null ? null : $incident->service_type,
                'Status' => $incident->status == "null" || $incident->status == null ?  : $incident->status,
                'Impact' => $incident->impact == "null" || $incident->impact == null ? null : $incident->impact,
                'Urgency' => $incident->urgency == "null" || $incident->urgency == null ? null : $incident->urgency,
                'Description' => $incident->summary_description == "null" || $incident->summary_description == null ? null : $incident->summary_description,
                'ProTier1' => $incident->prod_cat_tier1 == "null" || $incident->prod_cat_tier1 == null ? null : $incident->prod_cat_tier1,
                'ProTier2' => $incident->prod_cat_tier2 == "null" || $incident->prod_cat_tier2 == null ? null : $incident->prod_cat_tier2,
                'ProTier3' => $incident->prod_cat_tier3 == "null" || $incident->prod_cat_tier3 == null ? null : $incident->prod_cat_tier3,
                'OprTier1' => $incident->opr_cat_tier1 == "null" || $incident->opr_cat_tier1 == null ? null : $incident->opr_cat_tier1,
                'OprTier2' => $incident->opr_cat_tier2 == "null" || $incident->opr_cat_tier2 == null ? null : $incident->opr_cat_tier2,
                'OprTier3' => $incident->opr_cat_tier3 == "null" || $incident->opr_cat_tier3 == null ? null : $incident->opr_cat_tier3,
                'AssignedSupportCompany' => $incident->assignee_support_company == "null" || $incident->assignee_support_company == null ? null : $incident->assignee_support_company,
                'AssignedSupportOrganization' => $incident->assignee_support_organization == "null" || $incident->assignee_support_organization == null ? null : $incident->assignee_support_organization,
                'AssignedGroup' => $incident->assignee_support_group_name == "null" || $incident->assignee_support_group_name == null ? null : $incident->assignee_support_group_name,
                'Assignee' => $incident->assignee_name == "null" || $incident->assignee_name == null ? null : $incident->assignee_name,
                'AssignedGroupId' => $incident->assignee_support_group_id == "null" || $incident->assignee_support_group_id == null ? null : $incident->assignee_support_group_id,
                'DetailedDescription' => $incident->notes_description == "null" || $incident->notes_description == null ? null : $incident->notes_description,
                'ItoolsRequestId' => $incident->itools_request_id,
                'Resolution' => $incident->resolution == "null" || $incident->resolution == null ? null : $incident->resolution,
                'StatusReason' => $incident->status_reason == "null" || $incident->status_reason == null ? null : $incident->status_reason,
                'AssigneeLoginId' => $incident->assignee_login_id == "null" || $incident->assignee_login_id == null ? null : $incident->assignee_login_id,
                'ServiceCI' => $incident->service == "null" || $incident->service == null ? null : $incident->service,
                'ServiceCI_ReconID' => $reconID == null ? null : $reconID
            );
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeaderAPI($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->UpdateIncidentGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('IncidentNumber')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    //////////////////////////////////////////////////////////////// Below Code is Before Assessment ///////////////////////////////////////////////////

    public function addWorkInfoLogInc(array $array_data, $username = null, $password = null): WebServiceResponse
    {
        try {
            $data = array(
                'IncidentNumber'=> $array_data["ticket_id"],
                'IncidentEntryId'=> $array_data["ticket_id"],
                'WorkInfoType' => $array_data["type"],
                'WorkInfoNotes' => $array_data["notes"],
                'AttachmentData' => base64_decode($array_data["attachment_data"]),
                'AttachmentName' => $array_data["attachment_name"],
                'AttachmentSize' => $array_data["attachment_size"]
                );
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            if($username == null || $password == null){
                $headerbody = $this->generateHeader();
            }else{
                $headerbody = $this->generateHeaderAPI($username, $password);
            }
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->AddWorkInfoLogInc($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('WorkInfoId')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function bookIncident($request, $assignee_id)//Still used by parseIncident() on MainController
    {
        try {
            $actual_assingee = User::with('supportgroupassigneeremedy')->find($assignee_id);
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'IncidentNumber' => $request->RemedyTicketId,
                'AssigneeName' => $actual_assingee->supportgroupassigneeremedy->full_name,
                'AssigneeLoginID' => $actual_assingee->un_remedy,
                'AssigneeGroupName' => $actual_assingee->sgn_remedy,
                'AssigneeCompanyName' => $actual_assingee->supportgroupassigneeremedy->company,
                'AssigneeOrganizationName' => $actual_assingee->supportgroupassigneeremedy->organization,
                'AssigneeGroupId' => $actual_assingee->supportgroupassigneeremedy->support_group_id,
                'Status' => 'In Progress',
                'ItoolsRequestId' => $request->id
            );
            $client->BookIncident($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function isIncidentExist($request)
    {
        try {
            $client = new SoapClient($this->getWsdlUpdateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array('IncidentNumber' => $request->RemedyTicketId);
            $client->isIncidentExist($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }
}
