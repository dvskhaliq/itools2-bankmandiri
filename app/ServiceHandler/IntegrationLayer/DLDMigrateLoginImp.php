<?php

namespace App\ServiceHandler\IntegrationLayer;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use App\WebServiceResponse;

class DLDMigrateLoginImp implements DLDMigrateLoginService
{
    private $urlWsdl =  "http://10.204.86.229/arsys/WSDL/public/lbapp/MANDIRI:CSI:DLD:Interface2:DataWizardAction";    
    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar('imam.taufik', XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar('Mandiri1', XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    private function defaultAppList()
    {
        return array(
            'DWS' => ['ApplicationGUID' => 'FS00C04FA081BA0ctmQg[tLiEQmgAA', 'TargetObject' => 'CTM:People', 'AdminType' => 'Login ID'],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CTM:People", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CTM:People Permission Groups", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CTM:Support Group", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CTM:Support Group Association", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CTM:SupportGroupFunctionalRole", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "COM:Company", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CFG:Generic Catalog", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CFG:Service Catalog", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "SIT:Site", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "SIT:Site Company Association", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "Foundation forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "AR System User Preference", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "CMDB forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "AP00C04FA081BA2Nm_PwBDU0IQQAAA", 'TargetObject' => "Approval Server forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "AP00C04FA081BA2Nm_PwBDU0IQQAAA", 'TargetObject' => "Assignment Engine forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "AI00C04FA081BAfFEOQwIjPVWw8gYA", 'TargetObject' => "AST:WorkLog", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "AI00C04FA081BAfFEOQwIjPVWw8gYA", 'TargetObject' => "Asset Inventory forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a04548561", 'TargetObject' => "Asset Configuration forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a04548561", 'TargetObject' => "AST:PurchaseLineItem", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a04548561", 'TargetObject' => "AST:PurchaseRequisition", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a04548561", 'TargetObject' => "AST:PurchaseRequisitionWorkLog", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a04548561", 'TargetObject' => "AST:PurchaseOrder", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a04548561", 'TargetObject' => "Asset forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a0454854d", 'TargetObject' => "Change Configuration forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a0454854d", 'TargetObject' => "CHG:Infrastructure Change", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a0454854d", 'TargetObject' => "CHG:ChangeInterface_Create", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a0454854d", 'TargetObject' => "CHG:WorkLog", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "ID00C04F651D063b42125a0454854d", 'TargetObject' => "Change forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "IM00C04FA081BAUcxmQg[8XkEQmwAA", 'TargetObject' => "Help Desk Configuration forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "IM00C04FA081BAUcxmQg[8XkEQmwAA", 'TargetObject' => "HPD:Help Desk", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "IM00C04FA081BAUcxmQg[8XkEQmwAA", 'TargetObject' => "HPD:Help Desk Assignment Log", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "IM00C04FA081BAUcxmQg[8XkEQmwAA", 'TargetObject' => "HPD:IncidentInterface_Create", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "IM00C04FA081BAUcxmQg[8XkEQmwAA", 'TargetObject' => "HPD:WorkLog", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "IM00C04FA081BAUcxmQg[8XkEQmwAA", 'TargetObject' => "Help Desk forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "PM00C04FA081BAp8xmQghBbmEQnAAA", 'TargetObject' => "PBM:Known Error", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "PM00C04FA081BAp8xmQghBbmEQnAAA", 'TargetObject' => "PBM:Problem Investigation", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "PM00C04FA081BAp8xmQghBbmEQnAAA", 'TargetObject' => "PBM:ProblemInterface_Create", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "PM00C04FA081BAp8xmQghBbmEQnAAA", 'TargetObject' => "PBM:Solution Database", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "PM00C04FA081BAp8xmQghBbmEQnAAA", 'TargetObject' => "Problem forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BA1vRjQgnQ3LBgQgAA", 'TargetObject' => "Service Request Configuration forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BA1vRjQgnQ3LBgQgAA", 'TargetObject' => "SRM:Request", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BA1vRjQgnQ3LBgQgAA", 'TargetObject' => "SRM:RequestInterface_Create", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BA1vRjQgnQ3LBgQgAA", 'TargetObject' => "SRM:WorkInfo", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BA1vRjQgnQ3LBgQgAA", 'TargetObject' => "SRM:AppInstanceBridge", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BA1vRjQgnQ3LBgQgAA", 'TargetObject' => "Service Request forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "TM00C04FA081BAtrVmQgYXmMEQlgAA", 'TargetObject' => "TMS:Task", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "TM00C04FA081BAtrVmQgYXmMEQlgAA", 'TargetObject' => "TMS:Flow", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "TM00C04FA081BAtrVmQgYXmMEQlgAA", 'TargetObject' => "TMS:WorkInfo", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "TM00C04FA081BAtrVmQgYXmMEQlgAA", 'TargetObject' => "Task forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "User", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "FS00C04FA081BA0ctmQg[tLiEQmgAA", 'TargetObject' => "Report", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "RM00C04FA081BAqsqGRwN5fVKAMu0D", 'TargetObject' => "Release Configuration forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "RM00C04FA081BAqsqGRwN5fVKAMu0D", 'TargetObject' => "RMS:Release", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "RM00C04FA081BAqsqGRwN5fVKAMu0D", 'TargetObject' => "Release forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "AA00C04FA081BAFAvORwCkKyMw3EYA", 'TargetObject' => "Application Activity forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:ArticleHistory", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:Feedback", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:KnowledgeArticleManager", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:SearchHistory", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:UpdateRequests", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:VisibilityGroupAndCompany_Article", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:WatchList", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "Knowledge Management Configuration forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "SR00C04FA081BAhH7jRAhf9i_QTHEB", 'TargetObject' => "Service Request Management forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "Knowledge Management Search forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "Knowledge Management Template forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "Knowledge Management forms", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:NotificationToSend", 'AdminType' => "Login ID"],
            'DWS' => ['ApplicationGUID' => "KM00C04FA081BAkzXdRAKRHW5AtFIB", 'TargetObject' => "RKM:Notifications", 'AdminType' => "Login ID"]
        );
    }

    public function initMigration($source_login_id, $target_login_id, $event_guid)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array_merge(array(
                'FormName' => 'Login ID',
                'AdminType' => 'Update',
                'OldLoginID' => $source_login_id,
                'NewLoginID' => $target_login_id,
                'EventGUID' => $event_guid,
            ), $this->defaultAppList());
            $client->InitMigration($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'DataWizardID' => $doc->getElementsByTagName('Data_Wizard_ID')->item(0)->nodeValue,
            );
            return new WebServiceResponse('Migration from login ID:'.$source_login_id.' to login ID:'.$target_login_id.' Submited', 'success', '', $result);
            // return $result;
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }



    public function checkMigrationStatus($data_wizard_id)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'DataWizardID' => $data_wizard_id,
            );
            $client->GetWorkerStatus($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $result = array(
                'DWActionBy' => $doc->getElementsByTagName('DWActionBy')->item(0)->nodeValue,
                'DWActionDate' => $doc->getElementsByTagName('DWActionDate')->item(0)->nodeValue
            );
            return new WebServiceResponse('Migration Completed!!', 'success', '', $result);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }
}
