<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\ResponseTime\TbluEdcService;
use App\ServiceHandler\PBMLayer\ResponseTime\TbluKibanaService;
use App\ServiceHandler\PBMLayer\ResponseTime\TbluMandolService;
use App\ServiceHandler\PBMLayer\ResponseTime\TbluAspRespService;
use Illuminate\Http\Request;

class ResponseTimeCommonController extends BaseController
{
    protected $tblu_edc_repo;
    protected $tblu_kibana_repo;
    protected $tblu_mandol_repo;
    protected $tblu_asp_repo;

    public function __construct(
                
        TbluEdcService $tblu_edc_service,
        TbluKibanaService $tblu_kibana_service,
        TbluMandolService $tblu_mandol_service,
        TbluAspRespService $tblu_asp_service
        )
    {        
        $this->tblu_edc_repo = $tblu_edc_service;
        $this->tblu_kibana_repo = $tblu_kibana_service;
        $this->tblu_mandol_repo = $tblu_mandol_service;
        $this->tblu_asp_repo = $tblu_asp_service;
    }

    public function fetchResponseTime(Request $request)
    {
        switch($request->input('source')){
            case 'edc':
                $query = $this->tblu_edc_repo->getResponseTime();
                break;
            case 'kibana':
                $query = $this->tblu_kibana_repo->getResponseTime();
                break;
            case 'mandol':
                $query = $this->tblu_mandol_repo->getResponseTime();
                break;            
            case 'asp_source':
                $query = $this->tblu_asp_repo->getResponseTime();
                break;
            default:
                $query = $this->tblu_asp_repo->getResponseTime();
        }
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByDate(Request $request)
    {
        $selected_date = $request->input('selectedDate');
        switch($request->input('source')){
            case 'edc':
                $query = $this->tblu_edc_repo->getResponseTimeByDate($selected_date);
                break;
            case 'kibana':
                $query = $this->tblu_kibana_repo->getResponseTimeByDate($selected_date);
                break;
            case 'mandol':
                $query = $this->tblu_mandol_repo->getResponseTimeByDate($selected_date);
                break;
            case 'asp_source':
                $query = $this->tblu_asp_repo->getResponseTimeByDate($selected_date);
                break;
            default:
                $query = $this->tblu_asp_repo->getResponseTimeByDate($selected_date);
        }
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByMonth(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        switch($request->input('source')){
            case 'edc':
                $query = $this->tblu_edc_repo->getResponseTimeByMonth($selected_month);
                break;
            case 'kibana':
                $query = $this->tblu_kibana_repo->getResponseTimeByMonth($selected_month);
                break;
            case 'mandol':
                $query = $this->tblu_mandol_repo->getResponseTimeByMonth($selected_month);
                break;
            case 'asp_source':
                $query = $this->tblu_asp_repo->getResponseTimeByMonth($selected_month);
                break;
            default:
                $query = $this->tblu_asp_repo->getResponseTimeByMonth($selected_month);
        }
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByMonthAvg(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        switch($request->input('source')){
            case 'edc':
                $avg_query = $this->tblu_edc_repo->getResponseTimeByMonthAvg($selected_month);
                $data = $this->loopForeachGroup($avg_query, $selected_month, 'edc');
                break;
            case 'kibana':
                $avg_query = $this->tblu_kibana_repo->getResponseTimeByMonthAvg($selected_month);
                $data = $this->loopForeachGroup($avg_query, $selected_month, 'kibana');
                break;
            case 'mandol':
                $avg_query = $this->tblu_mandol_repo->getResponseTimeByMonthAvg($selected_month);
                $data = $this->loopForeachGroup($avg_query, $selected_month, 'mandol');
                break;
            default:
                $avg_query = $this->tblu_asp_repo->getResponseTimeByMonthAvg($selected_month);
                $data = $this->loopForeachGroup($avg_query, $selected_month, 'asp_source');
        }
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByYear(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        switch($request->input('source')){
            case 'edc':
                $query = $this->tblu_edc_repo->getResponseTimeByYear($selected_year);
                break;
            case 'kibana':
                $query = $this->tblu_kibana_repo->getResponseTimeByYear($selected_year);
                break;
            case 'mandol':
                $query = $this->tblu_mandol_repo->getResponseTimeByYear($selected_year);
                break;
            default:
            $query = $this->tblu_asp_repo->getResponseTimeByYear($selected_year);
        }
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function loopForeach($data){
        $hasil=array();
        foreach ($data as $key => $value) {
            $prop['layanan'] = $value->layanan;
            $prop['node'] = $value->node;
            $prop['restime'] = ROUND($value->restime*1,2);
            $prop['tanggal'] = $value->tanggal;

            array_push($hasil, [
                'layanan' => $prop['layanan'],
                'node' => $prop['node'],
                'restime' => $prop['restime'],
                'tanggal' => $prop['tanggal'],
                ]);
        }
        return $hasil;
    }

    public function loopForeachGroup($avg, $month, $source){
        $hasil=array();
        foreach ($avg as $key => $value) {
            $prop['layanan'] = $value->layanan;
            $prop['restime'] = ROUND($value->restime*1,2);
            $prop['tanggal'] = $value->bulan;
            switch($source){
                case 'edc':
                    $query = $this->tblu_edc_repo->getResponseTimeByMonthSystem($prop['layanan'], $month);
                    break;
                case 'kibana':
                    $query = $this->tblu_kibana_repo->getResponseTimeByMonthSystem($prop['layanan'], $month);
                    break;
                case 'mandol':
                    $query = $this->tblu_mandol_repo->getResponseTimeByMonthSystem($prop['layanan'], $month);
                    break;
                default:
                $query = $this->tblu_asp_repo->getResponseTimeByMonthSystem($prop['layanan'], $month);
            }
            $decodedetail=array();
            foreach ($query as $keydetail => $value) {
                    $prop['layanan'] = $value->layanan;
                    $prop['node'] = $value->node;
                    $prop['restime'] = ROUND($value->restime*1,2);
                    $prop['tanggal'] = $value->tanggal;
                array_push($decodedetail, [
                    'layanan' => $prop['layanan'],
                    'node' => $prop['node'],
                    'restime' => $prop['restime'],
                    'tanggal' => $prop['tanggal'],
                    ]);
            }
            array_push($hasil, [
                'id' => $key+1,
                'layanan' => $prop['layanan'],
                'restime' => $prop['restime'],
                'tanggal' => $prop['tanggal'],
                'detail' => $decodedetail
                ]);
        }
        return $hasil;
    }

    public function fetchYears(Request $request)
    {
        switch($request->input('source')){
            case 'edc':
                $data = $this->tblu_edc_repo->getYears();
                break;
            case 'kibana':
                $data = $this->tblu_kibana_repo->getYears();
                break;
            case 'mandol':
                $data = $this->tblu_mandol_repo->getYears();
                break;
            case 'asp_source':
                $data = $this->tblu_asp_repo->getYears();
                break;
            default:
                $data = $this->tblu_asp_repo->getYears();
        }
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }
}
