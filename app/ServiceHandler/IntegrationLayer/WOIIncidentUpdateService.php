<?php

namespace App\ServiceHandler\IntegrationLayer;

use App\Incident;
use App\WebServiceResponse;

interface WOIIncidentUpdateService
{
    public function getServiceReconID(String $serviceCI = null): WebServiceResponse;

    public function updateIncidentGeneric(Incident $incident, $reconID = null) : WebServiceResponse;

    public function chekingIncident(Incident $incident): WebServiceResponse;

    public function fetchIncident($incident_number, $remedy_login_id = null, $remedy_password = null);

    public function updateIncidentApi(Incident $incident, $username = '', $password = '', $reconID = null): WebServiceResponse;
    ///////////////////////////////////////////// Below Code is Before Assessment ////////////////////////////////////////////////////
    public function bookIncident($request,$assignee_id);//Still used by parseIncident() on MainController

    public function addWorkInfoLogInc(array $array_data, $username = null, $password = null): WebServiceResponse;
}
