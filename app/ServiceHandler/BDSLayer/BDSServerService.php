<?php

namespace App\ServiceHandler\BDSLayer;

interface BDSServerService
{
    public function listAll();

    public function listAllByPagination();

    public function findData($search);

    public function dataSelected($id);

    public function findByKodeCabang($kodeCabang);

    public function findByIpServer($ipServer);

    public function initBdsConnection();

    public function setConnection($ipServer, $kodeCabang, $namaCabang, $remedy_login_id = '', $remedy_password = '');

    public function setDisonnect($host, $database);

    public function submitInsert($dataUpdate);

    public function submitUpdated($id, $dataUpdate);

    public function submitDeleted($id);

}
