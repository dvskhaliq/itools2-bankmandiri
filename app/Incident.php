<?php 

namespace App;

use App\Inbox;
use App\SupportGroupAssigneeRemedy;
use App\EmployeeRemedy;
use App\User;
use App\ServiceHandler\DataLayer\UserService;

class Incident
{
    //Entity of Requestor
    public $requester_person_id;
    public $requester_full_name;
    public $requester_first_name;
    public $requester_last_name;
    public $requester_company;
    public $requester_email = '';
    public $requester_region_name;
    public $requester_group_name;
    public $requester_dept_name;
    //Entity of Incident
    public $incident_number;
    public $service_type = 'User Service Restoration';
    public $status;
    public $impact;
    public $urgency = '4-Low';
    public $summary_description;
    public $reported_source;
    public $prod_cat_tier1;
    public $prod_cat_tier2;
    public $prod_cat_tier3;
    public $opr_cat_tier1;
    public $opr_cat_tier2;
    public $opr_cat_tier3;
    public $notes_description;
    public $tamp_note;
    public $status_reason = 'No Further Action Required';
    public $priority = 'Low';
    public $product_name = '';
    public $model_version = '';
    public $resolution;
    public $service;
    public $survey;
    /* Add by 1991725227 - putut.mukti dashboard OD 19082020*/
    //Entity of Incident OD Command Center
    public $company;
    public $incident;
    public $type_incident;
    public $saverity;
    public $suspect;
    public $estimasi;
    public $status_incident;
    public $impactValue;
    public $incident_od_decode;
    public $ekskalasiStatus;
    public $solusi;
    public $durasiGangguan;
    public $waktuTerindikasiSelected;
    public $endEkskalasi;
    public $updateEkskalasi;
    public $eksternalPic;
    public $remedyProcessStatus;
    //Entity of Assignee
    public $assignee_support_company;
    public $assignee_support_organization;
    public $assignee_support_group_name;
    public $assignee_support_group_id;
    public $assignee_login_id;
    public $assignee_name;
    public $assignee_first_name;
    public $assignee_user_id;
    public $assignee_id;
    public $pic_support_group;
    public $pic_support_team;
    public $pic_infrastructure;
    //Entity of iTools
    public $itools_request_id;
    public $text_decoded;
    public $udh;

    public function parseInbox(Inbox $request): ?Incident
    {
        $text_encoded = explode("#", $request->TextDecoded);
        $cust = EmployeeRemedy::find($request->EmployeeSenderId);
        $asgn = SupportGroupAssigneeRemedy::where("employee_remedy_id", $request->EmployeeAssigneeId)->where("support_group_id", $request->EmployeeAssigneeGroupId)->first();
        $status = "";
        switch ($request->ProcessStatus) {
            case 'INPROGRESS':
                $status = "In Progress";
                break;
            case 'PENDING':
                $status = "Pending";
                break;
            case "QUEUED":
                $status = "Assigned";
                break;
            case "CLOSED":
                $status = "Resolved";
                break;
            case "REJECT":
                $status = "Cancelled";
                break;
        }
        try {
            $this->itools_request_id = $request->id;
            $this->incident_number = $request->RemedyTicketId;
            $this->status = $status;
            if ($cust) {
                $this->requester_company = $cust->company;
                $this->requester_person_id = $cust->id;
                $this->requester_first_name = $cust->first_name;
                $this->requester_last_name = $cust->last_name;
                $this->requester_dept_name = $cust->department_name;
                $this->requester_region_name = $cust->region_name;
                $this->requester_group_name = $cust->group_name;
                $this->requester_full_name = $cust->full_name;
            }
            if ($asgn) {
                $this->assignee_support_company = $asgn->company;
                $this->assignee_support_organization = $asgn->organization;
                $this->assignee_support_group_name = $asgn->support_group_name;
                $this->assignee_support_group_id = $asgn->support_group_id;
                $this->assignee_name = $asgn->full_name;
                $this->assignee_first_name = $asgn->first_name;
                $this->assignee_user_id = $asgn->user != null ? $asgn->user->id : null;
                $this->assignee_id = $asgn->employee_remedy_id;
                $this->assignee_login_id = $asgn->remedy_login_id;
            }
            $this->impact = $text_encoded[7];
            $this->urgency = $text_encoded[8];
            $this->notes_description = $text_encoded[3];
            $this->summary_description = $text_encoded[4];
            $this->reported_source = $text_encoded[10];
            $this->prod_cat_tier1 = $text_encoded[12];
            $this->prod_cat_tier2 = $text_encoded[13];
            $this->prod_cat_tier3 = $text_encoded[14];
            $this->opr_cat_tier1 = $text_encoded[17];
            $this->opr_cat_tier2 = $text_encoded[18];
            $this->opr_cat_tier3 = $text_encoded[19];
            $this->resolution = $text_encoded[20];
            $this->service = array_key_exists(21,$text_encoded) ? $text_encoded[21] : null;
            $this->survey = array_key_exists(22,$text_encoded) ? strlen($text_encoded[22]) == 1 ? $this->decodeSurvey($text_encoded[22]) : $text_encoded[22] : null;
            $this->text_decoded = "INCIDENT#"
                . $this->requester_company . "#"
                . $this->service_type . "#"
                . $this->notes_description . "#"
                . $this->summary_description . "#"
                . $this->assignee_support_group_name . "#"
                . $this->assignee_name . "#"
                . $this->impact . "#"
                . $this->urgency . "#"
                . $this->priority . "#"
                . $this->reported_source . "#"
                . $this->assignee_support_company . "#"
                . $this->prod_cat_tier1 . "#"
                . $this->prod_cat_tier2 . "#"
                . $this->prod_cat_tier3 . "#"
                . $this->product_name . "#"
                . $this->model_version . "#"
                . $this->opr_cat_tier1 . "#"
                . $this->opr_cat_tier2 . "#"
                . $this->opr_cat_tier3 . "#"
                . $this->resolution . "#"
                . $this->service . "#"
                . $this->survey;
            $this->udh = $this->requester_full_name . "#"
                . $this->requester_first_name . "#"
                . $this->requester_last_name . "#"
                . $this->requester_email . "#"
                . $this->requester_company . "#"
                . $this->requester_region_name . "#"
                . $this->requester_group_name . "#"
                . $this->requester_dept_name;
            return $this;
        } catch (\Exception $th) {
            return null;
        }
    }

    public function parseInboxOd(Inbox $request): ?Incident
    {
        $text_encoded = json_decode($request->TextDecoded);
        $cust = EmployeeRemedy::find($request->EmployeeSenderId);
        // $asgn = SupportGroupAssigneeRemedy::where("employee_remedy_id", $request->EmployeeAssigneeId)->where("support_group_id", $request->EmployeeAssigneeGroupId)->first();
        $status = "";
        switch ($request->ProcessStatus) {
            case 'INPROGRESS':
                $status = "In Progress";
                break;
            case 'PENDING':
                $status = "Pending";
                break;
            case "QUEUED":
                $status = "Assigned";
                break;
            case "CLOSED":
                $status = "Resolved";
                break;
            case "REJECT":
                $status = "Cancelled";
                break;
        }
        try {
            $this->itools_request_id = $request->id;
            $this->incident_number = $request->RemedyTicketId;
            $this->status = $status;
            if ($cust) {
                $this->requester_company = $cust->company;
                $this->requester_person_id = $cust->id;
                $this->requester_first_name = $cust->first_name;
                $this->requester_last_name = $cust->last_name;
                $this->requester_dept_name = $cust->department_name;
                $this->requester_region_name = $cust->region_name;
                $this->requester_group_name = $cust->group_name;
                $this->requester_full_name = $cust->full_name;
            }
            // if ($asgn) {
            //     $this->assignee_support_company = $asgn->company;
            //     $this->assignee_support_organization = $asgn->organization;
            //     $this->assignee_support_group_name = $asgn->support_group_name;
            //     $this->assignee_support_group_id = $asgn->support_group_id;
            //     $this->assignee_name = $asgn->full_name;
            //     $this->assignee_user_id = $asgn->user != null ? $asgn->user->id : null;
            //     $this->assignee_id = $asgn->employee_remedy_id;
            //     $this->assignee_login_id = $asgn->remedy_login_id;
            // }
            $this->text_decoded = "INCIDENT#"
                . $this->requester_company . "#"
                . $this->service_type . "#"
                . $this->notes_description . "#"
                . $this->summary_description . "#"
                . $this->assignee_support_group_name . "#"
                . $this->assignee_name . "#"
                . $this->impact . "#"
                . $this->urgency . "#"
                . $this->priority . "#"
                . $this->reported_source . "#"
                . $this->assignee_support_company . "#"
                . $this->prod_cat_tier1 . "#"
                . $this->prod_cat_tier2 . "#"
                . $this->prod_cat_tier3 . "#"
                . $this->product_name . "#"
                . $this->model_version . "#"
                . $this->opr_cat_tier1 . "#"
                . $this->opr_cat_tier2 . "#"
                . $this->opr_cat_tier3 . "#"
                . $this->resolution . "#"
                . $this->service . "#"
                . $this->survey;
            return $this;
        } catch (\Exception $th) {
            return null;
        }
    }

    public function generateStatus(String $status)
    {
        $process_status = '';
        switch ($status) {
            case  "In Progress":
                $process_status = 'INPROGRESS';
                break;
            case  "Pending":
                $process_status = 'PENDING';
                break;
            case  "Assigned":
                $process_status = "QUEUED";
                break;
            case  "Resolved":
                $process_status = "CLOSED";
                break;
            case  "Cancelled":
                $process_status = "REJECT";
                break;
        }
        return $process_status;
    }

    public function generateSurvey(String $survey)
    {
        $tmp_survey = '';
        switch ($survey) {
            case  "1 - Tidak Puas":
                $tmp_survey = '1';
                break;
            case  "2 - Kurang Puas":
                $tmp_survey = '2';
                break;
            case  "3 - Cukup Puas":
                $tmp_survey = "3";
                break;
            case  "4 - Puas":
                $tmp_survey = "4";
                break;
            case  "5 - Sangat Puas":
                $tmp_survey = "5";
                break;
        }
        return $tmp_survey;
    }

    public function decodeSurvey(Int $survey)
    {
        $tmp_survey = '';
        switch ($survey) {
            case  "1":
                $tmp_survey = '1 - Tidak Puas';
                break;
            case  "2":
                $tmp_survey = '2 - Kurang Puas';
                break;
            case  "3":
                $tmp_survey = "3 - Cukup Puas";
                break;
            case  "4":
                $tmp_survey = "4 - Puas";
                break;
            case  "5":
                $tmp_survey = "5 - Sangat Puas";
                break;
        }
        return $tmp_survey;
    }

    public function generateUDH($array_property)
    {
        return (array_key_exists("requester_full_name", $array_property) ? $array_property["requester_full_name"] : null) . "#"
            . (array_key_exists("requester_first_name", $array_property) ? $array_property["requester_first_name"] : null) . "#"
            . (array_key_exists("requester_last_name", $array_property) ? $array_property["requester_last_name"] : null) . "#"
            . (array_key_exists("requester_email", $array_property) ? $array_property["requester_email"] : null) . "#"
            . (array_key_exists("requester_company", $array_property) ? $array_property["requester_company"] : null) . "#"
            . (array_key_exists("requester_region_name", $array_property) ? $array_property["requester_region_name"] : null) . "#"
            . (array_key_exists("requester_group_name", $array_property) ? $array_property["requester_group_name"] : null) . "#"
            . (array_key_exists("requester_dept_name", $array_property) ? $array_property["requester_dept_name"] : null);
    }

    public function generateUDHbyOd($array_property)
    {
        return (array_key_exists("requester_full_name", $array_property) ? $array_property["requester_full_name"] : null) . "#"
            . (array_key_exists("requester_first_name", $array_property) ? $array_property["requester_first_name"] : null) . "#"
            . (array_key_exists("requester_last_name", $array_property) ? $array_property["requester_last_name"] : null) . "#"
            . (array_key_exists("requester_email", $array_property) ? $array_property["requester_email"] : null) . "#"
            . (array_key_exists("requester_company", $array_property) ? $array_property["requester_company"] : null) . "#"
            . (array_key_exists("requester_region_name", $array_property) ? $array_property["requester_region_name"] : null) . "#"
            . (array_key_exists("requester_group_name", $array_property) ? $array_property["requester_group_name"] : null) . "#"
            . (array_key_exists("requester_dept_name", $array_property) ? $array_property["requester_dept_name"] : null);
    }

    public function parseContent(array $array_property): ?Incident
    {
        //requester
        $this->requester_person_id = (array_key_exists("requester_person_id", $array_property) ? $array_property["requester_person_id"] : null);
        $this->requester_full_name = (array_key_exists("requester_full_name", $array_property) ? $array_property["requester_full_name"] : null);
        $this->requester_first_name = (array_key_exists("requester_first_name", $array_property) ? $array_property["requester_first_name"] : null);
        $this->requester_last_name = (array_key_exists("requester_last_name", $array_property) ? $array_property["requester_last_name"] : null);
        $this->requester_email = (array_key_exists("requester_email", $array_property) ? $array_property["requester_email"] : null);
        $this->requester_company = (array_key_exists("requester_company", $array_property) ? $array_property["requester_company"] : null);
        $this->requester_region_name = (array_key_exists("requester_region_name", $array_property) ? $array_property["requester_region_name"] : null);
        $this->requester_group_name = (array_key_exists("requester_group_name", $array_property) ? $array_property["requester_group_name"] : null);
        $this->requester_dept_name = (array_key_exists("requester_dept_name", $array_property) ? $array_property["requester_dept_name"] : null);
        //assignee
        $asgn = SupportGroupAssigneeRemedy::where("company", $array_property["assignee_support_company"])
            ->where("organization", $array_property["assignee_support_organization"])
            ->where("support_group_name", $array_property["assignee_support_group_name"])
            ->where("full_name", $array_property["assignee_name"])
            ->first();
        if ($asgn) {
            $this->assignee_support_company = $asgn->company;
            $this->assignee_support_organization = $asgn->organization;
            $this->assignee_support_group_name = $asgn->support_group_name;
            $this->assignee_support_group_id = $asgn->support_group_id;
            $this->assignee_name = $asgn->full_name;
            $this->assignee_first_name = $asgn->first_name;
            $this->assignee_user_id = $asgn->user != null ? $asgn->user->id : null;
            $this->assignee_id = $asgn->employee_remedy_id;
            $this->assignee_login_id = $asgn->remedy_login_id;
        } else {
            $this->assignee_support_company = (array_key_exists("assignee_support_company", $array_property) ? $array_property["assignee_support_company"] : null);
            $this->assignee_support_organization = (array_key_exists("assignee_support_organization", $array_property) ? $array_property["assignee_support_organization"] : null);
            $this->assignee_support_group_name = (array_key_exists("assignee_support_group_name", $array_property) ? $array_property["assignee_support_group_name"] : null);            
        }
        // remedy original field
        $this->service_type = (array_key_exists("service_type", $array_property) ? $array_property["service_type"] : $this->service_type);
        $this->notes_description = (array_key_exists("notes_description", $array_property) ? $array_property["notes_description"] : null);
        $this->summary_description = (array_key_exists("summary_description", $array_property) ? $array_property["summary_description"] : null);
        $this->impact = (array_key_exists("impact", $array_property) ? $array_property["impact"] : null);
        $this->urgency = (array_key_exists("urgency", $array_property) ? $array_property["urgency"] : $this->urgency);
        $this->priority = (array_key_exists("priority", $array_property) ? $array_property["priority"] : $this->priority);
        $this->reported_source = (array_key_exists("reported_source", $array_property) ? $array_property["reported_source"] : null);
        $this->product_name = (array_key_exists("product_name", $array_property) ? $array_property["product_name"] : null);
        $this->model_version = (array_key_exists("model_version", $array_property) ? $array_property["model_version"] : null);
        $this->status = (array_key_exists("status", $array_property) ? $array_property["status"] : null);
        $this->prod_cat_tier1 = (array_key_exists("prod_cat_tier1", $array_property) ? $array_property["prod_cat_tier1"] : null);
        $this->prod_cat_tier2 = (array_key_exists("prod_cat_tier2", $array_property) ? $array_property["prod_cat_tier2"] : null);
        $this->prod_cat_tier3 = (array_key_exists("prod_cat_tier3", $array_property) ? $array_property["prod_cat_tier3"] : null);
        $this->opr_cat_tier1 = (array_key_exists("opr_cat_tier1", $array_property) ? $array_property["opr_cat_tier1"] : null);
        $this->opr_cat_tier2 = (array_key_exists("opr_cat_tier2", $array_property) ? $array_property["opr_cat_tier2"] : null);
        $this->opr_cat_tier3 = (array_key_exists("opr_cat_tier3", $array_property) ? $array_property["opr_cat_tier3"] : null);
        $this->resolution = (array_key_exists("resolution", $array_property) ? $array_property["resolution"] : null);
        $this->service = (array_key_exists("service", $array_property) ? $array_property["service"] : null);
        $this->survey = (array_key_exists("survey", $array_property) ? $array_property["survey"] : null);
        //itools imploded field
        $this->udh = $this->generateUDH($array_property);
        $this->text_decoded = $this->generateTextDecoded($array_property);
        return $this;
    }

    /* Add by 1991725227 - putut.mukti dashboard OD 19082020*/
    public function parseContentIncidentOd(array $array_property): ?Incident
    {   
        $array_property_incident = array();
        //requester
        $this->requester_person_id = (array_key_exists("id", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["id"] : null);
        $this->requester_full_name = (array_key_exists("full_name", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["full_name"] : null);
        $this->requester_first_name = (array_key_exists("first_name", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["first_name"] : null);
        $this->requester_last_name = (array_key_exists("last_name", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["last_name"] : null);
        $this->requester_email = (array_key_exists("email", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["email"] : null);
        $this->requester_company = (array_key_exists("company", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["company"] : null);
        $this->requester_region_name = (array_key_exists("region_name", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["region_name"] : null);
        $this->requester_group_name = (array_key_exists("group_name",$array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["group_name"] : null);
        $this->requester_dept_name = (array_key_exists("department_name", $array_property['requesterByRemedyUser']) ? $array_property['requesterByRemedyUser']["department_name"] : null);
        

        // assignee
        $asgn = SupportGroupAssigneeRemedy::where("company", $array_property["assignSupportCompany"])
            ->where("organization", $array_property["assignSupportOrganization"])
            ->where("support_group_name", $array_property["assignSupportGroup"])
            ->first();
        if ($asgn) {
            $this->assignee_support_company = $asgn->company;
            $this->assignee_support_organization = $asgn->organization;
            $this->assignee_support_group_name = $asgn->support_group_name;
            $this->assignee_support_group_id = $asgn->support_group_id;
        } else {
            $this->assignee_support_company = (array_key_exists("assignSupportCompany", $array_property) ? $array_property["assignSupportCompany"] : null);
            $this->assignee_support_organization = (array_key_exists("assignSupportOrganization", $array_property) ? $array_property["assignSupportOrganization"] : null);
            $this->assignee_support_group_name = (array_key_exists("assignSupportGroup", $array_property) ? $array_property["assignSupportGroup"] : null); 
            // 'SGP000000003114';    
            // $this->assignee_support_company = 'PT. BANK MANDIRI, TBK';
            // $this->assignee_support_organization = 'IT INFRASTRUCTURE GROUP';
            // $this->assignee_support_group_name = 'IT ULP - IT COMMAND CENTER';    
            // $this->assignee_support_group_id = 'SGP000000003114';        
        }
        // remedy original field
        $this->resolution = (array_key_exists("solusi", $array_property) ? $array_property["solusi"] . " - ".$array_property["updateEkskalasi"] : null);
        $this->service = (array_key_exists("service", $array_property) ? $array_property["service"] : null);
        $this->company = (array_key_exists("company", $array_property) ? $array_property["company"] : null);
        $this->incident = (array_key_exists("incident", $array_property) ? $array_property["incident"] : null);
        $this->type_incident = (array_key_exists("type_incident", $array_property) ? $array_property["type_incident"] : null);
        $this->saverity = (array_key_exists("saverity", $array_property) ? $array_property["saverity"] : null);
        $this->suspect = (array_key_exists("suspect", $array_property) ? $array_property["suspect"] : null);
        $this->estimasi = (array_key_exists("estimasi", $array_property) ? $array_property["estimasi"] : null);
        $this->status_incident = (array_key_exists("status_incident", $array_property) ? $array_property["status_incident"] : null);
        $this->impactValue = (array_key_exists("impactValue", $array_property) ? $array_property["impactValue"] : null);
        $this->reported_source = 'Officer on Duty';
        $this->ekskalasiStatus = (array_key_exists("ekskalasiStatus", $array_property) ? $array_property["ekskalasiStatus"] : null);
        $this->solusi = (array_key_exists("solusi", $array_property) ? $array_property["solusi"] : null);
        $this->durasiGangguan = (array_key_exists("durasiGangguan", $array_property) ? $array_property["durasiGangguan"] : null);
        $this->waktuTerindikasiSelected = (array_key_exists("waktuTerindikasiSelected", $array_property) ? $array_property["waktuTerindikasiSelected"] : null);
        $this->endEkskalasi = (array_key_exists("endEkskalasi", $array_property) ? $array_property["endEkskalasi"] : null);
        $this->updateEkskalasi = (array_key_exists("updateEkskalasi", $array_property) ? $array_property["updateEkskalasi"] : null);
        $this->eksternalPic = (array_key_exists("eksternalPic", $array_property) ? $array_property["eksternalPic"] : null);
        $this->remedyProcessStatus = (array_key_exists("remedyProcessStatus", $array_property) ? $array_property["remedyProcessStatus"] : null); 
        $this->pic_support_group = (array_key_exists("picSupportGroup", $array_property) ? $array_property["picSupportGroup"] : null);
        $this->pic_support_team = (array_key_exists("picSupportTeam", $array_property) ? $array_property["picSupportTeam"] : null);
        $this->pic_infrastructure = (array_key_exists("picSupportInfra", $array_property) ? $array_property["picSupportInfra"] : null);
        
       // $this->ekskalasiStatus
        //itools imploded field
        //itools original field

        $array_property_incident["requester_company"] =  $array_property["company"];
        $array_property_incident["service_type"] =  'Layanan IT';
        $array_property_incident["service"] =  $array_property["service"];
        $array_property_incident["notes_description"] =  $array_property["impact"];
        $array_property_incident["summary_description"] =  $array_property["incident"];
        $array_property_incident["assignee_support_group_name"] =  $array_property["assignSupportGroup"];
        $array_property_incident["assignee_name"] =  $array_property["assignSelected"];
        $array_property_incident["impact"] =  $array_property["impactValue"];
        $array_property_incident["urgency"] =  $array_property["saverity"];
        $array_property_incident["reported_source"] =  'Officer on Duty';
        $array_property_incident["assignee_support_company"] =  $array_property["assignSupportCompany"];

        $array_property_incident["requester_full_name"] = $array_property['requesterByRemedyUser']["full_name"];
        $array_property_incident["requester_last_name"] = $array_property['requesterByRemedyUser']["last_name"];
        $array_property_incident["requester_first_name"] = $array_property['requesterByRemedyUser']["first_name"];
        $array_property_incident["requester_email"] = $array_property['requesterByRemedyUser']["email"];
        $array_property_incident["requester_company"] = $array_property['requesterByRemedyUser']["company"];
        $array_property_incident["requester_region_name"] = $array_property['requesterByRemedyUser']["region_name"];
        $array_property_incident["requester_group_name"] = $array_property['requesterByRemedyUser']["group_name"];
        $array_property_incident["requester_dept_name"] = $array_property['requesterByRemedyUser']["department_name"];

        $this->udh = $this->generateUDHbyOd($array_property_incident);
        $this->text_decoded = $this->generateTextDecoded($array_property_incident);
        $this->incident_od_decode =  $this->generateIncidentTemplateOd($array_property);
        return $this;
    }
    /* End */

    public function updateResolution($resolution, $notes_description = null)
    {
        if($notes_description == null){
            $tamp_note = $this->notes_description;
        }else{
            $tamp_note = $notes_description;
        }
        $this->resolution = $resolution;
        $this->text_decoded = "INCIDENT#"
            . $this->requester_company . "#"
            . $this->service_type . "#"
            . $tamp_note . "#"
            . $this->summary_description . "#"
            . $this->assignee_support_group_name . "#"
            . $this->assignee_name . "#"
            . $this->impact . "#"
            . $this->urgency . "#"
            . $this->priority . "#"
            . $this->reported_source . "#"
            . $this->assignee_support_company . "#"
            . $this->prod_cat_tier1 . "#"
            . $this->prod_cat_tier2 . "#"
            . $this->prod_cat_tier3 . "#"
            . $this->product_name . "#"
            . $this->model_version . "#"
            . $this->opr_cat_tier1 . "#"
            . $this->opr_cat_tier2 . "#"
            . $this->opr_cat_tier3 . "#"
            . $resolution . "#"
            . $this->service . "#"
            . $this->survey;
        return $this;
    }

    public function generateTier(array $array_property)
    {
        $this->prod_cat_tier1 = $array_property["prod_cat_tier1"];
        $this->prod_cat_tier2 = $array_property["prod_cat_tier2"];
        $this->prod_cat_tier3 = $array_property["prod_cat_tier3"];
        $this->opr_cat_tier1 = $array_property["opr_cat_tier1"];
        $this->opr_cat_tier2 = $array_property["opr_cat_tier2"];
        $this->opr_cat_tier3 = $array_property["opr_cat_tier3"];
        $this->text_decoded = "INCIDENT#"
            . $this->requester_company . "#"
            . $this->service_type . "#"
            . $this->notes_description . "#"
            . $this->summary_description . "#"
            . $this->assignee_support_group_name . "#"
            . $this->assignee_name . "#"
            . $this->impact . "#"
            . $this->urgency . "#"
            . $this->priority . "#"
            . $this->reported_source . "#"
            . $this->assignee_support_company . "#"
            . $this->prod_cat_tier1 . "#"
            . $this->prod_cat_tier2 . "#"
            . $this->prod_cat_tier3 . "#"
            . $this->product_name . "#"
            . $this->model_version . "#"
            . $this->opr_cat_tier1 . "#"
            . $this->opr_cat_tier2 . "#"
            . $this->opr_cat_tier3 . "#"
            . $this->resolution . "#"
            . $this->service . "#"
            . $this->survey;
        return $this;
    }

    public function generateTextDecoded(array $array_property)
    {
        return "INCIDENT#"
            . (array_key_exists("requester_company", $array_property) ? $array_property["requester_company"] : null) . "#"
            . (array_key_exists("service_type", $array_property) ? $array_property["service_type"] : null) . "#"
            . (array_key_exists("notes_description", $array_property) ? $array_property["notes_description"] : null) . "#"
            . (array_key_exists("summary_description", $array_property) ? $array_property["summary_description"] : null) . "#"
            . (array_key_exists("assignee_support_group_name", $array_property) ? $array_property["assignee_support_group_name"] : null) . "#"
            . (array_key_exists("assignee_name", $array_property) ? $array_property["assignee_name"] : null) . "#"
            . (array_key_exists("impact", $array_property) ? $array_property["impact"] : null) . "#"
            . (array_key_exists("urgency", $array_property) ? $array_property["urgency"] : null) . "#"
            . (array_key_exists("priority", $array_property) ? $array_property["priority"] : null) . "#"
            . (array_key_exists("reported_source", $array_property) ? $array_property["reported_source"] : null) . "#"
            . (array_key_exists("assignee_support_company", $array_property) ? $array_property["assignee_support_company"] : null) . "#"
            . (array_key_exists("prod_cat_tier1", $array_property) ? $array_property["prod_cat_tier1"] : null) . "#"
            . (array_key_exists("prod_cat_tier2", $array_property) ? $array_property["prod_cat_tier2"] : null) . "#"
            . (array_key_exists("prod_cat_tier3", $array_property) ? $array_property["prod_cat_tier3"] : null) . "#"
            . (array_key_exists("product_name", $array_property) ? $array_property["product_name"] : null) . "#"
            . (array_key_exists("model_version", $array_property) ? $array_property["model_version"] : null) . "#"
            . (array_key_exists("opr_cat_tier1", $array_property) ? $array_property["opr_cat_tier1"] : null) . "#"
            . (array_key_exists("opr_cat_tier2", $array_property) ? $array_property["opr_cat_tier2"] : null) . "#"
            . (array_key_exists("opr_cat_tier3", $array_property) ? $array_property["opr_cat_tier3"] : null) . "#"
            . (array_key_exists("resolution", $array_property) ? $array_property["resolution"] : null) . "#"
            . (array_key_exists("service", $array_property) ? $array_property["service"] : null) . "#"
            . (array_key_exists("survey", $array_property) ? $array_property["survey"] : null);
    }

    public function generateIncidentTemplateOd(array $array_property)
    {
        
  
        $array_template_inc = array();
        $array_template_inc = $array_property;
        $array_template_inc["header"] = "INCIDENT";

        

        return json_encode($array_template_inc);
        /*
        return "INCIDENT#"
            . (array_key_exists("requester_company", $array_property) ? $array_property["requester_company"] : null) . "#"
            . (array_key_exists("service_type", $array_property) ? $array_property["service_type"] : null) . "#"
            . (array_key_exists("notes_description", $array_property) ? $array_property["notes_description"] : null) . "#"
            . (array_key_exists("summary_description", $array_property) ? $array_property["summary_description"] : null) . "#"
            . (array_key_exists("assignee_support_group_name", $array_property) ? $array_property["assignee_support_group_name"] : null) . "#"
            . (array_key_exists("assignee_name", $array_property) ? $array_property["assignee_name"] : null) . "#"
            . (array_key_exists("impact", $array_property) ? $array_property["impact"] : null) . "#"
            . (array_key_exists("urgency", $array_property) ? $array_property["urgency"] : null) . "#"
            . (array_key_exists("priority", $array_property) ? $array_property["priority"] : null) . "#"
            . (array_key_exists("reported_source", $array_property) ? $array_property["reported_source"] : null) . "#"
            . (array_key_exists("assignee_support_company", $array_property) ? $array_property["assignee_support_company"] : null) . "#"
            . (array_key_exists("prod_cat_tier1", $array_property) ? $array_property["prod_cat_tier1"] : null) . "#"
            . (array_key_exists("prod_cat_tier2", $array_property) ? $array_property["prod_cat_tier2"] : null) . "#"
            . (array_key_exists("prod_cat_tier3", $array_property) ? $array_property["prod_cat_tier3"] : null) . "#"
            . (array_key_exists("product_name", $array_property) ? $array_property["product_name"] : null) . "#"
            . (array_key_exists("model_version", $array_property) ? $array_property["model_version"] : null) . "#"
            . (array_key_exists("opr_cat_tier1", $array_property) ? $array_property["opr_cat_tier1"] : null) . "#"
            . (array_key_exists("opr_cat_tier2", $array_property) ? $array_property["opr_cat_tier2"] : null) . "#"
            . (array_key_exists("opr_cat_tier3", $array_property) ? $array_property["opr_cat_tier3"] : null) . "#"
            . (array_key_exists("resolution", $array_property) ? $array_property["resolution"] : null) . "#"
            . (array_key_exists("service", $array_property) ? $array_property["service"] : null) . "#"
            . (array_key_exists("survey", $array_property) ? $array_property["survey"] : null);*/
    }

    public function doRemedyMapper(String $content_with_delimiter, $customer, $assignee, String $status, String $resolution = null)
    {
        try {
            $ar_content = explode("#", trim($content_with_delimiter));
            $this->requester_company = $customer->company;
            $this->requester_person_id = $customer->id;
            $this->requester_first_name = $customer->first_name;
            $this->requester_last_name = $customer->last_name;
            $this->requester_dept_name = $customer->department_name;
            $this->requester_region_name = $customer->region_name;
            $this->requester_group_name = $customer->group_name;
            $this->requester_full_name = $customer->full_name;

            $this->status = $status;

            $this->impact = $ar_content[7];
            $this->urgency = $ar_content[8];
            $this->notes_description = $ar_content[3];
            $this->summary_description = $ar_content[4];
            $this->reported_source = $ar_content[10];
            $this->prod_cat_tier1 = $ar_content[12];
            $this->prod_cat_tier2 = $ar_content[13];
            $this->prod_cat_tier3 = $ar_content[14];
            $this->opr_cat_tier1 = $ar_content[17];
            $this->opr_cat_tier2 = $ar_content[18];
            $this->opr_cat_tier3 = $ar_content[19];
            $this->resolution = $resolution;
            $this->service = $ar_content[21];
            $this->survey = $ar_content[22];
            $this->assignee_support_company = $assignee->company;
            $this->assignee_support_organization = $assignee->organization;
            $this->assignee_support_group_name = $assignee->support_group_name;
            $this->assignee_support_group_id = $assignee->support_group_id;
            $this->assignee_name = $assignee->full_name;
            $this->assignee_first_name = $assignee->first_name;
            $this->assignee_user_id = $assignee->user->id;
            $this->assignee_id = $assignee->employee_remedy_id;
            $this->assignee_login_id = $assignee->remedy_login_id;

            $this->text_decoded = "INCIDENT#"
                . $this->requester_company . "#"
                . $this->service_type . "#"
                . $this->notes_description . "#"
                . $this->summary_description . "#"
                . $this->assignee_support_group_name . "#"
                . $this->assignee_name . "#"
                . $this->impact . "#"
                . $this->urgency . "#"
                . $this->priority . "#"
                . $this->reported_source . "#"
                . $this->assignee_support_company . "#"
                . $this->prod_cat_tier1 . "#"
                . $this->prod_cat_tier2 . "#"
                . $this->prod_cat_tier3 . "#"
                . $this->product_name . "#"
                . $this->model_version . "#"
                . $this->opr_cat_tier1 . "#"
                . $this->opr_cat_tier2 . "#"
                . $this->opr_cat_tier3 . "#"
                . $this->resolution . "#"
                . $this->service . "#"
                . $this->survey;

            $this->udh = $this->requester_full_name . "#"
                . $this->requester_first_name . "#"
                . $this->requester_last_name . "#"
                . $this->requester_email . "#"
                . $this->requester_company . "#"
                . $this->requester_region_name . "#"
                . $this->requester_group_name . "#"
                . $this->requester_dept_name;
            return $this;
        } catch (\Throwable $th) {
            return "Someting wrong with your input, input must align with definde format " . $th->__toString();
        }
    }
}
