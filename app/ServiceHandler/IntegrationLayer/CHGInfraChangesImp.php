<?php

namespace App\ServiceHandler\IntegrationLayer;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use Crypt;

use function GuzzleHttp\json_encode;

class CHGInfraChangesImp implements CHGInfraChangesService
{
    // private $urlWsdl = "";
    private $urlWsdl =  "https://mandiriservicedesk/arsys/WSDL/public/appremedy/MANDIRI:CSI:CHG:Integration7:InfraChangesEndPoint";
    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeaderAPI($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }
    ///////////////////////////////////////////////////////////////////////HEAD//////////////////////////////////////////////////////////////
    public function getInfraChangesList($username, $password, $start_date, $end_date)
    {
        try {
            $client = new SoapClient($this->urlWsdl, $this->options());
            $headerbody = $this->generateHeaderAPI($username,$password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'StartDate' => $start_date,
                'EndDate' => $end_date
            );
            $client->GetListInfraChanges($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            $list = $doc->getElementsByTagName("ListOfChanges");
            $data = array();
            foreach ($list as $item) {
                array_push($data,[
                    "RequestID" => $item->getElementsByTagName('RequestID')->item(0)->nodeValue,
                    "Status" => $item->getElementsByTagName('Status')->item(0)->nodeValue,
                    "Summary" => $item->getElementsByTagName('Summary')->item(0)->nodeValue,
                    "SubmitDate" => date("d/m/Y H:i:s",strtotime($item->getElementsByTagName('SubmitDate')->item(0)->nodeValue)),
                ]);
            }
            return ["status" => true, "message" => "Max record is 100, total fetched record : ".count($data),"data" => $data];        
        } catch (\Exception $ex) {
            return ["status" => false, "message" => "no data found with message :".$ex->getMessage()];
        }
    }
}
