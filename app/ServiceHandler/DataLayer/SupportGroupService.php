<?php

namespace App\ServiceHandler\DataLayer;

use App\SupportGroupRemedy as SupportGroup;

interface SupportGroupService
{
    public function findByComOrgGrp(String $company_name,String $organization_name,String $group_name) : ?SupportGroup;
    public function getSupportGroupId($company, $organization, $support_group);
}