<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RspCps extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'RSP_CPS';
}
