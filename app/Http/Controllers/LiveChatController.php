<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Resources\Chat as ChatResources;
use App\Inbox;
use App\Incident;
use App\ServiceHandler\DataLayer\ChatHistoryService;
use App\ServiceHandler\DataLayer\InboxService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\DataLayer\TelegramUserService;
use App\ServiceHandler\DataLayer\ChatService;
use App\ServiceHandler\DataLayer\ServiceCatalogService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use App\ServiceHandler\DataLayer\SupportGroupService;
use App\ServiceHandler\NotificationLayer\NotificationService;
use Auth;

class LiveChatController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $inbox_ds;
    protected $suppusr_ds;
    protected $chat_ds;
    protected $chathis_ds;
    protected $incupdate_wsdl;
    protected $inccreate_wsdl;
    protected $tlguser_ds;
    protected $suppgrp_ds;
    protected $srvctlg_ds;
    protected $notif_ds;

    public function __construct(
        InboxService $inbox_service,
        SupportUserService $support_user_service,
        ChatService $lc_service,
        WOIIncidentUpdateService $incident_update_service,
        WOIIncidentService $incident_create_service,
        TelegramUserService $telegram_user_service,
        ChatHistoryService $chat_history_service,
        SupportGroupService $support_group_service,
        ServiceCatalogService $service_catalog_service,
        NotificationService $notif_service
    ) {
        ChatResources::withoutWrapping();
        $this->notif_ds = $notif_service;
        $this->srvctlg_ds = $service_catalog_service;
        $this->suppgrp_ds = $support_group_service;
        $this->inccreate_wsdl = $incident_create_service;
        $this->chathis_ds = $chat_history_service;
        $this->inbox_ds = $inbox_service;
        $this->tlguser_ds = $telegram_user_service;
        $this->suppusr_ds = $support_user_service;
        $this->chat_ds = $lc_service;
        $this->incupdate_wsdl = $incident_update_service;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private function convertServiceCatalog(Inbox $inbox)
    {
        $incident = new Incident();
        $incident->parseInbox($inbox);
        switch ($inbox->ServiceCatalogId) {
            case 2:
                $sc = $this->srvctlg_ds->findBySummaryAndTier($incident->summary_description, $incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                return ($sc ? $sc->id : 50);
                break;
            case 3:
                $sc = $this->srvctlg_ds->findBySummaryAndTier($incident->summary_description, $incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                return ($sc ? $sc->id : 38);
                break;
            case 4:
                $sc = $this->srvctlg_ds->findBySummaryAndTier($incident->summary_description, $incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                return ($sc ? $sc->id : 51);
                break;
            default:
                switch ($inbox->servicecatalog->servicechannel->service_type_name) {
                    case 'LAYANAN_IT':
                        return 3;
                        break;
                    case 'LAYANAN_CABANG':
                        return 2;
                        break;
                    case 'LAYANAN_CC':
                        return 4;
                        break;
                    default:
                        return $inbox->ServiceCatalogId;
                        break;
                }
                break;
        }
    }

    public function getChat($request_id)
    {
        $inc = new Incident();
        $inbox = $this->inbox_ds->find($request_id);
        $chatlog = $this->chat_ds->getListDTOByRequestId($request_id);
        return response()->json(["status" => true, "message" => "Related incident is in progress", "responsedata" => ['chatdata' => $chatlog, 'inbox' => $inbox, 'incident' => $inc->parseInbox($inbox)], "processStatus" => $inbox->ProcessStatus, "serviceCatalogId" => $inbox->ServiceCatalogId]);
    }

    public function initChat($request_id)
    {
        $inc = new Incident();
        $inbox = $this->inbox_ds->find($request_id);
        $assignee = $this->suppusr_ds->findByPersonIdGroupId(Auth::user()->employee_remedy_id, Auth::user()->sgn_id_remedy);
        $inc->parseInbox($this->inbox_ds->update($inbox->id, array("ProcessStatus" => "INPROGRESS", "AssigneeId" => Auth::user()->id, "EmployeeAssigneeId" => $assignee->employee_remedy_id, "EmployeeAssigneeGroupId" => $assignee->support_group_id)));
        if ($inbox->RemedyTicketId === 'N/A') {
            $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($inc->service);
            $wsdl = $this->inccreate_wsdl->createIncidentGeneric($inc, $wsdl_reconID->getContent());
            $reply = "Saat ini kamu terhubung dengan " . Auth::user()->fn_remedy . " Agent Mandiri Servicedesk.";
        } else {
            $inc->notes_description = $this->chathis_ds->getHistory($inbox->id)['content_summary'];
            $inc->status = 'Assigned';
            $this->tlguser_ds->updateToChat($inbox);
            $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($inc);
            $reply = "Saat ini kamu kembali terhubung dengan " . Auth::user()->fn_remedy . " Agent Mandiri Servicedesk.";
        }
        $request = $this->inbox_ds->refresh($inbox);
        $inc->parseInbox($request);
        if ($wsdl->getResponseStatus() == 'success') {
            $this->inbox_ds->update($inbox->id, array("TicketCreationDateTime" => date('Y-m-d H:i:s')));
            $this->notif_ds->chatFromAgent($inbox, $reply . " Nomor penanganan kamu adalah : " . $wsdl->getContent());
            $chatlog = $this->chat_ds->getListDTOByRequestId($request_id);
            $message = "Ref ID : " . $inbox->id . " - " . $wsdl->getContent() . " created and assigned to your task";
        } else {
            $this->inbox_ds->update($inbox->id, array("ActionTicketCreation" => "IN_PENDING_PRS", "TextDecoded" => $inc->text_decoded));
            $this->notif_ds->chatFromAgent($inbox, $reply);
            $chatlog = $this->chat_ds->getListDTOByRequestId($request_id);
            $message = "Ref ID : " . $inbox->id . " assigned to your task, but failed to create remedy ticket, please retry process from Pending Ticket Remedy!";
        }
        $inbox = $this->inbox_ds->refresh($inbox);
        return response()->json(["status" => $wsdl->getResponseStatus() == 'success' ? true : false, "message" => $message, "responsedata" => ['chatdata' => $chatlog, 'inbox' => $inbox], "processStatus" => $inbox->ProcessStatus, "serviceCatalogId" => $inbox->ServiceCatalogId, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $inc, 'wsdl_request' => $wsdl->getArrContent()]);
    }

    public function postMessageFromAgent()
    {
        $request = $this->inbox_ds->find(request()->get('requestId'));
        return $this->notif_ds->chatFromAgent($request, request()->get('content'));
    }

    public function postMessageFromCustomer()
    {
        return $this->chat_ds->findDTO(request()->get('id'));
    }

    public function rejectTaskLiveChat()
    {
        try {
            $request = $this->inbox_ds->find(request()->get('id'));
            if ($request->RemedyTicketId === 'N/A') {
                return response()->json(["status" => false, 'message' => 'Remedy ticket not created yet, you have to retry previous process!']);
            } else {
                $content = 'Permintaanmu diabaikan, karena indikasi spam atau karena konten permintaan tidak sesuai. Kamu tetap dapat terhubung dengan mandiri servicedesk melalui channel lainnya';
                $inc = new Incident();
                $inc->parseInbox($request);
                $inc->resolution = $content;
                $inc->notes_description = $this->chathis_ds->getHistory($request->id)['content_summary'];
                $inc->status = "Cancelled";
                $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($inc);
                $request2 = $this->inbox_ds->refresh($request);
                $inc->parseInbox($request2);
                if ($wsdl->getResponseStatus() == 'success') {
                    $this->notif_ds->chatFromAgent($request, $content);
                    $this->notif_ds->dismissLiveChat($request);
                    return response()->json(["status" => true, "message" => "Ref: " . $request->id . " - " . $request->RemedyTicketId . " has been Cancelled", 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $inc, 'wsdl_request' => $wsdl->getArrContent()]);
                } else {
                    $this->notif_ds->chatFromAgent($request, $content);
                    $this->notif_ds->dismissLiveChat($request);
                    $this->inbox_ds->update($request->id, array("ProcessStatus" => 'CLOSED', "ActionTicketCreation" => 'IN_PENDING_CLS'));
                    $request->refresh();
                    return response()->json(["status" => true, "message" => "Unable to close Ticket with Ref: " . $request->id . " - " . $request->RemedyTicketId . ", retry to close from pending ticket remedy", 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $inc, 'wsdl_request' => $wsdl->getArrContent()]);
                }
            }
        } catch (\Exception $ex) {
            return response()->json(["status" => false, 'message' => 'Fatal Error : Contact Admin, message: ' . $ex->__toString()]);
        }
    }

    public function getSummaryChatHistory($request_id)
    {
        $data = $this->chathis_ds->getHistory($request_id);
        return response()->json(['results' => $data]);
    }

    public function redirectToLivechat()
    {
        $request = $this->inbox_ds->find(request()->get('id'));
        if ($this->tlguser_ds->hasActiveChat($request->EmployeeSenderId)) {
            return response()->json(["status" => false, "message" => "You were unable to redirect this ticket to livechat, due customer has active livechat session."]);
        } else {
            $this->tlguser_ds->updateToChat($request);
            $this->inbox_ds->update($request->id, array("RecipientID" => "TELEGRAM", "AssigneeId" => Auth::user()->id, "ServiceCatalogId" => $this->convertServiceCatalog($request)));
            $this->notif_ds->chatFromAgent($request, $request->supportgroupassigneeremedy->first_name . " telah terhubung melalui livechat untuk melakukan verifikasi atas permintaan anda dengan nomor : " . $request->id);
            $chatlog = $this->chat_ds->getListDTOByRequestId($request->id);
            return response()->json(["status" => true, "message" => "This ticket has been redirected to livechat, you were online.", "responsedata" => ['chatdata' => $chatlog, 'inbox' => $request], "processStatus" => $request->ProcessStatus, "serviceCatalogId" => $request->ServiceCatalogId, 'wsdlStatus' => true]);
        }
    }

    public function dismissFromLivechat()
    {
        $request = $this->inbox_ds->find(request()->get('id'));
        $this->tlguser_ds->updateToMenu($request);
        $this->inbox_ds->update($request->id, array("RecipientID" => 'TELEGRAM', "ServiceCatalogId" => $this->convertServiceCatalog($request)));
        $this->notif_ds->chatFromAgent($request, $request->supportgroupassigneeremedy->first_name . " sudah meninggalkan chat, dan sedang menindaklanjuti permintaan kamu, setelah pesan ini muncul kamu kembali ke mode menu dan pesan tidak akan dikirim ke agent");
        return response()->json(["status" => true, "message" => "Your livechat session has been closed, you may open the ticket from working task"]);
    }

    public function ticketClosureLiveChat()
    {
        $request = $this->inbox_ds->find(request()->get('id'));
        $incident = new Incident();
        $incident->parseInbox($request);
        $incident->notes_description = $this->chathis_ds->getHistory($request->id)['content_summary'];
        return response()->json(["inbox" => $request, "incident" => $incident]);
    }

    public function updateIncidentLiveChat()
    {
        $incident = new Incident();
        $content = request()->get('content');
        $request = $this->inbox_ds->find(request()->get('id'));

        $tamp_RemedyTicketId = $request->RemedyTicketId;
        $tamp_ServiceCatalogId = $request->ServiceCatalogId;

        $status = $incident->generateStatus($content['status']);
        $text_decoded = $incident->generateTextDecoded($content);
        $assignee = $this->suppusr_ds->findByCompanyOrgGroupName($content['assignee_support_company'], $content['assignee_support_organization'], $content['assignee_support_group_name'], $content['assignee_name']);
        if ($assignee) {
            $array_inbox = array(
                "AssigneeId" => Auth::user()->id,
                "EmployeeAssigneeId" => $assignee->employee_remedy_id,
                "EmployeeAssigneeGroupId" => $assignee->support_group_id,
                "TextDecoded" => $text_decoded,
                "ProcessStatus" => $status,
                "RecipientID" => 'LIVECHAT',
                "ServiceCatalogId" => $tamp_RemedyTicketId == 'N/A' ? $tamp_ServiceCatalogId : $this->convertServiceCatalog($request),
            );
            $incident->parseInbox($this->inbox_ds->update($request->id, $array_inbox));
        } else {
            $array_inbox = array(
                "AssigneeId" => Auth::user()->id,
                "EmployeeAssigneeId" => null,
                "EmployeeAssigneeGroupId" => null,
                "TextDecoded" => $text_decoded,
                "ProcessStatus" => $status,
                "RecipientID" => 'LIVECHAT',
                "ServiceCatalogId" => $tamp_RemedyTicketId == 'N/A' ? $tamp_ServiceCatalogId : $this->convertServiceCatalog($request),
            );
            $incident->parseInbox($this->inbox_ds->update($request->id, $array_inbox));
            $group = $this->suppgrp_ds->findByComOrgGrp($content['assignee_support_company'], $content['assignee_support_organization'], $content['assignee_support_group_name']);
            $incident->assignee_support_company = $group->company;
            $incident->assignee_support_organization = $group->organization;
            $incident->assignee_support_group_name = $group->support_group_name;
            $incident->assignee_support_group_id = $group->id;
        }

        if ($request->RemedyTicketId === 'N/A') {
            return response()->json(["status" => false, 'message' => 'Remedy ticket not created yet, you have to retry previous process!']);
        } else {
            $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($incident->service);
            $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident, $wsdl_reconID->getContent());
            $this->notif_ds->dismissLiveChat($request);
            if ($wsdl->getResponseStatus() == 'success') {
                $this->notif_ds->afterClosureFeedback($this->inbox_ds->refresh($request), $incident);
                $message = "Ticket with Ref No : " . $request->id . " - " . $wsdl->getContent() . " has been updated";
            } else {
                $this->inbox_ds->update($request->id, array("ProcessStatus" => $status, "ActionTicketCreation" => 'IN_PENDING_PRS'));
                $this->notif_ds->afterClosureFeedback($this->inbox_ds->refresh($request), $incident);
                $message = "Ticket with Ref No : " . $request->id . " has been updated, but failed to synce with remedy, please retry from Pending Ticket Remedy";
            }
            return response()->json(["status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, "incident" => $incident, 'wsdl_request' => $wsdl->getArrContent()]);
        }
    }
}