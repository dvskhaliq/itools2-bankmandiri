import Vuex from "vuex";
import Vue from "vue";
import ENUM from "./enum";
import systemavailability from "./modules/system_availability";
import mastersystem from "./modules/master_system_avail";
import responsetime from "./modules/ResponseTime/response_time";
import dashboardit from "./modules/dashboard_it";
import successrate from "./modules/success_rate";
import userconfig from "./modules/user_config";
import pmroles from "./modules/pm_roles";
import pmfeatures from "./modules/pm_features";
import master_apdy_bridge from "./modules/master_apdy_bridge";
import restimeapdy from "./modules/ResponseTime/restime_apdy";
import restimecommon from "./modules/ResponseTime/restime_common";
import incidentproblem from "./modules/Bulk/incident_problem";
import migratelogin from "./modules/Bulk/migrate_login";
import knowledge from "./modules/Bulk/knowledge";
import update_incident from "./modules/Bulk/update_incident";
import create_incident from "./modules/Bulk/create_incident";
import update_problem from "./modules/Bulk/update_problem";
import create_relation from "./modules/Bulk/create_relation";

Vue.use(Vuex); // Load Vuex to Vue

export default new Vuex.Store({
    state: {
        layout: {},
        layoutForAppBar: {},
        layoutProperty: [],
        usermenu: [],
        apiState: ENUM.INIT,
        exceptionMessage: ""
    },
    actions: {
        changeLayoutFromAppBar({ commit }, current ) {                
            commit("setLayoutForAppBar", current);
        },
        changeLayout({ commit, state }, current) {
            let list = state.usermenu.flatMap(item =>
                !item.children ? item : item.children
            );
            let payload = list.find(item => item.id == current);
            commit("setLayout", payload);
            commit("setLayoutProperty", payload);
        },
        async renderUserMenu({ commit }) {
            commit("setApiState", ENUM.LOADING);
            const response = await axios.get("/data_management/problems/menu");
            response.data.status
                ? commit("setUserMenu", response.data.menu)
                : commit("setException", "Something Went Wrong");
        },
    },
    mutations: {
        setApiState: (state, enumState) => (state.apiState = enumState),
        setException: (state, message) => {
            state.exceptionMessage = message;
            state.apiState = ENUM.ERROR;
        },
        setLayout: (state, payload) => {
            state.layout = payload;
        },
        setLayoutForAppBar: (state, payload) => {
            state.layoutForAppBar = payload;
        },
        setUserMenu: (state, menu) => {
            state.usermenu = menu;
            state.apiState = ENUM.LOADED;
        },
        setLayoutProperty: (state, payload) => {
            if (payload.group == payload.text) {
                state.layoutProperty = [
                    {
                        text: payload.text.toUpperCase()
                    }
                ];
            } else {
                state.layoutProperty = [
                    {
                        text: payload.group.toUpperCase()
                    },
                    {
                        text: payload.text.toUpperCase()
                    }
                ];
            }
        }
    },
    getters: {
        getApiState: state => state.apiState,
        getLayout: state => state.layout,
        getLayoutForAppBar: state => state.layoutForAppBar,
        getLayoutProperty: state => state.layoutProperty,
        getUserMenu: state => state.usermenu,
    },
    modules: {
        systemavailability,
        mastersystem,
        responsetime,
        dashboardit,
        successrate,
        userconfig,
        pmroles,
        pmfeatures,
        master_apdy_bridge,
        restimeapdy,
        restimecommon,
        incidentproblem,
        migratelogin,
        knowledge,
        update_incident,
        create_incident,
        update_problem,
        create_relation
    }
});
