<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use View;

class BDSEJ2Controller extends BaseController
{
    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return View::make('bds_connection/bds_ej2/ej2');
    }

    public function getList()
    {
        $data = $this->bdsServerService->initBdsConnection()->table('EJ2')->orderBy('TellerID', 'asc')->get();
        $arr_data=array();
        foreach ($data as $key => $value) {
                $prop['TellerID'] = $value['TellerID'];
                $prop['TellerSeqNo'] = $value['TellerSeqNo'];
                $prop['VeriDigit'] = $value['VeriDigit'];
                $prop['TrialNo'] = $value['TrialNo'];
                $prop['Verified'] = $value['Verified'];
                $prop['Trancode'] = $value['Trancode'];
            array_push($arr_data, [
                'sequence' => $key+1,
                'TellerID' => $prop['TellerID'],
                'TellerSeqNo' => $prop['TellerSeqNo'],
                'VeriDigit' => $prop['VeriDigit'],
                'TrialNo' => $prop['TrialNo'],
                'Verified' => $prop['Verified'],
                'Trancode' => $prop['Trancode']
                ]);
        }
        return response()->json($arr_data);
    }

    public function submitEJ2(Request $request)
    {
        try{
            $details = collect($request->input('details'));
            $details->map(function ($item){
                $detail = [
                    'TellerID' => $item['TellerID'],
                    'TellerSeqNo' => $item['TellerSeqNo'],
                    'Trancode' => $item['Trancode'],
                    'TrialNo' => $item['TrialNo'],
                    'VeriDigit' => $item['VeriDigit'],
                    'Verified' => $item['Verified']
                ];
                return $this->updateVerified($detail);
            });
            $data = $this->getList();
            return response()->json([
                'status' => true,
                'data' => $data->original
            ], 200);
        }catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'data' => $ex->__toString()
            ], 500);
        }
    }

    public function updateVerified($payload){
        $data = array(
            'Verified' =>  'Y'
        );
        $whereUpdate = array(
            'TellerID' =>  $payload['TellerID'],
            'TellerSeqNo' =>  $payload['TellerSeqNo'],
        );
        if ($payload['Verified'] == "N") {
            $this->bdsServerService->initBdsConnection()->table('EJ2')->where($whereUpdate)->update($data);
            return true;
        }
    }

    public function findList()
    {
        if (request()->get('search') == "null"  || request()->get('search') == "") {
            return $this->getList();
        } else {
            $result = $this->bdsServerService->initBdsConnection()->table('EJ2')->where('TellerID', request()->get('search'))->orderBy('TellerID', 'asc')->get();
            return response()->json($result);
        }
    }
}
