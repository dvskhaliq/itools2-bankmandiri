export default{
    INIT:0,
    LOADING:1,
    LOADED:2,
    ERROR:3,
    SUCCESS:4,
    INFO:5,
    WARNING:6
}