<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\DataLayer\RemedyCategorizationService;
use App\ServiceHandler\DataLayer\ServiceCIService;

class RemedyCategorizationController extends BaseController
{

    protected $RemedyCategorizationService;
    protected $ServiceCIService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RemedyCategorizationService $RemedyCategorization, ServiceCIService $ServiceCI)
    {
        $this->RemedyCategorizationService = $RemedyCategorization;
        $this->ServiceCIService = $ServiceCI;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getProductCategoryTier1()
    {

        $result = $this->RemedyCategorizationService->getProductCategoryTier1();
        return response()->json([
            "result" => $result
        ]);
    }


    public function getProductCategoryTier2()
    {
        $result = $this->RemedyCategorizationService->getProductCategoryTier2(request()->get('productCategoryTier1'));
        return response()->json([
            "result" => $result
        ]);
    }

    public function getProductCategoryTier3()
    {
        $result = $this->RemedyCategorizationService->getProductCategoryTier3(request()->get('productCategoryTier1'),request()->get('productCategoryTier2'));
        return response()->json([
            "result" => $result
        ]);
    }

    public function getOprationalCategoryTier1()
    {
        $result = $this->RemedyCategorizationService->getOprationalCategoryTier1(request()->get('productCategoryTier1'), request()->get('productCategoryTier2'), request()->get('productCategoryTier3'));
        return response()->json([
            "result" => $result
        ]);
    }

    public function getOprationalCategoryTier2()
    {
        $result = $this->RemedyCategorizationService->getOprationalCategoryTier2(request()->get('productCategoryTier1'), request()->get('productCategoryTier2'), request()->get('productCategoryTier3'), request()->get('operationalCategoryTier1'));
        return response()->json([
            "result" => $result
        ]);
    }

    public function getOprationalCategoryTier3()
    {
        $result = $this->RemedyCategorizationService->getOprationalCategoryTier3(request()->get('productCategoryTier1'), request()->get('productCategoryTier2'), request()->get('productCategoryTier3'), request()->get('operationalCategoryTier1'), request()->get('operationalCategoryTier2'));
        return response()->json([
            "result" => $result
        ]);
    }

    public function getServices()
    {

        $result = $this->ServiceCIService->getServices();
        return response()->json([
            "result" => $result
        ]);
    }

    public function searchServiceCatalog()
    {
        return $this->RemedyCategorizationService->searchServiceCatalog(request()->get('name'));
    }
}