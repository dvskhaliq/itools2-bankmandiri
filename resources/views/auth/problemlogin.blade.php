<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
<title>Data Management</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" id="theme" href="{{asset('css/theme-white.css')}}" />
	<script type="text/javascript" src="{{asset('assets/js/plugins/jquery/jquery.min1.js')}}"></script>
	<script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	<script type="text/javascript">
    	$(window).on('load', function() { // makes sure the whole site is loaded 
            $('#status').fadeOut(); // will first fade out the loading animation 
            $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website. 
            $('body').delay(100).css({'overflow':'visible'});
        })
    </script>
</head>
<body>
	<div class="login-container">
		<div class="login-box animated fadeInDown">
			<div class="login-body">
				<div class="login-title">
					<div class="row">
						<div class="col-lg-2">
							<img src="{{asset('/img/settings-icon.png')}}"
                        	style="margin-top: 5px" width="40px" height="40px">
						</div>
						<div class="col-lg-10" style="margin-top:10px"><h2>Data Management</h2></div>
					</div>
				</div>
				<form method="POST" class="form-horizontal" class="needs-validation" novalidate action="/data_management/login">
                        @csrf
					<div class="form-group">
						<div class="col-md-12">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email" autofocus>

                            @if ($errors->has('email'))
                                <div class="valid-feedback text-danger">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
						
                            @if ($errors->has('password'))
                                <div class="valid-feedback text-danger">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<button class="btn btn-info btn-block" type="submit">Login</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	@include('scriptlib')
</body>
</html>