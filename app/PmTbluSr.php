<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmTbluSr extends Model
{
    protected $table = 'pm_tblu_sr';
    public $timestamps = false;
}
