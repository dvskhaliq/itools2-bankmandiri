import '../../bootstrap';
import 'es6-promise/auto';
import 'vuetify/dist/vuetify.min.css';
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from 'vuex';
import Vue from 'vue';
import Loading from "vue-loading-overlay";
import Vuetify from 'vuetify';

window.Vue = Vue;
window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('table-bds-ej2', ()=> import('../../components/bds_connection/ej2/BDSEJ2.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage3.vue'));
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Loading);

const vuetify = new Vuetify();

const bds_ej2 = new Vue({
    el: '#bds_ej2',
    vuetify,
    data: {
        ismodal: false,
    },

});
