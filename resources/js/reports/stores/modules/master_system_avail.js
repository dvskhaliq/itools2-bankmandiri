import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            system_name: "",
        }
    };
};

const state = {
    mastersystems: [],
    simpleHeader: [
        { text: "System Name", value: "system_name" },
        { text: "Action", value: "action", sortable: false }
    ],
    editedIndex: defaultItem().index,
    editedItem: defaultItem().item,
    apiState: ENUM.INIT,
    alertMessage: "",
};

const getters = {
    getMasterSystems: state => state.mastersystems,
    getSimpleHeader: state => state.simpleHeader,
    getEditItem: state => state.editedItem,
    getEditIndex: state => state.editedIndex,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
};

const actions = {
    async initMasterSystem({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/data_management/problems/master_system/fetch");
          commit("setMasterSystems", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async addMasterSystem({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/master_system/create", payload);
            commit("newMasterSystem", response.data.data);
            commit("setSuccess", "Data has been successfully saved !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Adding data has failed"
                  );
        }
    },
    async updateMasterSystem({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.put("/data_management/problems/master_system/update", payload);
            commit("updateMasterSystem", response.data.data);
            commit("setSuccess", "Data has been successfully updated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Updating data has failed"
                  );
        }
    },
    async deleteMasterSystem({ commit }, id) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.delete("/data_management/problems/master_system/delete/" + id);
            commit("deleteMasterSystem", response.data.data);
            commit("setSuccess", "Data has been successfully deleted !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Deleting data has failed"
                  );
        }
    },

    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
    resetMasterSystems({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setMasterSystems", []);
    }
};

const mutations = {
    newMasterSystem: (state, mastersystem) => {
        state.apiState = ENUM.LOADED;
        state.mastersystems.unshift(mastersystem);
    },
    deleteMasterSystem: (state, deletedMasterSystem) => {
        state.apiState = ENUM.LOADED;
        const index = state.mastersystems.findIndex(mastersystem => mastersystem.id == deletedMasterSystem.id);
        state.mastersystems.splice(index, 1);
    },
    updateMasterSystem: (state, updatedMasterSystem) => {
        state.apiState = ENUM.LOADED;
        const index = state.mastersystems.findIndex(mastersystem => mastersystem.id == updatedMasterSystem.id);
        state.mastersystems.splice(index, 1, updatedMasterSystem);
    },
    setEditItem: (state, payload) => {
        state.editedItem = payload;
    },
    setEditIndex: (state, index) => {
        state.editedIndex = index;
    },
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setMasterSystems: (state, mastersystem) => {
        state.apiState = ENUM.LOADED;
        state.mastersystems = mastersystem;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
