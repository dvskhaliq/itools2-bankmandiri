<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ServiceHandler\PBMLayer\Bulk\IncidentProblemService;
use App\BulkStatus;
use App\PmCreateRelation;
use App\WebServiceResponse;
use Auth;
use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class CreateRelation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $inc_prb_repo;
    protected $details;
    protected $wsdl_update_inc;
    protected $wsdl_update_problem;
    protected $wsdl_create_inc;
    protected $wsdl_problem_relation;
    protected $auth;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        IncidentProblemService $inc_prb_service, 
        $details,
        $auth
        )
    {
        $this->inc_prb_repo = $inc_prb_service;
        $this->details = $details;
        $this->auth = $auth;
        $this->wsdl_update_inc = env('WSDL_UPDATE_INC');
        $this->wsdl_update_problem = env('WSDL_UPDATE_PROBLEM');
        $this->wsdl_create_inc = env('WSDL_CREATE_INC');
        $this->wsdl_problem_relation = env('WSDL_PROBLEM_RELATION');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public $tries = 3;

    private function getWsdlUpdateInc(){
        return $this->wsdl_update_inc;
    }

    private function getWsdlUpdateProblem(){
        return $this->wsdl_update_problem;
    }

    private function getWsdlCreateInc(){
        return $this->wsdl_create_inc;
    }

    public function getWsdlProblemRelation(){
        return $this->wsdl_problem_relation;
    }

    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar($this->auth->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt($this->auth->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    public function handle(IncidentProblemService $inc_prb_service)
    {
        $bulkstatus = BulkStatus::where('name', 'create_relation')->first();
        $bulkstatus->is_processing = true;
        $bulkstatus->save();
        foreach ($this->details as $value) {
            $check_incident = $this->chekingIncident((array_key_exists("IncidentNumber", $value) ? $value["IncidentNumber"] : null));
            $check_problem = $this->chekingProblem((array_key_exists("ProblemNumber", $value) ? $value["ProblemNumber"] : null));

            $detail = [
                'IncidentNumber' => (array_key_exists("IncidentNumber", $value) ? $value["IncidentNumber"] : null),
                'ProblemNumber' => (array_key_exists("ProblemNumber", $value) ? $value["ProblemNumber"] : null),
                'Description' => $check_incident->getResponseStatus() == 'success' ? $check_incident->getContent() : $check_incident->getResponseMessage()
            ];

            $is_processing = BulkStatus::where('name', 'create_relation')->pluck('is_processing')->first();
            if($is_processing == true){
                $check_incident = $this->chekingIncident($detail['IncidentNumber']);
                $check_problem = $this->chekingProblem($detail['ProblemNumber']);
                if(strpos($check_incident->getResponseMessage(),"ARERR [623] Authentication failed")!==false){
                    $data = new PmCreateRelation();
                    $data->incident_number = $check_incident->getResponseMessage();
                    $data->problem_number = $check_problem->getResponseMessage();
                    $data->description = $detail['Description'];
                    $data->incident_relation = null;
                    $data->problem_relation = null;
                    $data->status = false;
                    $data->save();

                    $bulkstatus = BulkStatus::where('name', 'create_relation')->first();
                    $bulkstatus->is_processing = false;
                    $bulkstatus->save();
                    break;
                }else{
                    if ($check_incident->getResponseStatus() == 'success' && $check_problem->getResponseStatus() == 'success') {
                        $wsdl_incident = $this->createIncidentRelation($detail);
                        if ($wsdl_incident->getResponseStatus() == 'success'){
                            $wsdl_problem = $this->createProblemRelation($detail);
                            if($wsdl_problem->getResponseStatus() == 'success'){
                                $data = new PmCreateRelation();
                                $data->incident_number = $detail['IncidentNumber'];
                                $data->problem_number = $detail['ProblemNumber'];
                                $data->description = $detail['Description'];
                                $data->incident_relation = $wsdl_incident->getContent();
                                $data->problem_relation = $wsdl_problem->getContent();
                                $data->status = true;
                                $data->save();
                            }else{
                                $data = new PmCreateRelation();
                                $data->incident_number = $detail['IncidentNumber'];
                                $data->problem_number = $detail['ProblemNumber'];
                                $data->description = $detail['Description'];
                                $data->incident_relation = $wsdl_incident->getContent();
                                $data->problem_relation = $wsdl_problem->getResponseMessage();
                                $data->status = false;
                                $data->save();
                            }
                        }else{
                            $wsdl_problem = $this->createProblemRelation($detail);
                            if($wsdl_problem->getResponseStatus() == 'success'){
                                $data = new PmCreateRelation();
                                $data->incident_number = $detail['IncidentNumber'];
                                $data->problem_number = $detail['ProblemNumber'];
                                $data->description = $detail['Description'];
                                $data->incident_relation = $wsdl_incident->getResponseMessage();
                                $data->problem_relation = $wsdl_problem->getContent();
                                $data->status = false;
                                $data->save();
                            }else{
                                $data = new PmCreateRelation();
                                $data->incident_number = $detail['IncidentNumber'];
                                $data->problem_number = $detail['ProblemNumber'];
                                $data->description = $detail['Description'];
                                $data->incident_relation = $wsdl_incident->getResponseMessage();
                                $data->problem_relation = $wsdl_problem->getResponseMessage();
                                $data->status = false;
                                $data->save();
                            }
                        }
                    }else{
                        $data = new PmCreateRelation();
                        $data->incident_number = $check_incident->getResponseStatus() == 'success' ? $detail['IncidentNumber'] : $check_incident->getResponseMessage();
                        $data->problem_number = $check_problem->getResponseStatus() == 'success' ? $detail['ProblemNumber'] : $check_problem->getResponseMessage();
                        $data->description = $detail['Description'];
                        $data->incident_relation = null;
                        $data->problem_relation = null;
                        $data->status = false;
                        $data->save();
                    }
                }
            }else{
                $bulkstatus = BulkStatus::where('name', 'create_relation')->first();
                $bulkstatus->is_processing = false;
                $bulkstatus->save();
                break;
            }
        }
    }
}
