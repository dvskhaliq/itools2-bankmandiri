<?php

namespace App\ServiceHandler\DataLayer;

use App\RemedyHPDIncidentInterface as Incident;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class RemedyHPDIncidentInterfaceImp implements RemedyHPDIncidentInterfaceService
{
    protected $incident_repo;

    public function __construct(HPDIncidentInterfaceService $incident_service)
    {
        $this->incident_repo = $incident_service;
    }

    public function convertUTF($content)
    {
        if (!mb_check_encoding($content, 'UTF-8') || ($content === mb_convert_encoding(mb_convert_encoding($content, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32'))) {
            $content = mb_convert_encoding($content, 'UTF-8');
        }
        return $content;
    }

    public function generateSurvey(String $survey)
    {
        $tmp_survey = '';
        switch ($survey) {
            case  "Tidak Puas":
                $tmp_survey = 0;
                break;
            case  "Kurang Puas":
                $tmp_survey = 1;
                break;
            case  "Cukup Puas":
                $tmp_survey = 2;
                break;
            case  "Puas":
                $tmp_survey = 3;
                break;
            case  "Sangat Puas":
                $tmp_survey = 4;
                break;
        }
        return $tmp_survey;
    }

    public function generateSource(String $source)
    {
        $tmp_source = '';
        switch ($source) {
            case "Call":
                $tmp_source = 6000;
                break;
            case "Email":
                $tmp_source = 2000;
                break;
            case "WA":
                $tmp_source = 11004;
                break;
            case "Telegram":
                $tmp_source = 11007;
                break;
            case "Other":
                $tmp_source = 4200;
                break;
        }
        return $tmp_source;
    }

    public function generateServiceType($serviceType)
    {
        $service_type = '';
        switch ($serviceType) {
            case 0:
                $service_type = 'User Service Restoration';
                break;
            case 1:
                $service_type = 'User Service Request';
                break;
            case 2:
                $service_type = 'Infrastructure Restoration';
                break;
            case 3:
                $service_type = 'Infrastructure Event';
                break;
        }
        return $service_type;
    }

    public function generateReportedSource($ReportedSource)
    {
        $reported_source = '';
        switch ($ReportedSource) {
            case 2000:
                $reported_source = 'EMAIL';
                break;
            case 6000:
                $reported_source = 'CALL';
                break;
            case 11004:
                $reported_source = 'WA';
                break;
            case 11007:
                $reported_source = 'TELEGRAM';
                break;
            case 4200:
                $reported_source = 'ITSRM';
                break;
        }
        return $reported_source;
    }

    public function generateImpact($Impact)
    {
        $impact = '';
        switch ($Impact) {
            case 1000:
                $impact = '1-Extensive/Widespread';
                break;
            case 2000:
                $impact = '2-Significant/Large';
                break;
            case 3000:
                $impact = '3-Moderate/Limited';
                break;
            case 4000:
                $impact = '4-Minor/Localized';
                break;
        }
        return $impact;
    }

    public function generateStatus($Status)
    {
        $status = '';
        switch ($Status) {
            case 0:
                $status = 'New';
                break;
            case 1:
                $status = 'Assigned';
                break;
            case 2:
                $status = 'In Progress';
                break;
            case 3:
                $status = 'Pending';
                break;
            case 4:
                $status = 'Resolved';
                break;
            case 5:
                $status = 'Closed';
                break;
            case 6:
                $status = 'Cancelled';
                break;
        }
        return $status;
    }

    public function generateUrgency($Urgency)
    {
        $urgency = '';
        switch ($Urgency) {
            case 1000:
                $urgency = '1-Critical';
                break;
            case 2000:
                $urgency = '2-High';
                break;
            case 3000:
                $urgency = '3-Medium';
                break;
            case 4000:
                $urgency = '4-Low';
                break;
        }
        return $urgency;
    }

    public function generatePriority($Priority)
    {
        $priority = '';
        switch ($Priority) {
            case 0:
                $priority = 'Very High';
                break;
            case 1:
                $priority = 'High';
                break;
            case 2:
                $priority = 'Medium';
                break;
            case 3:
                $priority = 'Low';
                break;
        }
        return $priority;
    }

    public function decodeSurvey($Survey)
    {
        $survey = '';
        switch ($Survey) {
            case 0:
                $survey = '1 - Tidak Puas';
                break;
            case 1:
                $survey = '2 - Kurang Puas';
                break;
            case 2:
                $survey = '3 - Cukup Puas';
                break;
            case 3:
                $survey = '4 - Puas';
                break;
            case 4:
                $survey = '5 - Sangat Puas';
                break;
        }
        return $survey;
    }

    public function tranformToHPDIncidentInterface($status, $param1, $param2)
    {
        // $this->incident_repo->truncateHPDIncidentInterface();
        if ($status == 'year') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Reported_Source', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)->get();
        } else if ($status == 'month') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->where(DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param2)->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)->get();
        }else{
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->whereBetween(DB::raw("CONVERT(DATE,(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')))"), [$param1,$param2])->get();
        }
        // $data->transform(function ($item) {
        //     $payload = [
        //         'IncidentNumber' => $item->Incident_Number,
        //         'AssignedGroupId' => $item->Assigned_Group_ID,
        //         'AssigneeLoginId' => $item->Assignee_Login_ID,
        //         'CustomerPersonId' => $item->Customer_Person_ID,
        //         'ItoolsRequestId' => $item->Itools_Request_Id,
        //         'ProductCategorizationTier1' => $item->Product_Categorization_Tier_1,
        //         'ProductCategorizationTier2' => $item->Product_Categorization_Tier_2,
        //         'ProductCategorizationTier3' => $item->Product_Categorization_Tier_3,
        //         'OperationalCategorizationTier1' => $item->Categorization_Tier_1,
        //         'OperationalCategorizationTier2' => $item->Categorization_Tier_2,
        //         'OperationalCategorizationTier3' => $item->Categorization_Tier_3,
        //         'ReportedSource' => $this->generateReportedSource($item->Reported_Source),
        //         'Description' => $this->convertUTF($item->Description),
        //         'Notes' => $this->convertUTF($item->Notes),
        //         'Resolution' => $this->convertUTF($item->Resolution),
        //         'ServiceCI' => $item->ServiceCI,
        //         'ServiceType' => $this->generateServiceType($item->Service_Type),
        //         'Status' => $this->generateStatus($item->Status),
        //         'StatusReason' => $item->Status_Reason,
        //         'Impact' => $this->generateImpact($item->Impact),
        //         'Urgency' => $this->generateUrgency($item->Urgency),
        //         'Priority' => $this->generatePriority($item->Priority),
        //         'Survey' => $this->decodeSurvey($item->Survey__c),
        //         'SubmitDate' => date('Y-m-d H:i:s', strtotime($item->Submit_Date)),
        //         'AssignedSupportCompany' => $item->Assigned_Support_Company,
        //         'AssignedSupportOrganization' => $item->Assigned_Support_Organization,
        //         'AssignedGroup' => $item->Assigned_Group,
        //         'Assignee' => $item->Assignee
        //     ];
        //     $this->incident_repo->insertIncident($payload);
        // });
        // return $this->incident_repo->getHPDIncidentInterface();
        return $this->loopForeach($data);
    }

    public function tranformToHPDIncidentInterfaceSource($status, $param1, $param2, $source)
    {
        $goSource = $this->generateSource($source);
        if ($status == 'year') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Reported_Source', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)->where('Reported_Source', $goSource)->get();
        } else if ($status == 'month') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->where(DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param2)->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)->where('Reported_Source', $goSource)->get();
        } else {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->whereBetween(DB::raw("CONVERT(DATE,(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')))"), [$param1,$param2])->where('Reported_Source', $goSource)->get();
        }
        return $this->loopForeach($data);
    }

    public function tranformToHPDIncidentInterfaceSurvey($status, $param1, $param2, $survey)
    {
        $goSurvey = $this->generateSurvey($survey);
        if ($status == 'year') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Reported_Source', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)->where('Survey__c', $goSurvey)->get();
        } else if ($status == 'month') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->where(DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param2)->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)->where('Survey__c', $goSurvey)->get();
        } else {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')->whereBetween(DB::raw("CONVERT(DATE,(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')))"), [$param1,$param2])->where('Survey__c', $goSurvey)->get();
        }
        return $this->loopForeach($data);
    }

    public function tranformToHPDIncidentInterfaceSourceSurvey($status, $param1, $param2, $source, $survey)
    {
        $goSource = $this->generateSource($source);
        $goSurvey = $this->generateSurvey($survey);
        if ($status == 'year') {
            $data = Incident::select(
                'Incident_Number',
                'Assigned_Support_Company',
                'Assigned_Support_Organization',
                'Assigned_Group',
                'Assigned_Group_ID',
                'Assignee_Login_ID',
                'Assignee',
                'Person_ID as Customer_Person_ID',
                'Itools_Request_Id',
                'Categorization_Tier_1',
                'Categorization_Tier_2',
                'Categorization_Tier_3',
                'Product_Categorization_Tier_1',
                'Product_Categorization_Tier_2',
                'Product_Categorization_Tier_3',
                'Reported_Source',
                'Description',
                'Detailed_Decription as Notes',
                'Resolution',
                'ServiceCI',
                'Service_Type',
                'Status',
                'Status_Reason',
                'Impact',
                'Urgency',
                'Priority',
                'Survey__c',
                'Submit_Date'
            )
                ->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)
                ->where('Reported_Source', $goSource)
                ->where('Survey__c', $goSurvey)
                ->get();
        } else if ($status == 'month') {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')
                ->where(DB::raw("MONTH(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param2)
                ->where(DB::raw("YEAR(DATEADD(ss,Submit_Date,'19700101 07:00:00:000'))"), $param1)
                ->where('Reported_Source', $goSource)
                ->where('Survey__c', $goSurvey)
                ->get();
        } else {
            $data = Incident::select('Incident_Number', 'Assigned_Support_Company', 'Assigned_Support_Organization', 'Assigned_Group', 'Assigned_Group_ID', 'Assignee_Login_ID', 'Assignee', 'Person_ID as Customer_Person_ID', 'Itools_Request_Id', 'Categorization_Tier_1', 'Categorization_Tier_2', 'Categorization_Tier_3', 'Product_Categorization_Tier_1', 'Product_Categorization_Tier_2', 'Product_Categorization_Tier_3', 'Description', 'Detailed_Decription as Notes', 'Resolution', 'Reported_Source', 'ServiceCI', 'Service_Type', 'Status', 'Status_Reason', 'Impact', 'Urgency', 'Priority', 'Survey__c', 'Submit_Date')
            ->whereBetween(DB::raw("CONVERT(DATE,(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')))"), [$param1,$param2])
            ->where('Reported_Source', $goSource)->where('Survey__c', $goSurvey)->get();
        }
        return $this->loopForeach($data);
    }

    public function loopForeach($data){
        $hasil=array();
        foreach ($data as $key => $item) {
            array_push($hasil, [
                'IncidentNumber' => $item->Incident_Number,
                'AssignedGroupId' => $item->Assigned_Group_ID,
                'AssigneeLoginId' => $item->Assignee_Login_ID,
                'CustomerPersonId' => $item->Customer_Person_ID,
                'ItoolsRequestId' => $item->Itools_Request_Id,
                'ProductCategorizationTier1' => $item->Product_Categorization_Tier_1,
                'ProductCategorizationTier2' => $item->Product_Categorization_Tier_2,
                'ProductCategorizationTier3' => $item->Product_Categorization_Tier_3,
                'OperationalCategorizationTier1' => $item->Categorization_Tier_1,
                'OperationalCategorizationTier2' => $item->Categorization_Tier_2,
                'OperationalCategorizationTier3' => $item->Categorization_Tier_3,
                'ReportedSource' => $this->generateReportedSource($item->Reported_Source),
                'Description' => $this->convertUTF($item->Description),
                'Notes' => $this->convertUTF($item->Notes),
                'Resolution' => $this->convertUTF($item->Resolution),
                'ServiceCI' => $item->ServiceCI,
                'ServiceType' => $this->generateServiceType($item->Service_Type),
                'Status' => $this->generateStatus($item->Status),
                'StatusReason' => $item->Status_Reason,
                'Impact' => $this->generateImpact($item->Impact),
                'Urgency' => $this->generateUrgency($item->Urgency),
                'Priority' => $this->generatePriority($item->Priority),
                'Survey' => $this->decodeSurvey($item->Survey__c),
                'SubmitDate' => date('Y-m-d H:i:s', strtotime($item->Submit_Date)),
                'AssignedSupportCompany' => $item->Assigned_Support_Company,
                'AssignedSupportOrganization' => $item->Assigned_Support_Organization,
                'AssignedGroup' => $item->Assigned_Group,
                'Assignee' => $item->Assignee
                ]);
        }
        return $hasil;
    }
}
