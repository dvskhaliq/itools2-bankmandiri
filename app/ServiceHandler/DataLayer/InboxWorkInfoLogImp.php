<?php

namespace App\ServiceHandler\DataLayer;

use App\InboxWorkinfolog;
use App\Http\Resources\InboxWorkInfoLog as WorkInfoResource;
use App\SummaryWhatsappDispatcher;

class InboxWorkInfoLogImp implements InboxWorkInfoLogService
{
    public $status = false;
    public $message = 'this is default message';

    public function addAttachFile($array_attr)
    {
        try {
            $work_info = new InboxWorkinfolog();
            $work_info->request_id = (array_key_exists("request_id", $array_attr) ? $array_attr["request_id"] : null);
            $work_info->submitter_id = (array_key_exists("submitter_id", $array_attr) ? $array_attr["submitter_id"] : null);
            $work_info->workinfo_type = (array_key_exists("workinfo_type", $array_attr) ? $array_attr["workinfo_type"] : null);
            $work_info->workinfo_content = (array_key_exists("workinfo_content", $array_attr) ? $array_attr["workinfo_content"] : null);
            $work_info->submit_date = date('Y-m-d H:i:s');
            $work_info->first_attachment_file_name = (array_key_exists("first_attachment_file_name", $array_attr) ? $array_attr["first_attachment_file_name"] : null);
            $work_info->file_ext = (array_key_exists("file_ext", $array_attr) ? $array_attr["file_ext"] : null);
            $work_info->file_size_in_bytes = (array_key_exists("file_size_in_bytes", $array_attr) ? $array_attr["file_size_in_bytes"] : null);
            $work_info->save();
            return new WorkInfoResource($work_info);
        } catch (\Exception $th) {
            $this->status = false;
            $this->message = "message save failed: " . $th->__toString();
            return $th;
        }
    }

    public function deleteAttachFile($workinfo_id)
    {
        $data = InboxWorkinfolog::find($workinfo_id);
        $data->delete();
    }

    public function findWorkInfo($workinfo_id)
    {
        return InboxWorkinfolog::find($workinfo_id);
    }

    public function findWorkinfobyId($id)
    {
        return InboxWorkinfolog::where('request_id',$id)->first();
    }
}