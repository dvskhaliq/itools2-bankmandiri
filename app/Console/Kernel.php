<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Host AS400 for branch
        $schedule->call('App\Http\Controllers\Host400Controller@openBranchData')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\Host400Controller@closedBranchData')->everyThirtyMinutes();
        $schedule->call('App\Http\Controllers\Host400Controller@branchData')->everyThirtyMinutes();
        // APdynamycs
        $schedule->call('App\Http\Controllers\AppdynamicsController@index')->daily();
        // $schedule->command('inspire')
        //          ->hourly();
        // ASP Source
        $schedule->call('App\Http\Controllers\DataTableauController@aspResptime')
            ->dailyAt('05:00');
        $schedule->call('App\Http\Controllers\DataTableauController@success')
            ->dailyAt('05:00');        

        // Kibana Source
        $schedule->call('App\Http\Controllers\DataTableauController@eMoneyRestimeKibana')
            ->dailyAt('05:00');
        $schedule->call('App\Http\Controllers\DataTableauController@successEmoney')
            ->dailyAt('05:00');
        $schedule->call('App\Http\Controllers\DataTableauController@successSoaBiru')
           ->dailyAt('05:00');        

        // MySql Yogi Source
        $schedule->call('App\Http\Controllers\DataTableauController@edcUssd')
           ->dailyAt('05:00');

        // SR VE
        $schedule->call('App\Http\Controllers\Host400Controller@veData')
           ->dailyAt('06:00');

        // Kopra TS
        $schedule->call('App\Http\Controllers\AppdynamicsController@koprats')->everyTwoHours();

        // Data ICS
        $schedule->call('App\Http\Controllers\Host400Controller@icsData')
           ->dailyAt('07:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
