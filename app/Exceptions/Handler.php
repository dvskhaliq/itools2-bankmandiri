<?php

namespace App\Exceptions;

use Exception; //laravel 5.7
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Session;
use Throwable;
use Illuminate\Support\Arr;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    public function unauthenticated($request, AuthenticationException $exception)
    {
        // dd($exception);
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        // $guard = array_get($exception->guards(), 0);//ini masih di versi laravel 5.7 
        $guard = Arr::get($exception->guards(), 0);
        switch ($guard) {
            case 'problem':
                $login = '/data_management/login';
                break;
            case 'ma':
                $login = '/ma/login';
                break;
            default:
                $login = '/login';
                break;
        }
        Session::forget('url.intented');
        // $request->session()->invalidate();
        // $this->guard()->logout();
        return redirect($login);
    }
}
