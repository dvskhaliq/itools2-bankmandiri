<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;

class ProblemManagementController extends Controller
{
   
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/dashboard';
    // protected $loginPath = '/data_management/login';
    protected $redirectTo = '/data_management';
    protected $guard = 'problem';
    protected $maxAttempts = 2;
    protected $decayMinutes = 2;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:problem')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function showLoginForm()
    {
        return view('auth.problemlogin');
    }

    public function guard()
    {
        return auth()->guard('problem');
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'un_remedy';
            
        return [
            $field => $request->get($this->username()),
            'password' => $request->password, 'module_problem' => 1,
        ];
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    
    // protected function sendLoginResponse(Request $request)
    // {
    //     $request->session()->regenerate();

    //     $this->clearLoginAttempts($request);

    //     return $this->authenticated($request, $this->guard()->user())
    //             ?: redirect()->intended('/data_management');
    // }

    protected function logout(Request $request)
    {
        $this->guard()->logout();
        // $request->session()->invalidate();
        return redirect('/data_management/login');
    }

    protected function sendLockoutResponse(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
        ? $this->username()
        : 'un_remedy';

        $user = User::where($field, $request->{$this->username()})->first();
        try{
            $this->submitUpdate($user->id);
        }catch(\Exception $ex){
            throw ValidationException::withMessages([
                $this->username() => ['These credentials do not match our records.'],
            ])->status(429);
        }
        
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        throw ValidationException::withMessages([
            $this->username() => ['Akun telah dinonaktifkan, mohon hubungi admin !'],
        ])->status(429);
    }

    public function submitUpdate($id)
    {
        $dataUpdate = array(
            'module_problem' => false,
        );
        User::where('id', $id)->update($dataUpdate);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
        ? $this->username()
        : 'un_remedy';
        
        $errors = [$this->username() => trans('auth.failed')];

        $user = User::where($field, $request->{$this->username()})->first();
        
        if ($user && \Hash::check($request->password, $user->password) && $user->status != 1) {
            $errors = [$this->username() => trans('auth.notactivated')];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }
}
