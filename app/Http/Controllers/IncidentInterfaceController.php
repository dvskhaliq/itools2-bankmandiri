<?php

namespace App\Http\Controllers;

use App\ServiceHandler\DataLayer\HPDIncidentInterfaceService;
use App\ServiceHandler\DataLayer\RemedyHPDIncidentInterfaceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class IncidentInterfaceController extends BaseController
{
    protected $remedy_incident_repo;
    protected $incident_repo;

    public function __construct(RemedyHPDIncidentInterfaceService $remedy_incident_service, HPDIncidentInterfaceService $incident_service)
    {
        $this->remedy_incident_repo = $remedy_incident_service;
        $this->incident_repo = $incident_service;
    }

    public function index()
    {
        return View::make('report/public/incident_report');
    }

    public function populateHPDIncidentInterfaceByYear(Request $request)
    {
        $year = $request->input('selectedYear');
        $month = null;
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterface('year', $year, $month);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByMonth(Request $request)
    {
        $year = $request->input('selectedYear');
        $month = $request->input('selectedMonth');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterface('month', $year, $month);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByDate(Request $request)
    {
        $startDate = (int) str_replace("-", "", $request->input('startDate')) < (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $endDate = (int) str_replace("-", "", $request->input('startDate')) > (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterface('date', $startDate, $endDate);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByYearSource(Request $request)
    {
        $source = $request->input('source');
        $year = $request->input('selectedYear');
        $month = null;
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSource('year', $year, $month, $source);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByMonthSource(Request $request)
    {
        $source = $request->input('source');
        $year = $request->input('selectedYear');
        $month = $request->input('selectedMonth');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSource('month', $year, $month, $source);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByDateSource(Request $request)
    {
        $source = $request->input('source');
        $startDate = (int) str_replace("-", "", $request->input('startDate')) < (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $endDate = (int) str_replace("-", "", $request->input('startDate')) > (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSource('date', $startDate, $endDate, $source);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByYearSurvey(Request $request)
    {
        $survey = $request->input('survey');
        $year = $request->input('selectedYear');
        $month = null;
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSurvey('year', $year, $month, $survey);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByMonthSurvey(Request $request)
    {
        $survey = $request->input('survey');
        $year = $request->input('selectedYear');
        $month = $request->input('selectedMonth');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSurvey('month', $year, $month, $survey);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByDateSurvey(Request $request)
    {
        $survey = $request->input('survey');
        $startDate = (int) str_replace("-", "", $request->input('startDate')) < (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $endDate = (int) str_replace("-", "", $request->input('startDate')) > (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSurvey('date', $startDate, $endDate, $survey);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByYearSourceSurvey(Request $request)
    {
        $source = $request->input('source');
        $survey = $request->input('survey');
        $year = $request->input('selectedYear');
        $month = null;
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSourceSurvey('year', $year, $month, $source, $survey);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByMonthSourceSurvey(Request $request)
    {
        $source = $request->input('source');
        $survey = $request->input('survey');
        $year = $request->input('selectedYear');
        $month = $request->input('selectedMonth');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSourceSurvey('month', $year, $month, $source, $survey);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function populateHPDIncidentInterfaceByDateSourceSurvey(Request $request)
    {
        $source = $request->input('source');
        $survey = $request->input('survey');
        $startDate = (int) str_replace("-", "", $request->input('startDate')) < (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $endDate = (int) str_replace("-", "", $request->input('startDate')) > (int) str_replace("-", "", $request->input('endDate')) ? $request->input('startDate') : $request->input('endDate');
        $data = $this->remedy_incident_repo->tranformToHPDIncidentInterfaceSourceSurvey('date', $startDate, $endDate, $source, $survey);
        dd($data);
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }

    public function fetchHPDIncidentInterface()
    {
        $data = $this->incident_repo->getHPDIncidentInterface();
        return response()->json([
            'status' => true,
            'data' => $data,
        ], 200);
    }
}