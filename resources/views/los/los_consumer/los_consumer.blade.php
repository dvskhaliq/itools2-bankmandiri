@extends('sidebar')
@section('content')
<div id="los_consumer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading">
                    <h4>Los Consumer</h4>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info=""></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading" v-if="isloaded_after">
                                <span>Before Update</span>
                            </div>
                            <div class="panel-body">
                                <component-table-before-update :result_before_update="result_before_update" :isloaded_data="isloaded_data" v-on:ap-currtrcode-selected="AP_CURRTRCODEselected" v-on:ap-nexttrby-selected="AP_NEXTTRBYselected"></component-table-before-update>
                            </div>
                        </div>

                        <div class="panel panel-default" v-if="isloaded_after">
                            <div class="panel-heading">
                                <span>After Update</span>
                            </div>
                            <div class="panel-body">
                                <component-table-after-update :result_after_update="result_after_update" :isloaded_data="isloaded_data"></component-table-after-update>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <component-search v-on:search-form="searchData" :isloaded="isloaded"></component-search>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalAP_CURRTRCODE" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog  modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">UPDATE AP CURRTRCODE</h4>
                </div>
                <div v-if="isloaded">
                    <componen-ap_currtrcode :ap_currtrcode="ap_currtrcode" v-on:update-ap-currtrcode="updateAP_CURRTRCODE" v-on:close-windows="closeWindow" :isloaded="isloaded" :ismodal="ismodal">
                        </componen-ap_currtrcode>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalAP_NEXTTRBY" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog  modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">UPDATE AP NEXTTRBY</h4>
                </div>
                <div v-if="isloaded">
                    <componen-ap_nexttrby :ap_nexttrby="ap_nexttrby" :isloaded="isloaded" :ismodal="ismodal" v-on:close-windows="closeWindow" v-on:update-ap-currtrcode="updateAP_NEXTTRBY">
                        </componen-ap_nexttrby>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/los_consumer.js"></script>
@endsection
