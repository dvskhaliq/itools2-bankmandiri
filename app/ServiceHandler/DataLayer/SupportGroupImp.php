<?php

namespace App\ServiceHandler\DataLayer;

use App\SupportGroupRemedy as SupportGroup;

class SupportGroupImp implements SupportGroupService
{
    public function findByComOrgGrp(String $company_name,String $organization_name,String $group_name) : ?SupportGroup
    {
        try {
            return SupportGroup::where('company', $company_name)
                ->where('organization', $organization_name)
                ->where('support_group_name', $group_name)                
                ->first();
        } catch (\Exception $th) {
            return null;
        }
    }

    public function getSupportGroupId($company, $organization, $support_group){
        return SupportGroup::distinct('id')
        ->where('company',$company)
        ->where('organization',$organization)
        ->where('support_group_name',$support_group)
        ->pluck('id')->first();
    }
}