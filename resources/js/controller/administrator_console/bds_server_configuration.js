require('../../bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('component-table', ()=> import('../../components/administrator_console/bds_server_configuration/BDSServerConfiguration.vue'));
Vue.component('componen-insert', ()=> import('../../components/administrator_console/bds_server_configuration/BDSServerConfigurationInsert.vue'));
Vue.component('componen-edit-seleceted', ()=> import('../../components/administrator_console/bds_server_configuration/BDSServerConfigurationUpdate.vue'));
Vue.component('componen-confirm-delete', ()=> import('../../components/administrator_console/bds_server_configuration/BDSServerConfigurationDelete.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));

import VuePagination from '../../components/Pagination.vue';
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)
import 'es6-promise/auto'
import Vuex from 'vuex';
Vue.use(Vuex);
const bds_server = new Vue({
    el: '#bds_server_configuration',
    data: {
        json_fields: {
            'Branch Code': 'kode_cabang',
            'Branch Name': 'nama_cabang',
            'IP Address': 'ip_server',
        },

        json_meta: [
            [{
                'key': 'charset',
                'value': 'utf-8'
            }]
        ],

        result: null,
        selected_prop: {},
        isloaded: false,
        ismodal: false,
        alert: false,
        pages: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        search: '',
    },

    mounted() {
        this.getData();
    },

    components: {
        VuePagination,
    },

    methods: {

        getData() {
            this.alert = true;
            axios.get(`/bds_server_configuration/search?page=${this.pages.current_page}&search=` + this.search)
                .then((response) => {
                    this.pages = response.data.result;
                    this.result = response.data.result.data;
                }).catch(error => {
                  if (error.response.status === 419 || error.response.status === 401) {
                    window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                    location.reload(false);
                  } else {
                    this.setFlashDanger(error.response.data.message)
                  }
                });
        },

        setFlashInfo(reqResponse) {
            flash({
                id: "alert-info",
                message: reqResponse,
                flashtype: "info"
            });
        },

        setFlashDanger(reqResponse) {
            flash({
                id: "alert-danger",
                message: reqResponse,
                flashtype: "danger"
            });
        },

        searchData(reqResponse) {
            this.alert = true;
            this.search = reqResponse.search
            axios.get(`/bds_server_configuration/search?page=${1}&search=` + this.search)
                .then((response) => {
                    this.pages = response.data.result;
                    this.result = response.data.result.data;
                }).catch(error => {
                  if (error.response.status === 419 || error.response.status === 401) {
                    window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                    location.reload(false);
                  } else {
                    this.setFlashDanger(error.response.data.message)
                  }
                });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
        },

        dataSelected(reqResponse) {
            this.alert = true;
            this.ismodal = true;
            axios.post('/bds_server_configuration/selected', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.selected_prop = response.data.result;
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        },

        submitUpdate(reqResponse) {
            this.ismodal = true;
            this.alert = false;
            axios.post('/bds_server_configuration/submit_update', reqResponse.data).then(response => {                
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        },

        submitDelete(reqResponse) {
            this.ismodal = false;
            this.alert = true;
            axios.post('/bds_server_configuration/submit_delete', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        },

        submitInsert(reqResponse) {
            this.ismodal = true;
            this.alert = false;
            axios.post('/bds_server_configuration/submit_insert', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger(error.response.data.message)
              }
            });
        }

    },

});
