import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            option_company: ["PT. BANK MANDIRI, TBK", "MANDIRI CASH MANAGEMENT"],
            company_selected: "MANDIRI CASH MANAGEMENT",
            option_service: [],
            service_selected: "",
            Incident: "",
            impact_selected: "",
            type_incident_selected: [],
            option_type_incident: [
                "1-Internal",
                "2-Eksternal",
            ],
            option_saverity:["1-Very High","2-High","3-Medium","4-Low"],
            saverity_selected: "",
            suspect_selected: "",
            Estimasi: null,
            estimasi_selected: "",
            status_incident: "",
            status_incident_selected: "",
            option_odcommandcenter: [],
            odcommandcenter_selected: "",
            searchEmployeeRemedy: null,
            isLoading: false,

            status_ticket_selected: "Assigned",
            option_ticket_Status: [
                "Assigned",
                "In Progress",
                "Pending",
                "Resolved",
                "Cancelled"
            ],
            option_assigned_support_company: [
                "PT. BANK MANDIRI, TBK",
                "MANDIRI CASH MANAGEMENT"
            ],
            assigned_support_company_selected: "PT. BANK MANDIRI, TBK",
            option_assigned_support_organization: ["IT INFRASTRUCTURE GROUP"],
            assigned_support_organization_selected: "IT INFRASTRUCTURE GROUP",
            option_assigned_support_group: [],
            assigned_support_group_selected: null,
            option_Assigned: [],
            Assigned_selected: null,
        }
    };
};

const state = {
    assignSupportGroupValue: [],
    assignSupportName: [],
    employeeRemedy: [],
    serviceRemedy: [],
    listIncidentSelected: [],
    itemDefault: defaultItem().item,
    indexDefault: defaultItem().index,
    alertMessage: "",
    inboxId: null,
    durasiGangguan: "",
    picSupportGroupValue: [],
    picSupportName: []
    
    
};

const getters = {
    getAssignSupportGroupValue: state => state.assignSupportGroupValue,
    getAssignSupportName: state => state.assignSupportName,
    getEmployeeRemedy: state => state.employeeRemedy,
    getServiceRemedy: state=> state.serviceRemedy,
    getItemDefault: state=>state.itemDefault,
    getIndexDefault: state=>state.indexDefault,
    getException: state=>state.alertMessage,
    getListIncidentSelected: state=>state.listIncidentSelected,
    getInboxId: state=>state.inboxId,
    getDurasiGangguan: state=>state.durasiGangguan,
    getPicSupportGroupValue: state => state.picSupportGroupValue,
    getPicSupportName: state => state.picSupportName
   
};

const actions = {
    async initAssignSupportGroupValue({ commit }, payload) {
        //commit("setApiState", ENUM.LOADING);
        let response;
        try {         
          response = await axios.post("/employee_remedy/support_group", payload);
          commit("setAssignSupportGroupValue", response.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async iniPicSupportGroupValue({ commit }, payload) {
        //commit("setApiState", ENUM.LOADING);
        let response;
        try {         
          response = await axios.post("/employee_remedy/support_group", payload);
          commit("setPicSupportGroupValue", response.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async initAssignSupportName({ commit }, payload) {
       // commit("setApiState", ENUM.LOADING);
        let response;
        try {         
          response = await axios.post("/employee_remedy/assignee", payload);
          commit("setAssignSupportName", response.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async initPicSupportName({ commit }, payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {         
           response = await axios.post("/employee_remedy/assignee", payload);
           commit("setPicSupportName", response.data);
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
     },
    async initServiceRemedy({ commit }, payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {         
           response = await axios.post("/service", payload);
           commit("setServiceRemedy", response.data.result);
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
     },
    async employeSearch({ commit }, payload) {
       // commit("setApiState", ENUM.LOADING);
        let response;
        try {         
          response = await axios.post("/onduty/incident/customer_profile", payload);
         // console.log(response.data);
          commit("setEmployeeRemedy", response.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async createIncidentByOD({ commit }, payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {       
            //console.log('insideIncidentOd');  
            response = await axios.post("/onduty/incident/createincidentod", payload);
            console.log(response.data.inboxId);
            //return response.data.inboxId;
            commit("setInboxId", response.data.inboxId);
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },async createIncidentRemedy({ commit }, payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {       
            //console.log('insideIncidentOd');  
            response = await axios.post("/onduty/incident/createaproveincidentremedy", payload);
            console.log(response.data.inboxId);
            //return response.data.inboxId;

            if(response.data.status==true){
                //Jika sukses (udah pasti dapet incident number)
                console.log(response.data.incident_number);
                commit("setInboxId", response.data.inboxId);
            }else{
                //Jika gagal maka di frontend formnya jangan
            }
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },async updateIncidentByOD({ commit }, payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {       
            console.log('update incident od');
            console.log(payload);  
            response = await axios.post("/onduty/incident/updateincidentod", payload);
            commit("setInboxId", payload.id);
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },
    resetIncidentOdForm({ commit }) {
        commit("setItemDefault", defaultItem().item);
        commit("setIndexDefault", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
    initDateDiffIncident({ commit }, payload){
        var time = "";
        console.log(payload);
      
        var ekskalasiFrom = payload.ekskalasiFrom;
        var ekskalasiTo = payload.ekskalasiTo;
        var milliseconds =0;

        if(payload.ekskalasiStatus === 'CLOSED'){
            time += payload.durasiGangguan;
        }else{
            time +=  payload.labelEkskalasiFrom + ' - ' + payload.labelEkskalasiTo + ' ( ';
            
            if( isNaN(ekskalasiFrom) || isNaN(ekskalasiTo) ) return '';
            if (ekskalasiFrom < ekskalasiTo) milliseconds = ekskalasiTo - ekskalasiFrom; else milliseconds = ekskalasiFrom - ekskalasiTo;
            var days = Math.floor(milliseconds / 1000 / 60 / (60 * 24));
            var date_diff = new Date( milliseconds );
            if (days > 0) time += days + ' Hari ';
            if ((date_diff.getHours()-7) > 0) time += (date_diff.getHours()-7) + ' Jam ';
            if (date_diff.getMinutes() > 0) time += date_diff.getMinutes() + ' Menit ';
            if (date_diff.getSeconds() > 0) time += date_diff.getSeconds() + ' Detik ';

            time += ' )';
        }

        commit("setDurasiGangguan",time);
    }
};

const mutations = {
    setAssignSupportGroupValue: (state, assignSupportGroupValue) => {
       // state.apiState = ENUM.LOADED;
        state.assignSupportGroupValue = assignSupportGroupValue;
    },
    setAssignSupportName: (state, assignSupportName) => {
        // state.apiState = ENUM.LOADED;
        state.assignSupportName = assignSupportName;
    },
    setPicSupportGroupValue: (state, picSupportGroupValue) => {
        // state.apiState = ENUM.LOADED;
         state.picSupportGroupValue = picSupportGroupValue;
    },
    setPicSupportName: (state, picSupportName) => {
         // state.apiState = ENUM.LOADED;
         state.picSupportName = picSupportName;
    },
    setEmployeeRemedy: (state, employeeRemedy) => {
        // state.apiState = ENUM.LOADED;
        state.employeeRemedy = employeeRemedy;
    },
    setServiceRemedy: (state, serviceRemedy) => {
        state.serviceRemedy = serviceRemedy;
    },
    setItemDefault: (state, itemDefault) => {
        state.itemDefault = itemDefault;
    },
    setIndexDefault: (state, indexDefault) => {
        state.indexDefault = indexDefault;
    },
    setListIncidentSelected: (state, listIncidentSelected) => {
        state.listIncidentSelected = listIncidentSelected;
    },
    setInboxId: (state, inboxId) => {
        state.inboxId = inboxId;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setDurasiGangguan: (state, durasiGangguan)=>{
        state.durasiGangguan = durasiGangguan;
    }   

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
