<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SummaryWhatsappDispatcher extends Model
{
    public $primaryKey = 'id';	
    protected $table = 'summary_whatsapp_dispatcher';
    public $timestamps = false;    
    
}