<?php

namespace App\ServiceHandler\DataLayer;

use App\User;
use Auth;

class UserImp implements UserService
{
    public function findByLoginId(String $login_id): User
    {
        return User::with('supportgroupassigneeremedy')->where('un_remedy', $login_id)->first();
    }

    public function findByUserId(?Int $user_id): ?User
    {
        return User::with('supportgroupassigneeremedy', 'employeeremedy')->find($user_id);
    }

    public function getRestrictedMe(): array
    {
        return Auth::user()->only(['id', 'name', 'email', 'un_remedy', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'user_type', 'employee_remedy_id', 'is_delegate_enabled']);
    }

    public function getMe(): array
    {
        return Auth::user();
    }

    public function getUserByPMRole(int $role_id)
    {
        return User::where('problem_role_id', $role_id)->get();
    }
}