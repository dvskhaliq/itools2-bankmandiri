<?php

namespace App\ServiceHandler\IntegrationLayer;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use Illuminate\Support\Facades\DB;
use App\WebServiceResponse;
use App\User;
use App\Workorder;
use Crypt;

class WOIWorkOrderUpdateImp implements WOIWorkOrderUpdateService
{
    public $wsdl_update_wo;
    public $wsdl_update_wo_test;

    public function __construct()
    {
        $this->wsdl_update_wo = env('WSDL_CREATEUPDATE_WO');
        $this->wsdl_update_wo_test = env('WSDL_UPDATE_WO_TEST');
    }
    
    public function getWsdlUpdateWo(){
        return $this->wsdl_update_wo;
    }

    public function getWsdlUpdateWoTest(){
        return $this->wsdl_update_wo_test;
    }

    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeaderAPI($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    ///////////////////////////////////////////////////////////////////////HEAD//////////////////////////////////////////////////////////////
    public function updateStatusWO(Workorder $workorder): WebServiceResponse
    {
        try {
            $client = new SoapClient($this->getWsdlUpdateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'WorkorderId' => $workorder->workorder_number == "null" || $workorder->workorder_number == null ? null : $workorder->workorder_number,
                'AssigneeName' => $workorder->assignee_name == "null" || $workorder->assignee_name == null ? null : $workorder->assignee_name,
                'Status' => $workorder->status == "null" || $workorder->status == null ? null : $workorder->status,
                'StatusReason' => ''
            );
            $client->UpdateStatus($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '', $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '', $data);
        }
    }

    public function updateAssigneeWO(Workorder $workorder): WebServiceResponse
    {
        try {
            $client = new SoapClient($this->getWsdlUpdateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'WorkorderId' => $workorder->workorder_number == "null" || $workorder->workorder_number == null ? null : $workorder->workorder_number,
                'SupportGroupName' => $workorder->assignee_support_group_name == "null" || $workorder->assignee_support_group_name == null ? null : $workorder->assignee_support_group_name,
                'AssigneeName' => $workorder->assignee_name == "null" || $workorder->assignee_name == null ? null : $workorder->assignee_name,
                'AssigneeLoginId' => $workorder->assignee_user_id == "null" || $workorder->assignee_user_id == null ? null : $workorder->assignee_user_id,
                'AssigneeGroupId' => $workorder->assignee_support_group_id == "null" || $workorder->assignee_support_group_id == null ? null : $workorder->assignee_support_group_id,
            );
            $client->UpdateAssignee($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '', $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '', $data);
        }
    }

    public function updateWOGeneric(Workorder $workorder, $username = '', $password = ''): WebServiceResponse
    {
        try {
            $data = array(
                'WorkorderId' => $workorder->workorder_number == "null" || $workorder->workorder_number == null ? null : $workorder->workorder_number,
                'CustomerFirstName' => $workorder->requester_first_name == "null" || $workorder->requester_first_name == null ? null : $workorder->requester_first_name,
                'CustomerLastName' => $workorder->requester_last_name == "null" || $workorder->requester_last_name == null ? null : $workorder->requester_last_name,
                'CustomerCompany' => $workorder->requester_company == "null" || $workorder->requester_company == null ? null : $workorder->requester_company,
                'SupportGroupCompany' => $workorder->assignee_support_company == "null" || $workorder->assignee_support_company == null ? null : $workorder->assignee_support_company,
                'SupportGroupOrganization' => $workorder->assignee_support_organization == "null" || $workorder->assignee_support_organization == null ? null : $workorder->assignee_support_organization,
                'SupportGroupName' => $workorder->assignee_support_group_name == "null" || $workorder->assignee_support_group_name == null ? null : $workorder->assignee_support_group_name,
                'AssigneeName' => $workorder->assignee_name == "null" || $workorder->assignee_name == null ? null : $workorder->assignee_name,
                // 'AssigneeLoginId' => $workorder->assignee_user_id == "null" || $workorder->assignee_user_id == null ? null : $workorder->assignee_user_id,
                'AssigneeLoginId' => $workorder->assignee_login_id == "null" || $workorder->assignee_login_id == null ? null : $workorder->assignee_login_id,
                'AssigneeGroupId' => $workorder->assignee_support_group_id == "null" || $workorder->assignee_support_group_id == null ? null : $workorder->assignee_support_group_id,
                'ItoolsRequestId' => $workorder->itools_request_id == "null" || $workorder->itools_request_id == null ? null : $workorder->itools_request_id,
                'ReportedSources' => $workorder->reported_source == "null" || $workorder->reported_source == null ? null : $workorder->reported_source,
                'Status' => $workorder->status == "null" || $workorder->status == null ? null : $workorder->status,
                'StatusReason' => '',
                'Notes' => $workorder->notes_description == "null" || $workorder->notes_description == null ? null : $workorder->notes_description,
                'Summary' => $workorder->summary_description == "null" || $workorder->summary_description == null ? null : $workorder->summary_description,            
                'Priority' => $workorder->priority == "null" || $workorder->priority == null ? null : $workorder->priority,            
                'ProCatTier1' => $workorder->prod_cat_tier1 == "null" || $workorder->prod_cat_tier1 == null ? null : $workorder->prod_cat_tier1,
                'ProCatTier2' => $workorder->prod_cat_tier2 == "null" || $workorder->prod_cat_tier2 == null ? null : $workorder->prod_cat_tier2,
                'ProCatTier3' => $workorder->prod_cat_tier3 == "null" || $workorder->prod_cat_tier3 == null ? null : $workorder->prod_cat_tier3,
                'OprCatTier1' => $workorder->opr_cat_tier1 == "null" || $workorder->opr_cat_tier1 == null ? null : $workorder->opr_cat_tier1,
                'OprCatTier2' => $workorder->opr_cat_tier2 == "null" || $workorder->opr_cat_tier2 == null ? null : $workorder->opr_cat_tier2,
                'OprCatTier3' => $workorder->opr_cat_tier3 == "null" || $workorder->opr_cat_tier3 == null ? null : $workorder->opr_cat_tier3,
                'ReserveField01' => $workorder->resrv_field_1 == "null" || $workorder->resrv_field_1 == null ? null : $workorder->resrv_field_1,
                'ReserveField02' => $workorder->resrv_field_2 == "null" || $workorder->resrv_field_2 == null ? null : $workorder->resrv_field_2,
                'ReserveField03' => $workorder->resrv_field_3 == "null" || $workorder->resrv_field_3 == null ? null : $workorder->resrv_field_3,
                'ReserveField04' => $workorder->resrv_field_4 == "null" || $workorder->resrv_field_4 == null ? null : $workorder->resrv_field_4,
                'ReserveField05' => $workorder->resrv_field_5 == "null" || $workorder->resrv_field_5 == null ? null : $workorder->resrv_field_5,
                'ReserveDate06' => $workorder->resrv_field_6 == "null" || $workorder->resrv_field_6 == null ? null : $workorder->resrv_field_6,
                'ReserveDate07' => $workorder->resrv_field_7 == "null" || $workorder->resrv_field_7 == null ? null : $workorder->resrv_field_7,
                'ReserveFlag08' => $workorder->resrv_field_8 == "null" || $workorder->resrv_field_8 == null ? null : $workorder->resrv_field_8,
                'ReserveFlag09' => $workorder->resrv_field_9 == "null" || $workorder->resrv_field_9 == null ? null : $workorder->resrv_field_9,
                'ReserveField10' => $workorder->resrv_field_10 == "null" || $workorder->resrv_field_10 == null ? null : $workorder->resrv_field_10,
                'ReserveField11' => $workorder->resrv_field_11 == "null" || $workorder->resrv_field_11 == null ? null : $workorder->resrv_field_11,
                'ReserveField12' => $workorder->resrv_field_12 == "null" || $workorder->resrv_field_12 == null ? null : $workorder->resrv_field_12,
                'ReserveField13' => $workorder->resrv_field_13 == "null" || $workorder->resrv_field_13 == null ? null : $workorder->resrv_field_13,
                'ReserveField14' => $workorder->resrv_field_14 == "null" || $workorder->resrv_field_14 == null ? null : $workorder->resrv_field_14,
                'ReserveField15' => $workorder->resrv_field_15 == "null" || $workorder->resrv_field_15 == null ? null : $workorder->resrv_field_15,
                'Company' => $workorder->requester_company == "null" || $workorder->requester_company == null ? null : $workorder->requester_company,
                'WorkorderType' => $workorder->service_type == "null" || $workorder->service_type == null ? null : $workorder->service_type,
            );
            $client = new SoapClient($this->getWsdlUpdateWoTest(), $this->options());
            if($username !== '' && $password !== ''){
                $headerbody = $this->generateHeaderAPI($username, $password);
            }else{
                $headerbody = $this->generateHeader();
            }
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->UpdateWorkOrderGeneric($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '', $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '', $data);
        }
    }

    ///////////////////////////////////////////////////////Below Code is Before Asssessment /////////////////////////////////////////////////

    public function updateStatus($request, $status, $assignee_id)
    {
        try {
            $client = new SoapClient($this->getWsdlUpdateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $actual_assingee = User::with('supportgroupassigneeremedy')->find($assignee_id);
            $data = array(
                'WorkorderId' => $request->RemedyTicketId,
                'AssigneeName' => $actual_assingee->supportgroupassigneeremedy->full_name,
                'Status' => $status,
                'StatusReason' => ''
            );
            $client->UpdateStatus($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }

    public function updateAssignee($request, $assignee_id)
    {
        try {
            $actual_assingee = User::with('supportgroupassigneeremedy')->find($assignee_id);
            $client = new SoapClient($this->getWsdlUpdateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'WorkorderId' => $request->RemedyTicketId,
                'SupportGroupName' => $actual_assingee->supportgroupassigneeremedy->support_group_name,
                'AssigneeName' => $actual_assingee->supportgroupassigneeremedy->full_name,
                'AssigneeLoginId' => $actual_assingee->supportgroupassigneeremedy->remedy_login_id,
                'AssigneeGroupId' => $actual_assingee->supportgroupassigneeremedy->support_group_id
            );
            $client->UpdateAssignee($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }
    
    public function updateAssigneeOtherGroup($request, $assignee)
    {
        try {
            $client = new SoapClient($this->getWsdlUpdateWo(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $data = array(
                'WorkorderId' => $request->RemedyTicketId,
                'SupportGroupName' => $assignee->support_group_name,
                'AssigneeName' => $assignee->full_name,
                'AssigneeLoginId' => $assignee->remedy_login_id,
                'AssigneeGroupId' => $assignee->support_group_id,
                'SupportGroupOrganization' => $assignee->organization,
                'SupportGroupCompany' => $assignee->company
            );
            $client->UpdateAssignee($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, '');
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '');
        }
    }
}