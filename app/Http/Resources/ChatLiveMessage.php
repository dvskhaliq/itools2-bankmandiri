<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatLiveMessage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "sender" => $this->sender,
            "content" => $this->content,
            "timestamp" => date('Y-m-d H:i:s',strtotime($this->timestamp)),
            "requestId" => $this->request_id,
            "senderName" => $this->inbox->chatsubscribeduser->first_name,
            "agentName" => $this->inbox->supportgroupassigneeremedy->first_name,
            "agentId" => $this->inbox->AssigneeId,
            "employeeAssigneeId" => $this->inbox->EmployeeAssigneeId,
        ];
    }
}
