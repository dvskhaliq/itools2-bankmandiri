<?php

namespace App\ServiceHandler;

use App\Inbox;

interface TaskRequestService
{
    //Class not used. All of functions of this service are not used

    // public function bookRequest($request_id); //this function will be deprecated due moved to inbox service

    // public function update($request_id, $array_inbox); //this function will be deprecate due moved to inbox service

    // /////////////////////////////// Below Code is Before Assesment //////////////////////////////
    // public function addInbox($customerId, $content, $assignee, $RemedyTicketId, $ActionTicketCreation, $service_type);

    // public function add($array_inbox);    

    // public function updateClause($array_clause, $array_inbox);

    // public function findRemedyTicketId($RemedyTicketId);

    // public function findRemedyTicketDuplicated($remedy_ticket_id, $request_id);

    // // this function has been stopped because it was moved to the HistoryService.php
    // // public function getRequestsHistory($employee_remedy_id, $service_catalog_type, $manager, $searchdate = null, $tosearchdate = null, $search = null);
    // // public function getRequestListByStatusAndSupportGroupJson($process_status, $channel_name, $employee_assignee_group_id);

    // // public function getSingleDailyTaskStats($employee_assginee_id, $start_datetime, $end_datetime, $field_name); Has been move to ReportService

    // // public function getGroupHourlyTotalTask($support_group_id, $start_datetime, $end_datetime, $field_name, $process_status); Has been move to ReportService

    // // public function getGroupGrandTotalTask($support_group_id, $start_datetime, $end_datetime, $status); Has been move to ReportService

    // // public function getGroupDailyTotalTask($employee_assignee, $start_date, $end_date, $field_name); Has been move to ReportService

    // // public function getSingleWeeklyTaskStats($employee_assginee_id, $start_date, $end_date, $field_name); Has been move to ReportService

    // // public function getChannelStats($employee_assginee_id, $start_date, $end_datet, $status, $field_name); Has been move to ReportService

    // public function getRequestListByStatusAndCategoryJson($process_status, $service_type, $service_channel_name = null); //depracate

    // public function getTaskListByAssingeeAndStatusJson($employee_assginee_id, $process_status = null);

    // public function getQueueObjectById($id);

    // public function getRequestListByActionTicket();

    // public function isRemedyTicketExist($remedy_ticket_id);

    // public function isIdleRequestExist();

    // public function mergeRequest($origin_request, $duplicated_request);

    // public function updateStatusCompleteById($id, $status, $remedy_ticket_id, $process_status, $action_ticket_creation, $assignee);

    // public function updateStatusCompleteFailedById($id, $status, $remedy_ticket_id, $process_status, $action_ticket_creation, $content, $assignee);

    // public function updateStatusIncidentById($id, $process_status, $action_ticket_creation, $text_decoded);

    // public function updateInboxByRemedyTicketId($request, $content, $service_type);

    // public function updateQueueStatusById($id, $process_status, $user_id, $assignee_id);

    // public function updateStatusById($id, $user_id, $assignee_id, $process_status, $integration_status);

    // public function updateWoID($id, $assignee_id, $process_status, $integration_status, $work_order_id);

    // public function findServiceCatalogByCategorization($prod1, $prod2, $prod3, $opr1, $opr2, $opr3);
}
