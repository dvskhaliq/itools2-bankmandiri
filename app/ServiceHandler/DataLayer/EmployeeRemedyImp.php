<?php

namespace App\ServiceHandler\DataLayer;

use App\EmployeeRemedy;

class EmployeeRemedyImp implements EmployeeRemedyService
{
    public function getEmployeeRemedy($employee_id)
    {
        return EmployeeRemedy::find($employee_id);
    }

    public function getEmployeeRemedyByName($param1, $param2, $full_name = null){
        if($full_name == null){
            return EmployeeRemedy::where('first_name',$param1)->where('last_name',$param2)->groupBy('id')->first();
        }else{
            return EmployeeRemedy::where('company',$param1)->where('organization',$param2)->where('full_name',$full_name)->groupBy('id')->first();
        }
    }
}
