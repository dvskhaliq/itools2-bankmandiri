import { Line } from "vue-chartjs";

export default {
    extends: Line,
    props: ["data", "options"],
    watch: {
        data: function(newval) {
            this.renderChart(newval, this.options);
        }
    },
};
