<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluCps extends Model
{
    protected $table = 'pm_tblu_cps';
    public $timestamps = false;
}
