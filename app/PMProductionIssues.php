<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMProductionIssues extends Model
{
    protected $primaryKey = 'problem_id';
    protected $table = 'pm_tblu_production_issues_tab';
    public $incrementing = false;
}
