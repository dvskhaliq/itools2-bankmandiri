import ENUM from "../../enum";
const defaultItem = () => {
    return {
        item: {
            IncidentNumber: null,
            AssignedSupportCompany: null,
            AssignedSupportOrganization: null,
            AssignedSupportGroup: null,
            Assignee: null,
            Summary: null,
            Notes: null,
            Impact: null,
            Urgency: null,
            ProdCatTier1: null,
            ProdCatTier2: null,
            ProdCatTier3: null,
            OprCatTier1: null,
            OprCatTier2: null,
            OprCatTier3: null,
            ReportedSource: null,
            Resolution: null,
            Status: null,
            TanggalEmailMasuk: null,
            Survey: null,
            ServiceType: null,
            StatusReason: null,
            ServiceCI: null,
            ResponseStatus: null,
            ResponseMessage: null,
        },
        message: {
            message: "",
            type: ""
        }
    };
};

const state = {
    apiState: ENUM.INIT,
    editedItem: defaultItem().item,
    incident: {},
    message: defaultItem().message,
    transfered: {
        status: null,
        payload: {}
    },
};

const getters = {
    apiState: state => state.apiState,
    message: state => state.message,
    editItem: state => state.editedItem,
    transfered: state => state.transfered
};

const actions = {
    initBulk({ commit }, payload) {
        let data = {
            IncidentNumber: payload.IncidentNumber,
            AssignedSupportCompany: payload.AssignedSupportCompany,
            AssignedSupportOrganization: payload.AssignedSupportOrganization,
            AssignedSupportGroup: payload.AssignedSupportGroup,
            Assignee: payload.Assignee,
            Summary: payload.Summary,
            Notes: payload.Notes,
            Impact: payload.Impact,
            Urgency: payload.Urgency,
            ProdCatTier1: payload.ProdCatTier1,
            ProdCatTier2: payload.ProdCatTier2,
            ProdCatTier3: payload.ProdCatTier3,
            OprCatTier1: payload.OprCatTier1,
            OprCatTier2: payload.OprCatTier2,
            OprCatTier3: payload.OprCatTier3,
            ReportedSource: payload.ReportedSource,
            Resolution: payload.Resolution,
            Status: payload.Status,
            TanggalEmailMasuk: payload.TanggalEmailMasuk,
            Survey: payload.Survey,
            ServiceType: payload.ServiceType,
            StatusReason: payload.StatusReason,
            ServiceCI: payload.ServiceCI,
            ResponseStatus: "in progress",
            ResponseMessage: null
        };
        commit("setEditItem", data);
    },
    async doFetchIncident({ commit }, payload) {
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/incident/fetch",
                payload
            );
            console.log({ responseFetch: response });
            if (response.data.status) {
                commit("setIncident", {
                    payload: payload,
                    response: response.data.data
                });
            } else {
                commit("setError", {
                    payload: payload,
                    message: response.data.message
                })
            }
        } catch (error) {
            console.log({ errorFetch: error })
            if (error.response.status == 403) {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.error,
                    payload: payload
                });
            } else {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.message,
                    payload: payload
                });
            }
        }
    },
    async doUpdateIncident({ commit }, payload) {
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/incident/update",
                payload
            );
            console.log({ responseUpdate: response });
            commit("setSubmitted", {
                status: true,
                response: response.data,
                payload: payload
            });
        } catch (error) {
            console.log({ errorUpdate: error });
            if (error.response.status == 403) {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.error,
                    payload: payload
                });
            } else {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.message,
                    payload: payload
                });
            }
        }
    },
    async stopProcess({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/process/stop", payload);
                commit("setMessage", {
                    message: "The process has been successfully terminated !",
                    type: "success"
                })
        } catch (error) {
            error.response.status == 403
                ? commit("setMessage", {
                    message: error.response.data.error,
                    type: "error"
                })
                : commit("setMessage", {
                    message: "Something wrong !",
                    type: "error"
                });
        }
    },
};

const mutations = {
    setIncident: (state, data) => {
        let request = {
            IncidentNumber: data.payload.IncidentNumber,
            AssignedSupportCompany: data.payload.AssignedSupportCompany == null ? data.response.Assigned_Support_Company : data.payload.AssignedSupportCompany,
            AssignedSupportOrganization: data.payload.AssignedSupportOrganization == null ? data.response.Assigned_Support_Organization : data.payload.AssignedSupportOrganization,
            AssignedSupportGroup: data.payload.AssignedSupportGroup == null ? data.response.Assigned_Group : data.payload.AssignedSupportGroup,
            Assignee: data.payload.Assignee == null ? data.response.Assignee : data.payload.Assignee,
            Summary: data.payload.Summary == null ? data.response.Description : data.payload.Summary,
            Notes: data.payload.Notes == null ? data.response.Detailed_Description : data.payload.Notes,
            Impact: data.payload.Impact == null ? data.response.Impact : data.payload.Impact,
            Urgency: data.payload.Urgency == null ? data.response.Urgency : data.payload.Urgency,
            ProdCatTier1: data.payload.ProdCatTier1 == null ? data.response.Product_Categorization_Tier_1 : data.payload.ProdCatTier1,
            ProdCatTier2: data.payload.ProdCatTier2 == null ? data.response.Product_Categorization_Tier_2 : data.payload.ProdCatTier2,
            ProdCatTier3: data.payload.ProdCatTier3 == null ? data.response.Product_Categorization_Tier_3 : data.payload.ProdCatTier3,
            OprCatTier1: data.payload.OprCatTier1 == null ? data.response.Operational_Categorization_Tier_1 : data.payload.OprCatTier1,
            OprCatTier2: data.payload.OprCatTier2 == null ? data.response.Operational_Categorization_Tier_2 : data.payload.OprCatTier2,
            OprCatTier3: data.payload.OprCatTier3 == null ? data.response.Operational_Categorization_Tier_3 : data.payload.OprCatTier3,
            ReportedSource: data.payload.ReportedSource == null ? data.response.ReportedSource : data.payload.ReportedSource,
            Resolution: data.payload.Resolution == null ? data.response.Resolution : data.payload.Resolution,
            Status: data.payload.Status == null ? data.response.Status : data.payload.Status,
            TanggalEmailMasuk: data.payload.TanggalEmailMasuk == null ? null : data.payload.TanggalEmailMasuk,
            TanggalEmailMasukResp: data.response.TanggalEmailMasuk,
            Survey: data.payload.Survey == null ? null : data.payload.Survey,
            ServiceType: data.payload.ServiceType == null ? 'User Service Restoration' : data.payload.ServiceType,
            StatusReason: data.payload.StatusReason == null ? 'No Further Action Required' : data.payload.StatusReason,
            ServiceCI: data.payload.ServiceCI == null ? null : data.payload.ServiceCI
        }
        state.transfered = {
            status: true,
            payload: request
        };
    },
    setEditItem: (state, payload) => (state.editedItem = payload),
    setSubmitted: (state, data) => {
        let result = {}
        if (data.status) {
            result = {
                IncidentNumber: data.response.data.IncidentNumber,
                AssignedSupportCompany: data.response.data.AssignedSupportCompany,
                AssignedSupportOrganization: data.response.data.AssignedSupportOrganization,
                AssignedSupportGroup: data.response.data.AssignedSupportGroup,
                Assignee: data.response.data.Assignee,
                Summary: data.response.data.Summary,
                Notes: data.response.data.Notes,
                Impact: data.response.data.Impact,
                Urgency: data.response.data.Urgency,
                ProdCatTier1: data.response.data.ProdCatTier1,
                ProdCatTier2: data.response.data.ProdCatTier2,
                ProdCatTier3: data.response.data.ProdCatTier3,
                OprCatTier1: data.response.data.OprCatTier1,
                OprCatTier2: data.response.data.OprCatTier2,
                OprCatTier3: data.response.data.OprCatTier3,
                ReportedSource: data.response.data.ReportedSource,
                Resolution: data.response.data.Resolution,
                Status: data.response.data.Status,
                TanggalEmailMasuk: data.response.data.TanggalEmailMasuk==null?data.payload.TanggalEmailMasukResp:data.response.TanggalEmailMasuk,
                Survey: data.response.data.Survey,
                ServiceType: data.response.data.ServiceType,
                StatusReason: data.response.data.StatusReason,
                ServiceCI: data.response.data.ServiceCI,
                ResponseStatus: data.response.processStatus,
                ResponseMessage: data.response.message
            };
        } else {
            result = {
                IncidentNumber: data.payload.IncidentNumber,
                AssignedSupportCompany: data.payload.AssignedSupportCompany,
                AssignedSupportOrganization: data.payload.AssignedSupportOrganization,
                AssignedSupportGroup: data.payload.AssignedSupportGroup,
                Assignee: data.payload.Assignee,
                Summary: data.payload.Summary,
                Notes: data.payload.Notes,
                Impact: data.payload.Impact,
                Urgency: data.payload.Urgency,
                ProdCatTier1: data.payload.ProdCatTier1,
                ProdCatTier2: data.payload.ProdCatTier2,
                ProdCatTier3: data.payload.ProdCatTier3,
                OprCatTier1: data.payload.OprCatTier1,
                OprCatTier2: data.payload.OprCatTier2,
                OprCatTier3: data.payload.OprCatTier3,
                ReportedSource: data.payload.ReportedSource,
                Resolution: data.payload.Resolution,
                Status: data.payload.Status,
                TanggalEmailMasuk: data.payload.TanggalEmailMasuk,
                Survey: data.payload.Survey,
                ServiceType: data.payload.ServiceType,
                StatusReason: data.payload.StatusReason,
                ServiceCI: data.payload.ServiceCI,
                ResponseMessage: data.response,
                ResponseStatus: 'rejected'
            }
        }
        state.editedItem = result;
        state.transfered = {
            status: null,
            payload: {}
        };
    },
    setError: (state, rsp) => {
        console.log(rsp);
        let result = {
            IncidentNumber: rsp.payload.IncidentNumber,
            AssignedSupportCompany: rsp.payload.AssignedSupportCompany,
            AssignedSupportOrganization: rsp.payload.AssignedSupportOrganization,
            AssignedSupportGroup: rsp.payload.AssignedSupportGroup,
            Assignee: rsp.payload.Assignee,
            Summary: rsp.payload.Summary,
            Notes: rsp.payload.Notes,
            Impact: rsp.payload.Impact,
            Urgency: rsp.payload.Urgency,
            ProdCatTier1: rsp.payload.ProdCatTier1,
            ProdCatTier2: rsp.payload.ProdCatTier2,
            ProdCatTier3: rsp.payload.ProdCatTier3,
            OprCatTier1: rsp.payload.OprCatTier1,
            OprCatTier2: rsp.payload.OprCatTier2,
            OprCatTier3: rsp.payload.OprCatTier3,
            ReportedSource: rsp.payload.ReportedSource,
            Resolution: rsp.payload.Resolution,
            Status: rsp.payload.Status,
            TanggalEmailMasuk: rsp.payload.TanggalEmailMasuk,
            Survey: rsp.payload.Survey,
            ServiceType: rsp.payload.ServiceType,
            StatusReason: rsp.payload.StatusReason,
            ServiceCI: rsp.payload.ServiceCI,
            ResponseStatus: 'rejected',
            ResponseMessage: rsp.message,
        };
        state.editedItem = result;
        state.transfered = {
            status: null,
            payload: {}
        };
    },
    setApiState: (state, enumApiState) => {
        state.apiState = enumApiState;
    },
    setMessage: (state, message) => {
        switch (message.type) {
            case "success":
                state.apiState = ENUM.SUCCESS;
                break;
            case "error":
                state.apiState = ENUM.ERROR;
                break;
        }
        state.message.message = message.message
        state.message.type = message.type;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
