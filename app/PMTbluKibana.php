<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluKibana extends Model
{
    protected $table = 'pm_tblu_kibana';
    public $timestamps = false;
}
