<?php

namespace App\ServiceHandler\DataLayer;;

use App\SummaryLiveChatHistory;

class ChatHistoryImp implements ChatHistoryService
{
    /**
     * @Override
     */
    public $status = false;
    public $message = 'this is default message';

    public function getHistory(Int $request_id): SummaryLiveChatHistory
    {
        return SummaryLiveChatHistory::find($request_id);
    }
}
