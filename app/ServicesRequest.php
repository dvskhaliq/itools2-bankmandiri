<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesRequest extends Model
{
    protected $table = 'inbox';

    public $timestamps = false;

    public function servicesrequest()
    {
        return $this->belongsTo(Inbox::class);
    }
}
