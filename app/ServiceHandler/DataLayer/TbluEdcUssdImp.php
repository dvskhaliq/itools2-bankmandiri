<?php

namespace App\ServiceHandler\DataLayer;

use App\HourlyRep as EdcUssd;
use App\PMTbluEdc as Edc;
use App\RspBds as BdsData;
use App\RspEmasKln as EmasKlnData;
use App\RspCps as CpsData;
use App\RspSwift as SwiftData;
use App\PMTbluBds as Bds;
use App\PMTbluEmasKln as EmasKln;
use App\PMTbluCps as Cps;
use App\PMTbluSwift as Swift;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class TbluEdcUssdImp implements TbluEdcUssdService
{
    public function fetchEdcUssdData($day){
        $tgl = date('j', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $bln = date('n', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $thn = date('Y', strtotime('-'.$day.' day', strtotime(date('Y-m-d'))));
        $data = EdcUssd::select('tgl', 'bln', 'thn', 'layanan', 'restime')
        ->where('tgl', $tgl)
        ->where('bln', $bln)
        ->where('thn', $thn)
        ->whereNotIn('restime', ['maintenance', 'm', 'maintanance', '*', '-', '0 (Maintenance)', '5]', '2 ( ussd isat tidak dapat di akses )', 'ok', 'Not OK', '2 ( telkomsel 0 )', '2 (telkomsel 0)'])
        ->whereIn('layanan', ['EDC Purchase', 'EDC MTI Purchase', 'SMS Ketik', 'USSD'])        
        ->whereNotIn('status', ['error'])
	->get();
        $data->transform(function ($item) {
            $layanan = '';
            $node = '';
            switch ($item->layanan) {
                case 'EDC Purchase':
                    $layanan = 'EDC Mandiri';
                    $node = 'Purchase';
                    break;
                case 'EDC MTI Purchase':
                    $layanan = 'EDC MTI';
                    $node = 'Purchase';
                    break;
                case 'SMS Ketik':
                    $layanan = 'SMS Banking';
                    $node = 'Cek Saldo';
                    break;
                case 'USSD':
                    $layanan = 'USSD';
                    $node = 'Cek Saldo';
                    break;
                default:
                    $layanan = '';
                    $node = '';
                    break;
            }            

            return [
                'layanan' => $layanan,
                'restime' => $item->restime,
                'tanggal' => $item->thn .'-'. $item->bln .'-'. $item->tgl,
                'node' => $node
            ];
        });
        $rslt = Edc::insert($data->toArray());
        return $rslt;
    }        

    public function fetchBdsData(){
        $tgl = date('d-m-Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $bln = str_replace('-', '/', $tgl);
        $data = BdsData::select('CHANNEL', 'TGL', 'Resp')
        // ->whereIn('TGL', [$bln])
        ->get();
        $data->transform(function ($item) {
            $new_tanggal = date('Y-m-d', strtotime($item->TGL));
            return [
                'layanan' => 'BDS',
                'restime' => round($item->Resp, 2),
                'tanggal' => $new_tanggal,
                'node' => 'Default'
            ];
        });
        Bds::Truncate();
        $rslt = Bds::insert($data->toArray());
        return $rslt;
    }

    public function fetchEmasKlnData(){
        $tgl = date('d-m-Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $bln = str_replace('-', '/', $tgl);
        $data = EmasKlnData::select('CHANNEL', 'TGL', 'Resp')
        // ->whereIn('TGL', [$bln])
        ->get();
        $data->transform(function ($item) {
            $tanggal = str_replace('/', '-', $item->TGL);
            $new_tanggal = date('Y-m-d', strtotime($tanggal));
            return [
                'layanan' => 'eMAS KLN',
                'restime' => round($item->Resp, 2),
                'tanggal' => $new_tanggal,
                'node' => 'Default'
            ];
        });
        EmasKln::Truncate();
        $rslt = EmasKln::insert($data->toArray());
        return $rslt;
    }

    public function fetchCpsData(){
        $tgl = date('d-m-Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $bln = str_replace('-', '/', $tgl);
        $data = CpsData::select('CHANNEL', 'TGL', 'Resp')
        // ->whereIn('TGL', [$bln])
        ->get();
        $data->transform(function ($item) {
            $new_tanggal = date('Y-m-d', strtotime($item->TGL));
            return [
                'layanan' => 'CPS',
                'restime' => round($item->Resp, 2),
                'tanggal' => $new_tanggal,
                'node' => 'Default'
            ];
        });
        Cps::Truncate();
        $rslt = Cps::insert($data->toArray());
        return $rslt;
    }

    public function fetchSwiftData(){
        $tgl = date('d-m-Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $bln = str_replace('-', '/', $tgl);
        $data = SwiftData::select('CHANNEL', 'TGL', 'Resp')
        // ->whereIn('TGL', [$bln])
        ->get();
        $data->transform(function ($item) {
            $new_tanggal = date('Y-m-d', strtotime($item->TGL));
            return [
                'layanan' => 'Swift',
                'restime' => round($item->Resp, 2),
                'tanggal' => $new_tanggal,
                'node' => 'Default'
            ];
        });
        Swift::Truncate();
        $rslt = Swift::insert($data->toArray());
        return $rslt;
    }    
}
