<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\SuccessRateService;
use Illuminate\Http\Request;

class SuccessRateController extends BaseController
{
    protected $success_rate_repo;

    public function __construct(SuccessRateService $success_rate_service)
    {
        $this->success_rate_repo = $success_rate_service;
    }

    public function fetchYears()
    {
        $data = $this->success_rate_repo->getYears();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSuccessRate()
    {
        $data = $this->success_rate_repo->getSuccessRate();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSuccessRateByYear(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        $data = $this->success_rate_repo->getSuccessRateByYear($selected_year);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSuccessRateByMonth(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $data = $this->success_rate_repo->getSuccessRateByMonth($selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSuccessRateByMonthAvg(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $avg_query = $this->success_rate_repo->getSuccessRateByMonthAvg($selected_month);
        $data = $this->loopForeachGroup($avg_query, $selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSuccessRateByDate(Request $request)
    {
        $selected_date = $request->input('selectedDate');
        $data = $this->success_rate_repo->getSuccessRateByDate($selected_date);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    //correction
    public function loopForeachGroup($avg, $month){
        $hasil=array();
        foreach ($avg as $key => $value) {
            $prop['bulan'] = $value->bulan;
            $prop['layanan'] = $value->layanan;
            $prop['sukses'] = $value->sukses;
            $prop['total'] = $value->total;
            $prop['success_rate'] = ROUND($value->success_rate,2);
            $query = $this->success_rate_repo->getSuccessRateByMonthLayanan($prop['layanan'], $month);
            array_push($hasil, [
                'sequence' => $key+1,
                'bulan' => $prop['bulan'],
                'layanan' => $prop['layanan'],
                'sukses' => $prop['sukses'],
                'total' => $prop['total'],
                'success_rate' => $prop['success_rate'],
                'detail' => $query
                ]);
        }
        return $hasil;
    }
}
