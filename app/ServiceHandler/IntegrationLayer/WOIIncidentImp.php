<?php 

namespace App\ServiceHandler\IntegrationLayer;

use SoapClient;
use SOAPHeader;
use SoapVar;
use DOMDocument;
use Auth;
use App\User;
use App\EmployeeRemedy;
use App\Inbox;
use App\WebServiceResponse;
use App\SupportGroupAssigneeRemedy;
use App\Incident;
use function GuzzleHttp\json_encode;
use Crypt;

class WOIIncidentImp implements WOIIncidentService
{
    public $wsdl_create_inc;

    public function __construct()
    {
        $this->wsdl_create_inc = env('WSDL_CREATE_INC');
    }

    public function getWsdlCreateInc()
    {
        return $this->wsdl_create_inc;
    }

    private $ns = 'http://schemas.xmlsoap.org/wsdl/soap/';

    private function generateHeaderAPI($username, $password)
    {
        $headerbody = new SoapVar([
            new SoapVar($username, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar($password, XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateHeader()
    {
        $headerbody = new SoapVar([
            new SoapVar(Auth::user()->un_remedy, XSD_STRING, null, null, 'userName', $this->ns),
            new SoapVar(Crypt::decrypt(Auth::user()->pass_remedy), XSD_STRING, null, null, 'password', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'authentication', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'locale', $this->ns),
            new SoapVar('', XSD_STRING, null, null, 'timeZone', $this->ns),
        ], SOAP_ENC_OBJECT);

        return $headerbody;
    }

    private function generateContextOptions()
    {
        $arrContextOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT,
            ),
        );

        return $arrContextOptions;
    }

    private function options()
    {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($this->generateContextOptions()),
        );
        return $options;
    }

    ///////////////////////////////////////////////////////////////////////HEAD//////////////////////////////////////////////////////////////

    public function createIncidentGeneric(Incident $incident, $reconID = null): WebServiceResponse
    {
        try {
            $data = array(
                'PersonId' => $incident->requester_person_id == "null" || $incident->requester_person_id == null ? null : $incident->requester_person_id,
                'First_Name' => $incident->requester_first_name == "null" || $incident->requester_first_name == null ? null : $incident->requester_first_name,
                'Last_Name' => $incident->requester_last_name == "null" || $incident->requester_last_name == null ? null : $incident->requester_last_name,
                'Service_Type' => $incident->service_type == "null" || $incident->service_type == null ? null : $incident->service_type,
                'Status' => $incident->status == "null" || $incident->status == null ? null : $incident->status,
                'Impact' => $incident->impact == "null" || $incident->impact == null ? null : $incident->impact,
                'Urgency' => $incident->urgency == "null" || $incident->urgency == null ? null : $incident->urgency,
                'Description' => $incident->summary_description == "null" || $incident->summary_description == null ? null : $incident->summary_description,
                'Reported_Source' => $incident->reported_source == "null" || $incident->reported_source == null ? null : $incident->reported_source,
                'Product_Categorization_Tier_1' => $incident->prod_cat_tier1 == "null" || $incident->prod_cat_tier1 == null ? null : $incident->prod_cat_tier1,
                'Product_Categorization_Tier_2' => $incident->prod_cat_tier2 == "null" || $incident->prod_cat_tier2 == null ? null : $incident->prod_cat_tier2,
                'Product_Categorization_Tier_3' => $incident->prod_cat_tier3 == "null" || $incident->prod_cat_tier3 == null ? null : $incident->prod_cat_tier3,
                'Operational_Categorization_Tier_1' => $incident->opr_cat_tier1 == "null" || $incident->opr_cat_tier1 == null ? null : $incident->opr_cat_tier1,
                'Operational_Categorization_Tier_2' => $incident->opr_cat_tier2 == "null" || $incident->opr_cat_tier2 == null ? null : $incident->opr_cat_tier2,
                'Operational_Categorization_Tier_3' => $incident->opr_cat_tier3 == "null" || $incident->opr_cat_tier3 == null ? null : $incident->opr_cat_tier3,
                'z1D_Action' => 'CREATE',
                'Assigned_Support_Company' => $incident->assignee_support_company == "null" || $incident->assignee_support_company == null ? null : $incident->assignee_support_company,
                'Assigned_Support_Organization' => $incident->assignee_support_organization == "null" || $incident->assignee_support_organization == null ? null : $incident->assignee_support_organization,
                'Assigned_Group' => $incident->assignee_support_group_name == "null" || $incident->assignee_support_group_name == null ? null : $incident->assignee_support_group_name,
                'Assignee' => $incident->assignee_name == "null" || $incident->assignee_name == null ? null : $incident->assignee_name,
                'Assigned_Group_Id' => $incident->assignee_support_group_id == "null" || $incident->assignee_support_group_id == null ? null : $incident->assignee_support_group_id,
                'Resolution' =>  $incident->resolution == "null" || $incident->resolution == null ? null : $incident->resolution,
                'Detailed_Description' => $incident->notes_description == "null" || $incident->notes_description == null ? null : $incident->notes_description,
                'Status_Reason' => $incident->status_reason == "null" || $incident->status_reason == null ? null : $incident->status_reason,
                'Itools_Request_Id' => $incident->itools_request_id,
                'ServiceCI' => $incident->service == "null" || $incident->service == null ? null : $incident->service,
                'ServiceCI_ReconID' => $reconID == null ? null : $reconID,
                'Survey' => $incident->survey == "null" || $incident->survey == null ? null : $incident->generateSurvey($incident->survey),
                'Assignee_Login_Id' => $incident->assignee_login_id == "null" || $incident->assignee_login_id == null ? null : $incident->assignee_login_id,
                'Assigned_To' => $incident->assignee_login_id == "null" || $incident->assignee_login_id == null ? null : $incident->assignee_login_id,
                'TanggalEmailMasuk' => $incident->reported_source == "null" || $incident->reported_source == null ? null : ( $incident->reported_source == 'Email' ? time() : null)
            );
            $client = new SoapClient($this->getWsdlCreateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);

            $client->__setSoapHeaders($header);
            $client->CreateIncident($data);
            $xml = $client->__getLastResponse();

            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('Incident_Number')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    //add by putut mukti
    public function createIncidentGenericOd(Incident $incident, $reconID = null): WebServiceResponse
    {
        /*$data = array(
            'PersonId' => $incident->requester_person_id == "null" || $incident->requester_person_id == null ? null : $incident->requester_person_id,
            'First_Name' => $incident->requester_first_name == "null" || $incident->requester_first_name == null ? null : $incident->requester_first_name,
            'Last_Name' => $incident->requester_last_name == "null" || $incident->requester_last_name == null ? null : $incident->requester_last_name,
            'Service_Type' => $incident->service_type == "null" || $incident->service_type == null ? null : $incident->service_type,
            'Status' => $incident->status == "null" || $incident->status == null ? null : $incident->status,
            'Impact' => $incident->impactValue == "null" || $incident->impactValue == null ? null : $incident->impactValue,
            'Urgency' => $incident->urgency == "null" || $incident->urgency == null ? null : $incident->urgency,
            'Description' => $incident->incident == "null" || $incident->incident == null ? null : $incident->incident,
            'Reported_Source' => $incident->reported_source == "null" || $incident->reported_source == null ? null : $incident->reported_source,
            'Product_Categorization_Tier_1' => $incident->prod_cat_tier1 == "null" || $incident->prod_cat_tier1 == null ? null : $incident->prod_cat_tier1,
            'Product_Categorization_Tier_2' => $incident->prod_cat_tier2 == "null" || $incident->prod_cat_tier2 == null ? null : $incident->prod_cat_tier2,
            'Product_Categorization_Tier_3' => $incident->prod_cat_tier3 == "null" || $incident->prod_cat_tier3 == null ? null : $incident->prod_cat_tier3,
            'Operational_Categorization_Tier_1' => $incident->opr_cat_tier1 == "null" || $incident->opr_cat_tier1 == null ? null : $incident->opr_cat_tier1,
            'Operational_Categorization_Tier_2' => $incident->opr_cat_tier2 == "null" || $incident->opr_cat_tier2 == null ? null : $incident->opr_cat_tier2,
            'Operational_Categorization_Tier_3' => $incident->opr_cat_tier3 == "null" || $incident->opr_cat_tier3 == null ? null : $incident->opr_cat_tier3,
            'z1D_Action' => 'CREATE',
            'Assigned_Support_Company' => $incident->assignee_support_company == "null" || $incident->assignee_support_company == null ? null : $incident->assignee_support_company,
            'Assigned_Support_Organization' => $incident->assignee_support_organization == "null" || $incident->assignee_support_organization == null ? null : $incident->assignee_support_organization,
            'Assigned_Group' => $incident->assignee_support_group_name == "null" || $incident->assignee_support_group_name == null ? null : $incident->assignee_support_group_name,
            'Assignee' => $incident->assignee_name == "null" || $incident->assignee_name == null ? null : $incident->assignee_name,
            'Assigned_Group_Id' => $incident->assignee_support_group_id == "null" || $incident->assignee_support_group_id == null ? null : $incident->assignee_support_group_id,
            'Resolution' =>  $incident->resolution == "null" || $incident->resolution == null ? null : $incident->resolution,
            'Detailed_Description' => $incident->notes_description == "null" || $incident->notes_description == null ? null : $incident->notes_description,
            'Status_Reason' => $incident->status_reason == "null" || $incident->status_reason == null ? null : $incident->status_reason,
            'Itools_Request_Id' => $incident->itools_request_id,
            'ServiceCI' => $incident->service == "null" || $incident->service == null ? null : $incident->service,
            'ServiceCI_ReconID' => $reconID == null ? null : $reconID,
            'Survey' => $incident->survey == "null" || $incident->survey == null ? null : $incident->generateSurvey($incident->survey)
        );*/
        $data = array(
            'PersonId' =>  $incident->requester_person_id == "null" || $incident->requester_person_id == null ? null : $incident->requester_person_id,
            'First_Name' => $incident->requester_first_name == "null" || $incident->requester_first_name == null ? null : $incident->requester_first_name,
            'Last_Name' => $incident->requester_last_name == "null" || $incident->requester_last_name == null ? null : $incident->requester_last_name,
            'Service_Type' =>  $incident->service_type == "null" || $incident->service_type == null ? null : $incident->service_type,
            'Status' => $incident->status == "null" || $incident->status == null ? null : $incident->status,
            'Impact' => $incident->impactValue == "null" || $incident->impactValue == null ? null : $incident->impactValue,
            'Urgency' => $incident->urgency == "null" || $incident->urgency == null ? null : $incident->urgency,
            'Description' =>  $incident->incident == "null" || $incident->incident == null ? null : $incident->incident,
            'Product_Categorization_Tier_1' => $incident->prod_cat_tier1 == "null" || $incident->prod_cat_tier1 == null ? null : $incident->prod_cat_tier1,
            'Product_Categorization_Tier_2' => $incident->prod_cat_tier2 == "null" || $incident->prod_cat_tier2 == null ? null : $incident->prod_cat_tier2,
            'Product_Categorization_Tier_3' => $incident->prod_cat_tier3 == "null" || $incident->prod_cat_tier3 == null ? null : $incident->prod_cat_tier3,
            'Operational_Categorization_Tier_1' => $incident->opr_cat_tier1 == "null" || $incident->opr_cat_tier1 == null ? null : $incident->opr_cat_tier1,
            'Operational_Categorization_Tier_2' => $incident->opr_cat_tier2 == "null" || $incident->opr_cat_tier2 == null ? null : $incident->opr_cat_tier2,
            'Operational_Categorization_Tier_3' => $incident->opr_cat_tier3 == "null" || $incident->opr_cat_tier3 == null ? null : $incident->opr_cat_tier3,
            'z1D_Action' => 'CREATE',
            // 'Reported_Source' =>  "Phone",
            // 'Assigned_Support_Company' => 'PT. BANK MANDIRI, TBK',
            // 'Assigned_Support_Organization' => 'IT INFRASTRUCTURE GROUP',
            // 'Assigned_Group' => 'IT ULP - IT COMMAND CENTER',
            // 'Assignee' => null,
            // 'Assigned_Group_Id' => 'SGP000000003114',
            'Reported_Source' =>  "Officer on Duty", //$incident->reported_source == "null" || $incident->reported_source == null ? null : $incident->reported_source,
            'Assigned_Support_Company' => $incident->assignee_support_company == "null" || $incident->assignee_support_company == null ? null : $incident->assignee_support_company,
            'Assigned_Support_Organization' =>  $incident->assignee_support_organization == "null" || $incident->assignee_support_organization == null ? null : $incident->assignee_support_organization,
            'Assigned_Group' =>  $incident->assignee_support_group_name == "null" || $incident->assignee_support_group_name == null ? null : $incident->assignee_support_group_name,
            'Assignee' => $incident->assignee_name == "null" || $incident->assignee_name == null ? null : $incident->assignee_name,
            'Assigned_Group_Id' => $incident->assignee_support_group_id == "null" || $incident->assignee_support_group_id == null ? null : $incident->assignee_support_group_id,
            'Resolution' =>  $incident->resolution == "null" || $incident->resolution == null ? null : $incident->resolution,
            'Detailed_Description' =>  $incident->notes_description == "null" || $incident->notes_description == null ? null : $incident->notes_description,
            'Status_Reason' => $incident->status_reason == "null" || $incident->status_reason == null ? null : $incident->status_reason,
            'Itools_Request_Id' => $incident->itools_request_id,
            'ServiceCI' => $incident->service == "null" || $incident->service == null ? null : $incident->service,
            'ServiceCI_ReconID' => $reconID == null ? null : $reconID,
            'Survey' => $incident->survey == "null" || $incident->survey == null ? null : $incident->generateSurvey($incident->survey),
        );
        try {
            $client = new SoapClient($this->getWsdlCreateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->CreateIncident($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('Incident_Number')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {

            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    public function createIncidentBook(Inbox $request, SupportGroupAssigneeRemedy $assignee, $reconID = null): WebServiceResponse
    {
        try {
            $inc = new Incident();
            $customer = EmployeeRemedy::find($request->EmployeeSenderId);
            $status = "In Progress";
            if ($request->ProcessStatus == "BOOKED" && $assignee->user->id == Auth::user()->id) {
                $status = "In Progress";
            } else if ($request->ProcessStatus == "BOOKED" && $assignee->user->id != Auth::user()->id) {
                $status = "Assigned";
            } else if ($request->ProcessStatus == "INPROGRESS" && $assignee->user->id == Auth::user()->id) {
                $status = "In Progress";
            } else if ($request->ProcessStatus == "INPROGRESS" && $assignee->user->id != Auth::user()->id) {
                $status = "Assigned";
            } else {
                $status = "Cancelled";
            }
            $inc->doRemedyMapper($request->TextDecoded, $customer, $assignee, $status);
            $request->AssigneeId = ($inc->reported_source === 'Whatsapp' ? $request->AssigneeId : Auth::user()->id);
            $request->EmployeeAssigneeId = $assignee->employee_remedy_id;
            $request->EmployeeAssigneeGroupId = $assignee->support_group_id;
            $request->save();
            $data = array(
                'PersonId' => $inc->requester_person_id,
                'First_Name' => $inc->requester_first_name,
                'Last_Name' => $inc->requester_last_name,
                'Service_Type' => $inc->service_type == "null" || $inc->service_type == null ? "" : $inc->service_type,
                'Status' => $inc->status == "null" || $inc->status == null ? "" : $inc->status,
                'Impact' => $inc->impact == "null" || $inc->impact == null ? "" : $inc->impact,
                'Urgency' => $inc->urgency == "null" || $inc->urgency == null ? "" : $inc->urgency,
                'Description' => $inc->summary_description == "null" || $inc->summary_description == null ? "" : $inc->summary_description,
                'Reported_Source' => $inc->reported_source == "null" || $inc->reported_source == null ? "" : $inc->reported_source,
                'Product_Categorization_Tier_1' => $inc->prod_cat_tier1 == "null" || $inc->prod_cat_tier1 == null ? "" : $inc->prod_cat_tier1,
                'Product_Categorization_Tier_2' => $inc->prod_cat_tier2 == "null" || $inc->prod_cat_tier2 == null ? "" : $inc->prod_cat_tier2,
                'Product_Categorization_Tier_3' => $inc->prod_cat_tier3 == "null" || $inc->prod_cat_tier3 == null ? "" : $inc->prod_cat_tier3,
                'Operational_Categorization_Tier_1' => $inc->opr_cat_tier1 == "null" || $inc->opr_cat_tier1 == null ? "" : $inc->opr_cat_tier1,
                'Operational_Categorization_Tier_2' => $inc->opr_cat_tier2 == "null" || $inc->opr_cat_tier2 == null ? "" : $inc->opr_cat_tier2,
                'Operational_Categorization_Tier_3' => $inc->opr_cat_tier3 == "null" || $inc->opr_cat_tier3 == null ? "" : $inc->opr_cat_tier3,
                'z1D_Action' => 'CREATE',
                'Assigned_Support_Company' => $assignee->company,
                'Assigned_Support_Organization' => $assignee->organization,
                'Assigned_Group' => $assignee->support_group_name,
                'Assignee' => $assignee->full_name,
                'Assigned_Group_Id' => $assignee->support_group_id,
                'Detailed_Description' => $inc->notes_description == "null" || $inc->notes_description == null ? "" : $inc->notes_description,
                'Status_Reason' => 'No Further Action Required',
                'Itools_Request_Id' => $request->id,
                'ServiceCI' => $inc->service == "null" || $inc->service == null ? "" : $inc->service,
                'ServiceCI_ReconID' => $reconID == null ? null : $reconID,
                'Survey' => $inc->survey == "null" || $inc->survey == null ? null : $inc->generateSurvey($inc->survey),
                'Assignee_Login_Id' => $inc->assignee_login_id == "null" || $inc->assignee_login_id == null ? null : $inc->assignee_login_id,
                'Assigned_To' => $inc->assignee_login_id == "null" || $inc->assignee_login_id == null ? null : $inc->assignee_login_id,
                'TanggalEmailMasuk' => $inc->reported_source == "null" || $inc->reported_source == null ? null : ( $inc->reported_source == 'Email' ? time() : null)
            );
            $client = new SoapClient($this->getWsdlCreateInc(), $this->options());
            $headerbody = $this->generateHeader();
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->CreateIncident($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse(null, 'success', $doc->getElementsByTagName('Incident_Number')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', null, $data);
        }
    }

    ///////////////////////////////////////////////////////Below Code is Before Asssessment /////////////////////////////////////////////////

    public function createIncidentByEntity($username, $password, $request) //Still used by createRemedyIN() itoolsAPIController
    {
        try {
            $data = array(
                'PersonId' => $request['PersonId'],
                'First_Name' => $request['First_Name'],
                'Last_Name' => $request['Last_Name'],
                'Service_Type' => $request['Service_Type'],
                'Status' => $request['Status'],
                'Impact' => $request['Impact'],
                'Urgency' => $request['Urgency'],
                'Description' => $request['Description'],
                'Reported_Source' => $request['Reported_Source'],
                'Product_Categorization_Tier_1' => $request['Product_Categorization_Tier_1'],
                'Product_Categorization_Tier_2' => $request['Product_Categorization_Tier_2'],
                'Product_Categorization_Tier_3' => $request['Product_Categorization_Tier_3'],
                'Operational_Categorization_Tier_1' => $request['Operational_Categorization_Tier_1'],
                'Operational_Categorization_Tier_2' => $request['Operational_Categorization_Tier_2'],
                'Operational_Categorization_Tier_3' => $request['Operational_Categorization_Tier_3'],
                'z1D_Action' => $request['z1D_Action'],
                'Assigned_Support_Company' => $request['Assigned_Support_Company'],
                'Assigned_Support_Organization' => $request['Assigned_Support_Organization'],
                'Assigned_Group' => $request['Assigned_Group'],
                'Assignee' => $request['Assignee'],
                'Resolution' => $request['Resolution'],
                'Detailed_Description' => $request['Detailed_Description'],
                'Status_Reason' => $request['Status_Reason'],
                'Assigned_Group_Id' => $request['Assigned_Group_Id'],
                'ItoolsRequestId' => $request['ItoolsRequestId'],
            );
            $client = new SoapClient($this->getWsdlCreateInc(), $this->options());
            $headerbody = $this->generateHeaderAPI($username, $password);
            $header = new SOAPHeader($this->ns, 'AuthenticationInfo', $headerbody);
            $client->__setSoapHeaders($header);
            $client->CreateIncident($data);
            $xml = $client->__getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($xml);
            return new WebServiceResponse($doc->getElementsByTagName('ResponseMessage')->item(0)->nodeValue, $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue, $doc->getElementsByTagName('Incident_Number')->item(0)->nodeValue, $data);
        } catch (\Exception $ex) {
            return new WebServiceResponse($ex->getMessage(), 'failed', '', $data);
        }
    }
}
