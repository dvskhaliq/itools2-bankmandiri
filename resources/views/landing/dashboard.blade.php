@extends('sidebar')
@section('content')
<div class="row" id="app">
    <div class="col-md-12">
        <div class="panel panel-default" id="home">
            <div>
                <div class="panel-heading" style="background: #fff;">
                    <div class="panel-title">
                        <h4>Dashboard</h4>
                    </div>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="!ismodal" info=></flash>
                    <spinner-loader :isresponsereceived="isresponsereceived" :stomps="stomps"></spinner-loader>
                    <div class="panel panel-default tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tab-task" role="tab" data-toggle="tab">Task</a></li>
                            <li><a href="#tab-stats" role="tab" data-toggle="tab">Stats</a></li>
                            <li><a href="#tab-history" role="tab" data-toggle="tab">History</a></li>
                            <task-incoming v-on:create-staging-incident="createStagingIncident" :servicetype="servicetype"> </task-incoming>
                        </ul>
                    </div>

                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-task">
                            <div class="panel panel-default">
                                <a href="#">
                                    <div onclick="ecWorkingTask()" class="panel-heading">My Task <span style="color:gray" id="icon" class="pull-right glyphicon glyphicon-chevron-up" aria-hidden="true"></span></div>
                                </a>
                                <working-task class="cWorkingTask" :tasks="tasks" v-on:loadfilterprocessstatus="loadFilterProcessStatus" v-on:taskselected="loadSelectedRequest" v-on:active-livechat-selected="loadSelectedRequest" :auth="auth" />
                            </div>

                            <div class="panel panel-default">
                                <a href="#">
                                    <div onclick="ecRequestQueue()" class="panel-heading">Request Queue <span style="color:gray" id="iconRQ" class="pull-right glyphicon glyphicon-chevron-up" aria-hidden="true"></span></div>
                                </a>
                                <request-queue class="cRequestQueue" ref="requestqueuecnt" v-on:loadfilterchannelname="loadFilterChannelName" v-on:requestselected="loadSelectedRequest" v-on:selectedticket="selectedTicket" :requests="requests" :auth="auth" :servicetype="servicetype" />
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-stats">
                            
                        <div class="col-md-12">
                        <div class="col-md-1 pull-right" style="margin-top:-20px">
                                <button type="button" class="btn btn-info btn-md" @click="searchTaskReportByDate(selecteddate)">Search</button>
                            </div>
                            <div class="col-md-2 pull-right" style="margin-top:-20px">
                                <input
                                  v-model="selecteddate"
                                  type="date"
                                  autocomplete="off"
                                  class="form-control"
                                />
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Daily Task Stats</div>
                                    <div class="panel-body">
                                        <stats-chart-daily :personalstats="personalstats"></stats-chart-daily>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Weekly Task Stats</div>
                                    <div class="panel-body">
                                        <stats-chart-weekly :personalstats="personalstats">
                                        </stats-chart-weekly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Daily Channel Map</div>
                                    <div class="panel-body">
                                        <channel-chart-daily :personalstats="personalstats">
                                        </channel-chart-daily>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Weekly Channel Map</div>
                                    <div class="panel-body">
                                        <channel-chart-weekly :personalstats="personalstats">
                                        </channel-chart-weekly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-history">
                            <div class="panel panel-default">
                                <a href="#">
                                    <div onclick="ecServiceRequest()" class="panel-heading">Service Request<span style="color:gray" id="icon" class="pull-right glyphicon glyphicon-chevron-up" aria-hidden="true"></span></div>
                                </a>
                                <job-history-wo class="cServiceRequest" v-on:taskselected="loadSelectedRequest" :wohistory="wohistory" :auth="auth" :manager="false"></job-history-wo>
                            </div>
                            <div class="panel panel-default">
                                <a href="#">
                                    <div onclick="ecIncident()" class="panel-heading">Incident<span style="color:gray" id="iconRQ" class="pull-right glyphicon glyphicon-chevron-up" aria-hidden="true"></span></div>
                                </a>
                                <job-history-inc class="cIncident" v-on:taskselected="loadSelectedRequest" :inhistory="inhistory" :auth="auth" :manager="false"></job-history-inc>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- MODAL Closure-->
        <modal-workorder-closure :requestobject="requestobject" :isclosuring="isclosuring" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-workorder-closure>
        <!-- MODAL Livechat-->
        <modal-livechat ref="chatmodal" v-on:taskdelegated="delegateTask" :isredirecttolivechat="isredirecttolivechat" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :processstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" v-on:complete-task="taskCompleteLiveChat" v-on:push-flash-notification="pushPopFlashMessage" v-on:task-delegated="delegateTask">
        </modal-livechat>
        <!-- MODAL Create Ticket Live Chat-->
        <modal-create-ticket-live-chat ref="chatclosure" :modalname="modalname" v-on:modal-closed="modalClosed" :requestid="requestid" :isincidentlivechat="isincidentlivechat" v-on:push-flash-notification="pushPopFlashMessage" :auth="auth" :servicetype="servicetype">
        </modal-create-ticket-live-chat>
        <!-- MODAL Live Chat Generic-->
        {{-- <modal-livechat-created v-on:taskclosure="closuringTask" v-on:redirect-livechat="redirectToLivechat" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" :isdelegateenabled="isdelegateenabled">
        </modal-livechat-created> --}}
        <!-- MODAL Incident SDS-->
        <modal-create-incident-sds :auth="auth" :source="source" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :modalname="modalname" :processstatus="processstatus" v-on:push-flash-notification="pushPopFlashMessage" v-on:modal-closed="modalClosed" v-on:taskdelegated="delegateTask" v-on:changeadmin="changeAdminDispatcher" :servicetype="servicetype" :isincidentcreation="isincidentcreation" v-on:patching-data="patchingData" :istimemasuk="istimemasuk" v-on:modal-closed="modalClosed">
        </modal-create-incident-sds>
        <!-- MODAL View deprecated for agent-->
        <!-- <modal-view :requestdata="requestdata" :auth="auth" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modalclosed="modalClosed" v-on:requestbooked="bookSelectedRequest">            
        </modal-view> -->
        <!-- MODAL Delegate-->
        <modal-delegate :requestobject="requestobject" :isdelegating="isdelegating" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-delegate>
        <!-- MODAL BDS Error Telegram-->
        <modal-bds-error-tlg v-on:push-flash-notification="pushPopFlashMessage" v-on:taskclosure="closuringTask" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :processstatus="processstatus" :ismodal="ismodal" :modalname="modalname" :isdelegateenabled="isdelegateenabled" :servicetype="servicetype" v-on:redirect-livechat="redirectToLivechat">
        </modal-bds-error-tlg>
        <!-- MODAL Generic-->
        <modal-generic v-on:taskclosure="closuringTask" :isdelegateenabled="isdelegateenabled" v-on:taskdelegated="delegateTask" :isclosuring="isclosuring" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed">
        </modal-generic>
        <!-- MODAL Critical Report-->
        <modal-critical-report v-on:taskclosure="closuringTask" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed">
        </modal-critical-report>
        <!-- MODAL Admin Manual-->
        <modal-admin-manual v-on:taskclosure="closuringTask" :isdelegateenabled="isdelegateenabled" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-admin-manual>
        <!-- MODAL Admin Manual-->
        <modal-admin-manual-extended v-on:taskclosure="closuringTask" :isdelegateenabled="isdelegateenabled" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-admin-manual-extended>
        <!-- MODAL Incoming Incident-->
        <modal-incoming-incident :istimemasuk="istimemasuk" :auth="auth" :isdataloaded="isdataloaded" :isincidentcreation="isincidentcreation" :ismodal="ismodal" :modalname="modalname" :requestdata="requestdata" :servicetype="servicetype" :source="source" :processstatus="processstatus" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-incoming-incident>
        <!-- MODAL Incident Generic-->
        <modal-incident-generic :isdataloaded="isdataloaded" :isdelegateenabled="isdelegateenabled" :ismodal="ismodal" :modalname="modalname" :processstatus="processstatus" :requestdata="requestdata" :requestid="requestid" v-on:modal-closed="modalClosed" v-on:redirect-livechat="redirectToLivechat" v-on:taskclosure="closuringTask"	v-on:taskdelegated="delegateTask">
        </modal-incident-generic>
        <!-- MODAL Incident Closure-->
        <modal-incident-closure :requestobject="requestobject" :isclosuring="isclosuring" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-incident-closure>
        <!-- MODAL Incident Delegate-->
        <modal-incident-delegate :requestobject="requestobject" :isdelegating="isdelegating" :changeadmin="changeadmin" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-incident-delegate>
        <!-- MODAL Incident History-->
        <modal-incident-history :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" v-on:modal-closed="modalClosed">
        </modal-incident-history>
        <!-- MODAL WO History-->
        <modal-wo-history :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" v-on:modal-closed="modalClosed">
        </modal-wo-history>
        <!-- MODAL Admin Dispatcher-->
        <modal-wa-admin-dispatcher ref='wadispatcher' :auth="auth" :source="source" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :modalname="modalname" :processstatus="processstatus" v-on:push-flash-notification="pushPopFlashMessage" v-on:modal-closed="modalClosed" v-on:taskdelegated="delegateTask" v-on:changeadmin="changeAdminDispatcher" :servicetype="servicetype">
        </modal-wa-admin-dispatcher>
        <!-- MODAL DELEGATE TO OTHER-->
        <modal-delegate-toother :requestobject="requestobject" :isdelegating="isdelegating" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-delegate-toother>
        <!-- MODAL ATM LOADING-->
        <modal-atm-loading v-on:push-flash-notification="pushPopFlashMessage" v-on:taskclosure="closuringTask" v-on:taskdelegated="delegateTask" :requestdata="requestdata" :requestid="requestid" :isdataloaded="isdataloaded" :proccessstatus="processstatus" :ismodal="ismodal" :modalname="modalname" v-on:modal-closed="modalClosed" :isdelegateenabled="isdelegateenabled">
        </modal-atm-loading>
        <!-- MODAL KONFIRMASI TRANSAKSI-->
        <modal-konfirmasi-transaksi :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-konfirmasi-transaksi>
        <!-- MODAL TRANSAKSI EMONEY-->
        <modal-transaksi-emoney :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-transaksi-emoney>
        <!-- MODAL AKTIVASI REKENING-->
        <modal-aktivasi-rekening :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-aktivasi-rekening>
        <!-- MODAL LAYANAN KOTRAN 0036/0037-->
        <modal-kotran :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-kotran>
        <!-- MODAL LAYANAN TRENDMICRO-->
        <modal-trendmicro :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-trendmicro>
        <!-- MODAL LAYANAN INFORMASI TOKEN-->
        <modal-informasi-token :modalname="modalname" :requestdata="requestdata" :isdataloaded="isdataloaded" :ismodal="ismodal" :requestid="requestid" :isdelegateenabled="isdelegateenabled" :processstatus="processstatus" v-on:taskdelegated="delegateTask" v-on:modal-closed="modalClosed" v-on:taskclosure="closuringTask">
        </modal-informasi-token>
        <patching-data :isdataloaded="isdataloaded" :modalname="modalname" :ismodal="ismodal" :requestdata="requestdata" v-on:taskclosure="closuringTask"></patching-data>
        <!-- MODAL Incident Delegate-->
        <modal-delegate-multiple :auth="auth" v-on:assigntoother="assignToOther" :modalname="modalname" :multiplerequest="multiplerequest" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-delegate-multiple>
        <!-- MODAL Confirm Admin Manual-->
        <modal-confirm-admin-manual :requestobject="requestobject" :isclosuring="isclosuring" v-on:modal-closed="modalClosed" v-on:push-flash-notification="pushPopFlashMessage">
        </modal-confirm-admin-manual>
        <!-- GALERY -->
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a> <a class="next">›</a> <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
    </div>
</div>
@include('footer')
<script type="text/javascript">
    audiofile = "{{asset('assets/sounds/request_alert.mp3')}}";
    // var socket = new SockJS('http://localhost:8080/itools/websocket');
    // var socket = new SockJS('http://10.254.152.105:8080/itools/websocket'); production
    // var socket = new SockJS('http://10.204.1.16:8080/itools/websocket'); //dev    
    var socket = new SockJS("{{env('MIX_SOCKET_DASHBOARD')}}");    
    stompclient = Stomp.over(socket);
    stompclient.heartbeat.outgoing = 100000;
    stompclient.heartbeat.incoming = 100000;
    stompclient.reconnect_delay = 0;
    stompclient.debug = null;
</script>
<script type="text/javascript" src="../../js/dashboard.js"></script>
<script>
    function ecWorkingTask() {
        if ($(".cWorkingTask").css('display') == 'none') {
            $("#icon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cWorkingTask").show("fast");
        } else {
            $("#icon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cWorkingTask").hide("fast");
        }
    }

    function ecRequestQueue() {
        if ($(".cRequestQueue").css('display') == 'none') {
            $("#iconRQ").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cRequestQueue").show("fast");
        } else {
            $("#iconRQ").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cRequestQueue").hide("fast");
        }
    }

    function ecServiceRequest() {
        if ($(".cServiceRequest").css('display') == 'none') {
            $("#icon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cServiceRequest").show("fast");
        } else {
            $("#icon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cServiceRequest").hide("fast");
        }
    }

    function ecIncident() {
        if ($(".cIncident").css('display') == 'none') {
            $("#iconRQ").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $(".cIncident").show("fast");
        } else {
            $("#iconRQ").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $(".cIncident").hide("fast");
        }
    }
</script>
<!-- <script type="text/javascript" src="../../js/sidebar.js"></script> -->
<style>
    #overflowTest {
        height: 100px;
        overflow-y: scroll;
        overflow-x: hidden;
    }
</style>
@endsection