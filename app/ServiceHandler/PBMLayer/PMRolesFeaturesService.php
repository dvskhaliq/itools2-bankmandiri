<?php

namespace App\ServiceHandler\PBMLayer;

use Illuminate\Database\Eloquent\Collection;
use App\PMRolesFeatures;

interface PMRolesFeaturesService
{
    public function create($payload): ?PMRolesFeatures;
    public function update($payload): ?PMRolesFeatures;
    public function deleteByRole(int $role_id);
    public function delete(int $id);
}
