<?php

namespace App\ServiceHandler\DataLayer;

use App\EmployeeRemedy;

interface EmployeeRemedyService
{
    public function getEmployeeRemedy($employee_id);
    public function getEmployeeRemedyByName($param1, $param2, $full_name = null);
}
