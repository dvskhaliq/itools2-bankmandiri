<?php

namespace App\Http\Controllers;

use App\ServiceHandler\DataLayer\MasterSystemService;
use App\ServiceHandler\DataLayer\SystemAvailabilityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class MasterSystemController extends BaseController
{
    protected $master_system_repo;
    protected $system_repo;

    public function __construct(MasterSystemService $master_system_service, SystemAvailabilityService $system_service)
    {
        $this->master_system_repo = $master_system_service;
        $this->system_repo = $system_service;
        // $this->middleware('auth:problem');
    }

    public function fetchMasterSystems()
    {
        $data = $this->master_system_repo->getMasterSystem();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function createMasterSystem()
    { 
        try {
            $payload = [
            'system_name' => request()->get('system_name')
            ];
            $data = $this->master_system_repo->createMasterSystem($payload);
            $this->system_repo->bulkInsert($data['id']);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function updateMasterSystem()
    {
        try {
            $payload = [
                'id' => request()->get('id'),
                'system_name' => request()->get('system_name'),
                ];
            $data = $this->master_system_repo->updateMasterSystem($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function deleteMasterSystem(int $id){
        $data = $this->master_system_repo->deleteMasterSystem($id);
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

}
