<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{  
    protected $dateFormat = 'Y-m-d H:i:s' ;
    protected $fillable = ['role_users']; 
    protected $table = 'role_users';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function roles(){
        return $this->belongsTo(Roles::class,'role_id','id');
    }
}
