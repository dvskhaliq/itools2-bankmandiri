<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\PMMasterApdyBridgeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PMMasterApdyBridgeController extends BaseController
{
    protected $master_apdy_repo;

    public function __construct(PMMasterApdyBridgeService $master_apdy_service)
    {
        $this->master_apdy_repo = $master_apdy_service;
    }

    public function fetchSystemName()
    {
        $data = $this->master_apdy_repo->getSystemName();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchPMMasterApdyBridge()
    {
        $data = $this->master_apdy_repo->getPMMasterApdyBridge();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function createPMMasterApdyBridge()
    { 
        try {
            $header = request()->get('master_system');
            $payload = [
            'system_id' => $header['id'],
            'site' => request()->get('site'),
            'machine' => request()->get('machine'),
            'type' => request()->get('type'),
            'business_txn_metrics' => request()->get('business_txn_metrics'),
            'rest_url' => request()->get('rest_url'),
            'node' => request()->get('node'),
            ];
            $data = $this->master_apdy_repo->createPMMasterApdyBridge($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function updatePMMasterApdyBridge()
    {
        try {
            $header = request()->get('master_system');
            $payload = [
                'id' => request()->get('id'),
                'system_id' => $header['id'],
                'site' => request()->get('site'),
                'machine' => request()->get('machine'),
                'type' => request()->get('type'),
                'business_txn_metrics' => request()->get('business_txn_metrics'),
                'rest_url' => request()->get('rest_url'),
                'node' => request()->get('node'),
                ];
            $data = $this->master_apdy_repo->updatePMMasterApdyBridge($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function deletePMMasterApdyBridge(int $id){
            $data = $this->master_apdy_repo->deletePMMasterApdyBridge($id);
            return response()->json([
                'status' => true,
                'data' => $data
            ]);
    }
}
