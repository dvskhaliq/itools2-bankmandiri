require('../../bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.VueSelect = require('vue-select');
window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('component-table', () => import('../../components/bds_connection/bds_server/BDSServer.vue'));
Vue.component('component-seleceted', () => import('../../components/bds_connection/bds_server/BDSServerSelected.vue'));
Vue.component('component-insert', () => import('../../components/bds_connection/bds_server/BDSServerInsert.vue'));
Vue.component('ftp-seleceted', () => import('../../components/ftp_connect/FTPServerConnect.vue'));
Vue.component('ftp-file-list', () => import('../../components/ftp_connect/FTPServerFileList.vue'));
Vue.component('modal-ftp', () => import('../../components/ftp_connect/ModalFTP.vue'));
Vue.component('v-select', VueSelect.VueSelect);
Vue.component('flash', () => import('../../components/FlashMessage2.vue'));
Vue.component('confirm-delete', () => import('../../components/administrator_console/bds_server_configuration/BDSServerConfigurationDelete.vue'));

import VuePagination from '../../components/Pagination.vue';
import 'es6-promise/auto'
import Vuex from 'vuex';

import VeeValidate from 'vee-validate';
import moment from "moment";

Vue.filter('mydateformat', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY');
    }
})

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: 'errors', // change if property conflicts
    events: 'input|blur',
    fieldsBagName: 'fields',
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: 'validations', // the nested key under which the validation messages will be located
    inject: true,
    locale: 'en',
    validity: false,
    useConstraintAttrs: true
};
Vue.use(VeeValidate, config);
Vue.use(Vuex);
const bds_server = new Vue({
    el: '#bds_server',
    data: {
        selected_prop: {},
        result: null,
        status_connection: false,
        hasData: false,
        isloaded: false,
        ismodal: false,
        pages: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        search: '',
        ipserver: null,
        modalname: null
    },

    mounted() {
        this.getData();
    },

    components: {
        VuePagination,
    },

    methods: {
        dataSelected(reqResponse) {
            this.hasData = true;
            this.selected_prop = reqResponse;
        },
        getData() {
            this.ismodal = true;
            // axios.get(`/bds_server/page?page=${this.pages.current_page}`)
            axios.get(`/bds_server/search?page=${this.pages.current_page}&search=` + this.search)
                .then((response) => {
                    this.pages = response.data.result;
                    this.result = response.data.result.data;
                    this.status_connection = response.data.statusConnection;
                    if (this.status_connection) {
                        this.targetBranch = response.data.serverConf;
                    }
                }).catch(error => {
                    if (error.response.status === 419 || error.response.status === 401) {
                      window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                      location.reload(false);
                    } else {
                      this.setFlashDanger(error.response.data.message)
                    }
                  });
        },

        setFlashInfo(reqResponse) {
            flash({
                id: "ismodal-info",
                message: reqResponse,
                flashtype: "info"
            });
        },

        setFlashDanger(reqResponse) {
            flash({
                id: "ismodal-danger",
                message: reqResponse,
                flashtype: "danger"
            });
        },

        ftpConnect(reqResponse) {
            this.ipserver = reqResponse.ipserver
            this.modalname = 'modal-ftp'
            this.ismodal = true
        },

        submitConnect(reqResponse) {
            this.ismodal = true;
            axios.post('/bds_server/connect', reqResponse.selected).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    setTimeout(() => {
                        window.location.reload()
                    }, 2500);
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger(error.response.data.message)
                }
              });
            // this.getData();
        },

        submitDisconnect() {
            this.ismodal = true;
            axios.post('/bds_server/disconnect').then(response => {
                if (response.data.status == true) {

                    this.setFlashInfo(response.data.result)
                    setTimeout(() => {
                        window.location.reload()
                    }, 2500);
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger(error.response.data.message)
                }
              });
            // this.getData();
        },

        searchData(reqResponse) {
            this.ismodal = true;
            this.search = reqResponse.search
            axios.get(`/bds_server/search?page=${1}&search=` + this.search)
                .then((response) => {
                    this.pages = response.data.result;
                    this.result = response.data.result.data;
                    this.status_connection = response.data.statusConnection;
                }).catch(error => {
                    if (error.response.status === 419 || error.response.status === 401) {
                      window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                      location.reload(false);
                    } else {
                      this.setFlashDanger(error.response.data.message)
                    }
                  });
        },

        closeModal() {
            this.ipserver = null
            this.modalname = null
            this.ismodal = false
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
        },

        dataSelectedDel(reqResponse) {
            this.alert = true;
            this.ismodal = true;
            axios.post('/bds_server_configuration/selected', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.selected_prop = response.data.result;
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger(error.response.data.message)
                }
              });
        },

        submitDelete(reqResponse) {
            this.ismodal = true;
            this.alert = false;
            axios.post('/bds_server_configuration/submit_delete', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger(error.response.data.message)
                }
              });
        },

        submitUpdate(reqResponse) {
            this.ismodal = true;
            this.alert = false;
            axios.post('/bds_server_configuration/submit_update', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger(error.response.data.message)
                }
              });
        },

        submitInsert(reqResponse) {
            this.ismodal = true;
            this.alert = false;
            axios.post('/bds_server_configuration/submit_insert', reqResponse.data).then(response => {
                if (response.data.status == true) {
                    this.setFlashInfo(response.data.result)
                    this.getData();
                } else {
                    this.setFlashDanger(response.data.result)
                }
            }).catch(error => {
                if (error.response.status === 419 || error.response.status === 401) {
                  window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                  location.reload(false);
                } else {
                  this.setFlashDanger(error.response.data.message)
                }
              });
        }

    },

});
