require('../../bootstrap');

window.Vue = require('vue');
window.flash = function (message) {
    window.events.$emit('flash', message);
}

window.events = new Vue();

Vue.component('components-branch-conf', ()=>  import('../../components/bds_connection/branch_conf/BDSBranchConf.vue'));
Vue.component('flash', ()=>  import('../../components/FlashMessage2.vue'));

import 'es6-promise/auto'
import Vuex from 'vuex';
Vue.use(Vuex);
const bds_branch_conf = new Vue({
    el: '#bds_branch_conf',
    data: {
        data_branch_conf: null,
        ismodal: false,
        isloaded: false,
    },

    mounted() {
        this.getData();
    },

    methods: {
        getData() {
            this.isloaded = true;
            this.ismodal = true;
            axios.get(`/branch_conf/get_data`)
            .then((response) => {
                this.data_branch_conf = response.data.result;
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                flash({
                  id: "error code status",
                  message: error.response.data.message,
                  flashtype: "danger"
                });
              }
            });
        },
    },

});
