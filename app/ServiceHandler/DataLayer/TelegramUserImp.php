<?php

namespace App\ServiceHandler\DataLayer;

use App\ChatSubscribedUser as TelegramUser;
use App\Inbox;

class TelegramUserImp implements TelegramUserService
{
    public $status = false;
    public $message = 'this is default message';

    public function find(Int $telegram_id): TelegramUser
    {
        return TelegramUser::find($telegram_id);
    }

    // public function updateDefault(Int $telegram_id): TelegramUser
    // {
    //     try {
    //         $csu = TelegramUser::find($telegram_id);
    //         $csu->live_chat_request_id = null;
    //         $csu->current_level = 'level0';
    //         $csu->current_level_id = 2;
    //         $csu->current_active_mode = 'MENU';
    //         $csu->total_unrecognized_message = 0;
    //         $csu->save();
    //         return $csu;
    //     } catch (\Throwable $th) {
    //         $this->status = false;
    //         $this->message = 'Unable to update telegram user to default' . $th->__toString();
    //         return $this;
    //     }
    // }

    public function hasActiveChat(String $person_id): bool
    {
        return TelegramUser::where('employee_remedy_id', $person_id)
            ->where('current_active_mode', 'LIVECHAT')
            ->whereNotNull('live_chat_request_id')
            ->exists();
    }

    public function updateToChat(Inbox $request): TelegramUser
    {
        $csu = TelegramUser::find($request->TelegramSenderId);
        $csu->live_chat_request_id = $request->id;
        $csu->current_active_mode = 'LIVECHAT';
        $csu->total_unrecognized_message = 0;
        $csu->save();
        return $csu;
    }

    public function updateToMenu(Inbox $request): TelegramUser
    {
        $csu = TelegramUser::find($request->TelegramSenderId);
        $csu->live_chat_request_id = null;
        $csu->current_level = 'level0';
        $csu->current_level_id = 2;
        $csu->current_active_mode = 'MENU';
        $csu->total_unrecognized_message = 0;
        $csu->save();
        return $csu;
    }
}
