<?php

namespace App\ServiceHandler\DataLayer;

use App\SupportGroupAssigneeRemedy as SupportUser;

class SupportUserImp implements SupportUserService
{
    public function findByUserId(?Int $user_id):? SupportUser
    {
        return SupportUser::with('user')->whereHas('user', function ($q) use ($user_id) {
            $q->where('id', $user_id);
        })->first();
    }

    public function findByPersonIdGroupId(String $person_id, String $group_id): SupportUser
    {
        return SupportUser::with('user')->where('employee_remedy_id', $person_id)->where('support_group_id', $group_id)->first();
    }

    public function findByCompanyOrgGroupName($company_name, $organization_name, $group_name, $assignee_name = null): ?SupportUser
    {
        try {
            if($assignee_name==null){
                return SupportUser::where('company', $company_name)
                ->where('organization', $organization_name)
                ->where('support_group_name', $group_name)
                ->first();
            }else{
                return SupportUser::where('company', $company_name)
                ->where('organization', $organization_name)
                ->where('support_group_name', $group_name)
                ->where('full_name', $assignee_name)
                ->first();
            }
        } catch (\Exception $th) {
            return null;
        }
    }

    // public function findByFullNameGroupName(array $assignee): ?SupportUser//Not Used
    // {
    //     if ($assignee['assignee_fullname'] == null) {
    //         return SupportUser::where('company', $assignee['assignee_company'])
    //             ->where('organization', $assignee['assignee_organization'])
    //             ->where('support_group_name', $assignee['assignee_group'])
    //             ->first();
    //     } else {
    //         return SupportUser::where('company', $assignee['assignee_company'])
    //             ->where('organization', $assignee['assignee_organization'])
    //             ->where('support_group_name', $assignee['assignee_group'])
    //             ->where('full_name', $assignee['assignee_fullname'])
    //             ->first();
    //     }
    // }
}
