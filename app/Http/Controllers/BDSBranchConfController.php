<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use View;

class BDSBranchConfController extends BaseController
{

    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        return View::make('bds_connection/bds_branch_conf/branch_conf');
    }

    public function getData()
    {
        $result = $this->bdsServerService->initBdsConnection()->table('BrchConf')->select('BrhCode', 'BrhCtrlUnit', 'BrhName', 'BrhAddr1', 'BrhAddr2', 'BrhAddr3', 'BrhAddr4', 'SODInd', 'EODInd', 'CashOutlet', 'TradeDate')->get();

        return response()->json(['result' => $result]);
    }
}
