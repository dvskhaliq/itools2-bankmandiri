/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 **/

require("../../bootstrap");

window.Vue = require("vue");

window.events = new Vue();

window.flash = function(message) {
    window.events.$emit("flash", message);
};

window.dismissflash = function() {
    window.events.$emit("dismissflash");
};

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 **/

Vue.component("chart-table-1", () =>
    import("../../components/reports/public/template/ChartTable1Report.vue")
);

Vue.component("chart-table-2", () =>
    import("../../components/reports/public/template/ChartTable2Report.vue")
);

Vue.component("chart-table-3", () =>
    import("../../components/reports/public/template/ChartTable3Report.vue")
);

Vue.component("chart-line-1", () =>
    import("../../components/reports/public/template/ChartLine1Report.vue")
);

Vue.component("chart-web-1", () =>
    import("../../components/reports/public/template/ChartWeb1Report.vue")
);

Vue.component("chart-bar-1", () =>
    import("../../components/reports/public/template/ChartBar1Report.vue")
);

Vue.component("chart-bar-2", () =>
    import("../../components/reports/public/template/ChartBar2Report.vue")
);

Vue.component("flash", () => import("../../components/FlashMessage.vue"));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 **/

import "es6-promise/auto";
import Vuex from "vuex";
import moment from "moment";
Vue.filter("mydateformat", function(value) {
    if (value) {
        return moment(String(value)).format("DD/MM/YYYY HH:mm:ss");
    }
});
Vue.use(Vuex);
const app = new Vue({
    el: "#report_summary",
    data: {
        categorydata: total_request_category,
        channeldata: total_request_channel,
        typedata: total_request_type,
        totaldata: total_request_summary,
        chartreport: total_chart,
        topfivechannel: top_five_channel
    },
    methods: {}
});
