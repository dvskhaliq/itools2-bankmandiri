<?php
namespace App\ServiceHandler\IntegrationLayer;

interface AppdynamicsService{

    public function fetchData($rest_url, $appdy);
    public function fetchDataKoprats($target_url);

}
