import "../../bootstrap";
import "vuetify/dist/vuetify.min.css";
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from "vuex";
import VeeValidate from "vee-validate";
import Vue from "vue";
import Loading from "vue-loading-overlay";
import Vuetify from "vuetify";

window.Vue = Vue;
window.VueSelect = require("vue-select");
window.events = new Vue();

window.flash = function(message) {
    window.events.$emit("flash", message);
};

Vue.component("componen-kartu-instan", () =>
    import(
        "../../components/bds_connection/document_inventory/kartu_instan/BDSKartuInstan.vue"
    )
);
Vue.component("componen-form", () =>
    import(
        "../../components/bds_connection/document_inventory/kartu_instan/SetBDSKartuInstan.vue"
    )
);
Vue.component("v-select", VueSelect.VueSelect);
Vue.component("flash", () => import("../../components/FlashMessage2.vue"));
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Loading);

const config = {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: "errors", // change if property conflicts
    events: "input|blur",
    fieldsBagName: "fields",
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: "validations", // the nested key under which the validation messages will be located
    inject: true,
    locale: "en",
    validity: false,
    useConstraintAttrs: true
};

Vue.use(VeeValidate, config);

const vuetify = new Vuetify();

const bds_kartu_instan = new Vue({
    el: "#bds_kartu_instan",
    vuetify,
    data: {
        data_kartu_instan: null,
        ismodal: false,
        list_selected: null,
        data_holder: [],
        isloaded: false,
        selectdoc: "List Doc. Belum Kembali"
    },

    mounted() {
        this.getData();
        this.listHolder();
    },

    methods: {
        getData(spoilt = null) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.isloaded = true;
            this.ismodal = true;
            let onErrorResponse = function(error) {
                if (
                    error.response.status === 419 ||
                    error.response.status === 401
                ) {
                    window.alert(
                        "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                    );
                    location.reload(false);
                } else {
                    flash({
                        id: "error code status",
                        message: error.response.data.message,
                        flashtype: "danger"
                    });
                }
                loader.hide();
            };

            console.log({ isiSpoilt: spoilt });
            if (spoilt === null) {
                axios
                    .get(`/kartu_instan/page`)
                    .then(response => {
                        this.data_kartu_instan = response.data;
                        loader.hide();
                    })
                    .catch(error => {
                        onErrorResponse(error);
                    });
            } else {
                console.log(
                    { isSpoiltSelected: spoilt.selected },
                    "SPOILED TESTED"
                );

                if (spoilt.selected == "List Doc. Belum Kembali") {
                    var data = { spoilt: "N" };
                } else {
                    var data = { spoilt: "Y" };
                }
                axios
                    .post("/kartu_instan/filter", data)
                    .then(response => {
                        this.data_kartu_instan = response.data;
                        loader.hide();
                    })
                    .catch(error => {
                        onErrorResponse(error);
                    });
            }
        },

        listHolder() {
            axios
                .get("/user_profile/list_holder")
                .then(response => {
                    this.data_holder = response.data;
                    this.data_holder.unshift("SPVKMI");
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                });
        },

        submitAll(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.isloaded = true;
            this.ismodal = true;
            var data = {
                kartu_instan: reqResponse.data
            };
            axios
                .post("/kartu_instan/submit_all", data)
                .then(response => {
                    if (response.data.status == true) {
                        this.getData();
                        flash({
                            id: "success",
                            message: response.data.result,
                            flashtype: "info"
                        });
                    } else {
                        flash({
                            id: "warning",
                            message: response.data.result,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                });
        },

        submitForm(reqResponse) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.isloaded = true;
            this.ismodal = true;
            if (reqResponse.holder == "SPVKMI") {
                var data = {
                    serialNo: reqResponse.serialNo,
                    holder: reqResponse.holder,
                    allocatedToday: "N"
                };
            } else {
                var data = {
                    serialNo: reqResponse.serialNo,
                    holder: reqResponse.holder.UserID,
                    allocatedToday: "Y"
                };
            }
            console.log({ selected: this.selectdoc });
            axios
                .post("/kartu_instan/submit", data)
                .then(response => {
                    if (response.data.status == true) {
                        if (this.selectdoc == "List Doc. Belum Kembali") {
                            this.getData();
                        } else {
                            this.getData({ selected: this.selectdoc });
                        }

                        flash({
                            id: "success",
                            message: response.data.result,
                            flashtype: "info"
                        });
                    } else {
                        flash({
                            id: "warning",
                            message: response.data.result,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                    loader.hide();
                });
        },

        listSelected(reqResponse) {
            this.list_selected = reqResponse.serial_no;
        },

        onChangeFilter(reqResponse) {
            this.selectdoc = reqResponse.selected;
            this.getData(reqResponse);
            // let loader = this.$loading.show({
            //     loader: "dots"
            // });
            // if (event != null) {
            //     if (reqResponse.selected == "List Doc. Belum Kembali") {
            //         var data = { spoilt: "N" };
            //     } else {
            //         var data = { spoilt: "Y" };
            //     }
            //     axios
            //         .post("/kartu_instan/filter", data)
            //         .then(response => {
            //             this.data_kartu_instan = response.data;
            //             loader.hide();
            //         })
            //         .catch(error => {
            //             if (
            //                 error.response.status === 419 ||
            //                 error.response.status === 401
            //             ) {
            //                 window.alert(
            //                     "Mohon maaf session anda telah habis, tekan ok untuk relogin"
            //                 );
            //                 location.reload(false);
            //             } else {
            //                 flash({
            //                     id: "error code status",
            //                     message: error.response.data.message,
            //                     flashtype: "danger"
            //                 });
            //             }
            //             loader.hide();
            //         });
            // }
        }
    }
});
