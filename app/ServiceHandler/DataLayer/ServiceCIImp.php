<?php

namespace App\ServiceHandler\DataLayer;

use App\ServiceCI;
use Illuminate\Support\Facades\DB;

class ServiceCIImp implements ServiceCIService
{
    /**
     * @Override
     */
    public function getServices()
    {
        return  ServiceCI::orderBy('name', 'asc')->pluck('name');
    }
}
