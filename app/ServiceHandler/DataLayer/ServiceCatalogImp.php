<?php

namespace App\ServiceHandler\DataLayer;

use App\ServiceCatalog as Catalog;
use App\ServiceChannel;

class ServiceCatalogImp implements ServiceCatalogService
{
    public function find(Int $catalog_id): ?Catalog
    {
        return Catalog::with('servicechannel.servicetype')->find($catalog_id);
    }

    public function findBySummaryAndTier(String $summary, String $pro_cat_tier1, String $pro_cat_tier2, String $pro_cat_tier3, String $opr_cat_tier1, String $opr_cat_tier2, String $opr_cat_tier3): ?Catalog
    {
        try {
            return Catalog::with('servicechannel.servicetype')
                ->where("name", $summary)
                ->where("mapper_prod_tier_1", $pro_cat_tier1)
                ->where("mapper_prod_tier_2", $pro_cat_tier2)
                ->where("mapper_prod_tier_3", $pro_cat_tier3)
                ->where("mapper_opr_tier_1", $opr_cat_tier1)
                ->where("mapper_opr_tier_2", $opr_cat_tier2)
                ->where("mapper_opr_tier_3", $opr_cat_tier3)
                ->first();
        } catch (\Exception $th) {
            return null;
        }
    }

    public function findByTier(String $pro_cat_tier1, String $pro_cat_tier2, String $pro_cat_tier3, String $opr_cat_tier1, String $opr_cat_tier2, String $opr_cat_tier3): ?Catalog
    {
        try {
            return Catalog::with('servicechannel.servicetype')
                ->where("mapper_prod_tier_1", $pro_cat_tier1)
                ->where("mapper_prod_tier_2", $pro_cat_tier2)
                ->where("mapper_prod_tier_3", $pro_cat_tier3)
                ->where("mapper_opr_tier_1", $opr_cat_tier1)
                ->where("mapper_opr_tier_2", $opr_cat_tier2)
                ->where("mapper_opr_tier_3", $opr_cat_tier3)
                ->first();
        } catch (\Exception $th) {
            return null;
        }
    }

    public function searchServiceCatalog($name, $channel, $service_type){
        $s_channel = ServiceChannel::where('name', $channel)->where('service_type_name', $service_type)->first();
        $data = Catalog::with('servicechannel.servicetype')
            ->where('name', 'ILIKE', '%' . $name . '%')
            ->where('service_catalog_type', 'IN')
            ->where('service_channel_name', $s_channel->encoding_name)
            ->whereHas('servicechannel', function ($q) use ($service_type) {
                $q->where('service_type_name', $service_type);
            })
            ->whereNotNull('mapper_prod_tier_1')
            ->whereNotNull('mapper_opr_tier_1')
            ->get();
        return response()->json($data);
    }
}
