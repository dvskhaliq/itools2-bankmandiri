require("../../bootstrap");

window.Vue = require("vue");
window.events = new Vue();
window.flash = function(message) {
    window.events.$emit("flash", message);
};
window.dismissflash = function() {
    window.events.$emit("dismissflash");
};
window.refreshPendingCount = function(type) {
    window.events.$emit("refreshCount", type);
};

window.testEvent = function() {
    console.log("test");
};

Vue.component("chat-messages-log", () =>
    import("../../components/dashboard/childs/livechat/ChatLog.vue")
);
Vue.component("chat-messages", () =>
    import("../../components/dashboard/childs/livechat/ChatMessage.vue")
);
Vue.component("chat-compose", () =>
    import("../../components/dashboard/childs/livechat/ChatCompose.vue")
);
Vue.component("request-queue", () =>
    import("../../components/dashboard/RequestQueueSD.vue")
);
Vue.component("working-task", () =>
    import("../../components/dashboard/WorkingRequestSD.vue")
);
Vue.component("modal-livechat", () =>
    import("../../components/dashboard/childs/livechat/ModalLiveChat.vue")
);
Vue.component("modal-view", () =>
    import("../../components/dashboard/childs/view/ModalView.vue")
);
Vue.component("modal-generic", () =>
    import("../../components/dashboard/childs/generic/ModalGeneric.vue")
);
Vue.component("modal-workorder-closure", () =>
    import(
        "../../components/dashboard/childs/closure/ModalWorkorderClosure.vue"
    )
);
Vue.component("spinner-loader", () =>
    import("../../components/dashboard/SpinnerCenter.vue")
);
Vue.component("spinner-data-loading", () =>
    import("../../components/dashboard/SpinnerDataLoading.vue")
);
Vue.component("modal-delegate", () =>
    import("../../components/dashboard/childs/delegate/ModalDelegate.vue")
);
Vue.component("modal-bds-error-tlg", () =>
    import("../../components/dashboard/childs/bds_error/ModalBDSError.vue")
);
Vue.component("modal-critical-report", () =>
    import(
        "../../components/dashboard/childs/critical_report/ModalCriticalReport.vue"
    )
);
Vue.component("modal-create-ticket-live-chat", () =>
    import(
        "../../components/dashboard/childs/create_ticket/ModalTaskCreateTicketLiveChat.vue"
    )
);
Vue.component("task-incoming", () =>
    import("../../components/dashboard/TaskIncoming.vue")
);
Vue.component("modal-incoming-incident", () =>
    import(
        "../../components/dashboard/childs/create_ticket/ModalCreateIncomingIncident.vue"
    )
);
// Vue.component(
//     "modal-attach-file",
//     () => import("../../components/dashboard/childs/create_ticket/ModalAttachFi
// );
Vue.component("modal-admin-manual", () =>
    import(
        "../../components/dashboard/childs/admin_manual/ModalAdminManual.vue"
    )
);

Vue.component("modal-admin-manual-extended", () =>
    import(
        "../../components/dashboard/childs/admin_manual/ModalAdminManualExtended.vue"
    )
);


Vue.component("modal-incident-history", () =>
    import("../../components/dashboard/childs/incident/ModalIncident.vue")
);
Vue.component("modal-incident-delegate", () =>
    import(
        "../../components/dashboard/childs/delegate/ModalDelegateIncident.vue"
    )
);
Vue.component("modal-delegate-multiple", () =>
    import(
        "../../components/dashboard/childs/delegate/ModalDelegateMultiple.vue"
    )
);
Vue.component("modal-incident-closure", () =>
    import("../../components/dashboard/childs/closure/ModalIncidentClosure.vue")
);
Vue.component("modal-incident-generic", () =>
    import(
        "../../components/dashboard/childs/incident/ModalIncidentGeneric.vue"
    )
);
Vue.component("modal-wo-history", () =>
    import("../../components/dashboard/childs/generic/ModalWOGeneric.vue")
);
Vue.component("count-pending-ticket", () =>
    import("../../components/sidebar/CountPendingTicket.vue")
);
Vue.component("flash", () => import("../../components/FlashMessage.vue"));
Vue.component("stats-chart-daily", () =>
    import("../../components/dashboard/ChartDailyTaskReport.vue")
);
Vue.component("job-history-wo", () =>
    import("../../components/dashboard/JobHistory_WO.vue")
);
Vue.component("job-history-inc", () =>
    import("../../components/dashboard/JobHistory_INC.vue")
);
Vue.component("v-select", require("vue-select").VueSelect);
Vue.component("stats-chart-weekly", () =>
    import("../../components/dashboard/ChartWeeklyTaskReport.vue")
);
Vue.component("channel-chart-daily", () =>
    import("../../components/dashboard/ChartDailyChannelReport.vue")
);
Vue.component("channel-chart-weekly", () =>
    import("../../components/dashboard/ChartWeeklyChannelReport.vue")
);
Vue.component("modal-wa-admin-dispatcher", () =>
    import(
        "../../components/dashboard/childs/create_ticket/whatsapp/AdminDispatcher.vue"
    )
);
Vue.component("modal-create-incident-sds", () =>
    import(
        "../../components/dashboard/childs/create_ticket/ModalCreateIncidentSDS.vue"
    )
);

Vue.component("modal-delegate-toother", () =>
    import(
        "../../components/dashboard/childs/create_ticket/whatsapp/ModalDelegateOtherGroup.vue"
    )
);

Vue.component("modal-atm-loading", () =>
    import("../../components/dashboard/childs/atm_loading/ModalAtmLoading.vue")
);

Vue.component("modal-konfirmasi-transaksi", () =>
    import(
        "../../components/dashboard/childs/modal_wo/ModalKonfirmasiTransaksi.vue"
    )
);
Vue.component("modal-transaksi-emoney", () =>
    import(
        "../../components/dashboard/childs/modal_wo/ModalTransaksiEmoney.vue"
    )
);
Vue.component("modal-aktivasi-rekening", () =>
    import(
        "../../components/dashboard/childs/modal_wo/ModalAktivasiRekening.vue"
    )
);
Vue.component("modal-kotran", () =>
    import("../../components/dashboard/childs/modal_wo/ModalKotran.vue")
);
Vue.component("modal-trendmicro", () =>
    import("../../components/dashboard/childs/modal_wo/ModalTrendmicro.vue")
);
Vue.component("modal-informasi-token", () =>
    import("../../components/dashboard/childs/modal_wo/ModalInformasiToken.vue")
);
Vue.component("patching-data", () =>
    import("../../components/dashboard_sds/ModalPatchingData.vue")
);
// Vue.component(
//     "modal-livechat-created",
//     () => import("../../components/dashboard/childs/livechat/ModalLiveChatGener
// );
Vue.component("modal-confirm-admin-manual", () =>
    import(
        "../../components/dashboard/childs/admin_manual/ModalConfirmAdminManual.vue"
    )
);

import "es6-promise/auto";
import Vuex from "vuex";
import VeeValidate from "vee-validate";
import VueClipboard from "vue-clipboard2";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import moment from "moment";

Vue.filter("mydateformat", function(value) {
    if (value) {
        return moment(String(value)).format("DD/MM/YYYY HH:mm:ss");
    }
});
Vue.use(VeeValidate, {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: "errors", // change if property conflicts
    events: "input|blur",
    fieldsBagName: "fields",
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: "validations", // the nested key under which the validation messages will be located
    inject: true,
    locale: "en",
    validity: false,
    useConstraintAttrs: true
});
Vue.use(VueClipboard);
Vue.use(Vuex);
Vue.use(Loading);

const sidebar = new Vue({
    el: "#sidebar",
    data: {
        countpendingticket: 0,
        isconnect: false
    },
    methods: {
        initPendingTicketCount() {
            axios
                .get("/side/notify/pendingticket")
                .then(res => {
                    this.countpendingticket = res.data.message;
                    console.log(res.data.message, "count");
                })
                .catch(error => {
                    if (error.res.status === 419 || error.res.status === 401) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                });
        },
        refreshCount(req) {
            switch (req.type) {
                case "pending-ticket":
                    this.initPendingTicketCount();
                    break;
            }
        }
    }
});

window.app = new Vue({
    el: "#app",
    data: {
        selecteddate: "",
        personalstats: chart,
        requestdata: [],
        requestobject: {},
        requestid: 0,
        processstatus: "",
        requests: requestQueue,
        tasks: workingTask,
        inhistory: inHistory,
        wohistory: woHistory,
        auth: auth,
        stomps: stompclient,
        ismodal: false,
        isdelegating: false,
        isclosuring: false,
        isdataloaded: false,
        isresponsereceived: false,
        isincidentcreation: false,
        isincidentlivechat: false,
        isredirecttolivechat: false,
        isdelegateenabled: auth.is_delegate_enabled,
        modalname: "",
        source: null,
        servicetype: serviceType,
        changeadmin: false,
        usedchannel: "ALL",
        usedprocessstatus: "INPROGRESS",
        multiplerequest: {},
        istimemasuk: "Y-m-d H:i:s"
    },
    mounted() {
        let test = process.env.REMEDY_BASE_URL;
        console.log({ remedy_url: test });
    },
    methods: {
        loadFilterChannelName(req) {
            this.usedchannel = req.usedChannel;
            this.requests = req.dataKeep;
        },
        loadFilterProcessStatus(req) {
            this.usedprocessstatus = req.usedProcessStatus;
            this.tasks = req.wtaskData;
        },
        patchingData(req) {
            this.requestdata = req.request;
            this.ismodal = true;
            this.modalname = req.modalname;
            this.isdataloaded = req.isdataloaded;
        },

        createStagingIncident(req) {
            this.ismodal = true;
            this.isincidentcreation = req.isincidentcreation;
            this.source = req.source;
            this.modalname = req.modalname;
            this.processstatus = req.processstatus;
            this.isdataloaded = req.isdataloaded;
            this.requestdata = req.requestdata;
        },

        redirectToLivechat(req) {
            this.isredirecttolivechat = req.isredirecttolivechat;
            this.modalname = req.modalname;
            this.requestdata = req.requestdata;
            this.processstatus = req.requestdata.ProcessStatus;
            this.ismodal = true;
        },

        popFlashMessage(id, message, type) {
            setTimeout(() => {
                flash({
                    id: id,
                    message: message,
                    flashtype: type
                });
            }, 500);
        },

        taskCompleteLiveChat(req) {
            this.requestid = req.requestid;
            this.isincidentlivechat = req.isincidentlivechat;
            this.modalname = req.modalname;
            this.ismodal = true;
        },

        pushPopFlashMessage(req) {
            this.modalClosed();
            if (req.status) {
                if (typeof req.wsdlStatus !== "undefined") {
                    if (req.wsdlStatus) {
                        this.popFlashMessage(req.id, req.message, "success");
                    } else {
                        this.popFlashMessage(req.id, req.message, "warning");
                        refreshPendingCount("pending-ticket");
                    }
                } else {
                    this.popFlashMessage(req.id, req.message, "info");
                }
            } else {
                this.popFlashMessage(req.id, req.message, "danger");
                refreshPendingCount("pending-ticket");
            }
        },

        closuringTask(req) {
            this.requestobject = req.request;
            this.isclosuring = req.isclosuring;
            this.modalname = req.modalname;
            this.ismodal = true;
        },

        changeAdminDispatcher(req) {
            this.requestobject = req.request;
            this.isdelegating = req.isdelegating;
            this.modalname = req.modalname;
            this.ismodal = true;
            this.changeadmin = true;
        },

        delegateTask(req) {
            this.requestobject = req.request;
            this.isdelegating = req.isdelegating;
            this.modalname = req.modalname;
            this.ismodal = true;
            this.changeadmin = false;
        },

        modalClosed() {
            this.isdataloaded = false;
            this.processstatus = "";
            this.requestid = 0;
            this.ismodal = false;
            this.modalname = "";
            this.isclosuring = false;
            this.isdelegating = false;
            this.isincidentcreation = false;
            this.isincidentlivechat = false;
            this.isredirecttolivechat = false;
            this.source = null;
        },

        selectedTicket(req) {
            this.multiplerequest = req.multipleSelected;
            this.modalname = req.modalname;
        },
        assignToOther(req) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            this.ismodal = false;
            req.data.forEach(function(item) {
                var requestParam = {
                    id: item.id,
                    assigneeId: item.assignee.id,
                    serviceCatalogId: item.serviceCatalogId
                };
                axios
                    .post("/dashboard/requestqueue/pick", requestParam)
                    .then(response => {
                        if (response.data.status == false) {
                            if (response.data.processStatus != "BOOKED") {
                                flash({
                                    id: req.id,
                                    message: response.data.message,
                                    flashtype: "danger"
                                });
                                refreshPendingCount();
                            }
                            loader.hide();
                        } else {
                            if (response.data.processStatus == "INPROGRESS") {
                                if (response.data.wsdlStatus) {
                                    flash({
                                        id: req.id,
                                        message: response.data.message,
                                        flashtype: "success"
                                    });
                                } else {
                                    flash({
                                        id: req.id,
                                        message: response.data.message,
                                        flashtype: "warning"
                                    });
                                    refreshPendingCount();
                                }
                            }
                            loader.hide();
                        }
                    })
                    .catch(error => {
                        if (
                            error.response.status === 419 ||
                            error.response.status === 401
                        ) {
                            window.alert(
                                "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                            );
                            location.reload(false);
                        } else {
                            this.popFlashMessage(
                                "catch error code",
                                error.response.data.message,
                                "danger"
                            );
                        }
                        loader.hide();
                    });
            });
        },
        loadSelectedRequest(req) {
            console.log(process.env.REMEDY_URL)
            console.log(process.env.TICKET_URL)
            this.isredirecttolivechat = req.isredirecttolivechat;
            axios
                .post("/dashboard/requestqueue/pick", req)
                .then(response => {
                    if (response.data.status == false) {
                        this.isdataloaded = response.data.status;
                        this.processstatus = response.data.processStatus;
                        this.ismodal = true;
                        if (this.processstatus != "BOOKED") {
                            this.popFlashMessage(
                                req.id,
                                response.data.message,
                                "danger"
                            );
                            refreshPendingCount();
                        }
                    } else {
                        this.processstatus = response.data.processStatus;
                        this.requestdata = response.data.responsedata;
                        this.requestid = req.id;
                        this.isdataloaded = true;
                        this.ismodal = true;
                        this.modalname = req.modalname;
                        if (response.data.processStatus == "INPROGRESS") {
                            if (response.data.wsdlStatus) {
                                this.popFlashMessage(
                                    req.id,
                                    response.data.message,
                                    "success"
                                );
                            } else {
                                if (response.data.status == true) {
                                    this.popFlashMessage(
                                        req.id,
                                        response.data.message,
                                        "success"
                                    );
                                } else {
                                    this.popFlashMessage(
                                        req.id,
                                        response.data.message,
                                        "warning"
                                    );
                                    refreshPendingCount();
                                }
                            }
                        }
                        if (this.modalname == "modal-admin-dispatcher") {
                            this.source = "WA";
                        }
                        if (this.modalname == "modal-livechat") {
                            this.scrollToEnd();
                        }
                    }
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.popFlashMessage(
                            "catch error code",
                            error.response.data.message,
                            "danger"
                        );
                    }
                });
        },

        scrollToEnd: function() {
            setTimeout(() => {
                if (this.processstatus == "CLOSED" && this.isdataloaded) {
                    var container = this.$refs.chatmodal.$refs.chatlogclosed
                        .$refs.chatcontainer;
                    container.scrollTop = container.scrollHeight;
                } else if (this.isdataloaded) {
                    var container = this.$refs.chatmodal.$refs.chatlogactive
                        .$refs.chatcontainer;
                    container.scrollTop = container.scrollHeight;
                }
            }, 800);
        },

        searchTaskReportByDate(selecteddate) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            axios
                .post("/dashboard/stats/search/taskbydate", {
                    selecteddate: selecteddate
                })
                .then(response => {
                    this.personalstats = response.data;
                    loader.hide();
                });
        }
    },

    created() {
        this.stomps.connect(
            {},
            function(e) {
                var audio = new Audio(audiofile);
                app.isresponsereceived = true;
                flash({
                    id: "websocket-success",
                    message:
                        "Connection to backend established, click this alert to ignore!!",
                    flashtype: "success"
                });

                stompclient.subscribe("/request/drop/", function(stream) {
                    var data = JSON.parse(stream.body);
                    var rqObj = app.requests.find(item => item.id === data.id);
                    var wtObj = app.tasks.find(item => item.id === data.id);
                    var rqIdx = null;
                    var wtIdx = null;
                    if (typeof rqObj !== "undefined") {
                        console.log("request was deleted");
                        rqIdx = app.requests.indexOf(rqObj);
                        app.$delete(app.requests, rqIdx);
                    }
                    if (typeof wtObj !== "undefined") {
                        console.log("request was deleted");
                        wtIdx = app.tasks.indexOf(wtObj);
                        app.$delete(app.tasks, wtIdx);
                    }
                });

                //ON GOING
                stompclient.subscribe("/request/feed/", function(stream) {
                    var rqObj = {};
                    var wtObj = {};
                    var data = JSON.parse(stream.body);
                    console.log(data);

                    if (app.usedchannel !== "ALL") {
                        rqObj = app.requests.find(
                            item =>
                                item.id === data.id &&
                                item.serviceChannelName === app.usedchannel
                        );
                        console.log("RQ filter " + data.serviceChannelName);
                        console.log(rqObj);
                    } else {
                        rqObj = app.requests.find(item => item.id === data.id);
                        console.log("RQ if ALL " + data.serviceChannelName);
                        console.log(rqObj);
                    }

                    if (app.usedprocessstatus !== "ALL") {
                        wtObj = app.tasks.find(
                            item =>
                                item.id === data.id &&
                                item.processStatus === app.usedprocessstatus
                        );
                        console.log("WT filter " + data.processStatus);
                        console.log(wtObj);
                    } else {
                        wtObj = app.tasks.find(item => item.id === data.id);
                        console.log("WT if ALL " + data.processStatus);
                        console.log(wtObj);
                    }

                    if (data.serviceCatalogId == 88) {
                        console.log("Ini serviceCatalogId 88");
                    }

                    var rqIdx = null;
                    var wtIdx = null;

                    if (typeof rqObj !== "undefined") {
                        rqIdx = app.requests.indexOf(rqObj);
                        switch (data.processStatus) {
                            case "INPROGRESS": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (
                                    app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId &&
                                    app.usedprocessstatus !==
                                        data.processStatus &&
                                    app.usedprocessstatus != "ALL"
                                ) {
                                    console.log("moved to my task #");
                                } else if (
                                    app.auth.employee_remedy_id ===
                                    data.employeeAssigneeId
                                ) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "PENDING": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (
                                    app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId &&
                                    app.usedprocessstatus !==
                                        data.processStatus &&
                                    app.usedprocessstatus != "ALL"
                                ) {
                                    console.log("moved to my task #");
                                } else if (
                                    app.auth.employee_remedy_id ===
                                    data.employeeAssigneeId
                                ) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "WAITINGAPR": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (
                                    app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId &&
                                    app.usedprocessstatus !==
                                        data.processStatus &&
                                    app.usedprocessstatus != "ALL"
                                ) {
                                    console.log("moved to my task #");
                                } else if (
                                    app.auth.employee_remedy_id ===
                                    data.employeeAssigneeId
                                ) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "PLANNING": {
                                console.log("remove from request queue");
                                app.$delete(app.requests, rqIdx);
                                if (
                                    app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId &&
                                    app.usedprocessstatus !==
                                        data.processStatus &&
                                    app.usedprocessstatus != "ALL"
                                ) {
                                    console.log("moved to my task");
                                } else if (
                                    app.auth.employee_remedy_id ===
                                    data.employeeAssigneeId
                                ) {
                                    console.log("moved to my task");
                                    app.tasks.push(data);
                                }
                                break;
                            }
                            case "CLOSED": {
                                console.log("remove from queue to history");
                                app.$delete(app.requests, rqIdx);
                                break;
                            }
                            case "REJECT": {
                                console.log("remove from my task due rejected");
                                app.$delete(app.requests, rqIdx);
                                break;
                            }
                            case "QUEUED": {
                                console.log("MASUK QUEUE");
                                if (data.serviceCatalogId == 88) {
                                    console.log("CLEAR SERVICEID 88");
                                    app.$delete(app.requests, rqIdx);
                                }
                                break;
                            }
                        }
                    } else {
                        if (typeof wtObj !== "undefined") {
                            wtIdx = app.tasks.indexOf(wtObj);
                            switch (data.processStatus) {
                                case "QUEUED": {
                                    if (
                                        app.auth.employee_remedy_id !==
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (
                                        app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("remove from my task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (
                                        data.serviceTypeEncodingname ==
                                        serviceType
                                    ) {
                                        console.log("added to request queue");
                                        app.requests.push(data);
                                        audio.play();
                                    }
                                    break;
                                }
                                case "INPROGRESS": {
                                    if (
                                        app.auth.employee_remedy_id !==
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (
                                        app.auth.employee_remedy_id ===
                                            data.employeeAssigneeId &&
                                        app.usedprocessstatus !==
                                            data.processStatus &&
                                        app.usedprocessstatus != "ALL"
                                    ) {
                                        console.log("update my task #");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (
                                        app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "PENDING": {
                                    if (
                                        app.auth.employee_remedy_id !==
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (
                                        app.auth.employee_remedy_id ===
                                            data.employeeAssigneeId &&
                                        app.usedprocessstatus !==
                                            data.processStatus &&
                                        app.usedprocessstatus != "ALL"
                                    ) {
                                        console.log("update my task #");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (
                                        app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "PLANNING": {
                                    if (
                                        app.auth.employee_remedy_id !==
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (
                                        app.auth.employee_remedy_id ===
                                            data.employeeAssigneeId &&
                                        app.usedprocessstatus !==
                                            data.processStatus &&
                                        app.usedprocessstatus != "ALL"
                                    ) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (
                                        app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "WAITINGAPR": {
                                    if (
                                        app.auth.employee_remedy_id !==
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("remove to other task");
                                        app.$delete(app.tasks, wtIdx);
                                    }
                                    if (
                                        app.auth.employee_remedy_id ===
                                            data.employeeAssigneeId &&
                                        app.usedprocessstatus !==
                                            data.processStatus &&
                                        app.usedprocessstatus != "ALL"
                                    ) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                    } else if (
                                        app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId
                                    ) {
                                        console.log("update my task");
                                        app.$delete(app.tasks, wtIdx);
                                        app.tasks.push(data);
                                    }
                                    break;
                                }
                                case "CLOSED": {
                                    console.log(
                                        "remove from my task to history"
                                    );
                                    app.$delete(app.tasks, wtIdx);
                                    break;
                                }
                                case "REJECT": {
                                    console.log(
                                        "remove from my task due rejected"
                                    );
                                    app.$delete(app.tasks, wtIdx);
                                    break;
                                }
                            }
                        } else {
                            if (app.usedchannel !== "ALL") {
                                if (
                                    data.processStatus == "QUEUED" &&
                                    data.serviceTypeEncodingname ==
                                        serviceType &&
                                    data.serviceChannelName == app.usedchannel
                                ) {
                                    console.log(
                                        "newly added to request queue [filter]"
                                    );
                                    data.processStatus = "QUEUED";
                                    app.requests.push(data);
                                    audio.play();
                                }
                            } else if (app.usedprocessstatus != "ALL") {
                                if (
                                    (data.processStatus === "INPROGRESS" ||
                                        data.processStatus === "PENDING" ||
                                        data.processStatus === "WAITINGAPR" ||
                                        data.processStatus === "PLANNING") &&
                                    app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId &&
                                    data.processStatus == app.usedprocessstatus
                                ) {
                                    console.log(
                                        "added to my task from other [filter]"
                                    );
                                    data.processStatus = data.processStatus;
                                    app.tasks.push(data);
                                    audio.play();
                                    flash({
                                        id: data.id,
                                        message:
                                            "Request ID : " +
                                            data.id +
                                            " added to your task, assigned by : " +
                                            data.assigneeName,
                                        flashtype: "info"
                                    });
                                } else {
                                    if (
                                        data.processStatus == "QUEUED" &&
                                        data.serviceTypeEncodingname ==
                                            serviceType &&
                                        data.remedyTicketId !== "N/A"
                                    ) {
                                        console.log(
                                            "newly added to request queue ##"
                                        );
                                        data.processStatus = "QUEUED";
                                        app.requests.push(data);
                                        audio.play();
                                    }
                                    if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.remedyTicketId == "N/A" && (data.recipientID == "LIVECHAT" || data.recipientID == "TELEGRAM" || data.recipientID == "SMS")) {
                                        console.log("newly added to request queue #");
                                        data.processStatus = "QUEUED";
                                        app.requests.push(data);
                                        audio.play();
                                    }
                                }
                            } else {
                                if (
                                    data.processStatus == "QUEUED" &&
                                    data.serviceTypeEncodingname ==
                                        serviceType &&
                                    data.remedyTicketId !== "N/A"
                                ) {
                                    console.log("newly added to request queue");
                                    data.processStatus = "QUEUED";
                                    app.requests.push(data);
                                    audio.play();
                                }
                                if (data.processStatus == "QUEUED" && data.serviceTypeEncodingname == serviceType && data.remedyTicketId == "N/A" && (data.recipientID == "LIVECHAT" || data.recipientID == "TELEGRAM" || data.recipientID == "SMS")) {
                                    console.log("newly added to request queue #1");
                                    data.processStatus = "QUEUED";
                                    app.requests.push(data);
                                    audio.play();
                                }
                                if (
                                    (data.processStatus === "INPROGRESS" ||
                                        data.processStatus === "PENDING" ||
                                        data.processStatus === "WAITINGAPR" ||
                                        data.processStatus === "PLANNING") &&
                                    app.auth.employee_remedy_id ===
                                        data.employeeAssigneeId
                                ) {
                                    console.log("added to my task from other");
                                    data.processStatus = data.processStatus;
                                    app.tasks.push(data);
                                    audio.play();
                                    flash({
                                        id: data.id,
                                        message:
                                            "Request ID : " +
                                            data.id +
                                            " added to your task, assigned by : " +
                                            data.assigneeName,
                                        flashtype: "info"
                                    });
                                }
                            }
                        }
                    }
                });

                stompclient.subscribe("/request/livechat", function(stream) {
                    var data = JSON.parse(stream.body);
                    if (app.isdataloaded && app.requestid == data.requestId) {
                        app.requestdata.chatdata.push(data);
                        app.scrollToEnd();
                    }
                    if (
                        !app.ismodal &&
                        !app.isdataloaded &&
                        data.agentId === app.auth.id
                    ) {
                        flash({
                            id: data.id,
                            message:
                                "New message from " +
                                data.senderName +
                                " on Request ID :" +
                                data.requestId +
                                ", Message : " +
                                data.content,
                            flashtype: "info"
                        });
                    }
                    if (
                        app.ismodal &&
                        app.isdataloaded &&
                        app.requestid != data.requestId &&
                        data.agentId === app.auth.id
                    ) {
                        flash({
                            id: data.id,
                            message:
                                "New message from " +
                                data.senderName +
                                " on Request ID :" +
                                data.requestId +
                                ", Message : " +
                                data.content,
                            flashtype: "info"
                        });
                    }
                });
            },
            function(message) {
                app.isresponsereceived = true;
                stompclient.disconnect();
                flash({
                    id: "websocket-failure",
                    message:
                        "Connection to backend was interrupted, please try to refresh the page, if alert still appear change your browser or report to Administrator!!",
                    flashtype: "danger"
                });
            }
        );
    }
});
