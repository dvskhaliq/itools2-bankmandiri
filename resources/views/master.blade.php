<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
	<title>iTools</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('css/theme-white.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" id="theme" href="{{asset('css/app.css')}}" /> -->
    <script type="text/javascript" src="{{asset('assets/js/plugins/jquery/jquery.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('assets/js/plugins/jquery/jquery.min1.js')}}"></script> -->
    <script type="text/javascript" src="{{asset('assets/js/plugins/stompjs.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/socksjs.js')}}"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script> -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	<script type="text/javascript">
    	$(window).on('load', function() { // makes sure the whole site is loaded 
            $('#status').fadeOut(); // will first fade out the loading animation 
            $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website. 
            // $('body').delay(100).css({'overflow':'visible'});
        })
    </script>
</head>
<body class="main-body">
    <div class="page-container">
        @yield('sidebar')
	</div>
    @include('scriptlib')
</body>