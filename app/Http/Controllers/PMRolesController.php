<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\PMRolesFeaturesService;
use App\ServiceHandler\PBMLayer\PMRolesService;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\DataLayer\UserService;

class PMRolesController extends BaseController
{
    protected $roles_repo;
    protected $features_repo;
    protected $user_repo;

    public function __construct(PMRolesService $roles_service, PMRolesFeaturesService $features_service, UserService $user_service)
    {
        $this->roles_repo = $roles_service;
        $this->features_repo = $features_service;
        $this->user_repo = $user_service;
    }

    public function getRoles()
    {
        try {
            $roles = $this->roles_repo->getItems();
            return response()->json([
                'status' => true,
                'roles' => $roles
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function createRole(Request $request)
    {
        try {
            $payload = [
                'name' => request()->get('name'),
            ];
            $role = $this->roles_repo->create($payload);

            $features = $request->input('features');
            foreach ($features as $feature) {
                $payload = [
                    'feature_id' => $feature['id'],
                    'role_id' => $role->id,
                    'action_create' => $feature['pmrolesfeatures']['action_create'],
                    'action_read' => $feature['pmrolesfeatures']['action_read'],
                    'action_update' => $feature['pmrolesfeatures']['action_update'],
                    'action_delete' => $feature['pmrolesfeatures']['action_delete']
                ];
                if($payload['action_read']==true || $payload['action_create']==true || $payload['action_update']==true || $payload['action_delete']==true){
                    $this->features_repo->create($payload);
                }
            }
            return response()->json([
                'status' => true,
                'role' => $role
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function updateRole(Request $request)
    {
        try {
            $payload = [
                'id' => request()->get('id'),
                'name' => request()->get('name'),
            ];
            $role = $this->roles_repo->update($payload);

            $features = $request->input('features');
            foreach ($features as $feature) {
                $payload = [
                    'id' => $feature['pmrolesfeatures']['id'],
                    'feature_id' => $feature['pmrolesfeatures']['feature_id'],
                    'role_id' => $role->id,
                    'action_create' => $feature['pmrolesfeatures']['action_create'],
                    'action_read' => $feature['pmrolesfeatures']['action_read'],
                    'action_update' => $feature['pmrolesfeatures']['action_update'],
                    'action_delete' => $feature['pmrolesfeatures']['action_delete']
                ];
                if($payload['action_read']==true || $payload['action_create']==true || $payload['action_update']==true || $payload['action_delete']==true){
                    if ($payload['id'] == null) {
                        $this->features_repo->create($payload);
                    } else {
                        $this->features_repo->update($payload);
                    }
                }else if($payload['action_read']==false && $payload['id'] !== null){
                    $this->features_repo->delete($payload['id']);
                }
            }
            return response()->json([
                'status' => true,
                'role' => $role
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function deleteRole($role_id)
    {
        try {
            $checkRelate = $this->user_repo->getUserByPMRole($role_id);
            if(count($checkRelate)>0){
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal hapus role, terdapat relasi ke user aktif !',
                ], 200);
            }else{
                $this->features_repo->deleteByRole($role_id);
                $role = $this->roles_repo->delete($role_id);
                return response()->json([
                    'status' => true,
                    'role' => $role,
                ], 200);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }
}
