<?php

namespace App\ServiceHandler\PBMLayer;

use Illuminate\Database\Eloquent\Collection;
use App\PMRoles;

interface PMRolesService
{
    public function getItems(): ?Collection;
    public function create($payload): ?PMRoles;
    public function update($payload): ?PMRoles;
    public function delete(int $id): ?PMRoles;
}
