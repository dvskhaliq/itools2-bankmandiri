<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemedyCategorization extends Model
{
    protected $table = 'remedy_categorization';
}
