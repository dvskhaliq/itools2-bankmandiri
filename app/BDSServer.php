<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BDSServer extends Model
{
    protected $table = 'branch_connection';
    public $timestamps = false;

}
