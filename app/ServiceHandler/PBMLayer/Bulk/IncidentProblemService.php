<?php

namespace App\ServiceHandler\PBMLayer\Bulk;

use App\WebServiceResponse;

interface IncidentProblemService
{
    public function createIncidentRelation($payload);
    public function createProblemRelation($payload);
    public function chekingIncident($incident_number): WebServiceResponse;
    public function chekingProblem($problemNumber);
    public function createIncidentGeneric($payload): WebServiceResponse;
    public function updateIncidentGeneric($payload): WebServiceResponse;
    public function updateProblemGeneric($payload, $username = null, $password = null);
}
