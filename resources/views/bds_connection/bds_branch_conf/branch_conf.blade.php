@extends('sidebar')
@section('content')
<div id="bds_branch_conf">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS Branch Conf</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                            <flash v-if="ismodal" info=""></flash>
                                <components-branch-conf :data_branch_conf="data_branch_conf"></components-branch-conf>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="../../js/bds_branch_conf.js"></script>
@endsection
