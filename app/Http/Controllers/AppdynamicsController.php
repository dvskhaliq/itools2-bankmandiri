<?php

namespace App\Http\Controllers;

use App\PMMasterApdyBridge;
// use App\PMMasterSystemAvailability;
use App\ServiceHandler\PBMLayer\ResponseTime\TbluApdyService;
use App\ServiceHandler\IntegrationLayer\AppdynamicsService;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class AppdynamicsController extends BaseController
{
    protected $appdy;
    protected $tblu_appdy_repo;
    protected $start_time;
    protected $end_time;

    private function getData()
    {
        return PMMasterApdyBridge::with('pmmastersystemavailability')->get()->toArray();
        // return PMMasterApdyBridge::with('pmmastersystemavailability')->where('system_id', '!=', 38)->get()->toArray(); //appdy_1
        // return PMMasterApdyBridge::with('pmmastersystemavailability')->where('system_id', 38)->get()->toArray(); //appdy_2
    }

    public function __construct(AppdynamicsService $appdy_service, TbluApdyService $tblu_appdy_service)
    {
        $this->appdy = $appdy_service;
        $this->tblu_appdy_repo = $tblu_appdy_service;
    }

    public function index()
    {
        $this->start_time = strtotime('yesterday') * 1000;
        $this->end_time = strtotime('today - 1 sec') * 1000;
        $data = $this->executeProcess();
        try {
            if ($this->tblu_appdy_repo->insert($data)) {
                return $data;
            } else {
                return ["messages" => 'failed to insert data'];
            }
        } catch (\Exception $th) {
            return ["messages" => $th->__toString()];
        }
    }

    public function getBetweenRange(Request $request)
    {
        $start_time = $request->get('start_time');
        $end_time = $request->get('end_time');
        $this->start_time = strtotime($start_time) * 1000;
        $this->end_time = strtotime($end_time . ' -1 sec') * 1000;
        $days = ($this->end_time - $this->start_time) / 1000 / 60 / 60 / 24;
        if ($days < 1 && $days > 0) {
            $data = $request->has('application_name') ? $this->executeProcess($request->get('application_name')) : $this->executeProcess();
            try {
                if ($this->tblu_appdy_repo->insert($data)) {
                    return $data;
                } else {
                    return ["messages" => 'failed to insert data'];
                }
            } catch (\Exception $th) {
                return ["messages" => $th->__toString()];
            }
        } else {
            return ["messages" => 'Must be in range from 0 - 1 days'];
        }
    }

    public function getBetweenLongRange(Request $request)
    {     
        try{
            $start_time = new DateTime($request->get('start_time'));
            $end_time = new DateTime($request->get('end_time'));

            $interval =  DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start_time, $interval, $end_time);
            $temp = [];
            foreach ($period as $day) {
                $this->start_time = strtotime($day->format('Y-m-d H:i:s')) * 1000;;
                $this->end_time = strtotime($day->format('Y-m-d H:i:s') . '+1 day -1 sec')  * 1000;
                $data =  $request->has('application_name') ? $this->executeProcess($request->get('application_name')) : $this->executeProcess();
                array_push($temp, $data);
                if (!$this->tblu_appdy_repo->insert($data)) {
                    return false;
                }   
            }
            return response()->json([
                'status' => true,
                'data' => $temp
            ], 200);
        } catch (\Exception $th) {
            return response()->json([
                'status' => false,
                'message' => $th->__toString()
            ], 200);
        }
    }

    public function executeProcess($filter = null)
    {       
        $origin = ($filter == null ? $this->getData() : array_filter($this->getData(), function ($item) use ($filter) {
            return $item['pmmastersystemavailability']['system_name'] == $filter;
        }));
        // return $origin;
        $data = collect($origin)->map(function ($item) {
            $response = (object) $this->appdy->fetchData($item['rest_url'] . '&start-time=' . $this->start_time . '&end-time=' . $this->end_time . '&output=JSON', $item['link_appdy']);
            return [
                'layanan' => $item['pmmastersystemavailability']['system_name'],
                'site' => $item['site'],
                'machine' => $item['machine'],
                'node' => $item['node'],
                'business_txn_metrics' => $item['business_txn_metrics'],
                'type' => $item['type'],
                "start_from" => date("Y-m-d H:i:s", substr($response->startTimeInMillis, 0, 10)),
                "occurrences" => $response->occurrences,
                "current" =>  $response->current,
                "min" => $response->min,
                "max" => $response->max,
                "use_range" => $response->useRange,
                "count" => $response->count,
                "sum" => $response->sum,
                "avg_value" => $response->value,
                "standard_deviation" => $response->standardDeviation,
                "status" => $response->status,
                "messages" => $response->messages,
                "requested_at" => date("Y-m-d H:i:s"),
                "received_date" => date("Y-m-d", ($this->end_time / 1000)),
                "metric_name" => $response->metric_name,
                "metric_path" => $response->metric_path,
            ];
        })->toArray();
        return $data;
    }

    // public function bulk()
    // {
    //     $data = $this->getData();
    //     $test = collect($data)->map(function ($item) {
    //         return [
    //             'system_id' => PMMasterSystemAvailability::where('system_name', $item['application_name'])->first()['id'],
    //             'site' => $item['site'],
    //             'machine' => $item['machine'],
    //             'type' => $item['type'],
    //             'business_txn_metrics' => $item['business_txn_metrics'],
    //             'rest_url' => $item['rest_url'],
    //             'node' => $item['node']
    //         ];
    //     })->toArray();
    //     return PMMasterApdyBridge::insert($test);
        
    // }

    //Response Time
    public function fetchResponseTime()
    {
        $query = $this->tblu_appdy_repo->getResponseTime();
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByDate(Request $request)
    {
        $selected_date = $request->input('selectedDate');
        $query = $this->tblu_appdy_repo->getResponseTimeByDate($selected_date);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByMonth(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $query = $this->tblu_appdy_repo->getResponseTimeByMonth($selected_month);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByMonthAvg(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $avg_query = $this->tblu_appdy_repo->getResponseTimeByMonthAvg($selected_month);
        $data = $this->loopForeachGroup($avg_query, $selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchResponseTimeByYear(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        $query = $this->tblu_appdy_repo->getResponseTimeByYear($selected_year);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function loopForeach($data){
        $hasil=array();
        foreach ($data as $key => $value) {
            $prop['id'] = $value->id;
            $prop['layanan'] = $value->layanan;
            $prop['node'] = $value->node;
            $prop['count'] = $value->count;
            $prop['sum'] = $value->sum;
            $prop['status'] = $value->status;
            $prop['received_date'] = $value->received_date;

            array_push($hasil, [
                'id' => $prop['id'],
                'layanan' => $prop['layanan'],
                'node' => $prop['node'],
                'count' => $prop['count'],
                'sum' => $prop['sum'],
                'status' => $prop['status'],
                'received_date' => $prop['received_date'],
                ]);
        }
        return $hasil;
    }

    public function loopForeachGroup($avg, $month){
        $hasil=array();
        foreach ($avg as $key => $value) {
            $prop['layanan'] = $value->layanan;
            $prop['count'] = $value->count;
            $prop['sum'] = $value->sum;
            // $prop['restime'] = ROUND($value->restime*1,2);
            $prop['received_date'] = $value->bulan;
            $query = $this->tblu_appdy_repo->getResponseTimeByMonthSystem($prop['layanan'], $month);
            $decodedetail=array();
            foreach ($query as $keydetail => $value) {
                    $prop['layanan'] = $value->layanan;
                    $prop['node'] = $value->node;
                    $prop['count'] = $value->count;
                    $prop['sum'] = $value->sum;
                    $prop['status'] = $value->status;
                    $prop['received_date'] = $value->received_date;
                array_push($decodedetail, [
                    'id' => $keydetail+1,
                    'layanan' => $prop['layanan'],
                    'node' => $prop['node'],
                    'count' => $prop['count'],
                    'sum' => $prop['sum'],
                    'status' => $prop['status'],
                    'received_date' => $prop['received_date'],
                    ]);
            }
            array_push($hasil, [
                'id' => $key+1,
                'layanan' => $prop['layanan'],
                'count' => $prop['count'],
                'sum' => $prop['sum'],
                'status' => $prop['status'],
                'received_date' => $prop['received_date'],
                'detail' => $decodedetail
                ]);
        }
        return $hasil;
    }

    public function fetchYears()
    {
        $data = $this->tblu_appdy_repo->getYears();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function updateResponseTime()
    {
        try {
            $payload = [
                'id' => request()->get('id'),
                'count' => request()->get('count'),
                'sum' => request()->get('sum'),
                'avg_value' => request()->get('sum') == 0 || request()->get('count') == 0 ? 0 : intval((int)request()->get('sum') / (int)request()->get('count')),
                ];
            $data = $this->tblu_appdy_repo->updateResponseTime($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function koprats()
    {        
        try {
            // $appdy_link=['KOPRA-DC/request-snapshots?business-transaction-ids=29684&time-range-type=BEFORE_NOW&duration-in-mins=120&output=json','KOPRA-DRC/request-snapshots?time-range-type=BEFORE_NOW&business-transaction-ids=33934&duration-in-mins=120&output=json', 'KOPRA-DC/request-snapshots?time-range-type=BEFORE_NOW&business-transaction-ids=37564&duration-in-mins=120&output=json', 'KOPRA-DRC/request-snapshots?time-range-type=BEFORE_NOW&business-transaction-ids=50496&duration-in-mins=120&output=json'];
            $appdy_link= PMMasterApdyBridge::with('pmmastersystemavailability')->where('system_id', '=', 41)->get()->toArray();
            foreach($appdy_link as $result1){
                $data = $this->appdy->fetchDataKoprats($result1['rest_url']);            
            // $appdy_link='KOPRA-DC/request-snapshots?business-transaction-ids=29684&time-range-type=BEFORE_NOW&duration-in-mins=360&output=json';            
                foreach($data as $result){
                    $a[]= [
                        'user_experience' => $result->userExperience,
                        'error_summary' => isset($result->{'errorSummary'}) ? $result->errorSummary : 'no errorSummary',
                        'local_start_time' => date("Y-m-d H:i:s", substr($result->localStartTime, 0, 10)),
                        'fetched_date' => date("Y-m-d H:i:s"),
                        'node' => $result1['node'],
                        'site' => $result1['site'],
                        'summary' => $result->summary
                    ];
                }
            }
            if ($this->tblu_appdy_repo->insertKoprats($a)) {
                return response()->json([
                    'status' => true,
                    'data' => $a
                ], 200);
            } else {
                return ["messages" => 'failed to insert data'];
            }
        } catch (\Exception $th) {
            return ["messages" => $th->__toString()];
        }
    }
}
