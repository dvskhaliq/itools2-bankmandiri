import Vuex from "vuex";
import Vue from "vue";
import dashboard from "./modules/dashboard";

Vue.use(Vuex); // Load Vuex to Vue

export default new Vuex.Store({
    modules: {
        dashboard,
    }
});
