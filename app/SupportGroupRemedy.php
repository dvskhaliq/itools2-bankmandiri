<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportGroupRemedy extends Model
{    
    protected $table = 'support_group_remedy';
    public $timestamps = false;     
    protected $primaryKey = 'request_id';   
    public $keyType = 'string';
}