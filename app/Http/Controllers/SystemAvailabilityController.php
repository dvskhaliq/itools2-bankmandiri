<?php

namespace App\Http\Controllers;

use App\ServiceHandler\DataLayer\SystemAvailabilityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SystemAvailabilityController extends BaseController
{
    protected $system_avail_repo;

    public function __construct(SystemAvailabilityService $system_avail_service)
    {
        $this->system_avail_repo = $system_avail_service;
        // $this->middleware('auth:problem');
    }

    public function fetchYears()
    {
        $data = $this->system_avail_repo->getYears();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSystemAvailabilities()
    {
        $query = $this->system_avail_repo->getSystemAvail();
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSystemAvailByYear(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        $query = $this->system_avail_repo->getSystemAvailByYear($selected_year);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSystemAvailByMonth(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $query = $this->system_avail_repo->getSystemAvailByMonth($selected_month);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchSystemAvailByMonthAvg(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $avg_query = $this->system_avail_repo->getSystemAvailByMonthAvg($selected_month);
        $data = $this->loopForeachGroup($avg_query, $selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function loopForeachGroup($avg, $month){
        $hasil=array();
        foreach ($avg as $key => $value) {
            $prop['id_system'] = $value->id_system;
            $prop['header'] = $value->header;
            $prop['system_downtime'] = $value->system_downtime;
            $prop['percent_system_avail'] = ROUND($value->percent_system_avail*100,2);
            $prop['recorded_at'] = $value->bulan;
            $query = $this->system_avail_repo->getSystemAvailByMonthSystem($prop['id_system'], $month);
            array_push($hasil, [
                'id_system' => $prop['id_system'],
                'header' => $prop['header'],
                'system_downtime' => $prop['system_downtime'],
                'percent_system_avail' => $prop['percent_system_avail'],
                'recorded_at' => $prop['recorded_at'],
                'detail' => $query
                ]);
        }
        return $hasil;
    }

    public function fetchSystemAvailByDate(Request $request)
    {
        $selected_date = $request->input('selectedDate');
        $query = $this->system_avail_repo->getSystemAvailByDate($selected_date);
        $data = $this->loopForeach($query);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function loopForeach($data){
        $hasil=array();
        foreach ($data as $key => $value) {
            $prop['id'] = $value->id;
            $prop['header'] = $value->header;
            $prop['system_downtime'] = $value->system_downtime;
            $prop['percent_system_avail'] = ROUND($value->percent_system_avail*100,2);
            $prop['description'] = $value->description;
            $prop['recorded_at'] = $value->recorded_at;

            array_push($hasil, [
                'id' => $prop['id'],
                'header' => $prop['header'],
                'system_downtime' => $prop['system_downtime'],
                'percent_system_avail' => $prop['percent_system_avail'],
                'description' => $prop['description'],
                'recorded_at' => $prop['recorded_at'],
                ]);
        }
        return $hasil;
    }

    public function createSystemAvail()
    { 
        $header = request()->get('master_system');
        try {
            $payload = [
            'id_system' => $header['id'],
            'system_downtime' => request()->get('system_downtime'),
            'percent_system_avail' =>  1 - (request()->get('system_downtime')/24),
            'description' => request()->get('description'),
            'recorded_at' => request()->get('recorded_at')
            ];
            $data = $this->system_avail_repo->createSystemAvail($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function updateDescSystemAvail()
    {
        $header = request()->get('master_system');
        try {
            $payload = [
                'id' => request()->get('id'),
                'description' => request()->get('description'),
                ];
            $data = $this->system_avail_repo->updateDescSystemAvail($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function updateSystemAvail()
    {
        $header = request()->get('master_system');
        try {
            $payload = [
                'id' => request()->get('id'),
                'id_system' => $header['id'],
                'system_downtime' => request()->get('system_downtime'),
                'percent_system_avail' => 1 - (request()->get('system_downtime')/24),
                'description' => request()->get('description'),
                'recorded_at' => request()->get('recorded_at')
                ];
            $data = $this->system_avail_repo->updateSystemAvail($payload);
            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'message' => $ex->__toString()
            ], 500);
        }
    }

    public function deleteSystemAvail(int $id){
            $data = $this->system_avail_repo->deleteSystemAvail($id);
            return response()->json([
                'status' => true,
                'data' => $data
            ]);
    }

    public function fetchSystemName()
    {
        $data = $this->system_avail_repo->getSystemName();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }
}
