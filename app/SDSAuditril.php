<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SDSAuditril extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable = ['sds_audit_trail'];
    protected $table = 'sds_audit_trail';
    public $timestamps = false;
}
