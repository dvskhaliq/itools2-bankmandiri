<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use View;

class LosFingerprintStatusController extends BaseController
{
    /**
      * Create a new controller instance.
      *
      * @return void
      */
      public function __construct()
      {
          $this->middleware('auth');
      }

      /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */

      use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

      public function index()
      {
          return View::make('los/los_fingerprint_status/los_fingerprint_status');
      }
}