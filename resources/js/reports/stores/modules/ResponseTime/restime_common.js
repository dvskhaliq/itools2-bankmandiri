import ENUM from "../../enum";

const state = {
    responsetime_s: [],
    responsetime_savg: [],
    years: [],
    simpleHeader: [
        { text: "Service Name", value: "layanan" },
        { text: "Node", value: "node"},
        { text: "Response Time", value: "restime", align: 'center'},
        { text: "Date", value: "tanggal"}
    ],
    simpleHeaderAvg: [
        { text: "Service Name", value: "layanan" },
        { text: "Average Response Time", value: "restime" },
        { text: "Month", value: "tanggal" },
    ],
    apiState: ENUM.INIT,
    alertMessage: "",
};

const getters = {
    getResponseTime_sAvg: state => state.responsetime_savg,
    getResponseTime_s: state => state.responsetime_s,
    getYears: state => state.years,
    getSimpleHeader: state => state.simpleHeader,
    getSimpleHeaderAvg: state => state.simpleHeaderAvg,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
};

const actions = {
    async initResponseTime({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.post("/data_management/problems/response_time/common/fetch", payload);
          commit("setResponseTime_s", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByDate({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/response_time/common/getByDate", payload
              );
            commit("setResponseTime_s", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByMonth({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/response_time/common/getByMonth", payload
              );
            commit("setResponseTime_s", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByMonthAvg({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/response_time/common/getByMonthAvg", payload
              );
            commit("setResponseTime_sAvg", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByYear({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
                response = await axios.post(
                "/data_management/problems/response_time/common/getByYear", payload
              );
            commit("setResponseTime_s", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
    async fetchYears({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/response_time/common/fetchYears", payload);
            commit("setYear", response.data.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Something went wrong on fetch!"
                  );
        }
    },
    resetResponseTime({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setResponseTime_s", []);
    },
    resetResponseTimeAvg({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setResponseTime_sAvg", []);
    },
};

const mutations = {
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setResponseTime_s: (state, responsetime) => {
        state.apiState = ENUM.LOADED;
        state.responsetime_s = responsetime;
    },
    setResponseTime_sAvg: (state, responsetimeavg) => {
        state.apiState = ENUM.LOADED;
        state.responsetime_savg = responsetimeavg;
    },
    setYear: (state, year) => {
        state.apiState = ENUM.LOADED;
        state.years = year;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
