@extends('sidebar')
@section('content')
<div id="bds_atm_detail">
    <div class="row bds-atm-detail">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS ATM Detail</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>ATM Detail</span>
                            </div>
                            <div class="panel-body">
                                <table-bds-atm-detail :data_atm_detail="data_atm_detail" v-on:atm-detail-selected="atmDetailSelected" :isloaded="isloaded"></table-bds-atm-detail>
                                <!-- <vue-pagination :pagination="pages" @paginate="getData()" :offset="4">
                                </vue-pagination> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Add ATM Detail</span>
                            </div>
                            <componen-bds-atm-add :data_atm_detail="data_atm_detail" v-on:submit-add-atm="submitAdd"></componen-bds-atm-add>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Confirm Delete</h4>
                </div>
                <div v-if="isloaded">
                    <componen-confirm-delete v-on:submit-delete="submitDelete" v-on:closewindows="closeWindow" :atm_detail_prop="atm_detail_prop" :isloaded="isloaded" :ismodal="ismodal"></componen-confirm-delete>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-lg" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">ATM Detail</h4>
                </div>
                <div v-if="isloaded">
                    <componen-modal-edit v-on:closewindows="closeWindow" v-on:submit-update="submitUpdate" :atm_detail_prop="atm_detail_prop" :isloaded="isloaded" :ismodal="ismodal"></componen-modal-edit>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/bds_atm_detail.js"></script>
@endsection
