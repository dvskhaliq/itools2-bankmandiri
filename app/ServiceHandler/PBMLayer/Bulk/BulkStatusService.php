<?php

namespace App\ServiceHandler\PBMLayer\Bulk;

interface BulkStatusService
{
    public function create($payload);
}
