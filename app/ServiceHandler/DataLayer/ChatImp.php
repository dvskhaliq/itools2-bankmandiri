<?php

namespace App\ServiceHandler\DataLayer;;

use App\ChatLiveMessages as LiveChat;
use App\Http\Resources\ChatCollection;
use App\Http\Resources\Chat;
use App\Inbox;
use Auth;
use GuzzleHttp\Client;
use App\Incident;
use App\Workorder;
use App\User;

class ChatImp implements ChatService
{
    /**
     * @Override
     */
    public $status = false;
    public $message = 'this is default message';

    public function addChat(Inbox $request, String $sender_type, String $message): Chat
    {
        // if ($request->ProcessStatus != 'CLOSED'  && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
            try {
                $clm = new LiveChat();
                $clm->request_id = $request->id;
                $clm->sender = $sender_type;
                $clm->content = $message;
                $clm->timestamp = date('Y-m-d H:i:s', strtotime('now'));
                $clm->save();
                $clm->load('inbox.user', 'inbox.employeeremedy', 'inbox.chatsubscribeduser.employeeremedy', 'inbox.servicecatalog.servicechannel.servicetype');
                return new Chat($clm);
            } catch (\Exception $th) {
                $this->status = false;
                $this->message = "message save failed: " . $th->__toString();
                return "ERROR";
            }
        // } else {
            // $this->status = false;
            // $this->message = "not authorized to save message due ticket closed";
            // return "ERROR";
        // }
    }

    public function find(Int $message_id): LiveChat
    {
        return LiveChat::with('inbox.user', 'inbox.employeeremedy', 'inbox.chatsubscribeduser.employeeremedy', 'inbox.servicecatalog.servicechannel.servicetype')->find($message_id);
    }

    public function findDTO(int $message_id): Chat
    {
        return new Chat(LiveChat::with('inbox.user', 'inbox.chatsubscribeduser', 'inbox.servicecatalog.servicechannel.servicetype')->find($message_id));
    }

    public function getListByRequestId(Int $request_id): LiveChat
    {
        return LiveChat::with('inbox.user', 'inbox.chatsubscribeduser', 'inbox.servicecatalog.servicechannel.servicetype')->where('request_id', $request_id)->orderBy('id')->get();
    }

    public function getListDTOByRequestId(int $request_id): ChatCollection
    {
        return new ChatCollection(Chat::collection(LiveChat::with('inbox.user', 'inbox.chatsubscribeduser', 'inbox.supportgroupassigneeremedy', 'inbox.servicecatalog.servicechannel.servicetype')->where('request_id', $request_id)->orderBy('id')->get()));
    }

    public function postTelegramFeedback($request_id, $content)
    {
        $clm = new LiveChat();
        $clm->request_id = $request_id;
        $clm->sender = 'Agent';
        $clm->timestamp = date('Y-m-d H:i:s');
        $clm->content = $content;
        $clm->save();
    }

    public function postSMSFeedback($number, $content)
    {
        $client = new Client();
        $url = "https://sms-api.jatismobile.com/index.ashx?userid=BANK.MANDIRI&password=BANK.MANDIRI123&msisdn=" . $number . "&message=" . $content . "&sender=BANKMANDIRI&division=IT Applications Support Group&batchname=Service Eye&uploadby=CSI&channel=0";
        $response = $client->get($url, ['proxy' => 'http://proxymandiri.corp.bankmandiri.co.id:8080']);
        return $response->getBody()->getContents();
    }
}
