<?php

namespace App\ServiceHandler\DataLayer;

use App\RemedyCategorization;
use Illuminate\Support\Facades\DB;

class RemedyCategorizationImp implements RemedyCategorizationService
{
    /**
     * @Override
     */
    public function getProductCategoryTier1()
    {
        return  RemedyCategorization::distinct('product_category_tier_1')->orderBy('product_category_tier_1', 'asc')->pluck('product_category_tier_1');
    }

    public function getProductCategoryTier2(String $product_category_tier_1)
    {
        return RemedyCategorization::distinct('product_category_tier_2')
            ->where(DB::raw('TRIM(product_category_tier_1)'), $product_category_tier_1)->where('status',1)
            ->orderBy('product_category_tier_2', 'asc')->pluck('product_category_tier_2');
    }

    public function getProductCategoryTier3(String $product_category_tier_1, String $product_category_tier_2)
    {
        return RemedyCategorization::distinct('product_category_tier_3')->where([
            [DB::raw('TRIM(product_category_tier_1)'), $product_category_tier_1],
            [DB::raw('TRIM(product_category_tier_2)'), $product_category_tier_2]
        ])->where('status',1)->orderBy('product_category_tier_3', 'asc')->pluck('product_category_tier_3');
    }

    public function getOprationalCategoryTier1(String $product_category_tier_1, String $product_category_tier_2, String $product_category_tier_3)
    {
        return RemedyCategorization::distinct('operational_category_tier_1')->where([
            [DB::raw('TRIM(product_category_tier_1)'), $product_category_tier_1],
            [DB::raw('TRIM(product_category_tier_2)'), $product_category_tier_2],
            [DB::raw('TRIM(product_category_tier_3)'), $product_category_tier_3],
        ])->where('status',1)->orderBy('operational_category_tier_1', 'asc')->pluck('operational_category_tier_1');
    }

    public function getOprationalCategoryTier2(String $product_category_tier_1, String $product_category_tier_2, String $product_category_tier_3, $operational_category_tier_1)
    {
        return RemedyCategorization::distinct('operational_category_tier_2')->where([
            [DB::raw('TRIM(product_category_tier_1)'), $product_category_tier_1],
            [DB::raw('TRIM(product_category_tier_2)'), $product_category_tier_2],
            [DB::raw('TRIM(product_category_tier_3)'), $product_category_tier_3],
            [DB::raw('TRIM(operational_category_tier_1)'), $operational_category_tier_1],
        ])->where('status',1)->orderBy('operational_category_tier_2', 'asc')->pluck('operational_category_tier_2');
    }

    public function getOprationalCategoryTier3(String $product_category_tier_1, String $product_category_tier_2, String $product_category_tier_3, String $operational_category_tier_1, String $operational_category_tier_2)
    {
        return RemedyCategorization::distinct('operational_category_tier_3')->where([
            [DB::raw('TRIM(product_category_tier_1)'), $product_category_tier_1],
            [DB::raw('TRIM(product_category_tier_2)'), $product_category_tier_2],
            [DB::raw('TRIM(product_category_tier_3)'), $product_category_tier_3],
            [DB::raw('TRIM(operational_category_tier_1)'), $operational_category_tier_1],
            [DB::raw('TRIM(operational_category_tier_2)'), $operational_category_tier_2],
        ])->where('status',1)->orderBy('operational_category_tier_3', 'asc')->pluck('operational_category_tier_3');
    }

    public function searchServiceCatalog($name)
    {        
        $data = RemedyCategorization::where('name', 'ILIKE', '%' . $name . '%')->get();
        return response()->json($data);
    }
}
