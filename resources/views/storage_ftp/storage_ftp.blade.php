@extends('sidebar')
@section('content')
<div id="storage_ftp">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>STORAGE FILE INFRA</h4>                 
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="ismodal" info=""></flash>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <storage-table :result="result" :ismodal="ismodal" :category="category" v-on:refresh-file-list="refreshFileList"></storage-table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script src="../../js/storage_ftp.js"></script>
@endsection