import "../bootstrap";
// import 'es6-promise/auto';
import "vuetify/dist/vuetify.min.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import Alert from "./components/alert/Alert";
import SimpleLoading from "./components/SimpleLoading";
import Vue from "vue";
import store from "./store";
import Vuetify from "vuetify";

axios.defaults.baseURL = "/";
window.Vue = Vue;
Vue.use(Vuetify);
Vue.component("simple-loading", SimpleLoading);
Vue.component("alert-message", Alert);
Vue.component("incident-report", () => import("./IncidentReport.vue"));

const vuetify = new Vuetify();

const app = new Vue({
    el: "#report_incident",
    vuetify,
    store
});
export default app;
