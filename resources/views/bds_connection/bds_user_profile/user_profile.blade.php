@extends('sidebar')
@section('content')
<div id="bds_user_profile">
    <div class="row bds-user-profile">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>BDS User Profile</h4>
                    <small>{{ Session::get('data.database')." - ".Session::get('data.namaCabang') }}</small>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info=""></flash>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>User Profile</span>
                            </div>
                            <div class="panel-body">
                                <table-bds-user-profile :data_user_profile="data_user_profile" :isloaded="isloaded" :alert="alert" v-on:userprofileselected="userProfileSelected" v-on:tran_limit_selected="tranLimitSelected" v-on:search-data="searchData"></table-bds-user-profile>
                                <!-- <vue-pagination :pagination="pages" @paginate="getData()" :offset="4">
                                </vue-pagination> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalSetUserProfile" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog  modal-lg" style="width: 50%;">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Set User Profile</h4>
                </div>
                <div v-if="isloaded">
                    <component-set-user-profile :user_profile_prop="user_profile_prop" v-on:closewindows="closeWindow" v-on:sign-off="setSignOff" v-on:monetery-user="setMonetaryUser" v-on:ctt="setCTT" v-on:line-status="setLineStatus"></component-set-user-profile>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalTranLimit" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true" v-if="ismodal">
        <div class="modal-dialog modal-lg" style="width: 90%;">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalHead">Tran Limit</h4>
                </div>
                <div v-if="isloaded">
                    <component-tran-limit :tran_limit_props="tran_limit_props" v-on:closewindows="closeWindow"></component-tran-limit>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')

<script src="../../js/bds_user_profile.js"></script>
@endsection
