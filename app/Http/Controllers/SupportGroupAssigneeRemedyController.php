<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\SupportGroupAssigneeRemedy;

class SupportGroupAssigneeRemedyController extends BaseController
{
    /**
     * Create a new controller instance.
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getAssignedSupportCompany(){
        return SupportGroupAssigneeRemedy::distinct('company')->orderBy('company', 'asc')->pluck('company');
    }

    public function getAssignedSupportOrganization(){
        return SupportGroupAssigneeRemedy::distinct('organization')->where('organization','<>',null)->where('company', request()->get('supportCompany'))->orderBy('organization', 'asc')->pluck('organization');
    }

    public function getAssignedSupportGroup(){
        return SupportGroupAssigneeRemedy::distinct('support_group_name')->where([['company', request()->get('supportCompany')],['organization', request()->get('supportOrganization')]])->orderBy('support_group_name', 'asc')->pluck('support_group_name'); 
    }

    public function getAssigned(){
        return SupportGroupAssigneeRemedy::distinct('full_name')->where([['company', request()->get('supportCompany')], ['organization', request()->get('supportOrganization')], ['support_group_name', request()->get('supportGroupName')]])->orderBy('full_name', 'asc')->pluck('full_name');
    }

    public function getAssignedByLoginId(){
        return SupportGroupAssigneeRemedy::where('remedy_login_id',request()->get('assigneeLoginId'))->first();
    }

    public function getAllSupportGroup(){
        return SupportGroupAssigneeRemedy::distinct('support_group_name')->where('support_group_name','~*','[a-zA-Z]')->orderBy('support_group_name', 'asc')->pluck('support_group_name');
    }
}
