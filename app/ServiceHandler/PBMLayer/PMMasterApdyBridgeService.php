<?php

namespace App\ServiceHandler\PBMLayer;
use Illuminate\Database\Eloquent\Collection;

interface PMMasterApdyBridgeService
{
    public function getPMMasterApdyBridge(): ?Collection;
    public function createPMMasterApdyBridge($payload);
    public function updatePMMasterApdyBridge($payload);
    public function deletePMMasterApdyBridge(int $id);
    public function getSystemName();
}
