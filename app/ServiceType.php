<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{

    protected $fillable = ['service_type'];
    protected $table = 'service_type';
    public $timestamps = false;

    public function servicechannel(){
        return $this->hasMany(ServiceChannel::class,'service_type_name','encoding_name');
    }
    
}

