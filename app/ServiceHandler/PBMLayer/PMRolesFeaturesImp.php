<?php

namespace App\ServiceHandler\PBMLayer;

use App\PMRolesFeatures;
use Illuminate\Database\Eloquent\Collection;

class PMRolesFeaturesImp implements PMRolesFeaturesService
{
    public function create($payload): ?PMRolesFeatures
    {
        $rf = new PMRolesFeatures();
        $rf->feature_id = $payload['feature_id'];
        $rf->role_id = $payload['role_id'];
        $rf->action_create = $payload['action_create'];
        $rf->action_read = $payload['action_read'];
        $rf->action_update = $payload['action_update'];
        $rf->action_delete = $payload['action_delete'];
        $rf->created_at = date('Y-m-d H:i:s');
        $rf->save();
        return $rf;
    }
    public function update($payload): ?PMRolesFeatures
    { 
        $rf = PMRolesFeatures::find($payload['id']);
        $rf->feature_id = $payload['feature_id'];
        $rf->role_id = $payload['role_id'];
        $rf->action_create = $payload['action_create'];
        $rf->action_read = $payload['action_read'];
        $rf->action_update = $payload['action_update'];
        $rf->action_delete = $payload['action_delete'];
        $rf->updated_at = date('Y-m-d H:i:s');
        $rf->save();
        return $rf;
    }
    public function deleteByRole(int $role_id)
    { 
        $rf = PMRolesFeatures::where('role_id', $role_id)->delete();
        return $rf;
    }

    public function delete(int $id)
    { 
        return PMRolesFeatures::where('id', $id)->delete();
    }
}
