<?php

namespace App\ServiceHandler\PBMLayer;
use Illuminate\Database\Eloquent\Collection;

interface SuccessRateService
{
    public function getSuccessRate(): ?Collection;
    public function getSuccessRateByDate($date): ?Collection;
    public function getSuccessRateByMonth($month): ?Collection;
    public function getSuccessRateByMonthLayanan($layanan, $month): ?Collection;
    public function getSuccessRateByMonthAvg($month): ?Collection;
    public function getSuccessRateByYear(array $years): ?Collection;
    public function getYears();
}