<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmCreateRelation extends Model
{
    protected $table = 'pm_create_relation';
}
