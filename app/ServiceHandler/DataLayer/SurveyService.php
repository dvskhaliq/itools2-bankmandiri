<?php

namespace App\ServiceHandler\DataLayer;


interface SurveyService
{
    public function saveSurvey($array_attr);

    public function deleteSurvey($survey_id);

    public function findSurvey($survey_id);

    // public function findWorkinfobyId($id);
}
