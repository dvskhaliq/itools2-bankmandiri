<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Incident;
use App\Inbox;
use App\ServiceHandler\DataLayer\InboxService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\DataLayer\UserService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ServiceHandler\TaskRequestService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderService;
use App\ServiceHandler\IntegrationLayer\WOIWorkOrderUpdateService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentService;
use App\ServiceHandler\IntegrationLayer\WOIIncidentUpdateService;
use App\ServiceHandler\UserSupportService;
use App\ServiceHandler\DataLayer\TelegramUserService;
use App\Workorder;
use JavaScript;
use View;
use Auth;
use App\ServiceHandler\DataLayer\ChatService;
use App\ServiceHandler\DataLayer\ServiceCatalogService;
use SebastianBergmann\Environment\Console;
use App\ServiceHandler\DataLayer\SupportGroupService;
use App\ServiceHandler\NotificationLayer\NotificationService;

class PendingTicketRemedyController extends BaseController
{

    protected $incupdate_wsdl;
    protected $wocreate_wsdl;
    protected $woupdate_wsdl;
    protected $taskrequest_service;
    protected $inccreate_wsdl;
    protected $inc_wsdl_service;
    protected $user_service;
    //
    protected $inbox_ds;
    protected $user_ds;
    protected $supusr_ds;
    protected $chat_ds;
    protected $tlguser_ds;
    protected $srvctlg_ds;
    protected $suppgrp_ds;
    protected $notif_ds;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        TaskRequestService $tr_service,
        WOIWorkOrderService $workorder_create_service,
        WOIWorkOrderUpdateService $workorder_update_service,
        WOIIncidentService $incident_services,
        WOIIncidentUpdateService $inc_service,
        UserSupportService $user_service,
        InboxService $inbox_data_service,
        UserService $user_data_service,
        SupportUserService $support_user_service,
        WOIIncidentUpdateService $incident_update_service,
        ChatService $lc_service,
        TelegramUserService $telegram_user_service,
        ServiceCatalogService $service_catalog_service,
        SupportGroupService $support_group_service,
        NotificationService $notif_service
    ) {
        $this->suppgrp_ds = $support_group_service;
        $this->srvctlg_ds = $service_catalog_service;
        $this->chat_ds = $lc_service;
        $this->tlguser_ds = $telegram_user_service;
        $this->incupdate_wsdl = $incident_update_service;
        $this->supusr_ds = $support_user_service;
        $this->user_ds = $user_data_service;
        $this->inbox_ds = $inbox_data_service;
        $this->notif_ds = $notif_service;
        //
        $this->taskrequest_service = $tr_service;
        $this->wocreate_wsdl = $workorder_create_service;
        $this->woupdate_wsdl = $workorder_update_service;
        $this->inccreate_wsdl = $incident_services;
        $this->inc_wsdl_service = $inc_service;
        $this->user_service = $user_service;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        JavaScript::put([
            "result" => $this->inbox_ds->findPendingTicket(Auth::user()->id),
            "auth" => $this->user_ds->getRestrictedMe()
        ]);
        return View::make('pending_ticket_remedy/pending_ticket_remedy');
    }

    public function viewTask()
    {
        $request = $this->inbox_ds->find(request()->get('id'));
        if ($request->servicecatalog->service_catalog_type == 'IN') {
            $incident = new Incident();
            $service = $incident->parseInbox($request);
        } else {
            $workorder = new Workorder();
            $service = $workorder->parseInbox($request);
        }
        $request['ticket'] = $service;
        // dd($request);
        return response()->json($request);
    }

    public function submitRetryProcess()
    {
        $content = request()->get('content');
        $request = $this->inbox_ds->find(request()->get('id'));
        $request_old = $this->inbox_ds->find(request()->get('id'));
        $tamp_RecipientID = $request->RecipientID;
        $tamp_ServiceCatalogId = $request->ServiceCatalogId;
        $assignee = $this->supusr_ds->findByCompanyOrgGroupName($content['assignee_support_company'], $content['assignee_support_organization'], $content['assignee_support_group_name'], $content['assignee_name']);
        if ($request->servicecatalog->service_catalog_type == 'IN') {
            $incident = new Incident();
            $status = $incident->generateStatus($content['status']);
            $text_decoded = $incident->generateTextDecoded($content);
            if ($assignee) {
                $array_inbox = array(
                    "AssigneeId" => Auth::user()->id,
                    "EmployeeAssigneeId" => $assignee->employee_remedy_id,
                    "EmployeeAssigneeGroupId" => $assignee->support_group_id,
                    "TextDecoded" => $text_decoded,
                    "ProcessStatus" => $status,
                    "RecipientID" => $tamp_RecipientID !== 'LIVECHAT' ? $tamp_RecipientID : 'TELEGRAM',
                    "ServiceCatalogId" => $tamp_RecipientID !== 'LIVECHAT' ? $tamp_ServiceCatalogId : $this->convertServiceCatalog($request),
                );
                $this->inbox_ds->update($request->id, $array_inbox);
                $incident->parseInbox($this->inbox_ds->refresh($request));
                $incident->updateResolution($content['resolution'], $content['notes_description']);
            } else {
                $array_inbox = array(
                    "AssigneeId" => Auth::user()->id,
                    "EmployeeAssigneeId" => null,
                    "EmployeeAssigneeGroupId" => null,
                    "TextDecoded" => $text_decoded,
                    "ProcessStatus" => $status,
                    "RecipientID" => $tamp_RecipientID !== 'LIVECHAT' ? $tamp_RecipientID : 'TELEGRAM',
                    "ServiceCatalogId" => $tamp_RecipientID !== 'LIVECHAT' ? $tamp_ServiceCatalogId : $this->convertServiceCatalog($request),
                );
                $this->inbox_ds->update($request->id, $array_inbox);
                $incident->parseInbox($this->inbox_ds->refresh($request));
                $incident->updateResolution($content['resolution'], $content['notes_description']);
                $group = $this->suppgrp_ds->findByComOrgGrp($content['assignee_support_company'], $content['assignee_support_organization'], $content['assignee_support_group_name']);
                $incident->assignee_support_company = $group->company;
                $incident->assignee_support_organization = $group->organization;
                $incident->assignee_support_group_name = $group->support_group_name;
                $incident->assignee_support_group_id = $group->id;
            }
            $check_ticket = $this->incupdate_wsdl->chekingIncident($incident);
            if ($check_ticket->getResponseStatus() !== 'success') {
                $array_inbox = array(
                    "RemedyTicketId" => 'N/A',
                );
                $this->inbox_ds->update($request->id, $array_inbox);
                $request = $this->inbox_ds->find(request()->get('id'));
                $incident->parseInbox($request);
            }
            $request['ticket'] = $incident;
            $wsdl_reconID = $this->incupdate_wsdl->getServiceReconID($incident->service);
            if ($request->RemedyTicketId == 'N/A') {
                $wsdl = $this->inccreate_wsdl->createIncidentGeneric($incident, $wsdl_reconID->getContent());
                $incident->parseInbox($this->inbox_ds->refresh($request));
            } else {
                $wsdl = $this->incupdate_wsdl->updateIncidentGeneric($incident, $wsdl_reconID->getContent());
            }
            try {
                if ($wsdl->getResponseStatus() == 'success') {
                    if ($tamp_RecipientID == 'LIVECHAT') {
                        $this->notif_ds->dismissLiveChat($request);
                        $this->notif_ds->afterClosureFeedback($this->inbox_ds->refresh($request), $incident);
                    }
                    $message = $request->RemedyTicketId == 'N/A' ? "Incident Number : " . $wsdl->getContent() . " has been created" : "Incident Number : " . $wsdl->getContent() . " has been updated";
                    //Notifier
                    $inbox = $this->inbox_ds->refresh($request);
                    if ($status == 'CLOSED' || $status == 'REJECT') {
                        $this->notif_ds->performFeedBackNotifier($inbox, $content['status'], $content['resolution']);
                    } else {
                        $this->notif_ds->performFeedBackNotifier($inbox);
                    }
                } else {
                    if ($tamp_RecipientID == 'LIVECHAT') {
                        $array_inbox = array(
                            "RecipientID" => 'LIVECHAT',
                            "ServiceCatalogId" => $tamp_ServiceCatalogId,
                        );
                        $this->inbox_ds->update($request->id, $array_inbox);
                    }
                    $message = "Failed to resubmit Incident with following error : " . $wsdl->getResponseMessage();
                }
                $result = $this->inbox_ds->findPendingTicket(Auth::user()->id);
                return response()->json(['status' => true, 'responsedata' => $request, 'message' => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'wsdl_request' => $wsdl->getArrContent(), 'result' => $result]);
            } catch (Exception $e) {
                $result = $this->inbox_ds->findPendingTicket(Auth::user()->id);
                return response()->json(['status' => false, 'message' => "Fatal Error : Unable connect to Remedy, your request not being processed ! " . $e->getMessage(), 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => false, 'wsdl_request' => $wsdl->getArrContent(), 'result' => $result]);
            }
        } else {
            $workorder = new Workorder();
            $status = $workorder->generateStatus($content['status']);
            $text_decoded = $workorder->generateTextDecoded($content);

            $array_inbox = array(
                "AssigneeId" => Auth::user()->id,
                "EmployeeAssigneeId" => $assignee->employee_remedy_id,
                "EmployeeAssigneeGroupId" => $assignee->support_group_id,
                "TextDecoded" => $text_decoded,
                "ProcessStatus" => $status,
            );
            $workorder->parseInbox($this->inbox_ds->update($request->id, $array_inbox));

            $check_ticket = $this->wocreate_wsdl->checkingWO($workorder);
            if ($check_ticket->getResponseStatus() !== 'success') {
                $array_inbox = array(
                    "RemedyTicketId" => 'N/A',
                );
                $workorder->parseInbox($this->inbox_ds->update($request->id, $array_inbox));
            }
            if ($request->RemedyTicketId == 'N/A') {
                $wsdl = $this->wocreate_wsdl->createWO($workorder);
            } else {
                $wsdl = $this->woupdate_wsdl->updateWOGeneric($workorder);
            }
            try {
                if ($wsdl->getResponseStatus() == 'success') {
                    $array_inbox = array(
                        "TicketCreationDateTime" => $request->RemedyTicketId == 'N/A' ? date('Y-m-d H:i:s') : $request->TicketCreationDateTime,
                        "RemedyTicketId" => $request->RemedyTicketId == 'N/A' ? $wsdl->getContent() : $workorder->workorder_number,
                        "ActionTicketCreation" => ($status == "CLOSED" || $status == "REJECT") ? 'WO_COMPLETED' : 'WO_PROCESS',
                        "FinishDateTime" => ($status == "CLOSED" || $status == "REJECT") ? date('Y-m-d H:i:s') : $request->FinishDateTime,
                    );
                    $workorder->parseInbox($this->inbox_ds->update($workorder->itools_request_id, $array_inbox));
                    $message = $request->RemedyTicketId == 'N/A' ? "Work Order Number: " . $wsdl->getContent() . " has been created" : "Work Order Number: " . $wsdl->getContent() . " has been updated";
                    $this->notif_ds->performFeedBackNotifierWO($this->inbox_ds->refresh($request), $content['addnote']);
                } else {
                    $message = "Failed to resubmit Work Order with following error : " . $wsdl->getResponseMessage();
                }
                $result = $this->inbox_ds->findPendingTicket(Auth::user()->id);
                return response()->json(["status" => true, "message" => $message, 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => $wsdl->getResponseStatus() == 'success' ? true : false, 'data' => $wsdl->getArrContent(), "result" => $result]);
            } catch (Exception $e) {
                $result = $this->inbox_ds->findPendingTicket(Auth::user()->id);
                return response()->json(['status' => false, 'message' => "Fatal Error : Unable connect to Remedy, your request not being processed ! " . $e->getMessage(), 'wsdl' => $wsdl->getResponseMessage(), 'wsdlStatus' => false, 'wsdl_request' => $wsdl->getArrContent(), 'result' => $result]);
            }
        }
    }

    private function convertServiceCatalog(Inbox $inbox)
    {
        $incident = new Incident();
        $incident->parseInbox($inbox);
        switch ($inbox->ServiceCatalogId) {
            case 2:
                $sc = $this->srvctlg_ds->findBySummaryAndTier($incident->summary_description, $incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                return ($sc ? $sc->id : 50);
                break;
            case 3:
                $sc = $this->srvctlg_ds->findBySummaryAndTier($incident->summary_description, $incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                return ($sc ? $sc->id : 38);
                break;
            case 4:
                $sc = $this->srvctlg_ds->findBySummaryAndTier($incident->summary_description, $incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                return ($sc ? $sc->id : 51);
                break;
            default:
                switch ($inbox->servicecatalog->servicechannel->service_type_name) {
                    case 'LAYANAN_IT':
                        return 3;
                        break;
                    case 'LAYANAN_CABANG':
                        return 2;
                        break;
                    case 'LAYANAN_CC':
                        return 4;
                        break;
                    default:
                        return $inbox->ServiceCatalogId;
                        break;
                }
                break;
        }
    }

    // private function afterClosureFeedbackWO(Inbox $request, Workorder $workorder, $note) \\ tidak dipakai sementara karena feedbacknotifierWO saat ini menggunakan notif_ds->performFeedBackNotifierWO
    // {
    //     if ($request->ProcessStatus == 'QUEUED' && $request->EmployeeAssigneeId != Auth::user()->employee_remedy_id) {
    //         $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $workorder->workorder_number . " telah kami teruskan ke tim " . $workorder->assignee_support_group_name . ", untuk penanganan lebih lanjut");
    //     }
    //     if ($request->ProcessStatus == 'PENDING' && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
    //         $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $workorder->workorder_number . " di pending untuk dilakukan analisa lebih lanjut, mohon maaf atas ketidaknyamanannya, mohon menunggu");
    //     }
    //     if ($request->ProcessStatus == 'INPROGRESS' && $request->EmployeeAssigneeId == Auth::user()->employee_remedy_id) {
    //         $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $workorder->workorder_number . " masih dalam proses penanganan oleh team Mandiri Servicedesk");
    //     }
    //     if ($request->ProcessStatus == 'INPROGRESS' && $request->EmployeeAssigneeId != Auth::user()->employee_remedy_id) {
    //         $this->chatFromAgent($request, "Permintaan dengan nomor tiket : " . $workorder->workorder_number . " telah kami teruskan ke tim " . $workorder->assignee_support_group_name . ", untuk penanganan lebih lanjut");
    //     }
    //     if ($request->ProcessStatus == 'CLOSED') {
    //         $this->chatFromAgent($request, 'Request ID: ' . $request->id . ' dengan nomor penanganan : ' . $workorder->workorder_number . ' telah selesai');
    //         $this->chatFromAgent($request, 'Note : ' . $note);
    //     }
    //     if ($request->ProcessStatus ==  'REJECT') {
    //         $this->chatFromAgent($request, 'Request ID: ' . $request->id . ' dengan nomor penanganan : ' . $workorder->workorder_number . ' telah dibatalkan');
    //         $this->chatFromAgent($request, 'Cancellation Note : ' . $note);
    //     }
    // }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // public function submitRetryClose()//Not Used
    // {
    //     // $task = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
    //     $task = $this->inbox_ds->find(request()->get('id'));
    //     $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($task->EmployeeAssigneeId, $task->EmployeeAssigneeGroupId);
    //     if (request()->get('type') == "WO") {
    //         $wsdl_update_status_rslt = $this->woupdate_wsdl->updateStatus($task, request()->get('status'), $actual_assignee->id);
    //         if ($wsdl_update_status_rslt->getResponseStatus() == 'success') {
    //             // if ($this->taskrequest_service->isRemedyTicketExist($wsdl_update_status_rslt->getContent())) {
    //             //     $duplicated = $this->taskrequest_service->findRemedyTicketDuplicated($wsdl_update_status_rslt->getContent(), $task->id);
    //             //     $this->taskrequest_service->mergeRequest($task, $duplicated);
    //             // } else {
    //             $this->taskrequest_service->updateStatusById(request()->get('id'), request()->get('assigneeId'), $actual_assignee->id, 'CLOSED', 'WO_COMPLETED');
    //             // }
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => true, "requestdata" => $data, "message" => "Ticket closed", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //         } else {
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => false, "requestdata" => $data, "message" => "Something went wrong! contact your Administrator immediately", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     } else {
    //         $wsdl_update_status_rslt = $this->inc_wsdl_service->updateIncidentStatus($task, request()->get('status'), $actual_assignee->id);
    //         if ($wsdl_update_status_rslt->getResponseStatus() == 'success') {
    //             // if ($this->taskrequest_service->isRemedyTicketExist($wsdl_update_status_rslt->getContent())) {
    //             //     $duplicated = $this->taskrequest_service->findRemedyTicketDuplicated($wsdl_update_status_rslt->getContent(), $task->id);
    //             //     $this->taskrequest_service->mergeRequest($task, $duplicated);
    //             // } else {
    //             $this->taskrequest_service->updateStatusIncidentById(request()->get('id'), 'CLOSED', 'IN_COMPLETED', $task->TextDecoded);
    //             // }
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => true, "requestdata" => $data, "message" => "Ticket closed", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //         } else {
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => false, "requestdata" => $data, "message" => "Something went wrong! contact your Administrator immediately", 'wsdl' => $wsdl_update_status_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     }
    // }

    // public function submitRetryDelegate()//Not Used
    // {
    //     // $task = $this->taskrequest_service->getQueueObjectById(request()->get('id'));
    //     $task = $this->inbox_ds->find(request()->get('id'));
    //     $actual_assignee = $this->user_service->getUserByEmployeeAndGroup($task->EmployeeAssigneeId, $task->EmployeeAssigneeGroupId);
    //     if (request()->get('type') == "IN") {
    //         $wsdl_ticket_checking = $this->inc_wsdl_service->isIncidentExist($task);
    //         if ($wsdl_ticket_checking->getResponseStatus() == true) {
    //             $wsdl_rslt = $this->inc_wsdl_service->updateIncidentAssignee($task, request()->get('delegateTo'));
    //             if ($wsdl_rslt->getResponseStatus() == 'success') {
    //                 // if ($this->taskrequest_service->isRemedyTicketExist($wsdl_rslt->getContent())) {
    //                 //     $duplicated = $this->taskrequest_service->findRemedyTicketDuplicated($wsdl_rslt->getContent(), $task->id);
    //                 //     $this->taskrequest_service->mergeRequest($task, $duplicated);
    //                 // } else {
    //                 $this->taskrequest_service->updateStatusById(request()->get('id'), Auth::user()->id, $actual_assignee->id, 'INPROGRESS', 'IN_PROCESS');
    //                 $this->taskrequest_service->updateQueueStatusById(request()->get('id'), 'INPROGRESS', $actual_assignee->id, request()->get('delegateTo'));
    //                 // }
    //                 $data = $this->taskrequest_service->getRequestListByActionTicket();
    //                 return response()->json(["status" => true, "requestdata" => $data, "message" => "Ticket " . $task->id . " delegated and has been moved from your task!", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //             } else {
    //                 $data = $this->taskrequest_service->getRequestListByActionTicket();
    //                 return response()->json(["status" => false, "requestdata" => $data, "message" => "Something went wrong! contact your Administrator immediately", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //             }
    //         } else {
    //             return response()->json(["status" => false, "message" => "Remedy ticket Id" . $task->RemedyTicketId . " does not exist in Remedy", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     } else {
    //         $wsdl_ticket_checking = $this->inc_wsdl_service->isIncidentExist($task);
    //         if ($wsdl_ticket_checking->getResponseStatus() == true) {
    //             $wsdl_rslt = $this->woupdate_wsdl->updateAssignee($task, request()->get('delegateTo'));
    //             if ($wsdl_rslt->getResponseStatus() == 'success') {
    //                 // if ($this->taskrequest_service->isRemedyTicketExist($wsdl_rslt->getContent())) {
    //                 //     $duplicated = $this->taskrequest_service->findRemedyTicketDuplicated($wsdl_rslt->getContent(), $task->id);
    //                 //     $this->taskrequest_service->mergeRequest($task, $duplicated);
    //                 // } else {
    //                 $this->taskrequest_service->updateStatusById(request()->get('id'), Auth::user()->id, $actual_assignee->id, 'INPROGRESS', 'WO_PROCESS');
    //                 $this->taskrequest_service->updateQueueStatusById(request()->get('id'), 'INPROGRESS', $actual_assignee->id, request()->get('delegateTo'));
    //                 // }
    //                 $data = $this->taskrequest_service->getRequestListByActionTicket();
    //                 return response()->json(["status" => true, "requestdata" => $data, "message" => "Ticket " . $task->id . " delegated and has been moved from your task!", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //             } else {
    //                 $data = $this->taskrequest_service->getRequestListByActionTicket();
    //                 return response()->json(["status" => false, "requestdata" => $data, "message" => "Something went wrong! contact your Administrator immediately", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //             }
    //         } else {
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => false, "requestdata" => $data, "message" => "Remedy ticket Id" . $task->RemedyTicketId . " does not exist in Remedy", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     }
    // }

    // public function saveDraft()//Not Used
    // {
    //     $wsdl_rslt = $this->inccreate_wsdl->createIncidentIncoming(request()->get('requestId'), request()->get('customer'), request()->get('assignee'), request()->get('content'));
    //     try {
    //         if ($wsdl_rslt->getResponseStatus() == 'success') {
    //             // $array_inbox = array(
    //             //     "RemedyTicketId" => $wsdl_rslt->getContent(),
    //             // );
    //             // $this->taskrequest_service->update($inbox->id, $array_inbox);
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => true, "requestdata" => $data, "ticket" => $wsdl_rslt->getContent(), 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => true]);
    //         } else {
    //             $data = $this->taskrequest_service->getRequestListByActionTicket();
    //             return response()->json(["status" => false, "requestdata" => $data, "message" => "Something went wrong! contact your Administrator immediately", 'wsdl' => $wsdl_rslt->getResponseMessage(), 'wsdlStatus' => false]);
    //         }
    //     } catch (Exception $e) {
    //         return response()->json(['status' => false, 'message' => "Fatal Error : Unable to connect to Remedy, your request not being processed!"]);
    //     }
    // }
}
