<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportGroupAssigneeRemedy extends Model
{

    use \Awobaz\Compoships\Compoships;

    protected $primaryKey = 'request_id';
    protected $fillable = ['support_group_assignee_remedy'];
    protected $table = 'support_group_assignee_remedy';
    public $timestamps = false;
    public $keyType = 'string';

    public function user()
    {
        return $this->hasOne(User::class, ['un_remedy', 'sgn_id_remedy'], ['remedy_login_id', 'support_group_id']);
    }

    public function inbox()
    {
        return $this->hasMany(User::class, ['EmployeeAssigneeId', 'EmployeeAssigneeGroupId'], ['employee_remedy_id', 'support_group_id']);
    }
}
