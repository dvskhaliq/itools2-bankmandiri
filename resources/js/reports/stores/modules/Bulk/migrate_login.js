import { data } from "jquery";
import ENUM from "../../enum";

const defaultItem = () => {
    return {
        message: {
            message: "",
            type: ""
        }
    };
};

const state = {
    apiState: ENUM.INIT,
    message: defaultItem().message,
    migrationData: {
        source: null,
        target: null,
        description: null,
        processStatus: null,
        wizardId: null,
        checkCount: 0
    },
};

const getters = {
    apiState: state => state.apiState,
    message: state => state.message,
    migrationData: state => state.migrationData
};

const actions = {
    initMigration({ state }, payload) {
        let data = {
            source: payload.source,
            target: payload.target,
            description: "Request Initialized",
            wizardId: null,
            processStatus: "submit"
        };
        state.migrationData = data;
    },
    async checkMigration({ commit }, payload) {
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/check_migration",
                payload
            );
            commit("setChecked", { payload: payload, data: response.data });
        } catch (error) {
            console.log(error);
            error.response.status == 403
                ? 
                commit("setMessage", {
                    message: "You are not authorized to do this!",
                    type: "error"
                })
                : commit("setMigrationError", {
                      payload: payload,
                      data: error.response.data
                  });
        }
    },
    async doMigration({ commit }, payload) {
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/migrate_login",
                payload
            );
            if (response.data.status) {
                commit("setSubmited", response.data);
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setMessage", {
                    message: "You are not authorized to do this!",
                    type: "error"
                })
                : commit("setMigrationError", {
                      payload: payload,
                      data: error.response.data
                  });
        }
    },
    async stopProcess({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/process/stop", payload);
                commit("setMessage", {
                    message: "The process has been successfully terminated !",
                    type: "success"
                })
        } catch (error) {
            error.response.status == 403
                ? commit("setMessage", {
                    message: error.response.data.error,
                    type: "error"
                })
                : commit("setMessage", {
                    message: "Something wrong !",
                    type: "error"
                });
        }
    },
};

const mutations = {
    setChecked: (state, rsp) => {
        console.log(rsp);
        let result = {
            source: rsp.payload.source,
            target: rsp.payload.target,
            description: rsp.data.message,
            processStatus: rsp.data.processStatus,
            wizardId: rsp.data.wizardId,
            checkCount: rsp.data.nextCheckCount
        };
        state.responseMessage = {
            id: rsp.data.wizardId,
            type: result.status,
            message: result.description
        };
        state.migrationData = result;
    },
    setSubmited: (state, data) => {
        let result = {
            source: data.source,
            target: data.target,
            description: data.message, //`Login ID ${data.source} has been migrated to ${data.target}`,
            processStatus: data.processStatus,
            wizardId: data.wizardId,
            checkCount: 1
        };
        state.responseMessage = {
            id: data.wizardId,
            type: result.status,
            message: result.description
        };
        state.migrationData = result;
    },
    setApiState: (state, enumApiState) => {
        state.apiState = enumApiState;
    },
    setMigrationError: (state, rsp) => {
        console.log(rsp);
        let result = {
            source: rsp.payload.source,
            target: rsp.payload.target,
            description: rsp.data.message, //`Login ID ${data.source} has been migrated to ${data.target}`,
            processStatus: 'rejected',
            wizardId: null,
            checkCount: 0
        };
        state.responseMessage = {
            id: result.source,
            type: result.status,
            message: result.description
        };
        state.migrationData = result;
    },
    setMessage: (state, message) => {
        switch (message.type) {
            case "success":
                state.apiState = ENUM.SUCCESS;
                break;
            case "error":
                state.apiState = ENUM.ERROR;
                break;
        }
        state.message.message = message.message
        state.message.type = message.type;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
