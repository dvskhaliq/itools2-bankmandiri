<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMTbluSwift extends Model
{
    protected $table = 'pm_tblu_ics';
    // protected $fillable = ['channel', 'code', 'in', 'out', 'response_time'];
    public $timestamps = false;
}
