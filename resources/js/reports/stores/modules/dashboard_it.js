import ENUM from "../enum";

const state = {
    years: [],
    dashboardit: [],
    dashboarditavg: [],
    simpleHeader: [
        { text: "Layanan", value: "layanan" },
        { text: "Node", value: "node" },
        { text: "System Downtime", value: "system_downtime" },
        { text: "%System Availability", value: "percent_system_avail", align: 'center'},
        { text: "Threshold Availability", value: "target_avail"},
        { text: "Restime", value: "restime", align: 'center'},
        { text: "Threshold Response Time", value: "threshold_restime"},
        { text: "Success Rate", value: "success_rate", align: 'center'},
        { text: "Threshold Success Rate", value: "target_success_rate"},
        { text: "Sc Node", value: "sc_node"},
        { text: "Recorded at", value:"tanggal"}
    ],
    simpleHeaderAvg: [
        { text: "Layanan", value: "layanan" },
        { text: "SUM System Downtime", value: "system_downtime" },
        { text: "AVG %System Avail", value: "percent_system_avail", align: 'center'},
        { text: "Threshold %System Avail", value: "target_avail"},
        { text: "AVG Restime", value: "restime", align: 'center'},
        { text: "Threshold Restime", value: "threshold_restime"},
        { text: "SR", value: "success_rate", align: 'center'},
        { text: "Threshold SR", value: "threshold_success_rate"},
    ],
    apiState: ENUM.INIT,
    alertMessage: "",
};

const getters = {
    getDashboardItAvg: state => state.dashboarditavg,
    getDashboardIt: state => state.dashboardit,
    getSimpleHeader: state => state.simpleHeader,
    getSimpleHeaderAvg: state => state.simpleHeaderAvg,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getYears: state => state.years,
};

const actions = {
    async initYears({ commit }) {
        let response;
        try {
          response = await axios.get("/data_management/problems/dashboard_it/fetchYears");
          commit("setYears", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
    async initDashboardIt({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/data_management/problems/dashboard_it/fetch");
          commit("setDashboardIt", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateDashboardItByDate({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/dashboard_it/getByDate", payload
              );
            commit("setDashboardIt", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateDashboardItByMonth({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/dashboard_it/getByMonth", payload
              );
            commit("setDashboardIt", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateDashboardItByMonthAvg({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/dashboard_it/getByMonthAvg", payload
              );
            commit("setDashboardItAvg", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateDashboardItByYear({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
                response = await axios.post(
                "/data_management/problems/dashboard_it/getByYear", payload
              );
            commit("setDashboardIt", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },

    resetDashboardIt({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setDashboardIt", []);
    },
    resetDashboardItAvg({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setDashboardItAvg", []);
    },
    resetYears({ commit }) {
        commit("setYears", []);
    }
};

const mutations = {
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setYears: (state, years) => {
        state.years = years;
    },
    setDashboardIt: (state, successrate) => {
        state.apiState = ENUM.LOADED;
        state.dashboardit = successrate;
    },
    setDashboardItAvg: (state, successrateavg) => {
        state.apiState = ENUM.LOADED;
        state.dashboarditavg = successrateavg;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
