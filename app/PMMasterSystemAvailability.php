<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMMasterSystemAvailability extends Model
{
    protected $table = 'pm_master_system_availability';
    public $timestamps = false;

    public function detail(){
        return $this->hasMany(PMSystemAvailability::class,'id_system','id');
    }

    public function pmmasteradpybridge(){
        return $this->hasMany(PMMasterApdyBridge::class,'system_id','id');
    }


}
