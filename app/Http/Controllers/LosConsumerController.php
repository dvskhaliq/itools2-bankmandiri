<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use View;
use App\LosConsumer;

class LosConsumerController extends BaseController
{
    /**
      * Create a new controller instance.
      *
      * @return void
      */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
       * Show the application dashboard.
       *
       * @return \Illuminate\Http\Response
       */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return View::make('los/los_consumer/los_consumer');
    }

    public function findDataByApRegNo()
    {
        $results = LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->first();
        if ($results != null) {
            return response()->json([
                'status' => true,
                'result' => $results,
            ]);
        } else {
            return response()->json([
                'status' => false,
                'result' => 'AP_REGNO : ' . request()->get('AP_REGNO') . ' not found in table APPFLAG',
            ]);
        }
    }

    public function submitUpdateApCurrtrcode()
    {
        $before = LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->first();
        LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->update(['AP_CURRTRCODE' => request()->get('AP_CURRTRCODE')]);
        $after = LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->first();
        return response()->json([
            'status' => true,
            'result' => [
                'before' => $before,
                'after' => $after,
                'message' => 'AP CURRTRCODE change to '.request()->get('AP_CURRTRCODE').' successs',
            ]
        ]);
    }

    public function submitUpdateApNexttrby(){
        $before = LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->first();
        LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->update(['AP_NEXTTRBY' => request()->get('AP_NEXTTRBY')]);
        $after = LosConsumer::where('AP_REGNO', request()->get('AP_REGNO'))->first();
        return response()->json([
            'status' => true,
            'result' => [
                'before' => $before,
                'after' => $after,
                'message' => 'AP NEXTTRBY change to '.request()->get('AP_NEXTTRBY').' successs',
            ]
        ]);
    }
}
