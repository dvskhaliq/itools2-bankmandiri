<?php

namespace App\ServiceHandler\PBMLayer;

use App\ViewDashboardIt;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class DashboardItImp implements DashboardItService
{
    public function getDashboardIt(): ?Collection
    {
        $result = ViewDashboardIt::where('tanggal', '=', date('Y-m-d', strtotime('yesterday')))->orderBy('tanggal','desc')
        ->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getDashboardItByYear(array $years): ?Collection
    {
        $result = ViewDashboardIt::where(function($q) use($years) {
            foreach ($years as $year) {
                $q->whereYear('tanggal', '=', $year, 'or');
            }
        })->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getDashboardItByMonth($month): ?Collection
    {
        $result = ViewDashboardIt::whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getDashboardItByMonthLayanan($layanan, $month): ?Collection
    {
        $result = ViewDashboardIt::where('layanan',$layanan)
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','asc')->get();
        return $result;
    }

    public function getDashboardItByMonthAvg($month): ?Collection
    {
        $result = ViewDashboardIt::select(DB::raw("to_char(tanggal, 'YYYY-MM') as bulan, layanan,sum(system_downtime) as system_downtime, avg(percent_system_avail) as percent_system_avail, avg(target_avail) as target_avail, avg(restime) as restime, avg(threshold_restime) as threshold_restime, avg(success_rate) as success_rate, avg(target_success_rate) as target_success_rate"))
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->groupBy('bulan')->groupBy('layanan')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getDashboardItByDate($date): ?Collection
    {
        $result = ViewDashboardIt::where('tanggal', '=', $date)->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getYears(){
        return DB::select("select distinct EXTRACT(YEAR from tanggal) as year from view_dashboard_it order by EXTRACT(YEAR from tanggal) desc");
    }
}
