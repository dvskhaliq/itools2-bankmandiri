import ENUM from "../../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            layanan: "",
            node: "",
            count:"",
            sum:"",
            status:"",
            received_date:null,
        }
    };
};

const state = {
    responsetime_s: [],
    responsetime_savg: [],
    years: [],
    editedItem: defaultItem().item,
    editedIndex: defaultItem().index,
    simpleHeader: [
        { text: "Service Name", value: "layanan" },
        { text: "Node", value: "node"},
        { text: "Count", value: "count"},
        { text: "Sum", value: "sum"},
        { text: "Status", value: "status", align: 'center'},
        { text: "Date", value: "received_date"}
    ],
    simpleHeaderAvg: [
        { text: "Service Name", value: "layanan" },
        { text: "Average Count", value: "count" },
        { text: "Average Sum", value: "sum" },
        { text: "Date", value: "received_date" },
    ],
    apiState: ENUM.INIT,
    alertMessage: "",
};

const getters = {
    getResponseTime_sAvg: state => state.responsetime_savg,
    getResponseTime_s: state => state.responsetime_s,
    getYears: state => state.years,
    getSimpleHeader: state => state.simpleHeader,
    getSimpleHeaderAvg: state => state.simpleHeaderAvg,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getEditItem: state => state.editedItem,
};

const actions = {
    async initResponseTime({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/data_management/problems/response_time/apdy/fetch");
          commit("setResponseTime_s", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByDate({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/response_time/apdy/getByDate", payload
              );
            commit("setResponseTime_s", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByMonth({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/response_time/apdy/getByMonth", payload
              );
            commit("setResponseTime_s", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByMonthAvg({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
              response = await axios.post(
                "/data_management/problems/response_time/apdy/getByMonthAvg", payload
              );
            commit("setResponseTime_sAvg", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
      async populateResponseTimeByYear({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
                response = await axios.post(
                "/data_management/problems/response_time/apdy/getByYear", payload
              );
            commit("setResponseTime_s", response.data.data);  
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
      },
    async fetchYears({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/response_time/apdy/fetchYears");
            commit("setYear", response.data.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Something went wrong on fetch!"
                  );
        }
    },
    async updateResponseTime({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.put("/data_management/problems/response_time/apdy/update", payload);
            commit("updateResponseTime", response.data.data);
            commit("setSuccess", "Data has been successfully updated !");
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Updating data has failed"
                  );
        }
    },
    async addBulkApdy({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/master_apdy_bridge/create/longranged", payload);
            if(response.data.status==true){
                commit("setSuccess", "Data has been successfully saved !");
            }else{
                commit("setWarning", "Unable to save, please ask administrator !");
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Failure System ! Adding data has failed"
                  );
        }
    },
    resetResponseTime({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setResponseTime_s", []);
    },
    resetResponseTimeAvg({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setResponseTime_sAvg", []);
    },
    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
};

const mutations = {
    updateResponseTime: (state, updatedResponseTime) => {
        state.apiState = ENUM.LOADED;
        const index = state.responsetime_s.findIndex(responsetime => responsetime.id == updatedResponseTime.id);
        state.responsetime_s.splice(index, 1, updatedResponseTime);
    },
    setEditItem: (state, payload) => {
        state.editedItem = payload;
    },
    setEditIndex: (state, index) => {
        state.editedIndex = index;
    },
    setApiState: (state, enumApiState) => {        
        state.apiState = enumApiState;
    },
    setResponseTime_s: (state, responsetime) => {
        state.apiState = ENUM.LOADED;
        state.responsetime_s = responsetime;
    },
    setResponseTime_sAvg: (state, responsetimeavg) => {
        state.apiState = ENUM.LOADED;
        state.responsetime_savg = responsetimeavg;
    },
    setYear: (state, year) => {
        state.apiState = ENUM.LOADED;
        state.years = year;
    },
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
