<?php

namespace App\ServiceHandler\IntegrationLayer;

interface DLDMigrateLoginService
{
    public function initMigration($source_login_id,$target_login_id,$event_guid);

    public function checkMigrationStatus($data_wizard_id);
}
