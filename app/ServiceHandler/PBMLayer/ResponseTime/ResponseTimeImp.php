<?php

namespace App\ServiceHandler\PBMLayer\ResponseTime;

use App\ViewResponseTime as ResponseTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ResponseTimeImp implements ResponseTimeService
{
    public function getResponseTime(): ?Collection
    {
        return ResponseTime::where('tanggal', '=', date('Y-m-d', strtotime('yesterday')))->orderBy('tanggal','desc')
        ->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByDate($date): ?Collection
    {
        return ResponseTime::where('tanggal', '=', $date)->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByMonth($month): ?Collection
    {
        return ResponseTime::whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getResponseTimeByMonthSystem($layanan, $month): ?Collection
    {
        $result = ResponseTime::where('layanan',$layanan)
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->orderBy('tanggal','asc')->get();
        return $result;
    }

    public function getResponseTimeByMonthAvg($month): ?Collection
    {
        $result = ResponseTime::select(DB::raw("layanan, AVG(restime) as restime, to_char(tanggal, 'YYYY-MM') as bulan"))
        ->whereMonth('tanggal', '=', date('m', strtotime($month)))->whereYear('tanggal', '=', date('Y', strtotime($month)))
        ->groupBy('layanan')->groupBy('bulan')->orderBy('layanan','asc')->get();
        return $result;
    }

    public function getResponseTimeByYear(array $years): ?Collection
    {
        return ResponseTime::where(function($q) use($years) {
            foreach ($years as $year) {
                $q->whereYear('tanggal', '=', $year, 'or');
            }
        })->orderBy('tanggal','desc')->orderBy('layanan','asc')->get();
    }

    public function getYears(){
        return DB::select("select distinct EXTRACT(YEAR from tanggal) as year from view_response_time order by EXTRACT(YEAR from tanggal) desc");
    }
}