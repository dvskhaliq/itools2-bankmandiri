<?php

namespace App\ServiceHandler\DataLayer;

interface CPSServerService
{
    public function queryTT1($osn);

    public function updateDataTT1($trackid, $osn, $old_data, $remedy_ticket, $summary_ticket);

    public function queryTT2($data);

    public function updateDataTT2($trackid, $ritrrf, $old_data, $remedy_ticket, $summary_ticket);

    public function queryTT3($data);

    public function updateDataTT3($trackid, $ritrrf, $old_data, $remedy_ticket, $summary_ticket);

    public function queryTT4($data);

    public function updateDataTT4($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);

    public function queryRTGS1($data);

    public function updateDataRTGS1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);

    public function queryRTGS2($data);

    public function updateDataRTGS2($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);

    public function queryMCS1($data);

    public function updateDataMCS1($trackid, $data, $old_data, $remedy_ticket, $summary_ticket);

    public function auditTrailView($field, $search);

    public function queryMandol1($data, $field);
}