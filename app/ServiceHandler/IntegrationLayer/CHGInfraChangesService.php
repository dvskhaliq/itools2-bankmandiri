<?php

namespace App\ServiceHandler\IntegrationLayer;

interface CHGInfraChangesService
{
    public function getInfraChangesList($username,$password,$start_date,$end_date);
}
