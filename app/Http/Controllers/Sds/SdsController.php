<?php

namespace App\Http\Controllers\Sds;

use App\Http\Controllers\BaseController;
use App\RemedyHPDIncidentInterface as Incident;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class SdsController extends BaseController
{

    public function index()
    {
        // return redirect('/sds/console/dashboard');
        return View::make('sds/public/index');
    }

    public function fetch(){
        // $data = Incident::select("Incident_Number as RemedyTicketId, CASE
        // WHEN Reported_Source = 2000 THEN 'Email'
        // WHEN Reported_Source = 4200 THEN 'Self Service'
        // WHEN Reported_Source = 6000 THEN 'Phone'
        // WHEN Reported_Source = 11004 THEN 'Whatsapp'
        // WHEN Reported_Source = 11007 THEN 'Telegram'
        // else null END as RecipientID, CASE
        // WHEN Status = 0 THEN 'NEW'
        // WHEN Status = 1 THEN 'QUEUED'
        // WHEN Status = 2 THEN 'INPROGRESS'
        // WHEN Status = 4 THEN 'RESOLVED'
        // WHEN Status = 5 THEN 'SYSTEM_CLOSED'
        // WHEN Status = 6 THEN 'CANCELLED'
        // else null END as ProcessStatus,convert(char(20),(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')),20) as ReceivingDateTime, convert(char(20),GETDATE(),20) as CurrentDate, DATEDIFF(MINUTE, convert(char(20),(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')),20), convert(char(20),GETDATE(),20)) as Sla, Assigned_Group as EmployeeAssigneeGroup")->get();
        // $data = Incident::select('Incident_Number')->where(DB::raw("CONVERT(DATE,(DATEADD(ss,Submit_Date,'19700101 07:00:00:000')))"), date("Y-m-d"))->get();
        // $data = Incident::select('Incident_Number')->where("Incident_Number", "INC000005003345")->get();
        
        // PT. BANK MANDIRI, TBK | SERVICEDESK - 2ND LAYER INCIDENT = SGP000000000414
        // MANDIRI CASH MANAGEMENT | SERVICEDESK - MCM = SGP000000000196
        // MANDIRI CASH MANAGEMENT | SERVICEDESK - SCM = SGP000000009213
        $temp = [
            ["Sla"=>221,"RemedyTicketId"=>"INC000005008662","RecipientID"=>"WA","ProcessStatus"=>"RESOLVED","ReceivingDateTime"=>"2021-03-02 13:22:42","EmployeeAssigneeGroupId"=>"SGP000000000414","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes1#Summary1#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution##"],
            ["Sla"=>133,"RemedyTicketId"=>"INC000005008669","RecipientID"=>"WA","ProcessStatus"=>"RESOLVED","ReceivingDateTime"=>"2021-03-02 13:24:31","EmployeeAssigneeGroupId"=>"SGP000000000414","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes2#Summary2#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution2##"],
            ["Sla"=>216,"RemedyTicketId"=>"INC000005008717","RecipientID"=>"WA","ProcessStatus"=>"INPROGRESS","ReceivingDateTime"=>"2021-03-02 13:17:55","EmployeeAssigneeGroupId"=>"SGP000000000414","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes3#Summary3#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution3##"],
            ["Sla"=>199,"RemedyTicketId"=>"INC000005008722","RecipientID"=>"WA","ProcessStatus"=>"QUEUED","ReceivingDateTime"=>"2021-03-02 13:20:30","EmployeeAssigneeGroupId"=>"SGP000000000414","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes4#Summary4#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution4##"],
            ["Sla"=>158,"RemedyTicketId"=>"INC000005006274","RecipientID"=>"CALL","ProcessStatus"=>"RESOLVED","ReceivingDateTime"=>"2021-03-02 08:19:58","EmployeeAssigneeGroupId"=>"SGP000000000196","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes5#Summary5#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution5##"],
            ["Sla"=>142,"RemedyTicketId"=>"INC000005007368","RecipientID"=>"EMAIL","ProcessStatus"=>"QUEUED","ReceivingDateTime"=>"2021-03-02 08:19:15","EmployeeAssigneeGroupId"=>"SGP000000000196","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes6#Summary6#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution6##"],
            ["Sla"=>157,"RemedyTicketId"=>"INC000005007659","RecipientID"=>"EMAIL","ProcessStatus"=>"INPROGRESS","ReceivingDateTime"=>"2021-03-02 09:36:49","EmployeeAssigneeGroupId"=>"SGP000000000196","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes7#Summary7#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution7##"],
            ["Sla"=>171,"RemedyTicketId"=>"INC000005007725","RecipientID"=>"CALL","ProcessStatus"=>"QUEUED","ReceivingDateTime"=>"2021-03-02 09:51:54","EmployeeAssigneeGroupId"=>"SGP000000000196","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes8#Summary8#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution8##"],
            ["Sla"=>118,"RemedyTicketId"=>"INC000005007752","RecipientID"=>"CALL","ProcessStatus"=>"QUEUED","ReceivingDateTime"=>"2021-03-02 09:51:54","EmployeeAssigneeGroupId"=>"SGP000000009213","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes9#Summary9#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution9##"],
            ["Sla"=>10,"RemedyTicketId"=>"INC000005007753","RecipientID"=>"CALL","ProcessStatus"=>"QUEUED","ReceivingDateTime"=>"2021-03-02 09:51:54","EmployeeAssigneeGroupId"=>"SGP000000009213","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes9#Summary9#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution9##"],
            ["Sla"=>21,"RemedyTicketId"=>"INC000005007754","RecipientID"=>"CALL","ProcessStatus"=>"QUEUED","ReceivingDateTime"=>"2021-03-02 09:51:54","EmployeeAssigneeGroupId"=>"SGP000000009213","TextDecoded"=>"Incident generic from EMAIL#PT. BANK MANDIRI, TBK#User Service Restoration#Notes9#Summary9#SERVICEDESK#MUHAMMAD ILHAM CANTA YUDA#4-Minor/Localized#4-Low#Low#Email#PT. BANK MANDIRI, TBK#APPLICATION#CHANNEL#SMS BANKING###INFO#-#-#Resolution9##"],
        ];
        $data = $this->refactor($temp);
        $ticketAssign = array_filter($data,function ($item){if($item["ProcessStatus"]=="QUEUED"){return $item;}});
        $ticketInprogress = array_filter($data,function ($item){if($item["ProcessStatus"]=="INPORGRESS"){return $item;}});
        $ticketClosed = array_filter($data,function ($item){if($item["ProcessStatus"]=="RESOLVED" || $item["ProcessStatus"]=="SYSTEM_CLOSED" || $item["ProcessStatus"]=="CANCELLED"){return $item;}});
        //2nd Layer
        $ticketSecondLayer = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000414"){return $item;}});
        $ticketSecondLayerAssign = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000414" && $item["ProcessStatus"]=="QUEUED"){return $item;}});
        $ticketSecondLayerInprogress = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000414" && $item["ProcessStatus"]=="INPROGRESS"){return $item;}});
        $ticketSecondLayerClosed = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000414" && ($item["ProcessStatus"]=="INPROGRESS" || $item["ProcessStatus"]=="SYSTEM_CLOSED" || $item["ProcessStatus"]=="CANCELLED")){return $item;}});
        //MCM
        $ticketMcm = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000196"){return $item;}});
        $ticketMcmAssign = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000196" && $item["ProcessStatus"]=="QUEUED"){return $item;}});
        $ticketMcmInprogress = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000196" && $item["ProcessStatus"]=="INPROGRESS"){return $item;}});
        $ticketMcmClosed = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000000196" && ($item["ProcessStatus"]=="INPROGRESS" || $item["ProcessStatus"]=="SYSTEM_CLOSED" || $item["ProcessStatus"]=="CANCELLED")){return $item;}});
        //SCM
        $ticketScm = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000009213"){return $item;}});
        $ticketScmAssign = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000009213" && $item["ProcessStatus"]=="QUEUED"){return $item;}});
        $ticketScmInprogress = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000009213" && $item["ProcessStatus"]=="INPROGRESS"){return $item;}});
        $ticketScmClosed = array_filter($data,function ($item){if($item["EmployeeAssigneeGroupId"]=="SGP000000009213" && ($item["ProcessStatus"]=="INPROGRESS" || $item["ProcessStatus"]=="SYSTEM_CLOSED" || $item["ProcessStatus"]=="CANCELLED")){return $item;}});
        return response()->json([
            "status" => true,
            "ticket"=>$data,
            "ticketAssign"=>$ticketAssign,
            "ticketInprogress"=>$ticketInprogress,
            "ticketClosed"=>$ticketClosed,
            "ticketSecondLayer"=>$ticketSecondLayer,
            "ticketSecondLayerAssign"=>$ticketSecondLayerAssign,
            "ticketSecondLayerInprogress"=>$ticketSecondLayerInprogress,
            "ticketSecondLayerClosed"=>$ticketSecondLayerClosed,
            "ticketMcm"=>$ticketMcm,
            "ticketMcmAssign"=>$ticketMcmAssign,
            "ticketMcmInprogress"=>$ticketMcmInprogress,
            "ticketMcmClosed"=>$ticketMcmClosed,
            "ticketScm"=>$ticketScm,
            "ticketScmAssign"=>$ticketScmAssign,
            "ticketScmInprogress"=>$ticketScmInprogress,
            "ticketScmClosed"=>$ticketScmClosed,
            ]);
    }

    public function refactor($data){
        $hasil=array();
        foreach ($data as $key => $item) {
            $text_encoded = explode("#", $item['TextDecoded']);
            array_push($hasil, [
                'Sla' => $item['Sla'],
                'RemedyTicketId' => $item['RemedyTicketId'],
                'RecipientID' => $item['RecipientID'],
                'ProcessStatus' => $item['ProcessStatus'],
                'ReceivingDateTime' => $item['ReceivingDateTime'],
                'EmployeeAssigneeGroupId' => $item['EmployeeAssigneeGroupId'],
                'TextDecoded' => $item['TextDecoded'],
                'Notes' => $text_encoded[3],
                'Summary' => $text_encoded[4],
                'Assignee' => $text_encoded[6],
                'Impact' => $text_encoded[7],
                'Urgency' => $text_encoded[8],
                'Priority' => $text_encoded[9],
                ]);
        }
        return $hasil;
    }
        
}
