<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebServiceResponse extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $response_status;
    protected $response_message;
    protected $content;
    protected $arr_contents;

    public function __construct(?String $response_message, ?String $response_status, $content, $arr_contents = null)
    {
        $this->response_message = $response_message;
        $this->response_status = $response_status;
        $this->content = $content;
        $this->arr_contents = $arr_contents;
    }

    public function getResponseMessage()
    {
        return $this->response_message;
    }

    public function getResponseStatus()
    {
        return $this->response_status;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getArrContent()
    {
        return $this->arr_contents;
    }
}
