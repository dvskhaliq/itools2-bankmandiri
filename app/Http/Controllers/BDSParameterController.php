<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;


class BDSParameterController extends BaseController
{
    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {
        return View::make('bds_connection/bds_parameter/bds_parameter');
    }

    public function getDirContents($dir, &$results = array())
    {
        date_default_timezone_set('Asia/Jakarta');        
        $files = shell_exec($dir);
        $test = explode("\n", $files);
        foreach($test as $value){
            $coba = explode(" ", $value);
            foreach($coba as $item){
                if($item == 'dparam.dat' || $item=='dailydparam.dat' || $item=='param.dat' || $item == 'dailyparam.dat'){
                    $results[] = ['fileName' => $item, 'lastDateModify' => date('d F Y H:i:s A',strtotime(substr($value,-26)))];
                }

            }
        }
        return $results;
    }
    
    public function getParameterFile()
    {
        $host = Session::get('data.ipServer');
        $kdcabang = Session::get('data.database');        
        return  response()->json(['result' => $this->getDirContents("smbclient //".$host."/bds -U 'admin-".$kdcabang."%Arch1p3l@g0' -c 'dir'")]);
    }

    public function getParameterDB()
    {
        $DefaultParamData = $this->bdsServerService->initBdsConnection()->table('DefaultParamData')->count();
        $ParamData = $this->bdsServerService->initBdsConnection()->table('ParamData')->count();
        $CNDest = $this->bdsServerService->initBdsConnection()->table('CNDest')->count();
        $SandiBank = $this->bdsServerService->initBdsConnection()->table('SandiBank')->count();
        $GLRef = $this->bdsServerService->initBdsConnection()->table('GLRef')->count();
        $InkasoCityBranch = $this->bdsServerService->initBdsConnection()->table('InkasoCityBranch')->count();

        $Branch = $this->bdsServerService->initBdsConnection()->table('Branch')->count();
        return response()->json([
            'result' => [
                // 'data' => [
                'DefaultParamData' => $DefaultParamData,
                'ParamData' => $ParamData,
                'CNDest' => $CNDest,
                'SandiBank' => $SandiBank,
                'GLRef' => $GLRef,
                'InkasoCityBranch' => $InkasoCityBranch,
                'Branch' => $Branch
                // ]
            ]
        ]);
    }
}