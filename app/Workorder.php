<?php

namespace App;

use App\Inbox;
use App\SupportGroupAssigneeRemedy;
use App\EmployeeRemedy;

class Workorder
{
    //Entity of Requestor
    public $requester_person_id;
    public $requester_full_name;
    public $requester_first_name;
    public $requester_last_name;
    public $requester_company;
    public $requester_email = '';
    public $requester_region_name;
    public $requester_group_name;
    public $requester_dept_name;
    //Entity of Workorder
    public $workorder_type;
    public $status;
    public $workorder_number;
    public $reported_source;
    public $service_cat_name;
    public $service_type;
    public $notes_description;
    public $summary_description;
    public $priority;
    public $prod_cat_tier1;
    public $prod_cat_tier2;
    public $prod_cat_tier3;
    public $opr_cat_tier1;
    public $opr_cat_tier2;
    public $opr_cat_tier3;
    public $resrv_field_1;
    public $resrv_field_2;
    public $resrv_field_3;
    public $resrv_field_4;
    public $resrv_field_5;
    public $resrv_field_6;
    public $resrv_field_7;
    public $resrv_field_8;
    public $resrv_field_9;
    public $resrv_field_10;
    public $resrv_field_11;
    public $resrv_field_12;
    public $resrv_field_13;
    public $resrv_field_14;
    public $resrv_field_15;
    //Entity of Assignee
    public $assignee_support_company;
    public $assignee_support_organization;
    public $assignee_support_group_name;
    public $assignee_support_group_id;
    public $assignee_name;
    public $assignee_first_name;
    public $assignee_user_id;
    public $assignee_id;
    public $assignee_login_id;
    //Entity of iTools
    public $itools_request_id;
    public $text_decoded;
    public $udh;

    public function parseInbox(Inbox $request): ?Workorder
    {
        $text_encoded = explode("#", $request->TextDecoded);
        $cust = EmployeeRemedy::find($request->EmployeeSenderId);
        $asgn = SupportGroupAssigneeRemedy::where("employee_remedy_id", $request->EmployeeAssigneeId)->where("support_group_id", $request->EmployeeAssigneeGroupId)->first();
        $status = "";
        switch ($request->ProcessStatus) {
            case 'INPROGRESS':
                $status = "In Progress";
                break;
            case 'PENDING':
                $status = "Pending";
                break;
            case "QUEUED":
                $status = "Assigned";
                break;
            case "CLOSED":
                $status = "Completed";
                break;
            case "REJECT":
                $status = "Rejected";
                break;
            case "REJECT":
                $status = "Cancelled";
                break;
            case "WAITINGAPR":
                $status = "Waiting Approval";
                break;
            case "PLANNING":
                $status = "Planning";
                break;
        }
        try {
            $this->itools_request_id = $request->id;
            $this->workorder_number = $request->RemedyTicketId;
            $this->status = $status;
            if ($cust) {
                $this->requester_company = $cust->company;
                $this->requester_person_id = $cust->id;
                $this->requester_first_name = $cust->first_name;
                $this->requester_last_name = $cust->last_name;
                $this->requester_dept_name = $cust->department_name;
                $this->requester_region_name = $cust->region_name;
                $this->requester_group_name = $cust->group_name;
                $this->requester_full_name = $cust->full_name;
            }
            if ($asgn) {
                $this->assignee_support_company = $asgn->company;
                $this->assignee_support_organization = $asgn->organization;
                $this->assignee_support_group_name = $asgn->support_group_name;
                $this->assignee_support_group_id = $asgn->support_group_id;
                $this->assignee_name = $asgn->full_name;
                $this->assignee_first_name = $asgn->first_name;
                $this->assignee_user_id = $asgn->user->id;
                $this->assignee_id = $asgn->employee_remedy_id;
                $this->assignee_login_id = $asgn->remedy_login_id;
            }
            $this->reported_source = $request->servicecatalog->servicechannel->name;
            $this->service_cat_name = $text_encoded[0];
            $this->service_type = $text_encoded[2];
            $this->notes_description = $text_encoded[3];
            $this->summary_description = $text_encoded[4];
            $this->priority = $text_encoded[7];
            $this->prod_cat_tier1 = $text_encoded[8];
            $this->prod_cat_tier2 = $text_encoded[9];
            $this->prod_cat_tier3 = $text_encoded[10];
            $this->opr_cat_tier1 = $text_encoded[11];
            $this->opr_cat_tier2 = $text_encoded[12];
            $this->opr_cat_tier3 = $text_encoded[13];
            $this->resrv_field_1 = $text_encoded[14];
            $this->resrv_field_2 = $text_encoded[15];
            $this->resrv_field_3 = $text_encoded[16];
            $this->resrv_field_4 = $text_encoded[17];
            $this->resrv_field_5 = $text_encoded[18];
            $this->resrv_field_6 = $text_encoded[19];
            $this->resrv_field_7 = $text_encoded[20];
            $this->resrv_field_8 = $text_encoded[21];
            $this->resrv_field_9 = $text_encoded[22];
            $this->resrv_field_10 = $text_encoded[23];
            $this->resrv_field_11 = $text_encoded[24];
            $this->resrv_field_12 = $text_encoded[25];
            $this->resrv_field_13 = $text_encoded[26];
            $this->resrv_field_14 = $text_encoded[27];
            $this->resrv_field_15 = $text_encoded[28];
            $this->text_decoded = $this->service_cat_name . "#"
                . $this->requester_company . "#"
                . $this->service_type . "#"
                . $this->notes_description . "#"
                . $this->summary_description . "#SERVICEDESK#"
                . $this->assignee_support_group_name . "#"
                . $this->priority . "#"
                . $this->prod_cat_tier1 . "#"
                . $this->prod_cat_tier2 . "#"
                . $this->prod_cat_tier3 . "#"
                . $this->opr_cat_tier1 . "#"
                . $this->opr_cat_tier2 . "#"
                . $this->opr_cat_tier3 . "#"
                . $this->resrv_field_1 . "#"
                . $this->resrv_field_2 . "#"
                . $this->resrv_field_3 . "#"
                . $this->resrv_field_4 . "#"
                . $this->resrv_field_5 . "#"
                . $this->resrv_field_6 . "#"
                . $this->resrv_field_7 . "#"
                . $this->resrv_field_8 . "#"
                . $this->resrv_field_9 . "#"
                . $this->resrv_field_10 . "#"
                . $this->resrv_field_11 . "#"
                . $this->resrv_field_12 . "#"
                . $this->resrv_field_13 . "#"
                . $this->resrv_field_14 . "#"
                . $this->resrv_field_15;
            $this->udh = $this->requester_full_name . "#"
                . $this->requester_first_name . "#"
                . $this->requester_last_name . "#"
                . $this->requester_email . "#"
                . $this->requester_company . "#"
                . $this->requester_region_name . "#"
                . $this->requester_group_name . "#"
                . $this->requester_dept_name;
            return $this;
        } catch (\Exception $th) {
            return null;
        }
    }

    public function generateStatus(String $status)
    {
        $process_status = '';
        switch ($status) {
            case  "In Progress":
                $process_status = 'INPROGRESS';
                break;
            case  "Pending":
                $process_status = 'PENDING';
                break;
            case  "Assigned":
                $process_status = "QUEUED";
                break;
            case  "Completed":
                $process_status = "CLOSED";
                break;
            case  "Cancelled":
                $process_status = "REJECT";
                break;
            case  "Rejected":
                $process_status = "REJECT";
                break;
            case  "Waiting Approval":
                $process_status = "WAITINGAPR";
                break;
            case  "Planning":
                $process_status = "PLANNING";
                break;
        }
        return $process_status;
    }

    public function generateUDH($array_property)
    {
        return array_key_exists("requester_full_name", $array_property['requester_full_name']) ? $array_property("requester_full_name") : null . "#"
            . (array_key_exists("requester_first_name", $array_property['requester_first_name']) ? $array_property("requester_first_name") : null ). "#"
            . (array_key_exists("requester_last_name", $array_property['requester_last_name']) ? $array_property("requester_last_name") : null ). "#"
            . (array_key_exists("requester_email", $array_property['requester_email']) ? $array_property("requester_email") : null ). "#"
            . (array_key_exists("requester_company", $array_property['requester_company']) ? $array_property("requester_company") : null ). "#"
            . (array_key_exists("requester_region_name", $array_property['requester_region_name']) ? $array_property("requester_region_name") : null ). "#"
            . (array_key_exists("requester_group_name", $array_property['requester_group_name']) ? $array_property("requester_group_name") : null ). "#"
            . (array_key_exists("requester_dept_name", $array_property['requester_dept_name']) ? $array_property("requester_dept_name") : null);
    }

    public function generateTextDecoded($array_property)
    {
        return (array_key_exists("service_cat_name", $array_property) ? $array_property["service_cat_name"] : null) . "#"
            . (array_key_exists("requester_company", $array_property) ? $array_property["requester_company"] : null) . "#"
            . (array_key_exists("service_type", $array_property) ? $array_property["service_type"] : null) . "#"
            . (array_key_exists("notes_description", $array_property) ? $array_property["notes_description"] : null) . "#"
            . (array_key_exists("summary_description", $array_property) ? $array_property["summary_description"] : null) . "#Servicedesk#"
            . (array_key_exists("assignee_support_group_name", $array_property) ? $array_property["assignee_support_group_name"] : null) . "#"
            . (array_key_exists("priority", $array_property) ? $array_property["priority"] : null) . "#"
            . (array_key_exists("prod_cat_tier1", $array_property) ? $array_property["prod_cat_tier1"] : null) . "#"
            . (array_key_exists("prod_cat_tier2", $array_property) ? $array_property["prod_cat_tier2"] : null) . "#"
            . (array_key_exists("prod_cat_tier3", $array_property) ? $array_property["prod_cat_tier3"] : null) . "#"
            . (array_key_exists("opr_cat_tier1", $array_property) ? $array_property["opr_cat_tier1"] : null) . "#"
            . (array_key_exists("opr_cat_tier2", $array_property) ? $array_property["opr_cat_tier2"] : null) . "#"
            . (array_key_exists("opr_cat_tier3", $array_property) ? $array_property["opr_cat_tier3"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_1"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_2"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_3"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_4"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_5"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_6"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_7"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_8"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_9"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_10"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_11"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_12"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_13"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_14"] : null) . "#"
            . (array_key_exists("resrv_field_1", $array_property) ? $array_property["resrv_field_15"] : null);
    }
}
