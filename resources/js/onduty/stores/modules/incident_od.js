import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            headers: [
                
                { text: 'Actions', value: 'actions', sortable: false },
                { text: 'Request Name', align: 'start', sortable: false,value: 'requestbyremedyuser'},
                { text: 'Incident Id', value: 'remedyTicketId' },
                { text: 'Severity', value: 'saverityOdCommandCenter' },
                { text: 'Service', value: 'serviceOdCommandCenter' },
                { text: 'Incident', value: 'text' },
                { text: 'Impact', value: 'impactOdCommandCenter' },
                { text: 'Suspect', value: 'suspectOdCommandCenter' },
                { text: 'Estimasi', value: 'estimasiOdCommandCenter' },
                { text: 'PIC', value: 'picSupportInfra' },
                { text: 'Type Incident', value: 'typeIncidentOdCommandCenter' },
            //    { text: 'Receive Date', value: 'receivingDateTime' },
            //  { text: 'Finised', value: 'finishDateTime' },
                { text: 'Ekskalasi Start', value: 'ekskalasiStartDate' },
                { text: 'Ekskalasi End', value: 'ekskalasiEndDate' },
           //     { text: 'Status Remedy', value: 'processStatus' },
            //    { text: 'Status Ekskalasi', value: 'ekskalasiStatus' },
            ],
            valueIncident: [
                {RequestName: 'PUTUT MUKTI WIBOWO'}
            ]
        }
    };
};

const state = {
    headerIncidentOd: [],
    valueIncidentOd: [],
    selectedIncidentOd:[]
};

const getters = {
   getHeaderIncidentOd: state => state.headerIncidentOd,
   getValueIncidentOd: state => state.valueIncidentOd,
   getSelectedIncidentOd: state => state.selectedIncidentOd
};

const actions = {
    async initSystemAvailability({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
          response = await axios.get("/problem_management/problems/system_availability/fetch");
          commit("setSystemAvails", response.data.data);        
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit("setException", "Something went wrong on fetch!");
        }
    },
    async initIncidentOd({ commit },payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {         
           response = await axios.get("/onduty/incident/listincidentod/"+payload['inboxId']+"/"+payload['odDashboardRole']);
           console.log(response.data.data.data);
            commit("setValueIncidentOd", response.data.data.data); 
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },async incidentSelectedOd({ commit },payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {         
           response = await axios.get("/onduty/incident/listincidentod/"+payload['inboxId']+"/"+payload['odDashboardRole']);
           console.log('incident seloy')
           console.log(response.data.data);
           commit("setSelectedIncidentOd", response.data.data); 
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },async searchIncidentByOd({ commit },payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {         
           console.log(payload);
           response = await axios.post("/onduty/incident/searchIncidentByOd",payload);
           console.log(response.data.data);
           commit("setValueIncidentOd", response.data.data); 
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },async sendMessageIncidentByOd({ commit },payload) {
        // commit("setApiState", ENUM.LOADING);
         let response;
         try {         
           console.log(payload);
           response = await axios.post("/onduty/incident/sendMessageIncidentByOd",payload);
          // console.log(response.data.data);
          // commit("setValueIncidentOd", response.data.data); 
         } catch (error) {
             error.response.status == 403
                 ? commit("setWarning", error.response.data.error)
                 : commit("setException", "Something went wrong on fetch!");
         }
    },
    headerIncident({ commit }){
        commit("setHeaderIncidentOd",defaultItem().item.headers);
    },
    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    }
};


const mutations = {
    setHeaderIncidentOd: (state, headerIncidentOd) => {
        // state.apiState = ENUM.LOADED;
         state.headerIncidentOd = headerIncidentOd;
    },
    setValueIncidentOd: (state, valueIncidentOd) => {
        // state.apiState = ENUM.LOADED;
         state.valueIncidentOd = valueIncidentOd;
    },
    setSelectedIncidentOd: (state, selectedIncidentOd) => {
        // state.apiState = ENUM.LOADED;
         state.selectedIncidentOd = selectedIncidentOd;
    },
    newSystemAvail: (state, systemavail) => {
        state.apiState = ENUM.LOADED;
        state.systemavails.unshift(systemavail);
    },
    deleteSystemAvail: (state, deletedSystemAvail) => {
        state.apiState = ENUM.LOADED;
        const index = state.systemavails.findIndex(systemavail => systemavail.id == deletedSystemAvail.id);
        state.systemavails.splice(index, 1);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
