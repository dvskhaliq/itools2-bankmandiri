@extends('sidebar')
@section('content')
<div id="los_fingerprint_status">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading">
                    <h4>Fingerprint Status</h4>
                </div>
                <!-- START DEFAULT DATATABLE -->
                <div class="panel-body">
                    <flash v-if="alert" info=""></flash>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <component-table></component-table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <component-upload></component-search>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@include('footer')
<script src="../../js/los_fingerprint_status.js"></script>
@endsection
