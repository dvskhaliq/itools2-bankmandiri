import ENUM from "../../enum";
const defaultItem = () => {
    return {
        item: {
            IncidentNumber: null,
            ProblemNumber: null,
            Description: null,
            IncidentRelation: null,
            ProblemRelation: null,
            ResponseStatus: null,
        },
        message: {
            message: "",
            type: ""
        }
    };
};

const state = {
    apiState: ENUM.INIT,
    editedItem: defaultItem().item,
    message: defaultItem().message,
};

const getters = {
    apiState: state => state.apiState,
    message: state => state.message,
    editItem: state => state.editedItem,
};

const actions = {
    initBulk({ commit }, payload) {
        let data = {
            IncidentNumber: payload.IncidentNumber,
            ProblemNumber: payload.ProblemNumber,
            Description: payload.Description,
            IncidentRelation: payload.IncidentRelation,
            ProblemRelation: payload.ProblemRelation,
            ResponseStatus: "in progress"
        };
        commit("setEditItem", data);
    },
    async doCreateRelation({ commit }, payload) {
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/relation/create",
                payload
            );
            console.log({ responseUpdate: response });
            if(response.data.status){
                commit("setSubmitted", {
                    responseStatus: true,
                    status: true,
                    response: response.data,
                    payload: payload
                });
            }else{
                commit("setSubmitted", {
                    responseStatus: false,
                    status: true,
                    response: response.data,
                    payload: payload
                });
            }
        } catch (error) {
            console.log({ errorUpdate: error });
            if (error.response.status == 403) {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.error,
                    payload: payload
                });
            } else {
                commit("setSubmitted", {
                    status: false,
                    response: error.response.data.message,
                    payload: payload
                });
            }
        }
    },
    async stopProcess({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        try {
            let response = await axios.post(
                "/data_management/problems/bulk/process/stop", payload);
                commit("setMessage", {
                    message: "The process has been successfully terminated !",
                    type: "success"
                })
        } catch (error) {
            error.response.status == 403
                ? commit("setMessage", {
                    message: error.response.data.error,
                    type: "error"
                })
                : commit("setMessage", {
                    message: "Something wrong !",
                    type: "error"
                });
        }
    },
};

const mutations = {
    setEditItem: (state, payload) => (state.editedItem = payload),
    setSubmitted: (state, data) => {
        let result = {}
        if (data.status) {
            if(data.responseStatus){
                result = {
                    IncidentNumber: data.response.data.IncidentNumber,
                    ProblemNumber: data.response.data.ProblemNumber,
                    Description: data.response.data.Description,
                    IncidentRelation: data.response.data.IncidentRelation,
                    ProblemRelation: data.response.data.ProblemRelation,
                    ResponseStatus: data.response.processStatus
                };
            }else{
                result = {
                    IncidentNumber: data.response.data.IncidentNumber,
                    ProblemNumber: data.response.data.ProblemNumber,
                    Description: data.response.data.Description,
                    IncidentRelation: data.response.data.IncidentRelation,
                    ProblemRelation: data.response.data.ProblemRelation,
                    ResponseStatus: data.response.processStatus
                };
            }
        } else {
            result = {
                IncidentNumber: data.payload.IncidentNumber,
                ProblemNumber: data.payload.ProblemNumber,
                Description: data.payload.Description,
                IncidentRelation: data.response.length>30 ? data.response.substring(0,50)+'...':data.response,
                ProblemRelation: data.response.length>30 ? data.response.substring(0,50)+'...':data.response,
                ResponseStatus: 'rejected'
            }
        }
        state.editedItem = result;
    },
    setApiState: (state, enumApiState) => {
        state.apiState = enumApiState;
    },
    setMessage: (state, message) => {
        switch (message.type) {
            case "success":
                state.apiState = ENUM.SUCCESS;
                break;
            case "error":
                state.apiState = ENUM.ERROR;
                break;
        }
        state.message.message = message.message
        state.message.type = message.type;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
