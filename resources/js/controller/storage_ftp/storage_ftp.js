require('../../bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

Vue.component('storage-table', ()=> import('../../components/ftp_connect/FTPStorageTable.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));

import VuePagination from '../../components/Pagination.vue';
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)
import 'es6-promise/auto'
import Vuex from 'vuex';
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);
Vue.use(Vuex);
const storage_ftp = new Vue({
    el: '#storage_ftp',
    data: {
        result: storage,
        file_list: {},
        isloaded: false,
        ismodal: false,
        alert: false,
        pages: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        search: '',
        ipserver: null,
        connect: false,
        category: category
    },

    mounted() {
    },

    components: {
        VuePagination,
    },

    methods: {
        refreshFileList(reqResponse){
            this.result = reqResponse.result
            this.ismodal = true
        },

        setFlashInfo(reqResponse) {
            flash({
                id: "ismodal-info",
                message: reqResponse,
                flashtype: "info"
            });
        },

        setFlashDanger(reqResponse) {
            flash({
                id: "ismodal-danger",
                message: reqResponse,
                flashtype: "danger"
            });
        },
    },

});
