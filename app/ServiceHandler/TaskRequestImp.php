<?php

namespace App\ServiceHandler;

use App\Inbox;
use App\ChatLiveMessages;
use App\ServiceCatalog;
use Illuminate\Support\Facades\DB;
use Auth;
use App\SupportGroupAssigneeRemedy;
use App\User;
use App\Http\Resources\Inbox as InboxResource;

class TaskRequestImp implements TaskRequestService
{

    //Class not used

    // public function bookRequest($request_id) //this function will be deprecated due moved to inbox service
    // {
    //     try {
    //         DB::beginTransaction();
    //         $inbox = Inbox::sharedLock()->find($request_id);
    //         if ($inbox->ProcessStatus == 'QUEUED') {
    //             $inbox->ProcessStatus = 'BOOKED';
    //             $inbox->save();
    //             DB::commit();
    //             return $inbox;
    //         } else if ($inbox->ProcessStatus == 'BOOKED') {
    //             DB::commit();
    //             $inbox = Inbox::find($request_id);
    //             $inbox->ProcessStatus = 'INPROGRESS';
    //             return $inbox;
    //         } else {
    //             DB::commit();
    //             $inbox = Inbox::find($request_id);
    //             return $inbox;
    //         }
    //     } catch (\PDOException $th) {
    //         DB::commit();
    //         $inbox = Inbox::find($request_id);
    //         $inbox->ProcessStatus = 'INPROGRESS';
    //         return $inbox;
    //     }
    // }

    // public function update($request_id, $array_inbox) //this function will be deprecated due moved to inbox service
    // {
    //     $inbox = Inbox::find($request_id);
    //     $inbox->ReceivingDateTime = (array_key_exists("ReceivingDateTime", $array_inbox) ? $array_inbox["ReceivingDateTime"] : $inbox->ReceivingDateTime);
    //     $inbox->Text = (array_key_exists("Text", $array_inbox) ? $array_inbox["Text"] : $inbox->Text);
    //     $inbox->SenderNumber = (array_key_exists("SenderNumber", $array_inbox) ? $array_inbox["SenderNumber"] : $inbox->SenderNumber);
    //     $inbox->Coding = (array_key_exists("Coding", $array_inbox) ? $array_inbox["Coding"] : $inbox->Coding);
    //     $inbox->UDH = (array_key_exists("UDH", $array_inbox) ? $array_inbox["UDH"] : $inbox->UDH);
    //     $inbox->SMSCNumber = (array_key_exists("SMSCNumber", $array_inbox) ? $array_inbox["SMSCNumber"] : $inbox->SMSCNumber);
    //     $inbox->Class = (array_key_exists("Class", $array_inbox) ? $array_inbox["Class"] : $inbox->Class);
    //     $inbox->TextDecoded = (array_key_exists("TextDecoded", $array_inbox) ? $array_inbox["TextDecoded"] : $inbox->TextDecoded);
    //     $inbox->RecipientID = (array_key_exists("RecipientID", $array_inbox) ? $array_inbox["RecipientID"] : $inbox->RecipientID);
    //     $inbox->Processed = (array_key_exists("Processed", $array_inbox) ? $array_inbox["Processed"] : $inbox->Processed);
    //     $inbox->Status = (array_key_exists("Status", $array_inbox) ? $array_inbox["Status"] : $inbox->Status);
    //     $inbox->BookDateTime = (array_key_exists("BookDateTime", $array_inbox) ? $array_inbox["BookDateTime"] : $inbox->BookDateTime);
    //     $inbox->AssigneeId = (array_key_exists("AssigneeId", $array_inbox) ? $array_inbox["AssigneeId"] : $inbox->AssigneeId);
    //     $inbox->RemedyTicketId = (array_key_exists("RemedyTicketId", $array_inbox) ? $array_inbox["RemedyTicketId"] : $inbox->RemedyTicketId);
    //     $inbox->ProcessStatus = (array_key_exists("ProcessStatus", $array_inbox) ? $array_inbox["ProcessStatus"] : $inbox->ProcessStatus);
    //     $inbox->FinishDateTime = (array_key_exists("FinishDateTime", $array_inbox) ? $array_inbox["FinishDateTime"] : $inbox->FinishDateTime);
    //     $inbox->ActionTicketCreation = (array_key_exists("ActionTicketCreation", $array_inbox) ? $array_inbox["ActionTicketCreation"] : $inbox->ActionTicketCreation);
    //     $inbox->TicketCreationDateTime = (array_key_exists("TicketCreationDateTime", $array_inbox) ? $array_inbox["TicketCreationDateTime"] : $inbox->TicketCreationDateTime);
    //     $inbox->TelegramSenderId = (array_key_exists("TelegramSenderId", $array_inbox) ? $array_inbox["TelegramSenderId"] : $inbox->TelegramSenderId);
    //     $inbox->ServiceCatalogId = (array_key_exists("ServiceCatalogId", $array_inbox) ? $array_inbox["ServiceCatalogId"] : $inbox->ServiceCatalogId);
    //     $inbox->EmployeeSenderId = (array_key_exists("EmployeeSenderId", $array_inbox) ? $array_inbox["EmployeeSenderId"] : $inbox->EmployeeSenderId);
    //     $inbox->EmployeeAssigneeId = (array_key_exists("EmployeeAssigneeId", $array_inbox) ? $array_inbox["EmployeeAssigneeId"] : $inbox->EmployeeAssigneeId);
    //     $inbox->EmployeeAssigneeGroupId = (array_key_exists("EmployeeAssigneeGroupId", $array_inbox) ? $array_inbox["EmployeeAssigneeGroupId"] : $inbox->EmployeeAssigneeGroupId);
    //     $inbox->save();
    //     $inbox->load('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype', 'employeeremedy');
    //     return $inbox;
    // }

    // //////////////////////////////////////// Below Code is Before Assesment //////////////////////////////////////

    // public function add($array_inbox)
    // {
    //     $inbox = new Inbox();
    //     $inbox->ReceivingDateTime = (array_key_exists("ReceivingDateTime", $array_inbox) ? $array_inbox["ReceivingDateTime"] : date('Y-m-d H:i:s'));
    //     $inbox->Text = (array_key_exists("Text", $array_inbox) ? $array_inbox["Text"] : null);
    //     $inbox->SenderNumber = (array_key_exists("SenderNumber", $array_inbox) ? $array_inbox["SenderNumber"] : "TBD");
    //     $inbox->Coding = (array_key_exists("Coding", $array_inbox) ? $array_inbox["Coding"] : "Default_No_Compression");
    //     $inbox->UDH = (array_key_exists("UDH", $array_inbox) ? $array_inbox["UDH"] : "TBD");
    //     $inbox->SMSCNumber = (array_key_exists("SMSCNumber", $array_inbox) ? $array_inbox["SMSCNumber"] : "TBD");
    //     $inbox->Class = (array_key_exists("Class", $array_inbox) ? $array_inbox["Class"] : -1);
    //     $inbox->TextDecoded = (array_key_exists("TextDecoded", $array_inbox) ? $array_inbox["TextDecoded"] : "TBD");
    //     $inbox->RecipientID = (array_key_exists("RecipientID", $array_inbox) ? $array_inbox["RecipientID"] : "TBD");
    //     $inbox->Processed = (array_key_exists("Processed", $array_inbox) ? $array_inbox["Processed"] : false);
    //     $inbox->Status = (array_key_exists("Status", $array_inbox) ? $array_inbox["Status"] : 111);
    //     $inbox->BookDateTime = (array_key_exists("BookDateTime", $array_inbox) ? $array_inbox["BookDateTime"] : null);
    //     $inbox->AssigneeId = (array_key_exists("AssigneeId", $array_inbox) ? $array_inbox["AssigneeId"] : null);
    //     $inbox->RemedyTicketId = (array_key_exists("RemedyTicketId", $array_inbox) ? $array_inbox["RemedyTicketId"] : null);
    //     $inbox->ProcessStatus = (array_key_exists("ProcessStatus", $array_inbox) ? $array_inbox["ProcessStatus"] : null);
    //     $inbox->FinishDateTime = (array_key_exists("FinishDateTime", $array_inbox) ? $array_inbox["FinishDateTime"] : null);
    //     $inbox->ActionTicketCreation = (array_key_exists("ActionTicketCreation", $array_inbox) ? $array_inbox["ActionTicketCreation"] : null);
    //     $inbox->TicketCreationDateTime = (array_key_exists("TicketCreationDateTime", $array_inbox) ? $array_inbox["TicketCreationDateTime"] : null);
    //     $inbox->TelegramSenderId = (array_key_exists("TelegramSenderId", $array_inbox) ? $array_inbox["TelegramSenderId"] : null);
    //     $inbox->ServiceCatalogId = (array_key_exists("ServiceCatalogId", $array_inbox) ? $array_inbox["ServiceCatalogId"] : null);
    //     $inbox->EmployeeSenderId = (array_key_exists("EmployeeSenderId", $array_inbox) ? $array_inbox["EmployeeSenderId"] : null);
    //     $inbox->EmployeeAssigneeId = (array_key_exists("EmployeeAssigneeId", $array_inbox) ? $array_inbox["EmployeeAssigneeId"] : null);
    //     $inbox->EmployeeAssigneeGroupId = (array_key_exists("EmployeeAssigneeGroupId", $array_inbox) ? $array_inbox["EmployeeAssigneeGroupId"] : null);
    //     $inbox->save();
    //     $inbox->load('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype', 'employeeremedy');
    //     return $inbox;
    // }

    // public function updateClause($array_clause, $array_inbox)
    // {
    //     $inbox = Inbox::where([
    //         ["ReceivingDateTime", array_key_exists("ReceivingDateTime", $array_clause)],
    //         ["Text", array_key_exists("Text", $array_clause)],
    //         ["SenderNumber", array_key_exists("SenderNumber", $array_clause)],
    //         ["Coding", array_key_exists("Coding", $array_clause)],
    //         ["UDH", array_key_exists("UDH", $array_clause)],
    //         ["SMSCNumber", array_key_exists("SMSCNumber", $array_clause)],
    //         ["Class", array_key_exists("Class", $array_clause)],
    //         ["TextDecoded", array_key_exists("TextDecoded", $array_clause)],
    //     ]);
    // }

    // public function findRemedyTicketId($RemedyTicketId)
    // {
    //     $data = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')->where('RemedyTicketId', $RemedyTicketId)->first();
    //     return $data;
    // }

    // public function findRemedyTicketDuplicated($remedy_ticket_id, $request_id)
    // {
    //     $data = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
    //         ->where('RemedyTicketId', $remedy_ticket_id)
    //         ->where('id', '!=', $request_id)
    //         ->first();
    //     return $data;
    // }

    // public function isRemedyTicketExist($remedy_ticket_id)
    // {
    //     return Inbox::where('RemedyTicketId', $remedy_ticket_id)->exists();
    // }

    // public function isIdleRequestExist()
    // {
    //     return Inbox::where('ActionTicketCreation', 'IN_IDLE')->exists();
    // }

    // public function mergeRequest($origin_request, $duplicated_request)
    // {
    //     $request = Inbox::find($origin_request->id);
    //     $request->UDH = $duplicated_request->UDH;
    //     $request->Processed = $duplicated_request->Processed;
    //     // $request->AssigneeId = $duplicated_request->AssigneeId;
    //     $request->ProcessStatus = $duplicated_request->ProcessStatus;
    //     $request->EmployeeSenderId = $duplicated_request->EmployeeSenderId;
    //     $request->RemedyTicketId = $duplicated_request->RemedyTicketId;
    //     $request->EmployeeAssigneeId = $duplicated_request->EmployeeAssigneeId;
    //     $request->EmployeeAssigneeGroupId = $duplicated_request->EmployeeAssigneeGroupId;
    //     $request->TextDecoded = $duplicated_request->TextDecoded;

    //     if (substr($request->RemedyTicketId, 0, 2) == 'IN') {
    //         $request->ActionTicketCreation = 'IN_MERGED';
    //     } else {
    //         $request->ActionTicketCreation = 'WO_MERGED';
    //     }

    //     if ($duplicated_request->ProcessStatus != 'CLOSED') {
    //         $request->BookDateTime = $duplicated_request->BookDateTime;
    //     }

    //     if ($duplicated_request->ProcessStatus == 'CLOSED') {
    //         $request->FinishDateTime = $duplicated_request->FinishDateTime;
    //     }

    //     //Kondisi ubah Service Catalog Id livechat ***&&&
    //     if ($origin_request->RecipientID == 'LIVECHAT' && $origin_request->Text == 'Livechat Cabang' && $origin_request->ServiceCatalogId == '2') {
    //         $request->ServiceCatalogId = '97';
    //     } else if ($origin_request->RecipientID == 'LIVECHAT' && $origin_request->Text == 'Livechat IT' && $origin_request->ServiceCatalogId == '3') {
    //         $request->ServiceCatalogId = '98';
    //     } else if ($origin_request->RecipientID == 'LIVECHAT' && $origin_request->Text == 'Livechat Kartu Kredit' && $origin_request->ServiceCatalogId == '4') {
    //         $request->ServiceCatalogId = '99';
    //     }

    //     $request->save();
    //     $duplicated_request->delete();
    //     $request->load('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype', 'employeeremedy');

    //     // $this->performFeedBackNotifier($request->RecipientID, $request->ProcessStatus, $request->id);
    //     return $request;
    // }

    // public function addInbox($customer, $content, $assignee, $remedy_ticket_id, $action_ticket_creation, $service_type)
    // {
    //     $sgar = SupportGroupAssigneeRemedy::where('company', $assignee['assignee_company'])
    //         ->where('organization', $assignee['assignee_organization'])
    //         ->where('support_group_name', $assignee['assignee_group'])
    //         ->where('full_name', $assignee['assignee_fullname'])
    //         ->first();

    //     $actual_assignee = null;
    //     if ($sgar == null) {
    //         $actual_assignee = null;
    //     } else {
    //         $actual_assignee = User::where('un_remedy', $sgar->remedy_login_id)->first();
    //     }

    //     // $urgency = 'Low';
    //     // if ($content['urgency'] == '4-Low') {
    //     //     $urgency = 'Low';
    //     // } else if ($content['urgency'] == '3-Medium') {
    //     //     $urgency = 'High';
    //     // } else if ($content['urgency'] == '2-High') {
    //     //     $urgency = 'Critical';
    //     // } else if ($content['urgency'] == '1-Critical') {
    //     //     $urgency = 'Critical';
    //     // }

    //     $service_catalog_id = 88;
    //     $channel = '';
    //     if ($content['reportedSource'] == 'Phone') {
    //         $channel = 'CALL';
    //         if ($service_type == 'LAYANAN_IT') {
    //             $service_catalog_id = 35;
    //         } else if ($service_type == 'LAYANAN_CABANG') {
    //             $service_catalog_id = 46;
    //         } else if ($service_type == 'LAYANAN_CC') {
    //             $service_catalog_id = 47;
    //         }
    //     } else if ($content['reportedSource'] == 'Email') {
    //         $channel = 'EMAIL';
    //         if ($service_type == 'LAYANAN_IT') {
    //             $service_catalog_id = 36;
    //         } else if ($service_type == 'LAYANAN_CABANG') {
    //             $service_catalog_id = 48;
    //         } else if ($service_type == 'LAYANAN_CC') {
    //             $service_catalog_id = 49;
    //         } else if ($service_type == 'LAYANAN_SDS') {
    //             $sc = $this->findServiceCatalogByCategorization($content['productCategoryTier1'], $content['productCategoryTier2'], $content['productCategoryTier3'], $content['operationalCategoryTier1'], $content['operationalCategoryTier2'], $content['operationalCategoryTier3']);
    //             if ($sc !== null) {
    //                 $service_catalog_id = $sc->id;
    //             }
    //         }
    //     } else if ($content['reportedSource'] == 'Whatsapp') {
    //         $channel = 'WA';
    //         if ($service_type == 'LAYANAN_IT') {
    //             $service_catalog_id = 37;
    //         }
    //     }

    //     $process_status = "QUEUED";
    //     if ($content['status'] == 'Assigned' && $actual_assignee == null) {
    //         $process_status = "QUEUED";
    //     } else if ($content['status'] == 'Assigned' && $actual_assignee != null) {
    //         $process_status = "INPROGRESS";
    //     } else if ($content['status'] == 'In Progress') {
    //         $process_status = "INPROGRESS";
    //     } else if ($content['status'] == 'Pending') {
    //         $process_status = 'PENDING';
    //     } else {
    //         $process_status = "CLOSED";
    //     }

    //     $text_decoded = "INCIDENT#" . $customer['company'] . "#User Service Restoration#" . $content['notes'] . "#" . $content['summary'] . "#" . $content['assignedSupportGroup'] . "#" . $content['assignee'] . "#" . $content['impact'] . "#" . $content['urgency'] . "#Priority#" . $content['reportedSource'] . "#" . $customer['company'] . "#" . $content['productCategoryTier1'] . "#" . $content['productCategoryTier2'] . "#" . $content['productCategoryTier3'] . "###" . $content['operationalCategoryTier1'] . "#" . $content['operationalCategoryTier2'] . "#" . $content['operationalCategoryTier3'] . "#" . $content['resolution'];
    //     $clm = new Inbox();
    //     $clm->ReceivingDateTime = date('Y-m-d H:i:s');
    //     $clm->Text = $content['summary'];
    //     $clm->SenderNumber = $customer['full_name'];
    //     $clm->UDH = $customer['full_name'] . "#" . $customer['first_name'] . "#" . $customer['last_name'] . "#" . $customer['email'] . "#" . $customer['company'] . "#" . $customer['region_name'] . "#" . $customer['group_name'] . "#" . $customer['department_name'];
    //     $clm->SMSCNumber = $customer['id'];
    //     $clm->TextDecoded = $text_decoded;
    //     $clm->RecipientID = $channel;
    //     $clm->BookDateTime = date('Y-m-d H:i:s');
    //     $clm->AssigneeId = Auth::user()->id;
    //     $clm->RemedyTicketId = $remedy_ticket_id;
    //     $clm->ProcessStatus = $process_status;
    //     if ($process_status == 'CLOSED') {
    //         $clm->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     $clm->ActionTicketCreation = $action_ticket_creation;
    //     if ($process_status != 'CLOSED') {
    //         $clm->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     $clm->TelegramSenderId = null;
    //     $clm->ServiceCatalogId = $service_catalog_id;
    //     $clm->EmployeeSenderId = $customer['id'];
    //     if ($actual_assignee != null) {
    //         $clm->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $clm->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }
    //     $clm->save();

    //     return $clm;
    // }

    // public function updateInboxByRemedyTicketId($request, $content, $service_type)
    // {
    //     $sgar = SupportGroupAssigneeRemedy::where('support_group_id', $request->EmployeeAssigneeGroupId)
    //         ->where('employee_remedy_id', $request->EmployeeAssigneeId)
    //         ->first();

    //     $actual_assignee = null;
    //     if ($sgar && User::where('un_remedy', $sgar->remedy_login_id)->exists()) {
    //         $actual_assignee = User::where('un_remedy', $sgar->remedy_login_id)->first();
    //     } else {
    //         $actual_assignee = null;
    //     }

    //     $service_catalog_id = 88;
    //     $channel = '';
    //     if ($content['reportedSource'] == 'Phone') {
    //         $channel = 'CALL';
    //         if ($service_type == 'LAYANAN_IT') {
    //             $service_catalog_id = 35;
    //         } else if ($service_type == 'LAYANAN_CABANG') {
    //             $service_catalog_id = 46;
    //         } else if ($service_type == 'LAYANAN_CC') {
    //             $service_catalog_id = 47;
    //         }
    //     } else if ($content['reportedSource'] == 'Email') {
    //         $channel = 'EMAIL';
    //         if ($service_type == 'LAYANAN_IT') {
    //             $service_catalog_id = 36;
    //         } else if ($service_type == 'LAYANAN_CABANG') {
    //             $service_catalog_id = 48;
    //         } else if ($service_type == 'LAYANAN_CC') {
    //             $service_catalog_id = 49;
    //         } else if ($service_type == 'LAYANAN_SDS') {
    //             $sc = $this->findServiceCatalogByCategorization($content['productCategoryTier1'], $content['productCategoryTier2'], $content['productCategoryTier3'], $content['operationalCategoryTier1'], $content['operationalCategoryTier2'], $content['operationalCategoryTier3']);
    //             if ($sc !== null) {
    //                 $service_catalog_id = $sc->id;
    //             }
    //         }
    //     } else if ($content['reportedSource'] == 'Whatsapp') {
    //         $channel = 'WA';
    //         if ($service_type == 'LAYANAN_IT') {
    //             $service_catalog_id = 37;
    //         }
    //     }

    //     $process_status = "QUEUED";
    //     $action_ticket_creation = "IN_RECORDED";
    //     if ($content['status'] == 'Assigned' && $actual_assignee == null) {
    //         $process_status = "QUEUED";
    //         $action_ticket_creation = "IN_RECORDED";
    //         $service_catalog_id;
    //     } else if ($content['status'] == 'Assigned' && $actual_assignee != null) {
    //         $process_status = "INPROGRESS";
    //         $action_ticket_creation = "IN_PROCESS";
    //     } else if ($content['status'] == 'In Progress' || $content['status'] == 'Pending') {
    //         $process_status = "INPROGRESS";
    //         $action_ticket_creation = "IN_PROCESS";
    //     } else {
    //         $process_status = "CLOSED";
    //         $action_ticket_creation = "IN_COMPLETED";
    //     }

    //     $inbox = Inbox::find($request->id);
    //     $inbox->Text = $content['summary'];
    //     $inbox->RecipientID = $channel;
    //     if ($process_status == 'QUEUED') {
    //         $inbox->BookDateTime = date('Y-m-d H:i:s');
    //     }
    //     $inbox->AssigneeId = Auth::user()->id;
    //     $inbox->ProcessStatus = $process_status;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     $inbox->ActionTicketCreation = $action_ticket_creation;
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     $inbox->TelegramSenderId = null;
    //     $inbox->ServiceCatalogId = $service_catalog_id;
    //     if ($actual_assignee != null) {
    //         $inbox->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $inbox->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }
    //     $inbox->save();
    //     return $inbox;
    // }

    // public function findServiceCatalogByCategorization($prod1, $prod2, $prod3, $opr1, $opr2, $opr3)
    // {
    //     $service = ServiceCatalog::where([
    //         [DB::raw('TRIM(mapper_prod_tier_1)'), $prod1],
    //         [DB::raw('TRIM(mapper_prod_tier_2)'), $prod2],
    //         [DB::raw('TRIM(mapper_prod_tier_3)'), $prod3],
    //         [DB::raw('TRIM(mapper_opr_tier_1)'), $opr1],
    //         [DB::raw('TRIM(mapper_opr_tier_2)'), $opr2],
    //         [DB::raw('TRIM(mapper_opr_tier_3)'), $opr3],
    //     ])->first();
    //     return $service;
    // }

    // public function getTaskListByAssingeeAndStatusJson($employee_assignee_id, $process_status = "")
    // {
    //     $array_processStatus = ['INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'];

    //     if ($process_status == 'ALL') {
    //         $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
    //             ->where('EmployeeAssigneeId', $employee_assignee_id)
    //             ->whereIn('ProcessStatus', $array_processStatus)
    //             ->orderBy('ReceivingDateTime', 'desc')
    //             ->orderBy('id', 'asc')
    //             ->get());
    //     } else {
    //         $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
    //             ->where('EmployeeAssigneeId', $employee_assignee_id)
    //             ->where('ProcessStatus', $process_status)
    //             ->orderBy('ReceivingDateTime', 'desc')
    //             ->orderBy('id', 'asc')
    //             ->get());
    //     }
    //     return $data;
    // }

    // public function getRequestListByStatusAndCategoryJson($process_status, $service_type, $service_channel_name = "")
    // {
    //     if ($service_channel_name) {
    //         $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
    //             ->where('ProcessStatus', $process_status)
    //             ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
    //                 $q->where('encoding_name', $service_type);
    //             })
    //             ->where('RecipientID', $service_channel_name)
    //             ->orderBy('ReceivingDateTime', 'desc')
    //             ->orderBy('id', 'desc')
    //             ->get());
    //     } else {
    //         $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
    //             ->where('ProcessStatus', $process_status)
    //             ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
    //                 $q->where('encoding_name', $service_type);
    //             })
    //             ->orderBy('ReceivingDateTime', 'desc')
    //             ->orderBy('id', 'desc')
    //             ->get());
    //     }
    //     return $data;
    // }

    // public function updateStatusCompleteById($id, $status, $remedy_ticket_id, $process_status, $action_ticket_creation, $assignee)
    // {
    //     $sgar = SupportGroupAssigneeRemedy::where('company', $assignee['assignee_company'])
    //         ->where('organization', $assignee['assignee_organization'])
    //         ->where('support_group_name', $assignee['assignee_group'])
    //         ->where('full_name', $assignee['assignee_fullname'])
    //         ->first();

    //     $actual_assignee = null;
    //     if (User::where('un_remedy', $sgar->remedy_login_id)->exists()) {
    //         $actual_assignee = User::where('un_remedy', $sgar->remedy_login_id)->first();
    //     } else {
    //         $actual_assignee = null;
    //     }

    //     $inbox = Inbox::find($id);
    //     $inbox->Status = $status;
    //     $inbox->RemedyTicketId = $remedy_ticket_id;
    //     $inbox->ProcessStatus = $process_status;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     $inbox->ActionTicketCreation = $action_ticket_creation;
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($actual_assignee != null) {
    //         $inbox->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $inbox->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }
    //     $inbox->save();
    //     return $inbox;
    // }

    // public function updateStatusCompleteFailedById($id, $status, $remedy_ticket_id, $process_status, $action_ticket_creation, $content, $assignee)
    // {
    //     $sgar = SupportGroupAssigneeRemedy::where('company', $assignee['assignee_company'])
    //         ->where('organization', $assignee['assignee_organization'])
    //         ->where('support_group_name', $assignee['assignee_group'])
    //         ->where('full_name', $assignee['assignee_fullname'])
    //         ->first();

    //     $actual_assignee = null;
    //     if (User::where('un_remedy', $sgar->remedy_login_id)->exists()) {
    //         $actual_assignee = User::where('un_remedy', $sgar->remedy_login_id)->first();
    //     } else {
    //         $actual_assignee = null;
    //     }

    //     $text_decoded =     $content['summary'] . "#" . $content['assignedSupportOrganization'] . "#User Service Restoration#" . $content['notes'] . "#" . $content['summary'] . "#" . $content['assignedSupportGroup'] . "#" . $content['assignee'] . "#" . $content['impact'] . "#" . $content['urgency'] . "#Priority#" . $content['reportedSource'] . "#" . $content['assignedSupportCompany'] . "#" . $content['productCategoryTier1'] . "#" . $content['productCategoryTier2'] . "#" . $content['productCategoryTier3'] . "###" . $content['operationalCategoryTier1'] . "#" . $content['operationalCategoryTier2'] . "#" . $content['operationalCategoryTier3'] . "#" . $content['resolution'] . "#" . $content['status'];
    //     $inbox = Inbox::find($id);
    //     $inbox->Status = $status;
    //     $inbox->TextDecoded = $text_decoded;
    //     $inbox->RemedyTicketId = $remedy_ticket_id;
    //     $inbox->ProcessStatus = $process_status;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     $inbox->ActionTicketCreation = $action_ticket_creation;
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($actual_assignee != null) {
    //         $inbox->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $inbox->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }

    //     $inbox->save();

    //     return $inbox;
    // }

    // public function updateQueueStatusById($id, $process_status, $user_id, $assignee_id)
    // {
    //     $actual_assignee = User::find($assignee_id);
    //     $inbox = Inbox::find($id);

    //     $inbox->ProcessStatus = $process_status;
    //     $inbox->AssigneeId = $user_id;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->BookDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($actual_assignee != null) {
    //         $inbox->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $inbox->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }

    //     $inbox->save();

    //     return $inbox;
    // }


    // public function updateStatusById($id, $user_id, $assignee_id, $process_status, $integration_status)
    // {
    //     $actual_assignee = User::find($assignee_id);
    //     $inbox = Inbox::find($id);

    //     $inbox->ProcessStatus = $process_status;
    //     $inbox->AssigneeId = $user_id;
    //     $inbox->ActionTicketCreation = $integration_status;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->BookDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($actual_assignee != null) {
    //         $inbox->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $inbox->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }

    //     $inbox->save();

    //     return $inbox;
    // }

    // public function updateWoID($id, $assignee_id, $process_status, $integration_status, $work_order_id)
    // {
    //     $actual_assignee = User::find($assignee_id);
    //     $inbox = Inbox::find($id);

    //     $inbox->ProcessStatus = $process_status;
    //     $inbox->AssigneeId = $assignee_id;
    //     $inbox->RemedyTicketId = $work_order_id;
    //     $inbox->ActionTicketCreation = $integration_status;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->BookDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($actual_assignee != null) {
    //         $inbox->EmployeeAssigneeId = $actual_assignee->employee_remedy_id;
    //         $inbox->EmployeeAssigneeGroupId = $actual_assignee->sgn_id_remedy;
    //     }

    //     $inbox->save();

    //     return $inbox;
    // }

    // public function getQueueObjectById($id)
    // {
    //     return Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')->find($id);
    // }

    // public function getRequestListByActionTicket()
    // {
    //     $data = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
    //         ->where('AssigneeId', Auth::user()->id)
    //         ->whereIn('ActionTicketCreation', array('WO_PENDING_PRS', 'WO_PENDING_CLS', 'IN_PENDING_PRS', 'IN_PENDING_CLS', 'IN_PENDING_DLG', 'WO_PENDING_DLG'))
    //         ->orderBy('ReceivingDateTime', 'desc')
    //         ->orderBy('id', 'desc')
    //         ->get();

    //     return $data;
    // }

    // public function updateStatusIncidentById($id, $process_status, $action_ticket_creation, $text_decoded)
    // {
    //     $inbox = Inbox::find($id);

    //     $inbox->ProcessStatus = $process_status;
    //     $inbox->TextDecoded = $text_decoded;
    //     $inbox->ActionTicketCreation = $action_ticket_creation;
    //     if ($process_status == 'CLOSED') {
    //         $inbox->FinishDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->BookDateTime = date('Y-m-d H:i:s');
    //     }
    //     if ($process_status != 'CLOSED') {
    //         $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
    //     }

    //     $inbox->save();

    //     return $inbox;
    // }
}
