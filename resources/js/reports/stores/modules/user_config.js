import ENUM from "../enum";
const defaultItem = () => {
    return {
        index: -1,
        item: {
            id: null,
            name: "",
            email: "",
            password: "",
            module_problem: false,
            problem_role_id: "",
        }
    };
};

const state = {
    usersconcat: [],
    usersItools: [],
    users: [],
    user:{},
    simpleHeader: [
        { text: "Nama", value: "name" },
        { text: "Email", value: "email" },
        { text: "Support Group Name", value: "sgn_remedy" },
        { text: "Status", value: "module_problem" },
        { text: "Roles", value: "pmroles.name" },
        { text: "Actions", value: "action", sortable: false, align: "center" }
    ],
    simpleHeaderItools: [
        { text: "Nama", value: "name" },
        { text: "Email", value: "email" },
        { text: "Support Group Name", value: "sgn_remedy" },
        { text: "Employee Remedy ID", value: "employee_remedy_id" },
    ],
    editedIndex: defaultItem().index,
    editedStatus: true,
    editedItem: defaultItem().item,
    apiState: ENUM.INIT,
    alertMessage: "",
    permission: false,
    permissionSlot2: false,
};

const getters = {
    getUsers: state => state.users,
    getUsersItools: state => state.usersItools,
    getCurrentUser: state => state.user,
    getSimpleHeader: state => state.simpleHeader,
    getSimpleHeaderItools: state => state.simpleHeaderItools,
    getEditIndex: state => state.editedIndex,
    getEditItem: state => state.editedItem,
    getEditStatus: state => state.editedStatus,
    getApiState: state => state.apiState,
    getAlertMessage: state => state.alertMessage,
    getPermission: state => state.permission,
    getPermissionSlot2: state => state.permissionSlot2
};

const actions = {
    async fetchCurrentUser({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/user/current_user");
            commit("setCurrentUser", response.data.data);            
        } catch (error) {
            error.response.status == 403
                ? commit("setException", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Data User tidak dapat dimuat"
                  );
        }
    },
    async fetchListUsers({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/user/list_user");
            commit("setUsers", response.data.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setException", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Data User tidak dapat dimuat"
                  );
        }
    },
    async fetchListUsersItools({ commit }) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.get("/data_management/problems/user/list_user_itools");
            commit("setUsersItools", response.data.data);
        } catch (error) {
            error.response.status == 403
                ? commit("setException", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Data User tidak dapat dimuat"
                  );
        }
    },
    async updateUser({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            // response = await axios.post("/user_configuration/submit_update", payload);
            response = await axios.post("/data_management/problems/user/submit_update", payload);
            if(response.data.auth==true){
                commit("setCurrentUser", response.data.data);
            }else{
                commit("updateUser", response.data.data);
            }
            commit("setSuccess", response.data.message);
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Data User tidak dapat dimuat"
                  );
        }
    },
    async checkPermission({ commit }, payload) {
        let response;
        try {
            response = await axios.post("/data_management/problems/user/check_permission", payload);
            if(response.data.status==true){
                commit("setPermission", response.data.status);
            }else{
                commit("setWarning", response.data.error);
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Unauthorized"
                  );
        }
    },
    async checkPermissionSlot2({ commit }, payload) {
        let response;
        try {
            response = await axios.post("/data_management/problems/user/check_permission", payload);
            if(response.data.status==true){
                commit("setPermissionSlot2", response.data.status);
            }else{
                commit("setWarning", response.data.error);
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Unauthorized"
                  );
        }
    },
    async activatedUsers({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/user/activated_users", payload);
            if(response.data.status==true){
                commit("setActivatedUsers", response.data.data);
                commit("setSuccess", response.data.message);
            }else{
                commit("setWarning", "Ooopss. terjadi kesalahan saat mengaktifkan user, hubungi admin !");
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Data users gagal diaktifkan !"
                  );
        }
    },
    async deactivatedUsers({ commit }, payload) {
        commit("setApiState", ENUM.LOADING);
        let response;
        try {
            response = await axios.post("/data_management/problems/user/deactivated_users", payload);
            if(response.data.status==true){
                commit("setDeactivatedUsers", response.data.data);
                commit("setSuccess", response.data.message);
            }else{
                commit("setWarning", "Ooopss. terjadi kesalahan saat menonaktifkan user, hubungi admin !");
            }
        } catch (error) {
            error.response.status == 403
                ? commit("setWarning", error.response.data.error)
                : commit(
                      "setException",
                      "Sistem Galat! Data users gagal dinonaktifkan !"
                  );
        }
    },
    updateItem({ commit }, payload) {
        commit("setEditItem", payload.item);
        commit("setEditIndex", payload.index);
    },
    resetItem({ commit }) {
        commit("setEditItem", defaultItem().item);
        commit("setEditIndex", defaultItem().index);
    },
    resetUsers({ commit }) {
        commit("setApiState", ENUM.INIT);
        commit("setUsers", []);
    },
    resetPermission({ commit }) {
        commit("setPermission", false);
        commit("setPermissionSlot2", false);
    }
};

const mutations = {
    updateUser: (state, updateUser) => {
        state.apiState = ENUM.LOADED;
        const index = state.users.findIndex(user => user.id == updateUser.id);
        state.users.splice(index, 1, updateUser);
    },
    setPermission: (state, value) => {
        state.permission = value;
    },
    setPermissionSlot2: (state, value) => {
        state.permissionSlot2 = value;
    },
    setUsers: (state, users) => {
        state.apiState = ENUM.LOADED;
        state.users = users;
    },
    setUsersItools: (state, users) => {
        state.apiState = ENUM.LOADED;
        state.usersItools = users;
    },
    setCurrentUser: (state, user) => {
        state.apiState = ENUM.LOADED;
        state.user = user;
    },
    setActivatedUsers: (state, newusers) => {
        state.apiState = ENUM.LOADED;
        state.users = [...newusers, ...state.users]
        var i;
        for (i = 0; i < newusers.length; i++) {
            const index = state.usersItools.findIndex(activeuser => activeuser.id == newusers[i].id);
            state.usersItools.splice(index, 1);
        }
    },
    setDeactivatedUsers: (state, deactiveusers) => {
        state.usersItools = [...deactiveusers, ...state.usersItools]
        var i;
        for (i = 0; i < deactiveusers.length; i++) {
            const index = state.users.findIndex(activeuser => activeuser.id == deactiveusers[i].id);
            state.users.splice(index, 1);
        }
    },
    setEditItem: (state, payload) => (state.editedItem = payload),
    setEditIndex: (state, index) => (state.editedIndex = index),
    setEditStatus: (state, status) => (state.editedStatus = status),
    setApiState: (state, enumState) => (state.apiState = enumState),
    setException: (state, message) => {
        state.apiState = ENUM.ERROR;
        state.alertMessage = message;
    },
    setSuccess: (state, message) => {
        state.apiState = ENUM.SUCCESS;
        state.alertMessage = message;
    },
    setInfo: (state, message) => {
        state.apiState = ENUM.INFO;
        state.alertMessage = message;
    },
    setWarning: (state, message) => {
        state.apiState = ENUM.WARNING;
        state.alertMessage = message;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
