<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use \Awobaz\Compoships\Compoships;

    protected $dateFormat = 'Y-m-d H:i:s';
    // protected $dateFormat = 'Y-m-d H:i:s';
    // 'Y-m-d H:i:s';

    protected $fillable = [
        'name', 'email', 'password', 'un_remedy', 'pass_remedy', 'status', 'user_type', 'fn_remedy', 'ln_remedy', 'sgn_remedy', 'sgn_id_remedy', 'employee_remedy_id'
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function whatsappdispacthermessages()
    {
        return $this->hasMany(WhatsappDispatcherMessages::class, 'sender_id', 'id');
    }

    public function chatliverequests()
    {
        return $this->hasMany(ChatLiveMessages::class, 'assigned_agent_id', 'id');
    }

    public function pmroles(){
        return $this->belongsTo(PMRoles::class,'problem_role_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

    public function employeeremedy()
    {
        return $this->belongsTo(EmployeeRemedy::class, 'employee_remedy_id', 'id');
    }

    public function supportgroupassigneeremedy()
    {
        return $this->belongsTo(SupportGroupAssigneeRemedy::class, ['un_remedy', 'sgn_id_remedy'], ['remedy_login_id', 'support_group_id']);
    }

    public function inbox()
    {
        return $this->hasMany(User::class, 'AssigneeId', 'id');
    }

    public function inboxworkloginfo()
    {
        return $this->hasMany(InboxWorkinfolog::class, 'submitter_id', 'id');
    }

    /**
     * Checks if User has access to $permissions.
     */
    public function hasAccess(array $permissions): bool
    {
        // check if the permission is available in any role
        foreach ($this->roles as $role) {
            if ($role->hasAccess($permissions)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the user belongs to role.
     */
    public function inRole(string $roleSlug)
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }
}
