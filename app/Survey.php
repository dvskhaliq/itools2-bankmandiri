<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $table = 'survey';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function inbox()
    {
        return $this->belongsTo(Inbox::class, 'request_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'submitter_id','id');
    }
}
