<?php

namespace App\ServiceHandler\ReportLayer;

use App\Inbox;

interface ReportService
{
    //Dashboard Reporting
    public function generateSummaryTotal();
    public function generateSummaryTotalByDate($pdate);
}
