<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Crypt;

class ConfigurationBDSServerController extends BaseController
{

    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {

        return View::make('administrator_console/bds_server_configuration/bds_server_configuration');
    }

    public function page()
    {
        return $this->bdsServerService->listAllByPagination();
    }

    public function searchData()
    {
        return $this->bdsServerService->findData(request()->get('search'));
    }

    public function dataSelected()
    {
        return $this->bdsServerService->dataSelected(request()->get('id'));
    }

    public function submitUpdated()
    {

        $findData = $this->bdsServerService->findByIpServer(request()->get('ipServer'));
        if ($findData != null) {
            if ($findData->ip_server == request()->get('ipServer') && $findData->kode_cabang == request()->get('kodeCabang') && $findData->nama_cabang == request()->get('namaCabang')) {
                return response()->json([
                    'status' => false,
                    'result' => "Data not change in IP Server " . $findData->ip_server . " ( " . $findData->kode_cabang . " - " . $findData->nama_cabang . ")",
                ]);
            } else {
                $dataUpdate = array(
                    'ip_server' => request()->get('ipServer'),
                    'kode_cabang' => request()->get('kodeCabang'),
                    'nama_cabang' => request()->get('namaCabang'),
                );
                $this->bdsServerService->submitUpdated(request()->get('id'), $dataUpdate);
                return response()->json([
                    'status' => true,
                    'result' => "Update " . request()->get('kodeCabang') . " - " . request()->get('namaCabang') . " success",
                ]);
            }
        } else {
            $dataUpdate = array(
                'ip_server' => request()->get('ipServer'),
                'kode_cabang' => request()->get('kodeCabang'),
                'nama_cabang' => request()->get('namaCabang'),
            );
            $this->bdsServerService->submitUpdated(request()->get('id'), $dataUpdate);
            return response()->json([
                'status' => true,
                'result' => "Update " . request()->get('kodeCabang') . " - " . request()->get('namaCabang') . " success",
            ]);
        }
    }

    public function submitDeleted()
    {
        $this->bdsServerService->submitDeleted(request()->get('id'));

        return response()->json([
            'status' => true,
            'result' => "Delete " . request()->get('kode_cabang') . " - " . request()->get('nama_cabang') . " success",
        ]);
    }

    public function submitInsert()
    {

        $findData = $this->bdsServerService->findByIpServer(request()->get('ipServer'));
        if ($findData != null) {
            return response()->json([
                'status' => false,
                'result' => "Insert failed, IP Server " . $findData->ip_server . " already exist in " . $findData->kode_cabang . " - " . $findData->nama_cabang . "",
            ]);
        } else {
            $dataInsert = array(
                'ip_server' => request()->get('ipServer'),
                'kode_cabang' => request()->get('kodeCabang'),
                'nama_cabang' => request()->get('namaCabang')
            );
            $this->bdsServerService->submitInsert($dataInsert);
            return response()->json([
                'status' => true,
                'result' => "Insert IP Server " . request()->get('ipServer') . " (" . request()->get('kodeCabang') . " - " . request()->get('namaCabang') . ") success",
            ]);
        }
    }
}