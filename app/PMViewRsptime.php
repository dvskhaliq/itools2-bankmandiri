<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMViewRsptime extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'VIEW_RSPTIME';
}
