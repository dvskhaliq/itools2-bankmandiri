<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RspAtm extends Model
{
    protected $connection = 'sqlsrv_atm';
    protected $table = 'rsp_atm';
}
