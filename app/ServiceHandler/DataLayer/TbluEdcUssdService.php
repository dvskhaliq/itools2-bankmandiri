<?php

namespace App\ServiceHandler\DataLayer;

use App\HourlyRep as EdcUssd;
use App\TbluEdcUssd as Edc;
use Illuminate\Database\Eloquent\Collection;
use phpDocumentor\Reflection\Types\Boolean;

interface TbluEdcUssdService
{    
    public function fetchEdcUssdData($day);    
    public function fetchBdsData();
    public function fetchEmasKlnData();
    public function fetchCpsData();
    public function fetchSwiftData();
}
