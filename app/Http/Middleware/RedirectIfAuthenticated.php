<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //     return redirect('/home');
        // }

        // if (Auth::guard('problem')->check()) {
        //     return redirect('/data_management');
        // }else {
        //     return redirect('/');
        // }

        // if (Auth::guard('problem')->check()) {
        //     return redirect('/data_management');
        // }else if (Auth::guard('web')->check()) {
        //     return redirect('/');
        // }

        // if ($guard == "problem" && Auth::guard($guard)->check()) {
        //     return redirect('/data_management');
        // }if ($guard == "web" && Auth::guard($guard)->check()) {
        //     return redirect('/');
        // }if (Auth::guard($guard)->check()) {
        //     return redirect('/login');
        // }

        if ($guard == "problem" && Auth::guard($guard)->check()) {
            return redirect('/data_management');
        }if ($guard == "web" && Auth::guard($guard)->check()) {
            return redirect('/');
        }if ($guard == "onduty" && Auth::guard($guard)->check()) {
            return redirect('/onduty/dashboard');
        }

        return $next($request);
    }
}
