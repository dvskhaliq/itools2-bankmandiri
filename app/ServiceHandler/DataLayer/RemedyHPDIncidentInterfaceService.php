<?php

namespace App\ServiceHandler\DataLayer;

use App\RemedyHPDIncidentInterface as Incident;
use Illuminate\Database\Eloquent\Collection;
use phpDocumentor\Reflection\Types\Boolean;

interface RemedyHPDIncidentInterfaceService
{ 
    public function tranformToHPDIncidentInterface($status, $param1, $param2);
    public function tranformToHPDIncidentInterfaceSource($status, $param1, $param2, $source);
    public function tranformToHPDIncidentInterfaceSurvey($status, $param1, $param2, $survey);
    public function tranformToHPDIncidentInterfaceSourceSurvey($status, $param1, $param2, $source, $survey);
}
