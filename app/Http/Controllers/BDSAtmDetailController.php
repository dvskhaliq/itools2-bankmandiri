<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\BaseController;
use App\ServiceHandler\BDSLayer\BDSServerService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BDSAtmDetailController extends BaseController
{
    protected $bdsServerService;
    /**
     * Create a new controller instance.
     */

    public function __construct(BDSServerService $databaseConnection)
    {
        $this->bdsServerService = $databaseConnection;
        $this->middleware('auth');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {

        return View::make('bds_connection/bds_atm/bds_atm_detail/bds_atm_detail');
    }

    public function getList()
    {
        $result = $this->bdsServerService->initBdsConnection()->table('ATMDetail')->select()->get();

        return response()->json($result);
    }

    public function findAtmDetailById($id){
        $data = $this->bdsServerService->initBdsConnection()->table('ATMDetail')->select()->where('ATMGLRefNo', $id)->get();
        return $data;
    }

    public function listSelected(){
        $data = $this->findAtmDetailById(request()->get('id'));
        return response()->json([
            'status' => true,
            'result' => $data[0],
        ]);
    }

    public function submitDeleted()
    {

        $this->bdsServerService->initBdsConnection()->table('ATMDetail')->where([['ATMGLRefNo',  request()->get('ATMGLRefNo')]])->delete();

        return response()->json([
            'status' => true,
            'result' => 'Delete ATMGLRefNo : ' . request()->get('ATMGLRefNo').' - '.request()->get('Description').' success',
        ]);
    }

    public function submitUpdated()
    {
        $findAtm = $this->findAtmDetailById(request()->get('ATMGLRefNo'))->toArray();
        if (!empty($findAtm)) {
            if($findAtm[0]['Description'] == request()->get('Description')){
                return response()->json([
                    'status' => false,
                    'result' => 'ATMGLRefNo : ' . request()->get('ATMGLRefNo') . ' - ' . request()->get('Description') . ' not change',
                ]);
            }else{
                $this->bdsServerService->initBdsConnection()->table('ATMDetail')->where('ATMGLRefNo', request()->get('ATMGLRefNo'))->update(['Description' =>request()->get('Description')]);

                return response()->json([
                    'status' => true,
                    'result' => 'Update ATMGLRefNo : ' . request()->get('ATMGLRefNo').' - '.request()->get('Description').' success',
                ]);
            }
        }
    }

    public function submitAdd()
    {
        $data = array(
            'ATMGLRefNo' => request()->get('ATMGLRefNo'),
            'Description' => request()->get('Description')
        );

        $findAtm = $this->findAtmDetailById(request()->get('ATMGLRefNo'))->toArray();
        if (empty($findAtm)) {
            $this->bdsServerService->initBdsConnection()->table('ATMDetail')->insert($data);

            return response()->json([
                'status' => true,
                'result' => 'Add ATMGLRefNo : ' . request()->get('ATMGLRefNo').' - '.request()->get('Description').' success',
            ]);
        }else{
            return response()->json([
                'status' => false,
                'result' => 'Add failed, ATMGLRefNo : ' . request()->get('ATMGLRefNo').' already exists',
            ]);
        }
    }
}
