<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InboxWorkInfoLog extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "requestId" => $this->request_id,
            "timestamp" => date('Y-m-d H:i:s',strtotime($this->submit_date)),
            "submitterId" => $this->submitter_id,
            "type" => $this->workinfo_type,
            "content" => $this->workinfo_content,
            "fileName" => $this->first_attachment_file_name,
            "fileExt" => $this->file_ext,
            "fileSize" => $this->file_size_in_bytes
        ];
    }
}
