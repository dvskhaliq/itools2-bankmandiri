@extends('sidebar')
@section('content')
    <div class="row" id="userprofile">
        <div class="col-md-12">
            <div class="panel panel-default" id="home">
                <div class="panel-heading" style="background: #fff;">
                    <h4>User Profile</h4>
                </div>
                <div class="panel-body">
                    <flash v-if="alert" info=""></flash>
                    <div class="col-md-12">                        
                        <user-profile :selected_prop="auth" v-on:submit-update="submitUpdate" :isloaded="isloaded" :modalname="modalname" :ismodal="ismodal"></user-profile>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('footer')
<script src="../../js/sidebar.js"></script>
@endsection