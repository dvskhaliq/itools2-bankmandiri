<?php

namespace App\Http\Controllers;

use App\ServiceHandler\PBMLayer\DashboardItService;
use Illuminate\Http\Request;

class DashboardItController extends BaseController
{
    protected $dashboard_it_repo;

    public function __construct(DashboardItService $dashboard_it_service)
    {
        $this->dashboard_it_repo = $dashboard_it_service;
        // $this->middleware('auth:problem');
    }

    public function fetchYears()
    {
        $data = $this->dashboard_it_repo->getYears();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchDashboardIt()
    {
        $data = $this->dashboard_it_repo->getDashboardIt();
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchDashboardItByYear(Request $request)
    {
        $selected_year = $request->input('selectedYear');
        $data = $this->dashboard_it_repo->getDashboardItByYear($selected_year);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchDashboardItByMonth(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $data = $this->dashboard_it_repo->getDashboardItByMonth($selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchDashboardItByMonthAvg(Request $request)
    {
        $selected_month = $request->input('selectedMonth');
        $avg_query = $this->dashboard_it_repo->getDashboardItByMonthAvg($selected_month);
        $data = $this->loopForeachGroup($avg_query, $selected_month);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function fetchDashboardItByDate(Request $request)
    {
        $selected_date = $request->input('selectedDate');
        $data = $this->dashboard_it_repo->getDashboardItByDate($selected_date);
        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);
    }

    public function loopForeachGroup($avg, $month){
        $hasil=array();
        foreach ($avg as $key => $value) {
            $prop['bulan'] = $value->bulan;
            $prop['layanan'] = $value->layanan;
            $prop['system_downtime'] = $value->system_downtime;
            $prop['percent_system_avail'] = ROUND($value->percent_system_avail,2) == 100.00 ? 100 : ROUND($value->percent_system_avail,2);
            $prop['target_avail'] = ROUND($value->target_avail,2);
            $prop['restime'] = ROUND($value->restime,2);
            $prop['threshold_restime'] = ROUND($value->threshold_restime,2);
            $prop['success_rate'] = ROUND($value->success_rate,2);
            $prop['target_success_rate'] = ROUND($value->target_success_rate,2);
            $query = $this->dashboard_it_repo->getDashboardItByMonthLayanan($prop['layanan'], $month);
            array_push($hasil, [
                'sequence' => $key+1,
                'bulan' => $prop['bulan'],
                'layanan' => $prop['layanan'],
                'system_downtime' => $prop['system_downtime'],
                'percent_system_avail' => $prop['percent_system_avail'],
                'target_avail' => $prop['target_avail'],
                'restime' => $prop['restime'],
                'threshold_restime' => $prop['threshold_restime'],
                'success_rate' => $prop['success_rate'],
                'target_success_rate' => $prop['target_success_rate'],
                'detail' => $query
                ]);
        }
        return $hasil;
    }
}
