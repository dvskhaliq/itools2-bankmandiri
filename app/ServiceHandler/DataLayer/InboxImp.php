<?php

namespace App\ServiceHandler\DataLayer;

use App\Http\Resources\InboxCollection;
use App\Http\Resources\Inbox as InboxResource;
use App\Inbox;
use App\Incident;
use App\Workorder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\ServiceHandler\DataLayer\UserService;
use App\ServiceHandler\DataLayer\SupportUserService;
use App\ServiceHandler\DataLayer\SupportGroupService;
use App\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Auth;

class InboxImp implements InboxService
{
    protected $user_ds;
    protected $supuser_ds;
    protected $supgroup_ds;
    protected $srvctlg_ds;
    public function __construct(UserService $user_service, SupportUserService $support_user_service, SupportGroupService $support_group_service, ServiceCatalogService $service_catalog_service)
    {
        $this->srvctlg_ds = $service_catalog_service;
        $this->supuser_ds = $support_user_service;
        $this->supgroup_ds = $support_group_service;
        $this->user_ds = $user_service;
    }

    public function getByStatusAndCategory(String $process_status, String $service_type, String $service_channel = null, String $date = null): ?AnonymousResourceCollection
    {
        if($service_channel === 'INCIDENT'){
            $type = 'IN';
        }else if($service_channel === 'WORK ORDER'){
            $type = 'WO';
        }else{
            $field = 'RecipientID';
            $operator = '=';
            $service_channel_name = $service_channel;
        }
        if($service_channel === 'INCIDENT' || $service_channel === 'WORK ORDER'){
            if ($date == null) {
                $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
                    ->where('ProcessStatus', $process_status)
                    ->whereHas('servicecatalog', function ($q) use ($type) {
                        $q->where('service_catalog_type', $type);
                    })
                    ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
                        $q->where('encoding_name', $service_type);
                    })
                    ->orderBy('ReceivingDateTime', 'desc')
                    ->orderBy('id', 'desc')
                    ->get());
            }else {
                $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
                    ->where('ProcessStatus', $process_status)
                    ->whereHas('servicecatalog', function ($q) use ($type) {
                        $q->where('service_catalog_type', $type);
                    })
                    ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
                        $q->where('encoding_name', $service_type);
                    })
                    ->whereDate('ReceivingDateTime', $date)
                    ->orderBy('ReceivingDateTime', 'desc')
                    ->orderBy('id', 'desc')
                    ->get());
            } 
        }else{
            if ($service_channel_name != null && $date == null) {
                $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
                    ->where('ProcessStatus', $process_status)
                    ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
                        $q->where('encoding_name', $service_type);
                    })
                    ->where($field, $operator, $service_channel_name)
                    ->orderBy('ReceivingDateTime', 'desc')
                    ->orderBy('id', 'desc')
                    ->get());
            } else if ($service_channel_name == null && $date != null) {
                $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
                    ->where('ProcessStatus', $process_status)
                    ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
                        $q->where('encoding_name', $service_type);
                    })
                    ->whereDate('ReceivingDateTime', $date)
                    ->orderBy('ReceivingDateTime', 'desc')
                    ->orderBy('id', 'desc')
                    ->get());
            } else if ($service_channel_name != null && $date != null) {
                $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
                    ->where('ProcessStatus', $process_status)
                    ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
                        $q->where('encoding_name', $service_type);
                    })
                    ->where($field, $operator, $service_channel_name)
                    ->whereDate('ReceivingDateTime', $date)
                    ->orderBy('ReceivingDateTime', 'desc')
                    ->orderBy('id', 'desc')
                    ->get());
            } else {
                $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
                    ->where('ProcessStatus', $process_status)
                    ->whereHas('servicecatalog.servicechannel.servicetype', function ($q) use ($service_type) {
                        $q->where('encoding_name', $service_type);
                    })
                    ->orderBy('ReceivingDateTime', 'desc')
                    ->orderBy('id', 'desc')
                    ->get());
            }
        }
        
        return $data;
    }

    public function getByAssigneeAndStatus(String $employee_assignee_id, String $process_status = null): ?AnonymousResourceCollection
    {
        $array_processStatus = ['INPROGRESS', 'PENDING', 'WAITINGAPR', 'PLANNING'];

        if ($process_status == 'ALL') {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where('EmployeeAssigneeId', $employee_assignee_id)
                ->whereIn('ProcessStatus', $array_processStatus)
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'asc')
                ->get());
        } else {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->where('EmployeeAssigneeId', $employee_assignee_id)
                ->where('ProcessStatus', $process_status)
                ->orderBy('ReceivingDateTime', 'desc')
                ->orderBy('id', 'asc')
                ->get());
        }
        return $data;
    }

    public function findPendingTicket(Int $user_id): ?AnonymousResourceCollection
    {
        return InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')
            ->where('AssigneeId', $user_id)
            ->whereIn('ActionTicketCreation', array('WO_PENDING_PRS', 'WO_PENDING_CLS', 'IN_PENDING_PRS', 'IN_PENDING_CLS', 'IN_PENDING_DLG', 'IN_PENDING_SRVY', 'WO_PENDING_DLG'))
            ->orderBy('ReceivingDateTime', 'desc')
            ->orderBy('id', 'desc')
            ->get());
    }

    public function book($id): Inbox
    {
        try {
            DB::beginTransaction();
            $inbox = Inbox::sharedLock()->find($id);
            if ($inbox->ProcessStatus == 'QUEUED') {
                $inbox->ProcessStatus = 'BOOKED';
                $inbox->save();
                DB::commit();
                return $inbox;
            } else if ($inbox->ProcessStatus == 'BOOKED') {
                DB::commit();
                $inbox = Inbox::find($id);
                $inbox->ProcessStatus = 'INPROGRESS';
                return $inbox;
            } else {
                DB::commit();
                $inbox = Inbox::find($id);
                return $inbox;
            }
        } catch (\PDOException $th) {
            DB::commit();
            $inbox = Inbox::find($id);
            $inbox->ProcessStatus = 'INPROGRESS';
            return $inbox;
        }
    }

    public function find($id): Inbox
    {
        return Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy')->find($id);
    }

    public function update(Int $request_id, array $array_inbox): ?Inbox
    {
        try {
            $inbox = Inbox::find($request_id);
            $inbox->ReceivingDateTime = (array_key_exists("ReceivingDateTime", $array_inbox) ? $array_inbox["ReceivingDateTime"] : $inbox->ReceivingDateTime);
            $inbox->Text = (array_key_exists("Text", $array_inbox) ? $array_inbox["Text"] : $inbox->Text);
            $inbox->SenderNumber = (array_key_exists("SenderNumber", $array_inbox) ? $array_inbox["SenderNumber"] : $inbox->SenderNumber);
            $inbox->Coding = (array_key_exists("Coding", $array_inbox) ? $array_inbox["Coding"] : $inbox->Coding);
            $inbox->UDH = (array_key_exists("UDH", $array_inbox) ? $array_inbox["UDH"] : $inbox->UDH);
            $inbox->SMSCNumber = (array_key_exists("SMSCNumber", $array_inbox) ? $array_inbox["SMSCNumber"] : $inbox->SMSCNumber);
            $inbox->Class = (array_key_exists("Class", $array_inbox) ? $array_inbox["Class"] : $inbox->Class);
            $inbox->TextDecoded = (array_key_exists("TextDecoded", $array_inbox) ? $array_inbox["TextDecoded"] : $inbox->TextDecoded);
            $inbox->RecipientID = (array_key_exists("RecipientID", $array_inbox) ? $array_inbox["RecipientID"] : $inbox->RecipientID);
            $inbox->Processed = (array_key_exists("Processed", $array_inbox) ? $array_inbox["Processed"] : $inbox->Processed);
            $inbox->Status = (array_key_exists("Status", $array_inbox) ? $array_inbox["Status"] : $inbox->Status);
            $inbox->BookDateTime = (array_key_exists("BookDateTime", $array_inbox) ? $array_inbox["BookDateTime"] : $inbox->BookDateTime);
            $inbox->AssigneeId = (array_key_exists("AssigneeId", $array_inbox) ? $array_inbox["AssigneeId"] : $inbox->AssigneeId);
            $inbox->RemedyTicketId = (array_key_exists("RemedyTicketId", $array_inbox) ? $array_inbox["RemedyTicketId"] : $inbox->RemedyTicketId);
            $inbox->ProcessStatus = (array_key_exists("ProcessStatus", $array_inbox) ? $array_inbox["ProcessStatus"] : $inbox->ProcessStatus);
            $inbox->FinishDateTime = (array_key_exists("FinishDateTime", $array_inbox) ? $array_inbox["FinishDateTime"] : $inbox->FinishDateTime);
            $inbox->ActionTicketCreation = (array_key_exists("ActionTicketCreation", $array_inbox) ? $array_inbox["ActionTicketCreation"] : $inbox->ActionTicketCreation);
            $inbox->TicketCreationDateTime = (array_key_exists("TicketCreationDateTime", $array_inbox) ? $array_inbox["TicketCreationDateTime"] : $inbox->TicketCreationDateTime);
            $inbox->TelegramSenderId = (array_key_exists("TelegramSenderId", $array_inbox) ? $array_inbox["TelegramSenderId"] : $inbox->TelegramSenderId);
            $inbox->ServiceCatalogId = (array_key_exists("ServiceCatalogId", $array_inbox) ? $array_inbox["ServiceCatalogId"] : $inbox->ServiceCatalogId);
            $inbox->EmployeeSenderId = (array_key_exists("EmployeeSenderId", $array_inbox) ? $array_inbox["EmployeeSenderId"] : $inbox->EmployeeSenderId);
            $inbox->EmployeeAssigneeId = (array_key_exists("EmployeeAssigneeId", $array_inbox) ? $array_inbox["EmployeeAssigneeId"] : $inbox->EmployeeAssigneeId);
            $inbox->EmployeeAssigneeGroupId = (array_key_exists("EmployeeAssigneeGroupId", $array_inbox) ? $array_inbox["EmployeeAssigneeGroupId"] : $inbox->EmployeeAssigneeGroupId);
            $inbox->save();
            return $this->refresh($inbox);
        } catch (\Throwable $th) {
            return null;
        }
    }

    public function updateIncOd(Int $request_id, Incident $incident): ?Inbox
    {
        try {      
            
            $inbox = Inbox::find($request_id);

            if($incident->assignee_name!=null){
        
                $asgn = $this->supuser_ds->findByCompanyOrgGroupName($incident->assignee_support_company, $incident->assignee_support_organization, $incident->assignee_support_group_name, $incident->assignee_name);
                if (!$asgn) {
                    $asgn = $this->supgroup_ds->findByComOrgGrp($incident->assignee_support_company, $incident->assignee_support_organization, $incident->assignee_support_group_name);
                } else {
                    $inbox->EmployeeAssigneeId = $asgn->employee_remedy_id;
                    $inbox->EmployeeAssigneeGroupId = $asgn->support_group_id;
                }
            }
            
            $inbox->IncidentOdDecode = $incident->incident_od_decode;
            $inbox->ProcessStatusEkskalasiIncident = $incident->ekskalasiStatus;
            $inbox->Text = ($incident->reported_source == 'Officer on Duty') ? $incident->incident : $incident->summary_description;
            $inbox->TextDecoded = $incident->text_decoded;
            
            //$inbox->RemedyTicketId = $incident->remedyTicketId;
            
            $inbox->ProcessStatus = $incident->remedyProcessStatus;

            if($incident->ekskalasiStatus == 'OPEN' || $incident->ekskalasiStatus == 'NEW'){
                $inbox->SenderNumber = $incident->requester_full_name;
                $inbox->UDH = $incident->udh;
                $inbox->AssigneeId = Auth::user()->id;
            }
           
            
            if($incident->ekskalasiStatus == 'INPROGRESS'){
                $inbox->IncidentStartEkslasiDate =   date('Y-m-d H:i',strtotime($incident->waktuTerindikasiSelected)); //date('Y-m-d H:i:s');
            }
            if($incident->ekskalasiStatus == 'CLOSED'){
                $inbox->IncidentEndEkslasiDate = date('Y-m-d H:i',strtotime($incident->endEkskalasi)); //date('Y-m-d H:i:s');
            }
            $inbox->ProcessStatusEkskalasiIncident = $incident->ekskalasiStatus; 
            
            $inbox->save();
            
            return $this->refresh($inbox);
        } catch (\Throwable $th) {
            return null;
        }
    }

    public function add(array $array_inbox): ?Inbox
    {
        $inbox = new Inbox();
        $inbox->ReceivingDateTime = (array_key_exists("ReceivingDateTime", $array_inbox) ? $array_inbox["ReceivingDateTime"] : date('Y-m-d H:i:s'));
        $inbox->Text = (array_key_exists("Text", $array_inbox) ? $array_inbox["Text"] : null);
        $inbox->SenderNumber = (array_key_exists("SenderNumber", $array_inbox) ? $array_inbox["SenderNumber"] : "TBD");
        $inbox->Coding = (array_key_exists("Coding", $array_inbox) ? $array_inbox["Coding"] : "Default_No_Compression");
        $inbox->UDH = (array_key_exists("UDH", $array_inbox) ? $array_inbox["UDH"] : "TBD");
        $inbox->SMSCNumber = (array_key_exists("SMSCNumber", $array_inbox) ? $array_inbox["SMSCNumber"] : "TBD");
        $inbox->Class = (array_key_exists("Class", $array_inbox) ? $array_inbox["Class"] : -1);
        $inbox->TextDecoded = (array_key_exists("TextDecoded", $array_inbox) ? $array_inbox["TextDecoded"] : "TBD");
        $inbox->RecipientID = (array_key_exists("RecipientID", $array_inbox) ? $array_inbox["RecipientID"] : "TBD");
        $inbox->Processed = (array_key_exists("Processed", $array_inbox) ? $array_inbox["Processed"] : false);
        $inbox->Status = (array_key_exists("Status", $array_inbox) ? $array_inbox["Status"] : 111);
        $inbox->BookDateTime = (array_key_exists("BookDateTime", $array_inbox) ? $array_inbox["BookDateTime"] : null);
        $inbox->AssigneeId = (array_key_exists("AssigneeId", $array_inbox) ? $array_inbox["AssigneeId"] : null);
        $inbox->RemedyTicketId = (array_key_exists("RemedyTicketId", $array_inbox) ? $array_inbox["RemedyTicketId"] : null);
        $inbox->ProcessStatus = (array_key_exists("ProcessStatus", $array_inbox) ? $array_inbox["ProcessStatus"] : null);
        $inbox->FinishDateTime = (array_key_exists("FinishDateTime", $array_inbox) ? $array_inbox["FinishDateTime"] : null);
        $inbox->ActionTicketCreation = (array_key_exists("ActionTicketCreation", $array_inbox) ? $array_inbox["ActionTicketCreation"] : null);
        $inbox->TicketCreationDateTime = (array_key_exists("TicketCreationDateTime", $array_inbox) ? $array_inbox["TicketCreationDateTime"] : null);
        $inbox->TelegramSenderId = (array_key_exists("TelegramSenderId", $array_inbox) ? $array_inbox["TelegramSenderId"] : null);
        $inbox->ServiceCatalogId = (array_key_exists("ServiceCatalogId", $array_inbox) ? $array_inbox["ServiceCatalogId"] : null);
        $inbox->EmployeeSenderId = (array_key_exists("EmployeeSenderId", $array_inbox) ? $array_inbox["EmployeeSenderId"] : null);
        $inbox->EmployeeAssigneeId = (array_key_exists("EmployeeAssigneeId", $array_inbox) ? $array_inbox["EmployeeAssigneeId"] : null);
        $inbox->EmployeeAssigneeGroupId = (array_key_exists("EmployeeAssigneeGroupId", $array_inbox) ? $array_inbox["EmployeeAssigneeGroupId"] : null);
        $inbox->save();
        $inbox->load('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype', 'employeeremedy');
        return $inbox;
    }

    public function refresh(Inbox $inbox): Inbox
    {
        $inbox->refresh();
        return $inbox->load('user', 'supportgroupassigneeremedy', 'chatsubscribeduser.employeeremedy', 'servicecatalog.servicechannel.servicetype', 'employeeremedy');
    }

    public function addByInc(Incident $incident, Int $request_id = null): ?Inbox
    {
        
        // try {
        if($request_id != null){
            $inbox = Inbox::find($request_id);
        }else{
            $inbox = new Inbox();
        }
        
        $inbox->ServiceCatalogId = 88;
        $asgn = $this->supuser_ds->findByCompanyOrgGroupName($incident->assignee_support_company, $incident->assignee_support_organization, $incident->assignee_support_group_name, $incident->assignee_name);
        if (!$asgn) {
            $asgn = $this->supgroup_ds->findByComOrgGrp($incident->assignee_support_company, $incident->assignee_support_organization, $incident->assignee_support_group_name);
        } else {
            $inbox->EmployeeAssigneeId = $asgn->employee_remedy_id;
            $inbox->EmployeeAssigneeGroupId = $asgn->support_group_id;
        }

        if ($incident->reported_source == 'Phone') {
            $inbox->RecipientID = 'CALL';
            if ($asgn->support_group_name == "SERVICEDESK") {
                $inbox->ServiceCatalogId = 35;
            } else if ($asgn->support_group_name == "SERVICEDESK - CABANG") {
                $inbox->ServiceCatalogId = 46;
            } else if ($asgn->support_group_name == "SERVICEDESK - KARTU KREDIT") {
                $inbox->ServiceCatalogId = 47;
            }
        } else if ($incident->reported_source == 'Email') {
            $inbox->RecipientID = 'EMAIL';
            if ($asgn->support_group_name == "SERVICEDESK") {
                $inbox->ServiceCatalogId = 36;
            } else if ($asgn->support_group_name == "SERVICEDESK - CABANG") {
                $inbox->ServiceCatalogId = 48;
            } else if ($asgn->support_group_name == "SERVICEDESK - KARTU KREDIT") {
                $inbox->ServiceCatalogId = 49;
            } else if ($asgn->support_group_name == "SERVICEDESK - 2ND LAYER INCIDENT" || $asgn->support_group_name == "SERVICEDESK - MCM") {
                $sc = $this->srvctlg_ds->findByTier($incident->prod_cat_tier1, $incident->prod_cat_tier2, $incident->prod_cat_tier3, $incident->opr_cat_tier1, $incident->opr_cat_tier2, $incident->opr_cat_tier3);
                if ($sc !== null) {
                    $inbox->ServiceCatalogId = $sc->id;
                }else{
                    $inbox->ServiceCatalogId = 104;
                }
            }
        } else if ($incident->reported_source == 'Whatsapp') {
            $inbox->RecipientID = 'WA';
            $inbox->ServiceCatalogId = 37;
        }else if($incident->reported_source == 'Officer on Duty'){
            $inbox->ServiceCatalogId = 116;
            $incident->status = "Assigned";
            $inbox->RecipientID = 'OD_COMCEN';
            $inbox->IncidentOdDecode = $incident->incident_od_decode;
            $inbox->IncidentStartEkslasiDate = date('Y-m-d H:i:s');
            $inbox->ProcessStatusEkskalasiIncident = "INPROGRESS";

        }

        $inbox->ReceivingDateTime = date('Y-m-d H:i:s');
        $inbox->Text = ($incident->reported_source == 'Officer on Duty') ? $incident->incident : $incident->summary_description;
        $inbox->SenderNumber = $incident->requester_full_name;
        $inbox->UDH = $incident->udh;
        $inbox->SMSCNumber = $incident->requester_person_id;
        $inbox->TextDecoded = $incident->text_decoded;
        $inbox->BookDateTime = date('Y-m-d H:i:s');
        $inbox->AssigneeId = Auth::user()->id;
        $inbox->RemedyTicketId = $incident->incident_number !== null ? $incident->incident_number : "N/A";
        $inbox->ProcessStatus = $incident->generateStatus($incident->status);
        if ($inbox->ProcessStatus == 'CLOSED' || $inbox->ProcessStatus == 'REJECT') {
            $inbox->FinishDateTime = date('Y-m-d H:i:s');
        }
        if ($inbox->ProcessStatus != 'CLOSED' || $inbox->ProcessStatus != 'REJECT') {
            $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
        }
        $inbox->EmployeeSenderId = $incident->requester_person_id;
        
        $inbox->save();
        return $this->refresh($inbox);
        // } catch (\Exception $th) {
        //     return null;
        // }
    }

    public function addIncByFirstLayer(Incident $incident, Int $request_id = null): ?Inbox
    {
       
        // try {
        if($request_id != null){
            $inbox = Inbox::find($request_id);
        }else{
            $inbox = new Inbox();
        }
       
        $inbox->ServiceCatalogId = 105;
        $incident->status = "Assigned";
        $inbox->RecipientID = 'OD_COMCEN';
        $inbox->IncidentOdDecode = $incident->incident_od_decode;
      //  $inbox->IncidentStartEkslasiDate = date('Y-m-d H:i:s');
        $inbox->ProcessStatusEkskalasiIncident = $incident->ekskalasiStatus;
        $inbox->ReceivingDateTime = date('Y-m-d H:i:s');
        $inbox->Text = ($incident->reported_source == 'Officer on Duty') ? $incident->incident : $incident->summary_description;
        $inbox->SenderNumber = $incident->requester_full_name;
        $inbox->UDH = $incident->udh;
        $inbox->SMSCNumber = $incident->requester_person_id;
        $inbox->TextDecoded = $incident->text_decoded;
        $inbox->BookDateTime = date('Y-m-d H:i:s');
        $inbox->AssigneeId = Auth::user()->id;
        $inbox->RemedyTicketId = $incident->incident_number !== null ? $incident->incident_number : "N/A";
        $inbox->ProcessStatus = $incident->generateStatus($incident->status);
        if ($inbox->ProcessStatus == 'CLOSED' || $inbox->ProcessStatus == 'REJECT') {
            $inbox->FinishDateTime = date('Y-m-d H:i:s');
        }
        if ($inbox->ProcessStatus != 'CLOSED' || $inbox->ProcessStatus != 'REJECT') {
            $inbox->TicketCreationDateTime = date('Y-m-d H:i:s');
        }
        if($incident->ekskalasiStatus == 'INPROGRESS'){
                $inbox->IncidentStartEkslasiDate = date('Y-m-d H:i',strtotime($incident->waktuTerindikasiSelected));//date('Y-m-d H:i:s');
        }
        $inbox->EmployeeSenderId = $incident->requester_person_id;
        
        $inbox->save();
        return $this->refresh($inbox);
        // } catch (\Exception $th) {
        //     return null;
        // }
    }


    public function addByWo(Workorder $workorder): ?Inbox
    {
        return new Inbox();
    }

    public function findByInc($remedy_ticket_id): Inbox
    {
        return Inbox::where('RemedyTicketId', $remedy_ticket_id)->first();
    }

    //Reporting
    public function getGroupGrandTotalTask(String $support_group_id, String $start_datetime, String $end_datetime, array $status)
    {
        $draft = Inbox::select(DB::raw("\"RecipientID\" as sources"), DB::raw("count(\"RecipientID\") as total"), "EmployeeAssigneeId as employee_remedy_id")
            ->where('EmployeeAssigneeGroupId', $support_group_id)
            ->where('ReceivingDateTime', '>=', $start_datetime)
            ->where('ReceivingDateTime', '<', $end_datetime)
            ->whereIn('ProcessStatus', $status)
            ->groupBy("RecipientID", "EmployeeAssigneeId")->get();

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[$value->employee_remedy_id][$value->sources] = (int) $value->total;
        }

        $channels = ['SMS', 'ITSRM', 'TELEGRAM', 'WA', 'EMAIL', 'CALL', 'LIVECHAT'];
        $data = array();
        foreach (User::where('sgn_id_remedy', $support_group_id)->where('status', true)->get() as $user) {
            if (!empty($temp_data[$user->employee_remedy_id])) {
                $tmp = array();
                foreach ($channels as $channel) {
                    if (isset($temp_data[$user->employee_remedy_id][$channel])) {
                        $tmp[$channel] = $temp_data[$user->employee_remedy_id][$channel];
                    } else {
                        $tmp[$channel] = 0;
                    }
                }
                array_push($data, ["name" => $user->name, "grandTotal" => array_sum($temp_data[$user->employee_remedy_id]), "totalChannelRequest" => $tmp]);
            } else {
                array_push($data, ["name" => $user->name, "grandTotal" => 0, "totalChannelRequest" => ["ITSRM" => 0, 'SMS' => 0, 'TELEGRAM' => 0, 'WA' => 0, 'EMAIL' => 0, 'CALL' => 0, 'LIVECHAT' => 0]]);
            }
        }

        return $data;
    }

    public function getGroupHourlyTotalTask(String $support_group_id, String $start_datetime, String $end_datetime, String $field_name, array $process_status)
    {
        $draft = Inbox::select(DB::raw("date_part('hour', \"" . $field_name . "\") as hour"), DB::raw("count(date_part('hour', \"" . $field_name . "\")) as total"), "EmployeeAssigneeId as employee_remedy_id")
            ->where('EmployeeAssigneeGroupId', $support_group_id)
            ->where($field_name, '>=', $start_datetime)
            ->where($field_name, '<', $end_datetime)
            ->whereIn('ProcessStatus', $process_status)
            ->groupBy(DB::raw("date_part('hour',\"" . $field_name . "\")"), "EmployeeAssigneeId")->get();

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[$value->employee_remedy_id][$value->hour] = (int) $value->total;
        }

        $temp_data_user = array();
        foreach (User::where('sgn_id_remedy', $support_group_id)->where('status', true)->get() as $user) {
            if (!empty($temp_data[$user->employee_remedy_id])) {
                array_push($temp_data_user, ["name" => $user->name, "totalHourlyRequest" => $temp_data[$user->employee_remedy_id]]);
            } else {
                array_push($temp_data_user, ["name" => $user->name, "totalHourlyRequest" => [(string) "1" => 0]]);
            }
        }

        $data = array();
        foreach ($temp_data_user as $user) {
            $child_data = array();
            for ($i = 0; $i < 24; $i++) {
                if (isset($user["totalHourlyRequest"][$i])) {
                    $child_data[$i] = $user["totalHourlyRequest"][$i];
                } else {
                    $child_data[$i] =  0;
                }
            }
            array_push($data, ["name" => $user["name"], "totalHourlyRequest" => $child_data]);
        }

        return $data;
    }

    public function getGroupDailyTotalTask(String $support_group_id, String $start_date, String $end_date, String $field_name)
    {
        $draft = Inbox::select(DB::raw("count(date(\"" . $field_name . "\")) as total"), DB::raw("date(\"" . $field_name . "\") as date_recorded"), "EmployeeAssigneeId as employee_remedy_id")
            ->where('EmployeeAssigneeGroupId', $support_group_id)
            ->where($field_name, '>=', $start_date)
            ->where($field_name, '<', $end_date)
            ->groupBy(DB::raw("date(\"" . $field_name . "\")"), 'EmployeeAssigneeId')->get();

        $date_diff = round(((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24)));

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[$value->employee_remedy_id][date('d-m-Y', strtotime($value->date_recorded))] = (int) $value->total;
        }

        $data = array();
        foreach (User::where('sgn_id_remedy', $support_group_id)->where('status', true)->get() as $user) {
            if (!empty($temp_data[$user->employee_remedy_id])) {
                $tmp = array();
                for ($i = 0; $i < $date_diff; $i++) {
                    $cur_date = date("d-m-Y", strtotime("+" . $i . " day", strtotime($start_date)));
                    if (isset($temp_data[$user->employee_remedy_id][$cur_date])) {
                        $tmp[(string) $cur_date] = $temp_data[$user->employee_remedy_id][$cur_date];
                    } else {
                        $tmp[(string) $cur_date] = 0;
                    }
                }
                array_push($data, ["name" => $user->name, "totalDailyTask" => $tmp]);
            } else {
                $tmp = array();
                for ($i = 0; $i < $date_diff; $i++) {
                    $cur_date = date("d-m-Y", strtotime("+" . $i . " day", strtotime($start_date)));
                    $tmp[(string) $cur_date] = 0;
                }
                array_push($data, ["name" => $user->name, "totalDailyTask" => $tmp]);
            }
        }

        return $data;
    }

    public function getSingleDailyTaskStats($employee_assignee_id, $start_datetime, $end_datetime, $field_name)
    {
        $draft = Inbox::select(DB::raw("date_part('hour', \"" . $field_name . "\") as hour"), DB::raw("count(date_part('minute', \"" . $field_name . "\")) as total"))
            ->where('EmployeeAssigneeId', $employee_assignee_id)
            ->where($field_name, '>=', $start_datetime)
            ->where($field_name, '<', $end_datetime)
            ->groupBy(DB::raw("date_part('hour',\"" . $field_name . "\")"))->get();

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[$value->hour] = (int) $value->total;
        }

        $data = array();
        for ($i = 0; $i < 24; $i++) {
            if (!empty($temp_data[$i])) {
                $data[$i] = $temp_data[$i];
            } else {
                $data[$i] = 0;
            }
        }

        return $data;
    }

    public function getSingleWeeklyTaskStats(String $employee_assignee_id, String $start_date, String $end_date, String $field_name)
    {
        $draft = Inbox::select(DB::raw("count(date(\"" . $field_name . "\")) as total"), DB::raw("date(\"" . $field_name . "\") as date_recorded"))
            ->where('EmployeeAssigneeId', $employee_assignee_id)
            ->where($field_name, '>=', $start_date)
            ->where($field_name, '<', $end_date)
            ->groupBy(DB::raw("date(\"" . $field_name . "\")"))->get();

        $date_diff = round(((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24)));

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[date('d-m-Y', strtotime($value->date_recorded))] = (int) $value->total;
        }

        $data = array();
        for ($i = 0; $i < $date_diff; $i++) {
            $cur_date = date("d-m-Y", strtotime("+" . $i . " day", strtotime($start_date)));
            if (!empty($temp_data[$cur_date])) {
                $data[$cur_date] = $temp_data[$cur_date];
            } else {
                $data[$cur_date] = 0;
            }
        }

        return $data;
    }

    public function getChannelStats(String $employee_assignee_id, String $start_date, String $end_date, String $status, String $field_name)
    {
        $draft = Inbox::select(DB::raw('count("RecipientID") as total'), 'RecipientID')
            ->where($field_name, '>=', $start_date)
            ->where($field_name, '<', $end_date)
            ->where('ProcessStatus', $status)
            ->where('EmployeeAssigneeId', $employee_assignee_id)
            ->groupBy('RecipientID')
            ->get();

        $channel = ['SMS', 'ITSRM', 'TELEGRAM', 'WA', 'EMAIL', 'CALL', 'LIVECHAT'];

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[$value->RecipientID] = (int) $value->total;
        }

        $data = array();
        foreach ($channel as $value) {
            if (!empty($temp_data[$value])) {
                $data[$value] = $temp_data[$value];
            } else {
                $data[$value] = 0;
            }
        }

        return $data;
    }

    public function getSingleDailyTaskStatsFilter(String $user_id, String $start_datetime, String $end_datetime, String $field_name, String $layanan, String $layananc)
    {
        $draft = Inbox::select(DB::raw("date_part('hour', \"" . $field_name . "\") as hour"), DB::raw("count(date_part('minute', \"" . $field_name . "\")) as total"))
            ->where('EmployeeAssigneeGroupId', $user_id)
            ->where($field_name, '>=', $start_datetime)
            ->where($field_name, '<', $end_datetime)
            ->whereHas('servicecatalog', function ($q) use ($layanan, $layananc) {
                $q->whereHas('servicechannel', function ($q) use ($layanan, $layananc) {
                    $q->whereHas('servicetype', function ($q) use ($layanan, $layananc) {
                        $q->where('encoding_name', $layanan)->orwhere('encoding_name', $layananc);
                    });
                });
            })
            ->groupBy(DB::raw("date_part('hour',\"" . $field_name . "\")"))->get();

        $temp_data = array();
        foreach ($draft as $value) {
            $temp_data[$value->hour] = (int) $value->total;
        }

        $data = array();
        for ($i = 0; $i < 24; $i++) {
            if (!empty($temp_data[$i])) {
                $data[$i] = $temp_data[$i];
            } else {
                $data[$i] = 0;
            }
        }
        return $data;
    }
    // End of reporting

    //History
    public function getRequestsHistory(String $support_group_id, String $service_catalog_type, $manager, $checksgp, String $field = "", $searchdate = "", $tosearchdate = "", $search = "")
    {
        if ($manager == 'true') {
            $group_id = 'EmployeeAssigneeGroupId';
        } else {
            $group_id = 'EmployeeAssigneeId';
        }
        if($checksgp == "false"){
            if ($field && $searchdate && $tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($searchdate && $tosearchdate && !$search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($field && $searchdate && !$tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($field && !$searchdate && $tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($searchdate && !$tosearchdate && !$search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($field && !$searchdate && !$tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif (!$searchdate && $tosearchdate && !$search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } else {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where($group_id, $support_group_id)
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            }
        }else{
            if ($field && $searchdate && $tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($searchdate && $tosearchdate && !$search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($field && $searchdate && !$tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($field && !$searchdate && $tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($searchdate && !$tosearchdate && !$search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif ($field && !$searchdate && !$tosearchdate && $search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->where($field, 'ilike', '%' . $search . '%')
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } elseif (!$searchdate && $tosearchdate && !$search) {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->whereBetween('FinishDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            } else {
                $results = Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                    ->where('ProcessStatus', 'CLOSED')
                    ->whereHas('servicecatalog', function ($q) use ($service_catalog_type) {
                        $q->where('service_catalog_type', $service_catalog_type);
                    })
                    ->orderBy('BookDateTime', 'desc')
                    ->orderBy('id', 'desc');
            }
        }
        $data = new InboxCollection($results->paginate(15));
        $collect_data = [
            'data' => $data,
            'count' => $results->count()
        ];
        return $collect_data;
    }

    public function getRequestListByStatusAndSupportGroupJson($process_status, $channel_name, $employee_assignee_group_id, $field = "", $searchdate = "", $tosearchdate = "", $name = "")
    {
        if ($searchdate && $tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->where($field, $name)
                ->whereBetween('ReceivingDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif ($searchdate && $tosearchdate && !$name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereBetween('BookDateTime', array($searchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('BookDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif ($searchdate && !$tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->where($field, $name)
                ->whereBetween('ReceivingDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif (!$searchdate && $tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->where($field, $name)
                ->whereBetween('ReceivingDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif ($searchdate && !$tosearchdate && !$name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereBetween('BookDateTime', array($searchdate . ' 00:00:00', $searchdate . ' 23:59:59'))
                ->orderBy('BookDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif (!$searchdate && $tosearchdate && !$name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->whereBetween('BookDateTime', array($tosearchdate . ' 00:00:00', $tosearchdate . ' 23:59:59'))
                ->orderBy('BookDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } elseif (!$searchdate && !$tosearchdate && $name) {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->where($field, $name)
                ->orderBy('ReceivingDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        } else {
            $data = InboxResource::collection(Inbox::with('user', 'supportgroupassigneeremedy', 'chatsubscribeduser', 'servicecatalog.servicechannel.servicetype')
                ->whereIn('ProcessStatus', $process_status)
                ->whereIn('RecipientID', $channel_name)
                ->where('EmployeeAssigneeGroupId', $employee_assignee_group_id)
                ->orderBy('BookDateTime', 'asc')
                ->orderBy('id', 'asc')
                ->get());
        }

        $collect_data = [
            'data' => $data,
            'count' => $data->count()
        ];

        return $collect_data;
    }
    //End of history
}
