require("../../bootstrap");

window.Vue = require("vue");
window.events = new Vue();
window.flash = function(message) {
    window.events.$emit("flash", message);
};
window.dismissflash = function() {
    window.events.$emit("dismissflash");
};
window.refreshPendingCount = function(type) {
    window.events.$emit("refreshCount", type);
};

Vue.component("chat-messages-log", () =>
    import("../../components/dashboard/childs/livechat/ChatLog.vue")
);
Vue.component("chat-messages", () =>
    import("../../components/dashboard/childs/livechat/ChatMessage.vue")
);
Vue.component("chat-compose", () =>
    import("../../components/dashboard/childs/livechat/ChatCompose.vue")
);
Vue.component("manage-task", () =>
    import("../../components/dashboard_manager/AgentTask.vue")
);
Vue.component("working-task", () =>
    import("../../components/dashboard/WorkingRequestSD.vue")
);
Vue.component("modal-livechat", () =>
    import("../../components/dashboard/childs/livechat/ModalLiveChat.vue")
);
Vue.component("modal-view", () =>
    import("../../components/dashboard/childs/view/ModalView.vue")
);
Vue.component("modal-workorder-manager", () =>
    import(
        "../../components/dashboard_manager/childs/ModalWorkOrderManagerGeneric.vue"
    )
);
Vue.component("modal-workorder-closure", () =>
    import(
        "../../components/dashboard/childs/closure/ModalWorkorderClosure.vue"
    )
);
Vue.component("spinner-loader", () =>
    import("../../components/dashboard/SpinnerCenter.vue")
);
Vue.component("spinner-data-loading", () =>
    import("../../components/dashboard/SpinnerDataLoading.vue")
);
Vue.component("modal-delegate", () =>
    import("../../components/dashboard/childs/delegate/ModalDelegate.vue")
);
Vue.component("modal-bds-error-tlg", () =>
    import("../../components/dashboard/childs/bds_error/ModalBDSError.vue")
);
Vue.component("modal-change-assignee", () =>
    import("../../components/dashboard_manager/childs/ModalChangeAssignee.vue")
);
Vue.component("modal-create-ticket-live-chat", () =>
    import(
        "../../components/dashboard/childs/create_ticket/ModalTaskCreateTicketLiveChat.vue"
    )
);
Vue.component("task-incoming", () =>
    import("../../components/dashboard/TaskIncoming.vue")
);
Vue.component("modal-incoming-incident", () =>
    import(
        "../../components/dashboard/childs/create_ticket/ModalCreateIncomingIncident.vue"
    )
);
Vue.component("modal-admin-manual", () =>
    import(
        "../../components/dashboard/childs/admin_manual/ModalAdminManual.vue"
    )
);

Vue.component("modal-admin-manual-extended", () =>
    import(
        "../../components/dashboard/childs/admin_manual/ModalAdminManualExtended.vue"
    )
);
Vue.component("modal-incident-history", () =>
    import("../../components/dashboard/childs/incident/ModalIncident.vue")
);
Vue.component("modal-delegate-manager", () =>
    import("../../components/dashboard_manager/childs/ModalDelegateManager.vue")
);
Vue.component("modal-closure-manager", () =>
    import("../../components/dashboard_manager/childs/ModalClosureManager.vue")
);
Vue.component("modal-incident-manager", () =>
    import(
        "../../components/dashboard_manager/childs/ModalIncidentManagerGeneric.vue"
    )
);
Vue.component("modal-wo-history", () =>
    import("../../components/dashboard/childs/generic/ModalWOGeneric.vue")
);
Vue.component("count-pending-ticket", () =>
    import("../../components/sidebar/CountPendingTicket.vue")
);
Vue.component("flash", () => import("../../components/FlashMessage.vue"));
Vue.component("stats-group-hourly-total", () =>
    import("../../components/dashboard_manager/GroupChartHourlyTaskReport.vue")
);
Vue.component("stats-group-grand-total", () =>
    import("../../components/dashboard_manager/GroupChartGrandTotalReport.vue")
);
Vue.component("stats-group-daily-total", () =>
    import("../../components/dashboard_manager/GroupChartDailyTaskReport.vue")
);
Vue.component("job-history-wo", () =>
    import("../../components/dashboard/JobHistory_WO.vue")
);
Vue.component("job-history-inc", () =>
    import("../../components/dashboard/JobHistory_INC.vue")
);
Vue.component("v-select", require("vue-select").VueSelect);
Vue.component("stats-chart-weekly", () =>
    import("../../components/dashboard/ChartWeeklyTaskReport.vue")
);
Vue.component("channel-chart-daily", () =>
    import("../../components/dashboard/ChartDailyChannelReport.vue")
);
Vue.component("channel-chart-weekly", () =>
    import("../../components/dashboard/ChartWeeklyChannelReport.vue")
);
Vue.component("modal-konfirmasi-transaksi", () =>
    import(
        "../../components/dashboard/childs/modal_wo/ModalKonfirmasiTransaksi.vue"
    )
);
Vue.component("modal-transaksi-emoney", () =>
    import(
        "../../components/dashboard/childs/modal_wo/ModalTransaksiEmoney.vue"
    )
);
Vue.component("modal-aktivasi-rekening", () =>
    import(
        "../../components/dashboard/childs/modal_wo/ModalAktivasiRekening.vue"
    )
);
Vue.component("modal-kotran", () =>
    import("../../components/dashboard/childs/modal_wo/ModalKotran.vue")
);
Vue.component("modal-trendmicro", () =>
    import("../../components/dashboard/childs/modal_wo/ModalTrendmicro.vue")
);
Vue.component("modal-informasi-token", () =>
    import("../../components/dashboard/childs/modal_wo/ModalInformasiToken.vue")
);

Vue.component("audit-trail-sds", () =>
    import("../../components/dashboard_manager/AuditTrailSDS.vue")
);

import "es6-promise/auto";
import Vuex from "vuex";
import VeeValidate from "vee-validate";
import VueClipboard from "vue-clipboard2";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import moment from "moment";

Vue.filter("mydateformat", function(value) {
    if (value) {
        return moment(String(value)).format("DD/MM/YYYY HH:mm:ss");
    }
});
Vue.use(VeeValidate, {
    aria: true,
    classNames: {},
    classes: false,
    delay: 0,
    dictionary: null,
    errorBagName: "errors", // change if property conflicts
    events: "input|blur",
    fieldsBagName: "fields",
    i18n: null, // the vue-i18n plugin instance
    i18nRootKey: "validations", // the nested key under which the validation messages will be located
    inject: true,
    locale: "en",
    validity: false,
    useConstraintAttrs: true
});
Vue.use(VueClipboard);
Vue.use(Vuex);
Vue.use(Loading);

const appManager = new Vue({
    el: "#app-manager",
    data: {
        selecteddate: "",
        selected2date: "",
        selected3date: "",
        statusoptions: [
            "ALL",
            "INPROGRESS",
            "PENDING",
            "WAITINGAPR",
            "PLANNING",
            "CLOSED"
        ],
        selectedstatus: "ALL",
        periodoptions: ["DAILY", "WEEKLY", "BIWEEKLY", "MONTHLY"],
        selectedperiod: "DAILY",
        period2options: ["WEEKLY", "BIWEEKLY", "MONTHLY"],
        selected2period: "WEEKLY",
        grandtotalstats: chart.grandtotal,
        hourlytotalstats: chart.hourlytotal,
        dailytotalstats: chart.dailytotal,
        // personalstats: chart,
        requestdata: [],
        requestobject: {},
        requestid: 0,
        processstatus: "",
        requests: requestQueue,
        // tasks: workingTask,
        inhistory: inHistory,
        wohistory: woHistory,
        auth: auth,
        ismodal: false,
        isdelegating: false,
        isclosuring: false,
        isdataloaded: false,
        isresponsereceived: false,
        isincidentcreation: false,
        isincidentlivechat: false,
        isredirecttolivechat: false,
        isdelegateenabled: auth.is_delegate_enabled,
        modalname: "",
        source: null,
        servicetype: serviceType
    },
    methods: {
        searchTotalDailyRequestByChannel(processstatus, period, selecteddate) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            axios
                .post("/dashboard/manager/search/totalrequest", {
                    processstatus: processstatus,
                    period: period,
                    selecteddate: selecteddate
                })
                .then(response => {
                    this.grandtotalstats = response.data;
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                });
        },

        searchTaskDailyRequestByHour(processstatus, selecteddate) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            axios
                .post("/dashboard/manager/search/hourlyrequest", {
                    processstatus: processstatus,
                    selecteddate: selecteddate
                })
                .then(response => {
                    this.hourlytotalstats = response.data;
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                });
        },

        searchTaskDailyRequest(period, selecteddate) {
            let loader = this.$loading.show({
                loader: "dots"
            });
            axios
                .post("/dashboard/manager/search/dailyrequest", {
                    period: period,
                    selecteddate: selecteddate
                })
                .then(response => {
                    this.dailytotalstats = response.data;
                    loader.hide();
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        flash({
                            id: "error code status",
                            message: error.response.data.message,
                            flashtype: "danger"
                        });
                    }
                });
        },

        createStagingIncident(req) {
            this.ismodal = true;
            this.processstatus = req.processstatus;
            this.modalname = req.modalname;
            this.source = req.source;
            this.isdataloaded = req.isdataloaded;
            this.requestdata = req.requestdata;
        },

        createIncomingIncident(req) {
            this.ismodal = true;
            this.isincidentcreation = true;
            this.source = req.source;
            this.modalname = req.modalname;
        },

        redirectToLivechat(req) {
            this.isredirecttolivechat = req.isredirecttolivechat;
            this.modalname = req.modalname;
            this.requestdata = req.requestdata;
            this.processstatus = req.requestdata.ProcessStatus;
            this.ismodal = true;
        },

        popFlashMessage(id, message, type) {
            setTimeout(() => {
                flash({
                    id: id,
                    message: message,
                    flashtype: type
                });
            }, 500);
        },

        taskCompleteLiveChat(req) {
            this.requestobject = req.request;
            this.isincidentlivechat = req.isincidentlivechat;
            this.modalname = req.modalname;
            this.ismodal = true;
        },

        pushPopFlashMessage(req) {
            this.modalClosed();
            if (req.status) {
                if (typeof req.wsdlStatus !== "undefined") {
                    if (req.wsdlStatus) {
                        this.popFlashMessage(req.id, req.message, "success");
                    } else {
                        this.popFlashMessage(req.id, req.message, "warning");
                        refreshPendingCount("pending-ticket");
                    }
                } else {
                    this.popFlashMessage(req.id, req.message, "info");
                }
            } else {
                this.popFlashMessage(req.id, req.message, "danger");
                refreshPendingCount("pending-ticket");
            }
        },

        closuringTask(req) {
            this.requestobject = req.request;
            this.isclosuring = req.isclosuring;
            this.modalname = req.modalname;
            this.ismodal = true;
        },

        delegateTask(req) {
            this.requestobject = req.request;
            this.isdelegating = req.isdelegating;
            this.modalname = req.modalname;
            this.ismodal = true;
        },

        modalClosed() {
            this.isdataloaded = false;
            this.processstatus = "";
            this.requestid = 0;
            this.ismodal = false;
            this.modalname = "";
            this.isclosuring = false;
            this.isdelegating = false;
            this.isincidentcreation = false;
            this.isincidentlivechat = false;
            this.isredirecttolivechat = false;
        },

        loadSelectedRequest(req) {
            this.isredirecttolivechat = req.isredirecttolivechat;
            axios
                .post("/dashboard/manager/requestqueue/pick", req)
                .then(response => {
                    if (response.data.status == false) {
                        this.isdataloaded = response.data.status;
                        this.processstatus = response.data.processStatus;
                        this.ismodal = true;
                        if (this.processstatus != "QUEUED") {
                            this.popFlashMessage(
                                req.id,
                                response.data.message,
                                "danger"
                            );
                            refreshPendingCount();
                        }
                    } else {
                        this.processstatus = response.data.processStatus;
                        this.requestdata = response.data.responsedata;
                        this.requestid = req.id;
                        this.isdataloaded = true;
                        this.ismodal = true;
                        this.modalname = req.modalname;
                        if (response.data.processStatus == "QUEUED") {
                            if (response.data.wsdlStatus) {
                                this.popFlashMessage(
                                    req.id,
                                    response.data.message,
                                    "success"
                                );
                            } else {
                                this.popFlashMessage(
                                    req.id,
                                    response.data.message,
                                    "warning"
                                );
                                refreshPendingCount();
                            }
                        }
                        if (this.modalname == "modal-livechat") {
                            this.scrollToEnd();
                        }
                    }
                })
                .catch(error => {
                    if (
                        error.response.status === 419 ||
                        error.response.status === 401
                    ) {
                        window.alert(
                            "Mohon maaf session anda telah habis, tekan ok untuk relogin"
                        );
                        location.reload(false);
                    } else {
                        this.popFlashMessage(
                            "catch error code",
                            error.response.data.message,
                            "danger"
                        );
                    }
                });
        },

        scrollToEnd: function() {
            setTimeout(() => {
                if (this.processstatus == "CLOSED" && this.isdataloaded) {
                    var container = this.$refs.chatmodal.$refs.chatlogclosed
                        .$refs.chatcontainer;
                    container.scrollTop = container.scrollHeight;
                } else if (this.isdataloaded) {
                    var container = this.$refs.chatmodal.$refs.chatlogactive
                        .$refs.chatcontainer;
                    container.scrollTop = container.scrollHeight;
                }
            }, 800);
        }
    }
});
