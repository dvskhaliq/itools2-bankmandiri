import '../../bootstrap';
import 'es6-promise/auto';
import 'vuetify/dist/vuetify.min.css';
import "vue-loading-overlay/dist/vue-loading.css";
import Vuex from 'vuex';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Loading from "vue-loading-overlay";
import Vuetify from 'vuetify';

window.Vue = Vue;
window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}
Vue.component('table-bds-crm-detail', ()=> import('../../components/bds_connection/atm/crm_detail/BdsCrmDetail.vue'));
Vue.component('componen-confirm-delete', ()=> import('../../components/bds_connection/atm/crm_detail/BdsCrmDetailDelete.vue'));
Vue.component('componen-modal-edit', ()=> import('../../components/bds_connection/atm/crm_detail/BdsCrmDetailUpdate.vue'));
Vue.component('componen-bds-crm-add', ()=> import('../../components/bds_connection/atm/crm_detail/BdsCrmDetailAdd.vue'));
Vue.component('flash', ()=> import('../../components/FlashMessage2.vue'));
Vue.use(Vuex);
Vue.use(Vuetify);
Vue.use(Loading);

const config = {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: null,
  errorBagName: 'errors', // change if property conflicts
  events: 'input|blur',
  fieldsBagName: 'fields',
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: 'validations', // the nested key under which the validation messages will be located
  inject: true,
  locale: 'en',
  validity: false,
  useConstraintAttrs: true
};
Vue.use(VeeValidate, config);

const vuetify = new Vuetify();

const bds_crm_detail = new Vue({
    el: '#bds_crm_detail',
    vuetify,
    data: {
        data_crm_detail: null,
        crm_detail_prop: {},
        ismodal: false,
        alert: false,
        isloaded: false,
    },

    mounted() {
        this.getData();
    },

    methods: {
        setFlashDanger(reqResponse) {
            flash({
                id: "alert-danger",
                message: reqResponse.message,
                flashtype: "danger"
            });
        },

        setFlashInfo(reqResponse) {
            flash({
                id: "alert-info",
                message: reqResponse.message,
                flashtype: "info"
            });
        },

        getData() {
          let loader = this.$loading.show({
            loader: "dots"
          });
            this.alert = true;
            axios.get(`/crm_detail/list`)
                .then((response) => {
                    this.data_crm_detail = response.data;
                    loader.hide();
                }).catch(error => {
                  if (error.response.status === 419 || error.response.status === 401) {
                    window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                    location.reload(false);
                  } else {
                    this.setFlashDanger({
                      message: error.response.data.message
                    });
                  }
                  loader.hide();
                });
        },

        closeWindow() {
            this.isloaded = false;
            this.ismodal = false;
            this.crm_detail_prop = {};
        },

        submitDelete(reqResponse) {
            let loader = this.$loading.show({
              loader: "dots"
            });
            this.isloaded = false;
            this.alert = true;
            this.ismodal = false;
            axios.post('/crm_detail/delete', reqResponse.data).then(response => {
                this.getData();
                if (response.data.status == true) {
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result
                    });
                }
                loader.hide();
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger({
                  message: error.response.data.message
                });
              }
              loader.hide();
            });
        },

        crmDetailSelected(reqResponse) {
            this.ismodal = true;
            this.alert = false;
            axios.post('/crm_detail/selected', reqResponse).then(response => {
                if (response.data.status == true) {
                    this.isloaded = true;
                    this.crm_detail_prop = response.data.result;
                }
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger({
                  message: error.response.data.message
                });
              }
              this.isloaded = false;
            });
        },

        submitUpdate(reqResponse){
            let loader = this.$loading.show({
              loader: "dots"
            });
            this.alert = true;
            this.ismodal = false;
            this.isloaded = false;
            axios.post('/crm_detail/update', reqResponse).then(response => {
                this.getData();
                if (response.data.status == true) {
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result
                    });
                }
                loader.hide();
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger({
                  message: error.response.data.message
                });
              }
              loader.hide();
            });
        },

        submitAdd(reqResponse){
            let loader = this.$loading.show({
              loader: "dots"
            });
            this.alert = true;
            this.ismodal = false;
            this.isloaded = false;
            axios.post('/crm_detail/add', reqResponse).then(response => {
                this.getData();
                if (response.data.status == true) {
                    this.setFlashInfo({
                        message: response.data.result
                    });
                } else {
                    this.setFlashDanger({
                        message: response.data.result
                    });
                }
                loader.hide();
            }).catch(error => {
              if (error.response.status === 419 || error.response.status === 401) {
                window.alert("Mohon maaf session anda telah habis, tekan ok untuk relogin");
                location.reload(false);
              } else {
                this.setFlashDanger({
                  message: error.response.data.message
                });
              }
              loader.hide();
            });
        }
    },
});
